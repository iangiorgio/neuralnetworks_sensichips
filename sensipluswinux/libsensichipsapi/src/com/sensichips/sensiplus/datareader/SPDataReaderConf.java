package com.sensichips.sensiplus.datareader;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.datareader.*;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementMetaParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameter;
import com.sensichips.sensiplus.util.SPDataRepresentation;
import com.sensichips.sensiplus.util.SPDataRepresentationManager;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mario on 07/03/17.
 */
@SuppressWarnings("serial")
public class SPDataReaderConf implements Serializable {

    private boolean timePlot = true;
    private long delay = 1; // delay in ms
    private SPMeasurementParameter SPMeasurementParameter;
    private SPMeasurementMetaParameter meta;
    private SPConfiguration spConf;

    private String measureName = "";

    //public static final String MULTIPLIER_PREFIX[] = new String[]{"y", "z", "a", "f", "p", "n", new String(Character.toChars(0x00B5)), "m", "", "k", "M", "G", "T", "P", "E", "Z", "Y"};
    //public static final double MULTIPLIER[] = new double[]{ -21, -18, -15, -12, -9,  -6, -3,  0,  3,   6,   9,   12,  15,  18,  21,  24};


    //public static final String MEASURE_UNIT_LABEL[] = new String[]{"O", "F", "H", "C", "%", "V", "A", "L", "time", "", "rad"};
    //public static final String MEASURE_UNIT[] = new String[]{new String(Character.toChars(0x03a9)), "F", "H", new String(Character.toChars(0x00B0)) + "C", "%", "V", "A", "L", "t", "", "rad"};
    //        <!-- O=Ohm, F=Farad, H=Henry, C=Celsius, %=relative, V=Voltage, A=Current, L=Lumen, t=time-->


    private int maxDimQueue = 10000;
    private boolean restartDataReader = false;
    private int numOfMeasures = 9; // TODO: Andrebbe prelevato da SPMeasurementParameter in funzione della misura scelta
    private double spacePlotMin = -10;
    private double spacePlotMax = 40;
    private double thresholdAlarm = 0;
    private int TEMP_MEASUREMENT_MODE = com.sensichips.sensiplus.datareader.SPDataReader.TEMPMEASURE_STOP;
    private int multiplierIndex = 0;
    private String symbol = "";

    public SPDataRepresentationManager getSpDataRepresentationManager() {
        return spDataRepresentationManager;
    }

    public void setSpDataRepresentationManager(SPDataRepresentationManager spDataRepresentationManager) {
        this.spDataRepresentationManager = spDataRepresentationManager;
    }

    private SPDataRepresentationManager spDataRepresentationManager;

    public int getDimTimeQueue() {
        return dimTimeQueue;
    }


    public String getMeasureName(){
        return measureName;
    }


    public int getDimSpaceQueue() {
        return dimSpaceQueue;
    }

    private String yLabelPlot = "--";
    private String xLabelPlot = "--";
    private double validRangeMin = 0;
    private double validRangeMax = 100;

    private int dimTimeQueue = 100;
    private int dimSpaceQueue = 1;

    private boolean plotAutoRangeTime;

    private int valueToShow = 0;

    private List<SPChip> chipList;

    private static String LOG = "SPDataReaderConf";


    public SPDataReaderConf(SPMeasurementParameter SPMeasurementParameter,
                            SPConfiguration spConf, SPMeasurementMetaParameter meta,
                            boolean timePlot,
                            long delay, int dimSpaceQueue, int dimTimeQueue,
                            boolean restartDataReader,
                            int numOfMeasures,
                            boolean plotAutoRangeTime,
                            String xLabelPlot, String analyte,
                            double validRangeMin, double validRangeMax,
                            int valueToShow, int TEMP_MEASUREMENT_MODE,
                            String yLabelPlot, String measureName, SPDataRepresentationManager spDataRepresentationManager) throws SPException {


        this.spDataRepresentationManager = spDataRepresentationManager;
        this.timePlot = timePlot;
        this.delay = delay;
        this.dimSpaceQueue = dimSpaceQueue;
        this.dimTimeQueue = dimTimeQueue;
        this.SPMeasurementParameter = SPMeasurementParameter;
        this.spConf = spConf;
        this.restartDataReader = restartDataReader;
        this.numOfMeasures = numOfMeasures;
        //this.spacePlotMax = spacePlotMax;
        //this.spacePlotMin = spacePlotMin;
        this.validRangeMin = validRangeMin;
        this.validRangeMax = validRangeMax;
        this.plotAutoRangeTime = plotAutoRangeTime;
        this.valueToShow = valueToShow;
        this.meta = meta;
        //this.thresholdAlarm = thresholdAlarm;
        this.TEMP_MEASUREMENT_MODE = TEMP_MEASUREMENT_MODE;
        this.measureName = measureName;

        //this.multiplierIndex = multiplierIndex;

        this.xLabelPlot = xLabelPlot;
        this.yLabelPlot = yLabelPlot;

        if (spConf == null || spConf.getCluster() == null || spConf.getCluster().getActiveSPChipList() == null || spConf.getCluster().getActiveSPChipList() == null ||
                spConf.getCluster().getActiveSPChipList().size() == 0){
            throw new SPException("SPDataReaderConf");
        }


        chipList = spConf.getCluster().getActiveSPChipList();

        /*boolean flag;
        flag = chipList.get(0).getSpFamily() == null ||
                chipList.get(0).getSpFamily().getAnalyteList() == null ||
                chipList.get(0).getSpFamily().getAnalyteList().size() == 0;

        int i = 0;
        while(!flag && i < chipList.get(0).getSpFamily().getAnalyteList().size() ){
            flag = chipList.get(0).getSpFamily().getAnalyteList().get(i).getName().equals(analyte);
            if (!flag)
                i++;
        }
        if (flag){
            this.yLabelPlot = spConf.getCluster().getActiveSPChipList().get(0).getSpFamily().getAnalyteList().get(i).getMeasureUnit();
        } else {
            this.yLabelPlot = analyte;
        }
        // Sulla base dell'analita, cerca m ed n per ogni chip...
        flag = false;*/

    }

    public String getSymbol(){
        return symbol;
    }

    public int getMultiplierIndex(){
        return multiplierIndex;
    }


    public int getTEMP_MEASUREMENT_MODE(){
        return TEMP_MEASUREMENT_MODE;
    }



    public double getThresholdAlarm(){
        return thresholdAlarm;
    }

    public SPMeasurementMetaParameter getSPMeasurementMetaParameter(){
        return meta;
    }

    public void setSPMeasurementMetaParameter(SPMeasurementMetaParameter meta){
        this.meta = meta;
    }

    public int getValueToShow(){
        return valueToShow;
    }

    public boolean isPlotAutoRangeTime(){
        return plotAutoRangeTime;
    }

    public String getxLabelPlot() {
        return xLabelPlot;
    }

    public void setxLabelPlot(String xLabelPlot) {
        this.xLabelPlot = xLabelPlot;
    }

    public String getyLabelPlot() {
        return yLabelPlot;
    }

    public double getValidRangeMax() {
        return validRangeMax;
    }

    public void setValidRangeMax(double validRangeMax) {
        this.validRangeMax = validRangeMax;
    }

    public double getValidRangeMin() {
        return validRangeMin;
    }

    public void setValidRangeMin(double validRangeMin) {
        this.validRangeMin = validRangeMin;
    }

    public double getSpacePlotMax() {
        return spacePlotMax;
    }

    public double getSpacePlotMin() {
        return spacePlotMin;
    }

    public int getNumOfMeasures(){
        return numOfMeasures;
    }

    public boolean isTimePlot() {
        return timePlot;
    }

    public void setTimePlot(boolean timePlot){
        this.timePlot = timePlot;
    }

    public long getDelay() {
        return delay;
    }

    public SPMeasurementParameter getSPMeasurementParameter() {
        return SPMeasurementParameter;
    }

    public int getMaxDimQueue(){
        return maxDimQueue;
    }

    public SPConfiguration getSpConf(){
        return spConf;
    }

    public boolean getRestartDataReader(){
        return restartDataReader;
    }
}
