package com.sensichips.sensiplus.datareader.queue;


import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import javafx.application.Platform;
import sun.java2d.pipe.OutlineTextRenderer;

public class SPChartQueue {
    private int maxDim = 100;
    private int start = 0;
    private int end = 0;
    private int riemp = 0;
    private SPChartElement data[];
    private int numOfChips;
    private int numOfMeasures;
    private double timeRange[];
    private double valueRange[][][] = null;
    private int totalElement = 0;
    private boolean blockingWay = false;
    private boolean chipMask[];


    public int getRiemp() {
        return riemp;
    }

    public SPChartQueue(int maxDim, boolean blockingWait, boolean chipMask[],int statsQueueDepth){
        this.maxDim = maxDim;
        data = new SPChartElement[maxDim];
        start = 0;
        end = 0;
        this.blockingWay = blockingWait;
        this.chipMask = chipMask;
        this.statsQueueDepth = statsQueueDepth;

        numOfChips = chipMask.length;

        lastMaxSNR = new double[numOfChips];

    }

    public void clear(){
        start = 0;
        end = 0;
        riemp = 0;
    }

    public int size(){
        return maxDim;
    }


    public synchronized double[] getTimeRange(){
        return timeRange;
    }

    public synchronized void put(SPChartElement r){

        if (valueRange == null) {
            numOfChips = r.getNumOfChips();
            numOfMeasures = r.getNumOfMeasures();
            valueRange = new double[numOfChips][][];
            for (int i = 0; i < numOfChips; i++) {
                valueRange[i] = new double[numOfMeasures][];
            }
        }

        for (int i = 0; i < numOfChips; i++) {
            for(int k = 0; k < numOfMeasures; k++){
                valueRange[i][k] = new double[]{Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
            }
        }

        if (riemp < maxDim){
            riemp++;
        } else {
            start = (start + 1) % maxDim;
        }
        data[end] = r;
        end = (end + 1) % maxDim;

        totalElement++;
        totalElement = totalElement > maxDim ? maxDim : totalElement;

        //System.out.println("totalElement: " + totalElement);

        int startIndex = end - totalElement;
        if (startIndex < 0){
            startIndex = maxDim + startIndex;
        }
        timeRange = new double[]{Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
        int index = startIndex;
        for(int m = 0; m < totalElement; m++) {
            if (data[index].getxValue() < timeRange[0]) {
                timeRange[0] = data[index].getxValue();
            }
            if (data[index].getxValue() > timeRange[1]) {
                timeRange[1] = data[index].getxValue();
            }
            index = (index + 1) % maxDim;
        }

        // Update min e max nella coda...
        for(int i = 0; i < numOfChips; i++){
            for(int k = 0; k < numOfMeasures; k++){
                index = startIndex;
                for(int m = 0; m < totalElement; m++){
                    if (data[index].getyValues()[i][k] < valueRange[i][k][0]){
                        valueRange[i][k][0] = data[index].getyValues()[i][k];
                    }
                    if (data[index].getyValues()[i][k] > valueRange[i][k][1]){
                        valueRange[i][k][1] = data[index].getyValues()[i][k];
                    }
                    index = (index + 1) % maxDim;
                }
            }
        }
        for(int i = 0; i < numOfChips; i++){
            for (int k = 0; k < numOfMeasures; k++) {
                SPLogger.getLogInterface().d("SPChartQueue", "" + valueRange[i][k][0] + ", " + valueRange[i][k][1], SPLoggerInterface.DEBUG_VERBOSITY_L2);
            }
        }
        //System.out.println("riemp: " + riemp + "; start: " + start + "-" + end);

        notify();
    }

    public static final int MIN_INDEX = 0;
    public static final int MAX_INDEX = 1;
    public static final int MEAN_INDEX = 2;
    public static final int NOISE_DEV_STD_INDEX = 3;
    public static final int MAX_SNR_INDEX = 4;
    public static final int SNR_INDEX = 5;




    private int statsQueueDepth = 0;

    private double lastMaxSNR[];

    public synchronized double[][] getStatistics(int indexToSave){

        int numSamplesForStats = totalElement>statsQueueDepth?statsQueueDepth:totalElement;

        int numOfStatistics = 6;
        double[][] output = new double[numOfChips][numOfStatistics];

        for(int i = 0; i < numOfChips; i++){
            output[i][MIN_INDEX] = Double.MAX_VALUE;
            output[i][MAX_INDEX] = Double.MIN_VALUE;
        }





        int lastElementIndex = end-1>=0?end-1:maxDim-1;

        //System.out.println("\n\n\n");
        for(int i = 0; i < numOfChips; i++){
            double somma = 0;
            //System.out.println();
            for(int j = 0; j < numSamplesForStats; j++){

                int indexToWithdraw= (lastElementIndex-j)>=0?(lastElementIndex-j):(maxDim-Math.abs(lastElementIndex-j));

                //System.out.print(indexToWithdraw+",");

                somma += data[indexToWithdraw].getyValues()[i][indexToSave];
                if (output[i][MIN_INDEX] > data[indexToWithdraw].getyValues()[i][indexToSave]){
                    output[i][MIN_INDEX] = data[indexToWithdraw].getyValues()[i][indexToSave];
                }
                if (output[i][MAX_INDEX] < data[indexToWithdraw].getyValues()[i][indexToSave]){
                    output[i][MAX_INDEX] = data[indexToWithdraw].getyValues()[i][indexToSave];
                }
            }

            // Mean
            output[i][MEAN_INDEX] = somma/numSamplesForStats;

            double squareSum = 0;

            for(int j = 0; j < numSamplesForStats; j++){
                int indexToWithdraw= (lastElementIndex-j)>=0?(lastElementIndex-j):(maxDim-Math.abs(lastElementIndex-j));
                squareSum += Math.pow(data[indexToWithdraw].getyValues()[i][indexToSave] - output[i][MEAN_INDEX], 2);
            }
            //Standard Deviation
            output[i][NOISE_DEV_STD_INDEX] = Math.sqrt(squareSum/numSamplesForStats);

            //SNR
            if(output[i][NOISE_DEV_STD_INDEX]==0.0)
                output[i][SNR_INDEX] = 0.0;
            else
                output[i][SNR_INDEX] = output[i][MEAN_INDEX]/ output[i][NOISE_DEV_STD_INDEX];


            /*if(lastMaxSNR[i]==null){
                lastMaxSNR[i] = output[i][SNR_INDEX];

            }*/

            if (numSamplesForStats>1) {
                if(output[i][SNR_INDEX]>lastMaxSNR[i]) {
                    output[i][MAX_SNR_INDEX] = output[i][SNR_INDEX];
                    lastMaxSNR[i] = output[i][SNR_INDEX];
                }
                else{
                    output[i][MAX_SNR_INDEX] = lastMaxSNR[i];
                }

            }else{
                output[i][MAX_SNR_INDEX] = output[i][SNR_INDEX];
                lastMaxSNR[i] = output[i][SNR_INDEX];
            }

        }
        return output;
    }


    public int getStatsQueueDepth() {
        return statsQueueDepth;
    }

    public void setStatsQueueDepth(int statsQueueDepth) {
        this.statsQueueDepth = statsQueueDepth;
    }

    /*
    public synchronized double[][] getStatistics(int indexToSave){


        int numOfStatistics = 5;
        double[][] output = new double[numOfChips][numOfStatistics];

        for(int i = 0; i < numOfChips; i++){
            output[i][MIN_INDEX] = Double.MAX_VALUE;
            output[i][MAX_INDEX] = Double.MIN_VALUE;
        }


        double somma = 0;
        for(int i = 0; i < numOfChips; i++){
            for(int j = 0; j < totalElement; j++){
                somma += data[j].getyValues()[i][indexToSave];
                if (output[i][MIN_INDEX] > data[j].getyValues()[i][indexToSave]){
                    output[i][MIN_INDEX] = data[j].getyValues()[i][indexToSave];
                }
                if (output[i][MAX_INDEX] < data[j].getyValues()[i][indexToSave]){
                    output[i][MAX_INDEX] = data[j].getyValues()[i][indexToSave];
                }
            }

            // Mean
            output[i][MEAN_INDEX] = somma/totalElement;

            somma = 0;

            for(int j = 0; j < totalElement; j++){
                somma += Math.pow(data[j].getyValues()[i][indexToSave] - output[i][MEAN_INDEX], 2);
            }
            //Standard Deviation
            output[i][STD_INDEX] = Math.sqrt(somma/totalElement);

            //Variance
            output[i][VAR_INDEX] = Math.pow(output[i][STD_INDEX],2);

        }
        return output;
    }*/




    public synchronized double[] getValueRange(int measureIndex){
        double[] margins = new double[]{Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
        for(int i = 0; i < numOfChips; i++){
            if (chipMask[i]){
                if (valueRange[i][measureIndex][0] < margins[0]){
                    margins[0] = valueRange[i][measureIndex][0];
                }
                if (valueRange[i][measureIndex][1] > margins[1]){
                    margins[1] = valueRange[i][measureIndex][1];
                }
            }
        }
        return margins;
    }

    public synchronized SPChartElement get(int measureIndex){

        if (blockingWay && riemp == 0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else  if (riemp == 0){
            try {
                int TIMEOUT = 5;
                wait(TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (riemp == 0){
            return null;
        }

        SPChartElement spChartElement = data[start];
        riemp--;

        start = (start + 1) % maxDim;

        spChartElement.setxRange(getTimeRange());
        spChartElement.setyRange(getValueRange(measureIndex));

        return spChartElement;
    }



}
