package com.sensichips.sensiplus.datareader.queue;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.datareader.queue.SPChartQueue;

import java.util.ArrayList;

public class SPMainQueue {

    private int maxDim = 1000;
    private int start = 0;
    private int end = 0;
    private int riemp = 0;
    private SPChartElement data[];
    private ArrayList<SPChartQueue> subscribedQueue = new ArrayList<>();

    private double maxValues[][] = null;
    private double minValues[][] = null;

    private int chipNumber = 0;
    private int numOfMeasures = 0;


    //private int timeStamp = 0;

    private String LOG = "SPMainQueue";

    public SPMainQueue(int maxDim){
        this.maxDim = maxDim;
        data = new SPChartElement[maxDim];
        start = 0;
        end = 0;
    }


    public synchronized void put(SPChartElement r){

        if (maxValues == null){
            chipNumber = r.getyValues().length;
            numOfMeasures= r.getNumOfMeasures();
            maxValues = new double[r.getyValues().length][];
            minValues = new double[r.getyValues().length][];
            for(int i = 0; i < r.getyValues().length; i++){
                maxValues[i] = new double[r.getNumOfMeasures()];
                minValues[i] = new double[r.getNumOfMeasures()];
            }

            for(int i = 0; i < r.getyValues().length; i++){
                for(int j = 0; j < r.getNumOfMeasures(); j++){
                    maxValues[i][j] = Double.MIN_VALUE;
                    minValues[i][j] = Double.MAX_VALUE;
                }
            }


        }


        if (riemp < maxDim){
            riemp++;
        } else {
            start = (start + 1) % maxDim;
        }
        data[end] = r;
        end = (end + 1) % maxDim;

        for(int i = 0; i < subscribedQueue.size(); i++){
            subscribedQueue.get(i).put(r);
        }
        for(int i = 0; i < r.getyValues().length; i++){
            for(int j = 0; j < r.getNumOfMeasures(); j++){
                if (r.getyValues()[i][j] > maxValues[i][j]){
                    maxValues[i][j] = r.getyValues()[i][j];
                }
                if (r.getyValues()[i][j] < minValues[i][j]){
                    minValues[i][j] = r.getyValues()[i][j];
                }
            }
        }
    }



    public double getMin(int chipIndex, int measureIndex) throws SPException  {
        if (chipIndex >= chipNumber || measureIndex >= numOfMeasures)
            throw new SPException("Wrong chipIndex or measureIndex");

        return minValues[chipIndex][measureIndex];
    }

    public double getMax(int chipIndex, int measureIndex) throws SPException  {
        if (chipIndex >= chipNumber || measureIndex >= numOfMeasures)
            throw new SPException("Wrong chipIndex or measureIndex");

        return maxValues[chipIndex][measureIndex];
    }


    public void resetMainQueue(){
        //Reset main queue
        this.start = 0;
        this.end = 0;

        ArrayList<SPChartQueue> appoList = new ArrayList<>();

        while(this.subscribedQueue.size()>0){
            appoList.add(this.subscribedQueue.get(0));
            this.unSubscribe(this.subscribedQueue.get(0));
        }

        for(int i=0; i<appoList.size(); i++){
            this.subscribe(appoList.get(i));
        }


    }

    //public int getTimeStamp(){
    //    return timeStamp;
    //}

    public synchronized void subscribe(SPChartQueue q){
        q.clear();
        int requiredElem = Math.min(q.size(), riemp);
        int s = end;
        for(int i = 0; i < requiredElem; i++){
            s--;
            if (s < 0){
                s = maxDim - 1;
            }
        }

        // s punta al primo elemento della MainQueue da cui prelevare. Da questo indice si procede modulo maxDim

        for(int i = 0; i < requiredElem; i++){
            q.put(data[s]);
            s = (s + 1) % maxDim;
        }

        //System.out.println("s: " + s + ", end: " + end);
        subscribedQueue.add(q);
    }

    public synchronized SPChartElement getLast(){
        int lastPosition = (end-1)<0?maxDim-1:end-1;

        return data[lastPosition];

    }


    public synchronized void unSubscribe(SPChartQueue q){
        subscribedQueue.remove(q);
    }


}
