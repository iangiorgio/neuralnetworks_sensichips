package com.sensichips.sensiplus.level1;

import com.sensichips.sensiplus.SPException;

public class SPProtocolException extends SPException {
    public SPProtocolException(String message) {
        super(message);
    }
}
