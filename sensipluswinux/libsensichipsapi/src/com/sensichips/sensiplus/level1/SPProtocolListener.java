package com.sensichips.sensiplus.level1;

/**
 * The listener interface for receiving SPProtocol events. The class that is interested in processing an SPProtocol
 * event implements this interface, and the object created with that class is registered with the constructor.
 *
 *
 * Property: Sensichips s.r.l.
 *
 * @author: Mario Molinara, mario.molinara@sensichips.com, m.molinara@unicas.it
 */

public interface SPProtocolListener {
    /**
     * When the protocol complete the connection phase this method is invoked.
     *
     * @param protocol
     */
    void connectionUp(SPProtocol protocol);

    /**
     * String which can be used to show informagion messages (ex. "Device connected", etc.)
     * @param m
     */
    void message(String m);

    /**
     * Notify a connection down.
     */
    void connectionDown();

    /**
     * Used during debug session (for developers)
     * @param interruptValue
     */
    void interrupt(byte interruptValue);

    /**
     * This event is generated when some fatal error occur.
     *
     * @param message
     */
    void fatalError(String message);
}
