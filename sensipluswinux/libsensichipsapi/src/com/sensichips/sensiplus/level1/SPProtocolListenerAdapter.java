package com.sensichips.sensiplus.level1;

/**
 * Created by mario on 20/07/17.
 */
public class SPProtocolListenerAdapter implements SPProtocolListener {

    //protected SPDriver spDriver;

    public SPProtocolListenerAdapter(){
        //this.spDriver = spDriver;
    }

    /**
     * When the protocol complete the connection phase this method is invoked.
     *
     * @param protocol
     */
    @Override
    public void connectionUp(SPProtocol protocol) {

    }

    /**
     * String which can be used to show informagion messages (ex. "Device connected", etc.)
     *
     * @param m
     */
    @Override
    public void message(String m) {

    }

    /**
     * Notify a connection down.
     */
    @Override
    public void connectionDown() {

    }

    /**
     * Used during debug session (for developers)
     *
     * @param interruptValue
     */
    @Override
    public void interrupt(byte interruptValue) {

    }

    /**
     * This event is generated when some fatal error occur.
     *
     * @param message
     */
    @Override
    public void fatalError(String message) {

    }
}
