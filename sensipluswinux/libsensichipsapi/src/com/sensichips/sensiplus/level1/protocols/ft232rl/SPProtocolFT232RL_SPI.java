package com.sensichips.sensiplus.level1.protocols.ft232rl;

import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level1.decoder.SPDecoderSPI;

import java.util.ArrayList;

/**
 * Created by mario on 02/12/2015.
 */
public class SPProtocolFT232RL_SPI extends SPProtocolFT232RL_BUSPIRATE {


    public SPProtocolFT232RL_SPI(SPDriver readWrite, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException {
        super(readWrite, spCluster, addressingMode);




        protocolName = MCU + "_" + SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI];

    }


    @Override
    public void initSequence(){
        initSequence = new ArrayList<SCProtocol_InitOperation>();
        int sleep = 10;
        // Define the init sequence for SPI protocol
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("m\n", 12, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("5\n", 7, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("1\n", 4, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("\n", 4, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("\n", 4, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("\n", 4, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("\n", 4, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("2\n", 2, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("\n", 2, 0, 0,""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("w\n", 2, sleep, sleep*100, ""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("[\n", 2, sleep, sleep, ""));  ///CS ENABLED
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("A\n", 2, sleep, sleep, ""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("W\n", 2, sleep, sleep, ""));
        initSequence.add(new SPProtocol.SCProtocol_InitOperation("@\n", 6, sleep, sleep, ""));
    }


    @Override
    public long getADCDelay(){
        return 30;
    }

    @Override
    public String getProtocolName() {
        return SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI];
    }

    @Override
    public String setAddressingType(int mod) throws SPProtocolException {
        if (mod != SPProtocol.NO_ADDRESS || mod < 0 || mod >= 3){
            mod = mod < 0 ? 0 : mod >= 3 ? 2 : mod;
            throw new SPProtocolException("Communication mode not allowed in SPI: " + SPProtocol.ADDRESSING_TYPES[mod]);
        }
        addressingMode = mod;

        return getProtocolName() + " " + SPProtocol.ADDRESSING_TYPES[mod] + " OK!";
    }

    public boolean isResettable(){
        return true;
    }

    @Override
    protected String send(String comando) throws SPProtocolException {
        String output = "";
        try {

            String recl[] = null;
            boolean scrittura = !comando.contains("r");
            //boolean flagFirstSend = true;

            // So to ensure that the command contains "[..]\n"
            comando = "[" + comando.replace("[", "").replace("]", "").replace("\n","") + "]\n";

            int numToken = tokenToWaitInSPI(comando);

            //System.out.println("SPISPISPI\t " + comando);

            recl = sendL(comando, numToken);
            for(int i = 0; i < recl.length; i++){
                if (scrittura && recl[i].contains("WRITE")) {
                    output += recl[i].replace("WRITE:", "").trim() + " ";
                } else if (!scrittura && recl[i].contains("READ")){
                    output += recl[i].replace("READ:", "").trim() + " ";
                }
            }

            output = "[" + output.trim() + "]";
            //System.out.println("SPISPISPI\t " + output);


        } catch (Exception e) {
            throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + "Generic exception in SPProtocol.send. " + e.getMessage());
        }
        // This is the echo from Buspirate
        return output;
    }


    private int tokenToWaitInSPI(String comando){
        // First row: spi>[...]
        // Second row: /CS ENABLED
        // ...
        // Last row: /CS DISABLED
        int rowToIgnore = 3;
        return comando.replace("[", "").replace("]", "").trim().replace("0x", "").split(" ").length + rowToIgnore;
    }

    private String setAddressingType(int modoAttuale, int mod) throws SPProtocolException {
        if (mod != SPProtocol.SPI ){
            mod = mod < 0 ? 0 : mod >= 3 ? 2 : mod;
            throw new SPProtocolException("Communication mode not allowed in SPI: " + SPProtocol.ADDRESSING_TYPES[mod]);
        }

        if (modoAttuale == mod){
            return "";
        }

        String address = "";

        return "";

    }

    @Override
    public String resetChips(SPDecoderGenericInstruction instruction, SPChip spchip) throws SPProtocolException, SPDriverException {

        send(instruction, spchip);
        return setAddressingType(SPProtocol.NO_ADDRESS, addressingMode);

    }

    public SPDecoder getSPDecoder() throws SPProtocolException {
        return new SPDecoderSPI();
    }


}


