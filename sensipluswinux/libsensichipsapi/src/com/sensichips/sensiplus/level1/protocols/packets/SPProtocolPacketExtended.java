package com.sensichips.sensiplus.level1.protocols.packets;

import com.sensichips.sensiplus.util.log.SPLoggerInterface;

public class SPProtocolPacketExtended implements SPProtocolPacket {

    private byte byteReceived[] = null;
    private int dataLengthExpected;


    public void setByteReceived(byte[] byteReceived) {
        this.byteReceived = byteReceived;
    }

    public void setDataLengthExpected(int dataLengthExpected) {
        this.dataLengthExpected = dataLengthExpected;
    }

    @Override
    public byte[] getBytes() {
        return byteReceived;
    }

    @Override
    public int getDataLengthExpected() {
        return dataLengthExpected;
    }
}
