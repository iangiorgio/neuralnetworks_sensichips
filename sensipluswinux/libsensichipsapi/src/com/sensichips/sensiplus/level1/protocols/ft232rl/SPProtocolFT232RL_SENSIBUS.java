package com.sensichips.sensiplus.level1.protocols.ft232rl;


import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level1.decoder.SPDecoderSensibus;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.util.Arrays;

/**
 * Created by mario on 04/12/2015.
 */
public class SPProtocolFT232RL_SENSIBUS extends SPProtocolFT232RL_BUSPIRATE implements Runnable {

    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;

    public static int COUNTER[] = new int[READBUF_SIZE];
    public static int TOTAL_BYTE_SENT = 0;
    public static long SENDL_ELAPSED = 0;
    public static long WRITE_ELAPSED = 0;

    protected SCDriver_ReceiverQueue<Byte> receiverBufferByte;

    /***** 57600 bps di uart ******/

    /*
    private byte zeroWaveForm = (byte)0xE0; //PAY ATTENTION: UART SEND FROM LEAST SIGNIFICANT BIT
    private byte oneWaveForm = (byte) 0xFC;
    private byte startCommunication = (byte) 00; // 57600 bps
    private byte startCommunicationResp[] = new byte[]{(byte) 0x00, (byte) 0xFC};
    private byte interruptByte = (byte) 0x08;
    private byte speed = (byte) 0x68;
    */
    /***** 57600 bps di uart ******/

    /*****
     * 38400 bps di uart
     ******/

    //private byte zeroWaveForm = (byte) 0xF8; //PAY ATTENTION: UART SEND FROM LEAST SIGNIFICANT BIT
    //private byte oneWaveForm = (byte) 0xFE;

    private byte waveForm00 = (byte) 0x08; //PAY ATTENTION: UART SEND FROM LEAST SIGNIFICANT BIT
    private byte waveForm01 = (byte) 0xC8;
    private byte waveForm10 = (byte) 0x0E; //PAY ATTENTION: UART SEND FROM LEAST SIGNIFICANT BIT
    private byte waveForm11 = (byte) 0xCE;


    // Start communication byte
    private byte startCommunication = (byte) 0xE0;        // 38400 bps
    //private byte startCommunicationResp = (byte) 0x20;
    private byte startCommunicationResp[] = new byte[]{(byte) 0x20};
    private byte startCommunicationMask[] = new byte[]{(byte) 0x80}; // don't care bits
    private byte interruptByte = (byte) 0x77;
    private byte speed = (byte) 0x67;

    /*****
     * 38400 bps di uart
     ******/

    // Sensibus ID
    //private static final String ID_BROADCAST = "00 00 00 00 00 00";
    //private static final String ID_UNICAST = "E0 00 00 00 00 F5";
    //private String currentID = ID_UNICAST;



    public SPProtocolFT232RL_SENSIBUS(SPDriver readWrite, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException {
        super(readWrite, spCluster, addressingMode);
        protocolName = MCU + "_" + SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS];

        addressingMode = SPProtocol.FULL_ADDRESS;
    }

    public boolean isResettable(){
        return true;
    }

    /**
     * This version is specialized to set the buspirate in BB mode
     *
     * @throws SPProtocolException
     */
    @Override
    public void init(int addressingMode) throws SPProtocolException, SPDriverException {

        try{
            if (GENERAL_ERROR) {
                throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE);
            }
            stopReceiverThread = false;
            stoppedReceiverThread = true;
            receiverBufferByte = new SCDriver_ReceiverQueue<Byte>(Byte.class, queueSize);

            // Start communication byte
            startCommunication = (byte) 0xE0;        // 38400 bps
            //private byte startCommunicationResp = (byte) 0x20;
            startCommunicationResp = new byte[]{(byte) 0x20};
            startCommunicationMask = new byte[]{(byte) 0x80}; // don't care bits
            interruptByte = (byte) 0x77;
            speed = (byte) 0x67;


            waveForm00 = (byte) 0x08; //PAY ATTENTION: UART SEND FROM LEAST SIGNIFICANT BIT
            waveForm01 = (byte) 0xC8;
            waveForm10 = (byte) 0x0E; //PAY ATTENTION: UART SEND FROM LEAST SIGNIFICANT BIT
            waveForm11 = (byte) 0xCE;
            COUNTER = new int[READBUF_SIZE];


            spDriver.openConnection();
            startReceiverThread();

            boolean alreadySENSIBUS = false;



            try{
                startCommunication();
                alreadySENSIBUS = true;
            } catch (SPProtocolException e){
                GENERAL_ERROR = false;
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Try to go in SENSIBUS!", DEBUG_LEVEL);
            }


            if (!alreadySENSIBUS){

                // init sequence for SPI MODE - last sequence: [ A W
                //String mes = null;

                //stato = SCDriverFT232RL_Proxy.SEQ_SENDING_UARTCONF;
                ////SPLogger.d(TAG_SERVICE,"Init Sensibus started ...");

                ////SPLogger.d(TAG_SERVICE,"receiverByte started ...");

                //int bb_zeros = 20;

                // http://dangerousprototypes.com/blog/2009/10/09/bus-pirate-raw-bitbang-mode/
                byte[] seqBitBang = new byte[20];
                byte[] seqBitBangResp; // = new byte[bb_firstResp];

                //int bb_firstResp = 5;
                byte[] seqBitBangRespExpected = new byte[]{(byte) 0x42, (byte) 0x42, (byte) 0x49, (byte) 0x4F, (byte) 0x31};    // BBIO1

                // Send 20 byte with zero value
                // Receive BBIO
                try{
                    seqBitBangResp = sendL(seqBitBang, seqBitBangRespExpected.length);
                    for (int i = 0; i < seqBitBangRespExpected.length; i++) {
                        if (seqBitBangResp[i] != seqBitBangRespExpected[i]) {
                            //SPLogger.d(TAG_SERVICE_GENERAL_ERROR, "Wrong BBIO sequence response from bridge.");
                            GENERAL_ERROR = true;
                            throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + "Wrong BBIO sequence response from bridge.");
                        }
                    }
                } catch (SPProtocolException e){
                    throw new SPProtocolException("Problem with FT232RL or probably is in SENSIBUS mode but chips don't respond to startCommunication.");
                }


                SPLogger.getLogInterface().d(LOG_MESSAGE, "Bit bang ok", DEBUG_LEVEL);

                byte[] bb_uart_bridge = new byte[6];
                byte[] bb_uart_bridge_resp; // = new byte[10];

                // UART  - 00000011 – Enter binary UART mode, responds “ART1”
                bb_uart_bridge[0] = (byte) 0x03;

                // 0110xxxx – Set UART speed
                // Set the UART at a preconfigured speed value: 0000=300, 0001=1200, 0010=2400,0011=4800,0100=9600,0101=19200,0110=31250 (MIDI), 0111=38400,1000=57600,1010=115200
                bb_uart_bridge[1] = speed; // 38400 bps
                // bb_uart_bridge[1] = (byte) 0x68; // 57600 bps

                // 100wxxyz – Config, w=output type, xx=databits and parity, y=stop bits, z=RX polarity
                bb_uart_bridge[2] = (byte) 0x80; // Config UART

                // 0100wxyz – Configure peripherals w=power, x=pullups, y=AUX, z=CS
                //bb_uart_bridge[3] = (byte) 0x44; // Config UART - RUN3
                bb_uart_bridge[3] = (byte) 0x45;

                // bb_uart_bridge[4] = (byte) 0x4C; // Power up  - RUN3
                bb_uart_bridge[4] = (byte) 0x4D; // Power up

                // 00001111 – UART bridge mode (reset to exit)
                bb_uart_bridge[5] = (byte) 0x0F; // Transparent Bridge

                int bb_uart_bridge_resp_len = 9;
                try {
                    bb_uart_bridge_resp = sendL(bb_uart_bridge, bb_uart_bridge_resp_len); // Response 41 52 54 31 01 01 01 01 01 00 => ART1 0x01 0x01 0x01 0x01 0x01 0x00
                } catch (SPProtocolException e){
                    throw new SPProtocolException("Problem with FT232RL: transparent mode unavailable.");
                }
                SPLogger.getLogInterface().d(LOG_MESSAGE, "SENSIBUS ok", DEBUG_LEVEL);

            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // Chip reset
            //String instruction = "0x00 0x06";
            //send(instruction);

            //setAddressingType(SPProtocol.FULL_ADDRESS, addressingType);


                softTrim();

                setAddressingType(addressingMode);

            this.addressingMode = addressingMode;

            //currentID = ID_BROADCAST;
            //spProtocolListener.connectionUp(this);
            isInitialized = true;

        } catch (SPProtocolException sp){
            stopReceiverThread();
            try{
                spDriver.closeConnection();
            } catch (SPDriverException e){
                e.printStackTrace();
            }
            throw sp;
        }
        return;

    }


    private void softTrim() throws SPProtocolException, SPDriverException {
        String address = "00 00 00 00 00 00";
        String sc = startCommunication();

        if (sc.length() > 0){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start communication ok: " + sc, DEBUG_LEVEL);
        }
        if (address.length() > 0){
            sendID(address);
        }
        //User input:	WRITE USER_EFUSE S 0X06
        //sent:			0xF8 0x06

        String register = "0xF8";
        String data = "0x06";
        send(register + " " + data);

        String msg = "WRITE USER_EFUSE S " + data + " -- (soft trim for sensibus)";
        if ((SPLogger.DEBUG_TH | SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE) != 0){
            SPLogger.getLogInterface().d(LOG_MESSAGE, msg, SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE);
        } else {
            SPLogger.getLogInterface().d(LOG_MESSAGE, msg, DEBUG_LEVEL);
        }


    }

    private String setAddressingType(int modoAttuale, int newAddressType) throws SPProtocolException, SPDriverException{
        if (newAddressType < 0 || newAddressType >= 3){
            newAddressType = newAddressType < 0 ? 0 : newAddressType >= 3 ? 2 : newAddressType;
            throw new SPProtocolException("Communication mode not allowed in SENSIBUS: " + SPProtocol.ADDRESSING_TYPES[newAddressType]);
        }

        if (modoAttuale == newAddressType){
            return "";
        }

        String address = "";

        if (modoAttuale == SPProtocol.FULL_ADDRESS){
            address = "00 00 00 00 00 00";
        } else if (modoAttuale == SPProtocol.SHORT_ADDRESS){
            address = "00";
        }

        String sensibus_set = "0xD8";
        String data = "";
        //String mnemonic = "WRITE SENSIBUS_SET S ";
        if (newAddressType == SPProtocol.SHORT_ADDRESS){
            data = "0XE";
            //mnemonic += "0XEF";
        } else if (newAddressType == SPProtocol.FULL_ADDRESS){
            data = "0XC";
            //mnemonic += "0XCF";
        } else if (newAddressType == SPProtocol.NO_ADDRESS){
            data = "0XD";
            //mnemonic += "0XDF";
        }
        data += "3"; // "F" is maximum of... "C" is OK. TODO: verify the correct value

        String instruction = sensibus_set + " " + data;

        try {
            String sc = startCommunication();

            if (sc.length() > 0){
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Start communication ok: " + sc, DEBUG_LEVEL);
            }
            if (address.length() > 0){
                sendID(address);
            }
            send(instruction);

            String msg = "WRITE SENSIBUS_SET S " + data + " -- set sensibus addressing mode";
            if ((SPLogger.DEBUG_TH | SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE) != 0){
                SPLogger.getLogInterface().d(LOG_MESSAGE, msg, SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE);
            } else {
                SPLogger.getLogInterface().d(LOG_MESSAGE, msg, DEBUG_LEVEL);
            }

        } catch (SPProtocolException e) {
            throw new SPProtocolException(e.getMessage());
        }
        addressingMode = newAddressType;

        return getProtocolName() + " " + SPProtocol.ADDRESSING_TYPES[newAddressType] + " OK!";

    }

    @Override
    public String resetChips(SPDecoderGenericInstruction instruction, SPChip spchip) throws SPProtocolException, SPDriverException {
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Reset chips", DEBUG_LEVEL);
        send(instruction, spchip);
        // After reset, chip return in FULL_ADDRESS
        return setAddressingType(SPProtocol.FULL_ADDRESS, addressingMode);

    }

    @Override
    public String setAddressingType(int newAddressType) throws SPProtocolException, SPDriverException {
        return setAddressingType(addressingMode, newAddressType);
    }





    @Override
    protected String send(String comando) throws SPProtocolException, SPProtocolException {

        //System.out.println(comando);


        String output = "";
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Command to sent: " + comando, DEBUG_LEVEL);

        try {
            byte numberToDecode = 0;
            int numOfBit = 4;
            boolean flagFirstSend = true;
            boolean readOp = comando.contains("r");

            comando = comando.replace("0X", "").replace("0x", "");
            comando = comando.replace("[", "").replace("]", "");

            String[] st = comando.split(" "); // split in words (default delimitator= whitespace)
            int batchSizeMax = 3;
            //if (st.length % batchSize != 0){
            //    throw new SPProtocolException("Wrong number of token: should be a multiple of " + batchSize);
            //}
            byte[] byteReceived;

            byte MSb_mask = (byte)0XC0;

            int j = 0;
            while(j < st.length){
            //while (st.hasMoreTokens()) {
                int remainingToken = st.length - j;

                int batchSizeIter = remainingToken < batchSizeMax ? remainingToken : batchSizeMax;

                byte[] byteToSend = new byte[numOfBit * batchSizeIter ];
                int k = 0;
                while(k < batchSizeIter && j < st.length){
                    String token = st[j];
                    if (token.equalsIgnoreCase("r")) {
                        token = "FF"; // Send 0xFF in order to make a read
                    }
                    numberToDecode = (byte) Integer.parseInt(token, 16);

                    // Costruisci un array di byte con FC per 1 e C0 per 0
                    String echo = "";
                    for (int i = k * numOfBit; i < (1 + k) * numOfBit; i++) {
                        if ((numberToDecode & MSb_mask) == (byte)0X00){
                            // 00
                            byteToSend[i] = waveForm00;
                        } else if ((numberToDecode & MSb_mask) == (byte)0X40){
                            // 01
                            byteToSend[i] = waveForm01;
                        } else if ((numberToDecode & MSb_mask) == (byte)0X80){
                            // 10
                            byteToSend[i] = waveForm10;
                        } else if ((numberToDecode & MSb_mask) == (byte)0XC0){
                            // 11
                            byteToSend[i] = waveForm11;
                        } else {
                            throw new SPProtocolException("Wrong byte configuration!!!");
                        }
                        //byteToSend[i + k * numOfBit] = (numberToDecode & 0xC0) != 0 ? (byte) oneWaveForm : (byte) zeroWaveForm;
                        numberToDecode = (byte) (numberToDecode << 2);
                        echo += " " + byteToString(byteToSend[i]);
                    }
                    j++;
                    k++;
                }
                //SPLogger.d("DEBUG_LOW", echo);
                TOTAL_BYTE_SENT += byteToSend.length;

                byteReceived = sendL(byteToSend, numOfBit * k);

                if (flagFirstSend){
                    if (readOp){
                        byte[] appo = Arrays.copyOfRange(byteReceived, numOfBit, byteReceived.length);
                        byteReceived = appo;
                        k--;
                    }
                    flagFirstSend = false;
                }


                byte[] b = new byte[k];

                for(int m = 0; m < k; m++){
                    for (int i = m * numOfBit; i < (1 + m) * numOfBit; i++) {
                        b[m] = (byte) (b[m] << 2);
                        if (byteReceived[i] == waveForm00) {
                            b[m] = (byte) (b[m] | 0x00);
                        } else if (byteReceived[i] == waveForm01){
                            b[m] = (byte) (b[m] | 0x01);
                        } else if (byteReceived[i] == waveForm10){
                            b[m] = (byte) (b[m] | 0x02);
                        } else if (byteReceived[i] == waveForm11){
                            b[m] = (byte) (b[m] | 0x03);
                        }
                    }
                }
                for(int m = 0; m < k; m++){
                    output += byteToString(b[m]);
                }

            }
            output = "[" + output.trim() + "]";
        } catch (Exception e) {
            throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + "Generic exception in SPProtocol.send. " + e.getMessage());
        }
        //SPLogger.getLogInterface().d(LOG_MESSAGE, "Echo received: " + output, DEBUG_LEVEL);
        // This is the echo from Buspirate
        return output;
    }



    @Override
    public String getProtocolName() {
        return SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS];
    }


    @Override
    protected String startCommunication() throws SPProtocolException, SPDriverException {


        // Start com per 57600 bps
        // byte[] start = new byte[]{(byte)0x00};
        // Start com per 38400 bps
        byte[] start = new byte[]{startCommunication};
        byte resL[] = null;
        resL = sendL(start, startCommunicationResp.length);

        String out = "";
        //SPLogger.d("DEBUG_LOW", "start: " + byteToString(start[0]));
        //SPLogger.d("DEBUG_LOW", "response: " + byteToString(resL[0]));
        for(int i = 0; i < startCommunicationResp.length; i++){
            out += resL[i];
            if ((resL[i] | startCommunicationMask[i]) != (startCommunicationResp[i] | startCommunicationMask[i])){
                String resp = "response: " + "startCommunication failed. Expected: " + byteToString(startCommunicationResp[i]) + " or " +
                        byteToString((byte)(startCommunicationResp[i] | startCommunicationMask[i])) + ", received: " + byteToString(resL[i]);
                GENERAL_ERROR = true;
                throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + resp);
            }
        }
        /*
        if (resL[0] != startCommunicationResp) {
            String resp = "response: " + "startCommunication failed. Expected: " + byteToString(startCommunicationResp) + ", received: " + byteToString(resL[0]);
            GENERAL_ERROR = true;
            throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + resp);
        }*/
        return String.format("%02X ", startCommunication);

    }

    @Override
    protected void sendID(String id) throws SPProtocolException, SPProtocolException {
        send(id);
        SPLogger.getLogInterface().d(LOG_MESSAGE, "ID sent: " + id, DEBUG_LEVEL);
    }



    public String reset() throws SPProtocolException, SPDriverException  {
        try{
            clearCache();
            setAddressingType(SPProtocol.FULL_ADDRESS);
            cleanReceiver();
            stopReceiverThread();
        } catch (Exception e){
            e.printStackTrace();
        }

        return "SPProtocolFT232RL_SENSIBUS stopped!";
    }


    protected final byte[] sendL(byte b[], int n) throws SPProtocolException, SPDriverException {
        return sendL(b, n, timeOutFromDriver);
    }

    protected final byte[] sendL(byte b[], int n, long timeout) throws SPProtocolException, SPDriverException{

        long start = System.currentTimeMillis();

        byte output[] = new byte[n];
        receiverBufferByte.reset();

        long start1 = System.currentTimeMillis();
        spDriver.write(b);
        WRITE_ELAPSED += (System.currentTimeMillis() - start1);

        for (int i = 0; i < n; i++) {
            try {
                if (receiverBufferByte.getAndResetInterrupt()) {
                    //SPLogger.d("EXCEPTION", "interrupt received in byte[] sendL(...) ");
                    throw new SPProtocolException("interrupt received in byte[] sendL(...) ");
                }
                output[i] = receiverBufferByte.get(timeout);
            } catch (SPProtocolException e) {
                //SPLogger.d(SCDriverFT232RLService.TAG_SERVICE_GENERAL_ERROR, "timeout in byte[] sendL(...)");
                //annullaGeneralError = stato;
                GENERAL_ERROR = true;
                throw e;
            } catch (Exception e) {
                //SPLogger.d(SCDriverFT232RLService.TAG_SERVICE_GENERAL_ERROR, e.getMessage());
                //annullaGeneralError = stato;
                GENERAL_ERROR = true;
                throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + e.getMessage());
            }
        }
        receiverBufferByte.reset();
        SENDL_ELAPSED += (System.currentTimeMillis() - start);


        return output;
    }

    public void run() {
        byte bufferRec[] = new byte[READBUF_SIZE];
        int len = 0;
        String mess;
        //int counter = 0;
        stoppedReceiverThread = false;
        while (!stopReceiverThread) {
            //if (counter % 100 == 0){
            //    System.out.println("Thread counter: " + counter);
            //}
            //System.out.println("Thread counter: " + counter);
            //counter++;
            mess = "";
            try {
                len = spDriver.read(bufferRec);
                COUNTER[len]++;
            } catch (SPDriverException sce) {
                SPLogger.getLogInterface().d(SPProtocol.LOG_MESSAGE, "Exception in run(): " + sce.getMessage(), DEBUG_LEVEL);
            }
            if (len >= 1) {
                try {
                    if (bufferRec[0] == interruptByte) {
                        mess = "Interrupt: ";
                        if (spProtocolListener != null) {
                            spProtocolListener.interrupt(bufferRec[0]);
                        }

                        receiverBufferByte.setInterrupt(true);
                        //SPLogger.d("EXCEPTION", "put interrupt in bufferByte.");
                    } else {
                        for(int i = 0; i < len; i++){
                            receiverBufferByte.put(bufferRec[i]);
                            mess += " " + byteToString(bufferRec[i]);
                        }
                    }

                    //SPLogger.d("BUFFER_BYTE", mess);
                } catch (SPProtocolException ie) {
                    //SPLogger.d("EXCEPTION", "timeout in run() " + ie.getMessage());
                }
            } else {
                SPDelay.delay(SPDelay.DELAY_LOW);
            }
        }
        stoppedReceiverThread = true;

    }

    private String byteToString(byte b) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("0x%02X ", b));
        return sb.toString();
    }


    public SPDecoder getSPDecoder() throws SPProtocolException {
        return new SPDecoderSensibus();
    }

}
