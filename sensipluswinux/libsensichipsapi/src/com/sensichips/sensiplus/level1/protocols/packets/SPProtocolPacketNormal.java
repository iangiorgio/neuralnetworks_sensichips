package com.sensichips.sensiplus.level1.protocols.packets;

import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import org.apache.commons.codec.binary.Hex;

public class SPProtocolPacketNormal implements SPProtocolPacket {

    private byte bufferToSend[] = null;
    private int dataLengthExpected;

    private static final int NUM_BYTE_REGISTER = 1;
    private static final int NUM_BYTE_PROTOCOL = 1;
    public static final String LOG_MESSAGE = "SPProtocolPacketNormal";
    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;
    private static byte DataLengthMask      = (byte) 0x1F;     // 00011111

    public SPProtocolPacketNormal(byte protocol, byte register, byte[] addresses, byte[] data){


        int dataLength = 0;
        if (data != null){
            dataLength = data.length;
        }

        bufferToSend = new byte[NUM_BYTE_PROTOCOL + NUM_BYTE_REGISTER + addresses.length + dataLength];

        // FIRST BYTE: PROTOCOL
        bufferToSend[0] = protocol;

        // BYTE FOR ADDRESS: 0, 1, 6
        for(int i = NUM_BYTE_PROTOCOL; i < addresses.length + NUM_BYTE_PROTOCOL; i++){
            bufferToSend[i] = addresses[i - NUM_BYTE_PROTOCOL];
        }
        // BYTE FOR SENSIPLUS REGISTER: 1
        bufferToSend[addresses.length + NUM_BYTE_PROTOCOL] = register;

        int startIndex = (addresses.length + (NUM_BYTE_PROTOCOL + NUM_BYTE_REGISTER));
        int endIndex = (addresses.length + (NUM_BYTE_PROTOCOL + NUM_BYTE_REGISTER) + dataLength);

        // DATA TO SEND (0...31: we have the 5 lsb bits in protocol)


        for(int i = startIndex;  i < endIndex; i++){
            bufferToSend[i] = data[i - (addresses.length + (NUM_BYTE_PROTOCOL + NUM_BYTE_REGISTER))];
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Sent sequence: " + Hex.encodeHexString(bufferToSend), DEBUG_LEVEL);

        // For read operation, we expect more data with respect to data sent
        dataLengthExpected = protocol & DataLengthMask;     // Need for read operation in particular
    }



    @Override
    public byte[] getBytes() {
        return bufferToSend;
    }

    @Override
    public int getDataLengthExpected() {
        return dataLengthExpected;
    }
}
