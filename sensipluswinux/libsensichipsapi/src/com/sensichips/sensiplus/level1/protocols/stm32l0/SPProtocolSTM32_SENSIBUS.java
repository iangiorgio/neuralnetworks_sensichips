package com.sensichips.sensiplus.level1.protocols.stm32l0;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level1.decoder.SPDecoderSensibus;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacket;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import org.apache.commons.codec.binary.Hex;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SPProtocolSTM32_SENSIBUS extends SPProtocol implements Runnable {
    // *************************** MASK ************************************** //
    private static byte NoAddressMask       = (byte)0x00;    // 00000000
    private static byte ShortAddressMask    = (byte)0x20;    // 00100000
    private static byte LongAddressMask     = (byte)0x40;    // 01000000
    private static byte NoChangeMask        = (byte)0x60;    // 01100000

    // Not used. Need for new field in order to obtain feedback from MCU
    private static byte PseudoInstructionMask        = (byte)0x80;    // 10000000
    //private static byte FEEDBACK_ON_ERROR   = (byte)0x01;

    private boolean VERBOSE = false;
    // Not used. Need for new field in order to obtain continuos feedback from MCU

    private static byte DataLengthMask      = (byte) 0x1F;     // 00011111
    private static byte ProtocolMask        = (byte) 0xE0;      // 11100000


    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;
    public static int READBUF_SIZE = 4096;
    public static int COUNTER[] = new int[READBUF_SIZE];
    //public static int TOTAL_BYTE_SENT = 0;
    public static long SENDL_ELAPSED = 0;
    public static long WRITE_ELAPSED = 0;


    // EXTENDED MESSAGES
    // REQUESTS
    public static byte testReq =        (byte)0x80;           // 1000 0000 => send start com and receive an alive response
    public static byte networkInfoReq = (byte)0x81;           // 1000 0001
    public static byte resetReq =       (byte)0x82;           // 1000 0010
    public static byte chipListReq =    (byte)0x83;           // 1000 0011

    // RESPONSES
    public static byte testRespOk =     (byte)0x54;            // 1101 0100


    protected boolean stopReceiverThread = false;
    protected boolean stoppedReceiverThread = true;

    protected SPProtocol.SCDriver_ReceiverQueue<Byte> receiverBufferByte;

    public static final String MCU = "STM32";
    @Override
    public void setTk32xToDelete(byte b) throws SPProtocolException, SPDriverException {
        byte[] speed32x = new byte[2];
        speed32x[0] = (byte) 0xC0;
        speed32x[1] = b;
        sendL(speed32x, 2);
    }

    @Override
    public String toggleAUX() throws SPProtocolException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow toggleAUX");
    }

    public SPProtocolSTM32_SENSIBUS(SPDriver readWrite, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException {
        super(readWrite, spCluster, addressingMode);
        protocolName = MCU + "_" + SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS];
        //init();
    }

    @Override
    public boolean isInitialized() {
        return isInitialized;
    }

    @Override
    public void connectionUp() {
        spProtocolListener.connectionUp(this);
    }

    public String setSpeed(String speed, SPCluster spCluster) throws SPProtocolException, SPDriverException {
        int i = 0;
        boolean found = false;
        String out = "";
        while(i < SPEEDS.size() && !found){
            found = SPEEDS.get(i).equalsIgnoreCase(speed);
            if(!found){
                i++;
            }
        }
        if (found){
            out = "WRITE SENSIBUS_SET+1 S 0x" + String.format("%02x", SPEED_COMMANDS[i]).toUpperCase() + "  -- set sensibus speed";;
            //out = setSpeed(SPEED_COMMANDS[i]);
            sendInstruction(out, spCluster);
        } else {
            String speedAvailable = "";
            for(String item : SPEEDS){
                speedAvailable += item + " ";
            }
            throw new SPProtocolException("Requested speed: " + speed + " is not available.\n" + speedAvailable);
        }
        return out;
    }



    private byte testStartCommunicationMask;
    private byte testFirmwareCorrectValue;
    private byte testFirmwareVersionMask;
    private byte testStartCommunication;
    private byte testRightSpeedCommunicationMask;

    public String testMCU() throws SPProtocolException, SPDriverException {

        testStartCommunicationMask = (byte) 0x80;
        testFirmwareCorrectValue = (byte) 0x02;
        testFirmwareVersionMask = (byte) 0xFF;
        testStartCommunication = (byte) 0x81;
        testRightSpeedCommunicationMask = (byte) 0x54;

        byte[] testStartComm = new byte[]{testStartCommunication};
        byte[] response;

        try {
            response = sendL(testStartComm, 2);
        } catch (SPDriverException e) {
            e.printStackTrace();
            throw new SPProtocolException("Exception in testMCU(): " + e.getMessage());
        }

        if ((response[0] & testStartCommunicationMask) != testStartCommunicationMask){
            throw new SPProtocolException("No start communication from the cable. No chips presents or some other problems?");
        }

        if ((response[0] & testRightSpeedCommunicationMask) != testRightSpeedCommunicationMask){
            throw new SPProtocolException("Error occurred in communication. Correct speed has been specified?");

        }

        if ((response[1] & testFirmwareVersionMask) != testFirmwareCorrectValue){
            throw new SPProtocolException("Unexpected firmware version. Please update the MCU. Version obtained: " + response[1] + ", version expected: " + testFirmwareCorrectValue);

        }

        return "Start communication ok";
    }

    @Override
    public float elapsedStartComResponse() throws SPProtocolException, SPDriverException{
        byte[] array = new byte[4];

        byte[] toSend = new byte[]{(byte)0xA0};
        byte[] toReceive;

        //System.out.println("Repetition: " + (j + 1) + " ... ");
        int repetition = 1000;
        //float cycle = (float) 485;
        float tickDuration = 0;
        //float tickCounter[] = new float[repetition];
        float thickDuration[] = new float[repetition];
        int mask = 255;
        for(int i = 0; i < repetition; i++){

            toReceive = sendL(toSend, 5);

            array = new byte[]{toReceive[1], toReceive[2], toReceive[3], toReceive[4]};
            float elapsed = ByteBuffer.wrap(array).order(ByteOrder.LITTLE_ENDIAN).getFloat();

            thickDuration[i] = elapsed;
            //tickCounter[i] = mask & toReceive[2];

        }

        float totalThickDuration = 0;
        //float totalThickCounter = 0;
        for(int i = 0; i < repetition; i++){
            totalThickDuration += thickDuration[i];
            //totalThickCounter += tickCounter[i];
        }
        return totalThickDuration/repetition;

    }


    @Override
    public long getADCDelay(){
        //ccounter++;
        //System.out.println("counter of ADCDelay: " + counter);
        return 500;
        //return 500;
    }

    @Override
    public void updateSPDriverListener() throws SPProtocolException {
        spDriver.addSPDriverListener(this);
    }

    @Override
    public void closeAll() throws SPProtocolException, SPDriverException {
        reset();
        stopReceiverThread();

    }

    @Override
    public String reset() throws SPProtocolException {

        try{
            clearCache();
            setSpeed("1x", null);
            setAddressingType(SPProtocol.FULL_ADDRESS);

        } catch (Exception e){
            e.printStackTrace();
        }

        return "Reset Ok: " + getProtocolName();
    }


    /**
     * This version is specialized to set the buspirate in BB mode
     *
     * @throws SPProtocolException
     */
    @Override
    public void init(int addressingMode) throws SPDriverException, SPProtocolException {
        stopReceiverThread = false;
        stoppedReceiverThread = true;
        receiverBufferByte = new SPProtocol.SCDriver_ReceiverQueue<Byte>(Byte.class, queueSize);

        startReceiverThread();

        //testMCU();

        //softTrim();

        setAddressingType(addressingMode);

        this.addressingMode = addressingMode;

        return;
    }


    /**********************************************************************************************
     * THREAD GESTIONE LETTURA DA ESP8266
     *********************************************************************************************/




    protected void startReceiverThread() {
        if (stoppedReceiverThread) {
            // Only one thread can be active
            stopReceiverThread = false;
            stoppedReceiverThread = false;
            new Thread(this).start();
        }
    }

    public void stopReceiverThread() {
        stopReceiverThread = true;
        while (!stoppedReceiverThread) {
            SPDelay.delay(SPDelay.DELAY_LOW);
        }
    }

    private void softTrim() throws SPProtocolException, SPDriverException {

        byte addresses[]    = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00};  // Full address broadcast
        byte protocol       = (byte)0x41;             //0100000 FullAddress, 00000001 DataLength
        byte register       = (byte) 0xF8;            // USER_EFUSE
        byte data[]         = new byte[]{Byte.parseByte(spCluster.getFamilyOfChips().getOSC_TRIM().toUpperCase().replace("0X", ""), 16)};   // S 0x06

        byte response[] = sendData(protocol, register, addresses, data);

        String msg = "WRITE USER_EFUSE S " + spCluster.getFamilyOfChips().getOSC_TRIM() +" -- (soft trim for sensibus)";
        if ((SPLogger.DEBUG_TH | SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE) != 0){
            SPLogger.getLogInterface().d(LOG_MESSAGE, msg, SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE);
        } else {
            SPLogger.getLogInterface().d(LOG_MESSAGE, msg, DEBUG_LEVEL);
        }

    }


    @Override
    public String resetChips(SPDecoderGenericInstruction instruction, SPChip spchip) throws SPProtocolException, SPDriverException {

        send(instruction, spchip);
        return setAddressingType(SPProtocol.FULL_ADDRESS, addressingMode);

    }

    @Override
    public String send(SPDecoderGenericInstruction s, SPChip spChip) throws SPProtocolException, SPProtocolException {


        String output = "";
        String cleaned;
        byte[] addressByte = null;
        String addressToken[] = null;

        try {

            cleaned = spChip.getAddress(addressingMode, getProtocolName()).toUpperCase().trim().
                    replace("0X", "").replace("[", "").replace("]", "");


            if (cleaned.length() > 0){
                addressToken = cleaned.split(" ");
            } else {
                addressToken= new String[0];
            }



            addressByte = new byte[addressToken.length];
            for(int i = 0; i < addressToken.length; i++){
                addressByte[i] = (byte) Integer.parseInt(addressToken[i], 16);
            }


            byte protocol;

            if (addressByte.length == 0){
                protocol = (byte) NoAddressMask;
            } else if (addressByte.length == 1) {
                protocol = (byte) ShortAddressMask;
            } else {
                protocol = (byte) LongAddressMask;
            }

            String addressTokenEcho = "";
            for(int i = 0; i < addressToken.length; i++){
                addressTokenEcho += addressToken[i] + " ";
            }

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Chip address: " + addressTokenEcho, DEBUG_LEVEL);


            String comando = s.getDecodedInstruction().toUpperCase().trim().replace("0X", "").replace("[", "").replace("]", "");
            boolean readOp = comando.contains("R");

            String[] st = comando.split(" "); // split in words (default delimitator= whitespace)


            byte register = (byte) Integer.parseInt(st[0], 16);     // First byte is the register address

            byte byteToSendOrReceive = (byte) (st.length - 1);  // Count "r" for read operation or number of byte for

            protocol = (byte)(protocol | byteToSendOrReceive);  // Add to protocol the number of bytes to send/receive

            byte[] dataBytes = null;

            if (!readOp){
                dataBytes = new byte[byteToSendOrReceive];
                for(int i = 0; i < byteToSendOrReceive; i++){
                    dataBytes[i] = (byte) Integer.parseInt(st[i + 1], 16);
                }
            }


            String datatoSendLog = "Protocol: "+Hex.encodeHexString(new byte[]{protocol})+ " - Address: "+ (addressByte!=null ? Hex.encodeHexString(addressByte):"")+
                    " - Command: "+Hex.encodeHexString(new byte[]{register})+" - Data: "+ (dataBytes!=null ? Hex.encodeHexString(dataBytes):"");

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Sent: " + datatoSendLog, DEBUG_LEVEL);

            byte[] byteReceived = sendData(protocol, register, addressByte, dataBytes);



            for(int m = 0; m < byteReceived.length; m++){
                output += byteToString(byteReceived[m]);
            }

            output = "[" + output.trim() + "]";
        } catch (Exception e) {
            throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + "Generic exception in SPProtocol.send. " + e.getMessage());
        }
        //SPLogger.getLogInterface().d(LOG_MESSAGE, "Echo received: " + output, DEBUG_LEVEL);
        // This is the echo from Buspirate
        return output;
    }


    private static final int NUM_BYTE_REGISTER = 1;
    private static final int NUM_BYTE_PROTOCOL = 1;

    /** PROTOCOL FORMAT:
     * PROTOCOL[1]:ADDRESSES[0,1,6]:DATA[0...31]
     * Data length is indicated in 5 lsb in PROTOCOL
     * SENSIBUS transmission mode is defined in the 3 msb in protocol
     */
    private byte[] sendData(byte protocol, byte register, byte[] addresses, byte[] data) throws SPProtocolException, SPDriverException {
        byte[] byteReceived = null;



        int dataLength = 0;
        if (data != null){
            dataLength = data.length;
        }

        byte[] bufferToSend = new byte[NUM_BYTE_PROTOCOL + NUM_BYTE_REGISTER + addresses.length + dataLength];

        // FIRST BYTE: PROTOCOL
        bufferToSend[0] = protocol;

        // BYTE FOR ADDRESS: 0, 1, 6
        for(int i = NUM_BYTE_PROTOCOL; i < addresses.length + NUM_BYTE_PROTOCOL; i++){
            bufferToSend[i] = addresses[i - NUM_BYTE_PROTOCOL];
        }
        // BYTE FOR SENSIPLUS REGISTER: 1
        bufferToSend[addresses.length + NUM_BYTE_PROTOCOL] = register;

        int startIndex = (addresses.length + (NUM_BYTE_PROTOCOL + NUM_BYTE_REGISTER));
        int endIndex = (addresses.length + (NUM_BYTE_PROTOCOL + NUM_BYTE_REGISTER) + dataLength);

        // DATA TO SEND (0...31: we have the 5 lsb bits in protocol)


        for(int i = startIndex;  i < endIndex; i++){
            bufferToSend[i] = data[i - (addresses.length + (NUM_BYTE_PROTOCOL + NUM_BYTE_REGISTER))];
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Sent sequence: " + Hex.encodeHexString(bufferToSend), DEBUG_LEVEL);

        // For read operation, we expect more data with respect to data sent
        int dataLengthExpected = protocol & DataLengthMask;     // Need for read operation in particular

        //int counter = 0;
        //int maxCounter = 2;
        //boolean retry = true;
        //while(retry){
        //    try{



        byteReceived = sendL(bufferToSend, dataLengthExpected);
        //      retry = false;
        //    } catch (SPProtocolException e){
        //        counter++;
        //        retry = (counter < maxCounter);
        //        e.printStackTrace();
        //        SPDelay.delay(1000);
        //        receiverBufferByte.reset();
        //    }
        //}
        return byteReceived;
    }

    // Speed value: 255 = 102microseconds, 1=0.04microseconds
    private String setSpeed(byte speed) throws SPProtocolException, SPDriverException {


        if (speed == (byte) 0x00){
            throw new SPProtocolException("Maxmimum speed is equal to 0x01");
        }

        // Conversion of value from [0..255] to [255..0];
        //speed = (byte) (-speed - 1);


        byte addresses[] = null;
        byte protocol = 0;

        if (addressingMode == SPProtocol.FULL_ADDRESS){
            addresses = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            protocol = LongAddressMask;
        } else if (addressingMode == SPProtocol.SHORT_ADDRESS){
            addresses = new byte[]{0x00};
            protocol = ShortAddressMask;
        } else if (addressingMode == SPProtocol.NO_ADDRESS){
            addresses = new byte[0];
            protocol = NoAddressMask;
        }

        protocol = (byte)  (protocol | 0x01); // 1 byte to send


        byte register = (byte)0xDC; // SENSIBUS_SET address, write
        byte data[] = new byte[1];
        String mnemonic = "WRITE SENSIBUS_SET+1 S 0x";
        data[0] = speed;

        byte response[] = sendData(protocol, register, addresses, data);


        String msg = mnemonic + String.format("%02x", data[0]).toUpperCase() + "  -- set sensibus speed";
        if ((SPLogger.DEBUG_TH | SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE) != 0){
            SPLogger.getLogInterface().d(LOG_MESSAGE, msg, SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE);
        } else {
            SPLogger.getLogInterface().d(LOG_MESSAGE, msg, DEBUG_LEVEL);
        }


        return msg;

    }

    @Override
    public String setAddressingType(int mod) throws SPProtocolException, SPDriverException {
        String output = setAddressingType(addressingMode, mod);
        addressingMode = mod;
        return output;
    }



    private String setAddressingType(int modoAttuale, int newAddressType) throws SPProtocolException, SPDriverException {
        if (newAddressType < 0 || newAddressType >= 3){
            newAddressType = newAddressType < 0 ? 0 : newAddressType >= 3 ? 2 : newAddressType;
            throw new SPProtocolException("Communication mode not allowed in SENSIBUS: " + SPProtocol.ADDRESSING_TYPES[newAddressType]);
        }

        if (modoAttuale == newAddressType){
            return "";
        }

        String instruction = "WRITE SENSIBUS_SET S 0x";

        if (newAddressType == SPProtocol.SHORT_ADDRESS){
            //data[0] = (byte)0X2C;
            //data[0] = (byte)0XEC; //OK, SIFT = 11
            instruction += "EC";
        } else if (newAddressType == SPProtocol.FULL_ADDRESS){
            //data[0] = (byte)0X0C;
            //data[0] = (byte)0XCC; //OK, SIFT = 11
            instruction += "CC";
        } else if (newAddressType == SPProtocol.NO_ADDRESS){
            //data[0] = (byte)0X1F;
            //data[0] = (byte)0XDF; //OK, SIFT = 11
            instruction += "DC";
            //mnemonic += "0XDF";
        }
        //data += "C"; // "F" is maximum of... "C" is OK. TODO: verify the correct value


        //byte response[] = sendData(protocol, register, addresses, data);

        sendInstruction(instruction, null);

        addressingMode = newAddressType;

        return getProtocolName() + " " + SPProtocol.ADDRESSING_TYPES[newAddressType] + " OK!";

    }


    @Override
    public String getProtocolName() {
        return SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS];
    }



    public boolean isResettable(){
        return true;
    }

    protected final byte[] sendL(byte b[], int n) throws SPProtocolException, SPDriverException{
        return sendL(b, n, timeOutFromDriver);
    }


    public static int TIMEOUT_HIST = 100;
    public static float STEP_HIST = 0;
    public static int TIMEOUT_DISTRIBUTION[];

    protected final byte[] sendL(byte b[], int n, long timeout) throws SPProtocolException, SPDriverException {

        long start = System.currentTimeMillis();
        TIMEOUT_DISTRIBUTION =  new int[TIMEOUT_HIST];
        STEP_HIST = (float)timeout/TIMEOUT_HIST;

        byte output[] = new byte[n];
        receiverBufferByte.reset();

        long start1 = System.currentTimeMillis();
        spDriver.write(b);
        WRITE_ELAPSED += (System.currentTimeMillis() - start1);

        for (int i = 0; i < n; i++) {
            try {
                //if (receiverBufferByte.getAndResetInterrupt()) {
                //SPLogger.d("EXCEPTION", "interrupt received in byte[] sendL(...) ");
                //    throw new SPProtocolException("interrupt received in byte[] sendL(...) ");
                //}
                start1 = System.currentTimeMillis();
                output[i] = receiverBufferByte.get(timeout);
                long elapsed = System.currentTimeMillis() - start1;
                int k = 0;
                while(k < TIMEOUT_HIST && elapsed > STEP_HIST*k){
                    TIMEOUT_DISTRIBUTION[k]++;
                    k++;
                }
            } catch (SPProtocolException e) {
                //SPLogger.d(SCDriverFT232RLService.TAG_SERVICE_GENERAL_ERROR, "timeout in byte[] sendL(...)");
                //annullaGeneralError = stato;
                e.printStackTrace();
                throw e;
            } catch (Exception e) {
                //SPLogger.d(SCDriverFT232RLService.TAG_SERVICE_GENERAL_ERROR, e.getMessage());
                //annullaGeneralError = stato;
                e.printStackTrace();
                throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + e.getMessage());
            }
        }
        receiverBufferByte.reset();
        SENDL_ELAPSED += (System.currentTimeMillis() - start);


        return output;
    }





    public void run() {
        byte bufferRec[] = new byte[READBUF_SIZE];
        int len = 0;
        String mess;
        //int counter = 0;
        stoppedReceiverThread = false;
        while (!stopReceiverThread) {

            mess = "";
            try {
                len = spDriver.read(bufferRec);
                COUNTER[len]++;
            } catch (SPDriverException sce) {
                SPLogger.getLogInterface().d(SPProtocol.LOG_MESSAGE, "Exception in run(): " + sce.getMessage(), DEBUG_LEVEL);
            }
            if (len >= 1) {

                try {
                    for(int i = 0; i < len; i++){
                        receiverBufferByte.put(bufferRec[i]);
                        mess += " " + byteToString(bufferRec[i]);
                    }
                } catch (SPProtocolException ie) {
                    //SPLogger.d("EXCEPTION", "timeout in run() " + ie.getMessage());
                }
            } else {
                SPDelay.delay(SPDelay.DELAY_LOW);
            }
        }
        stoppedReceiverThread = true;

    }

    private String byteToString(byte b) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("0x%02X ", b));
        return sb.toString();
    }

    public SPDecoder getSPDecoder() throws SPProtocolException {
        return new SPDecoderSensibus();
    }

    @Override
    public byte[] sendPacket(SPProtocolPacket packet) throws SPDriverException, SPProtocolException {
        throw new SPProtocolException("Not yet implemented");
    }

}
