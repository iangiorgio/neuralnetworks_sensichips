package com.sensichips.sensiplus.level1.protocols.stm32l0;

import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.SPProtocolListener;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.protocols.ft232rl.SPProtocolFT232RL_BUSPIRATE;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level1.decoder.SPDecoderSPI;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

/**
 * Created by mario on 19/06/17.
 */
public class SPProtocolSTM32_SPI  extends SPProtocolFT232RL_BUSPIRATE implements Runnable {

    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;

    public static int COUNTER[] = new int[READBUF_SIZE];
    public static int TOTAL_BYTE_SENT = 0;
    public static long SENDL_ELAPSED = 0;
    public static long WRITE_ELAPSED = 0;

    protected SCDriver_ReceiverQueue<Byte> receiverBufferByte = new SCDriver_ReceiverQueue<Byte>(Byte.class, queueSize);

    public static final String MCU = "STM32L0";


    private byte interruptByte = (byte) 0x77;



    public SPProtocolSTM32_SPI (SPDriver readWrite, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException{
        super(readWrite, spCluster, addressingMode);
        protocolName = MCU + "_" + SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI];
    }


    /**
     * This version is specialized to set the buspirate in BB mode
     *
     * @throws SPProtocolException
     */
    @Override
    public void init(int addressingMode) throws SPProtocolException, SPDriverException {
        if (GENERAL_ERROR) {
            throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE);
        }
        spDriver.openConnection();
        startReceiverThread();


        setAddressingType(addressingMode);

        //currentID = ID_BROADCAST;
        spProtocolListener.connectionUp(this);
        isInitialized = true;

        return;

    }





    @Override
    public String resetChips(SPDecoderGenericInstruction instruction, SPChip spchip) throws SPProtocolException, SPDriverException {

        send(instruction, spchip);
        return setAddressingType(SPProtocol.FULL_ADDRESS, addressingMode);

    }

    @Override
    public String setAddressingType(int mod) throws SPProtocolException {
        String output = setAddressingType(addressingMode, mod);
        addressingMode = mod;
        return output;
    }

    private String setAddressingType(int modoAttuale, int mod) throws SPProtocolException {
        if (mod != 0 ){
            mod = mod < 0 ? 0 : mod >= 3 ? 2 : mod;
            throw new SPProtocolException("Communication mode not allowed in SPI: " + SPProtocol.ADDRESSING_TYPES[mod]);
        }

        if (modoAttuale == mod){
            return "";
        }

        String address = "";

        return "";
    }



    @Override
    protected String send(String comando) throws SPProtocolException {
        String output = "";
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Command to sent: " + comando, DEBUG_LEVEL);

        try {
            boolean readOp = comando.contains("r");

            comando = comando.replace("0X", "").replace("0x", "");
            comando = comando.replace("[", "").replace("]", "");

            String[] st = comando.split(" "); // split in words (default delimitator= whitespace)
            int batchSizeMax = 3;
            //if (st.length % batchSize != 0){
            //    throw new SPProtocolException("Wrong number of token: should be a multiple of " + batchSize);
            //}
            byte[] byteReceived;

            byte register = (byte) Integer.parseInt(st[0], 16);;

            byte byteToSendOrReceive = (byte) (st.length - 1);

            byte[] bufferToSend;

            if (readOp){
                bufferToSend = new byte[]{register, byteToSendOrReceive};
            } else {
                bufferToSend = new byte[byteToSendOrReceive + 2];
                bufferToSend[0] = register;
                bufferToSend[1] = byteToSendOrReceive;
                for(int i = 0; i < byteToSendOrReceive; i++){
                    bufferToSend[i + 2] = (byte) Integer.parseInt(st[i + 1], 16);
                }
            }
            byteReceived = sendL(bufferToSend, byteToSendOrReceive);

            for(int m = 0; m < byteReceived.length; m++){
                output += byteToString(byteReceived[m]);
            }

            output = "[" + output.trim() + "]";
        } catch (Exception e) {
            throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + "Generic exception in SPProtocol.send. " + e.getMessage());
        }
        //SPLogger.getLogInterface().d(LOG_MESSAGE, "Echo received: " + output, DEBUG_LEVEL);
        // This is the echo from Buspirate
        return output;
    }



    @Override
    public String getProtocolName() {
        return SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI];
    }


    @Override
    protected void sendID(String id) throws SPProtocolException {
        send(id);
        SPLogger.getLogInterface().d(LOG_MESSAGE, "ID sent: " + id, DEBUG_LEVEL);
    }



    public String reset() throws SPProtocolException {


        isInitialized = false;
        GENERAL_ERROR = false;
        stopReceiverThread();

        return "Reset Ok: " + getProtocolName();
    }

    public boolean isResettable(){
        return true;
    }

    protected final byte[] sendL(byte b[], int n) throws SPProtocolException, SPDriverException {
        return sendL(b, n, timeOutFromDriver);
    }

    protected final byte[] sendL(byte b[], int n, long timeout) throws SPProtocolException, SPDriverException {

        long start = System.currentTimeMillis();

        byte output[] = new byte[n];
        receiverBufferByte.reset();

        long start1 = System.currentTimeMillis();
        spDriver.write(b);
        WRITE_ELAPSED += (System.currentTimeMillis() - start1);

        for (int i = 0; i < n; i++) {
            try {
                //if (receiverBufferByte.getAndResetInterrupt()) {
                    //SPLogger.d("EXCEPTION", "interrupt received in byte[] sendL(...) ");
                //    throw new SPProtocolException("interrupt received in byte[] sendL(...) ");
                //}
                output[i] = receiverBufferByte.get(timeout);
            } catch (SPProtocolException e) {
                //SPLogger.d(SCDriverFT232RLService.TAG_SERVICE_GENERAL_ERROR, "timeout in byte[] sendL(...)");
                //annullaGeneralError = stato;
                e.printStackTrace();
                GENERAL_ERROR = true;
                throw e;
            } catch (Exception e) {
                //SPLogger.d(SCDriverFT232RLService.TAG_SERVICE_GENERAL_ERROR, e.getMessage());
                //annullaGeneralError = stato;
                e.printStackTrace();
                GENERAL_ERROR = true;
                throw new SPProtocolException(SPProtocol.GENERAL_ERROR_MESSAGE + e.getMessage());
            }
        }
        receiverBufferByte.reset();
        SENDL_ELAPSED += (System.currentTimeMillis() - start);


        return output;
    }

    public void run() {
        byte bufferRec[] = new byte[READBUF_SIZE];
        int len = 0;
        String mess;
        //int counter = 0;
        stoppedReceiverThread = false;
        while (!stopReceiverThread) {
            //if (counter % 100 == 0){
            //    System.out.println("Thread counter: " + counter);
            //}
            //System.out.println("Thread counter: " + counter);
            //counter++;
            mess = "";
            try {
                len = spDriver.read(bufferRec);
                COUNTER[len]++;
            } catch (SPDriverException sce) {
                SPLogger.getLogInterface().d(SPProtocol.LOG_MESSAGE, "Exception in run(): " + sce.getMessage(), DEBUG_LEVEL);
            }
            if (len >= 1) {

                try {
                //    if (bufferRec[0] == interruptByte) {
                //        mess = "Interrupt: ";
                //        if (spProtocolListener != null) {
                //            spProtocolListener.interrupt(bufferRec[0]);
                //        }

                //        receiverBufferByte.setInterrupt(true);
                        //SPLogger.d("EXCEPTION", "put interrupt in bufferByte.");
                //    } else {
                        for(int i = 0; i < len; i++){
                            receiverBufferByte.put(bufferRec[i]);
                            mess += " " + byteToString(bufferRec[i]);
                        }
                 //   }

                    //SPLogger.d("BUFFER_BYTE", mess);
                } catch (SPProtocolException ie) {
                    //SPLogger.d("EXCEPTION", "timeout in run() " + ie.getMessage());
                }
            } else {
                SPDelay.delay(SPDelay.DELAY_LOW);
            }
        }
        stoppedReceiverThread = true;

    }

    private String byteToString(byte b) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("0x%02X ", b));
        return sb.toString();
    }

    public SPDecoder getSPDecoder() throws SPProtocolException {
        return new SPDecoderSPI();
    }

}
