package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by mario on 01/03/17.
 */
public class SPRegisterList implements Serializable {

    public static final long serialVersionUID = 42L;


    //private static Hashtable<String, SPRegisterListItem> table = null;
    private static Hashtable<String, Hashtable<String, SPRegisterListItem>> versions = new Hashtable<>();


    public String[] getCommandSuggestions(String HW_VERSION) throws SPProtocolException{
        Hashtable<String, SPRegisterListItem> table = versions.get(HW_VERSION);

        if (table == null){
            table = createTable(HW_VERSION);
            versions.put(HW_VERSION, table);
        }
        ArrayList<String> registers = new ArrayList<String>();
        registers.addAll(table.keySet());
        int riemp = registers.size();
        ArrayList<String> output = new ArrayList<String>();
        output.add("READ ");
        output.add("WRITE ");
        for(int i = 0; i < riemp; i++){
            output.add("READ " + registers.get(i) + " ");
            output.add("WRITE " + registers.get(i) + " ");
        }
        for(int i = 0; i < riemp; i++){
            output.add("READ " + registers.get(i) + " M 2");
            output.add("WRITE " + registers.get(i) + " M 0x");
        }
        for(int i = 0; i < riemp; i++){
            output.add("READ " + registers.get(i) + " S 1");
            output.add("WRITE " + registers.get(i) + " S 0x");
        }
        return output.toArray(new String[0]);
    }


    public static SPRegisterListItem getSPRegister(String HW_VERSION, String name) throws SPProtocolException {
        Hashtable<String, SPRegisterListItem> table = versions.get(HW_VERSION);
        if (table == null){
            table = createTable(HW_VERSION);
            versions.put(HW_VERSION, table);
        }
        return table.get(name);
    }


    private static void putRegister(Hashtable<String, SPRegisterListItem> table, SPRegisterListItem spRegisterListItem){
        table.put(spRegisterListItem.getName(), spRegisterListItem);
    }

    private static Hashtable<String, SPRegisterListItem> createTable(String HW_VERSION) throws SPProtocolException {
        Hashtable<String, SPRegisterListItem> table = versions.get(HW_VERSION);

        if (table == null && (HW_VERSION.equals(SPFamily.RUN4) || HW_VERSION.equals(SPFamily.RUN5))) {
            table = new Hashtable<>();
            putRegister(table, new SPRegisterListItem("STATUS", "0x00"));
            putRegister(table, new SPRegisterListItem("COMMAND", "0x00"));
            putRegister(table, new SPRegisterListItem("INT_CONFIG", "0x02"));
            putRegister(table, new SPRegisterListItem("ANA_CONFIG", "0x04"));
            putRegister(table, new SPRegisterListItem("DIG_CONFIG", "0x06"));
            putRegister(table, new SPRegisterListItem("CHA_DIVIDER", "0x08"));
            putRegister(table, new SPRegisterListItem("CHA_CONFIG", "0x0A"));
            putRegister(table, new SPRegisterListItem("CHA_LIMIT", "0x0C"));
            putRegister(table, new SPRegisterListItem("CHA_CONDIT", "0x0E"));
            putRegister(table, new SPRegisterListItem("CHA_FILTER", "0x10"));
            putRegister(table, new SPRegisterListItem("CHA_SELECT", "0x12"));
            putRegister(table, new SPRegisterListItem("US_BURST", "0x14"));
            putRegister(table, new SPRegisterListItem("US_TRANSMIT", "0x16"));
            putRegister(table, new SPRegisterListItem("RXCOUNTERL", "0x18"));
            putRegister(table, new SPRegisterListItem("US_PROBE", "0x18"));
            putRegister(table, new SPRegisterListItem("RXCOUNTERH", "0x1A"));
            putRegister(table, new SPRegisterListItem("US_RECEIVE", "0x1A"));
            putRegister(table, new SPRegisterListItem("DSS_DIVIDER", "0X1C"));
            putRegister(table, new SPRegisterListItem("DDS_FIFO", "0x1E"));
            putRegister(table, new SPRegisterListItem("DDS_CONFIG", "0x20"));
            putRegister(table, new SPRegisterListItem("DAS_CONFIG", "0x22"));
            putRegister(table, new SPRegisterListItem("DSS_SELECT", "0x24"));
            putRegister(table, new SPRegisterListItem("TIMERL", "0x26"));
            putRegister(table, new SPRegisterListItem("TIMER_DIVL", "0x26"));
            putRegister(table, new SPRegisterListItem("TIMERH", "0x28"));
            putRegister(table, new SPRegisterListItem("TIMER_DIVH", "0x28"));
            putRegister(table, new SPRegisterListItem("PPR_COUNTL", "0x2A"));
            putRegister(table, new SPRegisterListItem("PPR_THRESL", "0x2A"));
            putRegister(table, new SPRegisterListItem("PPR_COUNTH", "0x2C"));
            putRegister(table, new SPRegisterListItem("PPR_THRESH", "0x2C"));
            putRegister(table, new SPRegisterListItem("PPR_SELECT", "0x2E"));
            putRegister(table, new SPRegisterListItem("CHA_FIFOL", "0x30"));
            putRegister(table, new SPRegisterListItem("CHA_FIFOH", "0x32"));
            putRegister(table, new SPRegisterListItem("EFUSE_PROGR", "0x34"));
            putRegister(table, new SPRegisterListItem("SENSIBUS_SET", "0x36"));
            putRegister(table, new SPRegisterListItem("DEVICE_ID0", "0x38"));
            putRegister(table, new SPRegisterListItem("DEVICE_ID1", "0x3A"));
            putRegister(table, new SPRegisterListItem("DEVICE_ID2", "0x3C"));
            putRegister(table, new SPRegisterListItem("USER_EFUSE", "0x3E"));

        } else if (table == null && HW_VERSION.equals(SPFamily.RUN6)) {
            table = new Hashtable<>();
            putRegister(table, new SPRegisterListItem("STATUS", "0x00"));
            putRegister(table, new SPRegisterListItem("COMMAND", "0x00"));
            putRegister(table, new SPRegisterListItem("INT_CONFIG", "0x02"));
            putRegister(table, new SPRegisterListItem("ANA_CONFIG", "0x04"));
            putRegister(table, new SPRegisterListItem("DIG_CONFIG", "0x06"));
            putRegister(table, new SPRegisterListItem("CHA_DIVIDER", "0x08"));
            putRegister(table, new SPRegisterListItem("CHA_CONFIG", "0x0A"));
            putRegister(table, new SPRegisterListItem("CHA_LIMIT", "0x0C"));
            putRegister(table, new SPRegisterListItem("CHA_CONDIT", "0x0E"));
            putRegister(table, new SPRegisterListItem("CHA_PSHIFT", "0x10"));
            putRegister(table, new SPRegisterListItem("CHA_FILTER", "0x12"));
            putRegister(table, new SPRegisterListItem("CHA_SELECT", "0x14"));
            putRegister(table, new SPRegisterListItem("US_BURST", "0x16"));
            putRegister(table, new SPRegisterListItem("US_TRANSMIT", "0x18"));
            putRegister(table, new SPRegisterListItem("RXCOUNTERL", "0x1A"));
            putRegister(table, new SPRegisterListItem("US_PROBE", "0x1A"));
            putRegister(table, new SPRegisterListItem("RXCOUNTERH", "0x1C"));
            putRegister(table, new SPRegisterListItem("US_RECEIVE", "0X1C"));
            putRegister(table, new SPRegisterListItem("DSS_DIVIDER", "0x1E"));
            putRegister(table, new SPRegisterListItem("DDS_FIFO", "0x20"));
            putRegister(table, new SPRegisterListItem("DDS_CONFIG", "0x22"));
            putRegister(table, new SPRegisterListItem("DAS_CONFIG", "0x24"));
            putRegister(table, new SPRegisterListItem("DSS_SELECT", "0x26"));
            putRegister(table, new SPRegisterListItem("TIMERL", "0x28"));
            putRegister(table, new SPRegisterListItem("TIMER_DIVL", "0x28"));
            putRegister(table, new SPRegisterListItem("TIMERH", "0x2A"));
            putRegister(table, new SPRegisterListItem("TIMER_DIVH", "0x2A"));
            putRegister(table, new SPRegisterListItem("PPR_COUNTL", "0x2C"));
            putRegister(table, new SPRegisterListItem("PPR_THRESL", "0x2C"));
            putRegister(table, new SPRegisterListItem("PPR_COUNTH", "0x2E"));
            putRegister(table, new SPRegisterListItem("PPR_THRESH", "0x2E"));
            putRegister(table, new SPRegisterListItem("PPR_SELECT", "0x30"));
            putRegister(table, new SPRegisterListItem("CHA_FIFOL", "0x32"));
            putRegister(table, new SPRegisterListItem("CHA_FIFOH", "0x34"));
            putRegister(table, new SPRegisterListItem("SENSIBUS_SET", "0x36"));
            putRegister(table, new SPRegisterListItem("DEVICE_ID0", "0x38"));
            putRegister(table, new SPRegisterListItem("DEVICE_ID1", "0x3A"));
            putRegister(table, new SPRegisterListItem("DEVICE_ID2", "0x3C"));
            putRegister(table, new SPRegisterListItem("USER_EFUSE", "0x3E"));


            // Bank 2 of new registers
            putRegister(table, new SPRegisterListItem("GPIO", "0x02"));
            putRegister(table, new SPRegisterListItem("PORT_FLAG1", "0x04"));
            putRegister(table, new SPRegisterListItem("PORT_FLAG2", "0x06"));
            putRegister(table, new SPRegisterListItem("PORT_FLAG3", "0x08"));
            putRegister(table, new SPRegisterListItem("PORT_DELAY", "0x0A"));

        } else {
            throw new SPProtocolException("SPRegisterList: register table for " + HW_VERSION + " version, is not available");
        }

        return table;
    }



}
