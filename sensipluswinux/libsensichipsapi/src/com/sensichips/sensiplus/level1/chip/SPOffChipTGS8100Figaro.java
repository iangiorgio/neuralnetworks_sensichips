package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.SPMeasurementRUN5_PC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterADC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemFilter;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemQI;

import javax.swing.*;

public class SPOffChipTGS8100Figaro {
    /*public static final Integer NUM_OF_OUTPUT = 3;
    public static final Integer RESISTANCE_SENSOR = 0;
    public static final Integer CAPACITANCE = 1;

    public static final Integer TEMPERATURE = 2;
    public static final Integer RESISTANCE_HEATER = 3;*/

    public static final Integer EIS_MODE = 0;
    public static final Integer TCO_MODE = 1;




    public static final String[] operationModes = {"EIS","TCO"};

    public String getTGS8100FigaroMode() {
        return TGS8100FigaroMode;
    }

    private String TGS8100FigaroMode = null;


    public SPMeasurementParameterEIS getParamEIS() {
        return paramEIS;
    }

    public SPMeasurementParameterEIS getParamTCO_RH() {
        return paramTCO_RH;
    }

    private String filter;

    public SPMeasurementParameterEIS getParamTCO_FS() {
        return paramTCO_FS;
    }

    SPMeasurementParameterEIS paramEIS   =   null;      //PARAMETER TO BE USED WITH EIS MODE
    SPMeasurementParameterEIS paramTCO_RH   =   null;   //PARAMETER TO BE USED WITH TCO MODE TO MEASURE RH
    SPMeasurementParameterEIS paramTCO_FS   =   null;   //PARAMETER TO BE USED WITH TCO MODE TO MEASURE TGS8100 SENSING RESISTANCE


    SPMeasurement spMeasurement_EIS;
    SPMeasurement spMeasurement_TCO_RH;
    SPMeasurement spMeasurement_TCO_FS;


    public SPMeasurement getSpMeasurement_EIS() {
        return spMeasurement_EIS;
    }

    public SPMeasurement getSpMeasurement_TCO_RH() {
        return spMeasurement_TCO_RH;
    }

    public SPMeasurement getSpMeasurement_TCO_FS() {
        return spMeasurement_TCO_FS;
    }

    public SPOffChipTGS8100Figaro(SPConfiguration spConfiguration,
                                  String TGS8100Mode,
                                  String TGS8100Vh)throws SPException {

        //Map<Double,FigaroDCBiasConf> map = createMap();

        if (TGS8100Mode.equals(operationModes[EIS_MODE])) {
            TGS8100FigaroMode = operationModes[EIS_MODE];
            paramEIS = new SPMeasurementParameterEIS(spConfiguration);
            paramEIS.setContacts("TWO");
            //if(toClone.getInGainLabel()!=null)
            paramEIS.setInGain("40");
            paramEIS.setHarmonic("FIRST_HARMONIC");
            paramEIS.setModeVI("VOUT_IIN");

            paramEIS.setFrequency(new Float(10));
            paramEIS.setConversion_Rate(new Double(10));

            paramEIS.setDCBiasP(-100);
            paramEIS.setDCBiasN(0);
            paramEIS.setOutGain("7");
            paramEIS.setInPort("PORT_HP");
            paramEIS.setOutPort("PORT_HP");
            paramEIS.setMeasure("RESISTANCE");
            paramEIS.setSequentialMode(false);
            //if(toClone.getRsenseLabel()!=null)
            paramEIS.setRsense("50");
            paramEIS.setFilter("1");
            paramEIS.setPhaseShiftQuadrants("Quadrants",
                    0, "IN_PHASE");

            if (spConfiguration.getCluster().getFamilyOfChips().
                    getHW_VERSION().equals(SPFamily.RUN5)) {
                paramEIS.setFIFO_DEEP(1);
                paramEIS.setFIFO_READ(1);
            } else {
                paramEIS.setFIFO_DEEP(8);
                paramEIS.setFIFO_READ(1);
            }

            paramEIS.setFillBufferBeforeStart(false);
            paramEIS.setBurstMode(false);
            paramEIS.setInPortADC(SPMeasurementParameterADC.INPORT_IA);

        } else if (TGS8100Mode.equals(operationModes[TCO_MODE])) {

            TGS8100FigaroMode = operationModes[TCO_MODE];


            spMeasurement_TCO_RH= spConfiguration.getSPMeasurement(SPFamily.MEASURE_TYPES_AVAILABLE[SPFamily.MEASURE_TYPE_EIS]);

            spMeasurement_TCO_FS= spConfiguration.getSPMeasurement(SPFamily.MEASURE_TYPES_AVAILABLE[SPFamily.MEASURE_TYPE_EIS]);

            Integer DCbiasP = (Integer.parseInt(TGS8100Vh)-spConfiguration.getVdd()/2)*4096/spConfiguration.getVREF();


            /**Preparing the SPMeasurementParameterEIS to measure the TGS8100 sensing resistance*/
            paramTCO_FS = new SPMeasurementParameterEIS(spConfiguration);
            paramTCO_FS.setContacts("FOUR");
            paramTCO_FS.setInGain("40");
            paramTCO_FS.setHarmonic("FIRST_HARMONIC");
            paramTCO_FS.setModeVI("VOUT_IIN");
            paramTCO_FS.setFrequency(new Float(0));

            paramTCO_FS.setConversion_Rate(50.0);
            //paramTCO_FS.setConversion_Rate(400.0);


            /*if(DCbiasP>SPMeasurementParameterEIS.dcbiasPMax){
                DCbiasP = SPMeasurementParameterEIS.dcbiasPMax;
            }else if(DCbiasP<SPMeasurementParameterEIS.dcbiasPMin){
                DCbiasP = SPMeasurementParameterEIS.dcbiasPMin;
            }*/

            try{
                paramTCO_FS.setDCBiasP(DCbiasP);
            }catch (SPException spe){

                double minVoltageAllowed = spConfiguration.getVdd()/2+spConfiguration.getVREF()*(SPMeasurementParameterEIS.dcbiasPMin)/4096;
                //double maxVoltageAllowed = spConfiguration.getVdd()/2+spConfiguration.getVREF()*(SPMeasurementParameterEIS.dcbiasPMax)/4096;
                double maxVoltageAllowed = 2000.0;
                throw new SPException(TGS8100Vh+"mV not allowed. Available values: [ "+minVoltageAllowed+" , "+maxVoltageAllowed+" ] mV");

                /*if(DCbiasP<SPMeasurementParameterEIS.dcbiasPMin)
                    paramTCO_FS.setDCBiasP(SPMeasurementParameterEIS.dcbiasPMin);
                else if(DCbiasP<SPMeasurementParameterEIS.dcbiasPMax)
                    paramTCO_FS.setDCBiasP(SPMeasurementParameterEIS.dcbiasPMax);*/

            }
            paramTCO_FS.setDCBiasN(0);
            paramTCO_FS.setOutGain("7");
            paramTCO_FS.setInPort("PORT_HP");
            paramTCO_FS.setOutPort("PORT_HP");
            paramTCO_FS.setMeasure("RESISTANCE");
            paramTCO_FS.setSequentialMode(false);
            //if(toClone.getRsenseLabel()!=null)
            paramTCO_FS.setRsense("50");
            paramTCO_FS.setFilter("1");
            paramTCO_FS.setPhaseShiftQuadrants("Quadrants",
                    0, "IN_PHASE");

            if (spConfiguration.getCluster().getFamilyOfChips().
                    getHW_VERSION().equals(SPFamily.RUN5)) {
                paramTCO_FS.setFIFO_DEEP(1);
                paramTCO_FS.setFIFO_READ(1);
            } else {
                paramTCO_FS.setFIFO_DEEP(8);
                paramTCO_FS.setFIFO_READ(1);
            }

            paramTCO_FS.setFillBufferBeforeStart(false);
            paramTCO_FS.setBurstMode(false);
            paramTCO_FS.setInPortADC(SPMeasurementParameterADC.INPORT_IA);

            /**preparing the SPMeasurementParameterEIS which will be used to monitor the Rh in order to calculate the
             * TGS8100 heater temperature*/
            paramTCO_RH = new SPMeasurementParameterEIS(spConfiguration);
            paramTCO_RH.setContacts("FOUR");
            paramTCO_RH.setInGain("12");
            paramTCO_RH.setHarmonic("FIRST_HARMONIC");
            paramTCO_RH.setModeVI("VOUT_VIN");
            paramTCO_RH.setFrequency(new Float(0));

            paramTCO_RH.setConversion_Rate(50.0);
            //paramTCO_RH.setConversion_Rate(1000.0);
            paramTCO_RH.setDCBiasP(DCbiasP);
            paramTCO_RH.setDCBiasN(0);
            paramTCO_RH.setOutGain("7");
            paramTCO_RH.setInPort("PORT_HP");
            paramTCO_RH.setOutPort("PORT_HP");
            paramTCO_RH.setMeasure("RESISTANCE");
            paramTCO_RH.setSequentialMode(false);
            //if(toClone.getRsenseLabel()!=null)
            paramTCO_RH.setRsense("50");
            paramTCO_RH.setFilter("1");
            paramTCO_RH.setPhaseShiftQuadrants("Quadrants",
                    0, "IN_PHASE");

            if (spConfiguration.getCluster().getFamilyOfChips().
                    getHW_VERSION().equals(SPFamily.RUN5)) {
                paramTCO_RH.setFIFO_DEEP(1);
                paramTCO_RH.setFIFO_READ(1);
            } else {
                paramTCO_RH.setFIFO_DEEP(8);
                paramTCO_RH.setFIFO_READ(1);
            }
            paramTCO_RH.setFillBufferBeforeStart(false);
            paramTCO_RH.setBurstMode(false);
            paramTCO_RH.setInPortADC(SPMeasurementParameterADC.INPORT_IA);
        }
    }

    public void setFilter(String filter){
        /*if(paramEIS!=null){
            paramEIS.setFilter(filter);
        }else{
            paramTCO_FS.setFilter(filter);
            paramTCO_RH.setFilter(filter);
        }*/
       this.filter = filter;
    }

    public String getFilter(){
        return this.filter;
    }





}
