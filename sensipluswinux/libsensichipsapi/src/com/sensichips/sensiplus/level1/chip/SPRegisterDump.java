package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mario on 10/01/17.
 */
public class SPRegisterDump implements Serializable {

    public static final long serialVersionUID = 42L;
    private LinkedHashMap<String, SPRegisterDumpItem> table = new LinkedHashMap<>();

    public static final String DONT_CARE = "--";
    public static final String OTP = "XX";

    public SPRegisterDump(String HW_VERSION) throws SPProtocolException {
        // Create a startup dump of RUN4
        if (HW_VERSION.equals(SPFamily.RUN4)) {
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "STATUS"), DONT_CARE, "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "COMMAND"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "INT_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "ANA_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DIG_CONFIG"), "06", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_DIVIDER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_CONFIG"), "07", "10"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_LIMIT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_CONDIT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FILTER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_BURST"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_TRANSMIT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "RXCOUNTERL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_PROBE"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "RXCOUNTERH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_RECEIVE"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DSS_DIVIDER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DDS_FIFO"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DDS_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DAS_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DSS_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMERL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMER_DIVL"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMERH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMER_DIVH"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_COUNTL"), "FF", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_THRESL"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_COUNTH"), "FF", "FF"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_THRESH"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FIFOL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FIFOH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "EFUSE_PROGR"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "SENSIBUS_SET"), "FF", "CF"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID0"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID1"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID2"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "USER_EFUSE"), OTP, OTP));

        } else if (HW_VERSION.equals(SPFamily.RUN5)){
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "STATUS"), DONT_CARE, "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "COMMAND"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "INT_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "ANA_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DIG_CONFIG"), "06", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_DIVIDER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_CONFIG"), "07", "10"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_LIMIT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_CONDIT"), "00", "1F"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FILTER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_BURST"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_TRANSMIT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "RXCOUNTERL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_PROBE"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "RXCOUNTERH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_RECEIVE"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DSS_DIVIDER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DDS_FIFO"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DDS_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DAS_CONFIG"), "07", "FF"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DSS_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMERL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMER_DIVL"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMERH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMER_DIVH"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_COUNTL"), "FF", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_THRESL"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_COUNTH"), "FF", "FF"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_THRESH"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FIFOL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FIFOH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "EFUSE_PROGR"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "SENSIBUS_SET"), "FF", "CF"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID0"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID1"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID2"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "USER_EFUSE"), OTP, OTP));


        } else if (HW_VERSION.equals(SPFamily.RUN6)){

            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "STATUS"), DONT_CARE, "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "COMMAND"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "INT_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "ANA_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DIG_CONFIG"), "06", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_DIVIDER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_CONFIG"), "07", "10"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_LIMIT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_CONDIT"), "00", "1F"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_PSHIFT"), "00", "1F"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FILTER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_BURST"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_TRANSMIT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "RXCOUNTERL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_PROBE"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "RXCOUNTERH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "US_RECEIVE"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DSS_DIVIDER"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DDS_FIFO"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DDS_CONFIG"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DAS_CONFIG"), "07", "FF"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DSS_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMERL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMER_DIVL"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMERH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "TIMER_DIVH"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_COUNTL"), "FF", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_THRESL"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_COUNTH"), "FF", "FF"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_THRESH"), DONT_CARE, DONT_CARE));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "PPR_SELECT"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FIFOL"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "CHA_FIFOH"), "00", "00"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "SENSIBUS_SET"), "FF", "CF"));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID0"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID1"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "DEVICE_ID2"), OTP, OTP));
            putRegister(new SPRegisterDumpItem(SPRegisterInfoList.getSPRegister(HW_VERSION, "USER_EFUSE"), OTP, OTP));



            // Bank 2 of new registers
            //putRegister(table, new SPRegisterDumpItem("GPIO", "0x02"));
            //putRegister(table, new SPRegisterDumpItem("PORT_FLAG1", "0x04"));
            //putRegister(table, new SPRegisterDumpItem("PORT_FLAG2", "0x06"));
            //putRegister(table, new SPRegisterDumpItem("PORT_FLAG3", "0x08"));
            //putRegister(table, new SPRegisterDumpItem("PORT_DELAY", "0x0A"));

        } else {
            throw new SPProtocolException("SPRegisterDump: initialization of the register dump for " + HW_VERSION + " version, is not available");
        }

    }

    static boolean HexVerifier(String val){

        if (val == null){
            return false;
        }

        val = val.toUpperCase();
        if (val.equals(DONT_CARE)|| val.equals(OTP)){
            return true;
        }

        val = val.replace("0X","");
        if (val.length() != 2){
            return false;
        }
        try {
            Integer.parseInt(val, 16); // To verify if is an hexadecimal value
        } catch (Exception e){
            return false;
        }
        return true;
    }

    public String getRegisterAddress(String name){
        return table.get(name).getSpRegisterInfo().getAddress();
    }



    public SPRegisterDumpItem getSPRegister(String HW_VERSION, String registerName){
        return table.get(registerName);
    }

    public void putRegister(SPRegisterDumpItem register){
        table.put(register.getSpRegisterInfo().getName(), register);
    }

    public String [] getRegisterNames(){
        String [] out = new String[table.size()];
        int i = 0;
        for(Map.Entry<String, SPRegisterDumpItem> entry:table.entrySet()){
            out[i++] = entry.getKey();
        }
        return out;
    }


    public String[] generateInstructionToRefresh(String HW_VERSION){
        String[] registerNames = getRegisterNames();
        String[] output;
        ArrayList<String> appo = new ArrayList<>();
        String instruction;
        int counter;
        String multiplicity;
        for(int i = 0; i < registerNames.length; i++){
            counter = 0;
            if (!getSPRegister(HW_VERSION, registerNames[i]).getLSB().equals(DONT_CARE)){
                counter++;
            }
            if (!getSPRegister(HW_VERSION, registerNames[i]).getMSB().equals(DONT_CARE)){
                counter++;
            }
            if (counter > 0){
                multiplicity = counter == 1 ? "S" : "M";
                instruction = "READ " + registerNames[i] + " " + multiplicity + " " + counter;
                appo.add(instruction);
            }
        }
        output = new String[appo.size()];
        for(int i = 0; i < appo.size(); i++){
            output[i] = appo.get(i);
        }
        return output;
    }


    public static void main(String[] args) throws SPProtocolException {
        SPRegisterDump dump = new SPRegisterDump(SPFamily.RUN5);
        String[] instruction = dump.generateInstructionToRefresh(SPFamily.RUN5);
        for(int i = 0; i < instruction.length; i++)  {
            System.out.println(instruction[i]);
        }
    }
}
