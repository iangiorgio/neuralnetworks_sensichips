package com.sensichips.sensiplus.level1.chip;


import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;

/**
 * Created by mario on 10/01/17.
 */
public class SPRegisterDumpItem implements Serializable {

    public static final long serialVersionUID = 42L;

    private SPRegisterInfo spRegisterInfo;
    private String MSB = "";
    private String LSB = "";

    public SPRegisterDumpItem(SPRegisterInfo spRegisterInfo, String MSB, String LSB) throws SPProtocolException {
        this.spRegisterInfo = spRegisterInfo;
        this.MSB = MSB.toUpperCase();
        this.LSB = LSB.toUpperCase();

        if (MSB == null || !SPRegisterDump.HexVerifier(MSB)){
            throw new SPProtocolException("SPRegisterDumpItem constructor. Wrong format for MSB: " + MSB);
        } else if (!SPRegisterDump.HexVerifier(LSB)){
            throw new SPProtocolException("SPRegisterDumpItem constructor. Wrong format for LSB: " + LSB);
        }

    }

    public SPRegisterInfo getSpRegisterInfo() {
        return spRegisterInfo;
    }

    public void setSpRegisterInfo(SPRegisterInfo spRegisterInfo) {
        this.spRegisterInfo = spRegisterInfo;
    }

    public String getMSB() {
        return MSB;
    }

    public void setMSB(String MSB) {
        this.MSB = MSB;
    }

    public String getLSB() {
        return LSB;
    }

    public void setLSB(String LSB) {
        this.LSB = LSB;
    }

}
