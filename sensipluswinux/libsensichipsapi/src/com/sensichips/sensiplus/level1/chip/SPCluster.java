package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco on 30/01/2017.
 */
public class SPCluster implements Serializable {

    public static final long serialVersionUID = 42L;

    private String LOG_MESSAGE = "SPCluster";

    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L1;

    private String broadcastI2CAddress;

    private String multicastAddress;
    private String broadcastAddress;
    private String broadcastSlowAddress;

    private String clusterID;

    private List<SPChip> chipList;

    private List<SPChip> chipWithBandgapProblem;

    private SPChip multicastChip;
    private SPChip broadcastChip;
    private SPChip broadcastSlowChip;


    private SPFamily familyOfChips;

    public SPCluster() {
        this.clusterID = null;
        this.chipList = new ArrayList<SPChip>();
        this.chipWithBandgapProblem = new ArrayList<SPChip>();
    }

    /**
     * All the chips received as a parameter, will be deactivated. All unselected chips will be reactivated.
     *
     * @param chipIDs         A list of SN of the chips (6 bytes) in hex format: "0x000000004405" for example.
     * @throws SPProtocolException
     */
    public void setSPChipActivationList(ArrayList<String> chipIDs, boolean activate) throws SPProtocolException {
        // chips contains a list of ChipID
        int pos;

        //for(int i = 0; i < chipList.size(); i++){
        //    chipList.get(i).setActive(true);
        //}

        for(int i = 0; i < chipIDs.size(); i++){
            String chip = chipIDs.get(i).toUpperCase().replace("0X", "");
            pos = findIDIndex(chipIDs.get(i));
            chipList.get(pos).setActive(activate);
        }
    }

    public List<SPChip> getChipWithBandgapProblem() {
        return chipWithBandgapProblem;
    }

    public void setChipWithBandgapProblem(List<SPChip> chipWithBandgapProblem) {
        this.chipWithBandgapProblem = chipWithBandgapProblem;
    }

    public List<SPChip> getChipList() {
        return chipList;
    }

    public void setChipList(List<SPChip> chipList) {
        this.chipList = chipList;
    }


    public int findIDIndex(String chipID) throws SPProtocolException {
        chipID = chipID.toUpperCase();
        boolean found = false;
        int j = 0;
        while(j < chipList.size() && !found){

            found = chipList.get(j).getSerialNumber().equals(chipID);
            j++;
        }
        j--;
        if (!found){
            throw new SPProtocolException("SPCluster: setChipActivationlist, chip not found!");
        }
        return j;
    }


    public SPChip getBroadcastSlowChip(){
        return broadcastSlowChip;
    }


    public SPChip getBroadcastChip(){
        return broadcastChip;
    }


    public SPChip getMulticastChip(){
        return multicastChip;
    }

    public void setBroadcastAddress(String address){
        this.broadcastAddress = address;
    }

    public void setBroadcastSlowAddress(String address){
        this.broadcastSlowAddress = address;
    }

    public void setMulticastAddress(String serialNumber) throws SPProtocolException {

        if (serialNumber == null){
            throw new SPProtocolException("multicastAddress cannot be null in SPCluster");
        }
        serialNumber = serialNumber.toUpperCase();

        int intValue = 0;
        try{
            intValue = Integer.parseInt(serialNumber.replace("0X",""), 16);
        } catch (Exception e){
            // intValue remain 0
        }

        if (intValue < 2 || intValue > 17){ // RUN5, bit MCA in DIG_CONFIG
            throw new SPProtocolException("Incorrect value for multicast address: " + serialNumber + ". Should be included in: [0x02 and 0x11])");
        }
        multicastAddress = serialNumber;
    }


    public String getMulticastAddress() {
        return multicastAddress;
    }


    public void setBroadcastAddressI2C(String I2CAddress){
        broadcastI2CAddress = I2CAddress;
    }


    public SPFamily getFamilyOfChips() {
        return familyOfChips;
    }

    public void setFamilyOfChips(SPFamily familyOfChips) throws SPProtocolException {
        this.familyOfChips = familyOfChips;

        if (familyOfChips == null){
            throw new SPProtocolException("SPFamily in SPChip is mandatory and cannot be null!");
        }
        broadcastSlowAddress = familyOfChips.getBroadcastSlowAddress();
        broadcastAddress = familyOfChips.getBroadcastAddress();

        try {
            //String serialNumberForMulticast = "0x0000000000";
            //String I2CAddressForMulticast = "0x00";
            SPFamily spFamilyForMulticast = new SPFamily();
            spFamilyForMulticast.setHW_VERSION(familyOfChips.getHW_VERSION());
            spFamilyForMulticast.setId("0X00");
            spFamilyForMulticast.setName(familyOfChips.getName() + "_MULTICAST");
            spFamilyForMulticast.setOSC_TRIM(familyOfChips.getOSC_TRIM());

            multicastChip = new SPChip(SPChip.CHIP_TYPE_MULTICAST);
            multicastChip.setSerialNumber(multicastAddress);
            multicastChip.setI2CAddress(broadcastI2CAddress);
            multicastChip.setSpFamily(spFamilyForMulticast);

            broadcastChip = new SPChip(SPChip.CHIP_TYPE_BROADCAST);
            broadcastChip.setSerialNumber(broadcastAddress);
            broadcastChip.setI2CAddress(broadcastI2CAddress);
            broadcastChip.setSpFamily(spFamilyForMulticast);

            broadcastSlowChip = new SPChip(SPChip.CHIP_TYPE_BROADCAST_SLOW);
            broadcastSlowChip.setSerialNumber(broadcastSlowAddress);
            broadcastSlowChip.setI2CAddress(broadcastI2CAddress);
            broadcastSlowChip.setSpFamily(spFamilyForMulticast);

        } catch (SPProtocolException e) {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception in SP constructor: invalid multicas id", SPLoggerInterface.DEBUG_VERBOSITY_L0);
            e.printStackTrace();
        }
    }

    public String getClusterID() {
        return clusterID;
    }

    public void setClusterID(String clusterID) {
        this.clusterID = clusterID;
    }

    public void add(SPChip spChip){
        chipList.add(spChip);
    }

    public SPCluster generateTempCluster(List<String> chips) throws SPProtocolException {
        int pos = 0;
        SPCluster out = new SPCluster();
        out.setBroadcastAddress(broadcastAddress);
        out.setBroadcastAddressI2C(broadcastI2CAddress);
        out.setBroadcastSlowAddress(broadcastSlowAddress);
        out.setClusterID(clusterID);
        out.setMulticastAddress(multicastAddress);
        out.setFamilyOfChips(familyOfChips);

        for(int i = 0; i < chips.size(); i++){
            pos = findIDIndex(chips.get(i));
            SPChip spChip = new SPChip();
            spChip.setActive(true);
            spChip.setSpFamily(chipList.get(pos).getSpFamily());
            spChip.setSerialNumber(chipList.get(pos).getSerialNumber());
            spChip.setI2CAddress(chipList.get(pos).getAddress(SPProtocol.SHORT_ADDRESS,SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C]));
            ArrayList<SPSensingElementOnChip> lista = new ArrayList<>();
            for(SPSensingElementOnChip sensingElementOnChip: chipList.get(pos).getSensingElementOnChipList()){
                lista.add(sensingElementOnChip);
            }
            spChip.setSensingElementOnChipList(lista);
            out.add(spChip);
        }
        return out;
    }

    /*public SPCluster generateTempCluster(List<SPChip> chips) throws SPProtocolException {
        int pos = 0;
        SPCluster out = new SPCluster();
        out.setBroadcastAddress(broadcastAddress);
        out.setBroadcastAddressI2C(broadcastI2CAddress);
        out.setBroadcastSlowAddress(broadcastSlowAddress);
        out.setClusterID(clusterID);
        out.setMulticastAddress(multicastAddress);
        out.setFamilyOfChips(familyOfChips);

        for(int i = 0; i < chips.size(); i++){
            pos = findIDIndex(chips.get(i).getSerialNumber());
            SPChip spChip = new SPChip();
            spChip.setActive(true);
            spChip.setSpFamily(chipList.get(pos).getSpFamily());
            spChip.setSerialNumber(chipList.get(pos).getSerialNumber());
            ArrayList<SPSensingElementOnChip> lista = new ArrayList<>();
            for(SPSensingElementOnChip sensingElementOnChip: chipList.get(pos).getSensingElementOnChipList()){
                lista.add(sensingElementOnChip);
            }
            spChip.setSensingElementOnChipList(lista);
            out.add(spChip);
        }
        return out;
    }*/

    public List<SPChip> getActiveSPChipList() {
        //TODO: creare una variabile di istanza che è la lista dei chip attivi e che viene restituita se è stata già
        //creata una volta. ATTENZIONE: facendo ciò bisogna stare attenti che se viene disattivato un chip una volta
        //creata la lista di chip attivi bisogna aggiornare lo stato del chip nella lista globale ma bisogna anche
        //aggiornare la lista dei chip attivi.
        List<SPChip> out = new ArrayList<>();
        for(int i = 0; i < chipList.size(); i++){
            if (chipList.get(i).isActive()){
                out.add(chipList.get(i));
            }
        }
        return out;
    }

    public void setSPChipAllActive(){
        for(int i = 0; i < chipList.size(); i++){
            chipList.get(i).setActive(true);
        }
    }
    public void setSPChipAllDisactive(){
        for(int i = 0; i < chipList.size(); i++){
            chipList.get(i).setActive(false);
        }
    }

    public void setSPChipActive(List<SPChip> spchipList) throws SPException{
        for(int i=0; i<spchipList.size(); i++){
            for(int j=0; j<chipList.size(); j++){
                if(spchipList.get(i).getSerialNumber().equals(chipList.get(j).getSerialNumber())){
                    chipList.get(j).setActive(true);
                }
            }
        }

        /*TODO: why all chips were activated?
        for(int i = 0; i < chipList.size(); i++){
            chipList.get(i).setActive(true);
        }*/
    }


    public List<SPChip> getUnactiveSPChipList(){
        List<SPChip> out = new ArrayList<>();
        for(int i = 0; i < chipList.size(); i++){
            if (!chipList.get(i).isActive()){
                out.add(chipList.get(i));
            }
        }
        return out;
    }

    public List<SPChip> getSPChipList() {
        List<SPChip> out = new ArrayList<>();
        for(int i = 0; i < chipList.size(); i++){
            out.add(chipList.get(i));
        }
        return out;
    }

    public List<String> getSPSerialNumbers() throws SPProtocolException {
        ArrayList<String> out = new ArrayList<>();
        for(int i = 0; i < chipList.size(); i++){
            out.add(chipList.get(i).getSerialNumber());
        }
        return out;
    }
    public List<String> getSPActiveSerialNumbers() throws SPProtocolException {
        ArrayList<String> out = new ArrayList<>();
        for(int i = 0; i < chipList.size(); i++){
            if (chipList.get(i).isActive()){
                out.add(chipList.get(i).getSerialNumber());
            }
        }
        return out;
    }
    public String toString(){
        String out = "";
        out += "\tclusterID:   " + clusterID + "\n";
        for(int i = 0; i < chipList.size(); i++){
            out += chipList.get(i);
        }
        return out;
    }




}
