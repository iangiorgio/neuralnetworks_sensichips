package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.SPException;

public class SPRegisterField {


    private static final String DONT_CARE = "-";

    private String fieldName;
    private String defaultValue;
    private byte mask = 0;
    private int binaryValue = 0;
    private int length = 0;
    private int position = 0;
    private String binCoded = null;
    private String hexCoded = null;

    public SPRegisterField(String fieldName, String defaultValue, byte mask) throws SPException {
        if (fieldName == null || fieldName.length() == 0){
            throw new SPException("Field name should be not null with length > 0 in SPRegisterField");
        }

        this.mask = mask;
        this.fieldName = fieldName;

        // CALCOLO
        position = -1;
        length = 0;


        boolean flagStart = false;
        boolean flagEnd = false;
        byte maskForPosition = 1;

        for(int i = 0; i < 8; i++){
            if (!flagStart) {
                flagStart = (mask & maskForPosition) == maskForPosition;
                if (flagStart){
                    position = i;
                }
            } else if (flagStart && !flagEnd){
                flagEnd = (mask & maskForPosition) == 0;

            }
            if (flagStart && !flagEnd){
                length++;
            }

            if (flagStart && flagEnd && (mask & maskForPosition) == maskForPosition){
                throw new SPException("SPRegisterField mask should be contiguous (example 0b11100000 or 0b00110000 but not 0b11001000).");
            }

            mask = (byte)((int)mask >> 1);

        }

        setValue(defaultValue);
        this.defaultValue = defaultValue;

    }

    public void reset() throws SPException{
        setValue(defaultValue);
    }

    public String getValueAsString() throws SPException{
        if (binCoded == null){
            throw new SPException("SPRegisterField value not specified!");
        }
        return binCoded;
    }

    public String getValueAsHexString() throws SPException{
        if (hexCoded == null){
            throw new SPException("SPRegisterField value not specified!");
        }
        return hexCoded;
    }

    public byte getValue(){
        return (byte) binaryValue;
    }

    public byte getMask(){
        return mask;
    }

    public String getFieldName(){
        return fieldName;
    }


    public void setValue(String value) throws SPException {
        //String value = "101";

        try{
            binaryValue = Integer.valueOf(value, 2);
        }catch (NumberFormatException nfe){
            throw new SPException("Invalid binary value: " + value);
        }
        if (value.length() != length) {
            throw new SPException("In SPRegisterField lenght of bits in value " + value + " should be " + length + " for the specified mask " + Utility.intToBinaryConversion(mask));
        }

        if (position < 0 || position > 7){
            throw new SPException("position must be >= 0 & <= 7 in SPRegisterField");
        }
        if (length > 8 || (length + position) > 8){
            throw new SPException("position must be >= 0 & <= 7 or (position + length) must be <= 8 in SPRegisterField");
        }

        binaryValue = binaryValue << position;
        binCoded = Utility.intToBinaryConversion(binaryValue);
        hexCoded = Utility.intToHexConversion((byte)binaryValue);

    }


    public String toString(){
        return"(" + fieldName + ", val: " + binCoded + ", mask: " + Utility.intToBinaryConversion(getMask()) + ")\n";
    }




    public static void main(String args[]) throws SPException {

        // INPUT
        SPRegisterField binaryField = new SPRegisterField("VCI", "000", (byte)0b11100000);
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());

        binaryField.setValue("000");
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());
        binaryField.setValue("001");
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());
        binaryField.setValue("010");
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());
        binaryField.setValue("011");
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());
        binaryField.setValue("100");
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());
        binaryField.setValue("101");
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());
        binaryField.setValue("110");
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());
        binaryField.setValue("111");
        System.out.println(binaryField.getValueAsString());
        System.out.println(binaryField.getValueAsHexString());
        System.out.println(binaryField.getValue());

    }
}
