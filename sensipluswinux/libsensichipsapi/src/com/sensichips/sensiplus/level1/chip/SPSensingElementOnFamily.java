package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterSENSORS;

import java.io.Serializable;


public class SPSensingElementOnFamily implements Serializable {

    public static final long serialVersionUID = 42L;


    private SPSensingElement sensingElement;
    private String ID;
    private SPPort port;


    public SPMeasurementParameter getSPMeasurementParameter(SPConfiguration spConfiguration) throws SPException {

        if (sensingElement.getMeasureTechnique().equals("EIS") || sensingElement.getName().contains("FIGARO")) {
            SPMeasurementParameterEIS param = new SPMeasurementParameterEIS(spConfiguration);
            param.setContacts(sensingElement.getContacts());
            param.setRsense(sensingElement.getRsense());
            param.setDCBiasP(sensingElement.getDCBiasP());
            param.setDCBiasN(sensingElement.getDCBiasN());
            param.setFrequency(sensingElement.getFrequency()); // Per ottenere 78125
            param.setHarmonic(sensingElement.getHarmonic());
            param.setOutPort(port.getPortLabel());
            param.setInPort(port.getPortLabel());
            param.setInGain(sensingElement.getInGain());
            param.setOutGain(sensingElement.getOutGain());
            param.setModeVI(sensingElement.getModeVI());
            param.setMeasure(sensingElement.getMeasureType());
            param.setPhaseShiftQuadrants(sensingElement.getPhaseShiftMode(), sensingElement.getPhaseShift(), sensingElement.getIq());
            param.setFilter(sensingElement.getFilter());
            param.setSequentialMode(sensingElement.isSequentialMode());
            param.setBurstMode(sensingElement.isBurstMode());
            param.setFillBufferBeforeStart(sensingElement.isFillBufferBeforeStart());
            return param;

        }  else if (sensingElement.getName().equals("ONCHIP_TEMPERATURE")){
            SPMeasurementParameterSENSORS spMeasurementParameterSENSORS = new SPMeasurementParameterSENSORS(spConfiguration,sensingElement.getName());
            spMeasurementParameterSENSORS.setSensorName(ID);
            spMeasurementParameterSENSORS.setFilter(sensingElement.getFilter());
            spMeasurementParameterSENSORS.setBurstMode(sensingElement.isBurstMode());
            spMeasurementParameterSENSORS.setFillBufferBeforeStart(sensingElement.isFillBufferBeforeStart());
            spMeasurementParameterSENSORS.setPort(port.getPortLabel());
            return spMeasurementParameterSENSORS;

        } else {
            throw new SPException("Some error occurred on SPSensingElementOnFamily on: " + sensingElement.getName());
        }
    }


    public void setID(String ID){
        this.ID = ID;
    }

    public String getID(){
        return this.ID;
    }


    public SPSensingElement getSensingElement() {
        return sensingElement;
    }

    public void setSensingElement(SPSensingElement sensingElement) {
        this.sensingElement = sensingElement;
    }

    public SPPort getPort() {
        return port;
    }

    public void setPort(SPPort port) {
        this.port = port;
    }





    public String toString(){
        String out = "";
        out += sensingElement;
        out += "\t\t\tport: " + port + "\n";

        return out;
    }

}


