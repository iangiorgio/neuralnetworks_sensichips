package com.sensichips.sensiplus.level1.chip;

import java.io.Serializable;

/**
 * Created by Marco Ferdinandi on 27/02/2017.
 */
public class SPAnalyte implements Serializable {

    public static final long serialVersionUID = 42L;
    private String name;
    private String measureUnit;

    public SPAnalyte(){
        this.name = null;
        this.measureUnit = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }


}
