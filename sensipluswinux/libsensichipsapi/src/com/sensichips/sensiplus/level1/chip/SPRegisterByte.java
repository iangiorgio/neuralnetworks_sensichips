package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.SPException;

import java.util.ArrayList;
import java.util.Hashtable;

public class SPRegisterByte {

    private Hashtable<String, SPRegisterField> fieldTable = new Hashtable<>();
    private int completenessMask;
    private int overlapMask;

    private int registerValue = 0;


    private String binCoded;
    private String hexCoded;

    public void addSPRegisterField(SPRegisterField spRegisterField) throws SPException {
        if (spRegisterField == null){
            throw new SPException("SPRegisterField should be not null");
        }

        completenessMask = (byte) (completenessMask | spRegisterField.getMask());
        overlapMask = overlapMask + spRegisterField.getMask();

        fieldTable.put(spRegisterField.getFieldName(), spRegisterField);


        updateFields();
    }

    public String toString(){
        String out = "";
        ArrayList<SPRegisterField> arr = new ArrayList(fieldTable.values());
        for(SPRegisterField item: arr){
            out += item;
        }
        return out;
    }

    private void updateFields(){
        registerValue = 0;
        ArrayList<SPRegisterField> arr = new ArrayList(fieldTable.values());

        for(SPRegisterField item: arr){
            registerValue = registerValue | item.getValue();
        }

        binCoded = Utility.intToBinaryConversion(registerValue);
        hexCoded = Utility.intToHexConversion((byte)registerValue);

    }

    public void updateRegisterField(SPRegisterField spRegisterField) throws SPException {
        // Set a previously added field
        fieldTable.replace(spRegisterField.getFieldName(), spRegisterField);

        updateFields();
    }

    private void verifyFields() throws SPException {
        if ((completenessMask & 255) != 255){
            throw new SPException("All field should be covered by SPRegisterField. Current coverage is: " + Utility.intToBinaryConversion(completenessMask));
        }
        if ((overlapMask & 255) != 255){
            throw new SPException("Some overlap between field occurred. Sum is: " + Utility.intToBinaryConversion(overlapMask) +", correct value should be: " + Utility.intToBinaryConversion(255));
        }

    }


    public byte getValue() throws SPException {
        verifyFields();
        return (byte) registerValue;

    }


    public String getValueAsString() throws SPException{
        verifyFields();
        return binCoded;
    }

    public String getValueAsHexString() throws SPException{
        verifyFields();
        return hexCoded;
    }

    public void setValueAsString(String binCoded) throws SPException {
        throw new SPException("Not yet implemented.");
    }

    public void setValueAsHexString(String hexCoded) throws SPException {
        throw new SPException("Not yet implemented");
    }


    public static void main(String args[]) throws SPException {
        SPRegisterByte spRegisterByte = new SPRegisterByte();
        SPRegisterField spRegisterField = new SPRegisterField("FIELD1", "111", (byte)0b11100000);
        spRegisterByte.addSPRegisterField(spRegisterField);
        spRegisterField = new SPRegisterField("FIELD2", "101", (byte)0b00011100);
        spRegisterByte.addSPRegisterField(spRegisterField);
        spRegisterField = new SPRegisterField("FIELD3", "01", (byte)0b00000011);
        spRegisterByte.addSPRegisterField(spRegisterField);

        spRegisterField.setValue("10");
        //spRegisterByte.updateRegisterField(spRegisterField);


        String value = spRegisterByte.getValueAsHexString();
        System.out.println(value);
        value = spRegisterByte.getValueAsString();
        System.out.println(value);

        System.out.println(spRegisterByte);


    }

}
