package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco on 30/01/2017.
 */

public class SPChip implements Serializable {

    public static final long serialVersionUID = 42L;

    private static int NUM_BYTE_SN = 5;
    private static int NUM_BYTE_I2C = 1;

    private SPFamily spFamily;


    private String serialNumber;
    private String addressReadyToSend;
    private String[] addressTokens;
    private String I2CAddress;
    private String oscTrimOverride;


    public static int CHIP_TYPE_UNICAST = 0;
    public static int CHIP_TYPE_BROADCAST = 1;
    public static int CHIP_TYPE_BROADCAST_SLOW = 2;
    public static int CHIP_TYPE_MULTICAST = 3;

    private int chip_type;

    private boolean active = true;

    private Float lastVBandGap;


    private List<SPSensingElementOnChip> sensingElementOnChipList;

    public SPChip(){
        chip_type = CHIP_TYPE_UNICAST;
        this.spFamily = null;
        this.serialNumber = null;
        sensingElementOnChipList = new ArrayList<SPSensingElementOnChip>();
    }

    public SPChip(int chip_type) throws SPProtocolException {
        if (chip_type != CHIP_TYPE_MULTICAST && chip_type != CHIP_TYPE_BROADCAST && chip_type != CHIP_TYPE_BROADCAST_SLOW && chip_type != CHIP_TYPE_UNICAST){
            throw new SPProtocolException("SPChip construtore: type of chip unavailable!");
        }
        this.chip_type = chip_type;
    }

    public String getOscTrimOverride() {
        return oscTrimOverride;
    }

    public void setOscTrimOverride(String oscTrimOverride) {
        this.oscTrimOverride = oscTrimOverride;
    }

    public Float getLastVBandGap() {
        return lastVBandGap;
    }

    public void setLastVBandGap(Float lastVBandGap) {
        this.lastVBandGap = lastVBandGap;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setI2CAddress(String I2CAddress) throws SPProtocolException {

        if (I2CAddress == null){
            throw new SPProtocolException("I2CAddress cannot be null!");
        }
        I2CAddress = I2CAddress.toUpperCase();
        if (!SNVerifier(I2CAddress, NUM_BYTE_I2C)){
            throw new SPProtocolException("Wrong I2C address: " + this.I2CAddress);
        }
        this.I2CAddress = I2CAddress;
    }

    public String getAddress(int commMod, String protocolName) throws SPProtocolException{
        if (commMod != SPProtocol.FULL_ADDRESS && commMod != SPProtocol.SHORT_ADDRESS && commMod != SPProtocol.NO_ADDRESS){
            throw new SPProtocolException("Wrong communication mode in getAddress(..) of SPChip with id: " + getSerialNumber());
        }
        if (commMod ==  SPProtocol.FULL_ADDRESS){
            if (protocolName.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS])){
                return getFullAddress();
            }
            throw new SPProtocolException("Address mode : " + SPProtocol.ADDRESSING_TYPES[commMod] + "not available for: " + protocolName + " protocol!!");
        } else if (commMod == SPProtocol.NO_ADDRESS) {
            return "";
        } else if (commMod == SPProtocol.SHORT_ADDRESS) {
            if (protocolName.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS])){
                return getShortAddress();
            } else if (protocolName.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C])){
                return getI2CAddress();
            }
            throw new SPProtocolException("Address mode : " + SPProtocol.ADDRESSING_TYPES[commMod] + "not available for: " + protocolName + " protocol!!");
        } else {
            throw new SPProtocolException("SPChip.getAddress(): communication mode requested is not avaiable!");
        }
    }




    private String getFullAddress() throws SPProtocolException {
        //String id = getSerialNumber();
        //return spFamily.getId() + " " + addressReadyToSend;
        // TODO: reinserire familyID
        return "00 " + addressReadyToSend;
    }

    private String getShortAddress() throws SPProtocolException {
        //String id = getSerialNumber();
        int index = 0;
        if (chip_type == CHIP_TYPE_UNICAST){
            index = 2;
        } else if (chip_type == CHIP_TYPE_MULTICAST){
            index = 1;
        }

        return addressTokens[addressTokens.length - index];

    }

    private String getI2CAddress(){
        return I2CAddress;
    }


    public SPFamily getSpFamily() {
        return spFamily;
    }

    public void setSpFamily(SPFamily spFamily) throws SPProtocolException {
        if (spFamily == null){
            throw new SPProtocolException("SPFamily in SPChip is mandatory and cannot be null!");
        }
        this.spFamily = spFamily;
    }

    /**
     * SN format: 0XAABBCCDDEEFF
     * @return Chip serial number
     * @throws SPProtocolException
     */
    public String getSerialNumber() throws SPProtocolException {
        if (spFamily == null || spFamily.getId() == null || serialNumber == null){
            throw new SPProtocolException("Family id or serial number not correctly specified");
        }
        return serialNumber;
    }


    /**
     * Accepted SN format: 0XAABBCCDDEEFF
     * @return Chip serial number
     * @throws SPProtocolException
     */
    public void setSerialNumber(String serialNumber) throws SPProtocolException {
        serialNumber = serialNumber.toUpperCase();
        if (!SNVerifier(serialNumber, NUM_BYTE_SN)){
            throw new SPProtocolException("Wrong serialNumber format: " + serialNumber);
        }
        this.serialNumber = serialNumber.toUpperCase();

    }

    public List<SPSensingElementOnChip> getSensingElementOnChipList() {
        return sensingElementOnChipList;
    }

    public void setSensingElementOnChipList(List<SPSensingElementOnChip> sensingElementOnChipList) {
        this.sensingElementOnChipList = sensingElementOnChipList;
    }





    protected boolean SNVerifier(String ID, int sn_len){
        String hexAppo = "";
        if (!ID.startsWith("0X")){
            return false;
        }
        ID = ID.replace("0X","");
        if (ID.length() != sn_len * 2){
            return false;
        }
        int i = 0, index = 0;
        if (sn_len == NUM_BYTE_SN){
            addressReadyToSend = "";
            addressTokens = new String[sn_len];
        }
        while(i < ID.length()){
            try {
                hexAppo = ID.substring(i, i + 2);
                if (sn_len == NUM_BYTE_SN) {
                    addressTokens[index] = hexAppo;
                    addressReadyToSend = addressReadyToSend + " " + addressTokens[index];
                    index++;
                }
                Integer.parseInt(hexAppo, 16); // To verify if is an hexadecimal value
            } catch (Exception e){
                //System.out.println(e.getMessage());
                addressReadyToSend = "";
                return false;
            }
            i += 2;
        }
        if (sn_len == NUM_BYTE_SN) {
            addressReadyToSend = addressReadyToSend.trim();
        }

        return true;
    }


    public String toString() {
        String out = "";
        out += "\t\tserialNumber:   " + serialNumber + "\n";
        out += "\t\tspFamily:       \n" + spFamily + "\n";
        for(SPSensingElementOnChip item: sensingElementOnChipList){
            out += item;
        }
        return out;
    }




}
