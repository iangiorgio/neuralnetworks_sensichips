package com.sensichips.sensiplus.level1.chip;

import java.io.Serializable;

/**
 * Created by Marco Ferdinandi on 27/02/2017.
 */
public class SPSensingElementOnChip implements Serializable {

    public static final long serialVersionUID = 42L;


    private SPSensingElementOnFamily sensingElementOnFamily;
    private SPCalibrationParameters calibrationParameters;

    private boolean autoScaleActive;

    public SPSensingElementOnFamily getSensingElementOnFamily() {
        return sensingElementOnFamily;
    }

    public void setSensingElementOnFamily(SPSensingElementOnFamily sensingElementOnFamily) {
        this.sensingElementOnFamily = sensingElementOnFamily;
    }

    public SPCalibrationParameters getCalibrationParameters() {
        return calibrationParameters;
    }

    public void setCalibrationParameters(SPCalibrationParameters calibrationParameters) {
        this.calibrationParameters = calibrationParameters;
    }


    public boolean isAutoScaleActive() {
        return autoScaleActive;
    }

    public void setAutoScaleActive(boolean autoScaleActive) {
        this.autoScaleActive = autoScaleActive;
    }


    public String toString(){
        String out = "";
        out += sensingElementOnFamily;
        out += calibrationParameters;
        return out;
    }

}
