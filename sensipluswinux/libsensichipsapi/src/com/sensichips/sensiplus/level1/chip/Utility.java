package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.level2.SendInstructionInOut;

import java.io.Serializable;

public class Utility implements Serializable {


    public static String intToBinaryConversion(int value){
        byte maskForCoding = (byte)128;
        String coded = "";
        for(int i = 0; i < 8; i++){
            coded += (value & maskForCoding) != (byte) 0 ? "1":"0";
            value = (byte)((int)value << 1);
        }
        return coded;
    }

    public static String intToHexConversion(byte value) {
        return String.format("%02X ", value);
    }

    private static final int mascheraLOW = 255;
    private static final int maschera = 65535;


    /**
     *
     * Example:
     * data: (row different chips, columns num of bytes for each integer to generate
     * [0xF2 0x12]
     * [0xF2 0x32]
     * [0xF2 0x52]
     *
     * numOfInt = 2
     *
     * output:
     * 4850     (ottenuto da 0x12F2)
     * 13042    (ottenuto da 0x32F2)
     * 21234    (ottenuto da 0x52F2)
     *
     * @param data bytes read from fifo
     * @param numOfBytePerRow num of integer to generate
     * @param ref to manage log
     * @return integer obtained from conversion
     */

    public static int[][] conversionFromByteToInt(byte data[][], int numOfChips, int numOfBytePerRow, SendInstructionInOut ref){
        int [][] output = new int[numOfChips][];
        for(int j = 0; j < numOfChips; j++){
            output[j] = new int[numOfBytePerRow / 2];
            for (int i = 0; i < numOfBytePerRow / 2; i++) {
                int val = conversionFromByteToIntSingle(data[j][i * 2 + 1], data[j][i * 2]);
                //int x = ((int) data[j][i * 2 + 1]) << 8;   // Prolunga il segno su 32 bit (cast a int)
                //int y = data[j][i * 2];
                //y = y & mascheraLOW;                       // Server per evitare di prolungare il segno LSB
                output[j][0] = output[j][0] | val; // & maschera;

                if (ref != null){
                    ref.log += "int values: " + output[j][i] + ", ";
                }
            }
        }
        return output;

    }

    public static int conversionFromByteToIntSingle(byte b1, byte b2){
        int x = ((int) b1) << 8;   // Prolunga il segno su 32 bit (cast a int)
        int y = b2;
        y = y & mascheraLOW;                       // Server per evitare di prolungare il segno LSB
        return (x | y);
    }



    public static String[] responseTokenizer(String message){
        String cleaned = message.replace("[", "").replace("]", "").toUpperCase().replace("0x", "").replace("0X", "").trim();
        return cleaned.split(" ");
    }

    public static byte[] responseTokenizerByte(String message){
        String[] tokens = responseTokenizer(message);
        byte[] output = new byte[tokens.length];
        for(int i = 0; i < tokens.length; i++){
            output[i] = (byte) Integer.parseInt(tokens[i], 16);
        }
        return output;
    }

    public static void main(String args[]){
        byte[][] data = new byte[2][2];

        data[0][0] = 0x00;
        data[0][1] = 0x00;

        data[1][0] = (byte)0x00;
        data[1][1] = (byte)0x80;

        int[][] output = conversionFromByteToInt(data, 2,1, null);

        for(int i = 0; i < output.length; i++){
            for(int j = 0; j < output[i].length; j++){
                System.out.print(output[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("*************************");

        byte LSB = (byte) 0xFF;
        byte MSB = (byte) 0x7F;

        int x = conversionFromByteToIntSingle(MSB, LSB);
        System.out.println(x);

    }



}
