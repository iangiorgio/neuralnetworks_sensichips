package com.sensichips.sensiplus.level1.chip;


import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;

/**
 * Created by mario on 20/07/17.
 */
public class SPPort implements Serializable {

    public static final long serialVersionUID = 42L;

    private String portLabel;
    private String portValue;
    private Boolean isInternal = false;
    private Boolean noPort = false;
    private static int MAX_INTERNAL_INDEX = 12;

    public static final Integer PORT_EXT1_1 = 16;
    public static final Integer PORT_EXT2_1 = 17;
    public static final Integer PORT_EXT3_1 = 18;

    public static final String[] portLabels = new String[]{"PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3","PORT_EXT1_1", "PORT_EXT2_1", "PORT_EXT3_1",  "PORT_TEMPERATURE", "PORT_VOLTAGE", "PORT_LIGHT",   "PORT_DARK", "PORT_NA"};
    //														"0x00",  "0x01",  "0x02",  "0x03",  "0x04",  "0x05" , "0x06",  "0x07",  "0x08",  "0x09",  "0x0A",   "0x0B",   "0x1C",    "0x1D",      "0x1E",      "0x1F",      "0x1D",      "0x1E",      "0x1F",           "0x0D",             "0x0C",         "0x0E",         "0x0F",         "0x1A};
    public static final String[] portValues = new String[]{"00000", "00001", "00010", "00011", "00100", "00101", "00110", "00111", "01000", "01001", "01010",  "01011",  "11100",   "11101",     "11110",     "11111",      "11101",     "11110",     "11111",          "01101",            "01100",        "01110",        "01111",        "11010"};


    //public static final String[] portLabels = new String[]{"PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3"};
    //public static final String[] portValues = new String[]{"00000", "00001", "00010", "00011", "00100", "00101", "00110", "00111", "01000", "01001", "01010", "01011", "11100", "11101", "11110", "11111"};

    public static final String NOPORT = "NOPORT";


    public SPPort(String portLabel) throws SPProtocolException {
        boolean portFound = false;
        isInternal = false;
        noPort = portLabel.equals(NOPORT);

        if (portLabel != null && !noPort){

            int i = 0;
            while(i < portLabels.length && !portFound){
                if (!(portFound = portLabels[i].equals(portLabel))){
                    i++;
                }
            }
            if (portFound){
                portValue = portValues[i];
                isInternal = i < MAX_INTERNAL_INDEX;
            }
        }

        if (noPort || portFound){
            this.portLabel = portLabel;
        } else {
            String message = "Wrong portLabel name: " + portLabel + "\nAvailable ports: ";
            for(int i = 0; i < portLabels.length; i++){
                message += portLabels[i] + "; ";
            }
            message += "\n";
            throw new SPProtocolException(message);
        }
    }

    public boolean isInternal() {
        return isInternal;
    }


    public String getPortLabel(){
        return portLabel;
    }

    public String getPortValue(){
        return portValue;
    }

    public String toString(){
        return "PortLabel: " + portLabel + ", PortValue: " + portValue;
    }

    public static void main(String args[]) throws SPProtocolException{
        SPPort port = new SPPort("PORT_DARK");
        System.out.println(port);
    }


}
