package com.sensichips.sensiplus.level1.chip;


import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;

/**
 * Created by mario on 01/03/17.
 */
public class SPRegisterListItem implements Serializable {

    public static final long serialVersionUID = 42L;

    private String name = "";
    private String address = "";

    public SPRegisterListItem(String name, String address) {
        this.name = name.toUpperCase();
        this.address = address.toUpperCase();
    }

    public void setAddress(String address) throws SPProtocolException {
        if (SPRegisterDump.HexVerifier(address)){
            throw new SPProtocolException("Wrong SPRegisterListItem: " + address);
        }
        this.address = address;
    }

    public String getAddress(){
        return address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
