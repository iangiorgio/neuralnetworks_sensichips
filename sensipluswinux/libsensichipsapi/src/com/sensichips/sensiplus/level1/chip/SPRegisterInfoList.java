package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by mario on 01/03/17.
 */
public class SPRegisterInfoList implements Serializable {

    public static final long serialVersionUID = 42L;


    //private static Hashtable<String, SPRegisterInfo> table = null;
    private static Hashtable<String, Hashtable<String, SPRegisterInfo>> versions = new Hashtable<>();


    public String[] getCommandSuggestions(String HW_VERSION) throws SPProtocolException{
        Hashtable<String, SPRegisterInfo> table = versions.get(HW_VERSION);

        if (table == null){
            table = createTable(HW_VERSION);
            versions.put(HW_VERSION, table);
        }
        ArrayList<String> registers = new ArrayList<String>();
        registers.addAll(table.keySet());
        int riemp = registers.size();
        ArrayList<String> output = new ArrayList<String>();
        output.add("READ ");
        output.add("WRITE ");
        for(int i = 0; i < riemp; i++){
            output.add("READ " + registers.get(i) + " ");
            output.add("WRITE " + registers.get(i) + " ");
        }
        for(int i = 0; i < riemp; i++){
            output.add("READ " + registers.get(i) + " M 2");
            output.add("WRITE " + registers.get(i) + " M 0x");
        }
        for(int i = 0; i < riemp; i++){
            output.add("READ " + registers.get(i) + " S 1");
            output.add("WRITE " + registers.get(i) + " S 0x");
        }
        return output.toArray(new String[0]);
    }


    public static SPRegisterInfo getSPRegister(String HW_VERSION, String name) throws SPProtocolException {
        Hashtable<String, SPRegisterInfo> table = versions.get(HW_VERSION);
        if (table == null){
            table = createTable(HW_VERSION);
            versions.put(HW_VERSION, table);
        }
        return table.get(name);
    }


    private static void putRegister(Hashtable<String, SPRegisterInfo> table, SPRegisterInfo spRegisterInfo){
        table.put(spRegisterInfo.getName(), spRegisterInfo);
    }

    private static Hashtable<String, SPRegisterInfo> createTable(String HW_VERSION) throws SPProtocolException {
        Hashtable<String, SPRegisterInfo> table = versions.get(HW_VERSION);

        if (table == null && (HW_VERSION.equals(SPFamily.RUN4) || HW_VERSION.equals(SPFamily.RUN5))) {
            table = new Hashtable<>();
            putRegister(table, new SPRegisterInfo("STATUS", "0x00"));
            putRegister(table, new SPRegisterInfo("COMMAND", "0x00"));
            putRegister(table, new SPRegisterInfo("INT_CONFIG", "0x02"));
            putRegister(table, new SPRegisterInfo("ANA_CONFIG", "0x04"));
            putRegister(table, new SPRegisterInfo("DIG_CONFIG", "0x06"));
            putRegister(table, new SPRegisterInfo("CHA_DIVIDER", "0x08"));
            putRegister(table, new SPRegisterInfo("CHA_CONFIG", "0x0A"));
            putRegister(table, new SPRegisterInfo("CHA_LIMIT", "0x0C"));
            putRegister(table, new SPRegisterInfo("CHA_CONDIT", "0x0E"));
            putRegister(table, new SPRegisterInfo("CHA_FILTER", "0x10"));
            putRegister(table, new SPRegisterInfo("CHA_SELECT", "0x12"));
            putRegister(table, new SPRegisterInfo("US_BURST", "0x14"));
            putRegister(table, new SPRegisterInfo("US_TRANSMIT", "0x16"));
            putRegister(table, new SPRegisterInfo("RXCOUNTERL", "0x18"));
            putRegister(table, new SPRegisterInfo("US_PROBE", "0x18"));
            putRegister(table, new SPRegisterInfo("RXCOUNTERH", "0x1A"));
            putRegister(table, new SPRegisterInfo("US_RECEIVE", "0x1A"));
            putRegister(table, new SPRegisterInfo("DSS_DIVIDER", "0X1C"));
            putRegister(table, new SPRegisterInfo("DDS_FIFO", "0x1E"));
            putRegister(table, new SPRegisterInfo("DDS_CONFIG", "0x20"));
            putRegister(table, new SPRegisterInfo("DAS_CONFIG", "0x22"));
            putRegister(table, new SPRegisterInfo("DSS_SELECT", "0x24"));
            putRegister(table, new SPRegisterInfo("TIMERL", "0x26"));
            putRegister(table, new SPRegisterInfo("TIMER_DIVL", "0x26"));
            putRegister(table, new SPRegisterInfo("TIMERH", "0x28"));
            putRegister(table, new SPRegisterInfo("TIMER_DIVH", "0x28"));
            putRegister(table, new SPRegisterInfo("PPR_COUNTL", "0x2A"));
            putRegister(table, new SPRegisterInfo("PPR_THRESL", "0x2A"));
            putRegister(table, new SPRegisterInfo("PPR_COUNTH", "0x2C"));
            putRegister(table, new SPRegisterInfo("PPR_THRESH", "0x2C"));
            putRegister(table, new SPRegisterInfo("PPR_SELECT", "0x2E"));
            putRegister(table, new SPRegisterInfo("CHA_FIFOL", "0x30"));
            putRegister(table, new SPRegisterInfo("CHA_FIFOH", "0x32"));
            putRegister(table, new SPRegisterInfo("EFUSE_PROGR", "0x34"));
            putRegister(table, new SPRegisterInfo("SENSIBUS_SET", "0x36"));
            putRegister(table, new SPRegisterInfo("DEVICE_ID0", "0x38"));
            putRegister(table, new SPRegisterInfo("DEVICE_ID1", "0x3A"));
            putRegister(table, new SPRegisterInfo("DEVICE_ID2", "0x3C"));
            putRegister(table, new SPRegisterInfo("USER_EFUSE", "0x3E"));

        } else if (table == null && HW_VERSION.equals(SPFamily.RUN6)) {
            table = new Hashtable<>();
            putRegister(table, new SPRegisterInfo("STATUS", "0x00"));
            putRegister(table, new SPRegisterInfo("COMMAND", "0x00"));
            putRegister(table, new SPRegisterInfo("INT_CONFIG", "0x02"));
            putRegister(table, new SPRegisterInfo("ANA_CONFIG", "0x04"));
            putRegister(table, new SPRegisterInfo("DIG_CONFIG", "0x06"));
            putRegister(table, new SPRegisterInfo("CHA_DIVIDER", "0x08"));
            putRegister(table, new SPRegisterInfo("CHA_CONFIG", "0x0A"));
            putRegister(table, new SPRegisterInfo("CHA_LIMIT", "0x0C"));
            putRegister(table, new SPRegisterInfo("CHA_CONDIT", "0x0E"));
            putRegister(table, new SPRegisterInfo("CHA_PSHIFT", "0x10"));
            putRegister(table, new SPRegisterInfo("CHA_FILTER", "0x12"));
            putRegister(table, new SPRegisterInfo("CHA_SELECT", "0x14"));
            putRegister(table, new SPRegisterInfo("US_BURST", "0x16"));
            putRegister(table, new SPRegisterInfo("US_TRANSMIT", "0x18"));
            putRegister(table, new SPRegisterInfo("RXCOUNTERL", "0x1A"));
            putRegister(table, new SPRegisterInfo("US_PROBE", "0x1A"));
            putRegister(table, new SPRegisterInfo("RXCOUNTERH", "0x1C"));
            putRegister(table, new SPRegisterInfo("US_RECEIVE", "0X1C"));
            putRegister(table, new SPRegisterInfo("DSS_DIVIDER", "0x1E"));
            putRegister(table, new SPRegisterInfo("DDS_FIFO", "0x20"));
            putRegister(table, new SPRegisterInfo("DDS_CONFIG", "0x22"));
            putRegister(table, new SPRegisterInfo("DAS_CONFIG", "0x24"));
            putRegister(table, new SPRegisterInfo("DSS_SELECT", "0x26"));
            putRegister(table, new SPRegisterInfo("TIMERL", "0x28"));
            putRegister(table, new SPRegisterInfo("TIMER_DIVL", "0x28"));
            putRegister(table, new SPRegisterInfo("TIMERH", "0x2A"));
            putRegister(table, new SPRegisterInfo("TIMER_DIVH", "0x2A"));
            putRegister(table, new SPRegisterInfo("PPR_COUNTL", "0x2C"));
            putRegister(table, new SPRegisterInfo("PPR_THRESL", "0x2C"));
            putRegister(table, new SPRegisterInfo("PPR_COUNTH", "0x2E"));
            putRegister(table, new SPRegisterInfo("PPR_THRESH", "0x2E"));
            putRegister(table, new SPRegisterInfo("PPR_SELECT", "0x30"));
            putRegister(table, new SPRegisterInfo("CHA_FIFOL", "0x32"));
            putRegister(table, new SPRegisterInfo("CHA_FIFOH", "0x34"));
            putRegister(table, new SPRegisterInfo("SENSIBUS_SET", "0x36"));
            putRegister(table, new SPRegisterInfo("DEVICE_ID0", "0x38"));
            putRegister(table, new SPRegisterInfo("DEVICE_ID1", "0x3A"));
            putRegister(table, new SPRegisterInfo("DEVICE_ID2", "0x3C"));
            putRegister(table, new SPRegisterInfo("USER_EFUSE", "0x3E"));


            // Bank 2 of new registers
            putRegister(table, new SPRegisterInfo("GPIO", "0x02"));
            putRegister(table, new SPRegisterInfo("PORT_FLAG1", "0x04"));
            putRegister(table, new SPRegisterInfo("PORT_FLAG2", "0x06"));
            putRegister(table, new SPRegisterInfo("PORT_FLAG3", "0x08"));
            putRegister(table, new SPRegisterInfo("PORT_DELAY", "0x0A"));

        } else {
            throw new SPProtocolException("SPRegisterInfoList: register table for " + HW_VERSION + " version, is not available");
        }

        return table;
    }



}
