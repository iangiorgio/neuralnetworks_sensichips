package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.SPException;

import java.io.Serializable;
import java.util.Hashtable;

public class SPRegister implements Serializable {


    private SPRegisterInfo spRegisterInfo;
    private SPRegisterByte spRegisterByteLSB;
    private SPRegisterByte spRegisterByteMSB;


    public SPRegister(String HW_VERSION, String registerName) throws SPException {
        spRegisterInfo = SPRegisterInfoList.getSPRegister(HW_VERSION, registerName);
    }

    public SPRegisterByte getSpRegisterByteLSB() {
        return spRegisterByteLSB;
    }

    public void setSpRegisterByteLSB(SPRegisterByte spRegisterByteLSB) {
        this.spRegisterByteLSB = spRegisterByteLSB;
    }

    public SPRegisterByte getSpRegisterByteMSB() {
        return spRegisterByteMSB;
    }

    public void setSpRegisterByteMSB(SPRegisterByte spRegisterByteMSB) {
        this.spRegisterByteMSB = spRegisterByteMSB;
    }



}
