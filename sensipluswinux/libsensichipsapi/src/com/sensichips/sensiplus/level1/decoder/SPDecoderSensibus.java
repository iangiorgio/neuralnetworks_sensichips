package com.sensichips.sensiplus.level1.decoder;

import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.SPChip;

import java.util.ArrayList;


/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPDecoderSensibus extends SPDecoder {


    public SPDecoderSensibus() throws SPProtocolException {
        super();
    }

    @Override
    public void Instruction2String(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException {
          /* ******
          final decoding
		   ****** */

        String multiple = instruction.getMultiple();
        boolean write = instruction.isWrite();
        String register = instruction.getRegisterEx();
        ArrayList<String> dati = instruction.getDati();
        String command = "";
        String registerOp = "";

        String stringBinaryReg = Integer.toBinaryString(Integer.parseInt(register.substring(2), 16)); //conversion string - integer
        while (stringBinaryReg.length() < 8) {
            stringBinaryReg = "0" + stringBinaryReg;
        }



        if (!multiple.equalsIgnoreCase("M"))
            // bit = 1 if it's a multiple operation (0 if these is single)
            registerOp += "0";
        else
            registerOp += "1";

        if (write)
            // bit = 1 if it's a read operation (0 if these is send)
            registerOp += "0";
        else
            registerOp += "1";


        registerOp = stringBinaryReg.substring(2) + registerOp;


        String valueInt = Integer.toHexString(Integer.parseInt(registerOp, 2)).toUpperCase();

        if (valueInt.length() == 1)
            command += "0x0" + valueInt;

        else //valueInt.lenght==2
            command += "0x" + valueInt;


        command += " ";

        for (int i = 1; i < dati.size(); i++) { // DATI.SIZE(0) is mnemonic command (e.g. send int_config s 0x1234)
            command += dati.get(i);
            if (i < dati.size() - 1)
                command += " ";
        }


        instruction.setDecodedInstruction(command);
    }


}// end class SPDecoder





