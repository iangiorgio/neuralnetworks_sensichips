package com.sensichips.sensiplus.level1.decoder;

import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.SPChip;

import java.util.ArrayList;


/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPDecoderSPI extends SPDecoder {

    public SPDecoderSPI() throws SPProtocolException {
        super();
    }


    @Override
    public void Instruction2String(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException {

        String multiple = instruction.getMultiple();
        boolean write = instruction.isWrite();
        String register = instruction.getRegisterEx();
        ArrayList<String> dati = instruction.getDati();
        String command = "";

        int binary = Integer.parseInt(register.substring(2), 16); //conversion string - integer
        binary = binary << 2; // 2 bit shift to left

        if (multiple.equalsIgnoreCase("M")) {
            binary = binary + 2;
        } // second-last bit = 1 if there is multiple in instruction string

        if (!write) {
            binary = binary + 1;
        } //last bit = 1 if it's a read operation (0 if these is send)

        String valueInt = Integer.toHexString(binary).toUpperCase();

        if (valueInt.length() == 1)
            command += "0x0" + valueInt;

        else //valueInt.lenght==2
            command += "0x" + valueInt;

        command += " ";
        for (int i = 1; i < dati.size(); i++) { // DATI.SIZE(0) is mnemonic command (e.g. send int_config s 0x1234)
            command += dati.get(i);
            if (i < dati.size() - 1)
                command += " ";
        }

        instruction.setDecodedInstruction(command);
    }

}// end class SPDecoderSPI





