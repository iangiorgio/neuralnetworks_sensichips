package com.sensichips.sensiplus.level0;

import com.sensichips.sensiplus.SPException;

public interface SPEnvironmentalSensors {

    void startMeasurement();

    void stopMeasurement();

    float getTemp() throws SPException;

    float getHum() throws SPException;

    boolean isAvailable();
}
