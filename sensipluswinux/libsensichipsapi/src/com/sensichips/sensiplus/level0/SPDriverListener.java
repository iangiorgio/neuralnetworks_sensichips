package com.sensichips.sensiplus.level0;


/**
 * The listener interface for receiving SPDriver events. The class that is interested in processing an SPDriver
 * event implements this interface, and the object created with that class is registered with the constructor.
 *
 *
 * Property: Sensichips s.r.l.
 *
 * @author: Mario Molinara, mario.molinara@sensichips.com, m.molinara@unicas.it
 */
public interface SPDriverListener {
    /**
     * When the driver complete the connection phase this method is invoked.
     *
     */
    void connectionUp();

    // STRING WHICH CAN BE USED FOR TO SHOW INFORMATION MESSAGE (ex. 'device connected', etc.)
    void message(String m);

    // notify a usb detach
    void connectionDown();

    void interrupt(byte interruptValue);

    void fatalError(String message);

}
