package com.sensichips.sensiplus.level0.drivermanager;

import com.sensichips.sensiplus.level0.SPDriver;

import java.util.Hashtable;

/**
 * Created by mario on 20/07/17.
 */
public class SPDriverManager {

    private static Hashtable<String, SPDriver> table = null;

    public static void add(SPDriver driver){
        if (table == null){
            table = new Hashtable<>();
        }
        SPDriver sp = table.get(driver.getDriverKey());
        if (sp == null){
            table.put(driver.getDriverKey(), driver);
            System.out.println("Registered driver: " + driver.getDriverKey());
        } else {
            //System.out.println("Driver: " + sp.getDriverKey() + " already exists.");
        }
    }

    public static SPDriver get(String driverKey){
        return table.get(driverKey);
    }

    public static void resetSPDriverManager(){
        table = null;
    }


}
