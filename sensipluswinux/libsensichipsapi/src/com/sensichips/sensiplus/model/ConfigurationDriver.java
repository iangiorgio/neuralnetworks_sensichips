//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ConfigurationDriver complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ConfigurationDriver">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DRIVER_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PARAM1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PARAM2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
//@XmlAccessorType(XmlAccessType.FIELD)

@XmlType( propOrder = {
    "driver",
    "param1",
    "param2"
})
public class ConfigurationDriver {
    protected String drivername;
    protected String param1;
    protected String param2;


    @XmlElement(name = "DRIVER_NAME", required = true)
    public String getDriver() {
        return drivername;
    }

  //  public StringProperty driverProperty() {
   //     return drivername;
  //  }

    public void setDriver(String drivername) {
        this.drivername=drivername;
    }


    /**
     * Recupera il valore della propriet� drivername.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */

    /**
     * Imposta il valore della propriet� drivername.
     * 
     *
     *     allowed object is
     *     {@link String }
     *     
     */

    @XmlElement(name = "PARAM1", required = true)
    /**
     * Recupera il valore della propriet� param1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParam1() {
        return param1;
    }

    /**
     * Imposta il valore della propriet� param1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParam1(String value) {
        this.param1=value;
    }

    @XmlElement(name = "PARAM2", required = true)
    /**
     * Recupera il valore della propriet� param2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParam2() {
        return param2;
    }

    /**
     * Imposta il valore della propriet� param2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParam2(String value) {
        this.param2=value;
    }

}
