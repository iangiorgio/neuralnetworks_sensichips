//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java per Chip complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Chip">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="spFamily" type="{}Family" minOccurs="0"/>
 *         &lt;element name="FAMILY_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SERIAL_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="I2C_ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OSC_TRIM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SENSING_ELEMENT_ONCHIP" type="{}SensingElementOnChip" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

@XmlType(name = "Chip", propOrder = {
        "spFamily_idSPFamily","idSPChip","CAddress","seOnChipList",
})



public class Chip {

    protected StringProperty spFamily_idSPFamily;
    protected StringProperty idSPChip;
    protected StringProperty cAddress;

    protected List<SensingElementOnChip> sensingelementonchip;

    private String oldidSPChip = "";
    private String oldspFamily_idSPFamily = null;


    public Chip() {
        this("", "","");

    }

    public Chip(String idSPChip, String spFamily_idSPFamily, String cAddress) {
        this.idSPChip = new SimpleStringProperty(idSPChip);
        this.spFamily_idSPFamily = new SimpleStringProperty(spFamily_idSPFamily);
        this.cAddress= new SimpleStringProperty(cAddress);

    }


    public String getOldidSPChip() {
        return oldidSPChip;
    }

    public void setOldidSPChip() {
        this.oldidSPChip = this.idSPChip.get();
    }

    public String getOldspFamily_idSPFamily() {
        return oldspFamily_idSPFamily;
    }

    public void setOldspFamily_idSPFamily() {
        this.oldspFamily_idSPFamily = this.spFamily_idSPFamily.get();
    }


    //@XmlTransient
    //AL POSTO DI XmlTransient METTO L'XmlElement E IN QUESTO MODO MI COMPARE ANCHE L'ELEMENTO <FAMILY_ID> IN CHIP

    @XmlElement (name ="FAMILY_ID")
    public String getSpFamily_idSPFamily() {
        return spFamily_idSPFamily.get();
    }

    public StringProperty spFamily_idSPFamilyProperty() {
        return spFamily_idSPFamily;
    }

    public void setSpFamily_idSPFamily(String spFamily_idSPFamily) {
        this.spFamily_idSPFamily.set(spFamily_idSPFamily);
    }


    @XmlElement(name = "SERIAL_NUMBER") //PRIMA ERA "idCHIP"
    public String getIdSPChip() {
        return idSPChip.get();
    }

    public StringProperty idSPChipProperty() {
        return idSPChip;
    }

    public void setIdSPChip(String idSPChip) {
        this.idSPChip.set(idSPChip);
    }

  //AGGIUNGO L'ELEMENTO 12C_ADDRESS
    @XmlElement (name="I2C_ADDRESS")
    public String getCAddress(){
       return cAddress.get();
     }

     public StringProperty cAddress(){return cAddress;}

    public void setCAddress(String cAddress){
        this.cAddress.set(cAddress);
    }


    /**
     * Gets the value of the sensingelementonchip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sensingelementonchip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSENSINGELEMENTONCHIP().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SensingElementOnChip }
     * 
     * 
     */


    @XmlElement(name = "SENSING_ELEMENT_ONCHIP", required = true)
    public List<SensingElementOnChip> getSeOnChipList(){
        if (sensingelementonchip == null) {
            sensingelementonchip = new ArrayList<SensingElementOnChip>();
        }
        return this.sensingelementonchip;
    }
    public void setSeOnChipList(List<SensingElementOnChip> seOnChipList) {
        this.sensingelementonchip = seOnChipList;
    }

}
