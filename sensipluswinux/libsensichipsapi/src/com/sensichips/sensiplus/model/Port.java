package com.sensichips.sensiplus.model;

import javafx.beans.property.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"seAssociated" ,"seNameAssociated","name" ,/*"internal"*/})
public class Port {

    private IntegerProperty idSPPort;
    private BooleanProperty internal;
    private StringProperty namePorta;
    private StringProperty seAssociated = new SimpleStringProperty("");
    private StringProperty seNameAssociated= new SimpleStringProperty(""); //AGGIUNTO

    public Port() {
        this(Integer.MIN_VALUE, false, "");
    }

    public Port(Integer idSPPort, Boolean internal, String namePorta) {
        this.idSPPort = new SimpleIntegerProperty(idSPPort);
        this.internal = new SimpleBooleanProperty(internal);
        this.namePorta = new SimpleStringProperty(namePorta);
    }

    public Integer getIdSPPort() {
        return idSPPort.get();
    }

    public IntegerProperty idSPPortProperty() {
        return idSPPort;
    }

    public void setIdSPPort(int idSPPort) {
        this.idSPPort.set(idSPPort);
    }

    //@XmlElement(name = "INTERNAL", required = true)
    @XmlTransient //PER NON CONSIDERARE <INTERNAL> NELL'XML UTILIZZO XMLTRANSIENT E TOLGO internal DAL propOrder!!!
    public boolean isInternal() {
        return internal.get();
    }

    public BooleanProperty internalProperty() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal.set(internal);
    }

    @XmlElement(name = "SENSING_ELEMENT_PORT", required = true)
    public String getName() {
        return namePorta.get();
    }

    public StringProperty nameProperty() {
        return namePorta;
    }

    public void setName(String name) {
        this.namePorta.set(name);
    }

    @XmlElement(name = "SENSING_ELEMENT_ID", required = true)
    public String getSeAssociated() {
        return seAssociated.get();
    }


    public StringProperty seAssociatedProperty() {
        return seAssociated;
    }

    public void setSeAssociated(String seAssociated) {
        this.seAssociated.set(seAssociated);
    }

//DEVO AGGIUNGERE ANCHE L'ELEMENTO SENSING_ELEMENT_NAME ALL'XML
 @XmlElement(name = "SENSING_ELEMENT_NAME", required = true)

    public String getSeNameAssociated() {
        return seNameAssociated.get();
    }
    public StringProperty seNameAssociatedProperty() {
        return seNameAssociated;
    }

    public void setSeNameAssociated(String seNameAssociated) {
        this.seNameAssociated.set(seNameAssociated);
    }
}
