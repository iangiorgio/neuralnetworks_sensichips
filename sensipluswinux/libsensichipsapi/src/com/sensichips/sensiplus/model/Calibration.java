package com.sensichips.sensiplus.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Calibration {

    private final StringProperty name;
    private final IntegerProperty idSPCalibration;

    public Calibration(){
        this("", Integer.MIN_VALUE);
    }

    public Calibration(String name, Integer idSPCalibration){
        this.name = new SimpleStringProperty(name);
        this.idSPCalibration = new SimpleIntegerProperty(idSPCalibration);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public Integer getIdSPCalibration() {
        return idSPCalibration.get();
    }

    public IntegerProperty idSPCalibrationProperty() {
        return idSPCalibration;
    }

    public void setIdSPCalibration(int idSPCalibration) {
        this.idSPCalibration.set(idSPCalibration);
    }

    @Override
    public String toString() {
        return  idSPCalibration.getValue().toString() + ", " + name.get();
    }
}
