package com.sensichips.sensiplus.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SENSING_ELEMENTS")
public class SensingElementListWrapper {
    /**
     * Helper class to wrap a list of persons. This is used for saving the
     * list of persons to XML.
     *
     * @author Marco Jakob
     */


        private List<SensingElement> se;

        @XmlElement (name = "SENSING_ELEMENT")

        public List<SensingElement> getSensingElements() {
            return se;
        }

        public void setSensingElements(List<SensingElement> sensingElements) {
            this.se = sensingElements;
        }
    }





