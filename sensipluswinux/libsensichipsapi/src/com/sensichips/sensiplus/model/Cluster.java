//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java per Cluster complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Cluster">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CLUSTER_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MULTICAST_CLUSTER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CHIP" type="{}Chip" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

//@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cluster", propOrder = {
    "idCluster"
        /*"idSPFamily"*/,
        "MULTICASTCLUSTER",
        "chip"
})
public class Cluster {


    protected StringProperty clusterid;

    protected StringProperty multicastCluster;

    protected List<Chip> chip;
    private final StringProperty idSPFamily;
    private String oldidCluster="";




    public Cluster(){this("","","");}

    public Cluster(String idCluster){
        this(idCluster,"","");
    }

    public Cluster(String idCluster, String idSPFamily, String multicastCluster){
        this.clusterid = new SimpleStringProperty(idCluster);
        this.idSPFamily = new SimpleStringProperty(idSPFamily);
        this.multicastCluster = new SimpleStringProperty(multicastCluster);
    }

    @XmlTransient //IN QUESTO MODO NELL'XML NON METTE L'ELEMENTO <idSPFamily>
    public String getIdSPFamily(){
        return idSPFamily.get();
    }

    public StringProperty idSPFamilyProperty(){
        return idSPFamily;
    }

    public void setIdSPFamily(String idSPFamily){
        this.idSPFamily.set(idSPFamily);
    }

    public String getOldidCluster(){
        return oldidCluster;
    }

    public void setOldidCluster() {
        this.oldidCluster = this.getIdCluster();
    }
    public StringProperty idClusterProperty() {
        return clusterid;
    }

    /**
     * Recupera il valore della propriet� clusterid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "CLUSTER_ID", required = true)
    public String getIdCluster() {
        return clusterid.get();
    }

    /**
     * Imposta il valore della propriet� clusterid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCluster(String value) {
        this.clusterid.set(value);
    }

    /**
     * Recupera il valore della propriet� multicastcluster.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "MULTICAST_CLUSTER", required = true)
    public String getMULTICASTCLUSTER() {
        return multicastCluster.get();
    }

    /**
     * Imposta il valore della propriet� multicastcluster.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMULTICASTCLUSTER(String value) {
        this.multicastCluster.set(value);
    }

    /**
     * Gets the value of the chip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCHIP().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Chip }
     * 
     * 
     */
    @XmlElement(name = "CHIP", required = true)

    public List<Chip> getChip() {
        if (chip == null) {
            chip = new ArrayList<Chip>();
        }
        return this.chip;
    }

    public void setChipList(List<Chip> chipList) {
        this.chip = chipList;
    }



}
