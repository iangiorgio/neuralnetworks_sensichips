//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.model;

import javafx.beans.property.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SensingElement complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SensingElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InPortADC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NData" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MeasureTechnique" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Filter" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RANGE_MIN" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="RANGE_MAX" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="DEFAULT_ALARM_THRESHOLD" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="MULTIPLIER" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="MEASURE_UNIT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Rsense" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InGain" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OutGain" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Contacts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Frequency" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="Harmonic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DCBiasN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="-32"/>
 *               &lt;maxInclusive value="31"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DCBiasP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="-2048"/>
 *               &lt;maxInclusive value="2047"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ModeVI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MeasureType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhaseShiftMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhaseShift" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
//@XmlAccessorType(XmlAccessType.FIELD)
/*@XmlType(name = "SensingElement", propOrder = {
        "name","conversionRate","inportADC",
        "NData", "measureTechnique","filter",
        "rangeMin","rangeMax" ,"defaultAlarmThreshold",
        "multiplier","measure_Unit","RSense",
        "inGain","outGain","contacts","frequency",
        "harmonic","dcBiasN","dcBiasP","modeVI","measureType",
        "phaseShiftMode", "phaseShift", "iq", "sequentialMode",
        "oldidSPSensingElement","idSPSensingElement"
})*/
@XmlType(name = "SensingElement", propOrder = {
        "name","conversionRate","inportADC",
        "NData", "measureTechnique","filter",
        "rangeMin","rangeMax" ,"defaultAlarmThreshold",
        "multiplier","measure_Unit","RSense",
        "inGain","outGain","contacts","frequency",
        "harmonic","dcBiasN","dcBiasP","modeVI","measureType",
        "phaseShiftMode", "phaseShift", "iq", "sequentialMode",
        "oldidSPSensingElement"})
public class SensingElement {


    protected StringProperty name;

    protected IntegerProperty conversionRate;

    protected StringProperty inPortADC;

    protected IntegerProperty nData;

    protected StringProperty measureTechnique;

    protected IntegerProperty filter;

    protected DoubleProperty rangemin;

    protected DoubleProperty rangemax;

    protected DoubleProperty defaultalarmthreshold;

    protected IntegerProperty multiplier;

    protected StringProperty measureunit;

    protected IntegerProperty rsense;

    protected IntegerProperty inGain;

    protected IntegerProperty outGain;

    protected StringProperty contacts;

    protected IntegerProperty frequency;

    protected StringProperty harmonic;

    protected IntegerProperty dcBiasN;

    protected IntegerProperty dcBiasP;

    protected StringProperty modeVI;

    protected StringProperty measureType;

    protected StringProperty phaseShiftMode;

    protected IntegerProperty phaseShift;

    protected StringProperty iq;

    protected IntegerProperty sequentialMode;

    private String oldidSPSensingElement = null;

    private final StringProperty idSPSensingElement;


    /* Default constructor.
     */

    public SensingElement() {
        this("",Integer.MIN_VALUE, Integer.MIN_VALUE,Integer.MIN_VALUE,"", Integer.MIN_VALUE,"",Integer.MIN_VALUE,Integer.MIN_VALUE,"","","",Integer.MIN_VALUE,"",Integer.MIN_VALUE,"",Integer.MIN_VALUE,"",Integer.MIN_VALUE,"","",Double.NaN,Double.NaN,Double.NaN,Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public SensingElement (String id){
        this(id,Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE,"",Integer.MIN_VALUE,"",Integer.MIN_VALUE, Integer.MIN_VALUE,"","","", Integer.MIN_VALUE,"",Integer.MIN_VALUE,"",Integer.MIN_VALUE,"",Integer.MIN_VALUE,"","",Double.NaN,Double.NaN,Double.NaN,Integer.MIN_VALUE,Integer.MIN_VALUE);
    }

    public SensingElement ( String idSPSensingElement, Integer rSense) {
        this(idSPSensingElement, rSense,Integer.MIN_VALUE,Integer.MIN_VALUE,"",Integer.MIN_VALUE,"",Integer.MIN_VALUE,Integer.MIN_VALUE,"","","",Integer.MIN_VALUE,"",Integer.MIN_VALUE,"",Integer.MIN_VALUE,"",Integer.MIN_VALUE,"","",Double.NaN,Double.NaN,Double.NaN,Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public SensingElement(String idSPSensingElement, Integer rSense, Integer inGain, Integer outGain, String contacts,
                          Integer frequency, String harmonic, Integer dcBiasN, Integer dcBiasP, String modeVI, String measureTechnique,
                          String measureType, Integer filter, String phaseShiftMode, Integer phaseShift, String iq,
                          Integer conversionRate, String inportADC, Integer nData, String measure_Unit, String name,
                          Double rangeMin, Double rangeMax, Double defaultAlarmThreshold, Integer multiplier, Integer sequentialMode) {

        this.idSPSensingElement = new SimpleStringProperty(idSPSensingElement);
        this.rsense = new SimpleIntegerProperty(rSense);
        this.inGain = new SimpleIntegerProperty(inGain);
        this.outGain = new SimpleIntegerProperty(outGain);
        this.contacts = new SimpleStringProperty(contacts);
        this.frequency = new SimpleIntegerProperty(frequency);
        this.harmonic = new SimpleStringProperty(harmonic);
        this.dcBiasN = new SimpleIntegerProperty(dcBiasN);
        this.dcBiasP=new SimpleIntegerProperty(dcBiasP);
        this.modeVI = new SimpleStringProperty(modeVI);
        this.measureTechnique = new SimpleStringProperty(measureTechnique);
        this.measureType = new SimpleStringProperty(measureType);
        this.filter = new SimpleIntegerProperty(filter);
        this.phaseShiftMode = new SimpleStringProperty(phaseShiftMode);
        this.phaseShift = new SimpleIntegerProperty(phaseShift);
        this.iq = new SimpleStringProperty(iq);
        this.conversionRate = new SimpleIntegerProperty(conversionRate);
        this.inPortADC = new SimpleStringProperty(inportADC);
        this.nData = new SimpleIntegerProperty(nData);
        this.measureunit = new SimpleStringProperty(measure_Unit);
        this.name = new SimpleStringProperty(name);
        this.rangemin = new SimpleDoubleProperty(rangeMin);
        this.rangemax = new SimpleDoubleProperty(rangeMax);
        this.defaultalarmthreshold = new SimpleDoubleProperty(defaultAlarmThreshold);
        this.multiplier = new SimpleIntegerProperty(multiplier);
        this.sequentialMode = new SimpleIntegerProperty(sequentialMode);

    }


    //metodi vecchia classe
    @XmlElement(required = true)
    public String getOldidSPSensingElement() {
        return oldidSPSensingElement;
    }

    public void setoldidSPSensingElement() {
        this.oldidSPSensingElement = this.idSPSensingElement.get();
    }
   // @XmlElement(required = true)
    public String getIdSPSensingElement() {
        return idSPSensingElement.get();
    }

    public StringProperty idSPSensingElementProperty() {
        return idSPSensingElement;
    }

    public void setidSPSensingElement(String idSPSensingElement) {
        this.idSPSensingElement.set(idSPSensingElement);
    }


    /**
     * Recupera il valore della propriet� name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "NAME", required = true)
    public String getName() {
        return name.get();
    }

    /**
     * Imposta il valore della propriet� name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name.set(value);
    }

    /**
     * Recupera il valore della propriet� conversionRate.
     * 
     */
    @XmlElement(name = "ConversionRate")
    public Integer getConversionRate() {
        return conversionRate.get();
    }

    /**
     * Imposta il valore della propriet� conversionRate.
     * 
     */
    public void setConversionRate(int value) {
        this.conversionRate.set(value);
    }

    /**
     * Recupera il valore della propriet� inPortADC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
     @XmlElement(name = "InPortADC", required = true)
    public String getInportADC()  {
        return inPortADC.get();
    }

    /**
     * Imposta il valore della propriet� inPortADC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInportADC(String value) {
        this.inPortADC.set(value);
    }

    /**
     * Recupera il valore della propriet� nData.
     * 
     */
     @XmlElement(name = "NData")
    public Integer getNData() {
        return nData.get();
    }

    /**
     * Imposta il valore della propriet� nData.
     * 
     */
    public void setnData(int value) {
        this.nData.set(value);
    }

    /**
     * Recupera il valore della propriet� measureTechnique.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "MeasureTechnique", required = true)
    public String getMeasureTechnique() {
        return measureTechnique.get();
    }

    /**
     * Imposta il valore della propriet� measureTechnique.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasureTechnique(String value) {
        this.measureTechnique.set(value);
    }

    /**
     * Recupera il valore della propriet� filter.
     * 
     */
    @XmlElement(name = "Filter")
    public Integer getFilter() {
        return filter.get();
    }

    /**
     * Imposta il valore della propriet� filter.
     * 
     */
    public void setFilter(int value) {
        this.filter.set(value);
    }

    /**
     * Recupera il valore della propriet� rangemin.
     * 
     */
    @XmlElement(name = "RANGE_MIN")
    public Double getRangeMin() {
        return rangemin.get();
    }

    /**
     * Imposta il valore della propriet� rangemin.
     * 
     */
    public void setRangeMin(Double value) {
        this.rangemin.set(value);
    }

    /**
     * Recupera il valore della propriet� rangemax.
     * 
     */
    @XmlElement(name = "RANGE_MAX")
    public Double getRangeMax() {
        return rangemax.get();
    }

    /**
     * Imposta il valore della propriet� rangemax.
     * 
     */
    public void setRangeMax(Double value) {
        this.rangemax.set(value);
    }

    /**
     * Recupera il valore della propriet� defaultalarmthreshold.
     * 
     */
    @XmlElement(name = "DEFAULT_ALARM_THRESHOLD")
    public Double getDefaultAlarmThreshold()  {
        return defaultalarmthreshold.get();
    }

    /**
     * Imposta il valore della propriet� defaultalarmthreshold.
     * 
     */
    public void setDefaultAlarmThreshold(Double value) {
        this.defaultalarmthreshold.set(value);
    }

    /**
     * Recupera il valore della propriet� multiplier.
     * 
     */
    @XmlElement(name = "MULTIPLIER")
    public Integer getMultiplier() {
        return multiplier.get();
    }

    /**
     * Imposta il valore della propriet� multiplier.
     * 
     */
    public void setMultiplier(int value) {
        this.multiplier.set(value);
    }

    /**
     * Recupera il valore della propriet� measureunit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
     @XmlElement(name = "MEASURE_UNIT", required = true)
    public String getMeasure_Unit() {
        return measureunit.get();
    }

    /**
     * Imposta il valore della propriet� measureunit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasure_Unit(String value) {
        this.measureunit.set(value);
    }

    /**
     * Recupera il valore della propriet� rsense.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @XmlElement(name = "Rsense")
   // @XmlElement(required = true)


    /**
     * Imposta il valore della propriet� rsense.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */


    public Integer getRSense() {
        if (rsense.get() == Integer.MIN_VALUE) {
            return null;
        }
        return rsense.get();
    }

    public void setrSense(int rSense) {
        this.rsense.set(rSense);
    }

    /**
     * Recupera il valore della propriet� inGain.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @XmlElement(name = "InGain")
    public Integer getInGain() {
        if(inGain.get()==Integer.MIN_VALUE) {
            return null;
        }
        return inGain.get();
    }

    /**
     * Imposta il valore della propriet� inGain.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInGain(Integer value) {
        this.inGain.set(value);
    }

    /**
     * Recupera il valore della propriet� outGain.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @XmlElement(name = "OutGain")
    public Integer getOutGain() {if(outGain.get()==Integer.MIN_VALUE) {
        return null;
    }
        return outGain.get();
    }


    /**
     * Imposta il valore della propriet� outGain.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutGain(Integer value) {
        this.outGain.set(value);
    }

    /**
     * Recupera il valore della propriet� contacts.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */

    @XmlElement(name = "Contacts")
    public String getContacts() {
        return contacts.get();
    }

    /**
     * Imposta il valore della propriet� contacts.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContacts(String value) {
        this.contacts.set(value);
    }

    /**
     * Recupera il valore della propriet� frequency.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    @XmlElement(name = "Frequency")
    public Integer getFrequency() {
        if(frequency.get()==Integer.MIN_VALUE) {
        return null;
    }
        return frequency.get();
    }


    /**
     * Imposta il valore della propriet� frequency.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFrequency(int value) {
        this.frequency.set(value);
    }

    /**
     * Recupera il valore della propriet� harmonic.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "Harmonic")
    public String getHarmonic() {
        return harmonic.get();
    }

    /**
     * Imposta il valore della propriet� harmonic.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHarmonic(String value) {
        this.harmonic.set(value);
    }

    /**
     * Recupera il valore della propriet� dcBiasN.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @XmlElement(name = "DCBiasN")
    public Integer getDcBiasN() {
        if(dcBiasN.get()==Integer.MIN_VALUE) {
        return null;
    }
        return dcBiasN.get();
    }

    /**
     * Imposta il valore della propriet� dcBiasN.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDcBiasN(Integer value) {
        this.dcBiasN.set(value);
    }

    @XmlElement (name= "DCBiasP")
    public Integer getDcBiasP() {
        if(dcBiasP.get()==Integer.MIN_VALUE) {
            return null;
        }
        return dcBiasP.get();
    }

    public void setDcBiasP(Integer value) {
        this.dcBiasP.set(value);
    }
    /**
     * Recupera il valore della propriet� modeVI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "ModeVI")
    public String getModeVI() {
        return modeVI.get();
    }

    /**
     * Imposta il valore della propriet� modeVI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeVI(String value) {
        this.modeVI.set(value);
    }

    /**
     * Recupera il valore della propriet� measureType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "MeasureType")
    public String getMeasureType() {
        return measureType.get();
    }

    /**
     * Imposta il valore della propriet� measureType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasureType(String value) {
        this.measureType.set(value);
    }

    /**
     * Recupera il valore della propriet� phaseShiftMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "PhaseShiftMode")
    public String getPhaseShiftMode() {
        return phaseShiftMode.get();
    }

    /**
     * Imposta il valore della propriet� phaseShiftMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhaseShiftMode(String value) {
        this.phaseShiftMode.set(value);
    }

    /**
     * Recupera il valore della propriet� phaseShift.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */

    @XmlElement(name = "PhaseShift")
    public Integer getPhaseShift() {
        if(phaseShift.get()==Integer.MIN_VALUE) {
            return null;
        }
        return phaseShift.get();
    }

    /**
     * Imposta il valore della propriet� phaseShift.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPhaseShift(Integer value) {
        this.phaseShift.set(value);
    }

    /**
     * Recupera il valore della propriet� iq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "IQ")
    public String getIq() {
        return iq.get();
    }

    /**
     * Imposta il valore della propriet� iq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIq(String value) {
        this.iq.set(value);
    }

    @XmlElement (name= "SequentialMode")
    public Integer getSequentialMode () {
        if(sequentialMode.get()==Integer.MIN_VALUE) {
            return null;
        }
        return sequentialMode.get();
    }

    public void setSequentialMode(Integer value) {
        this.sequentialMode.set(value);
    }


}


