//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per CalibrationParameter complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="CalibrationParameter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="M" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="N" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="threshold" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
//@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalibrationParameter", propOrder = {

})
public class CalibrationParameter {

    protected Double m;

    protected Double n;
    protected Double threshold;


    @XmlElement(name = "M")
    public Double getM() {
        return m;
    }

    public StringProperty mProperty() {
        return (new SimpleStringProperty( m.toString()));
    }



    public void setM(Double m) {
        this.m=m;
    }

    @XmlElement(name = "N")
    public Double getN() {
        return n;
    }

    public StringProperty nProperty() {
        return (new SimpleStringProperty(n.toString()));
    }

    public void setN(Double n) {
        this.n=n;
    }

    /**
     * Recupera il valore della propriet� threshold.
     *
     * @return
     *     possible object is
     *     {@link Double }
     *
     */
    @XmlTransient
    public Double getThreshold() {
        return threshold;
    }

    /**
     * Imposta il valore della propriet� threshold.
     *
     * @param value
     *     allowed object is
     *     {@link Double }
     *
     */
    public void setThreshold(Double value) {
        this.threshold = value;
    }


}
/*public class CalibrationParameter {

    protected DoubleProperty m;

    protected DoubleProperty n;
    protected Double threshold;


    @XmlElement(name = "M")
    public Double getM() {
        return m.get();
    }

    public StringProperty mProperty() {
        return (new SimpleStringProperty(m.getValue() == Double.MIN_VALUE ? "" : m.getValue().toString()));
    }

    public void setM(Double m) {
        this.m.set(m);
    }

    @XmlElement(name = "N")
    public Double getN() {
        return n.get();
    }

    public StringProperty nProperty() {
        return (new SimpleStringProperty(n.getValue() == Double.MIN_VALUE ? "" : n.getValue().toString()));
    }

    public void setN(Double n) {
        this.n.set(n);
    }

    /**
     * Recupera il valore della propriet� threshold.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
  /*  @XmlTransient
    public Double getThreshold() {
        return threshold;
    }

    /**
     * Imposta il valore della propriet� threshold.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
 /*   public void setThreshold(Double value) {
        this.threshold = value;
    }


}*/
