//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Analyte complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Analyte">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ANALYTE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ANALYTE_MEASURE_UNIT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Analyte", propOrder = {

})
public class Analyte {

    @XmlElement(name = "ANALYTE_NAME", required = true)
    protected String analytename;
    @XmlElement(name = "ANALYTE_MEASURE_UNIT", required = true)
    protected String analytemeasureunit;

    /**
     * Recupera il valore della propriet� analytename.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getANALYTENAME() {
        return analytename;
    }

    /**
     * Imposta il valore della propriet� analytename.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setANALYTENAME(String value) {
        this.analytename = value;
    }

    /**
     * Recupera il valore della propriet� analytemeasureunit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getANALYTEMEASUREUNIT() {
        return analytemeasureunit;
    }

    /**
     * Imposta il valore della propriet� analytemeasureunit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setANALYTEMEASUREUNIT(String value) {
        this.analytemeasureunit = value;
    }

}
