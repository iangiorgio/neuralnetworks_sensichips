//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//

package com.sensichips.sensiplus.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Configuration complex type.
 *
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 *
 * <pre>
 * &lt;complexType name="Configuration">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DRIVER" type="{}ConfigurationDriver"/>
 *         &lt;element name="HOST_CONTROLLER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="API_OWNER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MCU" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PROTOCOL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ADDRESSING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CLUSTER_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cluster" type="{}Cluster" minOccurs="0"/>
 *         &lt;element name="VCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
//@XmlAccessorType(XmlAccessType.FIELD)
/*
@XmlType(propOrder={"driver",
        "hostController",
        "apiOwner",
        "mcu",
        "protocol","addressingType",
        "idCluster"})
*/



@XmlType(name = "Configuration", propOrder = {
        "DESCRIPTION",
        "configurationDriver",
        "hostController",
        "apiOwner",
        "mcu",
        "protocol",
        "addressingType",
        "idCluster",
        "VCC"
})
public class Configuration {

    private final IntegerProperty idSPConfiguration;
    private Integer oldidSPConfiguration=null;

    protected StringProperty description;

    protected ConfigurationDriver configurationDriver;

   // protected StringProperty driver;

   // protected StringProperty param1;
   // protected StringProperty param2;

    protected StringProperty hostcontroller;
    protected StringProperty apiowner;
    protected StringProperty  mcu;
    protected StringProperty  protocol;
    protected StringProperty addressingtype;
    protected StringProperty clusterid;

    // protected Cluster cluster;
    protected StringProperty vcc;

    //costruttori

    public Configuration(){
        this(Integer.MIN_VALUE,"","","","","","","","","","","");
    }
    public Configuration(Integer idSPConfiguration){
        this(idSPConfiguration,"","","","","","","","","","","");
    }

    public Configuration(Integer idSPConfiguration, String description, String drivername, String param1, String param2, String hostController, String apiOwner, String mcu, String protocol, String addressingType, String idCluster, String vcc)
    {
        this.idSPConfiguration = new SimpleIntegerProperty(idSPConfiguration);
        this.description = new SimpleStringProperty(description);

        /*this.drivername = new SimpleStringProperty(driver);
        this.param1 = new SimpleStringProperty(param1);
        this.param2 = new SimpleStringProperty(param2);*/
        this.configurationDriver = new ConfigurationDriver();
        this.configurationDriver.setDriver(drivername);
        this.configurationDriver.setParam1(param1);
        this.configurationDriver.setParam2(param2);

        this.hostcontroller = new SimpleStringProperty(hostController);
        this.apiowner= new SimpleStringProperty(apiOwner);
        this.mcu = new SimpleStringProperty(mcu);
        this.protocol=new SimpleStringProperty(protocol);
        this.addressingtype = new SimpleStringProperty(addressingType);
        this.clusterid= new SimpleStringProperty(idCluster);
        this.vcc= new SimpleStringProperty(vcc);
    }

    public Integer getIdSPConfiguration() {
        return idSPConfiguration.get();
    }

    public IntegerProperty idSPConfigurationProperty() {
        return idSPConfiguration;
    }

    public void setIdSPConfiguration(int idSPConfiguration) {
        this.idSPConfiguration.set(idSPConfiguration);
    }
    public Integer getOldidSPConfiguration() {
        return oldidSPConfiguration;
    }
    //questa variabile ci è utile quando modifichiamo l'id della configurazione.
    public void setOldidSPConfiguration() {
        this.oldidSPConfiguration = this.getIdSPConfiguration();
    }

    /**
     * Recupera il valore della propriet� description.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @XmlElement(name = "DESCRIPTION", required = true)
    public String getDESCRIPTION() {
        return description.get();
    }

    /**
     * Imposta il valore della propriet� description.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDESCRIPTION(String value) {
        this.description.set(value);
    }


    @XmlElement(name="DRIVER")

    public ConfigurationDriver getConfigurationDriver() {
        return configurationDriver;
    }

    public void setConfigurationDriver(ConfigurationDriver configurationDriver) {
        this.configurationDriver = configurationDriver;
    }

    /**
     * Recupera il valore della propriet� driver.
     *
     * @return
     *     possible object is
     *     {@link String}
     *
     */


  /*  @XmlElement(name = "DRIVER_NAME", required = true)
    public String getDriver() {
        return drivername.get();
    }

    /**
     * Imposta il valore della propriet� driver.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
  /*  public void setDriver(String value) {
        this.drivername.set(value);
    }

    /**
     * Recupera il valore della propriet� hostcontroller.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */


    /*@XmlElement(name= "PARAM1", required = true)

    public String getParam1 (){
        return param1.get();
    }
    public void setParam1(String value){
        this.param1.set(value);
    }

    @XmlElement(name= "PARAM2", required = true)

    public String getParam2 (){
        return param2.get();
    }
    public void setParam2(String value){
        this.param2.set(value);
    }
*/

    @XmlElement(name = "HOST_CONTROLLER", required = true)
    public String getHostController() {
        return hostcontroller.get();
    }

    /**
     * Imposta il valore della propriet� hostcontroller.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setHostController(String value) {
        this.hostcontroller.set(value);
    }

    /**
     * Recupera il valore della propriet� apiowner.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @XmlElement(name = "API_OWNER", required = true)
    public String getApiOwner() {
        return apiowner.get();
    }

    /**
     * Imposta il valore della propriet� apiowner.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setApiOwner(String value) {
        this.apiowner.set(value);
    }

    /**
     * Recupera il valore della propriet� mcu.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @XmlElement(name = "MCU", required = true)
    public String getMcu() {
        return mcu.get();
    }

    /**
     * Imposta il valore della propriet� mcu.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMcu(String value) {
        this.mcu.set(value);
    }

    /**
     * Recupera il valore della propriet� protocol.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @XmlElement(name = "PROTOCOL", required = true)
    public String getProtocol() {
        return protocol.get();
    }

    /**
     * Imposta il valore della propriet� protocol.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProtocol(String value) {
        this.protocol.set(value);
    }

    /**
     * Recupera il valore della propriet� addressingtype.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @XmlElement(name = "ADDRESSING_TYPE", required = true)
    public String getAddressingType() {
        return addressingtype.get();
    }

    /**
     * Imposta il valore della propriet� addressingtype.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAddressingType(String value) {
        this.addressingtype.set(value);
    }

    /**
     * Recupera il valore della propriet� clusterid.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @XmlElement(name = "CLUSTER_ID", required = true)
    public String getIdCluster() {
        return clusterid.get();
    }

    /**
     * Imposta il valore della propriet� clusterid.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdCluster(String value) {
        this.clusterid.set(value);
    }

    /**
     * Recupera il valore della propriet� cluster.
     *
     * @return
     *     possible object is
     *     {@link Cluster }
     *
     */
  /*  public Cluster getCluster() {
        return cluster;
    }*/

    /**
     * Imposta il valore della propriet� cluster.
     *
     * @param value
     *     allowed object is
     *     {@link Cluster }
     *
     */
  /*  public void setCluster(Cluster value) {
        this.cluster = value;
    }*/

    /**
     * Recupera il valore della propriet� vcc.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @XmlElement(name = "VCC", required = true)
    public String getVCC() {
        return vcc.get();
    }

    /**
     * Imposta il valore della propriet� vcc.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setVCC(String value) {
        this.vcc.set(value);
    }

}
