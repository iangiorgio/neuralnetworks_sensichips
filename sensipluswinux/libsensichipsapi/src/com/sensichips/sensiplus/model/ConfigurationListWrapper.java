package com.sensichips.sensiplus.model;

import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "LAYOUT_CONFIGURATION")
@XmlType (propOrder={"sensingElementList", "familyElement", "clusterElement", "configurationElement"})
public class ConfigurationListWrapper {

    private List<SensingElement> sensingElementList;
    private Family familyElement;
    private Cluster clusterElement;
    private Configuration configurationElement;

    //@XmlElementWrapper(name = "SENSING_ELEMENTS")
    @XmlElement(name = "SENSING_ELEMENT")
    public List<SensingElement> getSensingElementList() {
        return sensingElementList;
    }

    public void setSensingElementList() {
        try {
            sensingElementList = SensingElementDAOMySQLImpl.getInstance().selectSensingElementOnFamily(familyElement);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    @XmlElement(name = "FAMILY")
    public Family getFamilyElement() {
        return familyElement;
    }

    public void setFamilyElement() {
        SensingElement se;
        try {
            familyElement = (Family) FamilyDAOMYSQLImpl.getInstance().select(new Family(clusterElement.getIdSPFamily())).get(0);
            familyElement.setPortList(FamilyDAOMYSQLImpl.getInstance().selectInPort(familyElement));
            familyElement.setMeasureTechniqueList(FamilyDAOMYSQLImpl.getInstance().selectInMeasureTechnique(familyElement));
            for(int i = 0; i< familyElement.getPortList().size(); i++){
                se = (SensingElement) FamilyDAOMYSQLImpl.getInstance().selectSensingElementIn(familyElement, familyElement.getPortList().get(i));
                if ( se!=null) {
                    familyElement.getPortList().get(i).setSeAssociated(se.getIdSPSensingElement());
                    familyElement.getPortList().get(i).setSeNameAssociated(se.getName());
                }
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    @XmlElement(name = "CLUSTER")
    public Cluster getClusterElement() {
        return clusterElement;
    }

    public void setClusterElement() {
        List<SensingElementOnChip> seOnChipList;
        List<SensingElement> seList =  new ArrayList<>();
        List<Chip> chipList = new  ArrayList<>();
        SensingElementOnChip seOnChip = new SensingElementOnChip();
        try {
            clusterElement = (Cluster) ClusterDAOMYSQLImpl.getInstance().select(new Cluster(configurationElement.getIdCluster())).get(0);
            seOnChip.setIdSPCluster(clusterElement.getIdCluster());
            chipList = ChipDAOMYSQLImpl.getInstance().selectChipOfCluster(clusterElement);
            clusterElement.setChipList(chipList);
            seOnChip.setIdSPCluster(clusterElement.getIdCluster());
            for (int i = 0; i < chipList.size(); i++) {
                seOnChip.setIdSPChip(chipList.get(i).getIdSPChip());
                seOnChipList = SensingElementOnChipDAOMYSQLImpl.getInstance().selectAll(seOnChip);
                for (int j=0; j<seOnChipList.size(); j++){
                    seList.add((SensingElement)SensingElementDAOMySQLImpl.getInstance().selectSensingElementOnChip(seOnChipList.get(j)));
                    seOnChipList.get(j).setSeAssociated(seList.get(j).getIdSPSensingElement());
                }
                chipList.get(i).setSeOnChipList(seOnChipList);
            }

        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    @XmlElement(name = "CONFIGURATION")
    public Configuration getConfigurationElement() {
        return configurationElement;
    }

    public void setConfigurationElement(Configuration configList) {
        this.configurationElement = configList;
    }

}

