package com.sensichips.sensiplus.model.dao;

import java.util.List;

public interface DAOFamily <T,E,M,S> extends DAO<T> {

    List<E> selectPortNotIn(T f) throws DAOException;
    List<E> selectInPort(T f) throws DAOException;
    void insertPort(T f, E p) throws DAOException;
    void deletePort(T f, E p)throws DAOException;
    void deleteAllPort(T f)throws DAOException;

    List<M> selectMeasureTechniqueNotIn(T f) throws DAOException;
    List<M> selectInMeasureTechnique(T f) throws DAOException;
    void insertMeasureTechnique(T f, M p) throws DAOException;
    void deleteMeasureTechnique(T f, M p)throws DAOException;
    void deleteAllMeasureTechnique(T f)throws DAOException;

    List<S> selectSensingElementNotIn(T f, E p) throws DAOException;
    S selectSensingElementIn(T f, E p) throws DAOException;

    void insertSensingElementInPort(T f, E p) throws DAOException;
    void deleteSensingElementInPort(T f, E p) throws  DAOException;

}
