package com.sensichips.sensiplus.model.dao;

import java.util.List;

public interface DAOSensignElementOnChip<T> extends DAO<T> {
    List<T> selectAll(T a) throws DAOException;
}
