package com.sensichips.sensiplus.model.dao.mysql;

import com.sensichips.sensiplus.model.Family;
import com.sensichips.sensiplus.model.MeasureTechnique;
import com.sensichips.sensiplus.model.Port;
import com.sensichips.sensiplus.model.SensingElement;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.DAOFamily;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class FamilyDAOMYSQLImpl implements DAOFamily<Family, Port, MeasureTechnique, SensingElement> {

    private FamilyDAOMYSQLImpl() {}

    private static DAOFamily dao = null;
    private static Logger logger = null;

    public static DAOFamily getInstance() {
        if (dao == null) {
            dao = new FamilyDAOMYSQLImpl();
            logger = Logger.getLogger(FamilyDAOMYSQLImpl.class.getName());

        }
        return dao;
    }

    @Override
    public List<Family> select(Family f) throws DAOException {

        if (f == null) {
            f = new Family();
        }

        ArrayList<Family> lista = new ArrayList<Family>();
        try {

            if (f == null || f.getIdSPFamily() == null
                    || f.getHwVersion() == null || f.getId() == null
                    || f.getSysclock() == null || f.getOsctrim()  == null|| f.getMulticastDefault()== null
                    ||f.getBroadcast()==null ||f.getBroadcastSlow()==null) {
                throw new DAOException("In select: any field can be null");
            }


            Statement st = DAOMySQLSettings.getStatement();

            String sql = "select * from SPFamily where idSPFamily like '";
            sql += f.getIdSPFamily();
            sql += "%' and id like '" + f.getId();
            sql += "%' and hwVersion like '" + f.getHwVersion();
            sql += "%' and sysclock like '" + f.getSysclock();
            sql += "%' and osctrim like '" + f.getSysclock();
            sql += "%' and multicastDefault like '" + f.getMulticastDefault();
            sql += "%' and broadcast like '" + f.getBroadcast();
            sql += "%' and broadcastSlow like '" + f.getBroadcastSlow() + "%'";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Family(rs.getString("idSPFamily"),
                        rs.getString("id"), rs.getString("hwVersion"), rs.getString("sysclock"),
                        rs.getString("osctrim"), rs.getString("multicastDefault"), rs.getString("broadcast"),
                        rs.getString("broadcastSlow")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }

    @Override
    public void update(Family f) throws DAOException {

        if (f == null || f.getIdSPFamily() == null
                || f.getHwVersion() == null || f.getId() == null
                || f.getSysclock() == null || f.getOsctrim() == null || f.getMulticastDefault()==null || f.getBroadcast()== null
        || f.getBroadcastSlow()==null) {
            throw new DAOException("In update: any field can be null");
        }
        //UPDATE `sensichipsdb`.`SPFamily` SET `id`='10' WHERE `idSPFamily`='5';
        String query = "UPDATE SPFamily SET id = '" + f.getId() + "', hwVersion = '" + f.getHwVersion()
                + "', sysclock = '" + f.getSysclock() + "', osctrim = '" + f.getOsctrim()
                + "', multicastDefault = '" + f.getMulticastDefault() +"', broadcast = '" + f.getBroadcast() + "', broadcastSlow= '" + f.getBroadcastSlow()
                +"',idSPFamily = '" + f.getIdSPFamily() + "'";
        query = query + " WHERE idSPFamily = '" + f.getOldidSPFamily() + "';";

        logger.info("SQL: " + query);

        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In update: " + e.getMessage());
        }
    }

    @Override
    public void insert(Family f) throws DAOException {

        if (f == null || f.getIdSPFamily() == null
                || f.getHwVersion() == null || f.getId() == null
                || f.getSysclock() == null || f.getOsctrim() == null|| f.getMulticastDefault()== null
        ||f.getBroadcast()==null ||f.getBroadcastSlow()==null) {
            throw new DAOException("In insert: any field can be null");
        }
        //NSERT INTO `sensichipsdb`.`SPFamily` (`idSPFamily`, `name`, `id`, `hwVersion`, `sysclock`, `osctrim`) VALUES ('7', '7', '7', '7', '7', '7');

        String query = "INSERT INTO SPFamily (idSPFamily, id, hwVersion, sysclock, osctrim, multicastDefault, broadcast, broadcastSlow) VALUES  ('"
                + f.getIdSPFamily() + "', '" + f.getId() + "', '" + f.getHwVersion()
                + "', '" + f.getSysclock() + "', '" + f.getOsctrim() + "', '"+ f.getMulticastDefault() +"', '" + f.getBroadcastSlow() + "')";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In insert: " + e.getMessage());
        }
    }

    @Override
    public void delete(Family f) throws DAOException {

        if (f == null || f.getIdSPFamily() == null
                || f.getHwVersion() == null || f.getId() == null
                || f.getSysclock() == null || f.getOsctrim() == null || f.getMulticastDefault()== null
                ||f.getBroadcast()==null ||f.getBroadcastSlow()==null ) {
            throw new DAOException("In delete: any field can be null");
        }

        String query = "DELETE FROM SPFamily WHERE idSPFamily='" + f.getIdSPFamily() + "';";
        logger.info("SQL: " + query);

        Statement st = null;
        try {
            st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In delete: " + e.getMessage());
        }
    }

    @Override
    public List<Port> selectPortNotIn(Family f) throws DAOException {

        if (f == null) {
            f = new Family();
        }

        ArrayList<Port> lista = new ArrayList<Port>();

        try {
            if (f == null || f.getIdSPFamily() == null
                    || f.getHwVersion() == null || f.getId() == null
                    || f.getSysclock() == null || f.getOsctrim()  == null|| f.getMulticastDefault()== null
                    ||f.getBroadcast()==null ||f.getBroadcastSlow()==null) {
                throw new DAOException("In Select: any field can be null");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "SELECT * FROM SPPort WHERE idSPPort NOT IN (SELECT SPPort_idSPPort FROM SPFamilyTemplate where SPFamily_idSPFamily = '"
                    + f.getIdSPFamily() + "')";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Port(rs.getInt("idSPPort"), rs.getBoolean("internal"),
                        rs.getString("name")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }


    @Override
    public List<Port> selectInPort(Family f) throws DAOException {

        if (f == null) {
            f = new Family();
        }

        ArrayList<Port> lista = new ArrayList<Port>();

        try {
            if (f == null || f.getIdSPFamily() == null
                    || f.getHwVersion() == null || f.getId() == null
                    || f.getSysclock() == null || f.getOsctrim()  == null|| f.getMulticastDefault()== null
                    ||f.getBroadcast()==null ||f.getBroadcastSlow()==null) {
                throw new DAOException("In Select: any field can be null");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "SELECT * FROM SPPort WHERE idSPPort IN (SELECT SPPort_idSPPort FROM SPFamilyTemplate where SPFamily_idSPFamily = '"
                    + f.getIdSPFamily() + "')";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Port(rs.getInt("idSPPort"), rs.getBoolean("internal"),
                       rs.getString("name")));

            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }


    @Override
    public void insertPort(Family f, Port p) throws DAOException {

        if (p == null || p.getIdSPPort() == null
                || p.getName() == null || f == null || f.getIdSPFamily() == null) {
            throw new DAOException("In insertPort: any field can be null");
        }
        //nell'inserimento non è necessario inserire la chiave primaria della tabella, poiche la chiave primaria
        // della tabella è autoincrement
        String query = "INSERT INTO SPFamilyTemplate (SPFamily_idSPFamily, SPPort_idSPPort) VALUES ('"
                + f.getIdSPFamily() + "', '" + p.getIdSPPort() + "');";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In insertPort: " + e.getMessage());
        }
    }

    @Override
    public void deletePort(Family f, Port p) throws DAOException {

        if (p == null || p.getIdSPPort() == null
                || p.getName() == null || f == null || f.getIdSPFamily() == null) {
            throw new DAOException("In deletePort: any field can be null");
        }

        //*DELETE FROM SPFamilyTemplate WHERE SPFamily_idSPFamily='1'AND SPPort_idSPPort='5'
        String query = "DELETE FROM SPFamilyTemplate WHERE SPFamily_idSPFamily='" + f.getIdSPFamily() + "'" + "AND SPPort_idSPPort='" + p.getIdSPPort() + "';";
        logger.info("SQL: " + query);

        Statement st = null;
        try {
            st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In deletePort: " + e.getMessage());
        }
    }

    @Override
    public void deleteAllPort(Family f) throws DAOException {

        if (f == null || f.getIdSPFamily() == null) {
            throw new DAOException("In deleteAllPort: any field can be null");
        }

        //DELETE FROM SPFamilyTemplate WHERE SPFamily_idSPFamily='1';
        String query = "DELETE FROM SPFamilyTemplate WHERE SPFamily_idSPFamily='" + f.getIdSPFamily() + "';";
        logger.info("SQL: " + query);
        Statement st = null;
        try {
            st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In deleteAllPort: " + e.getMessage());
        }
    }


    @Override
    public List<MeasureTechnique> selectMeasureTechniqueNotIn(Family f) throws DAOException {

        if (f == null) {
            f = new Family();
        }

        ArrayList<MeasureTechnique> lista = new ArrayList<MeasureTechnique>();

        try {
            if (f == null || f.getIdSPFamily() == null
                    || f.getHwVersion() == null || f.getId() == null
                    || f.getSysclock() == null || f.getOsctrim()  == null|| f.getMulticastDefault()== null
                    ||f.getBroadcast()==null ||f.getBroadcastSlow()==null) {
                throw new DAOException("In Select: any field can be null");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "SELECT * FROM SPMeasureTechnique WHERE idSPMeasureTechnique NOT IN (SELECT SPMeasureTechnique_idSPMeasureTechnique FROM spfamily_has_spmeasuretechnique where SPFamily_idSPFamily='"
                    + f.getIdSPFamily() + "')";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new MeasureTechnique(rs.getInt("idSPMeasureTechnique"), rs.getString("type")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }

    @Override
    public List<MeasureTechnique> selectInMeasureTechnique(Family f) throws DAOException {

        if (f == null) {
            f = new Family();
        }

        ArrayList<MeasureTechnique> lista = new ArrayList<MeasureTechnique>();

        try {
            if (f == null || f.getIdSPFamily() == null
                    || f.getHwVersion() == null || f.getId() == null
                    || f.getSysclock() == null || f.getOsctrim()  == null|| f.getMulticastDefault()== null
                    ||f.getBroadcast()==null ||f.getBroadcastSlow()==null) {
                throw new DAOException("In Select: any field can be null");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "SELECT * FROM SPMeasureTechnique WHERE idSPMeasureTechnique IN " +
                    "(SELECT SPMeasureTechnique_idSPMeasureTechnique FROM spfamily_has_spmeasuretechnique where SPFamily_idSPFamily='"
                    + f.getIdSPFamily() + "')";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new MeasureTechnique(rs.getInt("idSPMeasureTechnique"), rs.getString("type")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }

    @Override
    public void insertMeasureTechnique(Family f, MeasureTechnique m) throws DAOException {

        if (m == null || m.getIdSPMeasureTechnique() == null
                || m.getType() == null || f == null || f.getIdSPFamily() == null) {
            throw new DAOException("In insertMeasureTechnique: any field can be null");
        }
        //INSERT INTO `sensidb`.`spfamily_has_SPMeasureTechnique` (`SPFamily_idSPFamily`, `SPMeasureTechnique_idSPMeasureTechnique`) VALUES ('1', '1');

        String query = "INSERT INTO spfamily_has_spmeasuretechnique (SPFamily_idSPFamily, SPMeasureTechnique_idSPMeasureTechnique) VALUES ('"
                + f.getIdSPFamily() + "', '" + m.getIdSPMeasureTechnique() + "');";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In insertMeasureTechnique: " + e.getMessage());
        }
    }

    @Override
    public void deleteMeasureTechnique(Family f, MeasureTechnique m) throws DAOException {

        if (m == null || m.getIdSPMeasureTechnique() == null
                || m.getType() == null || f == null || f.getIdSPFamily() == null) {
            throw new DAOException("In deleteMeasureTechnique: any field can be null");
        }
        //DELETE FROM spfamily_has_spmeasuretechnique WHERE SPFamily_idSPFamily='1' and SPMeasureTechnique_idSPMeasureTechnique='1';
        String query = "DELETE FROM spfamily_has_spmeasuretechnique WHERE SPFamily_idSPFamily='" + f.getIdSPFamily() + "' AND SPMeasureTechnique_idSPMeasureTechnique='" + m.getIdSPMeasureTechnique() + "';";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In deleteMeasureTechnique: " + e.getMessage());
        }
    }

    @Override
    public void deleteAllMeasureTechnique(Family f) throws DAOException {

        if (f == null || f.getIdSPFamily() == null) {
            throw new DAOException("In deleteAllMeasureTechnique: any field can be null");
        }
        //DELETE FROM spfamily_has_spmeasuretechnique WHERE SPFamily_idSPFamily='2';
        String query = "DELETE FROM spfamily_has_spmeasuretechnique WHERE SPFamily_idSPFamily='" + f.getIdSPFamily() + "';";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In deleteAllMeasureTechnique: " + e.getMessage());
        }
    }

    //restiruisce il sensisng element inserito su una specifia porta
    @Override
    public SensingElement selectSensingElementIn(Family f, Port p) throws DAOException {

        if (f == null) {
            f = new Family();
        }
        if (p == null) {
            p = new Port();
        }

       SensingElement sensingElement = null;


        try {
            if (f == null || f.getIdSPFamily() == null
                    || f.getHwVersion() == null || f.getId() == null
                    || f.getSysclock() == null || f.getOsctrim()  == null|| f.getMulticastDefault()== null
                    ||f.getBroadcast()==null ||f.getBroadcastSlow()==null) {
                throw new DAOException("In Select: any field can be null");
            }
            //SELECT * FROM SPSensingElement WHERE idSPSensingElement IN (SELECT SPSensingElement_idSPSensingElement FROM SPSensingElementonfamily where SPFamilyTemplate_idSPFamilyTemplate =(select idSPFamilyTemplate from SPFamilyTemplate where '2'=SPPort_idSPPort and '3'=SPFamily_idSPFamily));
            Statement st = DAOMySQLSettings.getStatement();
            String sql = "SELECT * FROM SPSensingElement WHERE idSPSensingElement IN " +
                    "(SELECT SPSensingElement_idSPSensingElement FROM SPSensingElementOnFamily where SPFamilyTemplate_idSPFamilyTemplate =" +
                    "(select idSPFamilyTemplate from SPFamilyTemplate where SPPort_idSPPort='"
                    + p.getIdSPPort() + "' and SPFamily_idSPFamily='" + f.getIdSPFamily() + "')); ";


            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                sensingElement = new SensingElement(rs.getString("idSPSensingElement"), rs.getInt("Rsense"),
                        rs.getInt("InGain"), rs.getInt("OutGain"), rs.getString("Contacts"),
                        rs.getInt("Frequency"), rs.getString("Harmonic"), rs.getInt("DCBiasN"), rs.getInt("DCBiasP"),
                        rs.getString("ModeVI"), rs.getString("MeasureTechnique"), rs.getString("MeasureType"),
                        rs.getInt("Filter"), rs.getString("PhaseShiftMode"), rs.getInt("PhaseShift"),
                        rs.getString("IQ"), rs.getInt("ConversionRate"), rs.getString("InPortADC"),
                        rs.getInt("NData"), rs.getString("measureUnit"), rs.getString("name"),
                        rs.getDouble("rangeMin"), rs.getDouble("rangeMax"), rs.getDouble("defaultAlarmThreshold"),
                        rs.getInt("multiplier"), rs.getInt("SequentialMode"));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return sensingElement;
    }

    @Override
    public List<SensingElement> selectSensingElementNotIn(Family f, Port p) throws DAOException {
        if (f == null) {
            f = new Family();
        }
        if (p == null) {
            p = new Port();
        }

        List<SensingElement> list = new ArrayList<>();

        try {
            if (f == null || f.getIdSPFamily() == null
                    || f.getHwVersion() == null || f.getId() == null
                    || f.getSysclock() == null || f.getOsctrim()  == null|| f.getMulticastDefault()== null
                    ||f.getBroadcast()==null ||f.getBroadcastSlow()==null
                    || p == null || p.getIdSPPort() == null || p.getName() == null){
                throw new DAOException("In Select: any field can be null");
            }
            //SELECT * FROM SPSensingElement WHERE idSPSensingElement IN (SELECT SPSensingElement_idSPSensingElement FROM SPSensingElementOnFamily where SPFamilyTemplate_idSPFamilyTemplate =(select idSPFamilyTemplate from SPFamilyTemplate where '2'=SPPort_idSPPort and '3'=SPFamily_idSPFamily));
            Statement st = DAOMySQLSettings.getStatement();
            String sql = "SELECT * FROM SPSensingElement WHERE idSPSensingElement NOT IN (SELECT SPSensingElement_idSPSensingElement FROM SPSensingElementonfamily where SPFamilyTemplate_idSPFamilyTemplate =(select idSPFamilyTemplate from SPFamilyTemplate where SPPort_idSPPort='"
                    + p.getIdSPPort() + "' and SPFamily_idSPFamily='" + f.getIdSPFamily() + "')); ";


            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                list.add(new SensingElement(rs.getString("idSPSensingElement"), rs.getInt("Rsense"),
                        rs.getInt("InGain"), rs.getInt("OutGain"), rs.getString("Contacts"),
                        rs.getInt("Frequency"), rs.getString("Harmonic"), rs.getInt("DCBiasP"), rs.getInt("DCBiasP"),
                        rs.getString("ModeVI"), rs.getString("MeasureTechnique"), rs.getString("MeasureType"),
                        rs.getInt("Filter"), rs.getString("PhaseShiftMode"), rs.getInt("PhaseShift"),
                        rs.getString("IQ"), rs.getInt("ConversionRate"), rs.getString("InPortADC"),
                        rs.getInt("NData"), rs.getString("measureUnit"), rs.getString("name"),
                        rs.getDouble("rangeMin"), rs.getDouble("rangeMax"), rs.getDouble("defaultAlarmThreshold"),
                        rs.getInt("multiplier"), rs.getInt("SequentialMode")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return list;
    }

    @Override
    public void insertSensingElementInPort(Family f, Port p) throws DAOException {
        if (f == null || f.getIdSPFamily() == null
                || f.getHwVersion() == null || f.getId() == null
                || f.getSysclock() == null || f.getOsctrim()  == null|| f.getMulticastDefault()== null
                ||f.getBroadcast()==null ||f.getBroadcastSlow()==null
                || p == null || p.getIdSPPort() == null || p.getName() == null){
            throw new DAOException("In Select: any field can be null");

        }
        //nell'inserimento non è necessario inserire la chiave primaria della tabella, poiche la chiave primaria
        // della tabella è autoincrement
        //INSERT INTO `sensidb`.`SPSensingElementOnFamily` (`SPSensingElement_idSPSensingElement`, `SPFamilyTemplate_idSPFamilyTemplate`) VALUES ('ciao', (select idSPFamilyTemplate from SPFamilyTemplate where '1'=SPPort_idSPPort and '1'=SPFamily_idSPFamily));
        String query = "INSERT INTO SPSensingElementOnFamily (SPSensingElement_idSPSensingElement, name, SPFamilyTemplate_idSPFamilyTemplate) VALUES ('"
                + p.getSeAssociated() +"','" +p.getSeNameAssociated() +"', (select idSPFamilyTemplate from SPFamilyTemplate where SPPort_idSPPort='"+p.getIdSPPort()
                +"' and SPFamily_idSPFamily='"+f.getIdSPFamily()+"'));";

        /*String query = "INSERT INTO SPSensingElementOnFamily (SPSensingElement_idSPSensingElement, SPFamilyTemplate_idSPFamilyTemplate) VALUES ('"
                + p.getSeAssociated() +"', (select idSPFamilyTemplate from SPFamilyTemplate where SPPort_idSPPort='"+p.getIdSPPort()
                +"' and SPFamily_idSPFamily='"+f.getIdSPFamily()+"'));";*/

        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In insertSensingElementInPort: " + e.getMessage());
        }
    }

    @Override
    public void deleteSensingElementInPort(Family f, Port p) throws DAOException {
        if (     p == null || f == null){
            throw new DAOException("In Delete: any field can be null");

        }
        //DELETE FROM SPSensingElementOnFamily WHERE SPSensingElement_idSPSensingElement='ciao';

       /* String query = "DELETE FROM SPSensingElementOnFamily WHERE SPSensingElement_idSPSensingElement='"
                + p.getSeAssociated() + "' AND SPFamilyTemplate_idSPFamilyTemplate=" + "(select idSPFamilyTemplate from SPFamilyTemplate where SPPort_idSPPort='"+p.getIdSPPort()
                +"' and SPFamily_idSPFamily='"+f.getIdSPFamily()+ "');";*/

       //AGGIUNGO ANCHE IL NOME DEL SENSING ELEMENT NELLA QUERY "name= '" + p.getSeNameAssociated()"
        String query = "DELETE FROM SPSensingElementOnFamily WHERE SPSensingElement_idSPSensingElement='"
                + p.getSeAssociated() + "' and name= '" + p.getSeNameAssociated() +"' AND SPFamilyTemplate_idSPFamilyTemplate=" + "(select idSPFamilyTemplate from SPFamilyTemplate where SPPort_idSPPort='"+p.getIdSPPort()
                +"' and SPFamily_idSPFamily='"+f.getIdSPFamily()+ "');";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In deleteSensingElementInPort: " + e.getMessage());
        }
    }


}
