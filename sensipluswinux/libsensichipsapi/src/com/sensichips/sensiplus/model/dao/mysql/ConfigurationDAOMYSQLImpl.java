package com.sensichips.sensiplus.model.dao.mysql;

import com.sensichips.sensiplus.model.Configuration;
import com.sensichips.sensiplus.model.dao.DAO;
import com.sensichips.sensiplus.model.dao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ConfigurationDAOMYSQLImpl implements DAO<Configuration> {
    private ConfigurationDAOMYSQLImpl() {
    }

    private static DAO dao = null;
    private static Logger logger = null;

    public static DAO getInstance() {
        if (dao == null) {
            dao = new ConfigurationDAOMYSQLImpl();
            logger = Logger.getLogger(ConfigurationDAOMYSQLImpl.class.getName());
        }
        return dao;
    }

    @Override
    public List<Configuration> select(Configuration c) throws DAOException {

        if (c == null) {
            c = new Configuration();
        }
        if (c == null || c.getIdSPConfiguration() == null
                || c.getConfigurationDriver().getDriver() == null || c.getHostController() == null || c.getApiOwner() == null || c.getMcu() == null
                || c.getProtocol() == null || c.getIdCluster() == null || c.getDESCRIPTION()==null || c.getVCC()==null) {
            throw new DAOException("In select: any field can be null");
        }

        ArrayList<Configuration> lista = new ArrayList<Configuration>();
        try {
            Statement st = DAOMySQLSettings.getStatement();

            String sql = "select * from SPConfiguration where idSPConfiguration like '";
            sql += ((c.getIdSPConfiguration() == Integer.MIN_VALUE) ? "" : c.getIdSPConfiguration());
            sql += "%' and description like '" + c.getDESCRIPTION();
            sql += "%' and driver like '" + c.getConfigurationDriver().getDriver();
            sql += "%' and param1 like '" + c.getConfigurationDriver().getParam1();
            sql += "%' and param2 like '" + c.getConfigurationDriver().getParam2();
            sql += "%' and hostController like '" + c.getHostController();
            sql += "%' and apiOwner like '" + c.getApiOwner();
            sql += "%' and mcu like '" + c.getMcu();
            sql += "%' and protocol like '" + c.getProtocol();
            sql += "%' and addressingType like '" + c.getAddressingType();
            sql += "%' and idCluster like '" + c.getIdCluster() ;
            sql += "%' and vcc like '" + c.getVCC()+ "%'";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Configuration(rs.getInt("idSPConfiguration"), rs.getString("description"),rs.getString("driver"),
                        rs.getString("param1"), rs.getString("param2"),rs.getString("hostController"), rs.getString("apiOwner"), rs.getString("mcu"),
                        rs.getString("protocol"), rs.getString("addressingType"), rs.getString("idCluster")
                        ,rs.getString("vcc")));

            }

            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }


    @Override
    public void insert(Configuration c) throws DAOException {

        if (c == null || c.getIdSPConfiguration() == null
                || c.getConfigurationDriver().getDriver() == null || c.getHostController() == null || c.getApiOwner() == null || c.getMcu() == null
                || c.getProtocol() == null || c.getIdCluster() == null || c.getDESCRIPTION()==null || c.getVCC()==null) {
            throw new DAOException("In insert: any field can be null");
        }

        //INSERT INTO SPConfiguration(idSPConfiguration, driver, hostController, apiOwner, mcu, protocol, addressingType, idCluster) VALUES ('1', '1', '1', '1', '1', '1', '1', '1');
        String query = "INSERT INTO SPConfiguration(idSPConfiguration, description, driver, param1, param2, hostController, apiOwner, mcu, protocol, addressingType, idCluster, vcc) VALUES ('"
                + c.getIdSPConfiguration() + "', '" + c.getDESCRIPTION() + "', '" + c.getConfigurationDriver().getDriver() + "','" +c.getConfigurationDriver().getParam1() + "','" +c.getConfigurationDriver().getParam2() + "','" + c.getHostController() + "', '" +
                c.getApiOwner() + "', '" + c.getMcu() + "', '" + c.getProtocol() + "', '" + c.getAddressingType() + "', '" + c.getIdCluster() + "', '" + c.getVCC()+ "' );";

        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In insert: " + e.getMessage());
        }
    }

    @Override
    public void delete(Configuration c) throws DAOException {

        if (c == null || c.getIdSPConfiguration() == null
                || c.getConfigurationDriver().getDriver() == null || c.getHostController() == null || c.getApiOwner() == null || c.getMcu() == null
                || c.getProtocol() == null || c.getIdCluster() == null) {
            throw new DAOException("In delete: any field can be null");
        }
        //DELETE FROM SPConfiguration WHERE idSPConfiguration='1'
        String query = "DELETE FROM SPConfiguration WHERE idSPConfiguration='" + c.getIdSPConfiguration() + "';";
        logger.info("SQL: " + query);

        Statement st = null;
        try {
            st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In delete: " + e.getMessage());
        }
    }

    @Override
    public void update(Configuration c) throws DAOException {

        if (c == null || c.getIdSPConfiguration() == null
                || c.getConfigurationDriver().getDriver() == null || c.getHostController() == null || c.getApiOwner() == null || c.getMcu() == null
                || c.getProtocol() == null || c.getIdCluster() == null || c.getDESCRIPTION()== null || c.getVCC()==null) {
            throw new DAOException("In update: any field can be null");
        }
        //UPDATE SPConfiguration SET idSPConfiguration='2', driver='2', hostController='2', apiOwner='2', mcu='2', protocol='2', addressingType='2', idCluster='topolino' WHERE idSPConfiguration='1' ;
        String query = "UPDATE SPConfiguration SET idSPConfiguration='" + c.getIdSPConfiguration() + "', description='" + c.getDESCRIPTION() +"', driver='" +
                c.getConfigurationDriver().getDriver() + "',param1= '" + c.getConfigurationDriver().getParam1() +"',param2= '" + c.getConfigurationDriver().getParam2() +"', hostController='" + c.getHostController() + "', apiOwner='" + c.getApiOwner() + "', mcu='" +
                c.getMcu() + "', protocol='" + c.getProtocol() + "', addressingType='" + c.getAddressingType() + "', idCluster='" +
                c.getIdCluster() + "' WHERE idSPConfiguration='" + c.getOldidSPConfiguration() + "' ;";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In update: " + e.getMessage());
        }
    }
}
