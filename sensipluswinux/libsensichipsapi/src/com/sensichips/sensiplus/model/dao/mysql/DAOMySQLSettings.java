package com.sensichips.sensiplus.model.dao.mysql;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.prefs.Preferences;

public class DAOMySQLSettings {

    private final static String key = "Credential";
    private static boolean  loaded = false;
    public final static String DRIVERNAME = "com.mysql.cj.jdbc.Driver";
    public final static String HOST = "localhost";
   // public final static String USERNAME = "sensiDB";//"Sensichips";
    public final static String USERNAME = "root";//"Sensichips";
   // public final static String PWD = "sensiDB";//"sensichips";
    public final static String PWD = "abadacu5";//"sensichips";
    public final static String SCHEMA = "sensidb";
    public final static String PARAMETERS = "?useSSL=false&serverTimezone=UTC";


    //private String driverName = "com.mysql.cj.jdbc.Driver";
    private String host = "";
    private String userName = "";
    private String pwd = "";
    private String schema = "";


    public DAOMySQLSettings() {

        Preferences prefs = Preferences.userNodeForPackage(DAOMySQLSettings.class);
        String loadData= prefs.get(key, "");

        if (!loadData.equals("")){
            String credential[] = loadData.split(",");
            host = credential[0];
            userName = credential[1];
            pwd = credential[2];
            schema = credential[3];
            loaded = true;
        }
    }

    public String getHost() {
        return host;
    }

    public String getUserName() {
        return userName;
    }

    public String getPwd() {
        return pwd;
    }

    public String getSchema() {
        return schema;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    static{
        try {
            Class.forName(DRIVERNAME);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static DAOMySQLSettings currentDAOMySQLSettings = null;

    public static DAOMySQLSettings getCurrentDAOMySQLSettings(){
        if (currentDAOMySQLSettings == null){
            currentDAOMySQLSettings = getDefaultDAOSettings();
        }
        return currentDAOMySQLSettings;
    }

    private static DAOMySQLSettings getDefaultDAOSettings(){
        DAOMySQLSettings daoMySQLSettings = new DAOMySQLSettings();
        if(!loaded) {
            daoMySQLSettings.host = HOST;
            daoMySQLSettings.userName = USERNAME;
            daoMySQLSettings.schema = SCHEMA;
            daoMySQLSettings.pwd = PWD;
        }
        return daoMySQLSettings;
    }

    public static void setCurrentDAOMySQLSettings(DAOMySQLSettings daoMySQLSettings){
        currentDAOMySQLSettings = daoMySQLSettings;
    }


    public static Statement getStatement() throws SQLException{
        if (currentDAOMySQLSettings == null){
            currentDAOMySQLSettings = getDefaultDAOSettings();
        }
        return DriverManager.getConnection("jdbc:mysql://" + currentDAOMySQLSettings.host  + "/" + currentDAOMySQLSettings.schema + PARAMETERS, currentDAOMySQLSettings.userName, currentDAOMySQLSettings.pwd).createStatement();
    }

    public static void closeStatement(Statement st) throws SQLException{
        st.getConnection().close();
        st.close();
    }

    public void saveCredential(String host, String userName, String pwd, String schema ) {

        Preferences prefs = Preferences.userNodeForPackage(DAOMySQLSettings.class);
        String loginData = host + "," + userName + "," + pwd + "," + schema;
        prefs.put(key, loginData);

    }
}