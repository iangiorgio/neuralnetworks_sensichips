package com.sensichips.sensiplus.model.dao;



import java.util.List;

public interface DAOChip <T,S,E,C> extends DAO<T> {
    String selectMN(T c, S f, E p) throws DAOException;
    List<T> selectChipOfCluster(C c) throws DAOException;
}
