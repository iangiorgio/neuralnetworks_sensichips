package com.sensichips.sensiplus.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"seAssociated", "calibrationParameter"/*"m","n"*/})
public class SensingElementOnChip {

    private String oldidSPCluster = null;
    private final StringProperty idSPChip;
    private final IntegerProperty idSensingElementOnFamily;
    private final StringProperty idSPCluster;
    private final IntegerProperty idSPPort;

    protected CalibrationParameter calibrationParameter;
  //  private final DoubleProperty m;
  //  private final DoubleProperty n;
    private final StringProperty idFamily;
    private StringProperty portName;
    private StringProperty seAssociated;



    public SensingElementOnChip() {
        this("", Integer.MIN_VALUE, "", Integer.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE, "");
    }

    public SensingElementOnChip (String idSPChip){
        this(idSPChip, Integer.MIN_VALUE, "", Integer.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE, "");
    }

    public SensingElementOnChip(String idSPChip, Integer idSensingElementOnFamily, String idSPCluster, Double m, Double n) {
        this(idSPChip, idSensingElementOnFamily, idSPCluster, Integer.MIN_VALUE, m, n, "");

    }


   public SensingElementOnChip(String idSPChip, Integer idSensingElementOnFamily, String idSPCluster,
                                Integer idSPPort, Double m, Double n, String idFamily) {
        this.idSPChip = new SimpleStringProperty(idSPChip);
        this.idSensingElementOnFamily = new SimpleIntegerProperty(idSensingElementOnFamily);
        this.idSPCluster = new SimpleStringProperty(idSPCluster);
        this.idSPPort = new SimpleIntegerProperty(idSPPort);

        this.calibrationParameter = new CalibrationParameter();
        this.calibrationParameter.setM(m);
        this.calibrationParameter.setN(n);
       // this.m = new SimpleDoubleProperty(m);
       // this.n = new SimpleDoubleProperty(n);
        this.idFamily = new SimpleStringProperty(idFamily);
        this.portName = new SimpleStringProperty();
        this.seAssociated = new SimpleStringProperty();

    }


    @XmlTransient
    public Integer getIdSensingElementOnFamily() {
        return idSensingElementOnFamily.getValue();
    }

    public IntegerProperty idSensingElementOnFamilyProperty() {
        return idSensingElementOnFamily;
    }

    public void setIdSensingElementOnFamily(int idSensingElementOnFamily) {
        this.idSensingElementOnFamily.set(idSensingElementOnFamily);
    }

    @XmlTransient
    public String getIdSPCluster() {
        return idSPCluster.get();
    }

    public StringProperty idSPClusterProperty() {
        return idSPCluster;
    }

    public void setIdSPCluster(String idSPCluster) {
        this.idSPCluster.set(idSPCluster);
    }

    @XmlTransient
    public Integer getIdSPPort() {
        return idSPPort.get();
    }

    public IntegerProperty idSPPortProperty() {
        return idSPPort;
    }

    public void setIdSPPort(int idSPPort) {
        this.idSPPort.set(idSPPort);
    }




    @XmlTransient
    public String getIdSPChip() {
        return idSPChip.get();
    }

    public StringProperty idSPChipProperty() {
        return idSPChip;
    }

    public void setIdSPChip(String idSPChip) {
        this.idSPChip.set(idSPChip);
    }

    @XmlTransient
    public String getIdFamily() {
        return idFamily.get();
    }

    public StringProperty idFamilyProperty() {
        return idFamily;
    }

    public void setIdFamily(String idFamily) {
        this.idFamily.set(idFamily);
    }

    @XmlTransient
    public String getOldidSPCluster() {
        return oldidSPCluster;
    }

    public void setOldidSPCalibration() {
        this.oldidSPCluster = this.idSPCluster.getValue();
    }

    @XmlElement(name = "SENSING_ELEMENT_ID", required = true)
    public String getSeAssociated() {
        return seAssociated.get();
    }

    public void setSeAssociated(String seAssociated) {
        this.seAssociated.set(seAssociated);
    }

    public StringProperty seAssociatedProperty() {
        return seAssociated;
    }



    @XmlElement (name="CALIBRATION_PARAMETER")
    public CalibrationParameter getCalibrationParameter() {
        return calibrationParameter;
    }

    public void setCalibrationParameter(CalibrationParameter calibrationParameter) {
        this.calibrationParameter = calibrationParameter;
    }

    @XmlTransient
    public String getPortName() {
        return portName.get();
    }

    public StringProperty portNameProperty() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName.set(portName);
    }

    public void clearOldidSPCalibration(){
        this.oldidSPCluster = null;
    }




}
