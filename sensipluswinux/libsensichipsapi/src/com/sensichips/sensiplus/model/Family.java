//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;


/**
 * <p>Classe Java per Family complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Family">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FAMILY_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FAMILY_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HW_VERSION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SYS_CLOCK" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OSC_TRIM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MEASURE_TYPE" maxOccurs="4">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="EIS"/>
 *               &lt;enumeration value="POT"/>
 *               &lt;enumeration value="ULTRASOUND"/>
 *               &lt;enumeration value="ENERGY_SPECTROSCOPY"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MULTICAST_DEFAULT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BROADCAST" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BROADCAST_SLOW" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SENSING_ELEMENT_ONFAMILY" type="{}SensingElementOnFamily" maxOccurs="unbounded"/>
 *         &lt;element name="ANALYTE" type="{}Analyte" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */


@XmlType(name = "Family", propOrder = {
        "id",
        "idSPFamily",
        "hwVersion",
        "sysclock",
        "osctrim",
        "measureTechniqueList",
        "multicastDefault",
        "broadcast",
        "broadcastSlow",
        "portList"


})
public class Family {
    protected StringProperty idSPFamily;

    protected StringProperty  id;

    protected StringProperty hwversion;

    protected StringProperty sysclock;

    protected StringProperty osctrim;

    protected StringProperty multicastDefault;

    protected StringProperty broadcast;

    protected StringProperty broadcastSlow;


    protected List<MeasureTechnique> measuretype;

    protected List<SensingElementOnFamily> sensingelementonfamily;

    protected List<Analyte> analyte;
    private List<Port> portList;

    private String oldidSPFamily = null;


    //costruttori
    public Family() {
        this("", "", "", "", "","","","");
    }

    public Family(String idSPFamily) {
        this(idSPFamily, "", "", "", "","","","");
    }

    public Family(String idSPFamily, String id, String hwVersion, String sysclock, String osctrim, String multicastDefault, String broadcast, String broadcastSlow) {
        this.idSPFamily = new SimpleStringProperty(idSPFamily);
        this.id = new SimpleStringProperty(id);
        this.hwversion = new SimpleStringProperty(hwVersion);
        this.sysclock = new SimpleStringProperty(sysclock);
        this.osctrim = new SimpleStringProperty(osctrim);
        this.multicastDefault = new SimpleStringProperty(multicastDefault);
        this.broadcast = new SimpleStringProperty(broadcast);
        this.broadcastSlow =new SimpleStringProperty(broadcastSlow);

    }

    @XmlElement(name = "MEASURE_TYPE")
    public String[] getMeasureTechniqueList() {
        String[] m = new String[measuretype.size()];
        for (int i =0; i<measuretype.size(); i++){
            m[i] = measuretype.get(i).getType();
        }
        return m;
    }

    public void setMeasureTechniqueList(List<MeasureTechnique> measureTechniqueList) {
        this.measuretype = measureTechniqueList;
    }


    @XmlElement(name = "SENSING_ELEMENT_ONFAMILY", required = true)
    public List<Port> getPortList(){
        return portList;
    }

    public void setPortList(List<Port> portList){
        this.portList = portList;
    }

    public String getOldidSPFamily() {
        return oldidSPFamily;
    }

    public void setOldidSPFamily(){
        this.oldidSPFamily = this.idSPFamily.get();
    }

    /**
     * Recupera il valore della propriet� familyname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */

    @XmlElement(name = "FAMILY_ID", required = true)

    public String getIdSPFamily() {
        return idSPFamily.get();
    }

    /**
     * Imposta il valore della propriet� familyname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSPFamily(String value) {
        this.idSPFamily.set(value);
    }
    public StringProperty idSPFamilyProperty() {
        return idSPFamily;
    }

    /**
     * Recupera il valore della propriet� familyid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "FAMILY_NAME", required = true)
    public String getId() {
        return id.get();
    }

    /**
     * Imposta il valore della propriet� familyid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id.set(value);
    }

    /**
     * Recupera il valore della propriet� hwversion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "HW_VERSION", required = true)
    public String getHwVersion() {
        return hwversion.get();
    }

    /**
     * Imposta il valore della propriet� hwversion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHwVersion(String value) {
        this.hwversion.set(value);
    }

    /**
     * Recupera il valore della propriet� sysclock.
     * 
     */
    @XmlElement(name = "SYS_CLOCK")
    public String getSysclock() {
        return sysclock.get();
    }

    /**
     * Imposta il valore della propriet� sysclock.
     * 
     */
    public void setSysclock(String value) {
        this.sysclock.set(value);
    }

    /**
     * Recupera il valore della propriet� osctrim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @XmlElement(name = "OSC_TRIM", required = true)
    public String getOsctrim() {
        return osctrim.get();
    }

    /**
     * Imposta il valore della propriet� osctrim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOsctrim(String value) {
        this.osctrim.set(value);
    }

    @XmlElement (name= "MULTICAST_DEFAULT", required= true)
    public String getMulticastDefault(){
        return multicastDefault.get();
    }

    public void setMulticastDefault(String value) {
        this.multicastDefault.set(value);
    }


    @XmlElement (name= "BROADCAST")
    public String getBroadcast(){
        return broadcast.get();
    }

    public void setBroadcast(String value){
        this.broadcast.set(value);
    }


    @XmlElement (name= "BROADCAST_SLOW")
    public String getBroadcastSlow(){
        return broadcastSlow.get();
    }

    public void setBroadcastSlow(String value){
        this.broadcastSlow.set(value);
    }

}
