//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SensingElementOnFamily complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SensingElementOnFamily">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="sensingElement" type="{}SensingElement" minOccurs="0"/>
 *         &lt;element name="SENSING_ELEMENT_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SENSING_ELEMENT_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SENSING_ELEMENT_PORT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SensingElementOnFamily", propOrder = {

})
public class SensingElementOnFamily {

    protected SensingElement sensingElement;
    @XmlElement(name = "SENSING_ELEMENT_ID", required = true)
    protected String sensingelementid;
    @XmlElement(name = "SENSING_ELEMENT_NAME", required = true)
    protected String sensingelementname;
    @XmlElement(name = "SENSING_ELEMENT_PORT", required = true)
    protected String sensingelementport;

    /**
     * Recupera il valore della propriet� sensingElement.
     * 
     * @return
     *     possible object is
     *     {@link SensingElement }
     *     
     */
    public SensingElement getSensingElement() {
        return sensingElement;
    }

    /**
     * Imposta il valore della propriet� sensingElement.
     * 
     * @param value
     *     allowed object is
     *     {@link SensingElement }
     *     
     */
    public void setSensingElement(SensingElement value) {
        this.sensingElement = value;
    }

    /**
     * Recupera il valore della propriet� sensingelementid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSENSINGELEMENTID() {
        return sensingelementid;
    }

    /**
     * Imposta il valore della propriet� sensingelementid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSENSINGELEMENTID(String value) {
        this.sensingelementid = value;
    }

    /**
     * Recupera il valore della propriet� sensingelementname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSENSINGELEMENTNAME() {
        return sensingelementname;
    }

    /**
     * Imposta il valore della propriet� sensingelementname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSENSINGELEMENTNAME(String value) {
        this.sensingelementname = value;
    }

    /**
     * Recupera il valore della propriet� sensingelementport.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSENSINGELEMENTPORT() {
        return sensingelementport;
    }

    /**
     * Imposta il valore della propriet� sensingelementport.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSENSINGELEMENTPORT(String value) {
        this.sensingelementport = value;
    }

}
