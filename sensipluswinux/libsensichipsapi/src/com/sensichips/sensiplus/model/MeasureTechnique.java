package com.sensichips.sensiplus.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

public class MeasureTechnique {

    private IntegerProperty idSPMeasureTechnique;
    private StringProperty type;

    public MeasureTechnique(){
        this(Integer.MIN_VALUE, "");
    }

    public MeasureTechnique(Integer idSPMeasureTechnique, String type){
        this.idSPMeasureTechnique = new SimpleIntegerProperty(idSPMeasureTechnique);
        this.type = new SimpleStringProperty(type);
    }

    @XmlTransient
    public Integer getIdSPMeasureTechnique() {
        return idSPMeasureTechnique.get();
    }

    public IntegerProperty idSPMeasureTechniqueProperty() {
        return idSPMeasureTechnique;
    }

    public void setIdSPMeasureTechnique(int idSPMeasureTechnique) {
        this.idSPMeasureTechnique.set(idSPMeasureTechnique);
    }

    @XmlElement(required = true)
    public String getType() {
        return type.get();
    }

    public StringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }
}
