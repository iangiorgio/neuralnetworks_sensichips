package com.sensichips.sensiplus.util;

import javax.swing.text.NumberFormatter;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Formatter;

public class SPDataRepresentation implements Serializable {

    public static final long serialVersionUID = 42L;

    public static int NUMBER_OF_SIGNIFICANT_DIGITS = 5;

    public String measureUnit = ""; //Volt, Ampere, ecc.
    public String prefix;
    public double range_min;
    public double range_max;
    public double alarm_threshold;
    public double values[][];
    public double meanValue;

    private int indexToReduce = 0;

    private static DecimalFormat formatter;


    public SPDataRepresentation(String measureUnit, String prefix, double range_min, double range_max, double alarm_threshold, double[][] values, int indexToReduce, double meanValue, boolean adaptFormatter) {
        this.measureUnit = measureUnit;
        this.prefix = prefix;
        this.range_min = range_min;
        this.range_max = range_max;
        this.alarm_threshold = alarm_threshold;
        this.values = values;
        this.indexToReduce = indexToReduce;
        this.meanValue = meanValue;

        if(adaptFormatter)
            formatter = formatWithFixedNumberOfSignificantDigits(meanValue, NUMBER_OF_SIGNIFICANT_DIGITS);


    }



    public static DecimalFormat getFormatter(){
        return formatter;
    }

    public static DecimalFormat formatWithFixedNumberOfSignificantDigits(double value, int numberOfSignificantDigits){

        String valueString = "" + value;
        String tokens[] = valueString.split("\\.");

        String format = "##0.";

        for(int i = tokens[0].length(); i < numberOfSignificantDigits; i++){
            format = format + "0";
        }

        return new DecimalFormat(format);
    }


    public String toString(){
        String out = "[";
        for(int i = 0; i < values.length; i++){
            out += formatter.format(values[i][indexToReduce]) + (i < (values.length - 1)? ", " : "");
        }
        return out + "] " + "[mean: " + meanValue + "] " + prefix + measureUnit + " [" + formatter.format(range_min) + ", " + formatter.format(range_max) + "]; alarm_threshold = " + formatter.format(alarm_threshold);
    }

}
