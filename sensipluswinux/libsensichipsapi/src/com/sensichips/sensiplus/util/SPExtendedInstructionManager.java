package com.sensichips.sensiplus.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;


import com.sensichips.sensiplus.level1.chip.SPPort;
import com.sensichips.sensiplus.level1.protocols.esp8266.SPProtocolESP8266_SENSIBUS;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacketExtended;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterADC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPOT;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterSENSORS;
import com.sensichips.sensiplus.level2.parameters.items.*;
import com.sensichips.sensiplus.model.*;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.ChipDAOMYSQLImpl;
import com.sensichips.sensiplus.model.dao.mysql.FamilyDAOMYSQLImpl;
import com.sensichips.sensiplus.model.dao.mysql.SensingElementOnChipDAOMYSQLImpl;
import org.apache.commons.codec.binary.Hex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.util.*;

public final class SPExtendedInstructionManager {

    public static final int EX_NUMBER_OF_CHIP = 0x82;
    public static final int EX_DETECT_CHIP = 0x83;
    public static final int EX_SET_EIS = 0x90;
    public static final int EX_SET_ADC = 0x91;
    public static final int EX_GET_EIS = 0x92;
    public static final int EX_SET_SENSOR = 0x93;
    public static final int EX_GET_SENSOR = 0x94;
    public static final int EX_SET_POT = 0x95;
    public static final int EX_GET_POT = 0x96;
    public static final int EX_SEND_INIT = 0x97;

    private static boolean forceInit = true;

    private SPExtendedInstructionManager() {
    }

    public static void setForceInit(boolean forceInit) {
        SPExtendedInstructionManager.forceInit = forceInit;
    }

    public static SPProtocolPacketExtended getPacketForSetEIS(SPMeasurementParameterEIS param, int instance_ID) throws SPException{
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        int SIGNED_MASK = 255; // 11111111

        byte[] dataPacket = new byte[15];
        byte opCode = (byte)(EX_SET_EIS);
        byte payloadSize = 0x0D; //13 byte

        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        //Imposto il byte 1 del PAYLOAD
        if(Arrays.asList(SPParamItemRSense.rsenseValues).contains(param.getRsense())){
            int index = Arrays.asList(SPParamItemRSense.rsenseValues).indexOf(param.getRsense());
            byte rSense = (byte)((index & SIGNED_MASK) << 6);
            dataPacket[2] = rSense;
        }

        if(Arrays.asList(SPParamItemInGain.ingainValues).contains(param.getInGain())){
            int index = Arrays.asList(SPParamItemInGain.ingainValues).indexOf(param.getInGain());
            byte inGain = (byte)((index & SIGNED_MASK) << 4);
            dataPacket[2] = (byte)(dataPacket[2] | inGain);
        }

        if(Arrays.asList(SPParamItemHarmonic.harmonicValues).contains(param.getHarmonic())){
            int index = Arrays.asList(SPParamItemHarmonic.harmonicValues).indexOf(param.getHarmonic());
            byte harmonic = (byte)((index & SIGNED_MASK) << 2);
            dataPacket[2] = (byte)(dataPacket[2] | harmonic);
        }

        if(Arrays.asList(SPParamItemModeVI.ModeVIValues).contains(param.getModeVI())){
            int index = Arrays.asList(SPParamItemModeVI.ModeVIValues).indexOf(param.getModeVI());
            byte modeVI = (byte)((index & SIGNED_MASK));
            dataPacket[2] = (byte)(dataPacket[2] | modeVI);
        }

        //Imposto il byte 2 del PAYLOAD
        if(Arrays.asList(SPParamItemOutGain.outgainValues).contains(param.getOutGain())){
            int index = Arrays.asList(SPParamItemOutGain.outgainValues).indexOf(param.getOutGain());
            byte outGain = (byte)((index & SIGNED_MASK) << 5);
            dataPacket[3] = outGain;
        }

        if(Arrays.asList(SPPort.portValues).contains(param.getInPort())){
            int index = Arrays.asList(SPPort.portValues).indexOf(param.getInPort());
            byte inPort = (byte)(index & SIGNED_MASK);
            dataPacket[3] = (byte)(dataPacket[3] | inPort);
        }

        //Imposto il byte 3 del PAYLOAD
        if(Arrays.asList(SPParamItemQI.I_QValues).contains(param.getQI())){
            int index = Arrays.asList(SPParamItemQI.I_QValues).indexOf(param.getQI());
            byte qi = (byte)((index & SIGNED_MASK) << 6);
            dataPacket[4] = qi;
        }

        if(Arrays.asList(SPParamItemContacts.ContactsValues).contains(param.getContacts())){
            int index = Arrays.asList(SPParamItemContacts.ContactsValues).indexOf(param.getContacts());
            byte contacts = (byte)((index & SIGNED_MASK) << 5);
            dataPacket[4] = (byte)(dataPacket[4] | contacts);
        }

        if(Arrays.asList(SPPort.portValues).contains(param.getOutPort())){
            int index = Arrays.asList(SPPort.portValues).indexOf(param.getOutPort());
            byte outPort = (byte)(index & SIGNED_MASK);
            dataPacket[4] = (byte)(dataPacket[4] | outPort);
        }

        //Imposto il byte 4 del PAYLOAD
        int dcBiasN = param.getDCBiasN() + 32;
        if(dcBiasN >= 0 && dcBiasN <= 63){
            dataPacket[5] = (byte)dcBiasN;
        }

        //Imposto il byte 5 del PAYLOAD
        if(Arrays.asList(SPParamItemMeasures.measureValues).contains(param.getMeasure())){
            int index = Arrays.asList(SPParamItemMeasures.measureValues).indexOf(param.getMeasure());
            byte measure = (byte)((index & SIGNED_MASK) << 3);
            dataPacket[6] = (byte)(dataPacket[6] | measure);
        }

        if(Arrays.asList(SPParamItemFilter.filterValues).contains(param.getFilter())){
            int index = Arrays.asList(SPParamItemFilter.filterValues).indexOf(param.getFilter());
            byte filter = (byte)(index & SIGNED_MASK);
            dataPacket[6] = (byte)(dataPacket[6] | filter);
        }
        int renewBuffer = (param.getSPMeasurementMetaParameter().getRenewBuffer() == true ? 1 : 0) << 7;
        dataPacket[6] = (byte) (dataPacket[6] | renewBuffer);

        //Imposto il byte 6 del PAYLOAD
        int phaseShift = Integer.parseInt(param.getPhaseShiftLabel());
        dataPacket[7] = (byte)((phaseShift));
        if(Arrays.asList(SPParamItemPhaseShiftMode.PhaseShiftModeValues).contains(param.getPhaseShiftMode())){
            int index = Arrays.asList(SPParamItemPhaseShiftMode.PhaseShiftModeValues).indexOf(param.getPhaseShiftMode());
            byte phaseShiftMode = (byte)((index & SIGNED_MASK) << 5);
            dataPacket[7] = (byte)(dataPacket[7] | phaseShiftMode);
        }
        //Imposto il byte 7-8
        int FIRSTBYTE_MASK = 255; // 11111111
        int appo = param.getDCBiasP() + 2048;
        dataPacket[8] = (byte)(appo >> 8);
        dataPacket[9] = (byte)(appo & FIRSTBYTE_MASK);

        //imposto il byte 9-10-11-12
        byte[] frequency = new byte[4];
        ByteBuffer.wrap(frequency).putFloat(param.getFrequency());
        dataPacket[10] = frequency[0];
        dataPacket[11] = frequency[1];
        dataPacket[12] = frequency[2];
        dataPacket[13] = frequency[3];

        //imposto il byte 13
        dataPacket[14] = (byte) instance_ID;
        System.out.println(Hex.encodeHexString(dataPacket));

        out.setDataLengthExpected(2);
        out.setByteReceived(dataPacket);
        //throw new SPException("Not yet implemented!");

        return out;
    }

    public static SPProtocolPacketExtended getPacketForSetADC(SPMeasurementParameterADC param) throws SPException{
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        byte[] dataPacket = new byte[4];
        byte opCode = (byte)(EX_SET_ADC);
        byte payloadSize = 0x02;
        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        if(Arrays.asList(param.inPortADCLabels).contains(param.getInPortADC())){
            int index = Arrays.asList(param.inPortADCValues).indexOf(param.getInPortADC());
            byte inPort = (byte) (index << 4);
            dataPacket[2] = inPort;
        }

        byte DATA_MASK = 0x0F; //00001111
        byte fifoDeep = (byte) ((byte )param.getFIFO_DEEP() & DATA_MASK);
        dataPacket[2] = (byte)(dataPacket[2] | fifoDeep);

        byte fifoRead = (byte) ((byte)param.getFIFO_READ() & DATA_MASK);
        dataPacket[3] = fifoRead;

        if(Arrays.asList(SPParamItemInGain.ingainValues).contains(param.getInGain())){
            int index = Arrays.asList(SPParamItemInGain.ingainValues).indexOf(param.getInGain());
            byte inGain = (byte)(index << 4);
            dataPacket[3] = (byte)(dataPacket[3] | inGain);
        }

        System.out.println(Hex.encodeHexString(dataPacket)); //stampa di debug
        out.setDataLengthExpected(2);
        out.setByteReceived(dataPacket);
        //throw new SPException("Not yet implemented!");
        return out;
    }

    public static SPProtocolPacketExtended getPacketForGetEIS(int numOfChips) throws SPException{
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        byte[] dataPacket = new byte[2];
        byte opCode = (byte)(EX_GET_EIS);
        byte payloadSize = 0x00;

        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        //System.out.println(Hex.encodeHexString(dataPacket)); //stampa di debug
        int byteToRecive = (numOfChips * 72) + 2;

        out.setDataLengthExpected(byteToRecive);
        out.setByteReceived(dataPacket);

        return out;
    }

    //inizializzo tutti i sensori presenti sui chip 8105 e 8505
    private static void initSensorMap(Map<String, Integer> map){
        map.put("ONCHIP_DARK", 0);
        map.put("ONCHIP_LIGHT", 1);
        map.put("ONCHIP_TEMPERATURE", 2);
        map.put("ONCHIP_VOLTAGE", 3);
        map.put("ONCHIP_VOC_PORT0", 4);
        map.put("ONCHIP_VOC_PORT1", 5);
        map.put("ONCHIP_VOC_PORT2", 6);
        map.put("ONCHIP_VOC_PORT3", 7);
        map.put("ONCHIP_VOC_PORT4", 8);
        map.put("ONCHIP_ALUMINUM_OXIDE", 9);
        map.put("ONCHIP_UREA", 10);
        map.put("ONCHIP_VOC_PORT7", 11);
        map.put("ONCHIP_VOC_PORT8", 12);
        map.put("ONCHIP_VOC_PORT9", 13);
        map.put("ONCHIP_VOC_PORT10", 14);
        map.put("ONCHIP_POLYMIDE", 15);
        //questi ultimi 3 non so utilizzate dai chip 8105 e 8505 presenti sul MCU
        map.put("OFFCHIP_HP_DC", 16);
        map.put("OFFCHIP_HP_AC", 17);
        map.put("OFFCHIP_AU_GRAPHENE_EXT3", 18);

    }

    public static SPProtocolPacketExtended getPacketForSetSENSOR(SPMeasurementParameterSENSORS param, int measureIndexToSave, int instance_ID) throws SPException {
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();
        Map<String, Integer> sensorMap = new HashMap<String, Integer>();
        initSensorMap(sensorMap);
        int sensorCode = sensorMap.get(param.getSensorName());
        int measureIndexMask = 31; //00011111

        byte[] dataPacket = new byte[4];
        byte opCode = (byte) (EX_SET_SENSOR);
        byte payloadSize = (byte) (0x02);
        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;
        dataPacket[3] = (byte) (param.getSPMeasurementMetaParameter().getRenewBuffer() == true ? 0x01 : 0x00);

        byte instance_id =  (byte) (instance_ID << 1);
        dataPacket[3] = (byte) (dataPacket[3] | instance_id);

        if(sensorCode > 3){
            int dataToSend = measureIndexToSave << 5;
            dataToSend = dataToSend | sensorCode;
            dataPacket[2] = (byte) dataToSend;
        } else {
            dataPacket[2] = (byte) sensorCode;
        }


        System.out.println(Hex.encodeHexString(dataPacket)); //stampa di debug
        out.setByteReceived(dataPacket);
        out.setDataLengthExpected(2);

        return out;
    }

    public static SPProtocolPacketExtended getPacketForGetSENSOR(int numOfChips) throws SPException{
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        byte[] dataPacket = new byte[2];
        byte opCode = (byte)(EX_GET_SENSOR);
        byte payloadSize = 0x00;
        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        int byteToRecive = (numOfChips * 72) + 2;
        out.setByteReceived(dataPacket);
        out.setDataLengthExpected(byteToRecive);

        return out;
    }

    public static SPProtocolPacketExtended getPacketForSetPOT(SPMeasurementParameterPOT param, int instance_ID) throws SPException {
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        int SIGNED_MASK = 255; // 11111111

        byte[] dataPacket = new byte[15];
        byte opCode = (byte)(EX_SET_POT);
        byte payloadSize = 0x0D; //13 byte

        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        //Imposto il byte 1-2 del PAYLOAD
        byte[] initialPotential = new byte[2];
        ByteBuffer.wrap(initialPotential).putShort(param.getInitialPotential().shortValue());
        dataPacket[2] = initialPotential[0];
        dataPacket[3] = initialPotential[1];

        //Imposto il byte 3-4 del PAYLOAD
        byte[] finalPotential = new byte[2];
        ByteBuffer.wrap(finalPotential).putShort(param.getFinalPotential().shortValue());
        dataPacket[4] = finalPotential[0];
        dataPacket[5] = finalPotential[1];

        //Imposto il byte 5-6 del PAYLOAD
        byte[] step = new byte[2];
        ByteBuffer.wrap(step).putShort(param.getStep().shortValue());
        dataPacket[6] = step[0];
        dataPacket[7] = step[1];

        //Imposto il byte 7-8 del PAYLOAD
        byte[] pulsePeriod = new byte[2];
        ByteBuffer.wrap(pulsePeriod).putShort(param.getPulsePeriod().shortValue());
        dataPacket[8] = pulsePeriod[0];
        dataPacket[9] = pulsePeriod[1];

        //Imposto il byte 9-10 del PAYLOAD
        byte[] pulseAmplitude = new byte[2];
        ByteBuffer.wrap(pulseAmplitude).putShort(param.getPulseAmplitude().shortValue());
        dataPacket[10] = pulseAmplitude[0];
        dataPacket[11] = pulseAmplitude[1];

        //Imposto il byte 11 del PAYLOAD
        byte alternativeSignal = (byte) ((param.getAlternativeSignal() ? 1 : 0) & SIGNED_MASK);
        dataPacket[12] = alternativeSignal;

        if(Arrays.asList(SPParamItemContacts.ContactsValues).contains(param.getContacts())){
            int index = Arrays.asList(SPParamItemContacts.ContactsValues).indexOf(param.getContacts());
            byte contacts = (byte)((index & SIGNED_MASK) << 1);
            dataPacket[12] = (byte)(dataPacket[12] | contacts);
        }

        if(Arrays.asList(SPParamItemRSense.rsenseValues).contains(param.getRsense())){
            int index = Arrays.asList(SPParamItemRSense.rsenseValues).indexOf(param.getRsense());
            byte rSense = (byte)((index & SIGNED_MASK) << 2);
            dataPacket[12] = (byte)(dataPacket[12] | rSense);
        }

        if(Arrays.asList(SPParamItemPOTType.typeValues).contains(param.getTypeVoltammetry())){
            int index = Arrays.asList(SPParamItemPOTType.typeValues).indexOf(param.getTypeVoltammetry());
            byte type = (byte)((index & SIGNED_MASK) << 4);
            dataPacket[12] = (byte)(dataPacket[12] | type);
        }

        int renewBuffer = (param.getSPMeasurementMetaParameter().getRenewBuffer() == true ? 1 : 0) << 7;
        dataPacket[12] = (byte) (dataPacket[12] | renewBuffer);

        //imposto il byte 12 del PAYLOAD
        if(Arrays.asList(SPParamItemInGain.ingainValues).contains(param.getInGain())){
            int index = Arrays.asList(SPParamItemInGain.ingainValues).indexOf(param.getInGain());
            byte inGain = (byte)(index & SIGNED_MASK);
            dataPacket[13] = inGain;
        }

        if(Arrays.asList(SPPort.portValues).contains(param.getPort())){
            int index = Arrays.asList(SPPort.portValues).indexOf(param.getPort());
            byte port = (byte)((index & SIGNED_MASK) << 2);
            dataPacket[13] = (byte)(dataPacket[13] | port);
        }


        //imposto il byte 13 del PAYLOAD
        dataPacket[14] = (byte) instance_ID;
        System.out.println(Hex.encodeHexString(dataPacket)); //stampa di debug

        out.setDataLengthExpected(2);
        out.setByteReceived(dataPacket);
        //throw new SPException("Not yet implemented!");

        return out;
    }

    public static SPProtocolPacketExtended getPacketForGetPOT(int numOfChips) throws SPException{
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        byte[] dataPacket = new byte[2];
        byte opCode = (byte)(EX_GET_POT);
        byte payloadSize = 0x00;

        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        //System.out.println(Hex.encodeHexString(dataPacket)); //stampa di debug
        int byteToRecive = (numOfChips * 16) + 2;

        out.setDataLengthExpected(byteToRecive);
        out.setByteReceived(dataPacket);

        return out;
    }

    public static void detectChip(SPConfiguration spConfiguration, SPProtocol protocol)throws SPDriverException, SPProtocolException {
        SPProtocolPacketExtended packet = new SPProtocolPacketExtended();
        byte[] dataPacket = new byte[2];
        byte opCode = (byte)(EX_NUMBER_OF_CHIP);
        byte payloadSize = 0x00;

        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        packet.setDataLengthExpected(3);
        packet.setByteReceived(dataPacket);

        byte[] received_header = protocol.sendPacket(packet);

        int numOfChips = received_header[0];
        boolean initJson = received_header[0] == 1;

        opCode = (byte)(EX_DETECT_CHIP);
        payloadSize = 0x00;

        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        int byteToReceive = (numOfChips * 5) + 2;

        packet.setDataLengthExpected(byteToReceive);
        packet.setByteReceived(dataPacket);

        byte[] received = protocol.sendPacket(packet);

        String chipID[] = new String[numOfChips];
        byte appo[] = new byte[5];
        System.out.println("Chip Detected: ");
        for(int i = 0; i < numOfChips; i++){
            System.arraycopy(received, i*5, appo, 0, 5);
            chipID[i] = "0x" + Hex.encodeHexString(appo);
            System.out.println(chipID[i]);
        }

       if(initJson || forceInit){
            generateConfiguration(spConfiguration, chipID);
        }

    }

    private static void generateConfiguration(SPConfiguration spConfiguration, String[] chipsID){
        //Gli oggetti di seguito sono definiti in com.sensichips.sensiplus.model (quelli utilizzati per il data base)
        Cluster dbCluster = new Cluster();
        List<Chip> dbChipList = new ArrayList<>();
        Family dbFamily = new Family();
        List<Family> familyList;
        List<SensingElementOnChip> dbSeOnChipList = null;
        List<Port> appoPortList;
        List<Port> dbPortList = new ArrayList<>();
        List<SensingElement> dbSeList = new ArrayList<>();
        Analyte dbAnalyte = new Analyte();

        try {
            for ( int i = 0; i < chipsID.length; i++){
                List<Chip> chipList = ChipDAOMYSQLImpl.getInstance().select(new Chip(chipsID[i],"",""));
                dbChipList.add(chipList.get(0));
            }
            dbFamily.setIdSPFamily(dbChipList.get(0).getSpFamily_idSPFamily());
            familyList = FamilyDAOMYSQLImpl.getInstance().select(dbFamily);
            dbFamily = familyList.get(0);
            dbCluster.setIdSPFamily(dbFamily.getIdSPFamily());
            dbCluster.setIdCluster("0x05");
            dbCluster.setMULTICASTCLUSTER("0x0000000011");

            //Seleziono tutte le porte e i sensing element associati alla Family

            //tramite query vedo per una determinata famiglia quali porte sono state inserite, e le salvo nella lista
            appoPortList = FamilyDAOMYSQLImpl.getInstance().selectInPort(dbFamily);
            //per porta inserita va a associare un sensing element. Nel caso il se è presente associa il se alla porta
            //altrimenti elimina la porta a cui non è associato il se
            for (int i = 0; i < appoPortList.size(); i++) {
                SensingElement dbSe = (SensingElement) FamilyDAOMYSQLImpl.getInstance().selectSensingElementIn(dbFamily, appoPortList.get(i));
                if (dbSe != null) {
                    appoPortList.get(i).setSeAssociated(dbSe.getIdSPSensingElement());
                    appoPortList.get(i).setSeNameAssociated(dbSe.getName());
                    dbPortList.add(appoPortList.get(i));
                    dbSeList.add(dbSe);
                }
            }
            //seleziono tutti i sensing element on chip ed i calibration parameter associati al dato chip
            for (Chip chip : dbChipList) {
                dbSeOnChipList = new ArrayList<>();
                chip.setSeOnChipList(dbSeOnChipList);
                for (Port port : dbPortList) {
                    SensingElementOnChip dbSeOnChip = new SensingElementOnChip();
                    dbSeOnChip.setIdSPCluster(dbCluster.getIdCluster());
                    dbSeOnChip.setIdFamily(dbCluster.getIdSPFamily());
                    dbSeOnChip.setIdSPChip(chip.getIdSPChip());
                    dbSeOnChip.setPortName(port.getName());
                    dbSeOnChip.setSeAssociated(port.getSeAssociated());
                    dbSeOnChip.setIdSPPort(port.getIdSPPort());
                    List<SensingElementOnChip> appoListSeOnChip = SensingElementOnChipDAOMYSQLImpl.getInstance().select(dbSeOnChip);
                    appoListSeOnChip.get(0).setSeAssociated(dbSeOnChip.getSeAssociated());
                    appoListSeOnChip.get(0).setPortName(dbSeOnChip.getPortName());
                    appoListSeOnChip.get(0).setIdSPPort(dbSeOnChip.getIdSPPort());
                    dbSeOnChipList.add(appoListSeOnChip.get(0));
                }
            }

            initLayoutconfigurationParameters(spConfiguration, dbCluster, dbFamily, dbChipList, dbPortList, dbSeList, dbSeOnChipList);

        } catch (DAOException e) {
            e.printStackTrace();
        }


    }

    private static void initLayoutconfigurationParameters(SPConfiguration spConfiguration, Cluster dbCluster, Family dbFamily, List<Chip> dbChipList, List<Port> dbPortList, List<SensingElement> dbSeList,
                                                          List<SensingElementOnChip> dbSeOnChipList ){
        com.sensichips.sensiplus.config.configuration_handler.Configuration layoutConfiguration;
        List<com.sensichips.sensiplus.config.configuration_handler.Chip> layoutChipList = new ArrayList<>();
        com.sensichips.sensiplus.config.configuration_handler.Chip layoutChip = new com.sensichips.sensiplus.config.configuration_handler.Chip();
        com.sensichips.sensiplus.config.configuration_handler.Family layoutFamily = new com.sensichips.sensiplus.config.configuration_handler.Family();
        List<com.sensichips.sensiplus.config.configuration_handler.SensingElementOnChip> layoutSeOnChipList = new ArrayList<>();

        //Inizializzo la configurazione
        layoutConfiguration = initLayoutConfiguration(spConfiguration, dbCluster, dbChipList, dbFamily, dbPortList);


        //inizializzo i SensingElementOnFamily
        for(SensingElement dbSe : dbSeList){


        }


    }

    private static com.sensichips.sensiplus.config.configuration_handler.SensingElement initLayoutSensingElement(SensingElement dbSe){
        com.sensichips.sensiplus.config.configuration_handler.SensingElement layoutSe = new com.sensichips.sensiplus.config.configuration_handler.SensingElement();

        layoutSe.setNAME(dbSe.getName());
        layoutSe.setConversionRate(dbSe.getConversionRate());
        layoutSe.setInPortADC(dbSe.getInportADC());
        layoutSe.setNData(dbSe.getNData());
        layoutSe.setMeasureTechnique(dbSe.getMeasureTechnique());
        layoutSe.setFilter(dbSe.getFilter());
        layoutSe.setRANGEMIN(dbSe.getRangeMin());
        layoutSe.setRANGEMAX(dbSe.getRangeMax());
        layoutSe.setDEFAULTALARMTHRESHOLD(dbSe.getDefaultAlarmThreshold());
        layoutSe.setMULTIPLIER(dbSe.getMultiplier());
        layoutSe.setMEASUREUNIT(dbSe.getMeasure_Unit());

        if(!layoutSe.getMeasureTechnique().equals("DIRECT")) {
            layoutSe.setContacts(dbSe.getContacts());
            layoutSe.setDCBiasN(dbSe.getDcBiasN());
            layoutSe.setDCBiasP(dbSe.getDcBiasP());
            layoutSe.setPhaseShift(dbSe.getPhaseShift());
            layoutSe.setPhaseShiftMode(dbSe.getPhaseShiftMode());
            layoutSe.setInGain(dbSe.getInGain());
            layoutSe.setOutGain(dbSe.getOutGain());
            layoutSe.setFrequency((float) dbSe.getFrequency()); //Controllare questo tipo lato database
            layoutSe.setHarmonic(dbSe.getHarmonic());
            layoutSe.setModeVI(dbSe.getModeVI());
            layoutSe.setRsense(dbSe.getRSense());
            layoutSe.setIQ(dbSe.getIq());
            layoutSe.setSequentialMode(dbSe.getSequentialMode().toString());
            layoutSe.setMeasureType(dbSe.getMeasureType());
        }

        return layoutSe;
    }

    private static List<com.sensichips.sensiplus.config.configuration_handler.SensingElementOnFamily> initLayoutSensingElementOnFamily(List<Port> dbPortList, Family dbFamily){
        List<com.sensichips.sensiplus.config.configuration_handler.SensingElementOnFamily> layoutSeOnFamilyList = new ArrayList<>();
        for(Port dbPort : dbPortList){
            try {
                SensingElement dbSe = (SensingElement) FamilyDAOMYSQLImpl.getInstance().selectSensingElementIn(dbFamily, dbPort);
                com.sensichips.sensiplus.config.configuration_handler.SensingElementOnFamily layoutSeOnFamily = new com.sensichips.sensiplus.config.configuration_handler.SensingElementOnFamily();
                layoutSeOnFamily.setSENSINGELEMENTID(dbSe.getIdSPSensingElement());
                layoutSeOnFamily.setSENSINGELEMENTPORT(dbPort.getName());
                layoutSeOnFamily.setSENSINGELEMENTNAME(dbSe.getName());
                layoutSeOnFamily.setSensingElement(initLayoutSensingElement(dbSe));
                layoutSeOnFamilyList.add(layoutSeOnFamily);
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        return layoutSeOnFamilyList;
    }

    private static List<String> initLayoutMeasureType(Family dbFamily){
        List<String> layoutMeasureType = new ArrayList<>();
        List<MeasureTechnique> dbMeasureTypeList = null;
        try {
            dbMeasureTypeList = FamilyDAOMYSQLImpl.getInstance().selectInMeasureTechnique(dbFamily);
            dbFamily.setMeasureTechniqueList(dbMeasureTypeList);
            for(int i = 0; i < dbFamily.getMeasureTechniqueList().length; i++){
               layoutMeasureType.add(dbFamily.getMeasureTechniqueList()[i]);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return layoutMeasureType;
    }

    private static com.sensichips.sensiplus.config.configuration_handler.Family initLayoutFamily(Family dbFamily, List<Port> dbPortList){
        com.sensichips.sensiplus.config.configuration_handler.Family layoutFamily = new com.sensichips.sensiplus.config.configuration_handler.Family();
        layoutFamily.setFAMILYID(dbFamily.getIdSPFamily());
        layoutFamily.setFAMILYNAME(dbFamily.getId());
        layoutFamily.setHWVERSION(dbFamily.getHwVersion());
        layoutFamily.setSYSCLOCK(Integer.parseInt(dbFamily.getSysclock()));
        layoutFamily.setOSCTRIM(dbFamily.getOsctrim());
        layoutFamily.setMeasuretype(initLayoutMeasureType(dbFamily));
        layoutFamily.setMULTICASTDEFAULT(dbFamily.getMulticastDefault());
        layoutFamily.setBROADCAST(dbFamily.getBroadcast());
        layoutFamily.setBROADCASTSLOW(dbFamily.getBroadcastSlow());
        layoutFamily.setSensingelementonfamily(initLayoutSensingElementOnFamily(dbPortList, dbFamily));

        //TODO: Prevedere inserimento Analyte una volta che sarà definito nel DataBase
        //layoutFamily.setAnalyte(initLayoutAnalyte(dbFamily));

        return layoutFamily;
    }

    private static int findIdPort(List<Port> dbPortList, String portName){
        int idPort = -1;
        for(Port port : dbPortList){
            if (port.getName().equals(portName)){
                return port.getIdSPPort();
            }
        }
        return idPort;
    }

    private static com.sensichips.sensiplus.config.configuration_handler.CalibrationParameter initLayoutCalibrationParameter (
            com.sensichips.sensiplus.config.configuration_handler.SensingElementOnFamily layoutSeOnFamily, Cluster dbCluster, Chip dbChip, List<Port> dbPortList){

        com.sensichips.sensiplus.config.configuration_handler.CalibrationParameter layoutCalibrationParameter = new com.sensichips.sensiplus.config.configuration_handler.CalibrationParameter();
        SensingElementOnChip dbSeOnChip = new SensingElementOnChip();
        try {
            dbSeOnChip.setIdSPCluster(dbCluster.getIdCluster());
            dbSeOnChip.setIdFamily(dbCluster.getIdSPFamily());
            dbSeOnChip.setIdSPChip(dbChip.getIdSPChip());
            dbSeOnChip.setPortName(layoutSeOnFamily.getSENSINGELEMENTPORT());
            dbSeOnChip.setSeAssociated(layoutSeOnFamily.getSENSINGELEMENTNAME());
            dbSeOnChip.setIdSPPort(findIdPort(dbPortList, layoutSeOnFamily.getSENSINGELEMENTPORT()));
            List<SensingElementOnChip> appoListSeOnChip = null;
            appoListSeOnChip = SensingElementOnChipDAOMYSQLImpl.getInstance().select(dbSeOnChip);
            //appoListSeOnChip.get(0).setSeAssociated(dbSeOnChip.getSeAssociated());
            //appoListSeOnChip.get(0).setPortName(dbSeOnChip.getPortName());
            //appoListSeOnChip.get(0).setIdSPPort(dbSeOnChip.getIdSPPort());
            layoutCalibrationParameter.setM(appoListSeOnChip.get(0).getCalibrationParameter().getM());
            layoutCalibrationParameter.setN(appoListSeOnChip.get(0).getCalibrationParameter().getN());
            if(appoListSeOnChip.get(0).getCalibrationParameter().getThreshold() != null) {
                layoutCalibrationParameter.setThreshold(appoListSeOnChip.get(0).getCalibrationParameter().getThreshold());
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return layoutCalibrationParameter;
    }

    private static com.sensichips.sensiplus.config.configuration_handler.SensingElementOnChip initLayoutSensingElementOnChip(
            com.sensichips.sensiplus.config.configuration_handler.SensingElementOnFamily layoutSeOnFamily, Cluster dbCluster, Chip dbChip, List<Port> dbPortList){

        com.sensichips.sensiplus.config.configuration_handler.SensingElementOnChip layoutSeOnChip = new com.sensichips.sensiplus.config.configuration_handler.SensingElementOnChip();
        layoutSeOnChip.setSensingElementOnFamily(layoutSeOnFamily);
        layoutSeOnChip.setSENSINGELEMENTID(layoutSeOnFamily.getSENSINGELEMENTID());
        layoutSeOnChip.setCALIBRATIONPARAMETER(initLayoutCalibrationParameter(layoutSeOnFamily, dbCluster, dbChip, dbPortList));

        return layoutSeOnChip;
    }

    private static List<com.sensichips.sensiplus.config.configuration_handler.SensingElementOnChip> initLayoutSensingElementOnChipList(
            List<com.sensichips.sensiplus.config.configuration_handler.SensingElementOnFamily> layoutSeOnFamilyList, Cluster dbCluster, Chip dbChip, List<Port> dbPortList){

        List<com.sensichips.sensiplus.config.configuration_handler.SensingElementOnChip> layoutSeOnChipList = new ArrayList<>();
        for(com.sensichips.sensiplus.config.configuration_handler.SensingElementOnFamily layoutSeOnFamily : layoutSeOnFamilyList){
            layoutSeOnChipList.add(initLayoutSensingElementOnChip(layoutSeOnFamily, dbCluster, dbChip, dbPortList));
        }
        return layoutSeOnChipList;
    }

    private static com.sensichips.sensiplus.config.configuration_handler.Chip initLayoutChip(Chip dbChip, Family dbFamily, List<Port> dbPortList, Cluster dbCluster){
        com.sensichips.sensiplus.config.configuration_handler.Chip layoutChip = new com.sensichips.sensiplus.config.configuration_handler.Chip();
        layoutChip.setFAMILYID(dbChip.getSpFamily_idSPFamily());
        layoutChip.setSERIALNUMBER(dbChip.getIdSPChip());
        layoutChip.setI2CADDRESS(dbChip.getCAddress());
        layoutChip.setSpFamily(initLayoutFamily(dbFamily, dbPortList));
        layoutChip.setSensingelementonchip(initLayoutSensingElementOnChipList(layoutChip.getSpFamily().getSENSINGELEMENTONFAMILY(), dbCluster, dbChip, dbPortList));

        return layoutChip;
    }

    private static List<com.sensichips.sensiplus.config.configuration_handler.Chip> initLayoutChipList(List<Chip> dbChipList, Family dbFamily, List<Port> dbPortList, Cluster dbCluster){
        List<com.sensichips.sensiplus.config.configuration_handler.Chip> layoutChipList = new ArrayList<>();
        for(Chip dbChip : dbChipList){
            layoutChipList.add(initLayoutChip(dbChip, dbFamily, dbPortList, dbCluster));
        }
        return layoutChipList;
    }

    private static com.sensichips.sensiplus.config.configuration_handler.Cluster initLayoutCluster(Cluster dbCluster, List<Chip>  dbChipList, Family dbFamily, List<Port> dbPortList){
        com.sensichips.sensiplus.config.configuration_handler.Cluster layoutCluster = new com.sensichips.sensiplus.config.configuration_handler.Cluster();

        layoutCluster.setCLUSTERID(dbCluster.getIdCluster());
        layoutCluster.setMULTICASTCLUSTER(dbCluster.getMULTICASTCLUSTER());
        layoutCluster.setChip(initLayoutChipList(dbChipList, dbFamily, dbPortList, dbCluster));

        return layoutCluster;
    }

    private static com.sensichips.sensiplus.config.configuration_handler.ConfigurationDriver initLayoutConfigurationDriver(SPConfiguration spConfiguration){
        com.sensichips.sensiplus.config.configuration_handler.ConfigurationDriver layoutConfDriver = new com.sensichips.sensiplus.config.configuration_handler.ConfigurationDriver();

        layoutConfDriver.setDRIVERNAME(spConfiguration.getDriverName());
        layoutConfDriver.setPARAM1(spConfiguration.getDriverParameters().get(0));
        layoutConfDriver.setPARAM2(spConfiguration.getDriverParameters().get(1));

        return layoutConfDriver;
    }

    private static com.sensichips.sensiplus.config.configuration_handler.Configuration initLayoutConfiguration(SPConfiguration spConfiguration, Cluster dbCluster, List<Chip>  dbChipList,
                                                                                                               Family dbFamily, List<Port> dbPortList){
        com.sensichips.sensiplus.config.configuration_handler.Configuration layoutConfiguration = new com.sensichips.sensiplus.config.configuration_handler.Configuration();
        com.sensichips.sensiplus.config.configuration_handler.Cluster layoutCluster = new com.sensichips.sensiplus.config.configuration_handler.Cluster();

        //Inizializzo la configurazione
        layoutConfiguration.setHOSTCONTROLLER(spConfiguration.getHostController());
        layoutConfiguration.setDESCRIPTION(spConfiguration.getDescription());
        layoutConfiguration.setAPIOWNER(spConfiguration.getApiOwner());
        layoutConfiguration.setCLUSTERID(dbCluster.getIdCluster());
        layoutConfiguration.setVCC(Integer.toString(spConfiguration.getVCC()));
        layoutConfiguration.setPROTOCOL(spConfiguration.getProtocol());
        layoutConfiguration.setMCU(spConfiguration.getMcu());
        layoutConfiguration.setDRIVER(initLayoutConfigurationDriver(spConfiguration));
        layoutConfiguration.setCluster(initLayoutCluster(dbCluster, dbChipList, dbFamily, dbPortList));
        if(spConfiguration.getAddressingType() == 0){
            //dovrebbe essere no address
        } else if (spConfiguration.getAddressingType() == 1){
            layoutConfiguration.setADDRESSINGTYPE("ShortAddress");
        } else {
            //dovrebbe essere full address
        }

        return layoutConfiguration;
    }

    public static void testSendJSON(SPProtocol protocol, String apiOwner)throws SPDriverException, SPProtocolException {
        if (protocol.getFirmwareVersion() == SPProtocolESP8266_SENSIBUS.VER_3 && apiOwner.equals(SPConfiguration.API_OWNER_MCU)) {
            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            byte[] jsonByte = null;
            try {

                json = mapper.writeValueAsString(SPConfigurationManager.layoutConfiguration.getCONFIGURATION().get(6));

                jsonByte = json.getBytes();
                System.out.println(Hex.encodeHexString(jsonByte));
                try {
                    PrintWriter writer = new PrintWriter(new File("C:\\Users\\luca-\\Desktop\\outJSON_Byte.txt"));
                    writer.println(Hex.encodeHexString(jsonByte));
                    writer.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                //System.out.println(json);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            SPProtocolPacketExtended packet = new SPProtocolPacketExtended();
            byte opCode = (byte) EX_SEND_INIT;
            int bytePerPacket = 32;
            int payloadSize = jsonByte.length;
            int packetToSend = payloadSize / bytePerPacket;
            int byteSent = 0;
            byte[] header = new byte[4];

            header[0] = opCode;
            if (payloadSize > 65535) {
                int appo = (int)Math.ceil((payloadSize - 65535)/65536.0);
                header[1] = (byte) appo;
                appo = (int)Math.ceil((payloadSize - (65535 + 255))/256.0);
                header[2] = (byte) appo;
                appo = payloadSize - (header[1] * 65536 + appo * 256);
                header[3] = (byte) appo;
            } else if (payloadSize > 255){
                header[1] = (byte) 0x00;
                int appo = (int)Math.ceil((payloadSize - 255)/256.0);
                header[2] = (byte) appo;
                appo = payloadSize - (appo * 256);
                header[3] = (byte) appo;
            } else {
                header[1] = (byte) 0x00;
                header[2] = (byte) 0x00;
                header[3] = (byte) payloadSize;
            }

            packet.setDataLengthExpected(0);
            packet.setByteReceived(header);
            byte[] received_header = protocol.sendPacket(packet);

            byte[] payLoad = null;
            System.out.println("packetToSend: " + packetToSend);
            long start = System.currentTimeMillis();
            while(packetToSend > 0){
                payLoad = new byte[bytePerPacket];
                //testJson[0] = opCode;
                //testJson[1] = (byte) bytePerPacket;
                System.arraycopy(jsonByte, byteSent, payLoad, 0, bytePerPacket);

                byteSent += bytePerPacket;
                packetToSend--;

                if(payloadSize-byteSent > 0) {
                    packet.setDataLengthExpected(2);
                } else {
                    packet.setDataLengthExpected(2);
                }
                packet.setByteReceived(payLoad);

                byte[] received = protocol.sendPacket(packet);
                /*try {
                    TimeUnit.MILLISECONDS.sleep(100); //con 100 funziona
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
            //System.out.println("While() packetToSend: " + packetToSend);
            }
            if(payloadSize-byteSent > 0){
                payLoad = new byte[jsonByte.length - byteSent];
                //testJson[0] = opCode;
                //testJson[1] = (byte) (jsonByte.length - byteSent);
                System.arraycopy(jsonByte, byteSent, payLoad, 0, jsonByte.length - byteSent);

                packet.setDataLengthExpected(2);
                packet.setByteReceived(payLoad);

                byte[] received = protocol.sendPacket(packet);
            }

            long elapsed = System.currentTimeMillis() - start;
            System.out.println("Time Remaining : " + (elapsed)/1000.0 + " s");
        }
    }
}
