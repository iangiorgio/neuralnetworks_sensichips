package com.sensichips.sensiplus.util.rabbitmq_demo;



import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOError;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by Marco Ferdinandi on 07/03/2018.
 */

public class RabbitMQHandler {
    private String EXCHANGE_NAME = "MEASURE";

    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;
    private ObjectMapper mapper;
    public RabbitMQHandler(String server, String username, String password, String virtualHost,String exchange)throws IOException,TimeoutException{
        connectionFactory = new ConnectionFactory();
        //factory.setHost("localhost");
        connectionFactory.setHost(server);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);
        connection = connectionFactory.newConnection();
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME,"topic");

        this.EXCHANGE_NAME = exchange;

         mapper = new ObjectMapper();
    }


    public void sendMeasure(MeasureToSend measureToSend)throws IOException{
        String jsonInString = mapper.writeValueAsString(measureToSend);
        String rKey=measureToSend.configuration+"."+measureToSend.getChipid()+"."+measureToSend.getSensingElement();
        channel.basicPublish(EXCHANGE_NAME,rKey, new AMQP.BasicProperties.Builder().build(), jsonInString.getBytes());

    }




}
