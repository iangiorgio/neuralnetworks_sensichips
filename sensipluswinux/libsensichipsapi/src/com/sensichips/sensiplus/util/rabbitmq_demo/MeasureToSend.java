package com.sensichips.sensiplus.util.rabbitmq_demo;


import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "timestamp","configuration","chipid", "name","sensingElement","value","unitMeasure","concentration" })
public class MeasureToSend {
    String chipid;
    String sensingElement;
    String configuration;
    String timestamp;
    String value;

    String unitMeasure;


    String concentration;

    public MeasureToSend(String chipid, String sensingElement, String configuration, String timestamp, String value, String unitMeasure,String concentration) {
        this.chipid = chipid;
        this.sensingElement = sensingElement;
        this.configuration = configuration;
        this.timestamp = timestamp;
        this.value = value;
        this.concentration = concentration;
        this.unitMeasure = unitMeasure;
    }
    public MeasureToSend(){}
    public String getChipid() {
        return chipid;
    }

    public void setChipid(String chipid) {
        this.chipid = chipid;
    }

    public String getConfiguration() {
        return configuration;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public String getSensingElement() {
        return sensingElement;
    }

    public void setSensingElement(String sensingElement) {
        this.sensingElement = sensingElement;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public String getConcentration() {
        return concentration;
    }

    public void setConcentration(String concentration) {
        this.concentration = concentration;
    }


    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

}

