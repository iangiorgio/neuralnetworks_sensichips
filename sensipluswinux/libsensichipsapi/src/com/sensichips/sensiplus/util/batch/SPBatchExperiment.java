package com.sensichips.sensiplus.util.batch;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.datareader.queue.SPMainQueue;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.chip.SPPort;
import com.sensichips.sensiplus.level1.chip.SPSensingElementOnChip;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.SPMeasurementRUN4;
import com.sensichips.sensiplus.level2.parameters.*;
import com.sensichips.sensiplus.level2.parameters.items.*;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SPBatchExperiment {


	public List<SPMeasurementParameterPOT> getPotMeasurementParameterList() {
		return potMeasurementParameterList;
	}

	public void setPotMeasurementParameterList(List<SPMeasurementParameterPOT> potMeasurementParameterList) {
		this.potMeasurementParameterList = potMeasurementParameterList;
	}

	private List<SPMeasurementParameterPOT> potMeasurementParameterList = new ArrayList<>();
	/*public long getTimeToWait() {
		return timeToWait;
	}

	public void setTimeToWait(long timeToWait) {
		this.timeToWait = timeToWait;
	}
*/

	public List<Long> timeToWaitList = new ArrayList<>();

	public List<Long> measurementTimeList = new ArrayList<>();
	public List<Boolean> sensorSequentialModeList = new ArrayList<>();

	private int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L2;
	private String LOG_MESSAGE = "SPBatchExperiment";



	private List<SPSensingElementOnChip> spSensingElementList = new ArrayList<>();
	private List<Integer> sensorIndexToSaveList = new ArrayList<>();

	public List<List<Integer>> getSensorIndexToPlotList() {
		return sensorIndexToPlotList;
	}

	//List of all indexes to plot for each sensing element.
	private List<List<Integer>> sensorIndexToPlotList = new ArrayList<>();

	public int getTotalNumberOfSENSORPlot(){
		int result = 0;

		for(int i=0; i<sensorIndexToPlotList.size(); i++){
			result+= sensorIndexToPlotList.get(i).size();
		}

		return result;
	}


	public List<SPSensingElementOnChip> getSpSensingElementList() {
		return spSensingElementList;
	}

/*
	public String getGlobalCSVHeader(){
		String header = "";

        for(int i=0; i<getNumOfEISElement();i++){
            for(int j=0; j<this.numOfConfigurationsList.get(i); j++) {
                for (int k = 0; k < getSPConfiguration().getCluster().getActiveSPChipList().size(); k++) {

                }
            }

        }

        //SENSORS


		return header;
	}
*/

	public String getGlobalFileHeader() throws SPException{
		int numOfChips = getSPConfiguration().getCluster().getActiveSPChipList().size();
		String output[] = new String[getTotalNumberOfEISPlot()*numOfChips+getTotalNumberOfSENSORPlot()*numOfChips+
				getTotalNumberOfPOTplot()*numOfChips];

		List<String> headerLabels = new ArrayList<>();

		//EIS
		String eis_titles[] = SPMeasurementParameterEIS.getDifferences(getSpMeasurementParamEISList());


		for(int h = 0; h<getNumOfEISElement(); h++) {
			for (int i = 0; i < getNumOfConfigurationsList().get(h); i++) {
				for(int l=0; l<eisIndexToPlotList.get(h).size();l++){

					int configIndex = 0;
					for(int k=h-1; k>=0; k--){

						configIndex +=getNumOfConfigurationsList().get(k);
					}
					for (int j = 0; j < getSPConfiguration().getCluster().getActiveSPChipList().size(); j++) {

						headerLabels.add(eis_titles[configIndex+i]+"_"+
								SPParamItemMeasures.measureLabels[eisIndexToPlotList.get(h).get(l)]+"_"+
								(getSPConfiguration().getCluster().getActiveSPChipList().get(j).getSerialNumber()).substring(
										getSPConfiguration().getCluster().getActiveSPChipList().get(j).getSerialNumber().length()-1-4,
										getSPConfiguration().getCluster().getActiveSPChipList().get(j).getSerialNumber().length()-1));
					}
					//output += "\n";

				}
			}
		}


		//SENSORS
		for(int i=0; i<getSpSensingElementList().size(); i++){
			for(int j=0; j<sensorIndexToPlotList.get(i).size(); j++){
				for(int cid=0; cid<numOfChips; cid++){
					String head = "";
					if(getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().getMeasureTechnique().equals("EIS")||
							getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().getName().contains("FIGARO")){
						head = getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().getName()+"_"+
								SPParamItemMeasures.measureLabels[sensorIndexToPlotList.get(i).get(j)]+"_"+
								getSPConfiguration().getCluster().getActiveSPChipList().get(cid).getSerialNumber().substring(
										getSPConfiguration().getCluster().getActiveSPChipList().get(cid).getSerialNumber().length()-1-4,
										getSPConfiguration().getCluster().getActiveSPChipList().get(cid).getSerialNumber().length()-1);
					}else{
						head = getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().getName()+"_"+
								getSPConfiguration().getCluster().getActiveSPChipList().get(cid).getSerialNumber().substring(
										getSPConfiguration().getCluster().getActiveSPChipList().get(cid).getSerialNumber().length()-1-4,
										getSPConfiguration().getCluster().getActiveSPChipList().get(cid).getSerialNumber().length()-1);
					}
					headerLabels.add(head);
				}
			}
		}


		//POT
		for(int i=0; i<getTotalNumberOfPOTplot()*numOfChips;i++){
			headerLabels.add("POT");
		}


		String output_header = "Timestamp;Temp;Hum;"+String.join(";", headerLabels.toArray(new String[output.length]));

		return output_header;
	}



	public long getSensorTimeToWait(int i) {
		return sensorTimeToWait.get(i);
	}

	public void setSensorTimeToWait(List<Long> sensorTimeToWait) {
		this.sensorTimeToWait = sensorTimeToWait;
	}

	private List<Long> sensorTimeToWait = new ArrayList<>();
	private List<Long> sensorMeasurementTimeList = new ArrayList<>();

    public List<Long> getSensorMeasurementTimeList() {
        return sensorMeasurementTimeList;
    }

	//private SPConfiguration conf;
	private boolean makeAutoRange = false;

	public List<HashMap<String, ArrayList<String>>> containersList = new ArrayList<>();

	//public HashMap<String, ArrayList<String>> container = new HashMap();

	private String date = new String();
	private Integer configRepetition = 0;

    private Integer globalRepetition = 0;

	private Long delay = 0L;
	private Integer outputDecimation = 0;
	private Integer layout_configuration = 0;

	public List<Integer> eisIndexToSaveList = new ArrayList<>();



	private List<String> eisFilterList = new ArrayList<>();


	private List<String> eisBurstModeList = new ArrayList<>();
	private List<String> eisFillBufferBeforeStart = new ArrayList<>();

	//List of List of index to plot: The external list contains all the EIS in the experiment
	//For each EIS there is the possibility to request various plots. So The internal list contains the indexes to plot
	public List<List<Integer>> eisIndexToPlotList = new ArrayList<>();
	public List<List<Integer>> potIndexToPlotList = new ArrayList<>();

	public List<String> getEisFilterList() {
		return eisFilterList;
	}
	public List<String> getEisBurstModeList() {
		return eisBurstModeList;
	}

	public List<String> getEisFillBufferBeforeStart() {
		return eisFillBufferBeforeStart;
	}
	public int getTotalNumberOfEISPlot(){
		int result = 0;
		for(int i=0; i<eisIndexToPlotList.size(); i++){
			result+= eisIndexToPlotList.get(i).size()*getNumOfConfigurationsList().get(i);
		}

		return result;
	}

	public int getTotalNumberOfPOTplot(){
		int result = 0;
		for(int i=0; i<potIndexToPlotList.size(); i++){
			result+= potIndexToPlotList.get(i).size();
		}

		return result;
	}







	private Boolean fillBufferBeforeStart = false;
	//private Integer burst = 1;

	private List<Integer> numOfConfigurationsList = new ArrayList<Integer>();


	public List<SPMeasurementParameterEIS[]> getSpMeasurementParamEISList() {
		return spMeasurementParamEISList;
	}

	private List<SPMeasurementParameterEIS[]> spMeasurementParamEISList = new ArrayList<>();


	private PrintWriter logEISFiles[][] = null;
	private PrintWriter logSensorFiles[][] = null;
	private PrintWriter logPOTFiles[][] = null;

	private SPMeasurement[] measurementOnSensors;
	private List<SPMeasurement[]> measurementOnEISList = new ArrayList<>();
	private SPMeasurement[] measurementOnPOT;

	private String id;
	// Contiene l'elenco dei parametri nell'ordine specificato nel file batch
	// attraverso il parametro order
	public List<SPCSVRow> eisnameList = new ArrayList<>();


	// Serve a convertire in csv il set di parametri corrente
	private SPCSVRow currentRow = null;

	private SPConfiguration conf;
	//private int numOfChips;
	//private List<SPChip> chipList;
    private SPMainQueue mainQueue[];


	private String rabbitmq_server_ip = null;
    private String rabbitmq_username = null;
    private String rabbitmq_password = null;
    private String rabbitmq_virtualhost = null;
	private String rabbitmq_exchange = null;

	private Integer timeoutUSB = 3000;

	public Integer getTimeoutUSB() {
		return timeoutUSB;
	}

	public void setTimeoutUSB(Integer timeoutUSB) {
		this.timeoutUSB = timeoutUSB;
	}


	public int getNumOfEISElement(){
    	return containersList.size();
	}


    public List<Boolean> eisAutoscaleActiveFlags = new ArrayList<Boolean>();
	public List<Boolean> eisAutoRangeActiveFlags = new ArrayList<Boolean>();

    public SPBatchExperiment(SPConfiguration conf){
        this.conf = conf;
    }

    public void initMeasurementQueues(){
        //Allocate an array of queues to contain all measurement for each configuration and sensor;

		//mainQueue = new SPMainQueue[getTotalNumEISConfigurations()+spSensingElementList.size()];



		mainQueue = new SPMainQueue[getTotalNumberOfEISPlot()+getTotalNumberOfSENSORPlot()+getTotalNumberOfPOTplot()];

        for(int i=0; i<getTotalNumberOfEISPlot(); i++){
            mainQueue[i] = new SPMainQueue(globalRepetition*configRepetition);
        }
        for(int j=0; j<getTotalNumberOfSENSORPlot(); j++){
            mainQueue[j+getTotalNumberOfEISPlot()] = new SPMainQueue(globalRepetition);
        }
		for(int j=0; j<getTotalNumberOfPOTplot(); j++){
			mainQueue[j+getTotalNumberOfEISPlot()+getTotalNumberOfSENSORPlot()] = new SPMainQueue(globalRepetition);
		}
    }


    public int getTotalNumEISConfigurations(){
    	int result = 0;

    	for(int i=0; i<numOfConfigurationsList.size();i++){
    		result+=numOfConfigurationsList.get(i);
		}

		return result;
	}

	public List<Integer> getNumOfConfigurationsList() {
		return numOfConfigurationsList;
	}

	public void setNumOfConfigurationsList(List<Integer> numOfConfigurationsList) {
		this.numOfConfigurationsList = numOfConfigurationsList;
	}


	public SPMainQueue[] getMainQueue() {
        return mainQueue;
    }

    public void setMainQueue(SPMainQueue[] mainQueue) {
        this.mainQueue = mainQueue;
    }

	/*public Integer getEISIndexToPlotinList(int index) {
		return eisIndexToPlotList.get(index);
	}*/

	public Integer getEISIndexToSaveinList(int index){
    	return eisIndexToSaveList.get(index);
	}



	public void addSPSensingElementOnChip(SPSensingElementOnChip item, int indexToSave, List<Integer> indexToPlot, long timeToWait, long measurementTime) throws SPException{
		if (item == null){
		    throw new SPException("SPSensingElementOnChip item null is not permitted!");
        }
	    spSensingElementList.add(item);
        sensorIndexToSaveList.add(indexToSave);
        sensorIndexToPlotList.add(indexToPlot);
        sensorTimeToWait.add(timeToWait);
		sensorMeasurementTimeList.add(measurementTime);
    }

    public SPSensingElementOnChip getSPSensingElementOnChip(int i){
        return spSensingElementList.get(i);
    }

    public int getSensorIndexToSaveList(int i){
        return sensorIndexToSaveList.get(i);
    }

	public SPConfiguration getSPConfiguration(){
		return conf;
	}

    /*public Integer getBurst() {
        return burst;
    }*/

    /*public void setBurst(Integer burst) {
        this.burst = burst;
    }*/

    public boolean isMakeAutoRange() {
		return makeAutoRange;
	}

	public void setMakeAutoRange(boolean makeAutoRange) {
		this.makeAutoRange = makeAutoRange;
	}

	public String getCSVHeader(int eisIndex) {

		return eisnameList.get(eisIndex).getCSV();

	}


	public String getRabbitmq_server_ip() {
		return rabbitmq_server_ip;
	}

	public void setRabbitmq_server_ip(String rabbitmq_server_ip) {
		this.rabbitmq_server_ip = rabbitmq_server_ip;
	}

	public String getRabbitmq_username() {
		return rabbitmq_username;
	}

	public void setRabbitmq_username(String rabbitmq_username) {
		this.rabbitmq_username = rabbitmq_username;
	}

	public String getRabbitmq_password() {
		return rabbitmq_password;
	}

	public void setRabbitmq_password(String rabbitmq_password) {
		this.rabbitmq_password = rabbitmq_password;
	}

	public String getRabbitmq_virtualhost() {
		return rabbitmq_virtualhost;
	}

	public void setRabbitmq_virtualhost(String rabbitmq_virtualhost) {
		this.rabbitmq_virtualhost = rabbitmq_virtualhost;
	}

	public String getRabbitmq_exchange() {
		return rabbitmq_exchange;
	}

	public void setRabbitmq_exchange(String rabbitmq_exchange) {
		this.rabbitmq_exchange = rabbitmq_exchange;
	}




	public void prepareInternalState() {
		// Evaluate number of elements
		int currentNumOfConf = 0;
		for(int i=0; i<containersList.size(); i++){
			//numOfConfigurationsList.add(i,0);
			currentNumOfConf = 0;
			if(containersList.get(i).size()>0){
				//numOfConfigurationsList.set(i,1);
				currentNumOfConf = 1;
			}
			for (int j = 1; j <= containersList.get(i).size(); j++) {
				currentNumOfConf *= containersList.get(i).get("" + j).size();
			}
			numOfConfigurationsList.add(i,currentNumOfConf);
		}
	}


	/*public Boolean getFillBufferBeforeStart() {
		return fillBufferBeforeStart;
	}*/

	/*public void setFillBufferBeforeStart(Boolean fillBufferBeforeStart) {
		this.fillBufferBeforeStart = fillBufferBeforeStart;
	}*/
	public Integer getOutputDecimation() {
		return outputDecimation;
	}

	public void setOutputDecimation(Integer outputDecimation) {
		this.outputDecimation = outputDecimation;
	}

	public void setDelay(Long delay){
		this.delay = delay;
	}

	public Long getDelay(){
		return delay;
	}


	public String getId(){
		return id;
	}

	public void setId(String id){
		this.id = id;
	}

	public Integer getConfigRepetition() {
		return configRepetition;
	}
	public void setConfigRepetition(Integer configRepetition) {
		this.configRepetition = configRepetition;
	}

    public Integer getGlobalRepetition() {
        return globalRepetition;
    }

    public void setGlobalRepetition(Integer globalRepetition) {
        this.globalRepetition = globalRepetition;
    }


	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}


	public void saveEISMeasure(double[][] output, SPMeasurementParameterEIS paramEIS, int eisIndex) throws SPException {
        String fileRow = "";
		int numOfChips = conf.getCluster().getActiveSPChipList().size();

        for (int k = 0; k < numOfChips; k++) {
            fileRow = this.getCSVEISParameter(paramEIS,eisIndex) + output[k][SPMeasurementParameterEIS.IN_PHASE] + ";" + output[k][SPMeasurementParameterEIS.QUADRATURE] +
					";" + output[k][SPParamItemMeasures.CONDUCTANCE] +";" + output[k][SPParamItemMeasures.SUSCEPTANCE] +
                    ";" + output[k][SPParamItemMeasures.MODULE] + ";" + output[k][SPParamItemMeasures.PHASE] +
					";" + output[k][SPParamItemMeasures.RESISTANCE] + ";" + output[k][SPParamItemMeasures.CAPACITANCE] + "; - "+
					";" + output[k][SPParamItemMeasures.CURRENT] + ";" + output[k][SPParamItemMeasures.VOLTAGE];

            // Scrittura su file
            //System.out.println(output[k][SPMeasurementParameterEIS.CAPACITANCE]);
            logEISFiles[k][eisIndex].println(fileRow);
        }
    }

    public void savePOTMeasure(double[][] output, SPMeasurementParameterPOT paramPOT, int potIndex){
		String fileRow = "";
		int numOfChips = conf.getCluster().getActiveSPChipList().size();

		for (int k = 0; k < numOfChips; k++) {
			fileRow = paramPOT.getLabelsAsString() + output[k][SPParamItemPOTMeasures.VOLTAGE] + ";" +
					output[k][SPParamItemPOTMeasures.CURRENT];

			// Scrittura su file
			//System.out.println(output[k][SPMeasurementParameterEIS.CAPACITANCE]);
			logPOTFiles[k][potIndex].println(fileRow);
		}
	}

    public void saveSENSORSMeasure(double output[][], int currentSensor, SPMeasurementRUN4 spMeasurement, SPMeasurementParameterSENSORS sensorParam) throws SPException {
		int numOfChips = conf.getCluster().getActiveSPChipList().size();
        double M, N;
        String row = "";
		if (sensorParam.getParamInternalEIS()!=null){

			SPMeasurementParameterEIS paramEIS = sensorParam.getParamInternalEIS();

			//SPMeasurementParameterEIS paramEIS = getSPSensingElementOnChip(currentSensor).getSensingElementOnFamily().getSPMeasurementParameterEIS(conf);
            row = paramEIS.getLabelsAsString();
        }/*else if(sensorParam.getSpOffChipTGS8100Figaro()!=null){
			row = sensorParam.getSpOffChipTGS8100Figaro().getParamSensor().getLabelsAsString();
		}*/

		//M = getSPSensingElementOnChip(currentSensor).getCalibrationParameters().getM();
        //N = getSPSensingElementOnChip(currentSensor).getCalibrationParameters().getN();

        for (int k = 0; k < numOfChips; k++) {

			M = spMeasurement.getM()[k];
			N = spMeasurement.getN()[k];
            //logSensorFiles[k][currentSensor].print(output[k][0] + ";" + M + ";" + N + ";" + row);
            //System.out.println(output[k][0]);
			logSensorFiles[k][currentSensor].print(M + ";" + N + ";" + row);

            for (int j = 0; j < output[k].length; j++) {
                logSensorFiles[k][currentSensor].print(output[k][j] + "; ");
            }
            logSensorFiles[k][currentSensor].println();
        }
    }

    public void closeFiles()throws SPException {
		int numOfChips = conf.getCluster().getActiveSPChipList().size();
		try{
            //GlobalParameters.protocol.closeAll();
            if (getTotalNumEISConfigurations() > 0){
                for(int k = 0; k < numOfChips; k++){
                	for(int eisInd = 0; eisInd<numOfConfigurationsList.size(); eisInd++) {
						logEISFiles[k][eisInd].close();
					}
                }
            }
            if (getSpSensingElementList().size() > 0){
                for(int k = 0; k < numOfChips; k++){
                    for(int c = 0; c < getSpSensingElementList().size() ; c++){
                        logSensorFiles[k][c].close();
                    }
                }
            }
            if(getPotMeasurementParameterList().size()>0){
            	for(int k=0; k<numOfChips; k++ ){
            		for(int h=0; h<getPotMeasurementParameterList().size();h++){
            			logPOTFiles[k][h].close();
					}
				}
			}
        } catch (Exception e){
            throw new SPException("Exception in closeFiles() of SPBatchExperiment");
        }
    }


    private void generateParamList() throws SPException {

		for(int i=0; i<containersList.size(); i++){
			SPMeasurementParameterEIS currentSPMeasParamEIS[] = new SPMeasurementParameterEIS[numOfConfigurationsList.get(i)];
			for(int index = 0; index < numOfConfigurationsList.get(i); index++) {
				currentSPMeasParamEIS[index] = new SPMeasurementParameterEIS(conf);
				SPMeasurementMetaParameter meta = new SPMeasurementMetaParameter(conf.getCluster().getActiveSPChipList().size());
				currentSPMeasParamEIS[index].setSPMeasurementMetaParameter(meta);

				int indici[] = new int[containersList.get(i).size()];
				for (int m = 0; m < index; m++) {
					indici[0]++;
					for (int j = 1; j < containersList.get(i).size(); j++) {
						indici[j] += indici[j - 1] / containersList.get(i).get("" + (j)).size();
						indici[j - 1] = indici[j - 1] % containersList.get(i).get("" + (j)).size();
					}
				}

				//ArrayList<String> fields = new ArrayList<String>(), values = new ArrayList<String>();
				currentRow = new SPCSVRow();
				String currentField, currentValue;
				//makeAutoRange = false;
				for (int k = 0; k < containersList.get(i).size(); k++) {
					currentField = eisnameList.get(i).getItem(k);
					currentValue = containersList.get(i).get("" + (k + 1)).get(indici[k]);

					currentRow.addItem(currentValue);

					/*if (currentValue.equals("auto")) {
						makeAutoRange = true;
					} else {*/
						setParameters(currentSPMeasParamEIS[index], currentField, currentValue);
					//}
				}
				// Don't care
				//currentSPMeasParamEIS[index].setMeasure(SPMeasurementParameterEIS.measureLabels[2]);
				currentSPMeasParamEIS[index].setMeasure(SPParamItemMeasures.measureLabels[this.eisIndexToSaveList.get(i)]);
				if(eisFilterList.get(i)!=null)
					currentSPMeasParamEIS[index].setFilter(eisFilterList.get(i));
				if(eisBurstModeList.get(i)!=null)
					currentSPMeasParamEIS[index].setBurstMode(eisBurstModeList.get(i).toUpperCase().equalsIgnoreCase("TRUE"));
				if(eisFillBufferBeforeStart.get(i)!=null)
					currentSPMeasParamEIS[index].setFillBufferBeforeStart(eisFillBufferBeforeStart.get(i).toUpperCase().equalsIgnoreCase("TRUE"));


				//param.setOutputDecimation(outputDecimation);

				//meta.setFillBufferBeforeStart(fillBufferBeforeStart);
				//meta.setBurst(burst);

			}

			spMeasurementParamEISList.add(i,currentSPMeasParamEIS);

		}

    }


	public SPMeasurementParameterEIS getSPMeasurementParameterEIS(int eisIndex, int spMeasParamEISIndex) throws SPException {
	    if (spMeasurementParamEISList.size() == 0){
	        generateParamList();
        }
        return spMeasurementParamEISList.get(eisIndex)[spMeasParamEISIndex];
	}



	public void setSPMeasurementParameterEIS(SPMeasurementParameterEIS param, int eisIndex, int spMeasParamEISIndex) throws SPException {
		if (spMeasurementParamEISList.size() == 0){
			generateParamList();
		}
	    //paramList[index] = param;
		spMeasurementParamEISList.get(eisIndex)[spMeasParamEISIndex] = param;
    }




	public String getCSVEISParameter(SPMeasurementParameterEIS paramEIS, int eisIndex) throws SPException {
		//SPMeasurementParameterEIS paramEIS = getSPMeasurementParameterEIS(currentConfiguration);
		String result = "";
		try{
			SPCSVRow nameList = eisnameList.get(eisIndex);
			for(int i=0; i<nameList.size(); i++){
				if(nameList.getItem(i).equals(SPBatchManager.FREQUENCIES)){
					result += paramEIS.getFrequency().toString()+";";

				}else if (nameList.getItem(i).equals(SPBatchManager.DCBIASP)) {
					result += paramEIS.getDCBiasP().toString()+";";

				}else if (nameList.getItem(i).equals(SPBatchManager.DCBIASN)) {
					result += paramEIS.getDCBiasN().toString()+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.CONTACTS)) {
					result += SPMeasurementParameter.searchValue(SPParamItemContacts.ContactsValues,
							SPParamItemContacts.ContactsLabels, paramEIS.getContacts(),null)+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.INPORT)) {
					result += SPMeasurementParameter.searchValue(SPPort.portValues,
							SPPort.portLabels, paramEIS.getInPort(),null)+";";

				}else if (nameList.getItem(i).equals(SPBatchManager.OUTPORT)) {
					result += SPMeasurementParameter.searchValue(SPPort.portValues,
							SPPort.portLabels, paramEIS.getOutPort(),null)+";";

				}  else if (nameList.getItem(i).equals(SPBatchManager.HARMONIC)) {
					result += SPMeasurementParameter.searchValue(SPParamItemHarmonic.harmonicValues,
							SPParamItemHarmonic.harmonicLabels, paramEIS.getHarmonic(),null)+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.INGAIN)) {
					result += SPMeasurementParameter.searchValue(SPParamItemInGain.ingainValues,
							SPParamItemInGain.ingainLabels, paramEIS.getInGain(),null)+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.OUTGAIN)) {
					result += SPMeasurementParameter.searchValue(SPParamItemOutGain.outgainValues,
							SPParamItemOutGain.outgainLabels, paramEIS.getOutGain(),null)+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.SEQUENTIALMODE)) {
					result += paramEIS.isSequentialMode()+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.MODEVI)) {
					result += SPMeasurementParameter.searchValue(SPParamItemModeVI.ModeVIValues,
							SPParamItemModeVI.ModeVILabels, paramEIS.getModeVI(),null)+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.RSENSE)) {
					result += SPMeasurementParameter.searchValue(SPParamItemRSense.rsenseValues,
							SPParamItemRSense.rsenseLabels, paramEIS.getRsense(),null)+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.FILTER)) {
					result += SPMeasurementParameter.searchValue(SPParamItemFilter.filterValues,
							SPParamItemFilter.filterLabels, paramEIS.getFilter(),null)+";";

				} else if (nameList.getItem(i).equals(SPBatchManager.PHASESHIFT)) {
					result += SPParamItemPhaseShiftMode.PhaseShiftModeLabels[paramEIS.getPhaseShiftMode()]+","+
							paramEIS.getPhaseShift()+","+ SPMeasurementParameter.searchValue(SPParamItemQI.I_QValues,
							SPParamItemQI.I_QLabels, paramEIS.getQI(),null)+";";
				} 
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return result;
	}


	// Da tenere coerente con la lista dei parametri possibili per il singolo esperimento
	public static void setParameters(SPMeasurementParameterEIS param, String currentField, String currentValue) throws SPException {
		try {
			if (currentField.equals(SPBatchManager.INPORT)) {
				param.setInPort(currentValue);
			} else if (currentField.equals(SPBatchManager.OUTPORT)) {
				param.setOutPort(currentValue);
			} else if (currentField.equals(SPBatchManager.FREQUENCIES)) {
				param.setFrequency(Float.parseFloat(currentValue));
			} else if (currentField.equals(SPBatchManager.DCBIASP)) {
				param.setDCBiasP(Integer.parseInt(currentValue));
			} else if (currentField.equals(SPBatchManager.DCBIASN)) {
				param.setDCBiasN(Integer.parseInt(currentValue));
			} else if (currentField.equals(SPBatchManager.CONTACTS)) {
				param.setContacts(currentValue);
			} else if (currentField.equals(SPBatchManager.HARMONIC)) {
				param.setHarmonic(currentValue);
			} else if (currentField.equals(SPBatchManager.INGAIN)) {
				param.setInGain(currentValue);
			} else if (currentField.equals(SPBatchManager.OUTGAIN)) {
				param.setOutGain(currentValue);
			} else if (currentField.equals(SPBatchManager.MODEVI)) {
				param.setModeVI(currentValue);
			} else if (currentField.equals(SPBatchManager.RSENSE)) {
				param.setRsense(currentValue);
			}/* else if (currentField.equals(SPBatchManager.FILTER)) {
			param.setFilter(currentValue);
		}*/ else if (currentField.equals(SPBatchManager.PHASESHIFT)) {
				String values[] = currentValue.split(",");
				param.setPhaseShiftQuadrants(values[0].trim(), Integer.parseInt(values[1].trim()), values[2].trim());
			} else if (currentField.equals(SPBatchManager.REGISTERS)) {
				String values[] = currentValue.split(",");
				param.setRegistersList(values);
				//param.setPhaseShiftQuadrants(values[0].trim(), Integer.parseInt(values[1].trim()), values[2].trim());
			} else if (currentField.equals(SPBatchManager.SEQUENTIALMODE)) {
				param.setSequentialMode(currentValue.equalsIgnoreCase("1"));
			}/*else if (currentField.equals(SPBatchManager.BURSTMODE)) {
			param.setBurstMode(currentValue.toUpperCase().equalsIgnoreCase("TRUE"));
		}else if (currentField.equals(SPBatchManager.FILLBUFFERBEFORESTART)) {
			param.setFillBufferBeforeStart(currentValue.toUpperCase().equalsIgnoreCase("TRUE"));
		}*/
		}catch (SPException e){
			throw new SPException("EIS parameters configuration not allowed!");
		}


	}



	public synchronized SPMeasurement[] getSENSORMeasurement(String logDir, SPProtocol spProtocol) throws SPException, IOException{
        if (measurementOnSensors != null){
            return measurementOnSensors;
        }
        generateSENSORSFilesAndMeasurement(logDir, spProtocol);
        return measurementOnSensors;
    }

    public synchronized List<SPMeasurement[]> getEISMeasurement(String logDir, SPProtocol spProtocol) throws SPException, IOException{
        if (measurementOnEISList.size() != 0){
            return measurementOnEISList;
        }
        generateEISFilesAndMeasurement(logDir, spProtocol);
        return measurementOnEISList;
    }


	public synchronized SPMeasurement[] getPOTMeasurement(String logDir, SPProtocol spProtocol) throws SPException, IOException{
		if (measurementOnPOT != null){
			return measurementOnPOT;
		}
		generatePOTFilesAndMeasurement(logDir, spProtocol);
		return measurementOnPOT;
	}

	private void generatePOTFilesAndMeasurement(String logDir,SPProtocol spProtocol) throws SPException, IOException {
		if(getPotMeasurementParameterList().size()>0){
			List<SPChip> chipList = conf.getCluster().getActiveSPChipList();
			int numOfChips = chipList.size();

			logPOTFiles = new PrintWriter[numOfChips][getPotMeasurementParameterList().size()];
			String nomeOutputFile = "";
			boolean generaNuovoFile = true;

			measurementOnPOT = new SPMeasurement[getPotMeasurementParameterList().size()];
			DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH-mm");
			Date date = new Date();
			String dateString = sdf.format(date);
			for(int i=0; i<getPotMeasurementParameterList().size(); i++) {

				measurementOnPOT[i] = conf.getSPMeasurement(SPFamily.MEASURE_TYPES_AVAILABLE[SPFamily.MEASURE_TYPE_POT]);  // default decoder

				for (int k = 0; k < numOfChips; k++) {
					String shortAddress = chipList.get(k).getAddress(conf.getSPProtocol().getAddressingMode(), conf.getSPProtocol().getProtocolName());
					nomeOutputFile = logDir + "Experiment_" +dateString + "_POT_" + "_" + getId() + "_" + shortAddress + ".csv";
					SPLogger.getLogInterface().d(LOG_MESSAGE, "Output file. " + nomeOutputFile, DEBUG_LEVEL);
					logPOTFiles[k][i] = new PrintWriter(new BufferedWriter(new FileWriter(nomeOutputFile, !generaNuovoFile)), true);
					if (generaNuovoFile) {
						logPOTFiles[k][i].println(getPotMeasurementParameterList().get(i).getHeaderAsString() + "Voltage;Current");
					}
				}
			}
		}
	}

    private void generateEISFilesAndMeasurement(String logDir, SPProtocol spProtocol)  throws SPException, IOException {
        if(getTotalNumEISConfigurations() > 0) {
			List<SPChip> chipList = conf.getCluster().getActiveSPChipList();
			int numOfChips = chipList.size();

			logEISFiles = new PrintWriter[numOfChips][numOfConfigurationsList.size()];

			for (int i = 0; i < numOfConfigurationsList.size(); i++) {
				int currentNumOfConfigurations = numOfConfigurationsList.get(i);
				boolean generaNuovoFile = true;

				String nomeOutputFile = "";
				SPMeasurement[] currentSPMeasurementOnEIS = new SPMeasurement[currentNumOfConfigurations];

				for (int c = 0; c < currentNumOfConfigurations; c++) {
					currentSPMeasurementOnEIS[c] = conf.getSPMeasurement(SPFamily.MEASURE_TYPES_AVAILABLE[SPFamily.MEASURE_TYPE_EIS]);  // default decoder
				}
				measurementOnEISList.add(i,currentSPMeasurementOnEIS);

				SPLogger.getLogInterface().d(LOG_MESSAGE, "-------------------------------------", DEBUG_LEVEL);

				DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH-mm");
				Date date = new Date();
				String dateString = sdf.format(date);
				for (int k = 0; k < numOfChips; k++) {
					String shortAddress = chipList.get(k).getAddress(conf.getSPProtocol().getAddressingMode(), conf.getSPProtocol().getProtocolName());
					nomeOutputFile = logDir + "Experiment_" + dateString + "_EIS_" + i+"_" + getId() + "_" + shortAddress + ".csv";
					SPLogger.getLogInterface().d(LOG_MESSAGE, "Output file. " + nomeOutputFile, DEBUG_LEVEL);
					logEISFiles[k][i] = new PrintWriter(new BufferedWriter(new FileWriter(nomeOutputFile, !generaNuovoFile)), true);
					if (generaNuovoFile) {
						logEISFiles[k][i].println(getCSVHeader(i) + "VadcI;VadcQ;Conductance;Susceptance;Zmod;ZPha;Rp;Cp;Lp;Current[mA];Voltage[mV]");
					}
				}

			}
		}
    }



	private void generateSENSORSFilesAndMeasurement(String logDir, SPProtocol spProtocol)  throws SPException, IOException {
		if (getSpSensingElementList().size()  > 0){
            measurementOnSensors = new SPMeasurement[getSpSensingElementList().size() ];
			List<SPChip> chipList = conf.getCluster().getActiveSPChipList();
			int numOfChips = chipList.size();

            System.out.println("Detected sensors:");
            for(int c = 0; c < getSpSensingElementList().size(); c++){
                measurementOnSensors[c] = conf.getSPMeasurement(getSPSensingElementOnChip(c).getSensingElementOnFamily().getID());
            	if(getSPSensingElementOnChip(c).isAutoScaleActive()){
            		measurementOnSensors[c].setAutomaticADCRangeAdjustment(true);
				}
            }

            logSensorFiles = new PrintWriter[numOfChips][getSpSensingElementList().size()];
			DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH-mm");
			Date date = new Date();
			String dateString = sdf.format(date);
            for(int k = 0; k < numOfChips; k++){
                String shortAddress = chipList.get(k).getAddress(conf.getSPProtocol().getAddressingMode(), conf.getSPProtocol().getProtocolName());
                for(int c = 0; c < getSpSensingElementList().size(); c++){

                    String sensorName = getSPSensingElementOnChip(c).getSensingElementOnFamily().getID();
                    String fileName = logDir + "Experiment_" + dateString + "_" + sensorName + "_" + getId() + "_" + shortAddress + ".csv";
                    logSensorFiles[k][c] =  new PrintWriter(new BufferedWriter(new FileWriter(fileName, false)), true);

                    logSensorFiles[k][c].print("m;n;");


                    if (getSPSensingElementOnChip(c).getSensingElementOnFamily().getSensingElement().getMeasureTechnique().equals("EIS")){
						//SPMeasurementParameterEIS paramEIS = (SPMeasurementParameterEIS)getSPSensingElementOnChip(currentSensor).getSensingElementOnFamily().getSPMeasurementParameter(spConfiguration);
						logSensorFiles[k][c].print(((SPMeasurementParameterEIS)getSPSensingElementOnChip(c).getSensingElementOnFamily().
								getSPMeasurementParameter(conf)).getHeaderAsString());
                        logSensorFiles[k][c].println("VadcI;VadcQ;Conductance;Susceptance;Zmod;ZPha;Rp;Cp;Lp;Current[mA];Voltage[mV]");
                    }/*else if(getSPSensingElementOnChip(c).getSensingElementOnFamily().getSensingElement().getName().contains("FIGARO")){
						logSensorFiles[k][c].print(getSPSensingElementOnChip(c).getSensingElementOnFamily().
								getSensingElement().getSpOffChipTGS8100Figaro().getParamSensor().getHeaderAsString());
						logSensorFiles[k][c].println("VadcI;VadcQ;Conductance;Susceptance;Zmod;ZPha;Rp;Cp;Lp;Current[mA];Voltage[mV]");
					} */else {
                        logSensorFiles[k][c].println("");
                    }
                }
            }
        }
    }

    public int totalMeasureToFillBuffer() throws SPException {
	    int totalToWait = 0;

		for(int i = 0; i < getSpSensingElementList().size(); i++){
			String filter = getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getFilter();
			if (filter != null && !filter.equals("")){
				try{
					totalToWait += Integer.parseInt(filter);
				} catch (Exception e){}
			}

		}

        if (getTotalNumEISConfigurations() > 0){

            for(int i = 0; i < numOfConfigurationsList.size(); i++){
            	for(int j=0; j<numOfConfigurationsList.get(i);j++) {
					totalToWait += Integer.parseInt(getSPMeasurementParameterEIS(i,j).getFilter());
				}
            }
        }

        return totalToWait;
    }


}

class SPCSVRow {
	private ArrayList<String> items = new ArrayList<String>();

	public void addItem(String item) {
		items.add(item);
	}

	public int size() {
		return items.size();
	}

	public String getCSV() {
		String out = "";
		for (int i = 0; i < items.size(); i++) {
			out += items.get(i) + ";";
		}
		return out;
	}

	public String getItem(int i) {
		return items.get(i);
	}

	public void setItem(int i, String item) {
		items.set(i, item);
	}

}
