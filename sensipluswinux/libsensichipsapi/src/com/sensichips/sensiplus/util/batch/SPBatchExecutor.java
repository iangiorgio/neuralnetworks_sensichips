package com.sensichips.sensiplus.util.batch;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.protocols.ft232rl.SPProtocolFT232RL_SENSIBUS;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.SPMeasurementRUN4;
import com.sensichips.sensiplus.level2.parameters.*;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemMeasures;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemPOTMeasures;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemPOTType;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemTGS8100Measures;
import com.sensichips.sensiplus.util.SPDataRepresentation;
import com.sensichips.sensiplus.util.SPDataRepresentationManager;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.classifier.SPClientOnLineClassifier;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import com.sensichips.sensiplus.util.rabbitmq_demo.MeasureToSend;
import com.sensichips.sensiplus.util.rabbitmq_demo.RabbitMQHandler;

import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class SPBatchExecutor {
    public static String LOG_MESSAGE = "SPBatchExecutor";
    private boolean pause = false;
    private boolean step = false;
    private SPConfiguration conf = null;

    private SPBatchParameters spBatchParameters;
    private List<SPBatchExperiment> experimentList;
    private SPBatchExecutorObservator spBatchExecutorObservator;
    private boolean RUN5_ledTurnedOn = false;
    private static int DEBUG_LEVEL = 0;
    private static int DEBUG_LEVEL_ERROR = SPLoggerInterface.DEBUG_VERBOSITY_ERROR;
    private boolean stop = false;

    public String[] eis_titles = null;

    public SPBatchExecutor(SPBatchParameters spBatchParameters, SPBatchExecutorObservator spBatchExecutorObservator, SPConfiguration conf) throws SPException {
        this.spBatchParameters = spBatchParameters;
        this.spBatchExecutorObservator = spBatchExecutorObservator;
        this.conf = conf;
        // ************************************
        // Generate a deactivation list *******
        //ArrayList<String> deactivationList = new ArrayList<>();
        //deactivationList.add("0X0000004305");
        //deactivationList.add("0X0000004205");
        //deactivationList.add("0X0000004105");
        //deactivationList.add("0X0000004005");
        //String configurationFromWichRemoveChips = "WINUX_COMUSB-CLUSTERID_0X10-FT232RL_SENSIBUS-ShortAddress-ALL_SENSORS(RUN5)";
        //ArrayList<SPConfiguration> configurationsList = SPConfigurationManager.getListSPConfiguration();
        //for(int i = 0; i < configurationsList.size(); i++){
        //    if(configurationsList.get(i).getConfigurationName().equals(configurationFromWichRemoveChips)){
        //        configurationsList.get(i).getCluster().setSPChipActivationList(deactivationList, false);
        //    }
        //}
        // Generate a deactivation list *******
        // ************************************


        experimentList = null;
        try {
            experimentList = SPBatchManager.readConfig(new FileInputStream(new File(spBatchParameters.batchFile)), conf);
        } catch (FileNotFoundException e) {
            throw new SPException("Exception in batchFile access: " + spBatchParameters.batchFile + ". " + e.getMessage());
        }


    }

    public List<SPBatchExperiment> getExperimentList(){
        return experimentList;
    }


    public synchronized void play() {
        pause = false;
        step = false;
        notify();
    }

    public synchronized void stop(){
        stop = true;
    }

    public synchronized void pause() {
        pause = true;
    }

    public synchronized void step() {
        pause = true;
        step = true;
        notify();
    }

    public void executeExperiments() throws SPException {



        for(int i= 0; i < experimentList.size(); i++){

            SPLogger.getLogInterface().d(LOG_MESSAGE,"Current configuration: " + experimentList.get(i).getSPConfiguration().getConfigurationName(), SPLoggerInterface.DEBUG_VERBOSITY_ANY);

            executeExperiment(experimentList.get(i));
        }
    }

    private SPClientOnLineClassifier classifierClient = null;
    private void executeExperiment(SPBatchExperiment currentExperiment) throws SPException {


        SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().updateTimeoutFromDriver(currentExperiment.getTimeoutUSB());
        currentExperiment.initMeasurementQueues();
        int numOfChip = SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size();
        int numOfMeasures = 13;//TODO change this fixed value
        int globalRepetition = currentExperiment.getGlobalRepetition();

        boolean measurementTimeFLAG = false;//It indicates if there is a measurement configuration (EIS or SENSOR)
                                            //whose measurement time is greater than zero.


        int rep = 0;
        float progress = 0;
        long startTime = System.currentTimeMillis();

        RabbitMQHandler rabbitMQHandler = null;

        if(currentExperiment.getRabbitmq_server_ip()!=null && currentExperiment.getRabbitmq_username()!=null &&
                currentExperiment.getRabbitmq_password()!=null && currentExperiment.getRabbitmq_virtualhost()!=null &&
                currentExperiment.getRabbitmq_exchange()!=null){

            try {

                rabbitMQHandler = new RabbitMQHandler(currentExperiment.getRabbitmq_server_ip(), currentExperiment.getRabbitmq_username(),
                        currentExperiment.getRabbitmq_password(), currentExperiment.getRabbitmq_virtualhost(), currentExperiment.getRabbitmq_exchange());
            }catch (IOException|TimeoutException e){
                SPLogger.getLogInterface().d(LOG_MESSAGE,"problem with rabbitmq connection", DEBUG_LEVEL);
                throw new SPException("Problems with RabbitMQ server connection:" +e.getMessage());
            }


        }




        try {

            NumberFormat formatter4 = new DecimalFormat("#0.000000");
            NumberFormat formatter2 = new DecimalFormat("#0.000000");

            SPMeasurementOutput feedback  = new SPMeasurementOutput();
            SPMeasurementOutput m;


            int numOfEIS = currentExperiment.getNumOfEISElement();
            //int numOfConfigurations = currentExperiment.getNum
            int numOfSensingElementOnChip = currentExperiment.getSpSensingElementList().size();

            // PREPARE MEASURE OBJECTS
            List<SPMeasurement[]> measurementOnEISList = currentExperiment.getEISMeasurement(spBatchParameters.outputDir, spBatchParameters.spProtocol);
            SPMeasurement[] measurementOnSensors = currentExperiment.getSENSORMeasurement(spBatchParameters.outputDir, spBatchParameters.spProtocol);
            SPMeasurement[] measurementOnPOT = currentExperiment.getPOTMeasurement(spBatchParameters.outputDir, spBatchParameters.spProtocol);



            //SPMeasurementParameterSENSOR array whose size are the number of sensingelementonchip.
            SPMeasurementParameterSENSORS parameterSENSORS[] = new SPMeasurementParameterSENSORS[numOfSensingElementOnChip];
            for(int i=0; i<numOfSensingElementOnChip; i++){
                parameterSENSORS[i] = new SPMeasurementParameterSENSORS(SPConfigurationManager.getSPConfigurationDefault(),
                        currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getID());
                //parameterSENSORS[i].setSPMeasurementMetaParameter(meta);

                if(currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpOffChipFigaro()!=null){
                    parameterSENSORS[i].setSpOffChipTGS8100Figaro(currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpOffChipFigaro());

                }

                // BUG RUN6 corretto con doppia lettura
                if (conf.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)){
                    parameterSENSORS[i].setFIFO_DEEP(1);
                    parameterSENSORS[i].setFIFO_READ(1);
                } else {
                    parameterSENSORS[i].setFIFO_DEEP(8);
                    parameterSENSORS[i].setFIFO_READ(1);
                }
            }

            //MARCO FERDINANDI
            //Preparing eis experiment: I clone all SPMeasurementParameterEIS in a local array in order to avoid any overwriting
            List<SPMeasurementParameterEIS[]> localSPMeasurementParameterEISList = new ArrayList<>();


            if(numOfEIS>0) {
                for(int i=0; i<numOfEIS; i++) {
                    int currentNumOfConf = currentExperiment.getNumOfConfigurationsList().get(i);
                    SPMeasurementParameterEIS[] localSPMeasurementParameterEIS = new SPMeasurementParameterEIS[currentNumOfConf];

                    for (int j = 0; j < currentNumOfConf; j++) {
                        localSPMeasurementParameterEIS[j] = new SPMeasurementParameterEIS(currentExperiment.getSPConfiguration());
                        localSPMeasurementParameterEIS[j].clone(currentExperiment.getSPMeasurementParameterEIS(i,j));
                    }
                    localSPMeasurementParameterEISList.add(localSPMeasurementParameterEIS);

                    if(currentExperiment.measurementTimeList.get(i)>0)
                        measurementTimeFLAG = true;
                }
                eis_titles = SPMeasurementParameterEIS.getDifferences(localSPMeasurementParameterEISList);

            }

            RUN5_ledTurnedOn = currentExperiment.getSPConfiguration().getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5);

            // Autoranging if requested
            if (numOfEIS > 0) {
                long startTimeAutoRange = System.currentTimeMillis();
                for (int i = 0; i < numOfEIS; i++) {
                    for(int j=0; j<currentExperiment.getNumOfConfigurationsList().get(i); j++){

                        measurementOnEISList.get(i)[j].setMeasureIndexToSave(currentExperiment.eisIndexToSaveList.get(i));

                        SPMeasurementParameterEIS param =  localSPMeasurementParameterEISList.get(i)[j];
                        if (currentExperiment.eisAutoRangeActiveFlags.get(i)) {
                            turnOnLedOnchips(measurementOnEISList.get(i)[j], currentExperiment.getSPConfiguration());
                            SPLogger.getLogInterface().d(LOG_MESSAGE,"************************* Autorange running *********************\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);

                            measurementOnEISList.get(i)[j].autoRange(param);
                            spBatchExecutorObservator.newSetting("----AUTORANGE OK ----\nFrequency: "+ param.getFrequency()+ "\nRsense: "+param.getRsenseLabel()+"\nInGain: "+param.getInGainLabel()
                                    + "\nOutGain: "+param.getOutGainLabel());
                            SPLogger.getLogInterface().d(LOG_MESSAGE,param.toString() + "\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                            SPLogger.getLogInterface().d(LOG_MESSAGE,"************************* Autorange ok **************************\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                        } else {
                            SPLogger.getLogInterface().d(LOG_MESSAGE,"************************* No Autorange *************************\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                        }
                    }

                }

                long endTimeAutoRange = System.currentTimeMillis();
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Elapsed time for autorange: " + (endTimeAutoRange - startTimeAutoRange), DEBUG_LEVEL);
            }



            SPLogger.getLogInterface().d(LOG_MESSAGE,"************************* Start measure cycle *************************", DEBUG_LEVEL);

            /*int totalMeasureToFillBuffer = currentExperiment.totalMeasureToFillBuffer();
            if (totalMeasureToFillBuffer > 0 && currentExperiment.getFillBufferBeforeStart()){
                SPLogger.getLogInterface().d(LOG_MESSAGE,"Wait for " + totalMeasureToFillBuffer + " measure before start in order to fill buffer...\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
            } else {
                SPLogger.getLogInterface().d(LOG_MESSAGE,"Measurement started.\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
            }*/


            int configRepetition = currentExperiment.getConfigRepetition();

            //double[] logDati = new double[1 + currentExperiment.getSPSensingElementOnChipSize()];

            //String globalRow;

            String[] unitMeasure = new String[numOfSensingElementOnChip];
            boolean globalRowFLAG = true;

            for(int i=0; i<numOfSensingElementOnChip; i++) {
                /*if(currentExperiment.getSensorMeasurementTimeList().get(i)>0)
                    globalRowFLAG= false;*/
                for (int k = 0; k < currentExperiment.getSensorIndexToPlotList().get(i).size(); k++){
                    if (currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().
                            getMeasureTechnique().equals("EIS") || currentExperiment.getSPSensingElementOnChip(i).
                            getSensingElementOnFamily().getSensingElement().getName().contains("FIGARO")) {
                        unitMeasure[i] = SPParamItemMeasures.unitMeasuresLabels[currentExperiment.getSensorIndexToPlotList().get(i).get(k)];
                        currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().
                                getSpDataRepresentationManager().setIndexToReduce(currentExperiment.getSensorIndexToPlotList().get(i).get(k));
                        /*TODO: remove this 7. It's a specialized version for Capacitance*/
                        /*if (currentExperiment.getSensorIndexToPlotList().get(i).get(0) == 7) {
                            currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().
                                    getSensingElement().getSpDataRepresentationManager().setInitial_multiplier("-12");
                        }*/
                    } else {
                        unitMeasure[i] = currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().
                                getSensingElement().getSpDataRepresentationManager().getMeasureUnit();
                    }

                    if (currentExperiment.getSensorMeasurementTimeList().get(i) > 0)
                        measurementTimeFLAG = true;
                }

            }
            spBatchExecutorObservator.measureStarted(currentExperiment,unitMeasure);


            long startTimeStamp = System.currentTimeMillis();
            long endTimeStamp = startTimeStamp;
            long lastEndTimeStamp;
            spBatchExecutorObservator.updateInfo((float)0, (float)0, (float) 0);

            float deltaTMedio = 0;

            // ID of the first chip
            String idOfTheFirstChip =  currentExperiment.getSPConfiguration().getCluster().getSPActiveSerialNumbers().get(0);

            boolean flagSetEISApplied = false;
            boolean flagSetSENSORApplied = false;



            //int outputIndexToPlot = -1;
            //outputIndexToSave contains the eis measurement array index to save.

            //outputIndexToPlot = currentExperiment.getIndexToPlot();
            List<SPDataRepresentationManager[]> dataRepresentationManagerEISList = new ArrayList<>();





            for(int i=0; i<numOfEIS; i++){

                /*For each EIS configuration there is the possibility to more than one plot. For this reason the array size is the product between
                * the number of configurations of the EIS and the number of plot requested for the EIS.*/
                SPDataRepresentationManager[] currentDataRepresentationManager = new SPDataRepresentationManager[currentExperiment.getNumOfConfigurationsList().get(i)*
                        currentExperiment.eisIndexToPlotList.get(i).size()];

                for(int j=0; j<currentExperiment.getNumOfConfigurationsList().get(i);j++) {
                    for(int h=0; h<currentExperiment.eisIndexToPlotList.get(i).size(); h++) {
                        int ind = j*currentExperiment.eisIndexToPlotList.get(i).size()+h;
                        currentDataRepresentationManager[ind] = new SPDataRepresentationManager(currentExperiment.eisIndexToPlotList.get(i).get(h));
                        currentDataRepresentationManager[ind].initForEISRawMeasurements();
                    }
                }

                /*for(int j=0; j<(currentExperiment.getNumOfConfigurationsList().get(i)*currentExperiment.eisIndexToPlotList.get(i).size());j++) {
                    currentDataRepresentationManager[j] = new SPDataRepresentationManager(currentExperiment.eisIndexToPlotList.get(i).get(j));
                    currentDataRepresentationManager[j].initForEISRawMeasurements();
                }*/
                dataRepresentationManagerEISList.add(currentDataRepresentationManager);
            }


            List<HashMap<Integer,SPDataRepresentationManager>> dataRepresentationManagerPOTList = new ArrayList<>();
            for(int i=0; i<currentExperiment.getPotMeasurementParameterList().size(); i++){
                /*SPDataRepresentationManager[] currentDataRepresentationManager =
                        new SPDataRepresentationManager[currentExperiment.potIndexToPlotList.get(i).size()];*/
                HashMap<Integer,SPDataRepresentationManager> dataRepresentationManagerHashMap = new HashMap<>();
                for(int j=0; j<currentExperiment.potIndexToPlotList.get(i).size();j++){

                    int realInd = currentExperiment.potIndexToPlotList.get(i).get(j)<=SPParamItemPOTMeasures.CURRENT ?
                                    currentExperiment.potIndexToPlotList.get(i).get(j):SPParamItemPOTMeasures.CURRENT;
                    if(dataRepresentationManagerHashMap.get(realInd)==null){
                        SPDataRepresentationManager currentDataRepresentationManager = new SPDataRepresentationManager(
                                currentExperiment.potIndexToPlotList.get(i).get(j)<=SPParamItemPOTMeasures.CURRENT ?
                                        currentExperiment.potIndexToPlotList.get(i).get(j):SPParamItemPOTMeasures.CURRENT);
                        currentDataRepresentationManager.initForPOTRawMeasurements();

                        dataRepresentationManagerHashMap.put(realInd,currentDataRepresentationManager);
                    }
                }
                dataRepresentationManagerPOTList.add(dataRepresentationManagerHashMap);
            }


            List<SPDataRepresentationManager[]> dataRepresentationManagerSensorsList = new ArrayList<>();
            for(int i=0; i<currentExperiment.getSpSensingElementList().size();i++){
                SPDataRepresentationManager[] currentDataRepresentationManager =
                        new SPDataRepresentationManager[currentExperiment.getSensorIndexToPlotList().get(i).size()];


                for(int j=0; j<currentExperiment.getSensorIndexToPlotList().get(i).size();j++) {
                    if(currentExperiment.getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().getMeasureTechnique().equals("EIS")||
                            currentExperiment.getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().getName().contains("FIGARO")) {
                        currentDataRepresentationManager[j] = new SPDataRepresentationManager(currentExperiment.getSensorIndexToPlotList().get(i).get(j));
                        currentDataRepresentationManager[j].initForSensorMeasurements(currentExperiment.getSpSensingElementList().
                                get(i).getSensingElementOnFamily().getSensingElement());

                        if(currentExperiment.getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().getName().contains("FIGARO"))
                            currentDataRepresentationManager[j].setMeasureUnit(SPParamItemTGS8100Measures.unitMeasuresLabelsFigaro[currentExperiment.getSensorIndexToPlotList().get(i).get(j)]);
                        else
                            currentDataRepresentationManager[j].setMeasureUnit(SPParamItemMeasures.unitMeasuresLabelsRUN5[currentExperiment.getSensorIndexToPlotList().get(i).get(j)]);
                    }else{
                        currentDataRepresentationManager[j] = new SPDataRepresentationManager(currentExperiment.getSensorIndexToPlotList().get(i).get(j));
                        currentDataRepresentationManager[j].initForSensorMeasurements(currentExperiment.getSpSensingElementList().
                                get(i).getSensingElementOnFamily().getSensingElement());
                    }
                }
                dataRepresentationManagerSensorsList.add(currentDataRepresentationManager);
            }


            List<Double> last_timestampPOTMeasurementList = new ArrayList<>();
            for(int i=0; i<currentExperiment.getPotMeasurementParameterList().size(); i++){
                last_timestampPOTMeasurementList.add(new Double(0));
            }

            double lastPOTMeasurement[][] = null;



            boolean firstDownPot = true;
            boolean firstUpPot = false;


            for(rep = 0; rep < globalRepetition && !stop    ; rep++) {

                GlobalMeasurementRow globalMeasurementRow = new GlobalMeasurementRow(numOfChip,currentExperiment);

                List<SPDataRepresentation> spDataRepresentationList = new ArrayList<>();

                //globalRow = "";

                if (rep % 10 == 0 && rep != 0) {
                    SPLogger.getLogInterface().d(LOG_MESSAGE, "Progress: " + formatter4.format((((float) (rep) * 100) / (globalRepetition))) + "% (" + (rep) + " of " + (globalRepetition) + ")\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                }

                //long singleCycleTime = System.currentTimeMillis();
                boolean stoppedRead = false;

                // This cycle explore all the parameters ccombination


                String classBelongingTo = "";
                    for(int i=0; i<numOfEIS; i++) {


                    //measurementTime handling
                    Long startTime_measurementTime = System.currentTimeMillis();
                    Long tot_measurementTime = startTime_measurementTime;

                    double xCurrentTime = 0;

                    int cont = 0;
                    do{

                    if(cont>0){
                        globalMeasurementRow = new GlobalMeasurementRow(numOfChip,currentExperiment);
                    }

                    int outputIndexToPlot = currentExperiment.eisIndexToPlotList.get(i).get(0);
                    int currentConfigurationIndex = 0;
                    SPMeasurement measurementOnEIS[] = measurementOnEISList.get(i);
                    SPMeasurementParameterEIS localSPMeasurementParameterEIS[] = localSPMeasurementParameterEISList.get(i);
                    int numOfCurrentConfig = currentExperiment.getNumOfConfigurationsList().get(i);
                    SPDataRepresentationManager[] dataRepresentationManagerEIS = dataRepresentationManagerEISList.get(i);
                    while (currentConfigurationIndex < numOfCurrentConfig) {

                        SPDelay.delay(currentExperiment.timeToWaitList.get(i));

                        classBelongingTo = "";
                        feedback = new SPMeasurementOutput();

                        //System.out.println("************************* start setINIT *************************");

                        measurementOnEIS[currentConfigurationIndex].setAutomaticADCRangeAdjustment(currentExperiment.eisAutoscaleActiveFlags.get(i));

                        measurementOnEIS[currentConfigurationIndex].setMeasureIndexToSave(currentExperiment.eisIndexToSaveList.get(i));

                        turnOnLedOnchips(measurementOnEIS[currentConfigurationIndex], currentExperiment.getSPConfiguration());

                        localSPMeasurementParameterEIS[currentConfigurationIndex].getSPMeasurementMetaParameter().setRenewBuffer(rep == 0);

                        if (false && flagSetEISApplied && ((numOfCurrentConfig + numOfSensingElementOnChip) <= 1)) {
                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* setEIS avoided *************************", DEBUG_LEVEL);
                        } else {
                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* start setEIS *************************", DEBUG_LEVEL);

                            measurementOnEIS[currentConfigurationIndex].setEIS(localSPMeasurementParameterEIS[currentConfigurationIndex], feedback);

                            flagSetEISApplied = true;
                        }

                        feedback = new SPMeasurementOutput();
                        //int conversionRate = SPMeasurement.AvailableFrequencies.adcFreq(param.getFrequency(), 10, 200);
                        SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* start setADC *************************", DEBUG_LEVEL);
                        localSPMeasurementParameterEIS[currentConfigurationIndex].setInPortADC(SPMeasurementParameterADC.INPORT_IA);
                        // BUG RUN6 corretto con doppia lettura
                        if (conf.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)){
                            localSPMeasurementParameterEIS[currentConfigurationIndex].setFIFO_DEEP(1);
                            localSPMeasurementParameterEIS[currentConfigurationIndex].setFIFO_READ(1);
                        } else {
                            localSPMeasurementParameterEIS[currentConfigurationIndex].setFIFO_DEEP(8);
                            localSPMeasurementParameterEIS[currentConfigurationIndex].setFIFO_READ(1);
                        }
                        if(localSPMeasurementParameterEIS[currentConfigurationIndex].getFrequency()<=50.0 &&
                                localSPMeasurementParameterEIS[currentConfigurationIndex].getFrequency()!=0){
                            conf.getSPProtocol().setADCdelay(new Long(Math.round(10+1000*4*1/localSPMeasurementParameterEIS[currentConfigurationIndex].getFrequency())));
                            localSPMeasurementParameterEIS[currentConfigurationIndex].
                                    setConversion_Rate(new Double(localSPMeasurementParameterEIS[currentConfigurationIndex].getFrequency()));
                        }else{
                            conf.getSPProtocol().setADCdelay(new Long(Math.round(1000*4*1/50.0)));
                            localSPMeasurementParameterEIS[currentConfigurationIndex].setConversion_Rate(50.0);
                        }
                        measurementOnEIS[currentConfigurationIndex].setADC((SPMeasurementParameterADC) localSPMeasurementParameterEIS[currentConfigurationIndex], feedback);
                        //measurementOnEIS[currentConfigurationIndex].setADC(NDATA, null, SPMeasurementParameterADC.INPORT_IA, true, feedback);
                        double[][] output = null;

                        /*int mainQueueIndex = 0;
                        for(int cont=0; cont<=i; cont++){
                            mainQueueIndex += currentExperiment.getNumOfConfigurationsList().get(cont);
                        }
                        mainQueueIndex -= 1;*/





                        double[][] toStore = null;
                        for (int j = 0; j < configRepetition; j++) {

                            feedback = new SPMeasurementOutput();

                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* start getEIS *************************", DEBUG_LEVEL);

                            output = measurementOnEIS[currentConfigurationIndex].getEIS(localSPMeasurementParameterEIS[currentConfigurationIndex], feedback);

                            toStore = new double[SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size()][];

                            for (int r = 0; r < toStore.length; r++) {
                                toStore[r] = new double[output[r].length];
                                for (int c = 0; c < output[r].length; c++) {
                                    toStore[r][c] = output[r][c];
                                }
                            }

                            //System.out.println("j: " + (j + 1) + ", capacitance: " +  output[0][5]);

                            if ((rep % currentExperiment.getOutputDecimation() == 0)) {

                                //Insert the measure in the relative main queue!
                                //QUEUE: Adding a SPChartElement in the relative queue

                                //lastEndTimeStamp = endTimeStamp;
                                endTimeStamp = System.currentTimeMillis();

                                double x = (double) (endTimeStamp - startTimeStamp) / 1000;

                                if(rep>0){
                                    int totalTimeDiff = 0;
                                    for(int k=0; k<currentExperiment.measurementTimeList.size();k++){
                                        if(k!=i){
                                            totalTimeDiff+=currentExperiment.measurementTimeList.get(k);
                                        }
                                    }

                                    for(int k=0; k<currentExperiment.getSensorMeasurementTimeList().size(); k++){
                                        totalTimeDiff+=currentExperiment.getSensorMeasurementTimeList().get(k);
                                    }


                                    x = x-(totalTimeDiff*rep/1000);
                                }
                                xCurrentTime = x;

                                for(int plot_ind = 0; plot_ind<currentExperiment.eisIndexToPlotList.get(i).size(); plot_ind++){

                                    int mainQueueIndex = 0;

                                    for (int k = i - 1; k >= 0; k--) {
                                        mainQueueIndex += currentExperiment.getNumOfConfigurationsList().get(k)*currentExperiment.eisIndexToPlotList.get(k).size();
                                    }

                                    mainQueueIndex += currentConfigurationIndex*currentExperiment.eisIndexToPlotList.get(i).size()+plot_ind;

                                    int dataRepresentationIndex = currentConfigurationIndex*currentExperiment.eisIndexToPlotList.get(i).size()+plot_ind;

                                    SPDataRepresentation appo = dataRepresentationManagerEIS[dataRepresentationIndex].adapt(output, rep == 0 && j == 0 && cont==0);
                                    if(cont==0)
                                        spDataRepresentationList.add(appo);
                                    else{
                                        spDataRepresentationList.get(mainQueueIndex).values = appo.values;
                                    }
                                    SPChartElement item = new SPChartElement(x, output, null, numOfChip, numOfMeasures, appo);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                    for(int cid = 0; cid<numOfChip; cid++){
                                        globalMeasurementRow.addEISMeasure(toStore[cid][currentExperiment.eisIndexToPlotList.get(i).get(plot_ind)],i,currentConfigurationIndex,plot_ind,cid);
                                    }
                                }

                                currentExperiment.saveEISMeasure(toStore, localSPMeasurementParameterEIS[currentConfigurationIndex], i);

                            }

                            //
                        }

                        currentConfigurationIndex++;

                    }

                    if(measurementTimeFLAG){
                        endTimeStamp = System.currentTimeMillis();
                        globalMeasurementRow.insertTimeStamp((double)(endTimeStamp - startTimeStamp)/1000);
                        spBatchExecutorObservator.newMeasure(globalMeasurementRow.toString(), spDataRepresentationList,classBelongingTo);
                    }


                    cont++;
                    tot_measurementTime = System.currentTimeMillis()-startTime_measurementTime;
                    }while(tot_measurementTime<currentExperiment.measurementTimeList.get(i));

                        SPLogger.getLogInterface().d(LOG_MESSAGE, "EIS '"+i+"'"+" - Total Time = "+tot_measurementTime, SPLoggerInterface.DEBUG_VERBOSITY_ANY);

                }
                //---end eis


                // SENSORS measure *******************************************
                for(int i=0; i<numOfSensingElementOnChip; i++){
                    parameterSENSORS[i].getSPMeasurementMetaParameter().setRenewBuffer(rep==0);

                    /*if(currentExperiment.getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().getName().contains("FIGARO")){
                        currentExperiment.getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().
                                getSpOffChipTGS8100Figaro().getParamHeater().getSPMeasurementMetaParameter().setRenewBuffer(rep==0);
                        currentExperiment.getSpSensingElementList().get(i).getSensingElementOnFamily().getSensingElement().
                                getSpOffChipTGS8100Figaro().getParamSensor().getSPMeasurementMetaParameter().setRenewBuffer(rep==0);
                    }*/
                    //parameterSENSORS[i].getSPMeasurementMetaParameter().setFillBufferBeforeStart(currentExperiment.getFillBufferBeforeStart());

                    //currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().
                    //        getSpDataRepresentationManager().setIndexToReduce(currentExperiment.getSensorIndexToPlotList().get(i).get(0));
                }



                for(int i = 0; i < numOfSensingElementOnChip; i++) {

                    Long start_measurementTime = System.currentTimeMillis();
                    Long tot_measurementTime = start_measurementTime;
                    double xCurrentTime = 0;
                    int cont =0;
                    do{

                        if(cont>0){
                            globalMeasurementRow = new GlobalMeasurementRow(numOfChip,currentExperiment);
                        }

                        SPDelay.delay(currentExperiment.getSensorTimeToWait(i));

                        measurementOnSensors[i].setMeasureIndexToSave(currentExperiment.getSensorIndexToSaveList(i));
                        turnOnLedOnchips(measurementOnSensors[i], currentExperiment.getSPConfiguration());

                        m = new SPMeasurementOutput();

                        int outputIndexToPlot = currentExperiment.getSensorIndexToPlotList().get(i).get(0);
                        parameterSENSORS[i].setFilter(currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getFilter());
                        parameterSENSORS[i].setPort(currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getPort().getPortLabel());

                        if (false && flagSetSENSORApplied && ((0 + numOfSensingElementOnChip) <= 1)) {//TODO: control it
                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* setSENSOR avoided **********************", DEBUG_LEVEL);
                        } else {
                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* start setSENSOR *************************", DEBUG_LEVEL);
                            measurementOnSensors[i].setSENSOR(parameterSENSORS[i], m);
                            flagSetSENSORApplied = true;
                        }

                        //ADDED BY MARCO FERDINANDI
                        double[][] output = measurementOnSensors[i].getSENSOR(parameterSENSORS[i], m);

                        double[][] toStore = new double[SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size()][];

                        for (int r = 0; r < toStore.length; r++) {
                            toStore[r] = new double[output[r].length];
                            for (int c = 0; c < output[r].length; c++) {
                                toStore[r][c] = output[r][c];
                            }
                        }


                        endTimeStamp = System.currentTimeMillis();
                        double x = (double) (endTimeStamp - startTimeStamp) / 1000;

                        if(rep>0){
                            int totalTimeDiff = 0;
                            for(int k=0; k<currentExperiment.measurementTimeList.size();k++){

                                totalTimeDiff+=currentExperiment.measurementTimeList.get(k);
                            }
                            for(int k=0; k<currentExperiment.getSensorMeasurementTimeList().size(); k++){
                                if(k!=i) {
                                    totalTimeDiff += currentExperiment.getSensorMeasurementTimeList().get(k);
                                }
                            }
                            x = x-(totalTimeDiff*rep/1000);
                            xCurrentTime =x ;
                        }

                        //---------------------------------------------------------------------------------------------------
                        for(int plot_ind = 0; plot_ind<currentExperiment.getSensorIndexToPlotList().get(i).size(); plot_ind++){

                            int mainQueueIndex = currentExperiment.getTotalNumberOfEISPlot();

                            int offset=0;
                            for (int k = 0; k < i; k++) {
                                offset += currentExperiment.getSensorIndexToPlotList().get(k).size();
                            }
                            mainQueueIndex +=offset;
                            mainQueueIndex += plot_ind;

                            //int dataRepresentationIndex = offset + plot_ind;

                            SPDataRepresentation appo = dataRepresentationManagerSensorsList.get(i)[plot_ind].adapt(output, rep == 0 && cont==0);
                            if(cont==0)
                                spDataRepresentationList.add(appo);
                            else{
                                spDataRepresentationList.get(mainQueueIndex).values = appo.values;
                            }

                            int numofMeas =1;
                            if (parameterSENSORS[i].getParamInternalEIS() != null ) {
                                numofMeas = SPParamItemMeasures.measureLabels.length;
                            }else if(parameterSENSORS[i].getSpOffChipTGS8100Figaro() !=null){
                                numofMeas = SPParamItemTGS8100Measures.NUM_OF_OUTPUT;
                            }

                            SPChartElement item = new SPChartElement(x, output, null, numOfChip, numofMeas, appo);
                            currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                            if (rabbitMQHandler != null) {
                                for (int k = 0; k < SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size(); k++) {
                                    rabbitMQHandler.sendMeasure(
                                            new MeasureToSend(SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().get(k).getSerialNumber(),
                                                    currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getName(),
                                                    SPConfigurationManager.getSPConfigurationDefault().getConfigurationName(),
                                                    Double.toString(x),
                                                    appo.getFormatter().format(output[k][currentExperiment.getSensorIndexToPlotList().get(i).get(0)]),
                                                    currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().getMeasureUnit(),
                                                    "40%"));
                                }
                            }

                            for(int cid = 0; cid<numOfChip; cid++){
                                globalMeasurementRow.addSENSORMeasure(toStore[cid][currentExperiment.getSensorIndexToPlotList().get(i).get(plot_ind)],i,plot_ind,cid);
                            }
                        }





                        if ((rep % currentExperiment.getOutputDecimation() == 0)) {
                            currentExperiment.saveSENSORSMeasure(toStore, i, (SPMeasurementRUN4) measurementOnSensors[i], parameterSENSORS[i]);
                        }
                        //timeForOneMeasure = (System.currentTimeMillis() - timeForOneMeasure);

                        if(measurementTimeFLAG){
                            endTimeStamp = System.currentTimeMillis();
                            globalMeasurementRow.insertTimeStamp((double)(endTimeStamp - startTimeStamp)/1000);
                            spBatchExecutorObservator.newMeasure(globalMeasurementRow.toString(), spDataRepresentationList,classBelongingTo);
                        }

                        tot_measurementTime = System.currentTimeMillis()-start_measurementTime;
                        cont++;
                    }while(tot_measurementTime<currentExperiment.getSensorMeasurementTimeList().get(i));

                    SPLogger.getLogInterface().d(LOG_MESSAGE, "Sensor '"+i+"'"+" - Total Time = "+tot_measurementTime, SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                }




                for(int i=0;i<currentExperiment.getPotMeasurementParameterList().size();i++){

                    SPMeasurementParameterPOT currentParPOT = currentExperiment.getPotMeasurementParameterList().get(i);

                    SPMeasurementOutput out_msg = new SPMeasurementOutput();

                    if(rep==0)
                        measurementOnPOT[i].setPOT(currentParPOT, out_msg);
                    double[][] output = measurementOnPOT[i].getPOT(currentParPOT, out_msg);


                    int numofMeas = 2;

                    double derivative[][] = new double[SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size()][];
                    double[][] toStore = new double[SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size()][];

                    for (int r = 0; r < toStore.length; r++) {
                        toStore[r] = new double[output[r].length];
                        for (int c = 0; c < output[r].length; c++) {
                            toStore[r][c] = output[r][c];
                        }
                    }

                    endTimeStamp = System.currentTimeMillis();
                    double x = (double) (endTimeStamp - startTimeStamp) / 1000;

                    if(rep==0)
                        last_timestampPOTMeasurementList.set(i,x);

                    for(int plot_ind = 0; plot_ind<currentExperiment.potIndexToPlotList.get(i).size(); plot_ind++){

                        int mainQueueIndex = currentExperiment.getTotalNumberOfEISPlot() + currentExperiment.getTotalNumberOfSENSORPlot() +i;

                        for (int k = i - 1; k >= 0; k--) {
                            mainQueueIndex += currentExperiment.potIndexToPlotList.get(k).size();
                        }

                        mainQueueIndex += plot_ind;

                        //int dataRepresentationIndex = currentExperiment.potIndexToPlotList.get(i).size()+plot_ind;
                        int dataRepresentationIndex = mainQueueIndex-(currentExperiment.getTotalNumberOfEISPlot() + currentExperiment.getTotalNumberOfSENSORPlot());

                        int realInd = currentExperiment.potIndexToPlotList.get(i).get(plot_ind)<=SPParamItemPOTMeasures.CURRENT ?
                                currentExperiment.potIndexToPlotList.get(i).get(plot_ind):SPParamItemPOTMeasures.CURRENT;
                        SPDataRepresentation appo = null;
                        if(currentExperiment.potIndexToPlotList.get(i).get(plot_ind)!=SPParamItemPOTMeasures.DERIVATIVE_CURRENT_VS_VOLTAGE) {
                            appo = dataRepresentationManagerPOTList.get(i).get(realInd).adapt(output,
                                    rep == 0);
                            spDataRepresentationList.add(appo);
                        }

                        SPChartElement item = null;

                        if(currentExperiment.potIndexToPlotList.get(i).get(plot_ind)!= SPParamItemPOTMeasures.CURRENT_VS_VOLTAGE &&
                                currentExperiment.potIndexToPlotList.get(i).get(plot_ind)!= SPParamItemPOTMeasures.DERIVATIVE_CURRENT_VS_VOLTAGE) {

                            if(currentParPOT.getTypeVoltammetry().equals(SPParamItemPOTType.DIFFERENTIAL_PULSE_VOLTAMMETRY)){
                                if(currentExperiment.potIndexToPlotList.get(i).get(plot_ind)==SPParamItemPOTMeasures.VOLTAGE){
                                    if(rep==0) {
                                        //double deltaV = appo.prefix.equals("m") ? currentParPOT.getPulseAmplitude() / 1.0 : currentParPOT.getPulseAmplitude() / 1000.0;
                                        double deltaV = dataRepresentationManagerPOTList.get(i).get(realInd).transformValue(currentParPOT.getPulseAmplitude() / 1.0);

                                        double output1[][] = new double[output.length][];
                                        for (int c = 0; c < numOfChip; c++) {
                                            output1[c] = new double[output[c].length];
                                            output1[c][SPParamItemPOTMeasures.VOLTAGE] = output[c][SPParamItemPOTMeasures.VOLTAGE] - deltaV;
                                        }



                                        //SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output1,
                                        //        false);

                                        SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output1);

                                        item = new SPChartElement(x - (currentExperiment.getSPConfiguration().getSPProtocol().getADCDelay()*2 + currentParPOT.getPulsePeriod()) / 2000.0,
                                                output1, null, numOfChip, numofMeas, appo1);
                                        currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                        SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output);

                                        item = new SPChartElement(x - (currentExperiment.getSPConfiguration().getSPProtocol().getADCDelay()*2 + currentParPOT.getPulsePeriod()) / 2000.0,
                                                output, null, numOfChip, numofMeas, appo2);
                                        currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                        item = new SPChartElement(x,
                                                output, null, numOfChip, numofMeas, appo2);
                                        currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                    }else{
                                        if(out_msg.potUP) {
                                            if(firstUpPot){
                                                //double deltaV = appo.prefix.equals("m") ? currentParPOT.getPulseAmplitude() / 1.0 : currentParPOT.getPulseAmplitude() / 1000.0;
                                                double deltaV = dataRepresentationManagerPOTList.get(i).get(realInd).transformValue(currentParPOT.getPulseAmplitude() / 1.0);
                                                double output1[][] = new double[output.length][];
                                                for (int c = 0; c < numOfChip; c++) {
                                                    output1[c] = new double[output[c].length];
                                                    output1[c][SPParamItemPOTMeasures.VOLTAGE] = output[c][SPParamItemPOTMeasures.VOLTAGE] - deltaV;
                                                }

                                                //SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output1,
                                                 //       false);
                                                SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output1);

                                                item = new SPChartElement((x + last_timestampPOTMeasurementList.get(i)) / 2,
                                                        output1, null, numOfChip, numofMeas, appo1);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                //SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output,
                                                //        false);

                                                SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output);

                                                item = new SPChartElement((x + last_timestampPOTMeasurementList.get(i)) / 2,
                                                        output, null, numOfChip, numofMeas, appo2);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                item = new SPChartElement(x,
                                                        output, null, numOfChip, numofMeas, appo2);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                                firstUpPot = false;


                                            }else {
                                                firstDownPot = true;
                                                //double deltaV = appo.prefix.equals("m") ? currentParPOT.getPulseAmplitude() / 1.0 : currentParPOT.getPulseAmplitude() / 1000.0;
                                                double deltaV = dataRepresentationManagerPOTList.get(i).get(realInd).transformValue(currentParPOT.getPulseAmplitude() / 1.0);
                                                double output1[][] = new double[output.length][];
                                                for (int c = 0; c < numOfChip; c++) {
                                                    output1[c] = new double[output[c].length];
                                                    output1[c][SPParamItemPOTMeasures.VOLTAGE] = output[c][SPParamItemPOTMeasures.VOLTAGE] - deltaV;
                                                }

                                                //SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output1,
                                                //        false);
                                                SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output1);

                                                item = new SPChartElement(last_timestampPOTMeasurementList.get(i),
                                                        output1, null, numOfChip, numofMeas, appo1);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                item = new SPChartElement((x + last_timestampPOTMeasurementList.get(i)) / 2,
                                                        output1, null, numOfChip, numofMeas, appo1);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                //SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output,
                                                //        false);

                                                SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output);


                                                item = new SPChartElement((x + last_timestampPOTMeasurementList.get(i)) / 2,
                                                        output, null, numOfChip, numofMeas, appo2);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                item = new SPChartElement(x,
                                                        output, null, numOfChip, numofMeas, appo2);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                            }
                                        }else{
                                            if(firstDownPot) {

                                                //SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output,
                                                //        false);

                                                SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output);

                                                item = new SPChartElement((x + last_timestampPOTMeasurementList.get(i)) / 2,
                                                        output, null, numOfChip, numofMeas, appo2);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                //double deltaV = appo.prefix.equals("m") ? currentParPOT.getPulseAmplitude() / 1.0 : currentParPOT.getPulseAmplitude() / 1000.0;
                                                double deltaV = dataRepresentationManagerPOTList.get(i).get(realInd)
                                                        .transformValue(currentParPOT.getPulseAmplitude() / 1.0);
                                                double output1[][] = new double[output.length][];
                                                for (int c = 0; c < numOfChip; c++) {
                                                    output1[c] = new double[output[c].length];
                                                    output1[c][SPParamItemPOTMeasures.VOLTAGE] = output[c][SPParamItemPOTMeasures.VOLTAGE] - deltaV;
                                                }
                                                //SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output1,
                                                //        false);
                                                SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output1);


                                                item = new SPChartElement((x + last_timestampPOTMeasurementList.get(i)) / 2,
                                                        output1, null, numOfChip, numofMeas, appo1);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);


                                                item = new SPChartElement(x,
                                                        output1, null, numOfChip, numofMeas, appo1);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                                firstDownPot = false;
                                            }else{
                                                //SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output,
                                                //        false);

                                                SPDataRepresentation appo2 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output);

                                                item = new SPChartElement(last_timestampPOTMeasurementList.get(i),
                                                        output, null, numOfChip, numofMeas, appo2);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                item = new SPChartElement((x + last_timestampPOTMeasurementList.get(i)) / 2,
                                                        output, null, numOfChip, numofMeas, appo2);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                //double deltaV = appo.prefix.equals("m") ? currentParPOT.getPulseAmplitude() / 1.0 : currentParPOT.getPulseAmplitude() / 1000.0;
                                                double deltaV = dataRepresentationManagerPOTList.get(i).get(realInd).transformValue(currentParPOT.getPulseAmplitude() / 1.0);
                                                double output1[][] = new double[output.length][];
                                                for (int c = 0; c < numOfChip; c++) {
                                                    output1[c] = new double[output[c].length];
                                                    output1[c][SPParamItemPOTMeasures.VOLTAGE] = output[c][SPParamItemPOTMeasures.VOLTAGE] - deltaV;
                                                }
                                                //SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i)[dataRepresentationIndex].adapt(output1,
                                                //        false);
                                                SPDataRepresentation appo1 = dataRepresentationManagerPOTList.get(i).get(realInd).getSPDataRepresentation(output1);


                                                item = new SPChartElement((x + last_timestampPOTMeasurementList.get(i)) / 2,
                                                        output1, null, numOfChip, numofMeas, appo1);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);

                                                item = new SPChartElement(x,
                                                        output1, null, numOfChip, numofMeas, appo1);
                                                currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                                firstDownPot = false;
                                            }
                                            firstUpPot=true;
                                        }

                                    }
                                }else if(currentExperiment.potIndexToPlotList.get(i).get(plot_ind)==SPParamItemPOTMeasures.CURRENT){
                                    item = new SPChartElement(x,
                                            output, null, numOfChip, numofMeas, appo);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);


                                    /*item = new SPChartElement(x,
                                            output, null, numOfChip, numofMeas, appo);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);*/
                                }

                                last_timestampPOTMeasurementList.set(i, x);

                            }else {
                                if (rep == 0) {
                                    item = new SPChartElement(last_timestampPOTMeasurementList.get(i), output, null, numOfChip, numofMeas, appo);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                } else {
                                    //Here the current measured point is inserted in the relative queue twice with different x value.
                                    //This is done in order to obtain the graphic view of the measured values
                                    item = new SPChartElement(last_timestampPOTMeasurementList.get(i), output, null, numOfChip, numofMeas, appo);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                    item = new SPChartElement(x, output, null, numOfChip, numofMeas, appo);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                    last_timestampPOTMeasurementList.set(i, x);
                                }
                            }
                        }else{
                            if(currentExperiment.potIndexToPlotList.get(i).get(plot_ind)== SPParamItemPOTMeasures.DERIVATIVE_CURRENT_VS_VOLTAGE){
                                if(rep>0){
                                    //double derivative[][] = new double[output.length][output[0].length];
                                    for(int h=0; h<output.length; h++){
                                        derivative[h] = new double[output[h].length];
                                        for( int g=0; g<output[h].length; g++) {
                                            derivative[h][g] = output[h][g] - lastPOTMeasurement[h][g];
                                        }
                                    }

                                    SPDataRepresentation spDataRepresentation = dataRepresentationManagerPOTList.get(i).get(realInd).adapt(derivative,
                                            rep == 0);

                                    spDataRepresentationList.add(spDataRepresentation);

                                    spDataRepresentation.measureUnit = dataRepresentationManagerPOTList.get(i).get(SPParamItemPOTMeasures.CURRENT).getMeasureUnit()+"/"+
                                            dataRepresentationManagerPOTList.get(i).get(SPParamItemPOTMeasures.VOLTAGE).getMultiplierPrefix()+
                                            dataRepresentationManagerPOTList.get(i).get(SPParamItemPOTMeasures.VOLTAGE).getMeasureUnit();

                                    if (out_msg.resetCycle) {
                                        item = new SPChartElement(output[0][SPParamItemPOTMeasures.VOLTAGE], derivative, null, numOfChip, numofMeas, spDataRepresentation);
                                        item.setResetChart(true);
                                        currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                    } else {
                                        item = new SPChartElement(output[0][SPParamItemPOTMeasures.VOLTAGE], derivative, null, numOfChip, numofMeas, spDataRepresentation);
                                        currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                    }
                                }
                                lastPOTMeasurement = new double[output.length][output[0].length];
                                for(int h=0; h<output.length; h++){
                                    for( int g=0; g<output[h].length; g++) {
                                        lastPOTMeasurement[h][g] = output[h][g];
                                    }
                                }

                            }else if(currentExperiment.potIndexToPlotList.get(i).get(plot_ind)== SPParamItemPOTMeasures.CURRENT_VS_VOLTAGE) {

                                if (out_msg.resetCycle) {
                                    item = new SPChartElement(output[0][SPParamItemPOTMeasures.VOLTAGE], output, null, numOfChip, numofMeas, appo);
                                    item.setResetChart(true);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                } else {
                                    item = new SPChartElement(output[0][SPParamItemPOTMeasures.VOLTAGE], output, null, numOfChip, numofMeas, appo);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);
                                }
                            }

                            //item.setResetChart(false);
                        }

                        double potToStore[][] = new double[toStore.length][];
                        for(int h=0; h<toStore.length;h++){
                            potToStore[h] = new double[2*toStore[h].length];
                            for(int g=0; g<toStore[h].length; g++){
                                potToStore[h][g] = toStore[h][g];
                            }
                            if(rep>0 && currentExperiment.potIndexToPlotList.get(i).get(plot_ind)== SPParamItemPOTMeasures.DERIVATIVE_CURRENT_VS_VOLTAGE) {
                                for (int g = 2; g < toStore[h].length + 2; g++) {
                                    potToStore[h][g] = derivative[h][g - 2];
                                }
                            }
                        }

                        for(int cid = 0; cid<numOfChip; cid++)
                        {

                            if(currentExperiment.potIndexToPlotList.get(i).get(plot_ind)!=SPParamItemPOTMeasures.CURRENT_VS_VOLTAGE) {
                                globalMeasurementRow.addPOTMeasure(potToStore[cid][currentExperiment.potIndexToPlotList.get(i).get(plot_ind)], i, plot_ind, cid);
                            }
                        }
                    }

                    currentExperiment.savePOTMeasure(toStore, currentExperiment.getPotMeasurementParameterList().get(i), i);
                }


                lastEndTimeStamp = endTimeStamp;
                endTimeStamp = System.currentTimeMillis();
                //globalRow = formatter2.format(((double)(endTimeStamp - startTimeStamp))/1000) + ";" + globalRow;


                stoppedRead = true;


                globalMeasurementRow.insertTimeStamp((double)(endTimeStamp - startTimeStamp)/1000);

                /**Here the measures are sent to the server for the classification if the classify sample is active*/
                if (spBatchExecutorObservator.classifySample()) {
                    try {
                        if (classifierClient == null) {
                            //classifierClient = new SPClientOnLineClassifier("hego.unicas.it", 43547, "5A05", 3);
                            classifierClient = new SPClientOnLineClassifier("localhost", 8182, 5);
                            //classifierClient = new SPClientOnLineClassifier("localhost", 8184, 7);
                        }
                        classBelongingTo = classifierClient.sendDataToClassifier(globalMeasurementRow.toString());

                    } catch (Exception e) {
                        classifierClient = null;
                        e.printStackTrace();
                    }
                    SPDelay.delay(1);

                } else if (classifierClient != null) {
                    try {
                        classifierClient.close();
                        classifierClient = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }



                if ((rep % currentExperiment.getOutputDecimation() == 0 && !measurementTimeFLAG)){
                    //lobalRow = formatter2.format(((double)(endTimeStamp - startTimeStamp))/1000) + ";" + globalRow;

                    //TODO:ATTENTION- TEMPOrary COMMENT
                    //spBatchExecutorObservator.newMeasure(globalRow, spDataRepresentationList,classBelongingTo);

                    //globalMeasurementRow.insertTimeStamp((double)(endTimeStamp - startTimeStamp)/1000);

                    spBatchExecutorObservator.newMeasure(globalMeasurementRow.toString(), spDataRepresentationList,classBelongingTo);
                }







                deltaTMedio += (float)(endTimeStamp - lastEndTimeStamp)/1000;
                progress = ((float) (rep + 1) * 100) / globalRepetition;

                spBatchExecutorObservator.updateInfo(deltaTMedio/(rep + 1),
                        (rep + 1)/((float)(endTimeStamp - startTimeStamp)/1000),
                        progress);


                verityPause();

                //Thread.sleep(currentExperiment.getDelay());

                if(currentExperiment.getDelay()!=0){
                    //Command to stop stimulus on SENSIPLUS ANALOG PORTS
                    conf.standby();
                }
                SPDelay.delay(currentExperiment.getDelay());

            }

            conf.standby();

            //spBatchExecutorObservator.updateInfo(deltaTMedio/globalRepetition,
            //        globalRepetition/((float)(endTimeStamp - startTimeStamp)/1000),
            //        (float)100);


            //currentExperiment.getSPConfiguration().stopConfiguration(true);

            //currentExperiment.getSPConfiguration().getSPProtocol().sendInstruction("WRITE COMMAND S 0X14", null);
            currentExperiment.closeFiles();

            spBatchExecutorObservator.measureFinished(stop);

        } catch (Exception ex) {

            spBatchExecutorObservator.measureFinished(stop);
            ex.printStackTrace();

            String messaggio = "Exception during measurement. Repetition: " + rep + ", progress: " + progress + "\n" +
                    "Error message: " + ex.getMessage();
            SPLogger.getLogInterface().d(LOG_MESSAGE, messaggio, DEBUG_LEVEL_ERROR);
            throw new SPException(messaggio);
        }
        long endTime = System.currentTimeMillis();
        System.out.println();
        SPLogger.getLogInterface().d(LOG_MESSAGE,"Elapsed time: " + (endTime-startTime)/1000 + "\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);

        SPLogger.getLogInterface().d(LOG_MESSAGE,"Total byte sent: " + SPProtocolFT232RL_SENSIBUS.TOTAL_BYTE_SENT, DEBUG_LEVEL);

        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPMeasurementRUN4.GET_EIS_ELAPSED: " + SPMeasurementRUN4.GET_EIS_ELAPSED, DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPMeasurementRUN4.GET_I_ELAPSED: " + SPMeasurementRUN4.GET_I_ELAPSED, DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPMeasurementRUN4.GET_Q_ELAPSED: " + SPMeasurementRUN4.GET_Q_ELAPSED, DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPMeasurementRUN4.GET_DATA_ELAPSED: " + SPMeasurementRUN4.GET_DATA_ELAPSED, DEBUG_LEVEL);

        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPProtocol.SPMNEMONIC_ELAPSED: " + SPProtocol.SPMNEMONIC_ELAPSED + ", with: ", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPDecoder.DECODE_SINGLE_INSTRUCTION_ELAPSED: " + SPDecoder.DECODE_SINGLE_INSTRUCTION_ELAPSED, DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPProtocolFT232RL_SENSIBUS.SENDL_ELAPSED: " + SPProtocolFT232RL_SENSIBUS.SENDL_ELAPSED + ", di cui: ", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPProtocolFT232RL_SENSIBUS.WRITE_ELAPSED: " + SPProtocolFT232RL_SENSIBUS.WRITE_ELAPSED, DEBUG_LEVEL);

        for(int i = 0; i < SPProtocolFT232RL_SENSIBUS.COUNTER.length; i++){
            if (SPProtocolFT232RL_SENSIBUS.COUNTER[i] > 0){
                SPLogger.getLogInterface().d(LOG_MESSAGE,"\t" + i + " - " + SPProtocolFT232RL_SENSIBUS.COUNTER[i], DEBUG_LEVEL);
            }
        }

        SPLogger.getLogInterface().d(LOG_MESSAGE,"File saved. Program ended.\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);

    }


    private synchronized void verityPause() throws InterruptedException {

        if (pause){
            wait();
        }
        pause = step;

    }


    private void turnOnLedOnchips(SPMeasurement spMeasurement, SPConfiguration spConfiguration) throws SPException {

        SPCluster cluster = SPConfigurationManager.getSPConfigurationDefault().getCluster();
        ArrayList<String> turnOnList = new ArrayList<>();
        /*turnOnList.add("0X0000004005");
        turnOnList.add("0X0000004105");
        turnOnList.add("0X0000004205");
        turnOnList.add("0X0000004305");*/
        //turnOnList.add("0X0000004405");

        SPCluster newCluster = cluster.generateTempCluster(turnOnList);

        if (RUN5_ledTurnedOn && turnOnList.size() > 0){
            SPMeasurementParameterActuator actuator = new SPMeasurementParameterActuator(spConfiguration);
            actuator.setTemporaryCluster(newCluster);
            actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_RDY);
            actuator.setMode(SPMeasurementParameterActuator.ON);
            spMeasurement.setACTUATOR(actuator);

            actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_INT);
            spMeasurement.setACTUATOR(actuator);
            //setACTUATOR
            RUN5_ledTurnedOn = false;
        }
    }

}
