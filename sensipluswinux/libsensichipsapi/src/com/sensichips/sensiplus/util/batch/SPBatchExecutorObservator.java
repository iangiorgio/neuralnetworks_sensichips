package com.sensichips.sensiplus.util.batch;

import com.sensichips.sensiplus.util.SPDataRepresentation;
import com.sensichips.sensiplus.util.SPDataRepresentationManager;

import java.text.DecimalFormat;
import java.util.List;

public interface SPBatchExecutorObservator {

    void measureStarted(SPBatchExperiment currentExperiment, String[] unitMeasure);

    void newMeasure(String row, List<SPDataRepresentation> spDataRepresentationList,String className);

    void measureFinished(boolean stopped);

    void updateInfo(float deltaT, float measureForSecond, float progress);

    void newSetting(String row);

    boolean classifySample();



}
