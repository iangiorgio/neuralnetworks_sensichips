package com.sensichips.sensiplus.util.log;

/**
 * Created by mario on 20/12/2015.
 */
public interface SPLoggerInterface {

    int DEBUG_VERBOSITY_ERROR = 32;

    int DEBUG_VERBOSITY_L0 = 4;
    int DEBUG_VERBOSITY_L1 = 2;
    int DEBUG_VERBOSITY_L2 = 1;
    int DEBUG_VERBOSITY_ANY = 8;
    int DEBUG_VERBOSITY_READ_WRITE = 16;

    void d(String msgType, String msg, int debug_level);
}
