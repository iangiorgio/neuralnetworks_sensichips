package com.sensichips.sensiplus.util.log;

/**
 * Act as a delegated class. If any SPLogger is furnished, the will be istantiated the defautl
 * logger based on System.out.println.
 * Created by mario on 20/12/2015.
 */
public class SPLogger {


    private static SPLoggerInterface log;
    public static int DEBUG_TH = SPLoggerInterface.DEBUG_VERBOSITY_L0 | SPLoggerInterface.DEBUG_VERBOSITY_L1 | SPLoggerInterface.DEBUG_VERBOSITY_L2;

    public static void setLogInterface(SPLoggerInterface logImpl) {
        log = logImpl;
    }

    public static SPLoggerInterface getLogInterface() {
        if (log == null) {
            log = new SPLoggerDefaultImpl();
        }
        return log;
    }
}
