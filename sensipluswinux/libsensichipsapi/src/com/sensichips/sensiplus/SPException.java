package com.sensichips.sensiplus;

import com.sensichips.sensiplus.SPException;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPException extends Exception {
    public SPException(String message) {
        super(message);
    }
}
