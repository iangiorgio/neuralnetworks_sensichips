package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.*;
import com.sensichips.sensiplus.level2.parameters.*;
import com.sensichips.sensiplus.level2.parameters.items.*;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusIQ;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusPOT;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusSENSOR;
import com.sensichips.sensiplus.level2.util.SPMeasurementUtil;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLogger;

import java.util.ArrayList;
import java.util.List;

import static com.sensichips.sensiplus.level1.chip.Utility.responseTokenizerByte;


/**
 * The High Level part of the library presents to the programmers all the methods available for different types
 * of analytical measurements.  Alle the classes of this level are contained in com.sensichips.android.level2.
 * In particular SPMeasurement is the abstract class with all the expected methods while SPMeasurementRUN4
 * is the specialized class for the Run4 of the chip (2016). In addition the com.sensichips.android.level2.parameter
 * contains a set of classes created to manage the input parameters of the measurement methods.
 * In this module (in particulare in the SPMeasurementRUN4 class) it   is stored the complete state of the current measure.
 * In order to define the current measure type a call to the corresponding setMeasureType method is needed. For each
 * type of measurement there are two methods: setMeasureType and getMeasureType.
 * The setMeasureType sets the initial chip state and internal method parameters that will be used by
 * subsequent getMeasureType calls. The initial stte then evolves at each getMeasureType according to the
 * type of action that it's being performed. For each setMeasureType its possible to have 0 or more
 * getMeasureType subsequently calls. Each getMeasureType request to the chip one or more
 * measurements, it returns to the caller the corresponding numeric values and update the internal state of the
 * method.
 * Therefore the setMeasureType:
 * - parses the input parameters in order to identify errors and make some computation on it (for example in
 * order to identify the frequency available on the chip with respect to the frequency requested by the caller)
 * - send a sequence of WRITE instruction the the chip by mean of the L1 library;
 * - define the initial state of the L2 module.
 * <p>
 * The getMeasureType will then be called a number of times according to the performed measurement:
 * - each call will interact with the chip with WRITE and READ accesses, to read required data
 * - it will update the API module state and prepare for subsequent reads
 * <p>
 * For example:
 * setEIS/getEIS (Electrical Impedance Spectroscopy)
 * - setEIS writes into the chip registers that are involved in this type of measure by preparaing the stimulus
 * <p>
 * and the measurement section;
 * - consequently the getEIS call implements two measurements: in quadrature and in phase. By means of
 * this two value the methods computes Resistance, Capacitance and Inductance.
 * <p>
 * setPOT/getPOT (Potentiostat)
 * - setPOT sets the chip for the subsequent reads and initializes parameters within the API, i.e. the type of
 * <p>
 * ramp to be generated among the choices presented to the used,
 * - back-to-back getPOT calls will set the chip to the output stimulus according to the selected ramp and read
 * the sensor according to the user specified timing.
 * <p>
 * Methods of Level 2 throws SPException in case of errors with SPMeasurement API or
 * SPException in case of errors in the input parameters.
 * <p>
 * <p>
 * Property: Sensichips s.r.l.
 *
 * @author: Mario Molinara, mario.molinara@sensichips.com, m.molinara@unicas.it
 */
public class SPMeasurementRUN5_PC extends SPMeasurementRUN4 {

    public SPMeasurementRUN5_PC(SPConfiguration conf, String logDirectory, String measure_type) throws SPException {
        super(conf, logDirectory, measure_type);
    }


    @Override
    public synchronized void setSENSOR(SPMeasurementParameterSENSORS param, SPMeasurementOutput m)
            throws SPException {

        SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(param.getSensorName());
        SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        Double conversionRate50 = (double)50;
        String InPortADC = SPMeasurementParameterADC.INPORT_IA;
        //Integer NDATA = 1;
        Integer FIFO_DEEP = param.getFIFO_DEEP();
        Integer FIFO_READ = param.getFIFO_READ();

        generateCalibrationParameters_new(param.getSensorName());

        if(param.getParamInternalEIS()!=null){

            SPMeasurementOutput m2 = new SPMeasurementOutput();
            //SPMeasurementParameterEIS paramInternal = (SPMeasurementParameterEIS)spSensingElementOnChip.getSensingElementOnFamily().getSPMeasurementParameter(spConfiguration);
            //paramInternal.setSPMeasurementMetaParameter(param.getSPMeasurementMetaParameter());


            SPMeasurementParameterEIS paramInternal = param.getParamInternalEIS();

            if(paramInternal.getFrequency()<=50.0 &&
                    paramInternal.getFrequency()!=0){
                spProtocol.setADCdelay(new Long(Math.round(10+1000*4*1/paramInternal.getFrequency())));

                paramInternal.setConversion_Rate(new Double(paramInternal.getFrequency()));
            }else if(paramInternal.getFrequency()==0){
                if(param.getSensorName().contains("FIGARO")) {
                    paramInternal.setDCBiasP(400);
                }
                paramInternal.setConversion_Rate(50.0);
            }
            else{
                spProtocol.setADCdelay(new Long(Math.round(1000*4*1/50.0)));
                paramInternal.setConversion_Rate(50.0);
            }

            // Effettua la ricerca inversa: posseggo il valore, ritrovo la label
            setEIS(paramInternal, m2);
            ref.log += m2.out;
            m2 = new SPMeasurementOutput();




            setADC(FIFO_DEEP,FIFO_READ, paramInternal.getInGainLabel(), paramInternal.getConversion_Rate(), InPortADC, true, m2);
            ref.log += m2.out;

        }else if(param.getSpOffChipTGS8100Figaro()!=null){

            if(param.getSpOffChipTGS8100Figaro().getTGS8100FigaroMode().
                    equals(SPOffChipTGS8100Figaro.operationModes[SPOffChipTGS8100Figaro.EIS_MODE])){

            }else if(param.getSpOffChipTGS8100Figaro().getTGS8100FigaroMode().
                    equals(SPOffChipTGS8100Figaro.operationModes[SPOffChipTGS8100Figaro.TCO_MODE])){



            }

            if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null) {
                status = new SPMeasurementStatusSENSOR(this, param, SPParamItemTGS8100Measures.NUM_OF_OUTPUT,spProtocol, "");
            }


        }else if (spSensingElement.getMeasureTechnique().equals("DIRECT")) {

            /*ref.instructionToSend = "WRITE DAS_CONFIG M 0x07FF";
            sendSequence(ref);*/

            if (spSensingElement.getName().equals("ONCHIP_TEMPERATURE") || spSensingElement.getName().equals("ONCHIP_VOLTAGE")) {

                String instruction = "WRITE CHA_SELECT S 0X";
                String instructionNum = "110" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);

                ref.instructionToSend = "WRITE DSS_SELECT M 0x0020"; //  -- disconnect DAS from IA
                sendSequence(ref);

                String InGain = "40"; // Aggiustare coerentemente a CHA_CONDIT
                if (spSensingElement.getName().equals("ONCHIP_VOLTAGE")){
                    ref.instructionToSend = "WRITE CHA_CONDIT M 0x031F";
                } else {
                    ref.instructionToSend = "WRITE CHA_CONDIT M 0x011F"; // InGain = 40 => 001F; InGain = 20 => 011F, InGain = 12 => 021F; InGain = 1 => 031F
                }
                sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_FILTER M 0x9200";   //ref.instructionToSend = "WRITE CHA_FILTER+1 S 0xFE"; da verificare con Simone: F6 o FE
                sendSequence(ref);

                ref.instructionToSend = "WRITE ANA_CONFIG S 0x1B";
                sendSequence(ref);


                SPMeasurementOutput m2 = new SPMeasurementOutput();

                setADC(FIFO_DEEP,FIFO_READ, InGain, conversionRate50, InPortADC, true, m2);
                ref.log += m2.out;

                if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null) {
                    status = new SPMeasurementStatusSENSOR(this, param, 1,spProtocol, "");
                }

            } else if (spSensingElement.getName().equals("ONCHIP_LIGHT") || (spSensingElement.getName().equals("ONCHIP_DARK"))){

                ref.instructionToSend = "WRITE DSS_DIVIDER M 0x0002"; //  -- disconnect DAS from IA
                sendSequence(ref);

                String instruction = "WRITE CHA_SELECT S 0X";
                String instructionNum = "010" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);

                instruction = "WRITE DSS_SELECT M 0X"; //  -- disconnect DAS from IA
                instructionNum = "00000000001" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_FILTER M 0x9200";
                sendSequence(ref);


                String InGain = "40"; // Aggiustare coerentemente a CHA_CONDIT
                ref.instructionToSend = "WRITE CHA_CONDIT M 0x001F"; // 40 Ingain --
                sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_FILTER+1 S 0xB2";
                sendSequence(ref);

                ref.instructionToSend = "WRITE ANA_CONFIG S 0x3F";
                sendSequence(ref);

                SPMeasurementOutput m2 = new SPMeasurementOutput();
                setADC(FIFO_DEEP,FIFO_READ, InGain, conversionRate50, InPortADC, true, m2);
                ref.log += m2.out;

                if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null) {
                    status = new SPMeasurementStatusSENSOR(this, param, 1,spProtocol, "");
                }
            } else {
                throw new SPException("In setSENSOR. Sensing element unavailable: " + spSensingElement.getMeasureTechnique());
            }
        } else {
            throw new SPException("Measure technique unavailable: " + spSensingElement.getMeasureTechnique());
        }
    }


    public void restartBandGap(SPMeasurementParameterSENSORS param, boolean disableChipsBandgapProblems) throws SPException {

        SPMeasurementOutput out = new SPMeasurementOutput();

        double upperLimitVoltage = 3900;
        double lowerLimitVoltage = 1500;

        setSENSOR(param, out);

        SendInstructionInOut ref;
        SPCluster temporaryCluster = null;
        boolean onchip_voltage_ok = false;
        int maxTryal = 10;
        int counter = 0;
        int i = 0;
        String row = "";
        while(!onchip_voltage_ok && counter < maxTryal){
            // Restart bandgap
            ref = new SendInstructionInOut(temporaryCluster);
            ref.instructionToSend = spConfiguration.getSPBandGapRATIOMETRIC();
            sendSequence(ref);
            ref = new SendInstructionInOut(temporaryCluster);
            ref.instructionToSend = spConfiguration.getSPBandGapFromVCC();
            sendSequence(ref);

            //Verify if chips are started
            double[][] output = getSENSOR(param, out);
            i = 0;
            onchip_voltage_ok = true;
            while(i < numOfChips){
                onchip_voltage_ok = onchip_voltage_ok && output[i][0] < upperLimitVoltage && output[i][0] > lowerLimitVoltage;
                i++;
            }
            counter++;

            row = "";
            for(int k = 0; k < numOfChips; k++){
                //row += "" + output[k][0] + ((k < (numOfChips - 1)) ? ", ":"");
                cluster.getActiveSPChipList().get(k).setLastVBandGap((float)output[k][0]);
                row += "" + k + ") " + output[k][0] + " (" + cluster.getSPActiveSerialNumbers().get(k) + ")\n";
            }
            System.out.println("\nRestartBandGap repetition: " + counter + " Vcc:\n" + row);
        }

        if (!onchip_voltage_ok){
            if(disableChipsBandgapProblems) {
                List<SPChip> chipsToDisable = new ArrayList<>();
                List<SPChip> activeChips = new ArrayList<>();
                for (int k = 0; k < numOfChips; k++) {
                    double currentVBandgap = cluster.getActiveSPChipList().get(k).getLastVBandGap();
                    if(!(currentVBandgap<upperLimitVoltage && currentVBandgap>lowerLimitVoltage)) {
                        chipsToDisable.add(cluster.getActiveSPChipList().get(k));
                    }else{
                        activeChips.add(cluster.getActiveSPChipList().get(k));
                    }
                }
                cluster.setSPChipAllDisactive();
                cluster.setSPChipActive(activeChips);
                cluster.setChipWithBandgapProblem(chipsToDisable);

            }else {
                throw new SPException("Bandgap problem: impossible to restart!");
            }
        }
    }


    private double[][] getSingleSENSOR(SPMeasurementParameterSENSORS param,SPSensingElement spSensingElement,SPMeasurementOutput m) throws SPException{
        double output[][] = null;
        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        double k = 1;
        if (spSensingElement.getName().equals("ONCHIP_VOLTAGE")){
            k = 3 * spConfiguration.getVREF() / NORMALIZATION_FACTOR;
        }

        output = new double[cluster.getActiveSPChipList().size()][1];

        if(this.isSystemLevelChopperActive()) {
            ref.instructionToSend = ((SPMeasurementStatusSENSOR) status).getInstructionToggleCHAFILTERPCI();
            sendSequence(ref);
        }


        int[][] buffer = getDATA(param, true, true, new SPMeasurementSequential(param.isSequentialMode()), m);

        if(this.isSystemLevelChopperActive()) {
            if (status.getSystemLevelChopper().isPCIBitHigh()) {
                for (int i = 0; i < buffer.length; i++) {
                    for (int j = 0; j < buffer[i].length; j++) {
                        buffer[i][j] = buffer[i][j] * -1;
                    }
                }
            }

            double[][] appo = new double[buffer.length][buffer[0].length];
            for (int i = 0; i < buffer.length; i++) {
                for (int j = 0; j < buffer[i].length; j++) {
                    appo[i][j] = buffer[i][j];
                }
            }


            status.updateSystemLevelChopper(appo);


            for (int i = 0; i < numOfChips; i++) {
                output[i][0] = N[i] + (M[i] * appo[i][0] * k);// * SPConfiguration.VREF_1); // / NORMALIZATION_FACTOR;
                //output[i][0] = (1 * buffer[i][0] * k);// * SPConfiguration.VREF_1); // / NORMALIZATION_FACTOR;

            }
        }else{
            for (int i = 0; i < numOfChips; i++) {
                output[i][0] = N[i] + (M[i] * buffer[i][0] * k);// * SPConfiguration.VREF_1); // / NORMALIZATION_FACTOR;
                //output[i][0] = (1 * buffer[i][0] * k);// * SPConfiguration.VREF_1); // / NORMALIZATION_FACTOR;

            }
        }

        status.updateStatus(output);
        return output;

    }

    /*private double[][] getFIGARO(){
        double output[][] = new double[cluster.getActiveSPChipList().size()][SPOffChipFigaro.NUM_OF_OUTPUT];



        return output;
    }*/

    @Override
    public synchronized double[][] getSENSOR(SPMeasurementParameterSENSORS param, SPMeasurementOutput m)
            throws SPException {

        SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(param.getSensorName());
        SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();

        //TODO: Understand it
        //??????????generateCalibrationParameters_new(param.getSensorName());

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());

        double output[][] = null;
        //Integer NDATA = 2;

        if(param.getParamInternalEIS()!=null){

            output = new double[cluster.getActiveSPChipList().size()][SPMeasurementParameterEIS.NUM_OUTPUT ];

            //SPMeasurementOutput m2 = new SPMeasurementOutput();
            //SPMeasurementParameterEIS paramInternal = (SPMeasurementParameterEIS) spSensingElementOnChip.getSensingElementOnFamily().getSPMeasurementParameter(spConfiguration);
            //paramInternal.setSPMeasurementMetaParameter(paramInternal.getSPMeasurementMetaParameter());

            SPMeasurementParameterEIS paramInternal = param.getParamInternalEIS();

            double buffer[][] = getEIS(paramInternal, m);

            for(int i = 0; i < buffer.length; i++) {
               for(int j = 0; j< SPParamItemMeasures.measureLabels.length; j++){
                   if(j==paramInternal.getMeasure())
                       output[i][j] = M[i]*buffer[i][j]+N[i];
                   else
                    output[i][j] = buffer[i][j];
               }
            }

            //ADDED BY MARCO FERDINANDI
            //Se è stato rilevato l'ADC Overflow e è settato a true l'automaticADCRangeAdjustment,
            //la get Sensor aggiorna i parametri nei sensingelement. Per questo motivo, per riportarsi alle condizioni
            //originali, bisogna solo ricaricare l'xml di configurazione (per il momento equivale a riavviare l'applicazione).
           /* if(m.ADCOverflow && this.isAutomaticADCRangeAdjustment()){
                spSensingElement.setRsense(paramInternal.getRsenseLabel());
                spSensingElement.setInGain(paramInternal.getInGainLabel());
            }*/

            ref.log += m.out;

        }else if(param.getSpOffChipTGS8100Figaro()!=null){

            output = new double[cluster.getActiveSPChipList().size()][SPParamItemTGS8100Measures.NUM_OF_OUTPUT];
            SPMeasurementOutput messageInOut = new SPMeasurementOutput();

            if(param.getSpOffChipTGS8100Figaro().getTGS8100FigaroMode().
                    equals(SPOffChipTGS8100Figaro.operationModes[SPOffChipTGS8100Figaro.EIS_MODE])){

                setEIS(param.getSpOffChipTGS8100Figaro().getParamEIS(),messageInOut);
                ref.log += messageInOut.out;
                messageInOut = new SPMeasurementOutput();

                setADC(param.getSpOffChipTGS8100Figaro().getParamEIS(), messageInOut);
                ref.log += messageInOut.out;

                spConfiguration.getSPProtocol().setADCdelay(new Long(Math.round(1000*4*1/
                        param.getSpOffChipTGS8100Figaro().getParamEIS().getConversion_Rate())));
                ref.log += messageInOut.out;

                double buffer_rh[][] = getEIS(param.getSpOffChipTGS8100Figaro().getParamEIS(), m);
                ref.log = messageInOut.out;

            }else if(param.getSpOffChipTGS8100Figaro().getTGS8100FigaroMode().
                    equals(SPOffChipTGS8100Figaro.operationModes[SPOffChipTGS8100Figaro.TCO_MODE])){
                double buffer_rh[][] = null;
                boolean overflow = true;
                boolean minimumAmplification = false;
                /*while(overflow && !minimumAmplification) {
                    setEIS(param.getSpOffChipTGS8100Figaro().getParamTCO_RH(), messageInOut);
                    ref.log += messageInOut.out;
                    messageInOut = new SPMeasurementOutput();

                    setADC(param.getSpOffChipTGS8100Figaro().getParamTCO_RH(), messageInOut);
                    ref.log += messageInOut.out;

                    spConfiguration.getSPProtocol().setADCdelay(new Long(Math.round(1000 * 4 * 1 /
                            param.getSpOffChipTGS8100Figaro().getParamTCO_RH().getConversion_Rate())));
                    ref.log += messageInOut.out;

                    buffer_rh = getEIS(param.getSpOffChipTGS8100Figaro().getParamTCO_RH(), m);

                    overflow=false;
                    for(int i=0; i<buffer_rh.length; i++){
                        if (Math.abs(buffer_rh[i][SPParamItemMeasures.IN_PHASE])>25000.0)
                            overflow=true;
                    }
                    if(overflow) {
                        if (!param.getSpOffChipTGS8100Figaro().getParamTCO_RH().getInGainLabel().equals(
                                SPParamItemInGain.ingainLabels[SPParamItemInGain.INGAIN_1_INDEX])) {
                            String currentIngain = param.getSpOffChipTGS8100Figaro().getParamTCO_RH().getInGainLabel();
                            int cont = 0;

                            while (cont <= SPParamItemInGain.ingainLabels.length &&
                                    !currentIngain.equals(SPParamItemInGain.ingainLabels[cont])) {
                                cont++;
                            }
                            cont = cont - 1;
                            param.getSpOffChipTGS8100Figaro().getParamTCO_RH().setInGain(SPParamItemInGain.ingainLabels[cont]);

                        } else {
                            minimumAmplification = true;
                        }
                    }
                }*/
                param.getSpOffChipTGS8100Figaro().getParamTCO_RH().getSPMeasurementMetaParameter().setRenewBuffer(false);
                param.getSpOffChipTGS8100Figaro().getSpMeasurement_TCO_RH().setEIS(param.getSpOffChipTGS8100Figaro().getParamTCO_RH(), messageInOut);
                ref.log += messageInOut.out;
                messageInOut = new SPMeasurementOutput();

                param.getSpOffChipTGS8100Figaro().getSpMeasurement_TCO_RH().setADC(param.getSpOffChipTGS8100Figaro().getParamTCO_RH(), messageInOut);
                ref.log += messageInOut.out;

                spConfiguration.getSPProtocol().setADCdelay(new Long(Math.round(1000 * 4 * 1 /
                        param.getSpOffChipTGS8100Figaro().getParamTCO_RH().getConversion_Rate())));
                ref.log += messageInOut.out;

                buffer_rh = param.getSpOffChipTGS8100Figaro().getSpMeasurement_TCO_RH().getEIS(param.getSpOffChipTGS8100Figaro().getParamTCO_RH(), m);

                ref.log = messageInOut.out;

                for(int i=0; i<buffer_rh.length; i++){
                    /**CALIBRATED FORMULA*/

                    double dasVoltageApplied = (spConfiguration.getVdd()/2+spConfiguration.getVREF()*param.getSpOffChipTGS8100Figaro().getParamTCO_RH().getDCBiasP()/4096.0);

                    /**---------------------*/

                    if(dasVoltageApplied<700.0){
                        output[i][SPParamItemTGS8100Measures.RESISTANCE_HEATER] = (-1*((dasVoltageApplied/((buffer_rh[i][SPParamItemMeasures.VOLTAGE])/10.19))))*0.7928-5.2;
                    }else if(dasVoltageApplied>=700.0&& dasVoltageApplied<=1000){
                        output[i][SPParamItemTGS8100Measures.RESISTANCE_HEATER] = (-1*((dasVoltageApplied/((buffer_rh[i][SPParamItemMeasures.VOLTAGE])/10.19))))*0.8526-2.0577;
                    }
                    else{
                        output[i][SPParamItemTGS8100Measures.RESISTANCE_HEATER] = (-1*((dasVoltageApplied/((buffer_rh[i][SPParamItemMeasures.VOLTAGE])/10.19))))*0.8737-2.5968;
                    }
                    //output[i][SPParamItemTGS8100Measures.RESISTANCE_HEATER] = (-1*((dasVoltageApplied/((buffer_rh[i][SPParamItemMeasures.VOLTAGE])/10.19))))*0.7928-5.2;
                    //output[i][SPParamItemTGS8100Measures.RESISTANCE_HEATER] = (-1*((dasVoltageApplied/((buffer_rh[i][SPParamItemMeasures.VOLTAGE])/10.19))));
                    output[i][SPParamItemTGS8100Measures.TEMPERATURE] = 23.9 + (1/0.0030748)*(1/110.0)*(output[i][SPParamItemTGS8100Measures.RESISTANCE_HEATER]-111.5);
                }

                param.getSpOffChipTGS8100Figaro().getParamTCO_FS().getSPMeasurementMetaParameter().setRenewBuffer(false);
                param.getSpOffChipTGS8100Figaro().getSpMeasurement_TCO_FS().setEIS(param.getSpOffChipTGS8100Figaro().getParamTCO_FS(),messageInOut);
                ref.log += messageInOut.out;
                messageInOut = new SPMeasurementOutput();

                ArrayList<String> listInstr = new ArrayList<>();
                listInstr.add("WRITE CHA_SELECT S 0X5C");
                sendSequences(listInstr, ref,"figaro");

                param.getSpOffChipTGS8100Figaro().getSpMeasurement_TCO_FS().setADC(param.getSpOffChipTGS8100Figaro().getParamTCO_FS(), messageInOut);
                ref.log += messageInOut.out;

                spConfiguration.getSPProtocol().setADCdelay(new Long(Math.round(1000*4*1/
                        param.getSpOffChipTGS8100Figaro().getParamTCO_FS().getConversion_Rate())));
                ref.log += messageInOut.out;


                double buffer_fs[][] = param.getSpOffChipTGS8100Figaro().getSpMeasurement_TCO_FS().getEIS(param.getSpOffChipTGS8100Figaro().getParamTCO_FS(), m);
                ref.log = messageInOut.out;
                for(int i=0; i<buffer_fs.length; i++){
                    output[i][SPParamItemTGS8100Measures.RESISTANCE_SENSOR] = 0;
                    output[i][SPParamItemTGS8100Measures.CAPACITANCE] = 0;
                }

                status.updateStatus(output);

                /*
                for(int i=0; i<buffer_fs.length; i++){
                    output[i][SPParamItemTGS8100Measures.RESISTANCE_SENSOR] = buffer_fs[i][SPParamItemMeasures.RESISTANCE];
                    output[i][SPParamItemTGS8100Measures.CAPACITANCE] = buffer_fs[i][SPParamItemMeasures.CAPACITANCE];
                }*/
            }
        }else if (spSensingElement.getMeasureTechnique().equals("DIRECT")){
            if (spSensingElement.getName().equals("ONCHIP_TEMPERATURE") || spSensingElement.getName().equals("ONCHIP_VOLTAGE") ||
                    spSensingElement.getName().equals("ONCHIP_LIGHT") ||  spSensingElement.getName().equals("ONCHIP_DARK")) {

                int numberOfMeasure = param.isBurstMode() ? Integer.parseInt(param.getFilter()) : 1;
                if (status.isBufferFilled()){
                    for(int i = 0; i < numberOfMeasure; i++){
                        output = getSingleSENSOR(param,spSensingElement,m);
                    }
                } else {
                    for(int i = 0; i < Integer.parseInt(param.getFilter()); i++){
                        output = getSingleSENSOR(param,spSensingElement,m);
                        SPLogger.getLogInterface().d(LOG_MESSAGE, "Filling buffer: " + (i + 1), DEBUG_LEVEL);
                    }
                    status.setBufferFilled(true);
                }

                ref.log += m.out;


            }else {
                throw new SPException("In getSENSOR. Sensing element unavailable: " + spSensingElement.getMeasureTechnique());
            }



        }  else {
            throw new SPException("Measure technique unavailable: " + spSensingElement.getMeasureTechnique());
        }
        return output;
    }

    protected void evaluateImpedanceDataReader(SPMeasurementParameter param,  SendInstructionInOut ref,double output[][], SPMeasurementOutput m) throws SPException {

        // output:
        // in each row is stored a chip
        // first column contains inphase
        // second column contains quadrature


        SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

        if (!iqStatus.getSpMeasurementSequential().getActive()){
            super.evaluateImpedanceDataReader(param, ref, output, m);
        } else {

            ref.log += "START evaluateImpedanceDataReader RUN5.\n-----------------------------\n";

            SPMeasurementSequential spMeasurementSequential = iqStatus.getSpMeasurementSequential();


            SPCluster spCluster = spProtocol.getSpCluster();
            //byte[][] output_temp = new byte[spCluster.getActiveSPChipList().size()][];
            byte[] output_temp = null;
            //int output[][] = new int[spCluster.getActiveSPChipList().size()][];


            for (int i = 0; i < spCluster.getActiveSPChipList().size(); i++) {

                // Generate a cluster with one chip
                ArrayList<String> list = new ArrayList<>();
                list.add(spCluster.getActiveSPChipList().get(i).getSerialNumber());
                ref.temporaryCluster = cluster.generateTempCluster(list);

                sendBeforeInstruction(ref, spMeasurementSequential);
                //SPDelay.delay(1000);

                // Turn on sinusoid
                //if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.getBeforeInstruction() != null) {
                    // Start sinusoidal signal (for EIS)
                    //ref.instructionToSend = spMeasurementSequential.getBeforeInstruction();
                    //sendSequence(ref);
                    //SPDelay.delay(spProtocol.getADCDelay() * 10); // Wait for data ready on ADC
                //}

                // Read in phase
                if (((SPMeasurementParameterADC) param).getCommandX20()) {
                    ref.instructionToSend = "WRITE COMMAND S 0x20";    // Trigger per acquisizione
                    sendSequence(ref);
                    SPDelay.delay(spProtocol.getADCDelay()); // Wait for data ready on ADC
                }
                ref.instructionToSend = "READ CHA_FIFOL M " + param.getFIFO_READ() * 2;
                sendSequence(ref);

                // In sequential mode there is only one chip
                output_temp = responseTokenizerByte(ref.recValues[0]);
                output[i][SPParamItemMeasures.IN_PHASE] = Utility.conversionFromByteToIntSingle(output_temp[1], output_temp[0]);


                if (((SPMeasurementParameterADC) param).getCommandX20()) {
                    ref.instructionToSend = "WRITE COMMAND S 0x22";    // Trigger per acquisizione

                    sendSequence(ref);
                    SPDelay.delay(spProtocol.getADCDelay()); // Wait for data ready on ADC
                }
                ref.instructionToSend = "READ CHA_FIFOL M " + param.getFIFO_READ() * 2;
                sendSequence(ref);

                // In sequential mode there is only one chip
                output_temp = responseTokenizerByte(ref.recValues[0]);
                output[i][SPParamItemMeasures.QUADRATURE] = Utility.conversionFromByteToIntSingle(output_temp[1], output_temp[0]);

                sendAfterInstruction(ref, spMeasurementSequential);

                /*
                if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.getAfterInstruction() != null) {
                    // Stop sinusoidal signal (for EIS)
                    ref.instructionToSend = spMeasurementSequential.getAfterInstruction();
                    sendSequence(ref);
                }*/

                //SPDelay.delay(300);
            }

            if (status != null) {
                //status.updateLog(output);
            }

            ref.log += "END evaluateImpedanceDataReader.\n-----------------------------\n";

            if (m != null) {
                m.out = ref.log;
            }
        }

    }




    /**
     * @param param
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)

    @Override
    public synchronized  void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m)
            throws SPException {

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setADC", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {

            Integer  FNP, OSR_DIVIDER, CICW, SAM_DIVIDER_int, SAM_DIVIDER_fraz, SAM_DIVIDER, Conversion_Rate_int, CCO, FRD = 0;
            Double Conversion_Rate_fraz, Conversion_Rate;

            String instruction, instructionNum;
            ArrayList<String> instructionList = new ArrayList<>();

            Integer cutOffMin = 50;
            Integer cutOffMax = 100;

            // True if the conversion rate should be evaluated internally
            if (param.getConversion_Rate() == null){
                if (lastParameter == null) {
                    throw new SPException("Any setXXX called before setADC");
                }
                if (lastParameter instanceof SPMeasurementParameterEIS){
                    // Evaluate the conversion rate
                    Conversion_Rate = AvailableFrequencies.evaluateADCConversionRate(((SPMeasurementParameterEIS) lastParameter).getFrequency(), cutOffMin, cutOffMax);
                    FRD = ((SPMeasurementStatusIQ) status).getFRD();
                    ((SPMeasurementStatusIQ)status).setConversionRate(Conversion_Rate);
                    ((SPMeasurementStatusIQ)status).setConversionRate(25); // TODO: Verify with Simone Del Cesta
                } else {
                    //Throws and exception: any other alternatives are defined
                    throw new SPException("Any suitable setXXX called before setADC");
                }

            } else {
                Conversion_Rate = param.getConversion_Rate();
            }

            if (Conversion_Rate.intValue() <= 1000) {
                FNP = 0; //-- enabled ADC precision mode
            } else {
                FNP = 1; //-- enabled ADC fast mode
            }


            if (Conversion_Rate <= clock/2016.0){
                SAM_DIVIDER = 63;
                double f_osr = 2 * Conversion_Rate * 2016;
                if (FRD == 1){
                    f_osr = f_osr * 2 / 3;
                }
                OSR_DIVIDER = (int) Math.round(clock / f_osr);
            } else {

                OSR_DIVIDER = 2;
                SAM_DIVIDER_int = (int) Math.round(clock/ ((64 * OSR_DIVIDER.intValue() * Conversion_Rate))); // 8 bit integer
                Conversion_Rate_int = (int) (clock / (64 * OSR_DIVIDER.intValue() * SAM_DIVIDER_int.intValue()));

                SAM_DIVIDER_fraz = (int)  Math.round((clock / (64 * OSR_DIVIDER.intValue() * Conversion_Rate)) * 2 / 3);
                Conversion_Rate_fraz = (double) ((clock / (64 * OSR_DIVIDER.intValue() * SAM_DIVIDER_fraz.intValue())) * 2 / 3);
                if (Math.abs(Conversion_Rate_int - Conversion_Rate) <= Math.abs(Conversion_Rate_fraz - Conversion_Rate)) {
                    SAM_DIVIDER = SAM_DIVIDER_int;
                    FRD = 0;
                } else {
                    SAM_DIVIDER = SAM_DIVIDER_fraz;
                    FRD = 1;
                }
            }


            if (param.getNData().intValue() == 1)
                CCO = 0;
            else
                CCO = 1;


            if (CCO == 0) {
                if (SAM_DIVIDER <= 2) {
                    CICW = 0;
                } else if (SAM_DIVIDER <= 4) {
                    CICW = 3;
                } else if (SAM_DIVIDER <= 8) {
                    CICW = 6;
                } else if (SAM_DIVIDER <= 16) {
                    CICW = 9;
                } else if (SAM_DIVIDER <= 32) {
                    CICW = 12;
                } else { //if (SAM_DIVIDER <= 63){
                    CICW = 15;
                }
            } else {
                if (SAM_DIVIDER > 16) {
                    SAM_DIVIDER = 16;
                    FRD = 1;
                }

                if (SAM_DIVIDER <= 1) {
                    CICW = 3;
                } else if (SAM_DIVIDER <= 2) {
                    CICW = 6;
                } else if (SAM_DIVIDER <= 4) {
                    CICW = 9;
                } else if (SAM_DIVIDER <= 8) {
                    CICW = 12;
                } else { //if  (SAM_DIVIDER <= 16){
                    CICW = 15;
                }

            }

            instruction = "WRITE COMMAND S 0x04";
            instructionList.add(instruction);


            instruction = "WRITE COMMAND S 0x06";
            instructionList.add(instruction);


            instruction = "WRITE CHA_DIVIDER M 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(SAM_DIVIDER.intValue(), 6);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSR_DIVIDER.intValue(), 9);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(FRD, 1);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_SELECT+1 S 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "00" + param.getInPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONFIG S 0x";
            instructionNum = "" + CCO + "" + FNP + "1";
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(param.getNData().intValue(), 5);


            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            sendSequences(instructionList, ref, "setADC");

            if (m != null) {
                m.out = ref.log;
            }
            //lastParameter = param;

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setADC", DEBUG_LEVEL);

    }*/

    /**
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
    */
    /*@Override
    public synchronized  void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m)
            throws SPException {

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setADC", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {


            String instruction, instructionNum;
            ArrayList<String> instructionList = new ArrayList<>();

            // -- test WITH and WITHOUT the following instructions, if no difference then REMOVE
            instruction = "WRITE COMMAND S 0x04";
            instructionList.add(instruction);
            instruction = "WRITE COMMAND S 0x06";
            instructionList.add(instruction);




            Integer FNP, OSR_DIVIDER, CICW, SAM_DIVIDER_int, SAM_DIVIDER_fraz, SAM_DIVIDER, Conversion_Rate_int, CCO, FRD = 0;
            //Double Conversion_Rate_fraz;
            Double Conversion_Rate, OSR;


            Conversion_Rate = (12.5);
            //Conversion_Rate = (50.0);
            OSR = (double)512;

            if (Conversion_Rate <= 1000) {
                FNP = 0; //-- enabled ADC precision mode
            } else {
                FNP = 1; //-- enabled ADC fast mode
            }


            if (Conversion_Rate <= clock/511.0){
                SAM_DIVIDER = 32;
                double f_osr = Conversion_Rate * OSR;
                if (FRD == 1){
                    OSR_DIVIDER = (int)Math.round((clock*2/3)/f_osr);
                } else {
                    OSR_DIVIDER = (int)Math.round(clock/f_osr);
                }
                //  OSR_DIVIDER = (int)Math.round(clock / f_osr);
            } else {

                throw new SPException("Not yet implemented. setADC() Conversion_Rate > clock/511.0 = " + clock/511);
            }


            if (param.getFIFO_DEEP() == 1)
                CCO = 0;
            else
                CCO = 1;

            CICW = (int)(15 - 3 * (Math.log(1008.0 / OSR) / Math.log(2.0)));




            instruction = "WRITE CHA_DIVIDER M 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(SAM_DIVIDER.intValue(), 6);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSR_DIVIDER.intValue(), 9);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(FRD, 1);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_SELECT+1 S 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "00" + param.getInPortADC();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONFIG S 0x";
            instructionNum = "" + CCO + "" + FNP + "1";
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(param.getFIFO_DEEP(), 5);


            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            sendSequences(instructionList, ref, "setADC");

            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setADC", DEBUG_LEVEL);

    }*/

    /*
    NEW SETADC*/

    @Override
    public synchronized  void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m)
            throws SPException {

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setADC", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }


        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {


            String instruction, instructionNum;
            ArrayList<String> instructionList = new ArrayList<>();

            // -- test WITH and WITHOUT the following instructions, if no difference then REMOVE
            instruction = "WRITE COMMAND S 0x04";
            instructionList.add(instruction);
            instruction = "WRITE COMMAND S 0x06";
            instructionList.add(instruction);




            Integer FNP, OSR_DIVIDER, CICW, SAM_DIVIDER_int, SAM_DIVIDER_fraz, SAM_DIVIDER, Conversion_Rate_int, CCO, FRD = 0;
            //Double Conversion_Rate_fraz;
            Double Conversion_Rate, OSR;
            Integer PRE = 0;

            Conversion_Rate = param.getConversion_Rate();
            /*if(Conversion_Rate==null){
                //Conversion_Rate = 50.0;
                Conversion_Rate = 17.0;
            }*/
            //Conversion_Rate = (12.5);
            //Conversion_Rate = (50.0);
            OSR = (double)512;

            if (Conversion_Rate <= 1000) {
                FNP = 0; //-- enabled ADC precision mode
            } else {
                FNP = 1; //-- enabled ADC fast mode
            }


             if (Conversion_Rate <= clock/511.0){
                SAM_DIVIDER = 32;
                double f_osr = Conversion_Rate * OSR;
                if(f_osr<= clock/511.0){
                    PRE=1;
                    if (FRD == 1){
                        OSR_DIVIDER = (int)Math.round(((clock/512.0)*2/3)/f_osr);
                    } else {
                        OSR_DIVIDER = (int)Math.round((clock/512.0)/f_osr);
                    }
                }else{
                    PRE=0;
                    if (FRD == 1){
                        OSR_DIVIDER = (int)Math.round((clock*2/3)/f_osr);
                    } else {
                        OSR_DIVIDER = (int)Math.round(clock/f_osr);
                    }
                }


            } else {
                OSR_DIVIDER = 1;
                SAM_DIVIDER = 32;
                //throw new SPException("Not yet implemented. setADC() Conversion_Rate > clock/511.0 = " + clock/511);
            }


            if (param.getFIFO_DEEP() == 1)
                CCO = 0;
            else
                CCO = 1;


            CICW = (int)(15 - 3 * (Math.log(1008.0 / OSR)/ Math.log(2.0)));

            instruction = "WRITE DIG_CONFIG S 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(PRE,1);
            instructionNum +=  SPMeasurementUtil.fromIntegerToBinWithFill(0,7);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE CHA_DIVIDER M 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(SAM_DIVIDER.intValue(), 6);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSR_DIVIDER.intValue(), 9);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(FRD, 1);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE CHA_SELECT+1 S 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "00" + param.getInPortADC();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);



            instruction = "WRITE CHA_CONFIG S 0x";
            instructionNum = "" + CCO + "" + FNP + "1";

            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(param.getFIFO_DEEP(), 5);
            //instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(4, 5);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            sendSequences(instructionList, ref, "setADC");

            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setADC", DEBUG_LEVEL);

    }


    @Override
    protected void getQ(SendInstructionInOut ref, double output[][],SPMeasurementParameter param, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m) throws SPException {
        /// Leggo QUADRATURA
        long start = System.currentTimeMillis();

        SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;
        ref.instructionToSend = iqStatus.getInstructionQ_Prepared();

        GET_Q_ELAPSED += (System.currentTimeMillis() - start);

        if(this.isSystemLevelChopperActive()) {
            ref.instructionToSend = iqStatus.getInstructionToggleCHAFILTERPCI();
            sendSequence(ref);
        }

        //sendSequence(ref);

        start = System.currentTimeMillis();

        GET_Q_ELAPSED += (System.currentTimeMillis() - start);

        int[][] dataRead = getDATA(param, true, false, spMeasurementSequential, m);

        if(this.isSystemLevelChopperActive()) {
            if (iqStatus.getSystemLevelChopper().isPCIBitHigh()) {
                for (int i = 0; i < dataRead.length; i++) {
                    for (int j = 0; j < dataRead[i].length; j++) {
                        dataRead[i][j] = dataRead[i][j] * -1;
                    }
                }
            }
        }

        start = System.currentTimeMillis();

        ref.log += m.out;

        for(int i = 0; i < cluster.getActiveSPChipList().size(); i++) {
            output[i][SPMeasurementParameterQI.QUADRATURE] = dataRead[i][0];
        }
        GET_Q_ELAPSED += (System.currentTimeMillis() - start);

    }

    @Override
    protected void getI(SendInstructionInOut ref, double output[][],SPMeasurementParameter param, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m) throws SPException {

        long start = System.currentTimeMillis();

        SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

        /// Leggo FASE
        //ref.instructionToSend = iqStatus.getInstructionI_Prepared();
        GET_I_ELAPSED += (System.currentTimeMillis() - start);


        if(this.isSystemLevelChopperActive()) {
            ref.instructionToSend = iqStatus.getInstructionToggleCHAFILTERPCI();
            sendSequence(ref);
        }



        //sendSequence(ref);

        start = System.currentTimeMillis();



        GET_I_ELAPSED += (System.currentTimeMillis() - start);

        int[][] dataRead = getDATA(param, true, true, spMeasurementSequential, m);

        if(this.isSystemLevelChopperActive()) {
            if (iqStatus.getSystemLevelChopper().isPCIBitHigh()) {
                for (int i = 0; i < dataRead.length; i++) {
                    for (int j = 0; j < dataRead[i].length; j++) {
                        dataRead[i][j] = dataRead[i][j] * -1;
                    }
                }
            }
        }

        start = System.currentTimeMillis();

        ref.log += m.out;

        for(int i = 0; i < cluster.getActiveSPChipList().size(); i++) {
            output[i][SPMeasurementParameterQI.IN_PHASE] = dataRead[i][0];
        }
        GET_I_ELAPSED += (System.currentTimeMillis() - start);

    }

    /**
     * @param param
     * @param m     log messages
     * @return
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    @Override
    protected byte[][] getDATA_BYTE(SPMeasurementParameterADC param, Boolean inPhase, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m) throws SPException {

        // inPhase: is not used in RUN4

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getDATA", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }



        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        byte[][] output;

        try {

            ref.log = "-----------------------------\ngetData:\n";

            if (spMeasurementSequential != null && spMeasurementSequential.getActive()){
                // Unicast send
                // Unicast receive
                SPCluster spCluster = spProtocol.getSpCluster();
                output = new byte[spCluster.getActiveSPChipList().size()][];
                for(int i = 0; i < spCluster.getActiveSPChipList().size(); i++){
                    //System.out.println("getDATA_BYTE for: " + spCluster.getActiveSPChipList().get(i).getSerialNumber());
                    ArrayList<String> list = new ArrayList<>();
                    list.add(spCluster.getActiveSPChipList().get(i).getSerialNumber());
                    ref.temporaryCluster = cluster.generateTempCluster(list);
                    byte[][] output_temp = get(ref, param, inPhase, spMeasurementSequential, m);
                    //output[i] = new byte[output[0].length];
                    //System.arraycopy(output_temp[0], 0, output[i], 0, output_temp[i].length);
                    output[i] = output_temp[0];
                }

            } else {
                // Multicast send
                // Unicast polling
                output = get(ref, param, inPhase, spMeasurementSequential, m);
            }


        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End getDATA", DEBUG_LEVEL);

        return output;
    }

    protected void sendBeforeInstruction(SendInstructionInOut ref, SPMeasurementSequential spMeasurementSequential) throws SPException {
        if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.sizeBeforeInstruction() > 0){
            // Start sinusoidal signal (for EIS)
            /*for(int i = 0; i < spMeasurementSequential.sizeBeforeInstruction(); i++){
                ref.instructionToSend = spMeasurementSequential.getBeforeInstruction(i);
                sendSequence(ref);
            }*/
            for(int i =  spMeasurementSequential.sizeBeforeInstruction()-1; i >= 0; i--){
                ref.instructionToSend = spMeasurementSequential.getBeforeInstruction(i);
                //SPDelay.delay(500);
                sendSequence(ref);
                //SPDelay.delay(500);
            }
            //SPDelay.delay(100);

            //SPDelay.delay(spProtocol.getADCDelay() * 10); // Wait for data ready on ADC
        }
    }


    protected void sendAfterInstruction(SendInstructionInOut ref, SPMeasurementSequential spMeasurementSequential) throws SPException {
        if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.sizeAfterInstruction() > 0){
            // Start sinusoidal signal (for EIS)
            for(int i = 0; i < spMeasurementSequential.sizeAfterInstruction(); i++){
                ref.instructionToSend = spMeasurementSequential.getAfterInstruction(i);
                sendSequence(ref);
            }
            //SPDelay.delay(spProtocol.getADCDelay() * 10); // Wait for data ready on ADC
        }

    }

    protected byte[][] get(SendInstructionInOut ref, SPMeasurementParameterADC param, boolean inPhase,
                           SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m) throws SPException {


        sendBeforeInstruction(ref, spMeasurementSequential);


        if (param.getCommandX20()){
            ref.instructionToSend = "WRITE COMMAND S 0x2";    // Trigger per acquisizione

            if (inPhase){
                ref.instructionToSend += "0";
            } else {
                ref.instructionToSend += "2";
            }

            sendSequence(ref);
            SPDelay.delay(spProtocol.getADCDelay()); // Wait for data ready on ADC
        }

        //ref.instructionToSend = "READ CHA_FIFOL M " + param.getNData() * 2;
        //ref.instructionToSend = "READ CHA_CONFIG M " + param.getNData() * 2;
        //sendSequence(ref);

        ref.instructionToSend = "READ CHA_FIFOL M " + param.getFIFO_READ() * 2;
        sendSequence(ref);


        SPLogger.getLogInterface().d(LOG_MESSAGE, "ref.recValues.length" + ref.recValues.length, DEBUG_LEVEL);

        byte[][] output =  new byte[ref.recValues.length][];
        for(int k = 0; k < ref.recValues.length; k++) {
            output[k] = Utility.responseTokenizerByte(ref.recValues[k]);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "output[k]: " + output[k][0] + " " + output[k][1], DEBUG_LEVEL);
        }

        sendAfterInstruction(ref, spMeasurementSequential);
        /*
        if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.getAfterInstruction()  != null){
            // Stop sinusoidal signal (for EIS)
            ref.instructionToSend = spMeasurementSequential.getAfterInstruction();
            sendSequence(ref);
        }*/

        if (status != null){
            status.updateLog(output);
        }

        ref.log += "END getData.\n-----------------------------\n";

        if (m != null) {
            m.out = ref.log;
        }

        return output;
    }


    /**
     * @param param
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     */
    @Override
    public synchronized  void setPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m)
            throws SPException {

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setPOT", DEBUG_LEVEL);
        ArrayList<String> instructionList = new ArrayList<>();

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {


            Double conversionRate = (double)50;
            String instruction, instructionNum;

            String CES = "0";
            if (param.portIsInternal()) {
                CES = "1";    // TODO: perfezionare confronto per individuare se porta interna o esterna
            }

            instruction = "WRITE CHA_SELECT S 0x";
            instructionNum = "01" + CES + param.getPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONDIT M 0x";
            param.setInGain("1");
            instructionNum = "000000" + param.getInGain() + param.getRsense() + "011111";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            String FSC_DAS = param.getContacts();

            instruction = "WRITE DSS_SELECT M 0x";
            instructionNum = "0000011100" + FSC_DAS + param.getPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            /*instruction = "WRITE DSS_DIVIDER M 0x0014";
            instructionList.add(instruction);*/
            String FRD="0";
            instructionNum = "000000000000001"+FRD;
            instruction="WRITE DSS_DIVIDER M 0x";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE CHA_FILTER+1 S 0xB2";
            instructionList.add(instruction);

            sendSequences(instructionList, ref, "setPOT");

            if (m != null) {
                m.out = ref.log;
            }

            SPMeasurementOutput mADC = new SPMeasurementOutput();
            /*if (true)
                throw new SPException("setADC in autorange should be verified for RUN5 and RUN6 in terms of FIFO_READ and FIFO_DEEP dimension");*/

            setADC(1, 1, param.getInGainLabel(), conversionRate, SPMeasurementParameterADC.INPORT_IA, true, mADC);
            //instructionList.add(instruction);

            if (m != null) {
                m.out += mADC.out;
            }


            instructionList = new ArrayList<>();
            instruction = "WRITE ANA_CONFIG S 0x3F";
            instructionList.add(instruction);

            //if (param.getSPMeasurementMetaParameter().getRestartBandGap()){
            //    restartBandGap();
            //}


            if (param.getTypeVoltammetry().equals(SPParamItemPOTType.POTENTIOMETRIC)){
                instruction = "WRITE CHA_SELECT S 0x";
                instructionNum = "11" + CES + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);
            }

            sendSequences(instructionList, ref, "setPOT");

            status = new SPMeasurementStatusPOT(this, spConfiguration, param, SPParamItemPOTMeasures.NUM_OUTPUT,spProtocol, "" + logDirectory + "/Download/getPOT_dump.txt");


            if (m != null) {
                m.out += ref.log;
            }
            lastParameter = param;

        } catch (SPException  e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setPOT", DEBUG_LEVEL);

    }


    public synchronized void setACTUATOR(SPMeasurementParameterActuator param, SPCluster temporaryCluster) throws SPException {

    }


    public synchronized void setACTUATOR(SPMeasurementParameterActuator param) throws SPException{
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setACTUATOR", DEBUG_LEVEL);
        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        ArrayList<String> instructionList = new ArrayList<>();
        String instruction = null;

        if (param.getPort().equals(SPMeasurementParameterActuator.ACTUATOR_PORT_INT)){
            if (param.getMode() == SPMeasurementParameterActuator.ON){
                instruction = "WRITE INT_CONFIG S 0x01";
                instructionList.add(instruction);

                instruction = "WRITE DIG_CONFIG S 0x04";
                instructionList.add(instruction);
            } else {
                instruction = "WRITE DIG_CONFIG S 0x00";
                instructionList.add(instruction);
            }
            /*if (param.getMode() == SPMeasurementParameterActuator.ON){
                instruction = "WRITE INT_CONFIG S 0x05";
                instructionList.add(instruction);

                instruction = "WRITE DIG_CONFIG S 0x05";
                instructionList.add(instruction);
            } else {
                instruction = "WRITE INT_CONFIG S 0x00";
                instructionList.add(instruction);
            }*/
        } else if (param.getPort().equals(SPMeasurementParameterActuator.ACTUATOR_PORT_RDY)){

            if (param.getMode() == SPMeasurementParameterActuator.ON){
                instruction = "WRITE INT_CONFIG+1 S 0x02";
                instructionList.add(instruction);
            } else {
                instruction = "WRITE INT_CONFIG+1 S 0x00";
                instructionList.add(instruction);
            }
        }

        sendSequences(instructionList, ref, "setACTUATOR");
    }

    /**
     * The setEIS method allows setting all required parameters to perform Electric Impedance Spectroscopy
     * (EIS) measurements.
     *
     * @param param @see SPMeasurementParameterEIS
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    @Override
    public synchronized  void setEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m)
            throws SPException {


        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setEIS", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE, param.toString(), DEBUG_LEVEL);

        int chopCutOff = 10000;

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }


        //System.out.println("Sequential: " + param.isSequentialMode());
        SPMeasurementSequential spMeasurementSequential = new SPMeasurementSequential(param.isSequentialMode());

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {
            ArrayList<String> instructionList = new ArrayList<>();
            lastParameter = param;
            float OSM;
            int FRD, DSS_DIVIDER;

            AvailableFrequencies frequencyCalculus = new AvailableFrequencies();

            String instruction, instructionNum, instructionForSequential, instructionNumForSequential;

            instruction = "WRITE CHA_SELECT S 0x";
            instructionNum = param.getModeVI().substring(1, 2) + param.getContacts() + "0" + param.getInPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE DSS_SELECT M 0x";
            if (param.getModeVI().substring(0, 1).equals("1")) {
                if (param.getContactsLabel().equals(SPParamItemContacts.ContactsLabels[0])) {
                    //TWO CONTACTS
                    instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "0" + param.getModeVI().substring(1, 2) + "1" + param.getOutPort();
                } else if (param.getContactsLabel().equals(SPParamItemContacts.ContactsLabels[1])) {
                    //FOUR CONTACTS
                    instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + "1" + param.getOutPort();
                }
                //instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + "1" + param.getOutPort();
            } else {
                if (param.getContactsLabel().equals(SPParamItemContacts.ContactsLabels[0])) {
                    //two contacts
                    instructionNum = "0" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "0" + param.getModeVI().substring(1, 2) + param.getContacts() + param.getOutPort();
                } else if (param.getContactsLabel().equals(SPParamItemContacts.ContactsLabels[1])) {
                    //four contacts
                    instructionNum = "0" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + param.getContacts() + param.getOutPort();
                }
            }
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);
            spMeasurementSequential.addBeforeInstruction(instruction);


            instruction = "WRITE DSS_DIVIDER M 0x";
            AvailableFrequencies.availableFrequencies(spConfiguration, param.getFrequency(), frequencyCalculus);
            OSM = frequencyCalculus.OSM;
            DSS_DIVIDER = frequencyCalculus.DSS_DIVIDER;
            FRD = frequencyCalculus.FRD;

            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(DSS_DIVIDER, 15) + Integer.toString(FRD);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);




            /*instruction = "WRITE DDS_CONFIG+1 S 0x80";
            instructionList.add(instruction);*/


            int OSM_INT = (int) OSM;
            int numBit = 12;
            int modSignDCBias = bitSignDCBias(param.getDCBiasP(), numBit);

            if (param.isSequentialMode()){

            }



            instruction                 = "WRITE DAS_CONFIG M 0x";
            instructionForSequential    = "WRITE DAS_CONFIG M 0x";

            instructionNum              = "0";
            instructionNumForSequential = "0";

            instructionNum              += SPMeasurementUtil.fromIntegerToBinWithFill(OSM_INT, 3);
            instructionNumForSequential += "000";

            instructionNum              += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBias, numBit);
            instructionNumForSequential += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBias, numBit);

            instruction                 = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionForSequential    = SPMeasurementUtil.fromBitToHexWithConcat(instructionForSequential, instructionNumForSequential);

            //spMeasurementSequential.addBeforeInstruction(instruction);                  // Start the sinosoid
            //spMeasurementSequential.addAfterInstruction(instructionForSequential);      // Stop the sinusoid


/*
            if (param.isSequentialMode()) {
                // Stop the sinusoid
                sendAfterInstruction(ref, spMeasurementSequential);
                //instructionList.add(spMeasurementSequential.getAfterInstruction());
            } else {
                sendBeforeInstruction(ref, spMeasurementSequential);
                //instructionList.add(spMeasurementSequential.getBeforeInstruction());
            }
            */

            /*if (param.isSequentialMode()) {
                // Stop the sinusoid
                instructionList.add(instructionForSequential);
                //sendAfterInstruction(ref, spMeasurementSequential);
                //instructionList.add(spMeasurementSequential.getAfterInstruction());
            } else {
                //sendBeforeInstruction(ref, spMeasurementSequential);
                instructionList.add(instruction);
                //instructionList.add(spMeasurementSequential.getBeforeInstruction());
            }*/

            /*----------------------------------------*/
            instructionList.add(instruction);
            /*----------------------------------------*/

            instruction = "WRITE CHA_FILTER M 0x";
            String chopperValue = "";
            if (frequencyCalculus.availableFrequency <= chopCutOff) {
                //chopperValue = "90"; //only ADc choppers enabled (IA chopper causes error EIS in measurements)
                chopperValue = "12"; //Marco e Mattia - 17Hz
            } else {
                chopperValue = "12"; //only ADc choppers enabled (IA chopper causes error EIS in measurements)
            }

            if(frequencyCalculus.availableFrequency == 0){
                chopperValue = "B2";
            }

            String HE3 = "0";
            if (param.getHarmonic().equals(SPParamItemHarmonic.harmonicLabels[2])) {
                HE3 = "1";
            }
            instructionNum = "000" + param.getQI() + HE3;

            if (param.getFrequency() == 0){
                instructionNum += "00";
            } else {
                instructionNum +=  param.getHarmonic();
            }
            if (instructionNum.length() != 8)
                throw new SPException("lunghezza dati numerici sbagliata in instruction: " + instruction); //  numeric substring after 'OX' have to have lenght 8 bit

            String hexValueToFillWithZero = SPMeasurementUtil.zeroFill(Integer.toHexString(Integer.parseInt(instructionNum, 2)), 2);
            hexValueToFillWithZero = chopperValue + hexValueToFillWithZero;
            instruction += SPMeasurementUtil.zeroFill(hexValueToFillWithZero, 4);
            instructionList.add(instruction);

            instruction = "WRITE CHA_CONDIT M 0x";

            //String VSCM_BIAS = "011111";
            if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.QUADRANT){
                instructionNum = "000000";
            } else {
                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getPhaseShift(), 5);
                if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.COARSE){
                    instructionNum += "1";
                } else if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.FINE){
                    instructionNum += "0";
                }
            }

            instructionNum += param.getInGain() + param.getRsense();
            int numBitDCBiasN = 6;
            int modSignDCBiasN = bitSignDCBias(param.getDCBiasN(), numBitDCBiasN);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBiasN, numBitDCBiasN);

            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);
            instructionList.add(instruction);


            /*instruction                 = "WRITE ANA_CONFIG S 0x3F";
            instructionList.add(instruction);*/


            instruction                 = "WRITE ANA_CONFIG S 0x3F";
            //instructionForSequential    = "WRITE ANA_CONFIG S 0x00";
            instructionForSequential    = "WRITE ANA_CONFIG S 0x1B";
            //instructionForSequential    = "WRITE DSS_SELECT S 0x00";
            //spMeasurementSequential.addBeforeInstruction(instruction);                  // Start ANALOGICAL BLOCK
            //spMeasurementSequential.addAfterInstruction(instructionForSequential);      // Stop ANALOGICAL BLOCK


            spMeasurementSequential.addAfterInstruction("WRITE DSS_SELECT S 0X14");      // Stop ANALOGICAL BLOCK

            //instructionList.add(instruction);
            /*if (param.isSequentialMode()) {
                // Stop the sinusoid
                instructionList.add(instructionForSequential);
                //sendAfterInstruction(ref, spMeasurementSequential);
                //instructionList.add(spMeasurementSequential.getAfterInstruction());
            } else {
                //sendBeforeInstruction(ref, spMeasurementSequential);
                instructionList.add(instruction);
                //instructionList.add(spMeasurementSequential.getBeforeInstruction());
            }*/


            instructionList.add(instruction);

            //instruction = "WRITE DAS_CONFIG M 0X0FFF";
            //instructionList.add(instruction);

            sendSequences(instructionList, ref, "setEIS");

            // "" + logDirectory + "/Download/" + measureLogFile

            String nomeFileDump = "" + logDirectory + "/Download/" + "getEIS_dump.txt";
            if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null){
                status = new SPMeasurementStatusIQ(this, param, spProtocol, SPMeasurementParameterEIS.NUM_OUTPUT,
                        frequencyCalculus, FRD, spMeasurementSequential, nomeFileDump);
            }
            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setEIS", DEBUG_LEVEL);

    }

    protected static int bitSignDCBias(int x, int numBit) {
        int toAdd = (int) Math.pow(2, numBit - 1);
        return x + toAdd;
    }




}
