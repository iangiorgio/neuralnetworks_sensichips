package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.level1.chip.SPCluster;

public class SendInstructionInOut {
    public String instructionToSend;
    public String log;
    public String exceptionMessage;
    public String[] recValues;
    public SPCluster temporaryCluster;

    public SendInstructionInOut(SPCluster temporaryCluster){
        exceptionMessage = "";
        instructionToSend = "";
        log = "";
        recValues = null;
        this.temporaryCluster = temporaryCluster;
    }

    public String recValuesToString(){
        String output = "";
        if (recValues != null) {
            for (int i = 0; i < recValues.length; i++) {
                output += recValues[i] + "\n";
            }
        }
        return output;
    }
}
