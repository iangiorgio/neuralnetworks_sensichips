package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterADC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterQI;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemInGain;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemMeasures;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemOutGain;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemRSense;
import com.sensichips.sensiplus.level2.util.SPMeasurementUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import static com.sensichips.sensiplus.config.SPConfigurationManager.setDefaultSPConfiguration;

/**
 * Created by Marco Ferdinandi on 21/03/2018.
 */
public class SPAutoRangeAlgorithmV2{
    private int noiseENOB = 13;
    private double Vmax1 = 0.835;
    private double Vmax2 = 1.720;
    private double multiplier[] ={1.14,1.17,1.2,1.25,1.33,1.5,2.0,12.0,1.67,2.0,10.0,10.0,10.0,-1};


    private int VREF;

    public double getVminNoise() {
        return VminNoise;
    }

    private double VminNoise;
    private SPMeasurement measurement;
    private List<String> keysList = new ArrayList<>();

    public Hashtable<String, AutoRangeParameters> getAutoRangeParametersTable() {
        return autoRangeParametersTable;
    }

    private Hashtable<String, AutoRangeParameters> autoRangeParametersTable;

    private double[] Ifrequencies = {19531.25,9765.63,4882.81,2441.41};
    private double[] Qfrequencies = {78125.00,39062.50,19531.25,4882.81,1220.70};
    private boolean selectFrequency;

    public SPAutoRangeAlgorithmV2(SPMeasurement measurement, boolean selectFrequency,int VREF){
        this.measurement = measurement;
        this.VREF = VREF;
        this.selectFrequency = selectFrequency;

        this.VminNoise = this.VREF/Math.pow(2,this.noiseENOB);

        autoRangeParametersTable = new Hashtable<>();
        for(int i = 0; i< SPParamItemOutGain.outgainLabels.length; i++) {
            keysList.add(SPParamItemOutGain.outgainLabels[i] + "_" + SPParamItemInGain.ingainLabels[0] +
                    "_" + SPParamItemRSense.rsenseLabels[3]);
            autoRangeParametersTable.put(SPParamItemOutGain.outgainLabels[i] + "_" + SPParamItemInGain.ingainLabels[0] +
                            "_" + SPParamItemRSense.rsenseLabels[3],
                    new AutoRangeParameters(SPParamItemOutGain.outgainLabels[i],
                            SPParamItemInGain.ingainLabels[0], SPParamItemRSense.rsenseLabels[3], multiplier[i], Vmax1));
        }

        autoRangeParametersTable.put(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[1] +
                        "_" + SPParamItemRSense.rsenseLabels[3],
                new AutoRangeParameters(SPParamItemOutGain.outgainLabels[7],
                        SPParamItemInGain.ingainLabels[1], SPParamItemRSense.rsenseLabels[3], multiplier[8], Vmax2));
        keysList.add(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[1] +
                "_" + SPParamItemRSense.rsenseLabels[3]);

        autoRangeParametersTable.put(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[2] +
                        "_" + SPParamItemRSense.rsenseLabels[3],
                new AutoRangeParameters(SPParamItemOutGain.outgainLabels[7],
                        SPParamItemInGain.ingainLabels[2], SPParamItemRSense.rsenseLabels[3], multiplier[9], Vmax2));
        keysList.add(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[2] +
                "_" + SPParamItemRSense.rsenseLabels[3]);

        autoRangeParametersTable.put(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[3] +
                        "_" + SPParamItemRSense.rsenseLabels[3],
                new AutoRangeParameters(SPParamItemOutGain.outgainLabels[7],
                        SPParamItemInGain.ingainLabels[3], SPParamItemRSense.rsenseLabels[3], multiplier[10], Vmax2));
        keysList.add(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[3] +
                "_" + SPParamItemRSense.rsenseLabels[3]);

        autoRangeParametersTable.put(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[3] +
                        "_" + SPParamItemRSense.rsenseLabels[2],
                new AutoRangeParameters(SPParamItemOutGain.outgainLabels[7],
                        SPParamItemInGain.ingainLabels[3], SPParamItemRSense.rsenseLabels[2], multiplier[11], Vmax2));
        keysList.add(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[3] +
                "_" + SPParamItemRSense.rsenseLabels[2]);

        autoRangeParametersTable.put(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[3] +
                        "_" + SPParamItemRSense.rsenseLabels[1],
                new AutoRangeParameters(SPParamItemOutGain.outgainLabels[7],
                        SPParamItemInGain.ingainLabels[3], SPParamItemRSense.rsenseLabels[1], multiplier[12], Vmax2));
        keysList.add(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[3] +
                "_" + SPParamItemRSense.rsenseLabels[1]);

        autoRangeParametersTable.put(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[3] +
                        "_" + SPParamItemRSense.rsenseLabels[0],
                new AutoRangeParameters(SPParamItemOutGain.outgainLabels[7],
                        SPParamItemInGain.ingainLabels[3], SPParamItemRSense.rsenseLabels[0], multiplier[13], Vmax2));
        keysList.add(SPParamItemOutGain.outgainLabels[7] + "_" + SPParamItemInGain.ingainLabels[3] +
                "_" + SPParamItemRSense.rsenseLabels[0]);

    }



    public void autoRange(SPMeasurementParameterEIS paramEIS) throws SPException{
        SPMeasurementOutput message = new SPMeasurementOutput();
        //int tempIndexOfMeasure = paramEIS.getMeasure();
        //int indexOfMeasure = 7; // Capacitance in order to force I and Q measures
        //paramEIS.setMeasure(SPMeasurementParameterEIS.measureLabels[indexOfMeasure]);

        String outputMessage = "";

        int NDATA = 1;

        double VM;
        int i = 0;
        boolean firstMeasure = true;

        boolean continueLoopReadChain = true;
        boolean continueLoopStimulusChain = true;

        boolean Imeasure = false;
        boolean Qmeasure = false;
        boolean IQmeasure = false;
        if(paramEIS.getMeasure()==SPMeasurementParameterEIS.IN_PHASE || paramEIS.getMeasure()== SPParamItemMeasures.CONDUCTANCE){
            Imeasure = true;
        }else if(paramEIS.getMeasure()==SPMeasurementParameterEIS.QUADRATURE || paramEIS.getMeasure()==SPParamItemMeasures.SUSCEPTANCE){
            Qmeasure = true;
        }else{
            IQmeasure = true;
        }

        boolean keepFrequency = false;
        int frequencyIndex = 0;

        do {

            AutoRangeParameters currentAutoRangeParameter = autoRangeParametersTable.get(keysList.get(i));
            paramEIS.setOutGain(currentAutoRangeParameter.OutGain);
            paramEIS.setInGain(currentAutoRangeParameter.InGain);
            paramEIS.setRsense(currentAutoRangeParameter.Rsense);

            if(selectFrequency && !keepFrequency) {
                if(Imeasure||Qmeasure && frequencyIndex<Ifrequencies.length) {
                    keepFrequency=true;
                    if(Imeasure)
                        paramEIS.setFrequency((float)Ifrequencies[frequencyIndex]);
                    else
                        paramEIS.setFrequency((float)Qfrequencies[frequencyIndex]);

                    frequencyIndex++;

                }else{
                    throw new SPException("Autorange cannot set Frequency if I or Q impedance component have not been selected");
                }
            }

            measurement.setEIS(paramEIS, message);
            boolean sendCommandX20 = true;
            if (true)
                throw new SPException("setADC in autorange should be verified for RUN5 and RUN6 in terms of FIFO_READ and FIFO_DEEP dimension");

            measurement.setADC(NDATA, NDATA, paramEIS.getInGainLabel(), null, SPMeasurementParameterADC.INPORT_IA, sendCommandX20, message);
            //SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;
            //((SPMeasurementStatusIQ) status).setBufferFilled(true);
            double[][] output = measurement.getSingleEIS(paramEIS, false, message);

            output[0][SPMeasurementParameterQI.QUADRATURE] = (output[0][SPMeasurementParameterQI.QUADRATURE] * VREF) / (Math.pow(2, 15) * 1000);
            output[0][SPMeasurementParameterQI.IN_PHASE] = (output[0][SPMeasurementParameterQI.IN_PHASE] * VREF) / (Math.pow(2, 15) * 1000);
            VM = Math.sqrt(Math.pow(output[0][SPMeasurementParameterQI.QUADRATURE], 2) + Math.pow(output[0][SPMeasurementParameterQI.IN_PHASE], 2));

            if(i==0 && VM>currentAutoRangeParameter.Vmax){
                if(!selectFrequency) {
                    throw new SPException("Too Low Impedance");
                }else{
                    keepFrequency = false;
                }
            }
            else {


                /*if (i == keysList.size() - 1 ||
                        VM * autoRangeParametersTable.get(keysList.get(i)).multiplier >= autoRangeParametersTable.get(keysList.get(i)).Vmax) {
                    //configuration found!!!
                    continueLoopReadChain = false;
                }*/


                if (VM >= Math.sqrt(2) * this.VminNoise) { //If the measured signal is bigger then the noise threshold
                    if (i == keysList.size() - 1 ||
                            VM * autoRangeParametersTable.get(keysList.get(i)).multiplier >= autoRangeParametersTable.get(keysList.get(i)).Vmax) {
                        //configuration found!!!
                        continueLoopReadChain = false;
                    }
                } else {
                    if (i == keysList.size() - 1) {
                        throw new SPException("Signal too low to be measured. Too high impedance (too high resistance " +
                                "or too low capacitance)");
                    }
                }

                i++;

            }
        }while(continueLoopReadChain && i<keysList.size());

        if(!continueLoopReadChain){
            //If the first phase of the algorithm has found a correct configuration

            //Control stimulus chain:
            double Vmod_stimulus_nominal = (VREF*1/Math.PI*1/(8-Integer.parseInt(paramEIS.getOutGainLabel())))/1000;

            //Measure Vmod stimulus

            SendInstructionInOut ref = new SendInstructionInOut(paramEIS.getTemporaryCluster());

            //--------------------


            double[] availableFreq = null;
            if(selectFrequency) {
                if (paramEIS.getMeasureLabel().equals(SPParamItemMeasures.measureLabels[0])
                        || paramEIS.getMeasureLabel().equals(SPParamItemMeasures.measureLabels[2])) {
                    //I measure
                    availableFreq = this.Ifrequencies;

                } else if (paramEIS.getMeasureLabel().equals(SPParamItemMeasures.measureLabels[1])
                        || paramEIS.getMeasureLabel().equals(SPParamItemMeasures.measureLabels[3])) {
                    //Q measure
                    availableFreq = this.Qfrequencies;
                } else {
                    throw new SPException("Set a single component (I or Q) in order to allow the autorange algorithm to " +
                            "the best frequency");
                }
            }




            String currentInGain = paramEIS.getInGainLabel();
            int j=frequencyIndex-1;


            int indexToSaveappo = measurement.getMeasureIndexToSave();
            measurement.setMeasureIndexToSave(7);
            do{

                String instruction, instructionNum;
                //Set GA=1
                paramEIS.setInGain(SPParamItemInGain.ingainLabels[0]);

                //TODO:to remove
                //paramEIS.setOutGain(SPMeasurementParameterEIS.outgainLabels[3]);

                measurement.setEIS(paramEIS, message);

                ArrayList<String> instructionList = new ArrayList<>();

                instruction = "WRITE CHA_SELECT S 0x";
                instructionNum = "1" + paramEIS.getContacts() + "0" + paramEIS.getInPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE DSS_SELECT M 0x";
                instructionNum = "0" + paramEIS.getModeVI().substring(0, 1) + "000" + paramEIS.getOutGain() + "01"+
                        paramEIS.getContacts() + paramEIS.getOutPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                /*
                //IVM = 1
                instructionList.add("WRITE CHA_SELECT S 0X5C");
                //FSO = 1
                instructionList.add("WRITE DSS_SELECT S 0X7C");
                */

                measurement.sendSequences(instructionList,ref,"AutoRangeV2: Stimulus chain control");

                boolean sendCommandX20 = true;
                if (true)
                    throw new SPException("setADC in autorange should be verified for RUN5 and RUN6 in terms of FIFO_READ and FIFO_DEEP dimension");

                measurement.setADC(NDATA, NDATA, paramEIS.getInGainLabel(), null, SPMeasurementParameterADC.INPORT_IA, sendCommandX20, message);
                double[][] output = measurement.getSingleEIS(paramEIS, false, message);

                output[0][SPMeasurementParameterQI.QUADRATURE] = (output[0][SPMeasurementParameterQI.QUADRATURE] * VREF) / (Math.pow(2, 15) * 1000);
                output[0][SPMeasurementParameterQI.IN_PHASE] = (output[0][SPMeasurementParameterQI.IN_PHASE] * VREF) / (Math.pow(2, 15) * 1000);
                VM = Math.sqrt(Math.pow(output[0][SPMeasurementParameterQI.QUADRATURE], 2) + Math.pow(output[0][SPMeasurementParameterQI.IN_PHASE], 2));


                if(VM>0.9*Vmod_stimulus_nominal){
                    continueLoopStimulusChain = false;
                }
                else{
                    if(availableFreq!= null && j<this.Qfrequencies.length-1){
                        j = j+1;
                        paramEIS.setFrequency((float)availableFreq[j]);

                    }
                    else{
                        throw new SPException("Fail in stimulus chain control");
                    }
                }




            }while(this.selectFrequency && continueLoopStimulusChain && j<this.Qfrequencies.length);

            measurement.setMeasureIndexToSave(indexToSaveappo);
            paramEIS.setInGain(currentInGain);

        }

    }

    public static class AutoRangeParameters{
        String Rsense;
        String InGain;
        String OutGain;
        double Vmax;
        double multiplier;


        AutoRangeParameters(String OutGain, String InGain, String Rsense, double multiplier,double Vmax){
            this.OutGain = OutGain;
            this.Rsense = Rsense;
            this.InGain = InGain;
            this.multiplier = multiplier;
            this.Vmax = Vmax;
        }
    }










}
