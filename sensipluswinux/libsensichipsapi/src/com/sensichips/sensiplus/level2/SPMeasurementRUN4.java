    package com.sensichips.sensiplus.level2;

    import com.sensichips.sensiplus.SPException;
    import com.sensichips.sensiplus.config.SPConfiguration;
    import com.sensichips.sensiplus.level1.chip.*;
    import com.sensichips.sensiplus.level2.parameters.*;
    import com.sensichips.sensiplus.level2.parameters.items.*;
    import com.sensichips.sensiplus.level2.status.*;
    import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
    import com.sensichips.sensiplus.level2.util.SPMeasurementUtil;
    import com.sensichips.sensiplus.util.SPComplex;
    import com.sensichips.sensiplus.util.SPDelay;
    import com.sensichips.sensiplus.util.log.SPLogger;
    import com.sensichips.sensiplus.util.log.SPLoggerInterface;

    import java.util.ArrayList;
    import java.util.List;


    /**
     * The High Level part of the library presents to the programmers all the methods available for different types
     * of analytical measurements.  Alle the classes of this level are contained in com.sensichips.android.level2.
     * In particular SPMeasurement is the abstract class with all the expected methods while SPMeasurementRUN4
     * is the specialized class for the Run4 of the chip (2016). In addition the com.sensichips.android.level2.parameter
     * contains a set of classes created to manage the input parameters of the measurement methods.
     * In this module (in particulare in the SPMeasurementRUN4 class) it   is stored the complete state of the current measure.
     * In order to define the current measure type a call to the corresponding setMeasureType method is needed. For each
     * type of measurement there are two methods: setMeasureType and getMeasureType.
     * The setMeasureType sets the initial chip state and internal method parameters that will be used by
     * subsequent getMeasureType calls. The initial stte then evolves at each getMeasureType according to the
     * type of action that it's being performed. For each setMeasureType its possible to have 0 or more
     * getMeasureType subsequently calls. Each getMeasureType request to the chip one or more
     * measurements, it returns to the caller the corresponding numeric values and update the internal state of the
     * method.
     * Therefore the setMeasureType:
     * - parses the input parameters in order to identify errors and make some computation on it (for example in
     * order to identify the frequency available on the chip with respect to the frequency requested by the caller)
     * - send a sequence of WRITE instruction the the chip by mean of the L1 library;
     * - define the initial state of the L2 module.
     * <p>
     * The getMeasureType will then be called a number of times according to the performed measurement:
     * - each call will interact with the chip with WRITE and READ accesses, to read required data
     * - it will update the API module state and prepare for subsequent reads
     * <p>
     * For example:
     * setEIS/getEIS (Electrical Impedance Spectroscopy)
     * - setEIS writes into the chip registers that are involved in this type of measure by preparaing the stimulus
     * <p>
     * and the measurement section;
     * - consequently the getEIS call implements two measurements: in quadrature and in phase. By means of
     * this two value the methods computes Resistance, Capacitance and Inductance.
     * <p>
     * setPOT/getPOT (Potentiostat)
     * - setPOT sets the chip for the subsequent reads and initializes parameters within the API, i.e. the type of
     * <p>
     * ramp to be generated among the choices presented to the used,
     * - back-to-back getPOT calls will set the chip to the output stimulus according to the selected ramp and read
     * the sensor according to the user specified timing.
     * <p>
     * Methods of Level 2 throws SPException in case of errors with SPMeasurement API or
     * SPException in case of errors in the input parameters.
     * <p>
     * <p>
     * Property: Sensichips s.r.l.
     *
     * @author: Mario Molinara, mario.molinara@sensichips.com, m.molinara@unicas.it
     */
    public class SPMeasurementRUN4 extends SPMeasurement {

        public SPMeasurementStatus getStatus() {
            return status;
        }

        protected SPMeasurementStatus status = null;
        protected int numOfChips;



        protected double[] M;
        protected double[] N;




        protected static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L2;


        public static int GET_EIS_ELAPSED = 0;
        public static int GET_I_ELAPSED = 0;
        public static int GET_Q_ELAPSED = 0;
        public static int GET_DATA_ELAPSED = 0;

        protected double clock = 10000000.0;
        protected String osc_trim = "0x06";
        protected SPCluster cluster;


/*        // Marco FERDINANDI
        // ADC autoscale compensation
        private double[][] previousMeasurements=null;
        private double[][] delta = null;
        private boolean deltaActive = false;
        private boolean adcOverflowReached = false;*/

        public double[] getM() {
            return M;
        }

        public double[] getN() {
            return N;
        }


        /**
         * Do not invoke directly: use SPMeasurement.apiFactory
         * @param conf
         * @param logDirectory
         * @throws SPException
         * @throws SPException
         */
        public SPMeasurementRUN4(SPConfiguration conf, String logDirectory, String MEASURE_TYPE) throws SPException, SPException {
            super(conf, logDirectory, MEASURE_TYPE);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start SPMeasurementRUN4", DEBUG_LEVEL);
            spConfiguration = conf;
            cluster = spConfiguration.getCluster();
            numOfChips = cluster.getActiveSPChipList().size();

            if (numOfChips <= 0){
                throw new SPException("SPMeasurement cannot be instantiated with num of chips < 1");
            }

            M = new double[numOfChips];
            N = new double[numOfChips];
            clock = spConfiguration.getCluster().getFamilyOfChips().getSYS_CLOCK();
            osc_trim = spConfiguration.getCluster().getFamilyOfChips().getOSC_TRIM();



            //delta = new double[numOfChips][SPMeasurementParameterEIS.NUM_OUTPUT];
        }



        /**
         * The setEIS method allows setting all required parameters to perform Electric Impedance Spectroscopy
         * (EIS) measurements.
         *
         * @param param @see SPMeasurementParameterEIS
         * @param m     log messages
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized void setEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m)
                throws SPException {


            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setEIS", DEBUG_LEVEL);
            SPLogger.getLogInterface().d(LOG_MESSAGE, param.toString(), DEBUG_LEVEL);

            int chopCutOff = 10000;

            if (param == null || !param.isValid()) {
                throw new SPException("Parameters problem: " + param);
            }



            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            try {
                ArrayList<String> instructionList = new ArrayList<>();
                lastParameter = param;
                float OSM;
                int FRD, DSS_DIVIDER;

                AvailableFrequencies frequencyCalculus = new AvailableFrequencies();

                String instruction, instructionNum;

                instruction = "WRITE CHA_SELECT S 0x";
                instructionNum = param.getModeVI().substring(1, 2) + param.getContacts() + "0" + param.getInPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE DSS_SELECT M 0x";
                instructionNum = "0" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00"+ param.getContacts() + param.getOutPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE DSS_DIVIDER M 0x";
                AvailableFrequencies.availableFrequencies(spConfiguration, param.getFrequency(), frequencyCalculus);
                OSM = frequencyCalculus.OSM;
                DSS_DIVIDER = frequencyCalculus.DSS_DIVIDER;
                FRD = frequencyCalculus.FRD;

                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(DSS_DIVIDER, 15) + Integer.toString(FRD);
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE DAS_CONFIG M 0x";
                instructionNum = "0";
                int OSM_INT = (int) OSM;
                instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSM_INT, 3);
                int numBit = 12;
                int modSignDCBias = bitSignDCBias(param.getDCBiasP(), numBit);
                instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBias, numBit);
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE CHA_FILTER M 0x";
                String chopperValue = "";
                if (frequencyCalculus.availableFrequency <= chopCutOff) {
                    chopperValue = "12"; //only ADc choppers enabled (IA chopper causes error EIS in measurements)
                } else {
                    chopperValue = "12"; //only ADc choppers enabled (IA chopper causes error EIS in measurements)
                }

                String HE3 = "0";
                if (param.getHarmonic().equals(SPParamItemHarmonic.harmonicLabels[2])) {
                    HE3 = "1";
                }
                instructionNum = "000" + param.getQI() + HE3 + param.getHarmonic();
                if (instructionNum.length() != 8)
                    throw new SPException("lunghezza dati numerici sbagliata in instruction: " + instruction); //  numeric substring after 'OX' have to have lenght 8 bit

                String hexValueToFillWithZero = SPMeasurementUtil.zeroFill(Integer.toHexString(Integer.parseInt(instructionNum, 2)), 2);
                hexValueToFillWithZero = chopperValue + hexValueToFillWithZero;
                instruction += SPMeasurementUtil.zeroFill(hexValueToFillWithZero, 4);
                instructionList.add(instruction);

                instruction = "WRITE CHA_CONDIT M 0x";

                if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.QUADRANT){
                    instructionNum = "000000";
                } else {
                    instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getPhaseShift(), 5);
                    if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.COARSE){
                        instructionNum += "1";
                    } else if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.FINE){
                        instructionNum += "0";
                    }
                }
                instructionNum += param.getInGain() + param.getRsense() + "000000";
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                instruction = "WRITE ANA_CONFIG S 0x3F";
                instructionList.add(instruction);


                sendSequences(instructionList, ref, "setEIS");

                // "" + logDirectory + "/Download/" + measureLogFile

                String nomeFileDump = "" + logDirectory + "/Download/" + "getEIS_dump.txt";
                if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null){
                    status = new SPMeasurementStatusIQ(this, param, spProtocol, SPMeasurementParameterEIS.NUM_OUTPUT,
                        frequencyCalculus, FRD, null, nomeFileDump);
                }
                if (m != null) {
                    m.out = ref.log;
                }

            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End setEIS", DEBUG_LEVEL);

        }



        /**
         * This method realize two read operation for in quadrature component (Q) and for "in phase"
         * component of the signal.
         *
         * @param param
         * @param m     log messages
         * @return
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */

        protected void getIQ(SPMeasurementParameterQI param, SPMeasurementOutput m, SPMeasurementSequential spMeasurementSequential, double[][] output)
                throws SPException {


            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getIQ", DEBUG_LEVEL);

            if (lastParameter == null || !(lastParameter instanceof SPMeasurementParameterQI) || status == null || !(status instanceof SPMeasurementStatusIQ)) {
                throw new SPException("Before getQI must be called setEIS or setULTRASOUND");
            }

            //double[][] output = new double[cluster.getActiveSPChipList().size()][SPMeasurementParameterEIS.NUM_OUTPUT];

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());



            try {


                SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

                ref.log += "-----------------------------\ngetIQ:\n";

                getI(ref, output, param ,spMeasurementSequential,m);



                if (iqStatus.getFrequencyCalculus().OSM != 0){
                    getQ(ref, output, param,spMeasurementSequential,m);
                }

                ref.log += "\nEND getIQ.\n-----------------------------\n";

                if (m != null) {
                    m.out = ref.log;
                }


            } catch (SPException e) {
                throw e;
            } catch (Exception e) {
                // Catch any other exception
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End getQI", DEBUG_LEVEL);

            return;
        }



        protected void getQ(SendInstructionInOut ref, double output[][],SPMeasurementParameter param, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m) throws SPException {
            /// Leggo QUADRATURA
            long start = System.currentTimeMillis();

            SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;
            ref.instructionToSend = iqStatus.getInstructionQ_Prepared();

            GET_Q_ELAPSED += (System.currentTimeMillis() - start);

            sendSequence(ref);

            start = System.currentTimeMillis();


            GET_Q_ELAPSED += (System.currentTimeMillis() - start);

            int[][] dataRead = getDATA(param, true, true, spMeasurementSequential, m);

            start = System.currentTimeMillis();

            ref.log += m.out;

            for(int i = 0; i < cluster.getActiveSPChipList().size(); i++) {
                output[i][SPParamItemMeasures.QUADRATURE] = dataRead[i][0];
            }
            GET_Q_ELAPSED += (System.currentTimeMillis() - start);

        }
        protected void getI(SendInstructionInOut ref, double output[][],SPMeasurementParameter param, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m) throws SPException {

            long start = System.currentTimeMillis();

            SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

            /// Leggo FASE
            ref.instructionToSend = iqStatus.getInstructionI_Prepared();
            GET_I_ELAPSED += (System.currentTimeMillis() - start);

            sendSequence(ref);

            start = System.currentTimeMillis();

            GET_I_ELAPSED += (System.currentTimeMillis() - start);

            int[][] adc = getDATA(param, true, true, spMeasurementSequential, m);

            start = System.currentTimeMillis();

            ref.log += m.out;

            for(int i = 0; i < cluster.getActiveSPChipList().size(); i++) {
                output[i][SPParamItemMeasures.IN_PHASE] = adc[i][0];
            }
            GET_I_ELAPSED += (System.currentTimeMillis() - start);

        }





        /**
         * @param param
         * @param m     log messages
         * @return
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized double[][] getEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m)
                throws SPException {

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getEIS", DEBUG_LEVEL);

            if (lastParameter == null || !(lastParameter instanceof SPMeasurementParameterEIS) || status == null || !(status instanceof SPMeasurementStatusIQ)
                    ) {
                throw new SPException("Before getEIS must be called setEIS.");
            }

            SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

            double[][] output = null;

            int numberOfMeasure = param.isBurstMode() ? Integer.parseInt(param.getFilter()) : 1;

            if (iqStatus.isBufferFilled()){
                for(int i = 0; i < numberOfMeasure; i++){
                    output = getSingleEIS(param, isAutomaticADCRangeAdjustment(),m);
                }
            } else {
                for(int i = 0; i < Integer.parseInt(param.getFilter()); i++){
                    output = getSingleEIS(param, isAutomaticADCRangeAdjustment(),m);
                    SPLogger.getLogInterface().d(LOG_MESSAGE, "Filling buffer: " + (i + 1), DEBUG_LEVEL);
                }
                iqStatus.setBufferFilled(true);
            }


            return output;
        }

        protected void evaluateImpedanceDataReader(SPMeasurementParameter param,  SendInstructionInOut ref,double output[][], SPMeasurementOutput m) throws SPException {
            SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;


            if (param instanceof SPMeasurementParameterEIS) {
                SPMeasurementSequential spMeasurementSequential = iqStatus.getSpMeasurementSequential();

                if (this.getMeasureIndexToSave() == SPParamItemMeasures.IN_PHASE ||
                        (this.getMeasureIndexToSave() == SPParamItemMeasures.CONDUCTANCE)) {
                    getI(ref, output, param, spMeasurementSequential, m);


                } else if (this.getMeasureIndexToSave() == SPMeasurementParameterEIS.QUADRATURE ||
                        (this.getMeasureIndexToSave() == SPParamItemMeasures.SUSCEPTANCE)) {
                    getQ(ref, output, param, spMeasurementSequential, m);

                } else {

                    getIQ(((SPMeasurementParameterEIS) param), m, spMeasurementSequential, output);

                    ref.log += m.out;
                }

                if (this.isSystemLevelChopperActive()) {
                    iqStatus.updateSystemLevelChopper(output);
                }
            }
        }

        protected void evaluateImpedanceCalc(SPMeasurementParameter param, SendInstructionInOut ref, double output[][], SPMeasurementOutput m) throws SPException{
            SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

            for(int i = 0; i < cluster.getActiveSPChipList().size(); i++) {
                if (iqStatus.getFrequencyCalculus().availableFrequency == 0) {
                    double vdd = 3300;

                    double VSCM_BIAS = 0;
                    double adc = output[i][SPParamItemMeasures.IN_PHASE];
                    double v_dc_dass = vdd/2 + spConfiguration.getVREF() * ((((SPMeasurementParameterEIS)param).getDCBiasP()) / 4096.0);
                    double v_dc_vscm = vdd/2 +  spConfiguration.getVREF() * ((((SPMeasurementParameterEIS)param).getDCBiasN()) / 64.0);

                    double delta_V = v_dc_dass - v_dc_vscm;

                    output[i][SPParamItemMeasures.DELTA_V_APPLIED] = delta_V;


                    output[i][SPParamItemMeasures.CURRENT_APPLIED] = ((Math.pow(10,-3)*spConfiguration.getVREF())/
                            (2*1400))*((1/(8-Integer.parseInt(((SPMeasurementParameterEIS) param).getOutGainLabel())))+
                            ((SPMeasurementParameterEIS) param).getDCBiasP()/4096);


                    double v_adc = adc * spConfiguration.getVREF() / Math.pow(2, 15);


                    if(((SPMeasurementParameterEIS) param).getModeVILabel().equals(SPParamItemModeVI.ModeVILabels[1]))
                        output[i][SPParamItemMeasures.RESISTANCE] = iqStatus.getRsense() * iqStatus.getInGain() * delta_V / v_adc;
                    else if(((SPMeasurementParameterEIS) param).getModeVILabel().equals((SPParamItemModeVI.ModeVILabels[2]))){
                        double IoutMAX = 1.22*Math.pow(10,-3);//if IFS=1
                        double Iapplied = ((IoutMAX*spConfiguration.getVREF()*Math.pow(10,-3))/(2*1.14))
                                *(1+(((SPMeasurementParameterEIS)param).getDCBiasP())/4096.0 );
                        output[i][SPParamItemMeasures.RESISTANCE] = v_adc*Math.pow(10,-3)/(Iapplied*iqStatus.getInGain());
                    }


                    output[i][SPParamItemMeasures.CAPACITANCE] = 0;
                    output[i][SPParamItemMeasures.INDUCTANCE] = 0;

                    // Calcolo resistenza, capacità e induttanza per frequenza != 0
                } else {


                    double phaseComponent = output[i][SPMeasurementParameterQI.IN_PHASE]/NORMALIZATION_FACTOR;
                    double quadratureComponent = output[i][SPMeasurementParameterQI.QUADRATURE]/NORMALIZATION_FACTOR;

                    ref.log += "I: " + phaseComponent;
                    ref.log += ", Q: " + quadratureComponent;

                    // Calcoli con valori complessi
                    SPComplex c = new SPComplex(phaseComponent, quadratureComponent);

                    double zetaMod = iqStatus.getK() / (c.abs());
                    double zetaPha = c.phase();
                    output[i][SPParamItemMeasures.MODULE] = zetaMod;
                    output[i][SPParamItemMeasures.PHASE] = zetaPha;



                    //output[i][SPMeasurementParameterEIS.REAL] = zetaMod*Math.cos(zetaPha);
                    //output[i][SPMeasurementParameterEIS.IMAG] = zetaMod*Math.sin(zetaPha);
                    //Calcolo conduttanza e suscettanza
                    output[i][SPParamItemMeasures.CONDUCTANCE] = (1/zetaMod)*Math.cos(zetaPha);
                    output[i][SPParamItemMeasures.SUSCEPTANCE] = (1/zetaMod)*Math.sin(-zetaPha);


                    SPComplex zeta = new SPComplex(zetaMod * Math.cos(zetaPha), zetaMod * Math.sin(zetaPha));

                    // R
                    //output[i][SPParamItemMeasures.RESISTANCE] = 1 / (zeta.reciprocal().re());
                    output[i][SPParamItemMeasures.RESISTANCE] = 1/output[i][SPParamItemMeasures.CONDUCTANCE];


                    output[i][SPParamItemMeasures.CAPACITANCE] = (zeta.reciprocal().im() * Math.pow(10, 12)) / (2 * Math.PI * iqStatus.getFrequencyCalculus().availableFrequency);


                    output[i][SPParamItemMeasures.INDUCTANCE] = 0;
                }




                ref.log += "R: " + output[i][SPParamItemMeasures.RESISTANCE] + " [Ohm]";
                ref.log += ", C: " + output[i][SPParamItemMeasures.CAPACITANCE] + " [pF]";
                ref.log += ", L: " + output[i][SPParamItemMeasures.INDUCTANCE] + " [H]";

            }
        }


        protected void evaluateImpedance(SPMeasurementParameter param,  SendInstructionInOut ref,double output[][], SPMeasurementOutput m) throws SPException{


            evaluateImpedanceDataReader(param, ref, output, m);

            evaluateImpedanceCalc(param, ref, output, m);

        }


        boolean ADCSettingsAdjustment(SPMeasurementParameterEIS param, double[][] measures,  SPMeasurementOutput m)throws SPException{
            boolean flagUnderflow = true;
            boolean flagOverflow = false;
            double Vi,Vq,VM,Vmin;

            SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

            double minimumUnderflowThreshold = 0.016;

            String key = param.getInGainLabel()+"_"+param.getRsenseLabel();
            //String key = param.getOutGainLabel()+ "_" + param.getInGainLabel()+"_"+param.getRsenseLabel();
            SPAutoRangeAlgorithm.AutoRangeConfig autoRangeConfig = SPAutoRangeAlgorithm.getAutoRangeConfigMap().get(key);
            //SPAutoRangeAlgorithmV2 autoRangeConfig = SPAutoRangeAlgorithm.getAutoRangeConfigMap().get(key);

            /*if(this.autoRangeAlgorithm== null && this.autoRangeAlgorithm.getAutoRangeParametersTable() == null){
                throw new SPException("Autorange algorithm not initialized!");
            }*/


            //SPAutoRangeAlgorithmV2.AutoRangeParameters autoRangeParameters = this.autoRangeAlgorithm.getAutoRangeParametersTable().get(key);

            /*if(autoRangeParameters == null){
                if(param.getOutGainLabel().equals("7")){
                    key = "7_40_50000";
                }
                else{
                    key = "0_1_50";
                }
                autoRangeParameters = this.autoRangeAlgorithm.getAutoRangeParametersTable().get(key);

            }*/

            //EVALUATE Vmax
            double Vmax = 0;
            if(param.getInGainLabel().equals("1")){
                Vmax = (spConfiguration.getVREF()/Math.PI)/1000.0;
            }
            else{
                Vmax = (spConfiguration.getVREF()*2/Math.PI)/1000.0;
            }

            int divider = (Integer.parseInt(SPParamItemInGain.ingainLabels[3])*Integer.parseInt(SPParamItemRSense.rsenseLabels[0]))
                    /(Integer.parseInt(param.getRsenseLabel())*Integer.parseInt(param.getInGainLabel()));

            if(divider!=1)
                Vmin = Vmax/divider;
            else{
                //Vmin = this.autoRangeAlgorithm.getVminNoise();
                Vmin = autoRangeConfig.Vmin;
            }
            if(Vmin<minimumUnderflowThreshold){
                Vmin = minimumUnderflowThreshold;
            }

            //This cycle is over the chip number
            for(int i=0; i<measures.length; i++){
                if(param.getMeasureLabel().equals(SPParamItemMeasures.measureLabels[0]) || param.getMeasureLabel().equals(SPParamItemMeasures.measureLabels[2]) ){
                    Vq = 0;
                    Vi = (measures[i][SPMeasurementParameterQI.IN_PHASE] * spConfiguration.getVREF()) / (Math.pow(2,15) * 1000);
                    VM = Math.sqrt(Math.pow(Vi, 2) + Math.pow(Vq, 2));
                }
                else if(param.getMeasureLabel().equals(SPParamItemMeasures.measureLabels[1]) || param.getMeasureLabel().equals(SPParamItemMeasures.measureLabels[3]) ){
                    Vq = (measures[i][SPMeasurementParameterQI.QUADRATURE] * spConfiguration.getVREF()) / (Math.pow(2,15) * 1000);
                    Vi = 0;
                    VM = Math.sqrt(Math.pow(Vi, 2) + Math.pow(Vq, 2));
                }
                else {
                    Vq = (measures[i][SPMeasurementParameterQI.QUADRATURE] * spConfiguration.getVREF()) / (Math.pow(2, 15) * 1000);
                    Vi = (measures[i][SPMeasurementParameterQI.IN_PHASE] * spConfiguration.getVREF()) / (Math.pow(2, 15) * 1000);
                    VM = Math.sqrt(Math.pow(Vi, 2) + Math.pow(Vq, 2));
                }
                if(VM>=Vmax){
                    //ADC OVERFLOW
                    flagOverflow = true;
                    flagUnderflow = false;
                }

                else if(VM<Vmin){
                    flagUnderflow &=true;
                }
                else{
                    flagUnderflow = false;
                }
            }

            if(flagOverflow) {
                m.ADCOverflow = true;
                param.getSPMeasurementMetaParameter().setAdcOverflowReached(true);

                //OLD VERSION
                param.getSPMeasurementMetaParameter().setPreviousMeasurements(measures);

                //NEW VERSION
                //param.getSPMeasurementMetaParameter().setPreviousMeasurements(iqStatus.getLastInsertedSample());

                param.reduceADCRange();
                setEIS(param,m);
            }
            else if(flagUnderflow){
                m.ADCUnderflow = true;
                param.getSPMeasurementMetaParameter().setAdcUnderflowReached(true);
                param.increaseADCRange();
                setEIS(param,m);
            }

            return flagOverflow||flagUnderflow;
        }

        public void autoscale(){

        }


        /**
         *
         * @param param
         * @param m
         * @return
         * @throws SPException
         * @throws SPException
         * @throws SPException
         * @throws SPException
         * @throws SPException
         * @throws SPException
         */
        @Override
        public synchronized double[][] getSingleEIS(SPMeasurementParameterEIS param, boolean ADCAdjustment, SPMeasurementOutput m) throws SPException {
            // Numbero of output: P = in phase, Q = quadrature, R = resistance (Ohm), C = capacity (pF), L = inductance (H)
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getEIS", DEBUG_LEVEL);

            if (lastParameter == null || !(lastParameter instanceof SPMeasurementParameterEIS) || status == null || !(status instanceof SPMeasurementStatusIQ)) {
                throw new SPException("Before getEIS must be called setEIS.");
            }

            boolean noCorrection = false;


            long start = System.currentTimeMillis();

            double[][] output = new double[cluster.getActiveSPChipList().size()][SPMeasurementParameterEIS.NUM_OUTPUT];



            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());


            try {
                SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;


                ref.log += "-----------------------------\ngetEIS:\n";

                evaluateImpedance(param,ref,output,m);

                if(ADCAdjustment){
                    /*if(ADCSettingsAdjustment(param,output, m)){
                        evaluateImpedance(param,ref,output,m);
                    }*/
                    /*
                    while(ADCSettingsAdjustment(param,output,m)){
                        evaluateImpedance(param,ref,output,m);
                    }*/

                    if(ADCSettingsAdjustment(param,output,m)){
                        evaluateImpedance(param,ref,output,m);
                    }

                    if(param.getSPMeasurementMetaParameter().isAdcOverflowReached() || param.getSPMeasurementMetaParameter().isAdcUnderflowReached()) {
                        param.getSPMeasurementMetaParameter().updateDelta(output);
                    }
                    if(param.getSPMeasurementMetaParameter().isDeltaActive() ){
                        for(int i=0; i<output.length; i++){
                            for(int j=0; j<output[i].length; j++){
                                output[i][j] = output[i][j]+param.getSPMeasurementMetaParameter().getDelta()[i][j];
                            }
                        }
                    }
                }
                iqStatus.updateStatus(output);

                for(int i = 0; i < numOfChips; i++){
                    int ingain = Integer.parseInt(SPParamItemInGain.ingainLabels[SPMeasurementUtil.sarchIndex(SPParamItemInGain.ingainValues, param.getInGain())]);
                    int rsense = Integer.parseInt(SPParamItemRSense.rsenseLabels[SPMeasurementUtil.sarchIndex(SPParamItemRSense.rsenseValues, param.getRsense())]);
                    output[i][SPParamItemMeasures.VOLTAGE] = ((output[i][SPParamItemMeasures.IN_PHASE]) * spConfiguration.getVREF()) / (Math.pow(2,15) * ingain);
                    output[i][SPParamItemMeasures.CURRENT] = output[i][SPParamItemMeasures.VOLTAGE] / rsense;
                }

                ref.log += "\nEND getEIS.\n-----------------------------\n";

                if (m != null) {
                    m.out = ref.log;
                }

            } catch (  SPException e) {
                //} catch (SPException  e) {
                throw new SPException("Exception in getEIS_SINGLE: " + e.getMessage());
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }

            SPLogger.getLogInterface().d(LOG_MESSAGE, "End getEIS", DEBUG_LEVEL);

            return output;
        }

        /**
         * @param param
         * @param m     log messages
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized void setULTRASOUND(SPMeasurementParameterULTRASOUND param, SPMeasurementOutput m)
                throws SPException, SPException, SPException,
                SPException, SPException, SPException {

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setULTRASOUND", DEBUG_LEVEL);
            ArrayList<String> instructionList = new ArrayList<>();

            if (param == null || !param.isValid()) {
                throw new SPException("Parameters problem: " + param);
            }

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            try {
                float OSM;
                int FRD, DSS_DIVIDER;

                AvailableFrequencies frequencyCalculus = new AvailableFrequencies();

                String instruction, instructionNum;

                instruction = "WRITE INT_CONFIG+1 S 0x10";
                instructionList.add(instruction);

                instruction = "WRITE CHA_SELECT S 0x";
                instructionNum = "000" + param.getInPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE DSS_SELECT M 0x";
                instructionNum = "0" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "001" + param.getOutPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                instruction = "WRITE DSS_DIVIDER M 0x";
                AvailableFrequencies.availableFrequencies(spConfiguration, param.getFrequency(), frequencyCalculus);

                OSM = frequencyCalculus.OSM;
                DSS_DIVIDER = frequencyCalculus.DSS_DIVIDER;
                FRD = frequencyCalculus.FRD;

                if (OSM != 0) {

                    instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(DSS_DIVIDER, 15) + Integer.toString(FRD);

                    instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);

                } else {
                    instruction += "0000";
                }
                instructionList.add(instruction);


                int numBit = 12;

                instruction = "WRITE DAS_CONFIG M 0x";
                instructionNum = "0";
                instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill((int) OSM, 3);

                String bitValueToFillWithZero = "100101000000"; // 0x940
                instructionNum += SPMeasurementUtil.zeroFill(bitValueToFillWithZero, numBit);
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE CHA_FILTER S 0x";
                String HE3 = "0";
                if (param.getHarmonic().equals(SPParamItemHarmonic.harmonicLabels[2])) {
                    HE3 = "1";
                }
                instructionNum = "0" + param.getModeVI().substring(1, 2) + "100" + HE3 + param.getHarmonic();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE CHA_CONDIT M 0x";
                instructionNum = "000000" + param.getInGain() + param.getRsense() + "000000";
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE ANA_CONFIG S 0x3E";
                instructionList.add(instruction);

                instruction = "WRITE ANA_CONFIG S 0x3F";
                instructionList.add(instruction);

                instruction = "WRITE US_BURST M 0x";
                numBit = 12;
                String appo = SPMeasurementUtil.fromIntegerToBinWithFill(param.getPulse_Repetition(), numBit);

                String instructionNum_leftPart = appo.substring(0, 4);
                String instructionNum_rigthPart = appo.substring(4, 12);

                numBit = 4;
                appo = SPMeasurementUtil.fromIntegerToBinWithFill(param.getBurst(), numBit);
                instructionNum = instructionNum_rigthPart + "00" + (param.getQI_Toggle() ? "1" : "0") + (param.getPI_Toggle() ? "1" : "0") + appo;

                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);

                instructionList.add(instruction);


                instruction = "WRITE US_TRANSMIT M 0x";
                numBit = 12;
                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getTx_Start(), numBit);
                instructionNum = instructionNum_leftPart + instructionNum;
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                instruction = "WRITE US_PROBE M 0x";
                numBit = 24;
                appo = SPMeasurementUtil.fromIntegerToBinWithFill(param.getRX_Start(), numBit);

                instructionNum_leftPart = appo.substring(0, 16);    // RX_Start<23:8>
                instructionNum_rigthPart = appo.substring(16, 24);  // RX_Start<7:0>
                numBit = 8;
                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getProbe(), numBit);
                instructionNum = instructionNum_rigthPart + instructionNum;

                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE US_RECEIVE M 0x";
                instructionNum = instructionNum_leftPart;
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                instruction = "WRITE COMMAND S 0x24";
                instructionList.add(instruction);

                // "" + logDirectory + "/Download/" + measureLogFile

                String nomeFileDump = "" + logDirectory + "/Download/" + "getULTRASOUND_dump.txt";
                status = new SPMeasurementStatusULTRASOUND(this, param, spProtocol, SPMeasurementParameterULTRASOUND.NUM_OUTPUT,
                        frequencyCalculus, FRD, nomeFileDump);

                sendSequences(instructionList, ref, "setULTRASOUND");

                if (m != null) {
                    m.out = ref.log;
                }
                lastParameter = param;

            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End setULTRASOUND", DEBUG_LEVEL);

        }




        /**
         * @param param
         * @param m     log messages
         * @return
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized double[][] getULTRASOUND(SPMeasurementParameterULTRASOUND param,  SPMeasurementOutput m)
                throws SPException {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getULTRASOUND", DEBUG_LEVEL);

            if (lastParameter == null || !(lastParameter instanceof SPMeasurementParameterULTRASOUND) || status == null || !(status instanceof SPMeasurementStatusIQ)) {
                throw new SPException("Before getULTRASOUND must be called setULTRASOUND");
            }

            double[][] output = new double[cluster.getActiveSPChipList().size()][SPMeasurementParameterULTRASOUND.NUM_OUTPUT];

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            try {


                //String message = "";
                double[][] dataRead;

                SPMeasurementStatusULTRASOUND iqStatus = (SPMeasurementStatusULTRASOUND) status;


                ref.log += "-----------------------------\ngetULTRASOUND:\n";

                getIQ(param, m,null, output);
                ref.log += m.out;


                //TODO:Verify (MARCO FERDINANDI)
                evaluateImpedance(param,ref,output,m);
                /*for(int k = 0; k < cluster.getActiveSPChipList().size(); k++) {

                    output[k][SPMeasurementParameterULTRASOUND.IN_PHASE] = dataRead[k][SPMeasurementParameterULTRASOUND.IN_PHASE];
                    output[k][SPMeasurementParameterULTRASOUND.QUADRATURE] = dataRead[k][SPMeasurementParameterULTRASOUND.QUADRATURE];
                    output[k][SPMeasurementParameterULTRASOUND.MODULE] = dataRead[k][SPMeasurementParameterULTRASOUND.MODULE];
                    output[k][SPMeasurementParameterULTRASOUND.PHASE] = dataRead[k][SPMeasurementParameterULTRASOUND.PHASE];
                }*/

                status.updateStatus(output);

                ref.log += "\nEND getUltrasound.\n-----------------------------\n";


                if (m != null) {
                    m.out = ref.log;
                }
            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }

            SPLogger.getLogInterface().d(LOG_MESSAGE, "End getULTRASOUND", DEBUG_LEVEL);

            return output;
        }





        /**
         * @param param
         * @param m     log messages
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m)
                throws SPException {

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setADC", DEBUG_LEVEL);

            if (param == null || !param.isValid()) {
                throw new SPException("Parameters problem: " + param);
            }

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            try {

                Integer FNP, OSR_DIVIDER, CICW, SAM_DIVIDER_int, SAM_DIVIDER_fraz, SAM_DIVIDER, Conversion_Rate_int, CCO, FRD = 0;
                Double Conversion_Rate = null, Conversion_Rate_fraz;

                String instruction, instructionNum;
                ArrayList<String> instructionList = new ArrayList<>();

                Integer cutOffMin = 50;
                Integer cutOffMax = 100;

                // True if the conversion rate should be evaluated internally
                if (param.getConversion_Rate() == null){
                    if (lastParameter == null) {
                        throw new SPException("Any setXXX called before setADC");
                    }
                    if (lastParameter instanceof SPMeasurementParameterEIS){
                        // Evaluate the conversion rate
                        Conversion_Rate = AvailableFrequencies.evaluateADCConversionRate(((SPMeasurementParameterEIS) lastParameter).getFrequency(), cutOffMin, cutOffMax);
                        FRD = ((SPMeasurementStatusIQ) status).getFRD();
                        ((SPMeasurementStatusIQ)status).setConversionRate(Conversion_Rate);
                    } else {
                        //Throws and exception: any other alternatives are defined
                        throw new SPException("Any suitable setXXX called before setADC");
                    }

                } else {
                    Conversion_Rate = param.getConversion_Rate();
                }

                if (Conversion_Rate.intValue() <= 1000) {
                    FNP = 0; //-- enabled ADC precision mode
                } else {
                    FNP = 1; //-- enabled ADC fast mode
                }


                if (Conversion_Rate <= clock/2016.0){
                    SAM_DIVIDER = 63;
                    double f_osr = 2 * Conversion_Rate * 2016;
                    if (FRD == 1){
                        f_osr = f_osr * 2 / 3;
                    }
                    OSR_DIVIDER = (int)Math.round(clock / f_osr);
                } else {

                    OSR_DIVIDER = 2;
                    SAM_DIVIDER_int = (int)Math.round(clock/ ((64 * OSR_DIVIDER.intValue() * Conversion_Rate))); // 8 bit integer
                    Conversion_Rate_int = ((int)clock / (64 * OSR_DIVIDER * SAM_DIVIDER_int));

                    SAM_DIVIDER_fraz = (int)Math.round((clock / (64 * OSR_DIVIDER.intValue() * Conversion_Rate)) * 2 / 3);
                    Conversion_Rate_fraz = (double) ((clock / (64 * OSR_DIVIDER.intValue() * SAM_DIVIDER_fraz.intValue())) * 2 / 3);
                    if (Math.abs(Conversion_Rate_int - Conversion_Rate) <= Math.abs(Conversion_Rate_fraz - Conversion_Rate)) {
                        SAM_DIVIDER = SAM_DIVIDER_int;
                        FRD = 0;
                    } else {
                        SAM_DIVIDER = SAM_DIVIDER_fraz;
                        FRD = 1;
                    }
                }


                if (param.getFIFO_DEEP() == 1)
                    CCO = 0;
                else
                    CCO = 1;


                if (CCO == 0) {
                    if (SAM_DIVIDER <= 2) {
                        CICW = 0;
                    } else if (SAM_DIVIDER <= 4) {
                        CICW = 3;
                    } else if (SAM_DIVIDER <= 8) {
                        CICW = 6;
                    } else if (SAM_DIVIDER <= 16) {
                        CICW = 9;
                    } else if (SAM_DIVIDER <= 32) {
                        CICW = 12;
                    } else { //if (SAM_DIVIDER <= 63){
                        CICW = 15;
                    }
                } else {
                    if (SAM_DIVIDER > 16) {
                        SAM_DIVIDER = 16;
                        FRD = 1;
                    }

                    if (SAM_DIVIDER <= 1) {
                        CICW = 3;
                    } else if (SAM_DIVIDER <= 2) {
                        CICW = 6;
                    } else if (SAM_DIVIDER <= 4) {
                        CICW = 9;
                    } else if (SAM_DIVIDER <= 8) {
                        CICW = 12;
                    } else { //if  (SAM_DIVIDER <= 16){
                        CICW = 15;
                    }

                }

                instruction = "WRITE COMMAND S 0x02";
                instructionList.add(instruction);


                instruction = "WRITE COMMAND S 0x04";
                instructionList.add(instruction);


                instruction = "WRITE CHA_DIVIDER M 0X";
                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(SAM_DIVIDER.intValue(), 6);
                instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSR_DIVIDER.intValue(), 9);
                instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(FRD, 1);
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                instruction = "WRITE CHA_SELECT+1 S 0X";
                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "00" + param.getInPortADC();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                instruction = "WRITE CHA_CONFIG S 0x";
                instructionNum = "" + CCO + "" + FNP + "1";
                instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(param.getFIFO_DEEP(), 5);


                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                sendSequences(instructionList, ref, "setADC");

                if (m != null) {
                    m.out = ref.log;
                }
                //lastParameter = param;

            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End setADC", DEBUG_LEVEL);

        }



        /**
         * @param param
         * @param m     log messages
         * @return
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        protected byte[][] getDATA_BYTE(SPMeasurementParameterADC param, Boolean inPhase, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m)
                throws SPException {

            // inPhase: is not used in RUN4

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getDATA", DEBUG_LEVEL);

            if (param == null || !param.isValid()) {
                throw new SPException("Parameters problem: " + param);
            }

            byte[][] output;// = new byte[param.getNData() * 2];

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            try {

                ref.log = "-----------------------------\ngetData:\n";

                if (param.getCommandX20()){
                    ref.instructionToSend = "WRITE COMMAND S 0x20";    // Trigger per acquisizione

                    sendSequence(ref);
                    //SPDelay.delay(15); // Wait for data ready on ADC
                    SPDelay.delay(15); // Wait for data ready on ADC
                }


                //ref.instructionToSend = "READ CHA_FIFOL M " + param.getNData() * 2;
                //ref.instructionToSend = "READ CHA_CONFIG M " + param.getNData() * 2;
                //sendSequence(ref);


                ref.instructionToSend = "READ CHA_FIFOL M " + param.getFIFO_DEEP() * 2;
                //ref.instructionToSend = "READ CHA_CONFIG M " + param.getNData() * 2;
                sendSequence(ref);

                output = new byte[ref.recValues.length][];
                SPLogger.getLogInterface().d(LOG_MESSAGE, "ref.recValues.length" + ref.recValues.length, DEBUG_LEVEL);

                for(int k = 0; k < ref.recValues.length; k++) {
                    output[k] = Utility.responseTokenizerByte(ref.recValues[k]);
                    SPLogger.getLogInterface().d(LOG_MESSAGE, "output[k]" + output[k][0] + output[k][1], DEBUG_LEVEL);
                }

                if (status != null){
                    status.updateLog(output);
                }

                ref.log += "END getData.\n-----------------------------\n";

                if (m != null) {
                    m.out = ref.log;
                }

            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End getDATA", DEBUG_LEVEL);

            return output;
        }




        /**
         * @param param
         * @param m     log messages
         * @return
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized int[][] getDATA(SPMeasurementParameter param, Boolean inPhase, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m)
                throws SPException {

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getDATAINT", DEBUG_LEVEL);

            long start = System.currentTimeMillis();

            if (param == null || !((SPMeasurementParameterADC)param).isValid()) {
                throw new SPException("Parameters problem: " + param);
            }

            int[][] output;
            int maschera = 255;

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            //int righe = 0, colonne = 0;
            try {

                ref.log = "-----------------------------\ngetDataInt:\n";

                int numOfBytePerRow = ((SPMeasurementParameterADC)param).getFIFO_READ();

                SPMeasurementOutput m2 = new SPMeasurementOutput();

                GET_DATA_ELAPSED += (System.currentTimeMillis() - start);

                byte[][] data = getDATA_BYTE(((SPMeasurementParameterADC)param), inPhase, spMeasurementSequential, m2);

                start = System.currentTimeMillis();

                output = Utility.conversionFromByteToInt(data, numOfChips, numOfBytePerRow * 2, ref);

                /*
                output = new int[data.length][];
                ref.log += m2.out;
                //int sumADCValues = 0;
                //boolean flagUnderFlow = true;
                for(int j = 0; j < cluster.getActiveSPChipList().size(); j++){
                    output[j] = new int[numInt];
                    for (int i = 0; i < numInt; i++) {
                        int x = ((int) data[j][i * 2 + 1]) << 8;   // Prolunga il segno su 32 bit (cast a int)
                        int y = data[j][i * 2];
                        y = y & maschera;                       // Server per evitare di prolungare il segno LSB
                        output[j][i] = x | y;

                        ref.log += "int values: " + output[j][i] + ", "; */


                        //ADC OVERFLOW NOTIFICATION
                        /*if(Math.abs(output[j][i])>=param.upperADCThreshold) {
                            m.ADCOverflow = true;
                            flagUnderFlow = false;
                        }
                        else{
                            flagUnderFlow &= Math.abs(output[j][i])<param.lowerADCThreshold;
                        }*/

                /*
                    }
                }
                */

                /*if(flagUnderFlow){
                    m.ADCUnderflow = true;
                }*/


                for(int i=0; i<output.length; i++) {
                    SPLogger.getLogInterface().d(LOG_MESSAGE, "INT value: "+output[i][0], SPLoggerInterface.DEBUG_VERBOSITY_L0);
                }

                ref.log += "\nEND getDataInt.\n-----------------------------\n";

                if (m != null) {
                    m.out = ref.log;
                }
                GET_DATA_ELAPSED += (System.currentTimeMillis() - start);

            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End getDATAINT", DEBUG_LEVEL);

            return output;
        }





        /**
         * @param param
         * @param m     log messages
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized void setPPR(SPMeasurementParameterPPR param, SPMeasurementOutput m)
                throws SPException {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setPPR", DEBUG_LEVEL);

            if (param == null || !param.isValid()) {
                throw new SPException("Parameters problem: " + param);
            }

            if (cluster.getActiveSPChipList().size() != 1){
                throw new SPException("This version of the library is unable to manage more the one device (chip) at a time!!!");
            }

            //TODO: assign a correct number of numouput to status
            status = new SPMeasurementStatusPPR(this, param,1 , spProtocol, null);


            ArrayList<String> instructionList = new ArrayList<>();

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            try {

                String instruction;
                String instructionNum;

                instruction = "WRITE INT_CONFIG S 0x61"; //  -- enable TIMER, PPR HITS_COUNTER interrupts
                instructionList.add(instruction);


                instruction = "WRITE DIG_CONFIG S 0x16"; //  -- enable TIMER and Interrput push-pull
                instructionList.add(instruction);


                instruction = "WRITE PPR_SELECT S 0x";
                instructionNum = "0000" + param.getElectrodes() + param.getDetectorType() + param.getInPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                instruction = "WRITE CHA_SELECT S 0x0F";
                instructionList.add(instruction);

                instruction = "WRITE DSS_SELECT S 0x0F";
                instructionList.add(instruction);


                instruction = "WRITE PPR_THRESL+1 S 0xFF";
                instructionList.add(instruction);

                instruction = "WRITE PPR_THRESH M 0x";
                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getHitsThreshold(), 16);
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);

                if (param.getMode().equals(SPMeasurementParameterPPR.MODE_COUNTER)){
                    instruction = "WRITE PPR_THRESL S 0x";
                    instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getEnergyThreshold(), 8);
                    instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                    instructionList.add(instruction);

                    instruction = "WRITE TIMER_DIVL+1 S 0xFF";
                    instructionList.add(instruction);

                    instruction = "WRITE TIMER_DIVH M 0x";
                    instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getTimer(), 8);
                    instructionNum += "11111111";
                    instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                    instructionList.add(instruction);

                    instruction = "WRITE ANA_CONFIG S 0xD3";
                    instructionList.add(instruction);


                } else {
                    throw new SPException("Dosimetry method in PPR not yet implemented!!");
                }

                sendSequences(instructionList, ref, "setPPR");

                if (m != null) {
                    m.out = ref.log;
                }
                lastParameter = param;

            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End setPPR", DEBUG_LEVEL);
        }






        private boolean flagTIIHigh = true;
        private boolean flagThresholdExceeded = true;

        /**
         * @param param
         * @param m
         * @throws SPException
         * @throws SPException
         * @throws SPException
         * @throws SPException
         */
        @Override
        public synchronized SPMeasurementOutputPPR[] getPPR(SPMeasurementParameterPPR param, SPMeasurementOutput m)
                throws SPException {

            if (lastParameter == null || !(lastParameter instanceof SPMeasurementParameterPPR)) {
                throw new SPException("Before getPPR must be called setPPR.");
            }

            // TODO: generare eccezione per caso con numChip > 1 (PPR non può funzionare in questo caso)

            SPMeasurementOutputPPR[] out = new SPMeasurementOutputPPR[cluster.getActiveSPChipList().size()];
            out[0] = new SPMeasurementOutputPPR(0, false);
            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());

            try {

                byte statusTIIHigh = 32;
                byte statusThresholdExceeded = 64;


                ref.log += "-----------------------------\ngetPPR:\n";

                if (flagTIIHigh || flagThresholdExceeded){

                    ref.instructionToSend = "WRITE COMMAND S 0x22";
                    sendSequence(ref);

                }

                ref.instructionToSend = "READ STATUS S 1";
                sendSequence(ref);
                byte hexValue = (byte)Integer.parseInt(ref.recValues[0].toUpperCase().replace("[", "").replace("]", "").replace("0X","").trim(), 16);
                flagTIIHigh             = (statusTIIHigh & hexValue) != 0;
                flagThresholdExceeded   = (statusThresholdExceeded & hexValue) != 0;

                if (flagTIIHigh || flagThresholdExceeded){
                    ref.instructionToSend = "READ PPR_COUNTL+1 S 1";
                    sendSequence(ref);

                    String lettura1 = ref.recValues[0];

                    ref.instructionToSend = "READ PPR_COUNTH M 2";
                    sendSequence(ref);

                    String lettura2 = ref.recValues[0];

                    out[0].setPPR_COUNT(stringToIntConversionPPR(lettura1, lettura2));

                    SPMeasurementOutput m2 = new SPMeasurementOutput();
                    int read[][] = getDATA(param, false, true, null, m2);
                    ref.log += m2.out;

                    out[0].setADC(read[0][0]);
                    out[0].setHIT_THRESHOLD_EXCEEDED(flagThresholdExceeded);
                } else {
                    out[0].setPPR_COUNT(-1);
                    out[0].setADC(0);
                    out[0].setHIT_THRESHOLD_EXCEEDED(false);
                }
                ref.log += "\nEND getPPR.\n-----------------------------\n";

                if (m != null) {
                    m.out = ref.log;
                }

            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }

            return out;
        }


        private int stringToIntConversionPPR(String lettura1, String lettura2) {

            int mask = (int) (Math.pow(2, 24) - 1);

            String byte1 = lettura1.toUpperCase().replace("[", "").replace("]", "").replace("0X","").trim();
            String appo[] = lettura2.toUpperCase().replace("[", "").replace("]", "").replace("0X","").trim().split(" ");
            String byte2 = appo[0];
            String byte3 = appo[1];

            int PPR_COUNT = Integer.parseInt(byte3, 16);
            PPR_COUNT = PPR_COUNT << 8;
            PPR_COUNT = PPR_COUNT | Integer.parseInt(byte2, 16);
            PPR_COUNT = PPR_COUNT << 8;
            PPR_COUNT = PPR_COUNT | Integer.parseInt(byte1, 16);
            PPR_COUNT = PPR_COUNT & mask; // Azzera i bit più significativi dell'intero

            return PPR_COUNT;
        }






        @Override
        public synchronized void setSENSOR(SPMeasurementParameterSENSORS param, SPMeasurementOutput m)
                throws SPException {

            SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(param.getSensorName());
            SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            Double conversionRate50 = (double)50;
            String InPortADC = SPMeasurementParameterADC.INPORT_IA;
            Integer FIFO_DEEP = param.getFIFO_DEEP();
            Integer FIFO_READ = param.getFIFO_READ();

            generateCalibrationParameters_new(param.getSensorName());



            if(spSensingElement.getMeasureTechnique().equals("EIS")){

                SPMeasurementOutput m2 = new SPMeasurementOutput();
                SPMeasurementParameterEIS paramInternal = (SPMeasurementParameterEIS)spSensingElementOnChip.getSensingElementOnFamily().getSPMeasurementParameter(spConfiguration);

                paramInternal.setSPMeasurementMetaParameter(param.getSPMeasurementMetaParameter());

                // Effettua la ricerca inversa: posseggo il valore, ritrovo la label
                setEIS(paramInternal, m2);
                ref.log += m2.out;
                m2 = new SPMeasurementOutput();
                setADC(FIFO_DEEP, FIFO_READ, paramInternal.getInGainLabel(), conversionRate50, InPortADC, true, m2);
                ref.log += m2.out;

            } else if (spSensingElement.getMeasureTechnique().equals("DIRECT")) {


                if (spSensingElement.getName().equals("ONCHIP_TEMPERATURE") || spSensingElement.getName().equals("ONCHIP_VOLTAGE")) {

                    ref.instructionToSend = "WRITE CHA_SELECT S 0xC";

                    if (spSensingElement.getName().equals("ONCHIP_TEMPERATURE")) {
                        ref.instructionToSend += "D"; // Bug run4, dovrebbe essere C
                    } else {
                        ref.instructionToSend += "C"; // Bug run4, dovrebbe essere D
                    }

                    sendSequence(ref);

                    ref.instructionToSend = "WRITE DSS_SELECT M 0x0020"; //  -- disconnect DAS from IA
                    sendSequence(ref);

                    // CHA_CONDIT 0x0300 => InGain = "1"
                    String InGain = "1";
                    ref.instructionToSend = "WRITE CHA_CONDIT M 0x0300";
                    sendSequence(ref);

                    ref.instructionToSend = "WRITE CHA_FILTER M 0xF600";   //ref.instructionToSend = "WRITE CHA_FILTER+1 S 0xFE"; da verificare con Simone: F6 o FE
                    sendSequence(ref);

                    ref.instructionToSend = "WRITE ANA_CONFIG S 0x1B";
                    sendSequence(ref);

                    SPMeasurementOutput m2 = new SPMeasurementOutput();
                    setADC(FIFO_DEEP, FIFO_READ, InGain, conversionRate50, InPortADC, true, m2);
                    ref.log += m2.out;

                    if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null) {
                        status = new SPMeasurementStatusSENSOR(this, param, 1,spProtocol, "");
                    }


                } else {
                    throw new SPException("Sensing element unavailable: " + spSensingElement.getMeasureTechnique());

                }
            } else {
                throw new SPException("Measure technique unavailable: " + spSensingElement.getMeasureTechnique());
            }



        }



        @Override
        public synchronized double[][] getSENSOR(SPMeasurementParameterSENSORS param, SPMeasurementOutput m)
                throws SPException {

            SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(param.getSensorName());
            SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();

            generateCalibrationParameters_new(param.getSensorName());
            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());

            double output[][] = null;
            //Integer NDATA = 1;

            if(spSensingElement.getMeasureTechnique().equals("EIS")){
                output = new double[cluster.getActiveSPChipList().size()][SPMeasurementParameterEIS.NUM_OUTPUT + 1];

                SPMeasurementOutput m2 = new SPMeasurementOutput();
                SPMeasurementParameterEIS paramInternal = new SPMeasurementParameterEIS(spConfiguration);
                paramInternal.setContacts(spSensingElement.getContacts());
                paramInternal.setRsense(spSensingElement.getRsense());
                paramInternal.setDCBiasP(spSensingElement.getDCBiasP());
                paramInternal.setDCBiasN(spSensingElement.getDCBiasN());
                paramInternal.setFrequency(spSensingElement.getFrequency()); // Per ottenere 78125
                paramInternal.setHarmonic(spSensingElement.getHarmonic());
                paramInternal.setOutPort(spSensingElementOnChip.getSensingElementOnFamily().getPort().getPortLabel());
                paramInternal.setInPort(spSensingElementOnChip.getSensingElementOnFamily().getPort().getPortLabel());
                paramInternal.setInGain(spSensingElement.getInGain());
                paramInternal.setOutGain(spSensingElement.getOutGain());
                paramInternal.setModeVI(spSensingElement.getModeVI());
                paramInternal.setMeasure(spSensingElement.getMeasureType());
                paramInternal.setPhaseShiftQuadrants(spSensingElement.getPhaseShiftMode(), spSensingElement.getPhaseShift(), spSensingElement.getIq());
                paramInternal.setFilter(spSensingElement.getFilter());

                paramInternal.setSPMeasurementMetaParameter(paramInternal.getSPMeasurementMetaParameter());

                double buffer[][] = getEIS(paramInternal, m2);

                for(int i = 0; i < buffer.length; i++) {
                    output[i][0] = buffer[i][SPParamItemMeasures.CAPACITANCE];
                    output[i][0] = N[i] + M[i] * output[i][0];

                    output[i][SPParamItemMeasures.IN_PHASE + 1]       = buffer[i][SPParamItemMeasures.IN_PHASE];
                    output[i][SPMeasurementParameterEIS.QUADRATURE + 1]     = buffer[i][SPMeasurementParameterEIS.QUADRATURE];
                    output[i][SPParamItemMeasures.MODULE + 1]         = buffer[i][SPParamItemMeasures.MODULE];
                    output[i][SPParamItemMeasures.PHASE + 1]          = buffer[i][SPParamItemMeasures.PHASE];
                    output[i][SPParamItemMeasures.RESISTANCE + 1]     = buffer[i][SPParamItemMeasures.RESISTANCE];
                    output[i][SPParamItemMeasures.CAPACITANCE + 1]    = buffer[i][SPParamItemMeasures.CAPACITANCE];
                    output[i][SPParamItemMeasures.INDUCTANCE + 1]     = buffer[i][SPParamItemMeasures.INDUCTANCE];
                    output[i][SPParamItemMeasures.VOLTAGE + 1]     = buffer[i][SPParamItemMeasures.VOLTAGE];
                    output[i][SPParamItemMeasures.CURRENT + 1]     = buffer[i][SPParamItemMeasures.CURRENT];
                }

                ref.log += m2.out;

            } else if (spSensingElement.getMeasureTechnique().equals("DIRECT")) {

                if (spSensingElement.getName().equals("ONCHIP_TEMPERATURE") || spSensingElement.getName().equals("ONCHIP_VOLTAGE")) {
                    SPMeasurementOutput outMsg = new SPMeasurementOutput();

                    output = new double[cluster.getActiveSPChipList().size()][1];

                    int[][] buffer = getDATA(param, true, true, null, outMsg);
                    for(int i = 0; i < numOfChips; i++){
                        output[i][0] += N[i] + (M[i] * buffer[i][0] * spConfiguration.getVREF()) / NORMALIZATION_FACTOR;
                        //System.out.println("ADC: " + buffer[i][0]);
                    }

                    status.updateStatus(output);

                    ref.log += outMsg.out;
                }
            }  else {
                throw new SPException("Measure technique unavailable: " + spSensingElement.getMeasureTechnique());
            }




            return output;
        }



        protected void initializeCalibrationParameters(){
            for(int i = 0; i < numOfChips; i++){
                M[i] = 1;
                N[i] = 0;
            }
        }


        protected void generateCalibrationParameters_new(String sensorName){
            List<SPChip> chipList = spConfiguration.getCluster().getActiveSPChipList();

            // Generate default values
            initializeCalibrationParameters();
            int i = 0;
            boolean flag = false;

            for(int j = 0; j < chipList.size(); j++){
                i = 0;
                flag = false;
                while(i < chipList.get(j).getSensingElementOnChipList().size() && !flag){
                    flag = chipList.get(j).getSensingElementOnChipList().get(i).getSensingElementOnFamily().getID().equalsIgnoreCase(sensorName);
                    if (!flag)
                        i++;
                }
                if (flag){
                    M[j] =  chipList.get(j).getSensingElementOnChipList().get(i).getCalibrationParameters().getM();
                    N[j] =  chipList.get(j).getSensingElementOnChipList().get(i).getCalibrationParameters().getN();
                } else {
                    M[j] = 1;
                    N[j] = 0;
                }
            }


        }

        /*
        private void generateCalibrationParameters(String analyte){
            List<SPChip> chipList = spConfiguration.getCluster().getActiveSPChipList();

            // Generate default values
            initializeCalibrationParameters();
            int i = 0;
            boolean flag = false;
            while(!flag && i < chipList.get(0).getSpFamily().getAnalyteList().size() ){
                flag = chipList.get(0).getSpFamily().getAnalyteList().get(i).getName().equals(analyte);
                if (!flag)
                    i++;
            }

            for(int j = 0; j < chipList.size(); j++){
                i = 0;
                flag = false;
                while(i < chipList.get(j).getSensingElementOnChipList().size() && !flag){
                    flag = chipList.get(j).getSensingElementOnChipList().get(i).getSensingElementOnFamily().getSensingElement().getName().equalsIgnoreCase(analyte);
                    if (!flag)
                        i++;
                }
                if (flag){
                    M[j] =  chipList.get(j).getSensingElementOnChipList().get(i).getCalibrationParameters().getM();
                    N[j] =  chipList.get(j).getSensingElementOnChipList().get(i).getCalibrationParameters().getN();
                } else {
                    M[j] = 1;
                    N[j] = 0;
                }
            }
        }

    */

        /**
         * @param m log messages
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized void timerTest(SPMeasurementOutput m)
                throws SPException {
            throw new SPException("Not yet implemented");

        }


        /**
         * @param param
         * @param m     log messages
         * @throws SPException if there are parameters error: missing or out of range
         */
        @Override
        public synchronized void setPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m)
                throws SPException {

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setPOT", DEBUG_LEVEL);
            ArrayList<String> instructionList = new ArrayList<>();

            if (param == null || !param.isValid()) {
                throw new SPException("Parameters problem: " + param);
            }

            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            try {


                Double conversionRate = (double)50;
                String instruction, instructionNum;

                String CES = "0";
                if (param.portIsInternal()) {
                    CES = "1";    // TODO: perfezionare confronto per individuare se porta interna o esterna
                }

                instruction = "WRITE CHA_SELECT S 0x";
                instructionNum = "01" + CES + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                instruction = "WRITE CHA_CONDIT M 0x";
                param.setInGain("1");
                instructionNum = "000000" + param.getInGain() + param.getRsense() + "000000";
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                String FSC_DAS = param.getContacts();

                instruction = "WRITE DSS_SELECT M 0x";
                instructionNum = "0000011100" + FSC_DAS + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);


                instruction = "WRITE DSS_DIVIDER M 0x0014";
                instructionList.add(instruction);

                instruction = "WRITE CHA_FILTER+1 S 0xFE";
                instructionList.add(instruction);

                sendSequences(instructionList, ref, "setPOT");

                if (m != null) {
                    m.out = ref.log;
                }

                SPMeasurementOutput mADC = new SPMeasurementOutput();

                setADC(1, 1, param.getInGainLabel(), conversionRate, SPMeasurementParameterADC.INPORT_IA, true, mADC);
                //instructionList.add(instruction);

                if (m != null) {
                    m.out += mADC.out;
                }


                instructionList = new ArrayList<>();
                instruction = "WRITE ANA_CONFIG S 0x3F";
                instructionList.add(instruction);


                if (param.getTypeVoltammetry().equals(SPParamItemPOTType.POTENTIOMETRIC)){
                    instruction = "WRITE CHA_SELECT S 0x";
                    instructionNum = "11" + CES + param.getPort();
                    instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                    instructionList.add(instruction);
                }

                sendSequences(instructionList, ref, "setPOT");

                status = new SPMeasurementStatusPOT(this, spConfiguration, param, SPParamItemPOTMeasures.NUM_OUTPUT,spProtocol, "" + logDirectory + "/Download/getPOT_dump.txt");


                if (m != null) {
                    m.out += ref.log;
                }
                lastParameter = param;

            } catch (SPException  e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End setPOT", DEBUG_LEVEL);

        }

        /**
         * @param InOutPort
         * @param Type
         * @param InitialPotential
         * @param FinalPotential
         * @param step
         * @param PulsePeriod
         * @param PulseAmplitude
         * @param alternativeSignal
         * @param rsense
         * @param inGain
         * @param message           log messages
         * @return
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized double[][] getPOT(String InOutPort, String Type, Integer InitialPotential, Integer FinalPotential,
                                            Integer step, Integer PulsePeriod, Integer PulseAmplitude, Boolean alternativeSignal, String rsense, String inGain,
                                  SPMeasurementOutput message)
                throws SPException {
            SPMeasurementParameterPOT param = new SPMeasurementParameterPOT(spConfiguration);
            param.setPort(InOutPort);
            param.setType(Type);
            param.setInitialPotential(InitialPotential);
            param.setFinalPotential(FinalPotential);
            param.setStep(step);
            param.setPulsePeriod(PulsePeriod);
            param.setPulseAmplitude(PulsePeriod);
            param.setAlternativeSignal(alternativeSignal);
            param.setInGain(inGain);
            return getPOT(param, message);
        }

        /**
         * @param param
         * @param m     log messages
         * @return
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized double[][] getPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m)
                throws SPException {

            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getPOT", DEBUG_LEVEL);

            if (param == null || !param.isValid()) {
                throw new SPException("Parameters problem: " + param);
            }

            if (lastParameter == null || !(lastParameter instanceof SPMeasurementParameterPOT) || status == null || !(status instanceof SPMeasurementStatusPOT)) {
                throw new SPException("Before must be called setSENSOR");
            }




            SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
            double output[][]=null;
            try {
                ref.log = "-----------------------------\ngetPOT:\n";
                int RUN4_TRANSLATE = 0;
                String instructionNum;
                SPMeasurementStatusPOT potStatus = (SPMeasurementStatusPOT) status;

                if (potStatus.isCycleFinished()){
                    m.resetCycle = true;
                    potStatus.resetStimulus();
                    potStatus.setCycleFinished(false);
                }

                if ((!param.getTypeVoltammetry().equals(SPParamItemPOTType.POTENTIOMETRIC) &&
                        !param.getTypeVoltammetry().equals(SPParamItemPOTType.CURRENT))) {

                    SPLogger.getLogInterface().d("GETPOT", "" + potStatus.getStimulus(), DEBUG_LEVEL);
                    ref.instructionToSend = "WRITE DAS_CONFIG M 0x";
                    instructionNum = "0000" + potStatus.getStim2Bit();
                    ref.instructionToSend = SPMeasurementUtil.fromBitToHexWithConcat(ref.instructionToSend, instructionNum);
                    sendSequence(ref);
                }

                potStatus.readingDelay();

                SPMeasurementOutput m2 = new SPMeasurementOutput();

                int numberOfMeasure = param.isBurstMode() ? Integer.parseInt(param.getFilter()) : 1;
                potStatus.resetBufferStatus();
                for(int i = 0; i < numberOfMeasure; i++){
                    output = new double[cluster.getActiveSPChipList().size()][2];
                    int read[][] = getDATA(param, true, true, null, m2);
                    ref.log += m2.out;
                    for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                        output[k][0] = potStatus.getStimulus();    // Tensione
                        output[k][1] = potStatus.normalizeVoltage(read[k][0]);  // Corrente (in micro Ampere)

                    }
                    potStatus.updateStatus(output);
                }




                //m.resetCycle =  potStatus.getRestartStimulusCounter()==2;

                /*
                potStatus.updateLog(output, read);
                potStatus.updateStimulus();*/


                if(param.getTypeVoltammetry().equals(SPParamItemPOTType.DIFFERENTIAL_PULSE_VOLTAMMETRY)){

                    potStatus.updateStimulus();

                    if ((!param.getTypeVoltammetry().equals(SPParamItemPOTType.POTENTIOMETRIC) &&
                            !param.getTypeVoltammetry().equals(SPParamItemPOTType.CURRENT))) {

                        SPLogger.getLogInterface().d("GETPOT", "" + potStatus.getStimulus(), DEBUG_LEVEL);
                        ref.instructionToSend = "WRITE DAS_CONFIG M 0x";
                        instructionNum = "0000" + potStatus.getStim2Bit();
                        ref.instructionToSend = SPMeasurementUtil.fromBitToHexWithConcat(ref.instructionToSend, instructionNum);

                        sendSequence(ref);
                    }

                    potStatus.readingDelay();

                    potStatus.resetBufferStatus();
                    double output2[][] = null;
                    for(int i = 0; i < numberOfMeasure; i++){
                        output2 = new double[cluster.getActiveSPChipList().size()][2];
                        SPMeasurementOutput m3 = new SPMeasurementOutput();
                        int read2[][] = getDATA(param, true, true, null, m3);
                        ref.log += m3.out;
                        for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                            output2[k][0] = potStatus.getStimulus();    // Tensione
                            output2[k][1] = potStatus.normalizeVoltage(read2[k][0]);  // Corrente (in micro Ampere)
                        }
                        potStatus.updateStatus(output2);
                    }


                    for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                        //output[k][0] = potStatus.getStimulus();    // Tensione
                        if(potStatus.getUp()) {

                            output[k][1] = output2[k][1]-output[k][1];  // Corrente (in micro Ampere)

                        }
                        else {
                            output[k][1] = output[k][1]-output2[k][1];
                            //output[k][1] = potStatus.normalizeVoltage(read[k][0]-read2[k][0]);  // Corrente (in micro Ampere)
                        }

                    }

                    /*SPMeasurementOutput m3 = new SPMeasurementOutput();
                    int read2[][] = getDATA(param, true, true, null, m3);
                    ref.log += m3.out;*/


                    /*for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){

                        if(potStatus.getUp()) {
                            output[k][0] = potStatus.getStimulus();    // Tensione
                            output[k][1] = potStatus.normalizeVoltage(read2[k][0]-read[k][0]);  // Corrente (in micro Ampere)

                        }
                        else {
                            //output[k][0] = potStatus.getStimulus();    // Tensione
                            output[k][1] = potStatus.normalizeVoltage(read[k][0]-read2[k][0]);  // Corrente (in micro Ampere)
                        }

                    }*/


                }else{
                    /*for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                        output[k][0] = potStatus.getStimulus();    // Tensione
                        output[k][1] = potStatus.normalizeVoltage(read[k][0]);  // Corrente (in micro Ampere)

                    }*/


                    //m.resetCycle =  potStatus.getRestartStimulusCounter()==2;

                    //potStatus.updateLog(output, read);
                }

                if(m!=null){
                    m.potUP=((SPMeasurementStatusPOT) status).getUp();
                }
                //potStatus.updateLog(output, read);
                potStatus.updateStimulus();




                ref.log += "END getPot.\n-----------------------------\n";

                if (m != null) {
                    m.out = ref.log;
                }

            } catch (  SPException e) {
                throw e;
            } catch (Exception e) {
                ref.exceptionMessage += e.getMessage() + "\n";
                throw new SPException(ref.exceptionMessage);
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End setPOT", DEBUG_LEVEL);

            return output;
        }




        /**
         * @param ModeTimer
         * @param Timer
         * @param message   log messages
         * @throws SPException if there are parameters error: missing or out of range
         * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
         * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
         * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
         */
        @Override
        public synchronized void setTimer(String ModeTimer, Integer Timer, SPMeasurementOutput message)
                throws SPException {
            throw new SPException("Not yet implemented");
        }

        public synchronized void autoRange(SPMeasurementParameterEIS paramEIS) throws SPException {
            //autorangev2
            //autoRangeAlgorithm.autoRange(paramEIS);

            //autorangev1
            autoRangeAlgorithm.autoRange(paramEIS,spConfiguration.getVREF());
        }

        public synchronized void setACTUATOR(SPMeasurementParameterActuator parameterActuator) throws SPException{
            throw new SPException("setACTUATOR not implemented on RUN4");
        }

        public synchronized void setACTUATOR(SPMeasurementParameterActuator parameterActuator, SPCluster temporaryCluster) throws SPException{
            throw new SPException("setACTUATOR not implemented on RUN4");
        }


        /************************************************************************************************************************************************************************
         *
         * UTILITY PRIVATE SECTION
         *
         ************************************************************************************************************************************************************************/

        protected static int bitSignDCBias(int x, int numBit) {
            int sign = (int) Math.pow(2, numBit - 1);
            sign = x < 0 ? sign : 0;
            x = Math.abs(x);
            return x | sign;
        }





    }
