package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemPortADC extends SPParamItem {

    public static final String INPORT_IA = "IA";
    public static final String INPORT_MUX = "MUX";
    public static final String INPORT_DAC = "DAC";
    public static final String INPORT_PPR = "PPR";

    private String InPortADC = null;
    public static final String[] inPortADCLabelsRUN5 = new String[]{INPORT_IA, INPORT_MUX, INPORT_DAC, INPORT_PPR};
    public static final String[] inPortADCValuesRUN5 = new String[]{"00", "01", "10", "11"};

    public static final String[] inPortADCLabelsRUN6 = new String[]{INPORT_IA, INPORT_MUX, INPORT_DAC, INPORT_PPR};
    public static final String[] inPortADCValuesRUN6 = new String[]{"00", "01", "10", "11"};


    public SPParamItemPortADC(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            initialize(inPortADCLabelsRUN5, inPortADCValuesRUN5);
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            initialize(inPortADCLabelsRUN6, inPortADCValuesRUN6);
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }

    }
}
