package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemPOTMeasures extends SPParamItem {

    public final static int VOLTAGE = 0;
    public final static int CURRENT = 1;
    public final static int CURRENT_VS_VOLTAGE = 2;
    public final static int DERIVATIVE_CURRENT_VS_VOLTAGE = 3;

    public final static int NUM_OUTPUT = 2;

    /*public final static int VOLTAGE_1 = 0;
    public final static int CURRENT_1 = 1;
    public final static int VOLTAGE_2 = 2;
    public final static int CURRENT_2 = 3;
    public final static int DELTA_I = 4;
    public final static int DELTA_T = 5;*/




    private Integer measure = null;
    protected String measureLabel = null;

    public static final String[] measureLabelsRUN5 = new String[]{"VOLTAGE", "CURRENT","CURRENT_VS_VOLTAGE","DERIVATIVE_CURRENT_VS_VOLTAGE"};
    public static final Integer[] measureValuesRUN5 = new Integer[]{VOLTAGE, CURRENT,CURRENT_VS_VOLTAGE,DERIVATIVE_CURRENT_VS_VOLTAGE};


    public static final String[] measureLabelsRUN6 = new String[]{"VOLTAGE", "CURRENT","CURRENT_VS_VOLTAGE","DERIVATIVE_CURRENT_VS_VOLTAGE"};
    public static final Integer[] measureValuesRUN6 = new Integer[]{VOLTAGE, CURRENT,CURRENT_VS_VOLTAGE,DERIVATIVE_CURRENT_VS_VOLTAGE};

    public static final String[] unitMeasuresLabelsRUN5 = new String[]{"V","A","A","A/V"};
    public static final String[] unitMeasuresLabelsRUN6 = new String[]{"V","A","A","A/V"};
    public static String[] unitMeasuresLabels;



    public static String[] measureLabels = null;
    public static Integer[] measureValues = null;

    public SPParamItemPOTMeasures(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            measureLabels = measureLabelsRUN5;
            measureValues = measureValuesRUN5;
            unitMeasuresLabels = unitMeasuresLabelsRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            measureLabels = measureLabelsRUN6;
            measureValues = measureValuesRUN6;
            unitMeasuresLabels = unitMeasuresLabelsRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(measureLabels, measureValues);

    }



}
