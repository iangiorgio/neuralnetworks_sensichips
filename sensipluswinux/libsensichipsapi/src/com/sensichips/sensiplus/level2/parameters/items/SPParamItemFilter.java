package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemFilter extends SPParamItem {
    //private String Filter;
    //protected String FilterLabel;
    public static final String[] filterLabelsRUN5 = new String[]{"1", "4", "8", "16", "32", "64", "128", "256","1000","10000"};
    public static final String[] filterValuesRUN5 = new String[]{"1", "4", "8", "16", "32", "64", "128", "256","1000","10000"};

    public static final String[] filterLabelsRUN6 = new String[]{"1", "4", "8", "16", "32", "64", "128", "256","1000","10000"};
    public static final String[] filterValuesRUN6 = new String[]{"1", "4", "8", "16", "32", "64", "128", "256","1000","10000"};


    public static String[] filterLabels = filterLabelsRUN5;
    public static String[] filterValues = filterValuesRUN5;

    public SPParamItemFilter(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            filterLabels = filterLabelsRUN5;
            filterValues = filterValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            filterLabels = filterLabelsRUN6;
            filterValues = filterValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(filterLabels, filterValues);

    }


}
