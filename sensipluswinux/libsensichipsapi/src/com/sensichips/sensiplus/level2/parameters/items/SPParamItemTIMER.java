package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemTIMER extends SPParamItem {
    private static final String[] modetimerLabelsRUN5 = new String[]{"MODE_TIMER", "HITS_TIMER", "SCHEDULED_CONVERSION"};
    private static final String[] modetimerValuesRUN5 = new String[]{"MODE_TIMER", "HITS_TIMER", "SCHEDULED_CONVERSION"};

    private static final String[] modetimerLabelsRUN6 = new String[]{"MODE_TIMER", "HITS_TIMER", "SCHEDULED_CONVERSION"};
    private static final String[] modetimerValuesRUN6 = new String[]{"MODE_TIMER", "HITS_TIMER", "SCHEDULED_CONVERSION"};


    public SPParamItemTIMER(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            initialize(modetimerLabelsRUN5, modetimerValuesRUN5);
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            initialize(modetimerLabelsRUN6, modetimerValuesRUN6);
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }

    }

}
