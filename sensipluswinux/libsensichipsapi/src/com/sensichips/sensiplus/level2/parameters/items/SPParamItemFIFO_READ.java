package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemFIFO_READ extends SPParamItem {
    private static Number minRUN5 = 1;
    private static Number maxRUN5 = 16;

    private static Number minRUN6 = 1;
    private static Number maxRUN6 = 16;

    public static Number min = 0;
    public static Number max = 0;

    public static String[] ContactsLabels = null;
    public static String[] ContactsValues = null;
    public SPParamItemFIFO_READ(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            min = minRUN5;
            max = maxRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            min = minRUN6;
            max = maxRUN6;
        }

        initialize(min, max);

    }

}
