package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemQI extends SPParamItem {
    //private String Q_I = null;
    //protected String Q_ILabel = null;
    public static final String[] I_QLabelsRUN5 = new String[]{"IN_PHASE", "QUADRATURE", "ANTI_PHASE", "ANT_QUADRATURE"};
    public static final String[] I_QValuesRUN5 = new String[]{"00", "01", "10", "11"};

    public static final String[] I_QLabelsRUN6 = new String[]{"IN_PHASE", "QUADRATURE", "ANTI_PHASE", "ANT_QUADRATURE"};
    public static final String[] I_QValuesRUN6 = new String[]{"00", "01", "10", "11"};


    public static String[] I_QLabels = null;
    public static String[] I_QValues = null;

    public SPParamItemQI(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            I_QLabels = I_QLabelsRUN5;
            I_QValues = I_QValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            I_QLabels = I_QLabelsRUN6;
            I_QValues = I_QValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(I_QLabels, I_QValues);
    }
}
