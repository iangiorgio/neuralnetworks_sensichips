package com.sensichips.sensiplus.level2.parameters;


import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level2.parameters.items.*;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterPOT extends SPMeasurementParameterADC {



    //private SPConfiguration conf;

    private Integer initialPotential;
    private Integer finalPotential;
    private Integer step;
    private Integer pulsePeriod;
    private Integer pulseAmplitude;
    //private Integer typeVoltammetry;
    private Boolean alternativeSignal = true;
    //private String rsense;
    //private String InGain;

    //private String Contacts = null;
    //protected String ContactsLabel = null;
    //public static final String[] ContactsLabels = new String[]{"TWO", "THREE"};
    //public static final String[] ContactsValues = new String[]{"1", "0"};

    /*public final static int POTENTIOMETRIC = 0;
    public final static int CURRENT = 1;
    public final static int LINEAR_SWEEP_VOLTAMMETRY = 2;
    public final static int STAIRCASE_VOLTAMMETRY = 3;
    public final static int SQUAREWAVE_VOLTAMMETRY = 4;
    public final static int NORMAL_PULSE_VOLTAMMETRY = 5;
    public final static int DIFFERENTIAL_PULSE_VOLTAMMETRY = 6;

    public static final String[] typeLabels = new String[]{"LINEAR_SWEEP", "STAIRCASE",
            "SQUAREWAVE", "NORMAL_PULSE", "DIFFERENTIAL_PULSE", "POTENTIOMETRIC", "CURRENT"};
    public static final Integer[] typeValues = new Integer[]{LINEAR_SWEEP_VOLTAMMETRY, STAIRCASE_VOLTAMMETRY, SQUAREWAVE_VOLTAMMETRY,
            NORMAL_PULSE_VOLTAMMETRY, DIFFERENTIAL_PULSE_VOLTAMMETRY, POTENTIOMETRIC, CURRENT};


    public static final String[] rsenseLabels = new String[]{"50000", "5000", "500", "50"};
    public static final String[] rsenseValues = new String[]{"00", "01", "10", "11"};

    public static final String[] ingainLabels = new String[]{"1", "12", "20", "40"};
    public static final String[] ingainValues = new String[]{"11", "10", "01", "00"};
*/



    // PAY ATTENTION
    //if gcVCC>=1.8V then VREF- = -650,  VREF+ = +650
    //if gcVCC>=2.5V then VREF- = -1000, VREF+ = +1000
    //if gcVCC>=3.3V then VREF- = -1400, VREF+ = +1400



    public static Integer Vref_minus;
    public static Integer Vref_plus;



    public static Integer initialPotentialMin = Vref_minus;
    public static Integer initialPotentialMax = Vref_plus;

    public static Integer finalPotentialMin = Vref_minus;
    public static Integer finalPotentialMax = Vref_plus;

    public static Integer stepMin = Vref_minus;
    public static Integer stepMax = Vref_plus;

    public static Integer pulsePeriodMin = 0;
    public static Integer pulsePeriodMax = 60000;

    public static Integer pulseAmplitudeMin = Vref_minus;
    public static Integer pulseAmplitudeMax = Vref_plus;



    private SPParamItemPOTContacts spParamItemPOTContacts;
    private SPParamItemPOTType spParamItemPOTType;
    private SPParamItemRSense spParamItemRSense;
    //private SPParamItemInGain spParamItemInGain;
    //private SPParamItemContacts spParamItemContacts;

    public SPMeasurementParameterPOT(SPConfiguration spConfiguration) throws SPException {
        super(spConfiguration);

        if (spConfiguration.getVCC() >= SPConfiguration.VCC_Th1
                && spConfiguration.getVCC() < SPConfiguration.VCC_Th2) {
            Vref_plus = (SPConfiguration.VREF_1)/2;
        } else if (spConfiguration.getVCC() >= SPConfiguration.VCC_Th2 && spConfiguration.getVCC() < SPConfiguration.VCC_Th3) {
            Vref_plus = (SPConfiguration.VREF_2)/2;
        } else if (spConfiguration.getVCC() >= SPConfiguration.VCC_Th3) {
            Vref_plus = (SPConfiguration.VREF_3)/2;
        }
        Vref_minus = -Vref_plus;

        initialPotentialMin = Vref_minus;
        initialPotentialMax = Vref_plus;

        finalPotentialMin = Vref_minus;
        finalPotentialMax = Vref_plus;

        stepMin = Vref_minus;
        stepMax = Vref_plus;

        //pulsePeriodMin = Vref_minus;
        //pulsePeriodMax = Vref_plus;

        pulseAmplitudeMin = Vref_minus;
        pulseAmplitudeMax = Vref_plus;
        SPFamily spFamily = spConfiguration.getCluster().getFamilyOfChips();
        spParamItemPOTContacts  = new SPParamItemPOTContacts(spFamily);
        spParamItemPOTType      = new SPParamItemPOTType(spFamily);
        spParamItemRSense       = new SPParamItemRSense(spFamily);
        //spParamItemInGain       = new SPParamItemInGain(spFamily);
        setFIFO_DEEP(1);
        setFIFO_READ(1);

        
    }


    public SPMeasurementParameterPOT(SPConfiguration spConfiguration, String InOutPort, String TypeVoltammetry, Integer InitialPotential, Integer FinalPotential,
                                     Integer Step, Integer PulsePeriod, Integer PulseAmplitude, Boolean AlternativeSignal,
                                     String RSense, String InGain, String contacts) throws SPException {
        this(spConfiguration);

        setType(TypeVoltammetry);
        setFinalPotential(FinalPotential);
        setInitialPotential(InitialPotential);
        setStep(Step);
        setPulsePeriod(PulsePeriod);
        setPulseAmplitude(PulseAmplitude);
        setRsense(RSense);
        setInGain(InGain);
        setAlternativeSignal(AlternativeSignal);
        setPort(InOutPort);
        setContacts(contacts);
    }

    public boolean isValid() {
        boolean valid = false;
        try{
            valid = !(getInitialPotential() == null
                || getFinalPotential() == null
                || getStep() == null
                || getPulsePeriod() == null
                || getPulseAmplitude() == null
                || getTypeVoltammetry() == null && getAlternativeSignal() == null
                    && getRsense() == null && getInGain() == null)
                || getContacts() == null;
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return valid;

    }

    public static int getMeasureTypeIndex(String measureLabel) throws SPException{
        int i=0;


        if(measureLabel.equals("")){
            return 0;
        } else {
            SPParamItemPOTMeasures spParamItemMeasures =
                    new SPParamItemPOTMeasures(SPConfigurationManager.getSPConfigurationDefault().getCluster().getFamilyOfChips());
            return spParamItemMeasures.getIndexFromLabel(measureLabel);
        }
    }

    public boolean equals(SPMeasurementParameterPOT param) {
        boolean output = false;
        try {
            output = param.getTypeVoltammetry().equals(getTypeVoltammetry())
                    && param.getFinalPotential().equals(finalPotential)
                    && param.getInitialPotential().equals(initialPotential) && param.getStep().equals(step)
                    && param.getPulsePeriod().equals(pulsePeriod) && param.getPulseAmplitude().equals(pulseAmplitude)
                    && param.getPort().equals(getPort()) && param.getAlternativeSignal() == getAlternativeSignal()
                    && param.getRsense().equals(getRsense()) && param.getInGain().equals(getInGain())
                    && param.getContacts().equals(getContacts());
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }


    public String toString() {
        String output = "";
        try{
            output = "POT parameters:\n"
                + "\nType: " + getTypeVoltammetry()
                + "\nPort: " + getPort()
                + "\nInitialPotential: " + getInitialPotential()
                + "\nFinalPotential: " + getFinalPotential()
                + "\nStep: " + getStep()
                + "\nPulseAmplitude: " + getPulseAmplitude()
                + "\nalternativeSignal: " + getAlternativeSignal()
                + "\nRsense: " + getRsense()
                + "\nInGain: " + getInGain()
                + "\nContacts: " + getContacts();
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }


    public String getHeaderAsString(){
        String output = "";
        output += "Type;";
        output += "Port;";
        output += "InitialPotential;";
        output += "FinalPotential;";
        output += "Step;";
        output += "PulseAmplitude;";
        output += "AlternativeSignal;";
        output += "InGain;";
        output += "Rsense;";
        output += "Contacts;";
        return output;
    }

    public String getLabelsAsString(){
        String output = "";
        try {

            output += getTypeVoltammetry() + ";";
            output += getPort() + ";";
            output += getInitialPotential() + ";";
            output += getFinalPotential() + ";";
            output += getStep() + ";";
            output += getPulseAmplitude() + ";";
            output += getAlternativeSignal() + ";";
            output += getInGain() + ";";
            output += getRsense() + ";";
            output += getContacts() + ";";

        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }


    public void setContacts(String contacts) throws SPException {
        spParamItemPOTContacts.setLabel(contacts);
        //this.ContactsLabel = contacts;
        //Contacts = (String) searchValue(ContactsLabels, ContactsValues, contacts, "Contacts");
    }

    public String getContacts() throws SPException {
        return (String) spParamItemPOTContacts.getValue();
    }


    /*public void setInGain(String ingain) throws SPException {
        //InGain = (String) searchValue(ingainLabels, ingainValues, ingain, "InGain");
        spParamItemInGain.setLabel(ingain);
    }*/

    /*public String getInGain() throws SPException {
        return (String) spParamItemInGain.getValue();
    }*/

    public Boolean getAlternativeSignal() {
        return alternativeSignal;
    }


    public void setAlternativeSignal(Boolean alternative) {
        this.alternativeSignal = alternative;
    }


    public Integer getInitialPotential() {
        return initialPotential;
    }


    public void setInitialPotential(Integer initialPotential) throws SPException {
        if (initialPotential != null && inRange((int)initialPotentialMin, (int)initialPotentialMax, initialPotential))
            this.initialPotential = initialPotential;
        else
            throw new SPException("initialPotential: " + initialPotential + " out of range: [" + initialPotentialMin + ", " + initialPotentialMax + "]");
    }


    public Integer getFinalPotential() {
        return finalPotential;
    }


    public void setFinalPotential(Integer finalPotential) throws SPException {
        if (finalPotential != null && inRange((int)finalPotentialMin, (int)finalPotentialMax, finalPotential))
            this.finalPotential = finalPotential;
        else
            throw new SPException("finalPotential: " + finalPotential + " out of range: [" + finalPotentialMin + ", " + finalPotentialMax + "]");
    }


    public Integer getStep() {
        return step;
    }


    public void setStep(Integer step) throws SPException {
        if (step != null && inRange((int)stepMin, (int)stepMax, step))
            this.step = step;
        else
            throw new SPException("step " + step + " out of range: [" + stepMin + ", " + stepMax + "]");
    }


    public Integer getPulsePeriod() {
        return pulsePeriod;
    }


    public void setPulsePeriod(Integer pulsePeriod) throws SPException {
        if (pulsePeriod != null && inRange((int)pulsePeriodMin, (int)pulsePeriodMax, pulsePeriod))
            this.pulsePeriod = pulsePeriod;
        else
            throw new SPException("pulsePeriod: " + pulsePeriod + " out of range: [" + pulsePeriodMin + ", " + pulsePeriodMax + "]");
    }


    public Integer getPulseAmplitude() {
        return pulseAmplitude;
    }


    public void setPulseAmplitude(Integer pulseAmplitude) throws SPException {
        if (pulseAmplitude != null && inRange((int)pulseAmplitudeMin, (int)pulseAmplitudeMax, pulseAmplitude))
            this.pulseAmplitude = pulseAmplitude;
        else
            throw new SPException("pulseAmplitude: " + pulseAmplitude + " out of range: [" + pulseAmplitudeMin + ", " + pulseAmplitudeMax + "]");
    }


    public Integer getTypeVoltammetry() throws SPException {
        return (Integer) spParamItemPOTType.getValue();
    }


    public void setType(String type) throws SPException {
        //this.typeVoltammetry = (Integer) searchValue(typeLabels, typeValues, type, "type");
        spParamItemPOTType.setLabel(type);
    }


    public String getRsense() throws  SPException {
        return (String) spParamItemRSense.getValue();
    }

    public void setRsense(String rsense) throws SPException {
        spParamItemRSense.setLabel(rsense);
        //this.rsense = (String) searchValue(rsenseLabels, rsenseValues, rsense, "Rsense");
    }


}
