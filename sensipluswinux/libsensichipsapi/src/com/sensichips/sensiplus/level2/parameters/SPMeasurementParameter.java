package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemFIFO_DEEP;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemFIFO_READ;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameter implements Serializable {

    public static final long serialVersionUID = 42L;

    protected SPCluster temporaryCluster;
    protected boolean sequentialMode = false;
    protected SPParamItemFIFO_READ spParamItemFIFO_READ = null;
    protected SPParamItemFIFO_DEEP spParamItemFIFO_DEEP = null;

    //public static int final DIAGNOSYS_LEVEL
    private SPMeasurementMetaParameter metaParameter;

    private SPParamItemFilter spParamItemFilter;


    private boolean burstMode = false;

    private boolean fillBufferBeforeStart = false;


    public SPMeasurementParameter(SPConfiguration spConfiguration) throws SPException{
        super();
        spParamItemFilter       = new SPParamItemFilter(spConfiguration.getCluster().getFamilyOfChips());
        this.metaParameter = new SPMeasurementMetaParameter(spConfiguration.getCluster().getActiveSPChipList().size());
        spParamItemFIFO_READ = new SPParamItemFIFO_READ(spConfiguration.getCluster().getFamilyOfChips());
        spParamItemFIFO_DEEP = new SPParamItemFIFO_DEEP(spConfiguration.getCluster().getFamilyOfChips());
    }

    public SPMeasurementParameter(){
        this.metaParameter = new SPMeasurementMetaParameter();
        sequentialMode = false;
    }

    public boolean isFillBufferBeforeStart() {
        return fillBufferBeforeStart;
    }

    public void setFillBufferBeforeStart(boolean fillBufferBeforeStart) {
        this.fillBufferBeforeStart = fillBufferBeforeStart;
    }


    public void setFilter(String filter) throws SPException {
        //this.FilterLabel = filter;
        //this.Filter = (String) searchValue(filterLabels, filterValues, filter, "Filter");
        spParamItemFilter.setLabel(filter);
    }
    public String getFilter() throws SPException {
        return (String) spParamItemFilter.getValue();
    }
    public String getFilterLabel() throws SPException {
        return spParamItemFilter.getLabel();
    }


    public boolean isBurstMode() {
        return burstMode;
    }

    public void setBurstMode(boolean burstMode) {
        this.burstMode = burstMode;
    }


    public void setFIFO_DEEP(int NDATA) throws SPException{
        spParamItemFIFO_DEEP.setNumericalValue((Number)NDATA);
    }

    public int getFIFO_DEEP() throws SPException {
        return (int) spParamItemFIFO_DEEP.getNumericalValue();
    }

    public void setFIFO_READ(int NDATA) throws SPException{
        spParamItemFIFO_READ.setNumericalValue((Number)NDATA);
    }

    public int getFIFO_READ() throws SPException {
        return (int) spParamItemFIFO_READ.getNumericalValue();
    }


    public String[] getRegistersList() {
        return registersList;
    }

    public void setRegistersList(String[] registersList) {
        this.registersList = registersList;
    }

    private String[] registersList = new String[0];

    public boolean isSequentialMode() {
        return sequentialMode;
    }

    public void setSequentialMode(boolean sequentialMode) {
        this.sequentialMode = sequentialMode;
    }

    public SPMeasurementMetaParameter getSPMeasurementMetaParameter(){
        return metaParameter;
    }

    public void setSPMeasurementMetaParameter(SPMeasurementMetaParameter metaParameter){
        this.metaParameter = metaParameter;
    }

    public static Object searchValue(String keys[], Object values[], String label, String message) throws SPException {

        if (label == null || keys == null || values == null) {
            throw new SPException((message == null ? "" : message) + ": Null values are refused!");
        }
        if (keys.length != values.length) {
            throw new SPException((message == null ? "" : message) + ": labels.length should be equal to values.length!");
        }


        int count = 0;
        while (count < keys.length) {
            if (label.equalsIgnoreCase(keys[count])) {
                return values[count];
            }
            count++;
        }
        throw new SPException(message + " " + label + " is not in: " + Arrays.toString(keys));
    }


    public SPCluster getTemporaryCluster() {
        return temporaryCluster;
    }

    public void setTemporaryCluster(SPCluster temporaryCluster) {
        this.temporaryCluster = temporaryCluster;
    }

    protected static boolean inRange(int min, int max, Integer value) {
        return value >= min && value <= max;
    }

    protected static boolean inRange(double min, double max, double value) {
        return value >= min && value <= max;
    }

    public void reduceADCRange() throws SPException  {
        throw new SPException("The reduceADCRange method can't be called on the SPMeasurementParameter class ");
    }

    public void increaseADCRange() throws SPException  {
        throw new SPException("The increaseADCRange method can't be called on the SPMeasurementParameter class ");
    }




}

