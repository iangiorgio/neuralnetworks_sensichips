package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterULTRASOUND extends SPMeasurementParameterQI {

    //public final static int IN_PHASE = 0;
    //public final static int QUADRATURE = 1;
    public final static int NUM_OUTPUT = 4;

    public final static int IN_PHASE = 0;
    public final static int QUADRATURE = 1;
    public final static int MODULE = 2;
    public final static int PHASE = 3;

    public static final String[] measureLabels = new String[]{"IN-PHASE", "QUADRATURE", "MODULE", "PHASE"};
    public static final Integer[] measureValues = new Integer[]{IN_PHASE, QUADRATURE, MODULE, PHASE};

    public static final Integer pulseRepetitionMin = 0;
    public static final Integer pulseRepetitionMax = 4095;

    public static final Integer burstMin = 0;
    public static final Integer burstMax = 15;

    public static final Integer tx_StartMin = 0;
    public static final Integer tx_StartMax = 4095;

    public static final Integer probeMin = 0;
    public static final Integer probeMax = 255;

    public static final Integer rx_StartMin = 0;
    public static final Integer rx_StartMax = 16777215;

    private Integer Pulse_Repetition = null;
    private Integer Burst = null;
    private Integer Tx_Start = null;
    private Boolean PI_Toggle = null;


    // Meacurement
    private Integer Probe = null;
    public Integer RX_Start = null;
    private Boolean QI_Toggle = null;

    private Integer measure = null;


    public SPMeasurementParameterULTRASOUND(SPConfiguration spConfiguration) throws SPException {
        super(spConfiguration);
    }


    public boolean equals(SPMeasurementParameterULTRASOUND param) {
        boolean output = false;
        try{
            output = param.getInGain().equalsIgnoreCase(getInGain())
                && param.getModeVI().equalsIgnoreCase(getModeVI())
                && param.getFrequency().equals(getFrequency())
                && param.getPulse_Repetition().equals(Pulse_Repetition)
                && param.getOutGain().equals(getOutGain())
                && param.getInPort().equals(getInPort())
                && param.getOutPort().equals(getOutPort())
                && param.getRsense().equalsIgnoreCase(getRsense())
                && param.getBurst().equals(Burst)
                && param.getTx_Start().equals(Tx_Start)
                && param.getPI_Toggle().equals(PI_Toggle)
                && param.getQI_Toggle().equals(QI_Toggle)
                && param.getProbe().equals(Probe)
                && param.getRX_Start().equals(RX_Start)
                && param.getHarmonic().equals(getHarmonic())
                && param.getMeasure().equals(measure)
                && param.getFilter().equals(getFilter());
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }

    public String toString() {
        String output = "";
        try{
            output = "InGain: " + getInGain()
                + ", Pulse_Repetition: " + getPulse_Repetition()
                + ", ModeVI: " + getModeVI()
                + ", Frequency: " + getFrequency()
                + ", Burst: " + getBurst()
                + ", OutGain: " + getOutGain()
                + ", InPort: " + getInPort()
                + ", OutPort: " + getOutPort()
                + ", TX_Start: " + getTx_Start()
                + ", Rsense: " + getRsense()
                + ", PI_Toggle: " + getPI_Toggle()
                + ", QI_Toggle: " + getQI_Toggle()
                + ", Probe: " + getProbe()
                + ", RX_Start: " + getRX_Start()
                + ", Harmonic: " + getHarmonic()
                + ", Filter: " + getFilter()
                + ", Measure: " + measure;
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }

    public boolean isValid() {
        boolean output = false;
        try{
            output = !(getFrequency() == null
                || getModeVI() == null
                || getOutGain() == null
                || getInGain() == null
                || getOutPort() == null
                || getInPort() == null
                || getRsense() == null
                || getPulse_Repetition() == null
                || getBurst() == null
                || getTx_Start() == null
                || getPI_Toggle() == null
                || getQI_Toggle() == null
                || getMeasure() == null
                || getProbe() == null
                || getHarmonic() == null
                || getFilter() == null
                || getRX_Start() == null);
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }

    public void setMeasure(String measure) throws SPException {
        this.measure = (Integer) searchValue(measureLabels, measureValues, measure, "Measure");
    }

    public Integer getMeasure() {
        return measure;
    }


    public void setPulse_Repetition(Integer Pulse_Repetition) throws SPException {
        if (Pulse_Repetition != null && inRange((int)pulseRepetitionMin, (int)pulseRepetitionMax, Pulse_Repetition))
            this.Pulse_Repetition = Pulse_Repetition;
        else
            throw new SPException("Pulse_Repetition " + Pulse_Repetition + " out of range: [" + pulseRepetitionMin + ", " + pulseRepetitionMax + "]");
    }

    public Integer getPulse_Repetition() {
        return Pulse_Repetition;
    }

    public void setBurst(Integer Burst) throws SPException {
        if (Pulse_Repetition != null && inRange((int)burstMin, (int)burstMax, Burst))
            this.Burst = Burst;
        else
            throw new SPException("Burst " + Burst + " out of range: [" + burstMin + ", " + burstMax + "]");
    }

    public Integer getBurst() {
        return Burst;
    }

    public void setTx_Start(Integer Tx_Start) throws SPException {
        if (Tx_Start != null && inRange((int)tx_StartMin, (int)tx_StartMax, Tx_Start))
            this.Tx_Start = Tx_Start;
        else
            throw new SPException("Tx_Start " + Tx_Start + " out of range: [" + tx_StartMin + ", " + tx_StartMax + "]");
    }

    public Integer getTx_Start() {
        return Tx_Start;
    }


    public void setRX_Start(Integer RX_Start) throws SPException {
        if (RX_Start != null && inRange((int)rx_StartMin, (int)rx_StartMax, RX_Start))
            this.RX_Start = RX_Start;
        else
            throw new SPException("RX_Start " + RX_Start + " out of range: [" + rx_StartMin + ", " + rx_StartMax + "]");
    }

    public Integer getRX_Start() {
        return this.RX_Start;
    }


    public void setPI_Toggle(Boolean PI_Toggle) {
        this.PI_Toggle = PI_Toggle;
    }

    public Boolean getPI_Toggle() {
        return PI_Toggle;
    }


    public void setQI_Toggle(Boolean QI_Toggle) {
        this.QI_Toggle = QI_Toggle;
    }

    public Boolean getQI_Toggle() {
        return QI_Toggle;
    }

    public void setProbe(Integer Probe) throws SPException {
        if (Probe != null && inRange((int)probeMin, (int)probeMax, Probe))
            this.Probe = Probe;
        else
            throw new SPException("Probe " + Probe + " out of range: [" + probeMin + ", " + probeMax + "]");

    }

    public Integer getProbe() {
        return Probe;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        double OSM;
        OSM = (double) Integer.parseInt("111", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("110", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("101", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("100", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("011", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("010", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("001", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("000", 2);
        System.out.println("OSM: " + OSM);
    }

}
