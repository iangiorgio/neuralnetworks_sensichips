package com.sensichips.sensiplus.level2.parameters;

/**
 * Created by mario on 02/07/16.
 */
public class SPMeasurementOutputPPR extends SPMeasurementOutput {
    private Integer PPR_COUNT;
    private Boolean HIT_THRESHOLD_EXCEEDED;
    private int ADC;


    public SPMeasurementOutputPPR(Integer PPR_COUNT, Boolean HIT_THRESHOLD_EXCEEDED) {
        this.PPR_COUNT = PPR_COUNT;
        this.HIT_THRESHOLD_EXCEEDED = HIT_THRESHOLD_EXCEEDED;
    }

    public Boolean getHIT_THRESHOLD_EXCEEDED() {
        return HIT_THRESHOLD_EXCEEDED;
    }

    public void setHIT_THRESHOLD_EXCEEDED(Boolean HIT_THRESHOLD_EXCEEDED) {
        this.HIT_THRESHOLD_EXCEEDED = HIT_THRESHOLD_EXCEEDED;
    }

    public Integer getPPR_COUNT() {
        return PPR_COUNT;
    }

    public void setPPR_COUNT(Integer PPR_COUNT) {
        this.PPR_COUNT = PPR_COUNT;
    }

    public int getADC(){
        return ADC;
    }

    public void setADC(int ADC){
        this.ADC = ADC;
    }

}
