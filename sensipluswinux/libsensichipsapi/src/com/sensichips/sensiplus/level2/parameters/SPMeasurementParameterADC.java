package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacketExtended;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemInGain;
import org.apache.commons.codec.binary.Hex;

import java.util.Arrays;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterADC extends SPMeasurementParameterPort {

    private SPParamItemInGain spParamItemInGain;


    private static final int ndataMin = 0, ndataMax = 16;
    private static final int conversion_rateMin = 0, conversion_rateMax = 39000;

    public static final String INPORT_IA = "IA";
    public static final String INPORT_MUX = "MUX";
    public static final String INPORT_DAC = "DAC";
    public static final String INPORT_PPR = "PPR";


    private String InPortADC = null;
    public static final String[] inPortADCLabels = new String[]{INPORT_IA, INPORT_MUX, INPORT_DAC, INPORT_PPR};
    public static final String[] inPortADCValues = new String[]{"00", "01", "10", "11"};


    private Double Conversion_Rate = null;

    private Boolean CommandX20 = true;

    public SPMeasurementParameterADC(){
    }
    public SPMeasurementParameterADC(SPConfiguration spConfiguration) throws SPException{
        super(spConfiguration);
        SPFamily spFamily = spConfiguration.getCluster().getFamilyOfChips();
        spParamItemInGain       = new SPParamItemInGain(spFamily);

    }

    public SPMeasurementParameterADC(SPConfiguration spConfiguration, Integer FIFO_DEEP, Integer FIFO_READ, String InGain, Double conversion_Rate, String InPort, Boolean CommandX20) throws SPException {
        super(spConfiguration);
        spParamItemFIFO_DEEP.setNumericalValue(FIFO_DEEP);
        spParamItemFIFO_READ.setNumericalValue(FIFO_READ);
        setConversion_Rate(conversion_Rate);
        setInPortADC(InPort);
        setCommandX20(CommandX20);
        SPFamily spFamily = spConfiguration.getCluster().getFamilyOfChips();
        spParamItemInGain       = new SPParamItemInGain(spFamily);
        setInGain(InGain);
    }

    public void setInGain(String ingain) throws SPException {
        //this.InGainLabel = ingain;
        //InGain = (String) searchValue(ingainLabels, ingainValues, ingain, "InGain");
        spParamItemInGain.setLabel(ingain);
    }

    public String getInGain() throws SPException {
        return (String) spParamItemInGain.getValue();
    }

    public String getInGainLabel() throws SPException {
        return spParamItemInGain.getLabel();
    }


    public Boolean getCommandX20(){
        return CommandX20;
    }

    public void setCommandX20(Boolean val){
        CommandX20 = val;
    }

    public void setInPortADC(String port) throws SPException {
        InPortADC = (String) searchValue(inPortADCLabels, inPortADCValues, port, "InPort");
    }

    public String getInPortADC() {
        return InPortADC;
    }

    public void setConversion_Rate(Double conversion_Rate) throws SPException {
        if (conversion_Rate == null){
            this.Conversion_Rate = conversion_Rate;
            return;
        }
        if (conversion_Rate != null && inRange(conversion_rateMin, conversion_rateMax, conversion_Rate))
            Conversion_Rate = conversion_Rate;
        else
            throw new SPException("Conversion_Rate " + Conversion_Rate + " out of range: [" + conversion_rateMin + ", " + conversion_rateMax + "]");
    }

    /*
    public void setNData(Integer NData) throws SPException {
        if (NData != null && inRange(ndataMin, ndataMax, NData))
            this.NData = NData;
        else
            throw new SPException("NData " + NData + " out of range: [" + ndataMin + ", " + ndataMax + "]");
    }*/

    public SPProtocolPacketExtended getPacketForSetADC() throws SPException{
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        byte[] dataPacket = new byte[3];
        byte opCode = (byte)(0x91);
        byte payloadSize = 0x01;
        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        if(Arrays.asList(inPortADCLabels).contains(getInPortADC())){
            int index = Arrays.asList(inPortADCValues).indexOf(getInPortADC());
            byte inPort = (byte) (index << 4);
            dataPacket[2] = inPort;
        }

        byte DATA_MASK = 0x0F; //00001111
        byte nData = (byte) (spParamItemFIFO_DEEP.getNumericalValue().byteValue() & DATA_MASK);
        dataPacket[2] = (byte)(dataPacket[2] | nData);

        System.out.println(Hex.encodeHexString(dataPacket));
        out.setDataLengthExpected(2);
        out.setByteReceived(dataPacket);
        //throw new SPException("Not yet implemented!");
        return out;
    }

    public Double getConversion_Rate() {
        return Conversion_Rate;
    }

    /*
    public Integer getNData() {
        return NData;
    }*/

    public boolean isValid() {
        try {
            return !(spParamItemFIFO_DEEP.getNumericalValue() == null || getCommandX20() == null); // getConversion_Rate() == null ||  conversion rate pu? essere null (implica il calcolo automatico dello stesso)
        } catch (SPException e) {
            return false;
        }
    }

}
