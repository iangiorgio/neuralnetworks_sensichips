package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacketExtended;
import com.sensichips.sensiplus.level2.parameters.items.*;
import org.apache.commons.codec.binary.Hex;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterEIS extends SPMeasurementParameterQI {

    public final static int NUM_OUTPUT = 13;


    /*
    public final static int IN_PHASE = 0;
    public final static int QUADRATURE = 1;
    public final static int CONDUCTANCE = 2;
    public final static int SUSCEPTANCE = 3;
    public final static int MODULE = 4;
    public final static int PHASE = 5;
    public final static int RESISTANCE = 6;
    public final static int CAPACITANCE = 7;
    public final static int INDUCTANCE = 8;
    public final static int VOLTAGE = 9;
    public final static int CURRENT = 10;
    public final static int DELTA_V_APPLIED = 11;
    public final static int CURRENT_APPLIED = 12;

    private Integer measure = null;
    protected String measureLabel = null;
    public static final String[] measureLabels = new String[]{"IN-PHASE", "QUADRATURE","CONDUCTANCE","SUSCEPTANCE","MODULE", "PHASE", "RESISTANCE", "CAPACITANCE", "INDUCTANCE", "VOLTAGE", "CURRENT", "DELTA_V_APPLIED","CURRENT_APPLIED"};
    public static final Integer[] measureValues = new Integer[]{IN_PHASE, QUADRATURE,CONDUCTANCE,SUSCEPTANCE, MODULE, PHASE, RESISTANCE, CAPACITANCE, INDUCTANCE, VOLTAGE, CURRENT,DELTA_V_APPLIED,CURRENT_APPLIED};
    */



    //private String Contacts = null;
    //protected String ContactsLabel = null;
    /*
    public static final String[] ContactsLabels = new String[]{"TWO", "FOUR"};
    public static final String[] ContactsValues = new String[]{"1", "0"};
    */

    private SPParamItemContacts spParamItemContacts;
    private SPParamItemMeasures spParamItemMeasures;

    private Integer DCBiasP = null;
    protected String DCBiasPLabel = null;

    private Integer DCBiasN = null;
    protected String DCBiasNLabel = null;



    public static final Integer dcbiasPMin = -2048;
    public static final Integer dcbiasPMax = 2047;

    public static final Integer dcbiasNMin = -32;
    public static final Integer dcbiasNMax = 31;


    public SPMeasurementParameterEIS(SPConfiguration spConfiguration) throws SPException{
        super(spConfiguration);
        //if(!(measureLabels.length==measureValues.length && measureValues.length==unitMeasuresLabels.length)){
        //    throw new SPException("WARNING - array lent mismatch between measure labels, measure values and unit measures");
        //}
        SPFamily spFamily = spConfiguration.getCluster().getFamilyOfChips();
        spParamItemContacts = new SPParamItemContacts(spFamily);
        spParamItemMeasures = new SPParamItemMeasures(spFamily);
        setFIFO_DEEP(1);
        setFIFO_READ(1);
    }

    public static int getMeasureTypeIndex(String measureLabel) throws SPException{
        int i=0;

        if(measureLabel.equals("")){
            return -1;
        } else {
            SPParamItemMeasures spParamItemMeasures = new SPParamItemMeasures(SPConfigurationManager.getSPConfigurationDefault().getCluster().getFamilyOfChips());
            return spParamItemMeasures.getIndexFromLabel(measureLabel);

            /*
            while (i < measureLabels.length && !measureLabels[i].equals(measureLabel)) {
                i++;
            }
            if (i < measureLabels.length)
                return i;
            else {
                throw new SPException(measureLabel + "measure label not allowed");
            }*/
        }
    }


    public void increaseADCRange() throws SPException  {
        try {
            if(!this.getInGain().equals(SPParamItemInGain.ingainValues[3])){
                this.setInGain(SPParamItemInGain.ingainLabels[3]);
            }
            else{
                if(!this.getRsense().equals(SPParamItemRSense.rsenseValues[0])){
                    String rsense = this.getRsense();

                    int cont = 3;

                    while (cont >=0 &&
                            !rsense.equals(SPParamItemRSense.rsenseValues[cont])) {
                        cont--;
                    }

                    if (cont>=1)
                        cont = cont - 1;
                    this.setRsense(SPParamItemRSense.rsenseLabels[cont]);
                }
            }
        }catch (SPException spException){
            throw new SPException("SPException in getOptimizedSPMeasurementParameterEIS: "+spException.getMessage());
        }

        return;
    }


    @Override
    public void reduceADCRange() throws SPException {

        try {
            if (!this.getRsense().equals(SPParamItemRSense.rsenseValues[3])) {
                String rsense = this.getRsense();
                int cont = 3;

                while (cont >=0 &&
                        !rsense.equals(SPParamItemRSense.rsenseValues[cont])) {
                    cont--;
                }
                cont = cont +1;
                this.setRsense(SPParamItemRSense.rsenseLabels[cont]);

            } else {
                if(this.getInGain().equals(SPParamItemInGain.ingainValues[3])){
                    this.setInGain(SPParamItemInGain.ingainLabels[2]);
                }
            }
        }catch (SPException spException){
            throw new SPException("SPException in getOptimizedSPMeasurementParameterEIS: "+spException.getMessage());
        }

        return;
    }

    public SPMeasurementParameterEIS(SPConfiguration spConfiguration, String Contacts, String InGain, String Harmonic, String ModeVI, Float Frequency, Integer DCBiasP,
                                     Integer DCBiasN, String OutGain, String InPort, String OutPort, String Measure, String RSense, String Filter, String PhaseShiftMode,
                                     Integer PhaseShift, String I_Q) throws SPException {

        this(spConfiguration);
        setInPort(InPort);
        setOutPort(OutPort);
        setFrequency(Frequency);
        setDCBiasP(DCBiasP);
        setDCBiasN(DCBiasN);
        setModeVI(ModeVI);
        setContacts(Contacts);
        setOutGain(OutGain);
        setHarmonic(Harmonic);
        setInGain(InGain);
        setFilter(Filter);
        setMeasure(Measure);
        setRsense(RSense);
        setPhaseShiftQuadrants(PhaseShiftMode, PhaseShift, I_Q);

    }


    /*updateSPMeasurementPArameterEIS method verify if there are any differences between the current object and the input
    * parameter and in this case make the proper settings*/
    public void updateSPMeasurementParameterEIS(SPMeasurementParameterEIS param)throws SPException{
        try {
            if (!(this.getInPort().equals(param.getInPort()))) {
                this.setInPort(param.getInPortLabel());
            }

            if (!(this.getOutPort().equals(param.getOutPort()))) {
                this.setInPort(param.getOutPortLabel());
            }
            if (!(this.getFrequency() == param.getFrequency())) {
                this.setFrequency(param.getFrequency());
            }
            if(!(this.getDCBiasP()==param.getDCBiasP())){
                this.setDCBiasP(param.getDCBiasP());
            }
            if(!(this.getDCBiasN()==param.getDCBiasN())){
                this.setDCBiasN(param.getDCBiasN());
            }
            if(!(this.getModeVI().equals(param.getModeVI()))){
                this.setModeVI(param.getModeVILabel());
            }
            if(!(this.getContacts().equals(param.getContacts()))){
                this.setContacts(param.getContactsLabel());
            }
            if(!(this.getOutGain().equals(param.getOutGain()))){
                this.setOutGain(param.getOutGainLabel());
            }
            if(!(this.getHarmonic().equals(param.getHarmonic()))){
                this.setHarmonic(param.getHarmonicLabel());
            }
            if(!(this.getInGain().equals(param.getInGain()))){
                this.setInGain(param.getInGainLabel());
            }
            if(!(this.getFilter().equals(param.getFilter()))){
                this.setFilter(param.getFilterLabel());
            }
            if(!(this.getMeasure()==param.getMeasure())){
                this.setMeasure(param.getMeasureLabel());
            }
            if(!(this.getRsense().equals(param.getRsense()))){
                this.setRsense(param.getRsenseLabel());
            }
            if( !(this.getQI().equals(param.getQI())) || !(this.getPhaseShift()==param.getPhaseShift()) || !(this.getPhaseShiftMode()==param.getPhaseShiftMode())){
                this.setPhaseShiftQuadrants(param.getPhaseShiftModeLabel(),param.getPhaseShift(),param.getQILabel());
            }

        }catch(SPException spe){
            throw new SPException("SPException in updateSPMeasurementParameterEIS method: "+spe.getMessage());
        }
    }

    public boolean equals(SPMeasurementParameterEIS param) {
        boolean output = false;
        try {
            output = param.getContacts().equalsIgnoreCase(getContacts())
                    && param.getInGain().equalsIgnoreCase(getInGain())
                    && param.getHarmonic().equals(getHarmonic())
                    && param.getModeVI().equalsIgnoreCase(getModeVI())
                    && param.getFrequency().equals(getFrequency())
                    && param.getDCBiasP().equals(getDCBiasP())
                    && param.getOutGain().equals(getOutGain())
                    && param.getInPort().equals(getInPort())
                    && param.getOutPort().equals(getOutPort())
                    && param.getMeasure().equals(getMeasure())
                    && param.getRsense().equalsIgnoreCase(getRsense())
                    && param.getFilter().equalsIgnoreCase(getFilter())
                    && param.getPhaseShiftMode().equals(getPhaseShiftMode())
                    && param.getPhaseShift().equals(getPhaseShift());
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }


    public void clone(SPMeasurementParameterEIS toClone) throws SPException{
        try{
            this.setContacts(toClone.getContactsLabel());
            if(toClone.getInGainLabel()!=null)
                this.setInGain(toClone.getInGainLabel());
            this.setHarmonic(toClone.getHarmonicLabel());
            this.setModeVI(toClone.getModeVILabel());
            this.setFrequency(toClone.getFrequency());
            this.setDCBiasP(toClone.getDCBiasP());
            this.setDCBiasN(toClone.getDCBiasN());
            this.setOutGain(toClone.getOutGainLabel());
            this.setInPort(toClone.getInPortLabel());
            this.setOutPort(toClone.getOutPortLabel());
            this.setMeasure(toClone.getMeasureLabel());
            this.setSequentialMode(toClone.isSequentialMode());
            if(toClone.getRsenseLabel()!=null)
                this.setRsense(toClone.getRsenseLabel());
            this.setFilter(toClone.getFilterLabel());
            this.setPhaseShiftQuadrants(toClone.getPhaseShiftModeLabel(),toClone.getPhaseShift(),toClone.getQILabel());

            if(toClone.getRegistersList().length>0) {
                this.setRegistersList(new String[toClone.getRegistersList().length]);
                for(int i=0; i<toClone.getRegistersList().length; i++){
                    this.getRegistersList()[i] = toClone.getRegistersList()[i];
                }
            }
            this.setFIFO_DEEP(toClone.getFIFO_DEEP());
            this.setFIFO_READ(toClone.getFIFO_READ());
            this.setFillBufferBeforeStart(toClone.isFillBufferBeforeStart());
            this.setBurstMode(toClone.isBurstMode());

        }catch (SPException e){
            throw new SPException("Exception in clone method in SPMeasurementParameterEIS: "+e.getMessage());
        }

    }

    public static String[] getDifferences(List<SPMeasurementParameterEIS[]> inputList) throws SPException {

        List<SPMeasurementParameterEIS> list = new ArrayList<>();
        for(int i=0; i<inputList.size(); i++){
            for(int j=0; j<inputList.get(i).length; j++){
                list.add(inputList.get(i)[j]);
            }
        }


        String[] out = new String[list.size()];



        for(int i=0; i<out.length; i++){
            out[i]="";
        }

        if(list.size()>1) {
            int cont = 0;


            /*for(int i=0; i<list.size(); i++){*/
            String title1 = "";

            for (int j = 1; j < list.size(); j++) {
                if (!list.get(0).getFrequency().toString().equals(list.get(j).getFrequency().toString())
                        && !out[0].contains("_FREQ_")) {


                    for(int i=0; i<list.size(); i++){
                        out[i] += "_FREQ_"+list.get(i).getFrequency();
                    }

                    //title1 += "Frequency = " + list.get(0).getFrequency() + " - ";
                    //title2 += "Frequency = " + list.get(i + 1).getFrequency() + " - ";
                }

                if (!list.get(0).getInPortLabel().equals(list.get(j).getInPortLabel())
                        && !out[0].contains("_INPORT_")) {


                    for(int i=0; i<list.size(); i++){
                        out[i] += "_INPORT_"+list.get(i).getInPortLabel();
                    }

                    //title1 += "InPort = " + list.get(0).getInPortLabel() + " - ";
                    //title2 += "InPort = " + list.get(j).getInPortLabel() + " - ";
                }

                if (!list.get(0).getOutPortLabel().equals(list.get(j).getOutPortLabel())
                        && !out[0].contains("_OUTPORT_")) {


                    for(int i=0; i<list.size(); i++){
                        out[i] += "_OUTPORT_"+list.get(i).getOutPortLabel();
                    }
                    //title1 += "OutPort = " + list.get(0).getOutPortLabel() + " - ";
                    //title2 += "OutPort = " + list.get(i + 1).getOutPortLabel() + " - ";
                }

                if (list.get(0).getRsenseLabel() != null && list.get(j).getRsenseLabel() != null) {
                    if (!list.get(0).getRsenseLabel().equals(list.get(j).getRsenseLabel())
                            && !out[0].contains("_RSENSE_")) {

                        for(int i=0; i<list.size(); i++){
                            out[i] += "_RSENSE_"+list.get(i).getRsenseLabel();
                        }

                        //title1 += "Rsense = " + list.get(0).getRsenseLabel() + " - ";
                        //title2 += "Rsense = " + list.get(i + 1).getRsenseLabel() + " - ";
                    }
                } else if (!out[0].contains("_RSENSE_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_RSENSE_"+"AUTO";
                    }

                    //title1 += "Rsense = " + "auto - ";
                    //title2 += "Rsense = " + "auto - ";
                }

                if (list.get(0).getInGainLabel() != null && list.get(j).getInGainLabel() != null) {
                    if (!list.get(0).getInGainLabel().equals(list.get(j).getInGainLabel())
                            && !out[0].contains("_INGAIN_")) {

                        for(int i=0; i<list.size(); i++){
                            out[i] += "_INGAIN_"+list.get(i).getInGainLabel();
                        }
                        //title1 += "InGain = " + list.get(0).getInGainLabel() + " - ";
                        //title2 += "InGain = " + list.get(i + 1).getInGainLabel() + " - ";
                    }
                } else if (!out[0].contains("_INGAIN_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_INGAIN_"+"AUTO";
                    }
                    //title1 += "InGain = " + "auto - ";
                    //title2 += "InGain = " + "auto - ";
                }

                if (!list.get(0).getOutGainLabel().equals(list.get(j).getOutGainLabel())
                        && !out[0].contains("_OUTGAIN_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_OUTGAIN_"+list.get(i).getOutGainLabel();
                    }
                    //title1 += "OutGain = " + list.get(0).getOutGainLabel() + " - ";
                    //title2 += "OutGain = " + list.get(i + 1).getOutGainLabel() + " - ";
                }
                if (list.get(0).getDCBiasP() != list.get(j).getDCBiasP()
                        && !out[0].contains("_DCBIASP_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_DCBIASP_"+list.get(i).getDCBiasP();
                    }

                    //title1 += "DCBiasP = " + list.get(0).getDCBiasP() + " - ";
                    //title2 += "DCBiasP = " + list.get(i + 1).getDCBiasP() + " - ";
                }

                if (list.get(0).getDCBiasN() != list.get(j).getDCBiasN()
                        && !out[0].contains("_DCBIASN_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_DCBIASN_"+list.get(i).getDCBiasN();
                    }

                    //title1 += "DCBiasN = " + list.get(0).getDCBiasN() + " - ";
                    //title2 += "DCBiasN = " + list.get(i + 1).getDCBiasN() + " - ";
                }
                if (!list.get(0).getContactsLabel().equals(list.get(j).getContactsLabel())
                        && !out[0].contains("_CONTACTS_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_CONTACTS_"+list.get(i).getContactsLabel();
                    }
                    //title1 += "Contacts = " + list.get(0).getContactsLabel() + " - ";
                    //title2 += "Contacts = " + list.get(i + 1).getContactsLabel() + " - ";
                }
                if (!list.get(0).getModeVILabel().equals(list.get(j).getModeVILabel())
                        && !out[0].contains("_MODEVI_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_MODEVI_"+list.get(i).getModeVILabel();
                    }
                    //title1 += "ModeVI = " + list.get(0).getModeVILabel() + " - ";
                    //title2 += "ModeVI = " + list.get(i + 1).getModeVILabel() + " - ";
                }

                if (!list.get(0).getHarmonicLabel().equals(list.get(j).getHarmonicLabel())
                        && !out[0].contains("_HARMONIC_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_HARMONIC_"+list.get(i).getHarmonicLabel();
                    }
                    //title1 += "Harmonic = " + list.get(0).getHarmonicLabel() + " - ";
                    //title2 += "Harmonic = " + list.get(i + 1).getHarmonicLabel() + " - ";
                }
                if (!list.get(0).getFilterLabel().equals(list.get(j).getFilterLabel())
                        && !out[0].contains("_FILTER_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_FILTER_"+list.get(i).getFilterLabel();
                    }
                    //title1 += "Filter = " + list.get(0).getFilterLabel() + " - ";
                    //title2 += "Filter = " + list.get(i + 1).getFilterLabel() + " - ";
                }
                if (!list.get(0).getPhaseShiftLabel().equals(list.get(j).getPhaseShiftLabel())
                        && !out[0].contains("_PHASESHIFT_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_PHASESHIFT_"+list.get(i).getPhaseShiftLabel();
                    }
                    //
                    // title1 += "PhaseShift = " + list.get(0).getPhaseShiftLabel() + " - ";
                    //title2 += "PhaseShift = " + list.get(i + 1).getPhaseShiftLabel() + " - ";
                }
                if (!list.get(0).getPhaseShiftModeLabel().equals(list.get(j).getPhaseShiftModeLabel())
                        && !out[0].contains("_PHASESHIFTMODE_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_PHASESHIFTMODE_"+list.get(i).getPhaseShiftModeLabel();
                    }
                    //title1 += "PhaseShiftMode = " + list.get(0).getPhaseShiftModeLabel() + " - ";
                    //title2 += "PhaseShiftMode = " + list.get(i + 1).getPhaseShiftModeLabel() + " - ";
                }
                if (!list.get(0).getQILabel().equals(list.get(j).getQILabel())
                        && !out[0].contains("_QI_")) {
                    for(int i=0; i<list.size(); i++){
                        out[i] += "_QI_"+list.get(i).getQILabel();
                    }
                    //title1 += "QI = " + list.get(0).getQILabel() + " - ";
                    //title2 += "QI = " + list.get(i + 1).getQILabel() + " - ";
                }


                if (!Arrays.equals(list.get(0).getRegistersList(), list.get(j).getRegistersList())
                        && !out[0].contains("_REGISTERS_")) {

                    for(int i=0; i<list.size(); i++){
                        out[i] += "_REGISTERS_"+String.join(",", list.get(0).getRegistersList());
                    }

                    //title1 += "REGISTERS = " + String.join(",", list.get(0).getRegistersList()) + " - ";

                }


                //}
            }
        }

        /*
            out[i] = title1;
        }*/


        return out;
    }

    public String toString() {
        String output = "";
        try{
            output = "EIS parameters:\n"
                + "- InGainLabel: " + getInGainLabel() + ", InGain: " + getInGain() + "\n"
                + "- RsenseLabel: " + getRsenseLabel() + ", Rsense: " + getRsense() + "\n"
                + "- ContactsLabel: " + getContactsLabel() + ", Contacts: " + getContacts() + "\n"
                + "- Q_ILabel: " + getQILabel() + ", Q_I: " + getQI() + "\n"
                + "- HarmonicLabel: " + getHarmonicLabel() + ", Harmonic: " + getHarmonic() + "\n"
                + "- ModeVILabel: " + getModeVILabel() + ", ModeVI: " + getModeVI() + "\n"
                + "- FrequencyLabel: " + FrequencyLabel + ", Frequency: " + getFrequency() + "\n"
                + "- DCBiasPLabel: " + DCBiasPLabel + ", DCBiasP: " + getDCBiasP() + "\n"
                + "- OutGainLabel: " + getOutGainLabel() + ", OutGain: " + getOutGain() + "\n"
                + "- InPortLabel: " + InPortLabel + ", InPort: " + getInPort() + "\n"
                + "- OutPortLabel: " + OutPortLabel + ", OutPort: " + getOutPort() + "\n"
                + "- MeasureLabel: " + spParamItemMeasures.getLabel() + ", Measure: " + spParamItemMeasures.getValue()+ "\n"
                + "- FilterLabel: " + getFilterLabel() + ", Filter: " + getFilter() + "\n"
                + "- PhaseShiftModeLabel: " + getPhaseShiftModeLabel() + ", PhaseShiftMode: " + getPhaseShiftMode() + "\n"
                + "- PhaseShiftLabel: " + PhaseShiftLabel + ", PhaseShift: " + getPhaseShift();
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }


    public String getMeasureLabel() throws SPException {
        return spParamItemMeasures.getLabel();
    }

    public void setMeasureLabel(String measureLabel) throws SPException {
        spParamItemMeasures.setLabel(measureLabel);
    }

    public String getHeaderAsString(){

        String output = "";
        output += "ingain;";
        output += "rsense;";
        output += "contacts;";
        output += "harmonic;";
        output += "modevi;";
        output += "frequency;";
        output += "dcbiasP;";
        output += "dcbiasN;";
        output += "outgain;";
        output += "inport;";
        output += "outport;";
        output += "filter;";
        output += "phaseShift;";
        return output;
    }


    public String getLabelsAsString(){
        String output = "";

        try {

            output += getInGainLabel() + ";";
            output += getRsenseLabel() + ";";
            output += getContactsLabel() + ";";
            output += getHarmonicLabel() + ";";
            output += getModeVILabel() + ";";
            output += FrequencyLabel + ";";
            output += DCBiasPLabel + ";";
            output += DCBiasNLabel + ";";
            output += getOutGainLabel() + ";";
            output += InPortLabel + ";";
            output += OutPortLabel + ";";
            output += getFilterLabel() + ";";
            output += getPhaseShiftModeLabel() + ",";
            output += getQILabel() + ",";
            output += PhaseShiftLabel + ";";
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }



    public boolean isValid() {
        boolean output = false;
        try{
            output = !(getFrequency() == null
                || getDCBiasP() == null
                || getModeVI() == null
                || getContacts() == null
                || getOutGain() == null
                || getHarmonic() == null
                || getQI() == null
                || getPhaseShift() == null
                || getPhaseShiftMode() == null
                || getInGain() == null
                || getOutPort() == null
                || getInPort() == null
                || getRsense() == null
                || getFilter() == null);
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }


    public void setMeasure(String measure) throws SPException {
        spParamItemMeasures.setLabel(measure);
    }

    public void setContacts(String contacts) throws SPException {
        spParamItemContacts.setLabel(contacts);
        //this.ContactsLabel = contacts;
        //Contacts = (String) searchValue(ContactsLabels, ContactsValues, contacts, "Contacts");
    }

    public String getContactsLabel() throws SPException {
        return spParamItemContacts.getLabel();
    }

    public String getContacts() throws SPException {
        return (String) spParamItemContacts.getValue();
    }

    public SPProtocolPacketExtended getPacketForSetEIS() throws SPException{
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        /*

        int SIGNED_MASK = 255; // 11111111

        byte[] dataPacket = new byte[14];
        byte opCode = (byte)(0x90);
        byte payloadSize = 0x0C;

        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        //Imposto il byte 1 del PAYLOAD
        if(Arrays.asList(rsenseValues).contains(getRsense())){
            int index = Arrays.asList(rsenseValues).indexOf(getRsense());
            byte rSense = (byte)((index & SIGNED_MASK) << 6);
            dataPacket[2] = rSense;
        }

        if(Arrays.asList(ingainValues).contains(getInGain())){
            int index = Arrays.asList(ingainValues).indexOf(getInGain());
            byte inGain = (byte)((index & SIGNED_MASK) << 4);
            dataPacket[2] = (byte)(dataPacket[2] | inGain);
        }

        if(Arrays.asList(harmonicValues).contains(getHarmonic())){
            int index = Arrays.asList(harmonicValues).indexOf(getHarmonic());
            byte harmonic = (byte)((index & SIGNED_MASK) << 2);
            dataPacket[2] = (byte)(dataPacket[2] | harmonic);
        }

        if(Arrays.asList(ModeVIValues).contains(getModeVI())){
            int index = Arrays.asList(ModeVIValues).indexOf(getModeVI());
            byte modeVI = (byte)((index & SIGNED_MASK));
            dataPacket[2] = (byte)(dataPacket[2] | modeVI);
        }

        //Imposto il byte 2 del PAYLOAD
        if(Arrays.asList(outgainValues).contains(getOutGain())){
            int index = Arrays.asList(outgainValues).indexOf(getOutGain());
            byte outGain = (byte)((index & SIGNED_MASK) << 5);
            dataPacket[3] = outGain;
        }

        if(Arrays.asList(portValues).contains(getInPort())){
            int index = Arrays.asList(portValues).indexOf(getInPort());
            byte inPort = (byte)(index & SIGNED_MASK);
            dataPacket[3] = (byte)(dataPacket[3] | inPort);
        }

        //Imposto il byte 3 del PAYLOAD
        if(Arrays.asList(I_QValues).contains(getQI())){
            int index = Arrays.asList(I_QValues).indexOf(getQI());
            byte qi = (byte)((index & SIGNED_MASK) << 6);
            dataPacket[4] = qi;
        }

        if(Arrays.asList(getContacts()).contains(getContacts())){
            int index = Arrays.asList(getContacts()).indexOf(getContacts());
            byte contacts = (byte)((index & SIGNED_MASK) << 5);
            dataPacket[4] = (byte)(dataPacket[4] | contacts);
        }

        if(Arrays.asList(portValues).contains(getOutPort())){
            int index = Arrays.asList(portValues).indexOf(getOutPort());
            byte outPort = (byte)(index & SIGNED_MASK);
            dataPacket[4] = (byte)(dataPacket[4] | outPort);
        }

        //Imposto il byte 4 del PAYLOAD
        int dcBiasN = getDCBiasN() + 32;
        if(dcBiasN >= 0 && dcBiasN <= 63){
            dataPacket[5] = (byte)dcBiasN;
        }

        //Imposto il byte 5 del PAYLOAD
        if(Arrays.asList(measureValues).contains(getMeasure())){
            int index = Arrays.asList(measureValues).indexOf(getMeasure());
            byte measure = (byte)((index & SIGNED_MASK) << 3);
            dataPacket[6] = (byte)(dataPacket[6] | measure);
        }

        if(Arrays.asList(filterValues).contains(getFilter())){
            int index = Arrays.asList(filterValues).indexOf(getFilter());
            byte filter = (byte)(index & SIGNED_MASK);
            dataPacket[6] = (byte)(dataPacket[6] | filter);
        }

        //Imposto il byte 6 del PAYLOAD
        int phaseShift = Integer.parseInt(getPhaseShiftLabel());
        dataPacket[7] = (byte)((phaseShift));
        if(Arrays.asList(PhaseShiftModeValues).contains(getPhaseShiftMode())){
            int index = Arrays.asList(PhaseShiftModeValues).indexOf(getPhaseShiftMode());
            byte phaseShiftMode = (byte)((index & SIGNED_MASK) << 5);
            dataPacket[7] = (byte)(dataPacket[7] | phaseShiftMode);
        }
        //Imposto il byte 7-8
        int FIRSTBYTE_MASK = 255; // 11111111
        int appo = getDCBiasP() + 2048;
        dataPacket[8] = (byte)(appo >> 8);
        dataPacket[9] = (byte)(appo & FIRSTBYTE_MASK);

        //imposto il byte 9-10-11-12
        byte[] frequency = new byte[4];
        ByteBuffer.wrap(frequency).putFloat(getFrequency());
        dataPacket[10] = frequency[0];
        dataPacket[11] = frequency[1];
        dataPacket[12] = frequency[2];
        dataPacket[13] = frequency[3];

        System.out.println(Hex.encodeHexString(dataPacket));

        out.setDataLengthExpected(2);
        out.setByteReceived(dataPacket);
        //throw new SPException("Not yet implemented!");
        */
        return out;
    }



    public SPProtocolPacketExtended getPacketForGetEIS() throws SPException{
        SPProtocolPacketExtended out = new SPProtocolPacketExtended();

        byte[] dataPacket = new byte[2];
        byte opCode = (byte)(0x92);
        byte payloadSize = 0x00;

        dataPacket[0] = opCode;
        dataPacket[1] = payloadSize;

        //System.out.println(Hex.encodeHexString(dataPacket));

        //out.setDataLengthExpected(74); //72 byte per la misura (9 elementi double) + 2 byte (error code + payload size (A0 00)) = 74 byte totali
        out.setDataLengthExpected(146); // caso 2 chip
        out.setByteReceived(dataPacket);
        //throw new SPException("Not yet implemented!");
        return out;
    }



    public void setDCBiasP(Integer DCBiasP) throws SPException {
        this.DCBiasPLabel = "" + DCBiasP;
        if (DCBiasP != null && inRange((int) dcbiasPMin, (int) dcbiasPMax, DCBiasP))
            this.DCBiasP = DCBiasP;
        else
            throw new SPException("DCBiasP " + DCBiasP + " out of range: [" + dcbiasPMin + ", " + dcbiasPMax + "]");
    }


    public void setDCBiasN(Integer DCBiasN) throws SPException {
        this.DCBiasNLabel = "" + DCBiasN;
        if (DCBiasN != null && inRange((int) dcbiasNMin, (int) dcbiasNMax, DCBiasN))
            this.DCBiasN = DCBiasN;
        else
            throw new SPException("DCBiasN " + DCBiasN + " out of range: [" + dcbiasNMin + ", " + dcbiasNMax + "]");
    }

    public Integer getMeasure() throws SPException {
        return (Integer) spParamItemMeasures.getValue();
    }





    public Integer getDCBiasP() {
        return DCBiasP;
    }


    public Integer getDCBiasN() {
        return DCBiasN;
    }




    /**
     * @param args
     */
    public static void main(String[] args) {
        double OSM;
        OSM = (double) Integer.parseInt("111", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("110", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("101", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("100", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("011", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("010", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("001", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("000", 2);
        System.out.println("OSM: " + OSM);

        byte dataPacket[] = new byte[1];
        int phaseShift = Integer.parseInt("20");
        dataPacket[0] = (byte)(phaseShift);
        if(Arrays.asList(SPParamItemPhaseShiftMode.PhaseShiftModeValues).contains(SPParamItemPhaseShiftMode.FINE)){
            int index = Arrays.asList(SPParamItemPhaseShiftMode.PhaseShiftModeValues).indexOf(SPParamItemPhaseShiftMode.FINE);
            byte phaseShiftMode = (byte)((index & 255)<<5);
            dataPacket[0] = (byte)(dataPacket[0] | phaseShiftMode);
        }
        System.out.println("MY PACKET: ");
        System.out.println(Hex.encodeHexString(dataPacket));
    }

}
