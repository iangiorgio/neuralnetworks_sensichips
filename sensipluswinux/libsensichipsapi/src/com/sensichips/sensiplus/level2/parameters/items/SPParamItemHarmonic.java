package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemHarmonic extends SPParamItem {
    //private String Harmonic = null;
    //protected String HarmonicLabel = null;
    public static final String[] harmonicLabelsRUN5 = new String[]{"FIRST_HARMONIC", "SECOND_HARMONIC", "THIRD_HARMONIC"};
    public static final String[] harmonicValuesRUN5 = new String[]{"01", "10", "11"};

    public static final String[] harmonicLabelsRUN6 = new String[]{"FIRST_HARMONIC", "SECOND_HARMONIC", "THIRD_HARMONIC"};
    public static final String[] harmonicValuesRUN6 = new String[]{"01", "10", "11"};

    public static String[] harmonicLabels;
    public static String[] harmonicValues;

    public static String[] ContactsLabels = null;
    public static String[] ContactsValues = null;

    public SPParamItemHarmonic(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            harmonicLabels = harmonicLabelsRUN5;
            harmonicValues = harmonicValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            harmonicLabels = harmonicLabelsRUN6;
            harmonicValues = harmonicValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(harmonicLabels, harmonicValues);

    }
}
