package com.sensichips.sensiplus.level2.parameters;


import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.chip.SPPort;
import com.sensichips.sensiplus.level2.parameters.items.*;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterQI extends SPMeasurementParameterADC {




    private static String LOG_MESSAGE = "SPMeasurementParameterQI";

    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L2;



    public final static int NUM_OUTPUT = 4;

    public final static int IN_PHASE = 0;
    public final static int QUADRATURE = 1;
    //public final static int MODULE = 2;
    //public final static int PHASE = 3;

    // Generale
    //private String Rsense = null;
    //protected String RsenseLabel = null;
    //public static final String[] rsenseLabels = new String[]{"50000", "5000", "500", "50"};
    //public static final String[] rsenseValues = new String[]{"00", "01", "10", "11"};

    private SPParamItemRSense spParamItemRSense;


    // Generale
    //private String InGain = null;
    //protected String InGainLabel = null;
    //public static final String[] ingainLabels = new String[]{"1", "12", "20", "40"};
    //public static final String[] ingainValues = new String[]{"11", "10", "01", "00"};
    //private SPParamItemInGain spParamItemInGain;

    // Generale
    //private String Harmonic = null;
    //protected String HarmonicLabel = null;
    //public static final String[] harmonicLabels = new String[]{"FIRST_HARMONIC", "SECOND_HARMONIC", "THIRD_HARMONIC"};
    //public static final String[] harmonicValues = new String[]{"01", "10", "11"};
    private SPParamItemHarmonic spParamItemHarmonic;

    // Generale
    //private String ModeVI = null;
    //protected String ModeVILabel = null;
    //public static final String[] ModeVILabels = new String[]{"VOUT_VIN", "VOUT_IIN", "IOUT_VIN", "IOUT_IIN"};
    //public static final String[] ModeVIValues = new String[]{"01", "00", "11", "10"};
    private SPParamItemModeVI spParamItemModeVI;

    // Generale
    private Float Frequency = null;
    protected String FrequencyLabel = null;
    public static int frequencyLimits[] = null;


    // Generale
    //private String OutGain = null;
    //protected String OutGainLabel = null;
    //public static final String[] outgainLabels = new String[]{"0", "1", "2", "3", "4", "5", "6", "7"};
    //public static final String[] outgainValues = new String[]{"000", "001", "010", "011", "100", "101", "110", "111"};
    private SPParamItemOutGain spParamItemOutGain;


    //private String Q_I = null;
    //protected String Q_ILabel = null;
    //public static final String[] I_QLabels = new String[]{"IN_PHASE", "QUADRATURE", "ANTI_PHASE", "ANT_QUADRATURE"};
    //public static final String[] I_QValues = new String[]{"00", "01", "10", "11"};
    private SPParamItemQI spParamItemQI;

    // Generale
    private String InPort = null;
    protected String InPortLabel = null;
    // Generale
    private String OutPort = null;

    protected String OutPortLabel = null;

    // Generale
    //private String Filter;
    //protected String FilterLabel;
    //public static final String[] filterLabels = new String[]{"1", "4", "8", "16", "32", "64", "128", "256","1000","10000"};
    //public static final String[] filterValues = new String[]{"1", "4", "8", "16", "32", "64", "128", "256","1000","10000"};
    //private SPParamItemFilter spParamItemFilter;


    // outputDecimation = true: the Filter measure will be made as a burst and will return only one measure
    // outputDecimation = false: each request (e.g. getXXX) contains a buffer of Filter dimension but return a value
    // for each call.
    //private Integer outputDecimation;


    private Integer PhaseShift = null;
    protected String PhaseShiftLabel = null;
    public static final Integer PhaseShiftMin = 0;
    public static final Integer PhaseShiftMax = 31;

    //public final static int QUADRANT = 0;
    //public final static int COARSE = 1;
    //public final static int FINE = 2;

    //private Integer PhaseShiftMode = null;
    //protected String PhaseShiftModeLabel = null;
    //public static final String[] PhaseShiftModeLabels = new String[]{"Quadrants", "Coarse", "Fine"};
    //public static final Integer[] PhaseShiftModeValues = new Integer[]{QUADRANT, COARSE, FINE};
    private SPParamItemPhaseShiftMode spParamItemPhaseShiftMode;


    private int SYS_CLOCK = 10000000;


    public SPMeasurementParameterQI(SPConfiguration spConfiguration) throws SPException {
        super(spConfiguration);
        this.SYS_CLOCK = SYS_CLOCK;
        frequencyLimits = new int[]{0, 78125, 156250, 312500, 625000, 1250000, 2500000, SYS_CLOCK};

        SPFamily spFamily = spConfiguration.getCluster().getFamilyOfChips();
        spParamItemRSense       = new SPParamItemRSense(spFamily);
        spParamItemHarmonic     = new SPParamItemHarmonic(spFamily);
        spParamItemModeVI       = new SPParamItemModeVI(spFamily);
        spParamItemOutGain      = new SPParamItemOutGain(spFamily);
        spParamItemQI           = new SPParamItemQI(spFamily);
        //spParamItemFilter       = new SPParamItemFilter(spFamily);
        spParamItemPhaseShiftMode = new SPParamItemPhaseShiftMode(spFamily);
    }

    public SPMeasurementParameterQI(){
        this.SYS_CLOCK = SYS_CLOCK;
        frequencyLimits = new int[]{0, 78125, 156250, 312500, 625000, 1250000, 2500000, SYS_CLOCK};
    }



    public String getPhaseShiftModeLabel() throws SPException {
        return spParamItemPhaseShiftMode.getLabel();
    }

    public void setPhaseShiftModeLabel(String phaseShiftModeLabel) throws SPException {
        spParamItemPhaseShiftMode.setLabel(phaseShiftModeLabel);
    }

    public String getPhaseShiftLabel() {
        return PhaseShiftLabel;
    }

    public void setPhaseShiftLabel(String phaseShiftLabel) {
        PhaseShiftLabel = phaseShiftLabel;
    }

    public String getQILabel() throws SPException {
        return spParamItemQI.getLabel();
    }
    /*
    public void setQ_ILabel(String q_ILabel) throws SPException {
        spParamItemQI.setLabel(q_ILabel);
    }*/

    public String getRsenseLabel() throws SPException {
        return spParamItemRSense.getLabel();
    }

    public void setRsenseLabel(String rsenseLabel) throws SPException {
        spParamItemRSense.setLabel(rsenseLabel);
    }
    /*public String getFilterLabel() throws SPException {
        return spParamItemFilter.getLabel();
    }*/

    /*
    public void setFilterLabel(String filterLabel) throws SPException {
        spParamItemFilter.setLabel(filterLabel);
    }*/


    public void setInGainLabel(String inGainLabel) throws SPException  {
        spParamItemRSense.setLabel(inGainLabel);
    }


    public String getHarmonicLabel() throws SPException {
        return spParamItemHarmonic.getLabel();
    }

    public void setHarmonicLabel(String harmonicLabel) throws SPException {
        spParamItemHarmonic.getLabel();
    }
    public String getOutGainLabel() throws SPException {
        return spParamItemOutGain.getLabel();
    }

    public void setOutGainLabel(String outGainLabel) throws SPException {
        //OutGainLabel = outGainLabel;
        spParamItemOutGain.setLabel(outGainLabel);
    }

    public String getModeVILabel() throws SPException {
        return spParamItemModeVI.getLabel();
    }

    public void setModeVILabel(String modeVILabel) throws SPException {
        spParamItemHarmonic.setLabel(modeVILabel);
    }

    public String getInPortLabel() {
        return InPortLabel;
    }

    public void setInPortLabel(String inPortLabel) {
        InPortLabel = inPortLabel;
    }

    public String getOutPortLabel() {
        return OutPortLabel;
    }

    public void setOutPortLabel(String outPortLabel) {
        OutPortLabel = outPortLabel;
    }


    public boolean equals(SPMeasurementParameterQI param) {
        boolean output = false;
        try{
            output = param.getInGain().equalsIgnoreCase(getInGain())
                && param.getHarmonic().equals(getHarmonic())
                && param.getModeVI().equalsIgnoreCase(getModeVI())
                && param.getFrequency().equals(getFrequency())
                && param.getOutGain().equals(getOutGain())
                && param.getQI().equalsIgnoreCase((String)spParamItemQI.getValue())
                && param.getInPort().equals(getInPort())
                && param.getOutPort().equals(getOutPort())
                && param.getRsense().equalsIgnoreCase(getRsense())
                //&& param.getOutputDecimation().equals(getOutputDecimation())
                && param.getPhaseShift().equals(getPhaseShift())
                && param.getFilter().equalsIgnoreCase(getFilter())
                && param.getPhaseShiftMode() == getPhaseShiftMode();
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }



    public String toString() {
        String output = "";

        try{
            output = "IQ parameters:"
                    + "InGainLabel: " + getInGainLabel() + "InGain: " + getInGain()
                    + ", HarmonicLabel: " + getHarmonicLabel()+ ", Harmonic: " + getHarmonic()
                    + ", ModeVILabel: " + getModeVILabel() + ", ModeVI: " + getModeVI()
                    + ", FrequencyLabel: " + FrequencyLabel+ ", Frequency: " + getFrequency()
                    + ", OutGainLabel: " + getOutGainLabel()+ ", OutGain: " + getOutGain()
                    + ", InPortLabel: " + InPortLabel + ", InPort: " + getInPort()
                    + ", OutPortLabel: " + OutPortLabel + ", OutPort: " + getOutPort()
                    + ", Q_ILabel: " + getQILabel() + ", Q_I: " + getQI()
                    + ", RsenseLabel: " + getRsenseLabel() + ", Rsense: " + getRsense()
                    + ", FilterLabel: " + getFilterLabel() + ", Filter: " + getFilter();
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }

    public boolean isValid() {
        boolean output = false;

        try {
            output = !(getFrequency() == null
                    || getModeVI() == null
                    || getOutGain() == null
                    || getHarmonic() == null
                    || getInGain() == null
                    || getOutPort() == null
                    || getInPort() == null
                    || getRsense() == null
                    || getFilter() == null
                    //|| getOutputDecimation() == null
                    || getPhaseShift() == null);
        } catch (SPException spe){
            spe.printStackTrace();
        }
        return output;
    }

    //public Integer getOutputDecimation(){
    //    return outputDecimation;
    //}

    //public void setOutputDecimation(Integer outputDecimation){
    //    this.outputDecimation = outputDecimation;
    //}


    public Integer getPhaseShift(){
        return PhaseShift;
    }

    /**
     *
     * @param PhaseShiftMode One of ["Quadrants", "Coarse", "Fine"]
     * @param PhaseShift     A value in the interval [0, 31]
     * @param i_q            One of ["IN_PHASE", "QUADRATURE", "ANTI_PHASE", "ANT_QUADRATURE"]
     * @throws SPException
     */
    public void setPhaseShiftQuadrants(String PhaseShiftMode, Integer PhaseShift, String i_q) throws SPException {
        this.PhaseShiftLabel = "" + PhaseShift;
        if (PhaseShift != null && inRange((int)PhaseShiftMin, (int)PhaseShiftMax, PhaseShift))
            this.PhaseShift = PhaseShift;
        else
            throw new SPException("PhaseShift " + PhaseShift + " out of range: [" + PhaseShiftMin + ", " + PhaseShiftMax + "]");

        //this.PhaseShiftModeLabel = PhaseShiftMode;
        //this.PhaseShiftMode = (Integer) searchValue(PhaseShiftModeLabels, PhaseShiftModeValues, PhaseShiftMode, "PhaseShiftMode");
        spParamItemPhaseShiftMode.setLabel(PhaseShiftMode);
        //this.Q_ILabel = i_q;
        //Q_I = (String) searchValue(I_QLabels, I_QValues, i_q, "Q_I");
        spParamItemQI.setLabel(i_q);

    }


    public Integer getPhaseShiftMode() throws SPException {
        return (Integer) spParamItemPhaseShiftMode.getValue();
    }

    public void setPhaseShiftMode(String PhaseShiftMode) throws SPException {
        //this.PhaseShiftModeLabel = PhaseShiftMode;
        //this.PhaseShiftMode = (Integer) searchValue(PhaseShiftModeLabels, PhaseShiftModeValues, PhaseShiftMode, "PhaseShiftMode");
        spParamItemPhaseShiftMode.setLabel(PhaseShiftLabel);

    }

    public String getQI() throws SPException {
        return (String) spParamItemQI.getValue();
    }
    public void setIQ(String i_q) throws SPException {
        //this.Q_ILabel = i_q;
        //Q_I = (String) searchValue(I_QLabels, I_QValues, i_q, "Q_I");
        spParamItemQI.setLabel(i_q);
    }




    public void setRsense(String Rsense) throws SPException {
        spParamItemRSense.setLabel(Rsense);

        //this.RsenseLabel = Rsense;
        //this.Rsense = (String) searchValue(rsenseLabels, rsenseValues, Rsense, "Rsense");
    }


    public void setInPort(String port) throws SPException {
        this.InPortLabel = port;
        InPort = (String) searchValue(SPPort.portLabels, SPPort.portValues, port, "InPort");
    }


    public void setOutPort(String port) throws SPException {
        this.OutPortLabel = port;
        OutPort = (String) searchValue(SPPort.portLabels, SPPort.portValues, port, "OutPort");
    }

    public void setHarmonic(String harmonic) throws SPException {
        //this.HarmonicLabel = harmonic;
        //Harmonic = (String) searchValue(harmonicLabels, harmonicValues, harmonic, "Harmonic");
        spParamItemHarmonic.setLabel(harmonic);
    }


    public void setModeVI(String mode) throws SPException {
        //this.ModeVILabel = mode;
        //ModeVI = (String) searchValue(ModeVILabels, ModeVIValues, mode, "ModeVI");
        spParamItemModeVI.setLabel(mode);
    }




    public void setOutGain(String outgain) throws SPException {
        //this.OutGainLabel = outgain;
        //OutGain = (String) searchValue(outgainLabels, outgainValues, outgain, "OutGain");
        spParamItemOutGain.setLabel(outgain);
    }

    /*
    1-78125: 		OSM=111
    78126-156250:  		OSM=110
    156251-312500:		OSM=101
    312501-625000:		OSM=100
    625001-1250000:		OSM=011
    1250001-2500000:	OSM=010
    2500001-10000000:	OSM=001
    0:					OSM=000
    */
    // TODO: AGGIORNARE MARCO
    public void setFrequency(Float frequency) throws SPException {
        int maxValue = 0;
        maxValue = SYS_CLOCK/2;

        this.FrequencyLabel = "" + frequency;
        if (frequency != null && inRange(0, maxValue, (int)Math.ceil(frequency))) {
            this.Frequency = frequency; //frequencyLimits[i];
        } else {
            throw new SPException("Frequency out of range: 0, " + maxValue/2);
        }
    }



    public String getRsense() throws SPException {
        return (String) spParamItemRSense.getValue();
    }


    public String getHarmonic()  throws SPException {
        return (String) spParamItemHarmonic.getValue();
    }

    public String getModeVI() throws SPException {
        return (String) spParamItemModeVI.getValue();
    }

    public Float getFrequency() {
        return Frequency;
    }

    public String getOutGain() throws SPException {
        return (String) spParamItemOutGain.getValue();
    }

    public String getInPort() {
        return InPort;
    }

    public String getOutPort() {
        return OutPort;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        double OSM;
        OSM = (double) Integer.parseInt("111", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("110", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("101", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("100", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("011", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("010", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("001", 2);
        System.out.println("OSM: " + OSM);
        OSM = (double) Integer.parseInt("000", 2);
        System.out.println("OSM: " + OSM);
    }

}
