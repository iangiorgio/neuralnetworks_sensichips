package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

import java.util.HashMap;
import java.util.Map;

public class SPParamItemMeasures extends SPParamItem {

    public final static int IN_PHASE = 0;
    public final static int QUADRATURE = 1;
    public final static int CONDUCTANCE = 2;
    public final static int SUSCEPTANCE = 3;
    public final static int MODULE = 4;
    public final static int PHASE = 5;
    public final static int RESISTANCE = 6;
    public final static int CAPACITANCE = 7;
    public final static int INDUCTANCE = 8;
    public final static int VOLTAGE = 9;
    public final static int CURRENT = 10;
    public final static int DELTA_V_APPLIED = 11;
    public final static int CURRENT_APPLIED = 12;


    //public static HashMap<Integer,String> measurementUnitsMap = new HashMap<>();


    public static final String[] unitMeasuresLabelsRUN5 = new String[]{"N","N","S","S",new String(Character.toChars(0x03a9)),"rad",
            new String(Character.toChars(0x03a9)),"F","H", "V", "A", "V","A"};

    public static final String[] unitMeasuresLabelsRUN6 = new String[]{"N","N","S","S",new String(Character.toChars(0x03a9)),"rad",
            new String(Character.toChars(0x03a9)),"F","H", "V", "A", "V","A"};
    public static String[] unitMeasuresLabels;


    public static final String[]  measureLabelsRUN5 = new String[]{"IN-PHASE", "QUADRATURE","CONDUCTANCE","SUSCEPTANCE","MODULE", "PHASE", "RESISTANCE", "CAPACITANCE", "INDUCTANCE", "VOLTAGE", "CURRENT", "DELTA_V_APPLIED","CURRENT_APPLIED"};
    public static final Integer[] measureValuesRUN5 = new Integer[]{IN_PHASE, QUADRATURE,CONDUCTANCE,SUSCEPTANCE, MODULE, PHASE, RESISTANCE, CAPACITANCE, INDUCTANCE, VOLTAGE, CURRENT,DELTA_V_APPLIED,CURRENT_APPLIED};

    public static final String[]  measureLabelsRUN6 = new String[]{"IN-PHASE", "QUADRATURE","CONDUCTANCE","SUSCEPTANCE","MODULE", "PHASE", "RESISTANCE", "CAPACITANCE", "INDUCTANCE", "VOLTAGE", "CURRENT", "DELTA_V_APPLIED","CURRENT_APPLIED"};
    public static final Integer[] measureValuesRUN6 = new Integer[]{IN_PHASE, QUADRATURE,CONDUCTANCE,SUSCEPTANCE, MODULE, PHASE, RESISTANCE, CAPACITANCE, INDUCTANCE, VOLTAGE, CURRENT,DELTA_V_APPLIED,CURRENT_APPLIED};


    public static String[] measureLabels = null;
    public static Integer[] measureValues = null;

    public SPParamItemMeasures(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            measureLabels = measureLabelsRUN5;
            measureValues = measureValuesRUN5;
            unitMeasuresLabels = unitMeasuresLabelsRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            measureLabels = measureLabelsRUN6;
            measureValues = measureValuesRUN6;
            unitMeasuresLabels = unitMeasuresLabelsRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(measureLabels, measureValues);

    }
}
