package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemModeVI extends SPParamItem {

    //private String ModeVI = null;
    //protected String ModeVILabel = null;
    public static final String[] ModeVILabelsRUN5 = new String[]{"VOUT_VIN", "VOUT_IIN", "IOUT_VIN", "IOUT_IIN"};
    public static final String[] ModeVIValuesRUN5 = new String[]{"01", "00", "11", "10"};


    public static final String[] ModeVILabelsRUN6 = new String[]{"VOUT_VIN", "VOUT_IIN", "IOUT_VIN", "IOUT_IIN"};
    public static final String[] ModeVIValuesRUN6 = new String[]{"01", "00", "11", "10"};

    public static String[] ModeVILabels = null;
    public static String[] ModeVIValues = null;


    public SPParamItemModeVI(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            ModeVILabels = ModeVILabelsRUN5;
            ModeVIValues = ModeVIValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            ModeVILabels = ModeVILabelsRUN6;
            ModeVIValues = ModeVIValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(ModeVILabels, ModeVIValues);

    }
}
