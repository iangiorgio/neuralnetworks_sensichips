package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemPhaseShiftMode extends SPParamItem {

    public final static int QUADRANT = 0;
    public final static int COARSE = 1;
    public final static int FINE = 2;

    //private Integer PhaseShiftMode = null;
    //protected String PhaseShiftModeLabel = null;
    public static final String[] PhaseShiftModeLabelsRUN5 = new String[]{"Quadrants", "Coarse", "Fine"};
    public static final Integer[] PhaseShiftModeValuesRUN5 = new Integer[]{QUADRANT, COARSE, FINE};

    public static final String[] PhaseShiftModeLabelsRUN6 = new String[]{"Quadrants", "Coarse", "Fine"};
    public static final Integer[] PhaseShiftModeValuesRUN6 = new Integer[]{QUADRANT, COARSE, FINE};

    public static String[] PhaseShiftModeLabels;
    public static Integer[] PhaseShiftModeValues;

    public SPParamItemPhaseShiftMode(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            PhaseShiftModeLabels = PhaseShiftModeLabelsRUN5;
            PhaseShiftModeValues = PhaseShiftModeValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            PhaseShiftModeLabels = PhaseShiftModeLabelsRUN6;
            PhaseShiftModeValues = PhaseShiftModeValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(PhaseShiftModeLabels, PhaseShiftModeValues);

    }
}
