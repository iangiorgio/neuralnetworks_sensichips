package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemOutGain extends SPParamItem {


    // Generale
    //private String OutGain = null;
    //protected String OutGainLabel = null;
    public static final String[] outgainLabelsRUN5 = new String[]{"0", "1", "2", "3", "4", "5", "6", "7"};
    public static final String[] outgainValuesRUN5 = new String[]{"000", "001", "010", "011", "100", "101", "110", "111"};

    public static final String[] outgainLabelsRUN6 = new String[]{"0", "1", "2", "3", "4", "5", "6", "7"};
    public static final String[] outgainValuesRUN6 = new String[]{"000", "001", "010", "011", "100", "101", "110", "111"};

    public static String[] outgainLabels = null;
    public static String[] outgainValues = null;


    public SPParamItemOutGain(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            outgainLabels = outgainLabelsRUN5;
            outgainValues = outgainValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            outgainLabels = outgainLabelsRUN6;
            outgainValues = outgainValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(outgainLabels, outgainValues);

    }
}
