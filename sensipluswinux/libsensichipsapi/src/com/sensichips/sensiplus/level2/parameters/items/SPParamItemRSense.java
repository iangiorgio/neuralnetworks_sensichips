package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemRSense extends SPParamItem {


    public static final String[] rsenseLabelsRUN5 = new String[]{"50000", "5000", "500", "50"};
    public static final String[] rsenseValuesRUN5 = new String[]{"00", "01", "10", "11"};

    public static final String[] rsenseLabelsRUN6 = new String[]{"50000", "5000", "500", "50"};
    public static final String[] rsenseValuesRUN6 = new String[]{"00", "01", "10", "11"};

    public static String[] rsenseLabels;
    public static String[] rsenseValues;


    public SPParamItemRSense(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            rsenseLabels = rsenseLabelsRUN5;
            rsenseValues = rsenseValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            rsenseLabels = rsenseLabelsRUN6;
            rsenseValues = rsenseValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(rsenseLabels, rsenseValues);

    }

}
