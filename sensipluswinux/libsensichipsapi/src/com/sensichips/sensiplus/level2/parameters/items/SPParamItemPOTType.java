package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemPOTType extends SPParamItem {
    public final static int POTENTIOMETRIC = 0;
    public final static int CURRENT = 1;
    public final static int LINEAR_SWEEP_VOLTAMMETRY = 2;
    public final static int STAIRCASE_VOLTAMMETRY = 3;
    public final static int SQUAREWAVE_VOLTAMMETRY = 4;
    public final static int NORMAL_PULSE_VOLTAMMETRY = 5;
    public final static int DIFFERENTIAL_PULSE_VOLTAMMETRY = 6;

    public static final String[] typeLabelsRUN5 = new String[]{"LINEAR_SWEEP", "STAIRCASE",
            "SQUAREWAVE", "NORMAL_PULSE", "DIFFERENTIAL_PULSE", "POTENTIOMETRIC", "CURRENT"};
    public static final Integer[] typeValuesRUN5 = new Integer[]{LINEAR_SWEEP_VOLTAMMETRY, STAIRCASE_VOLTAMMETRY, SQUAREWAVE_VOLTAMMETRY,
            NORMAL_PULSE_VOLTAMMETRY, DIFFERENTIAL_PULSE_VOLTAMMETRY, POTENTIOMETRIC, CURRENT};


    public static final String[] typeLabelsRUN6 = new String[]{"LINEAR_SWEEP", "STAIRCASE",
            "SQUAREWAVE", "NORMAL_PULSE", "DIFFERENTIAL_PULSE", "POTENTIOMETRIC", "CURRENT"};
    public static final Integer[] typeValuesRUN6 = new Integer[]{LINEAR_SWEEP_VOLTAMMETRY, STAIRCASE_VOLTAMMETRY, SQUAREWAVE_VOLTAMMETRY,
            NORMAL_PULSE_VOLTAMMETRY, DIFFERENTIAL_PULSE_VOLTAMMETRY, POTENTIOMETRIC, CURRENT};


    public static String[] typeLabels = null;
    public static Integer[] typeValues = null;

    public SPParamItemPOTType(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            typeLabels = typeLabelsRUN5;
            typeValues = typeValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            typeLabels = typeLabelsRUN6;
            typeValues = typeValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(typeLabels, typeValues);

    }


}
