package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemPOTContacts extends SPParamItem {

    //private String Contacts = null;
    //protected String ContactsLabel = null;
    public static final String[] ContactsLabelsRUN5 = new String[]{"TWO", "THREE"};
    public static final String[] ContactsValuesRUN5 = new String[]{"1", "0"};


    public static final String[] ContactsLabelsRUN6 = new String[]{"TWO", "THREE"};
    public static final String[] ContactsValuesRUN6 = new String[]{"1", "0"};

    public static String[] ContactsLabels = null;
    public static String[] ContactsValues = null;

    public SPParamItemPOTContacts(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            ContactsLabels = ContactsLabelsRUN5;
            ContactsValues = ContactsValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            ContactsLabels = ContactsLabelsRUN6;
            ContactsValues = ContactsValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(ContactsLabels, ContactsValues);
    }

}
