package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPOffChipTGS8100Figaro;

import com.sensichips.sensiplus.level1.chip.SPSensingElement;
import com.sensichips.sensiplus.level1.chip.SPSensingElementOnChip;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterSENSORS extends SPMeasurementParameterADC {

    private String sensorName;
    //private String filter;
    private SPMeasurementParameterEIS paramInternalEIS = null;

    private SPOffChipTGS8100Figaro spOffChipTGS8100Figaro = null;

    public SPMeasurementParameterSENSORS(SPConfiguration spConfiguration, String sensorName) throws SPException {
        super(spConfiguration);
        this.sensorName = sensorName;

        SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(this.getSensorName());
        SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();


        if(spSensingElement.getMeasureTechnique().equals("EIS")) {
            this.paramInternalEIS = (SPMeasurementParameterEIS) spSensingElementOnChip.getSensingElementOnFamily().
                    getSPMeasurementParameter(spConfiguration);
            this.paramInternalEIS.setSPMeasurementMetaParameter(this.getSPMeasurementMetaParameter());
        }

        /*if(spSensingElement.getName().contains("FIGARO")){
            this.spOffChipTGS8100Figaro = spSensingElement.getSpOffChipTGS8100Figaro();
        }*/

        this.setFillBufferBeforeStart(spSensingElement.isFillBufferBeforeStart());
        this.setBurstMode(spSensingElement.isBurstMode());
        this.setFilter(spSensingElement.getFilter());
    }

    public SPOffChipTGS8100Figaro getSpOffChipTGS8100Figaro() {
        return spOffChipTGS8100Figaro;
    }

    public void setSpOffChipTGS8100Figaro(SPOffChipTGS8100Figaro spOffChipTGS8100Figaro)throws SPException{
        this.spOffChipTGS8100Figaro = spOffChipTGS8100Figaro;
        this.setFilter(spOffChipTGS8100Figaro.getFilter());
    }

    public SPMeasurementParameterEIS getParamInternalEIS() {
        return paramInternalEIS;
    }

    public void setParamInternalEIS(SPMeasurementParameterEIS paramInternalEIS) {
        this.paramInternalEIS = paramInternalEIS;
    }


    public String getSensorName(){
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    /*public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }*/

    public String getPort(){
        return spPort.getPortValue();
    }

}
