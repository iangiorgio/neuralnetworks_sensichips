package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;

import java.util.Arrays;

public abstract class SPParamItem {
    private String label;
    private Object value;

    private String[] labels;
    private Object[] values;

    private boolean numerical = false;
    private boolean initialized = false;

    private Number numericalValue;
    private Number min;
    private Number max;


    protected SPParamItem(){}


    public SPParamItem initialize(String[] labels, Object[] values) throws SPException {
        String message = getClass().getSimpleName();
        if (labels.length != values.length) {
            throw new SPException((message == null ? "" : message) + ": labels.length should be equal to values.length!");
        }
        this.labels = labels;
        this.values = values;
        numerical = false;
        initialized = true;
        return this;
    }

    public SPParamItem initialize(Number min, Number max) throws SPException {
        if (!minMaxOk(min, max)){
            throw new SPException("SPParamItem. " + getClass().getSimpleName() + " min should be <= of max: [" + min + ", " + max + "]");
        }
        numerical = true;

        this.min = min;
        this.max = max;
        initialized = true;
        return this;
    }


    public String getLabel() throws SPException {
        if (!initialized){
            throw new SPException("Uninitialized SPParameterItem");
        } else if (numerical){
            throw new SPException("This SPParamItem is numerical. getLabel not permitted.");
        }

        return label;
    }

    public SPParamItem setLabel(String label) throws SPException {
        if (!initialized){
            throw new SPException("Uninitialized SPParameterItem");
        } else if (numerical){
            throw new SPException("This SPParamItem is numerical. setLabel not permitted.");
        }
        value = searchValueFromLabel(label);
        this.label = label;
        return this;
    }

    public Object getValue() throws SPException {
        if (!initialized){
            throw new SPException("Uninitialized SPParameterItem");
        } else if (numerical){
            throw new SPException("This SPParamItem is numerical. getValue not permitted.");
        }
        return value;
    }

    public int getIndexFromLabel(Object label) throws SPException {
        return searchIndex(labels, values, label, "getIndexFromLabel");
    }

    public SPParamItem setValue(Object value) throws SPException {
        if (!initialized){
            throw new SPException("Uninitialized SPParameterItem");
        } else if (numerical){
            throw new SPException("This SPParamItem is numerical. setValue not permitted.");
        }
        label = (String) searchLabelFromValue(value);
        this.value = value;
        return this;
    }


    public Number getNumericalValue() throws SPException {
        if (!initialized){
            throw new SPException("Uninitialized SPParameterItem");
        } else if (!numerical){
            throw new SPException("This SPParamItem is not numerical. getNumericalValue not permitted.");
        } else if (numericalValue == null){
            throw new SPException("Numerical value is null. Use before setNumericalValue()");
        }
        return numericalValue;
    }


    public SPParamItem setNumericalValue(Number numericalValue) throws SPException {
        if (!initialized){
            throw new SPException("Uninitialized SPParameterItem");
        } else if (!numerical){
            throw new SPException("This SPParamItem is not numerical. setNumericalValue not permitted.");
        }
        this.numericalValue = numericalValue;
        return this;
    }

    private Object searchValueFromLabel(Object value) throws SPException {
        String message = "Search value from label in: " + getClass().getSimpleName();
        return search(labels, values, value, message);
    }

    private Object searchLabelFromValue(Object value) throws SPException {
        String message = "Search label from value in: " + getClass().getSimpleName();
        return search(values, labels, value, message);

    }


    private static int searchIndex(Object labels[], Object values[], Object label, String message) throws SPException {

        if (label == null || labels == null || values == null) {
            throw new SPException((message == null ? "" : message) + ": Null values are refused!");
        }
        if (labels.length != values.length) {
            throw new SPException((message == null ? "" : message) + ": labels.length should be equal to values.length!");
        }

        int count = 0;
        while (count < labels.length) {
            if (label.equals(labels[count])) {
                return count;
            }
            count++;
        }
        throw new SPException(message + " " + label + " is not in: " + Arrays.toString(labels));
    }



    private static Object search(Object labels[], Object values[], Object label, String message) throws SPException {
        return values[searchIndex(labels, values, label, message)];
    }

    protected static boolean minMaxOk(Number min, Number max) throws SPException {
        if (min instanceof Integer && max instanceof Integer ){
            return min.intValue() <= max.intValue();
        } else if (min instanceof Double && max instanceof Double){
            return min.doubleValue() <= max.doubleValue();
        } else if (min instanceof Float && max instanceof Float) {
            return min.floatValue() <= max.floatValue();
        } else {
            throw new SPException("SPParamItem: type not permitted. Only Integer, Double or Float");
        }
    }
    protected static boolean inRange(Number min, Number max, Number value) throws SPException {
        if (min instanceof Integer && max instanceof Integer && value instanceof Integer){
            return value.intValue() >= min.intValue() && value.intValue() <= max.intValue();
        } else if (min instanceof Double && max instanceof Double && value instanceof Double){
            return value.doubleValue() >= min.doubleValue() && value.doubleValue() <= max.doubleValue();
        } else if (min instanceof Float && max instanceof Float && value instanceof Float) {
            return value.floatValue() >= min.floatValue() && value.floatValue() <= max.floatValue();
        } else {
            throw new SPException("SPParamItem: type not permitted. Only Integer, Double or Float");
        }
    }
}
