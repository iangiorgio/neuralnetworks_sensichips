package com.sensichips.sensiplus.level2.parameters;


import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPPort;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public abstract class SPMeasurementParameterPort extends SPMeasurementParameter {

    public SPMeasurementParameterPort(SPConfiguration spConfiguration) throws SPException {
        super(spConfiguration);
    }

    public SPMeasurementParameterPort(){

    }


    //protected String Port = null;
    //private String PortLabel = null;
    protected SPPort spPort = null;

    //private static int MAX_INTERNAL_INDEX = 12;

    //public static final String[] portLabels = new String[]{"PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3",  "PORT_TEMPERATURE", "PORT_VOLTAGE", "PORT_LIGHT",   "PORT_DARK"};
    //														"0x00",  "0x01",  "0x02",  "0x03",  "0x04",  "0x05" , "0x06",  "0x07",  "0x08",  "0x09",  "0x0A",   "0x0B",   "0x1C",    "0x1D",      "0x1E",      "0x1F",      "0x0D",             "0x0C",         "0x0E",         "0x0F"};
    //public static final String[] portValues = new String[]{"00000", "00001", "00010", "00011", "00100", "00101", "00110", "00111", "01000", "01001", "01010",  "01011",  "11100",   "11101",     "11110",     "11111",      "01101",            "01100",        "01110",        "01111"};



    public void setPort(String port) throws SPException {
        //PortLabel = port;
        spPort = new SPPort(port);
        //Port = (String) searchValue(portLabels, portValues, port, "Port");
    }

    public String getPort() {
        return spPort.getPortValue();
    }

    public boolean portIsInternal() throws SPException {
        if (spPort == null){
            throw new SPException("Port is null in SPMeasurementParameter!");
        }
        return spPort.isInternal();
    }


}
