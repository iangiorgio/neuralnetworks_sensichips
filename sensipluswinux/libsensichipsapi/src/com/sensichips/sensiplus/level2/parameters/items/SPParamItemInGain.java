package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemInGain extends SPParamItem {

    public static Integer INGAIN_1_INDEX = 0;
    public static Integer INGAIN_12_INDEX = 1;
    public static Integer INGAIN_20_INDEX = 2;
    public static Integer INGAIN_40_INDEX = 3;

    public static final String[] ingainLabelsRUN5 = new String[]{"1", "12", "20", "40"};
    public static final String[] ingainValuesRUN5 = new String[]{"11", "10", "01", "00"};

    public static final String[] ingainLabelsRUN6 = new String[]{"1", "12", "20", "40"};
    public static final String[] ingainValuesRUN6 = new String[]{"11", "10", "01", "00"};

    public static String[] ingainLabels = null;
    public static String[] ingainValues = null;

    public SPParamItemInGain(SPFamily family) throws SPException {
        if (family.RUN5.equalsIgnoreCase(family.getHW_VERSION())){
            ingainLabels = ingainLabelsRUN5;
            ingainValues = ingainValuesRUN5;
        } else if (family.RUN6.equalsIgnoreCase(family.getHW_VERSION())){
            ingainLabels = ingainLabelsRUN6;
            ingainValues = ingainValuesRUN6;
        } else {
            throw new SPException(getClass().getSimpleName() + ", SPFamily unavailable");
        }
        initialize(ingainLabels, ingainValues);

    }
}
