package com.sensichips.sensiplus.level2.parameters;

import java.util.ArrayList;

public class SPMeasurementSequential {

    private ArrayList<String> beforeInstruction = new ArrayList<>();
    private ArrayList<String> afterInstruction = new ArrayList<>();
    private boolean active;

    public SPMeasurementSequential(boolean active) {
        this.active = active;
    }

    public void addBeforeInstruction(String instruction){
        beforeInstruction.add(instruction);
    }

    public void addAfterInstruction(String instruction){
        afterInstruction.add(instruction);
    }

    public String getBeforeInstruction(int index){
        return beforeInstruction.get(index);
    }

    public String getAfterInstruction(int index){
        return afterInstruction.get(index);
    }

    public int sizeAfterInstruction(){
        return afterInstruction.size();
    }

    public int sizeBeforeInstruction(){
        return beforeInstruction.size();
    }

    public boolean getActive(){
        return active;
    }


}
