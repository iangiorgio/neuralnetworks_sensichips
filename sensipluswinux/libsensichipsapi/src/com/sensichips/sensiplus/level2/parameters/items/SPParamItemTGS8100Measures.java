package com.sensichips.sensiplus.level2.parameters.items;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPFamily;

public class SPParamItemTGS8100Measures extends SPParamItem {


    public static final Integer NUM_OF_OUTPUT = 4;
    public static final Integer RESISTANCE_SENSOR = 0;
    public static final Integer CAPACITANCE = 1;

    public static final Integer TEMPERATURE = 2;
    public static final Integer RESISTANCE_HEATER = 3;



    public static final String[] unitMeasuresLabelsFigaro = new String[]{new String(Character.toChars(0x03a9)),"pF","°C",new String(Character.toChars(0x03a9))};

    public static final String[]  measureLabelsRUNx = new String[]{"RESISTANCE_SENSOR", "CAPACITANCE","TEMPERATURE","RESISTANCE_HEATER"};
    public static final Integer[] measureValuesRUNx = new Integer[]{RESISTANCE_SENSOR, CAPACITANCE,TEMPERATURE,RESISTANCE_HEATER};

    public static String[] measureLabels = null;
    public static Integer[] measureValues = null;

    public SPParamItemTGS8100Measures(SPFamily family) throws SPException {
        measureLabels = measureLabelsRUNx;
        measureValues = measureValuesRUNx;
        initialize(measureLabels, measureValues);
    }

}
