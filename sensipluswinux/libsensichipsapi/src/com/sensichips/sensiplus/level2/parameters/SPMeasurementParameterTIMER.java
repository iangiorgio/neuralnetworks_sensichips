package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterTIMER extends SPMeasurementParameter {

    private String ModeTimer = null;
    private Integer Timer = null;

    public static final Integer timerMin = 0;
    public static final Integer timerMax = (int) Math.pow(2, 24) - 1;

    // Parameter defined by set of labels and values
    public static final String[] modetimerLabels = new String[]{"MODE_TIMER", "HITS_TIMER", "SCHEDULED_CONVERSION"};
    private static final String[] modetimerValues = new String[]{"MODE_TIMER", "HITS_TIMER", "SCHEDULED_CONVERSION"};


    public boolean isValid() {
        return !(getModeTimer() == null || getTimer() == null);
    }


    public SPMeasurementParameterTIMER(SPConfiguration conf) throws SPException {
        super(conf);
    }

    //MODE_TIMER, HITS_TIMER, SCHEDULED_CONVERSION=0x08

    public void setModeTimer(String ModeTimer) throws SPException {

        this.ModeTimer = (String) searchValue(modetimerLabels, modetimerValues, ModeTimer, "ModeTimer");

    }

    public void setTimer(Integer timerMax) throws SPException {

        if (timerMax != null && inRange((int)timerMin, (int)timerMax, Timer))
            this.Timer = Timer;
        else
            throw new SPException("Timer " + Timer + " out of range: [" + timerMin + ", " + timerMax + "]");
    }

    public String getModeTimer() {
        return ModeTimer;
    }

    public Integer getTimer() {
        return Timer;
    }

}
