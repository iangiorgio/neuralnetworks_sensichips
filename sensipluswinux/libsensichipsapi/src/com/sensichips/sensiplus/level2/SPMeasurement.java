package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.protocols.util.SPProtocolOut;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level2.parameters.*;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara, mario.molinara@sensichips.com, m.molinara@unicas.it
 */
public abstract class SPMeasurement {


    private int measureIndexToSave = -1;
    public static int COUNTER_ID = 0; //Added 17/05/2019 Luca
    protected int instance_ID = 0; //Added 17/05/2019 Luca

    protected SPMeasurementParameter lastParameter = null;

    private boolean automaticADCRangeAdjustment = false;

    public static String LOG_MESSAGE = "SPMeasurement";

    public static final int NUM_OF_BIT = 16;
    public static final double NORMALIZATION_FACTOR = Math.pow(2, NUM_OF_BIT - 1);

    // Internal status
    protected String logDirectory;
    protected SPProtocol spProtocol;
    protected SPConfiguration spConfiguration;

    protected SPAutoRangeAlgorithm autoRangeAlgorithm;

    public boolean isSystemLevelChopperActive() {
        return systemLevelChopperActive;
    }

    public void setSystemLevelChopperActive(boolean systemLevelChopperActive) {
        this.systemLevelChopperActive = systemLevelChopperActive;
    }

    private boolean systemLevelChopperActive = false;



    protected SPMeasurement(SPConfiguration spConfiguration, String logDirectory, String MEASURE_TYPE) throws SPException {

        if (spConfiguration == null || spConfiguration.getSPProtocol() == null) {
            throw new SPException("Mandatory parameter is null in constructor method SPMeasurement");
        }



        spConfiguration.verifyMeasurementType(MEASURE_TYPE);

        this.spConfiguration = spConfiguration;
        this.logDirectory = logDirectory;
        this.spProtocol = spConfiguration.getSPProtocol();

        if (this.spConfiguration.getCluster().getActiveSPChipList().size() <= 0){
            throw new SPException("SPMeasurement cannot be instantiated with num of chips < 1");
        }
        //Autorangev2
        //autoRangeAlgorithm = new SPAutoRangeAlgorithmV2(this,true,spConfiguration.getVREF());

        //Autorangev1
        autoRangeAlgorithm = new SPAutoRangeAlgorithm(this);

    }

    //Added 17/05/2019 Luca
    public static void resetCounterID(){
        COUNTER_ID = 0;
    }

    public int getMeasureIndexToSave() {
        return measureIndexToSave;
    }

    public void setMeasureIndexToSave(int measureToSave) {
        this.measureIndexToSave = measureToSave;
    }

    public boolean isAutomaticADCRangeAdjustment() {
        return automaticADCRangeAdjustment;
    }

    public void setAutomaticADCRangeAdjustment(boolean automaticADCRangeAdjustment) {
        this.automaticADCRangeAdjustment = automaticADCRangeAdjustment;
    }

    public SPProtocol getSPProtocol() {
        return spProtocol;
    }


    public int getClusterSize(){
        return spConfiguration.getCluster().getActiveSPChipList().size();
    }


    /**

     * @param ModeTimer
     * @param Timer
     * @param message
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract void setTimer(String ModeTimer, Integer Timer, SPMeasurementOutput message)
            throws SPException;



    /**
     * @param param
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    public synchronized void setTimer(SPMeasurementParameterTIMER param, SPMeasurementOutput m)
            throws SPException {
        throw new SPException("Not yet implemented");

    }

    /*************************************************************************************************
     *************************************************************************************************
     *									ULTRASOUND
     *************************************************************************************************
     *************************************************************************************************
     */





    /**
     * The setINIT method allows initializing the SENSIPLUS chip to a known state. It will typically soft reset the chip, configure Interrupt settings and initialize all global constants.
     *
     * @param ModeVI
     * @param InPort
     * @param OutPort
     * @param Frequency
     * @param Pulse_Repetition
     * @param Burst
     * @param TX_Start
     * @param RX_Start
     * @param Probe
     * @param InGain
     * @param OutGain
     * @param PI_Toggle
     * @param QI_Toggle
     * @param Harmonic
     * @param message          log messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    public synchronized void setULTRASOUND(String ModeVI, String InPort, String OutPort, Float Frequency,
                              Integer Pulse_Repetition, Integer Burst, Integer TX_Start, Integer RX_Start,
                              Integer Probe, String InGain, String OutGain, Boolean PI_Toggle, Boolean QI_Toggle, String Harmonic,
                              SPMeasurementOutput message)
            throws SPException {

        SPMeasurementParameterULTRASOUND param = new SPMeasurementParameterULTRASOUND(spConfiguration);
        param.setModeVI(ModeVI);
        param.setInPort(InPort);
        param.setOutPort(OutPort);
        param.setFrequency(Frequency);
        param.setPulse_Repetition(Pulse_Repetition);
        param.setBurst(Burst);
        param.setTx_Start(TX_Start);
        param.setRX_Start(RX_Start);
        param.setProbe(Probe);
        param.setOutGain(OutGain);
        param.setInGain(InGain);
        param.setPI_Toggle(PI_Toggle);
        param.setQI_Toggle(QI_Toggle);
        param.setHarmonic(Harmonic);
        setULTRASOUND(param, message);

    }


    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract void setULTRASOUND(SPMeasurementParameterULTRASOUND param,
                                       SPMeasurementOutput m) throws SPException; //end setEIS

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract double[][] getULTRASOUND(SPMeasurementParameterULTRASOUND param, SPMeasurementOutput m)
            throws SPException;

    /**
     * The setINIT method allows initializing the SENSIPLUS chip to a known state. It will typically soft reset the chip, configure Interrupt settings and initialize all global constants.
     *
     * @param ModeVI
     * @param InPort
     * @param OutPort
     * @param Frequency
     * @param Pulse_Repetition
     * @param Burst
     * @param TX_Start
     * @param RX_Start
     * @param Probe
     * @param InGain
     * @param OutGain
     * @param PI_Toggle
     * @param QI_Toggle
     * @param Harmonic
     * @param message          log messages
     * @return
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    public synchronized double[][] getULTRASOUND(String ModeVI, String InPort, String OutPort, Float Frequency,
                                    Integer Pulse_Repetition, Integer Burst, Integer TX_Start, Integer RX_Start,
                                    Integer Probe, String InGain, String OutGain, Boolean PI_Toggle, Boolean QI_Toggle, String Harmonic,
                                    SPMeasurementOutput message)
            throws SPException {

        SPMeasurementParameterULTRASOUND param = new SPMeasurementParameterULTRASOUND(spConfiguration);
        param.setModeVI(ModeVI);
        param.setInPort(InPort);
        param.setOutPort(OutPort);
        param.setFrequency(Frequency);
        param.setPulse_Repetition(Pulse_Repetition);
        param.setBurst(Burst);
        param.setTx_Start(TX_Start);
        param.setRX_Start(RX_Start);
        param.setProbe(Probe);
        param.setOutGain(OutGain);
        param.setInGain(InGain);
        param.setPI_Toggle(PI_Toggle);
        param.setQI_Toggle(QI_Toggle);
        param.setHarmonic(Harmonic);
        return getULTRASOUND(param, message);

    }

    /*************************************************************************************************
     *************************************************************************************************
     *									EIS
     *************************************************************************************************
     *************************************************************************************************
     */


    /**
     * The setEIS method allows setting all required parameters to perform Electric Impedance Spectroscopy
     * (EIS) measurements.
     *
     * @param InPort    is the input port for the measurement. It can be assigned with any of the following
     *                  16 values: Port0-11, Port_HP, Port_Ext1-3.
     * @param OutPort   is the output port to provide the stimulus for the measurement. It can be assigned with any of
     *                  the following 16 values: Port0-11, Port_HP, Port_Ext1-3.
     * @param Frequency allows setting the frequency of the stimulus Sinewave used for the Electric Impedance
     *                  Spectroscopy measurement. It can be set to any value ranging from 0 to SYS_CLOCK/2.
     *                  The method will return the closest permissible value that was actually programmed in the chip.
     * @param DCBias    is a DC offset that can be added or subtracted from the stimulus Sinwewave. DCBias can be
     *                  set to any value from +2047 to -2048.
     * @param ModeVI    sets input and output measurement mode in Voltage or Current. It can be assigned with any of
     *                  the following four values: VOUT_VIN, VOUT_IIN, IOUT_VIN, IOUT_IIN.
     * @param Contacts  number of contacts for the EIS measurement. It can be assigned with any of
     *                  the following two values: 2_CONTACTS, 4_CONTACTS.
     * @param OutGain   sets the peak-to-peak amplitude of the output Sinewave. OutGain can be set to any value
     *                  from 0 to 7 and it will scale the output peak-to-peak voltage as follows: Vpp = VREF/(8-OutGain).
     * @param Harmonic  sets the demodulation harmonic. It can be assigned with any of the following three values:
     *                  FIRST_HARMONIC, SECOND_HARMONIC, THIRD_HARMONIC.
     * @param I_Q       sets the demodulation quadrant. It can be assigned with either of the following two values:
     *                  IN_PHASE, QUADRATURE
     * @param InGain    sets the gain factor of the input Instrumentation Amplifier. It can be assigned
     *                  with any of the following four values: X01, X12, X20, X40.
     * @param filter    set the mobile average dimensionality.
     * @param message   messageToSend feedback from this method
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     * @see #setEIS(SPMeasurementParameterEIS, SPMeasurementOutput)
     */
    public synchronized void setEIS(String InPort, String OutPort, Float Frequency,
                        Integer DCBias, String ModeVI, String Contacts, String OutGain,
                        String Harmonic, String I_Q, String InGain, String RSense, String filter, Integer PhaseShift, String PhaseShiftMode,
                        SPMeasurementOutput message)
            throws SPException {

        SPMeasurementParameterEIS param = new SPMeasurementParameterEIS(spConfiguration);
        param.setInPort(InPort);
        param.setOutPort(OutPort);
        param.setFrequency(Frequency);
        param.setDCBiasP(DCBias);
        param.setModeVI(ModeVI);
        param.setContacts(Contacts);
        param.setOutGain(OutGain);
        param.setHarmonic(Harmonic);
        param.setRsense(RSense);
        param.setInGain(InGain);
        param.setFilter(filter);
        param.setPhaseShiftQuadrants(PhaseShiftMode, PhaseShift, I_Q);
        setEIS(param, message);
    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract void setEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m)
            throws SPException; //end setEIS




    public abstract double[][] getSingleEIS(SPMeasurementParameterEIS param, boolean ADCAdjustment ,SPMeasurementOutput m) throws SPException;

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract double[][] getEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m)
            throws SPException;


    /**
     * The setINIT method allows initializing the SENSIPLUS chip to a known state. It will typically soft reset the chip, configure Interrupt settings and initialize all global constants.
     *
     * @param message   log messages
     * @param InPort
     * @param OutPort
     * @param Frequency
     * @param DCBias
     * @param ModeVI
     * @param Contacts
     * @param OutGain
     * @param Harmonic
     * @param I_Q
     * @param InGain
     * @param avg
     * @param message
     * @return
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    public synchronized double[][] getEIS(String InPort, String OutPort, Float Frequency,
                              Integer DCBias, String ModeVI, String Contacts, String OutGain,
                              String Harmonic, String I_Q, String InGain, String avg, Integer PhaseShift, String PhaseShiftMode,
                              SPMeasurementOutput message)
            throws SPException {

        SPMeasurementParameterEIS param = new SPMeasurementParameterEIS(spConfiguration);
        //param.setOutputDecimation(outputDecimation);
        param.setInPort(InPort);
        param.setOutPort(OutPort);
        param.setFrequency(Frequency);
        param.setDCBiasP(DCBias);
        param.setModeVI(ModeVI);
        param.setContacts(Contacts);
        param.setOutGain(OutGain);
        param.setHarmonic(Harmonic);
        param.setPhaseShiftQuadrants(PhaseShiftMode, PhaseShift, I_Q);
        param.setInGain(InGain);
        return getEIS(param, message);
    }





    /*************************************************************************************************
     *************************************************************************************************
     *									ADC
     *************************************************************************************************
     *************************************************************************************************
     */
    /**
     * The setINIT method allows initializing the SENSIPLUS chip to a known state. It will typically soft reset the chip, configure Interrupt settings and initialize all global constants.
     *
     * @param FIFO_DEEP
     * @param Conversion_Rate
     * @param message      messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    public synchronized void setADC(Integer FIFO_DEEP, Integer FIFO_READ, String InGain, Double Conversion_Rate, String InPort, Boolean CommandX20, SPMeasurementOutput message)
            throws SPException {
        SPMeasurementParameterADC param = new SPMeasurementParameterADC(spConfiguration, FIFO_DEEP, FIFO_READ, InGain, Conversion_Rate, InPort, CommandX20);
        setADC(param, message);
    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m)
            throws SPException;


    /*************************************************************************************************
     *************************************************************************************************
     *									POT
     *************************************************************************************************
     *************************************************************************************************
     */



    /**
     * @param InOutPort
     * @param Type
     * @param InitialPotential
     * @param FinalPotential
     * @param step
     * @param PulsePeriod
     * @param PulseAmplitude
     * @param alternativeSignal
     * @param rsense
     * @param InGain
     * @param message           log messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    public synchronized void setPOT(String InOutPort, String Type, Integer InitialPotential, Integer FinalPotential,
                        Integer step, Integer PulsePeriod, Integer PulseAmplitude, Boolean alternativeSignal, String rsense, String InGain, String contacts,
                        SPMeasurementOutput message)
            throws SPException {
        SPMeasurementParameterPOT param = new SPMeasurementParameterPOT(spConfiguration, InOutPort, Type, InitialPotential, FinalPotential,
                step, PulsePeriod, PulseAmplitude, alternativeSignal, rsense, InGain, contacts);
        setPOT(param, message);

    }


    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract void setPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m)
            throws SPException;

    /**

     * @param InOutPort
     * @param Type
     * @param InitialPotential
     * @param FinalPotential
     * @param step
     * @param PulsePeriod
     * @param PulseAmplitude
     * @param alternativeSignal
     * @param rsense
     * @param InGain
     * @param message
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract double[][] getPOT(String InOutPort, String Type, Integer InitialPotential, Integer FinalPotential,
                                    Integer step, Integer PulsePeriod, Integer PulseAmplitude, Boolean alternativeSignal, String rsense, String InGain,
                                      SPMeasurementOutput message)
            throws SPException;

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract double[][] getPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m)
            throws SPException;

    /*************************************************************************************************
     *************************************************************************************************
     *									getData
     *************************************************************************************************
     *************************************************************************************************
     */

    /**
     * @param message   log messages
     * @return
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    public synchronized int[][] getDATA(SPMeasurementParameter param, Boolean CommandX20, Boolean inPhase, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput message)
            throws SPException {
        //SPMeasurementParameterADC p = new SPMeasurementParameterADC(spConfiguration);
        //((SPMeasurementParameterADC)param).setNData(NData);
        ((SPMeasurementParameterADC)param).setCommandX20(CommandX20);
        return getDATA(param, inPhase, spMeasurementSequential, message);
    }

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract int[][] getDATA(SPMeasurementParameter param, Boolean inPhase, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m)
            throws SPException;


    /*************************************************************************************************
     *************************************************************************************************
     *									PPR
     *************************************************************************************************
     *************************************************************************************************
     */

    /**
     * @param InPort
     * @param Mode
     * @param EnergyThreshold
     * @param HitsThreshold
     * @param message                  messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    public synchronized void setPPR(String Electrodes, String DetectorType, String InPort, Integer Timer, Integer EnergyThreshold,
                        Integer HitsThreshold, String Mode, String DAO, SPMeasurementOutput message)
            throws SPException {
        SPMeasurementParameterPPR param = new SPMeasurementParameterPPR(spConfiguration, Electrodes, DetectorType, InPort, Timer, EnergyThreshold, HitsThreshold, Mode, DAO);
        setPPR(param, message);
    }



    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract void setPPR(SPMeasurementParameterPPR param,
                                SPMeasurementOutput m)
            throws SPException;



    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract SPMeasurementOutputPPR[] getPPR(SPMeasurementParameterPPR param,
                                                    SPMeasurementOutput m)
            throws SPException;



    /**
     *
     * @param Electrodes
     * @param DetectorType
     * @param InPort
     * @param Timer
     * @param EnergyThreshold
     * @param HitsThreshold
     * @param Mode
     * @param message
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public synchronized SPMeasurementOutputPPR[] getPPR(String Electrodes, String DetectorType, String InPort, Integer Timer, Integer EnergyThreshold,
                                           Integer HitsThreshold, String Mode, String DAO,

                                           SPMeasurementOutput message)
            throws SPException {

        SPMeasurementParameterPPR param = new SPMeasurementParameterPPR(spConfiguration, Electrodes, DetectorType,
                InPort, Timer, EnergyThreshold, HitsThreshold, Mode, DAO);
        return getPPR(param, message);
    }


    public synchronized String hitCalibration(SPCluster temporaryCluster, String hit) throws SPException {
        SendInstructionInOut ref = new SendInstructionInOut(temporaryCluster);
        if (hit.equals("HitL")){
            ref.instructionToSend = "WRITE COMMAND s 0xFE";
        } else if (hit.equals("HitH")){
            ref.instructionToSend = "WRITE COMMAND s 0xFC";
        } else {
            throw new SPException("Available hit value: HitL, HitH but you send " + hit);
        }
        sendSequence(ref);
        return ref.log;
    }


    protected void sendSequence(SendInstructionInOut ref)
            throws SPException {

        ref.exceptionMessage += ref.instructionToSend + "\n";
        SPProtocolOut out = spProtocol.sendInstruction(ref.instructionToSend, ref.temporaryCluster);
        ref.recValues = out.recValues;
        ref.log += ref.instructionToSend + "\n" + ref.recValuesToString() + "\n";
    }

    protected void sendSequences(ArrayList<String> instructionList, SendInstructionInOut ref, String mes)
            throws SPException {

        ref.log = "-----------------------------\n" + mes + ":\n";
        for(int i = 0; i < instructionList.size(); i++){
            ref.instructionToSend = instructionList.get(i);
            sendSequence(ref);
        }
        ref.log += "END " + mes + ".\n-----------------------------\n";

    }


    public abstract void setACTUATOR(SPMeasurementParameterActuator parameterActuator) throws SPException;

    public abstract void setACTUATOR(SPMeasurementParameterActuator parameterActuator, SPCluster temporaryCluster) throws SPException;


    public abstract double[][] getSENSOR(SPMeasurementParameterSENSORS sensorParam, SPMeasurementOutput m)
            throws SPException;

    public abstract void setSENSOR(SPMeasurementParameterSENSORS parameter, SPMeasurementOutput m)
            throws SPException;

    /*************************************************************************************************
     *************************************************************************************************
     *									timerTest
     *************************************************************************************************
     *************************************************************************************************
     */
    /**
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    public abstract void timerTest(SPMeasurementOutput m)
            throws SPException;


   
    public abstract void autoRange(SPMeasurementParameterEIS paramEIS)
            throws SPException;
    /*************************************************************************************************
     * ************************************************************************************************
     * Available frequency
     * ************************************************************************************************
     * ************************************************************************************************
     */
    public static class AvailablePhaseShift{
        public static float stepQ = 90;
        public static float stepC = 5;
        public static float stepF = 1;

        public static void updateSteps(AvailableFrequencies fc) {
            double FRD_fraz = fc.FRD == 1 ? 2.0/3.0 : 1.0;

            stepQ = 90;
            stepC = (float)(360/Math.pow(2, fc.OSM));
            stepF = (float)(360/(Math.pow(2, fc.OSM) * FRD_fraz * fc.DSS_DIVIDER));

        }


    }


    /**
     *
     */
    public static class AvailableFrequencies {

        public Integer FRD;
        public Float OSM;
        public Integer DSS_DIVIDER;
        public Float availableFrequency;


        private NumberFormat formatter = new DecimalFormat("#0.000");

        public String toString(){
            return "" + (int)OSM.doubleValue() + ";" + DSS_DIVIDER + ";" +  FRD + ";" +  availableFrequency + ";";
        }


        private static float log(float x, float base)
        {
            return (float) Math.log(x) / (float) Math.log(base);
        }
        private final static int limits0 = 40;
        private final static int limits1 = 1000; // Divisione in tre aree: f < 1000, 1000 <= f <= 78125 e f > 78125
        private final static int limits2 = 78125;

        private final static float freqErrThresholdPerc = 5; // Percentuale di errore ammesso rispoetto alla frequenza richiesta
        private final static int DSS_MAX = 32767;


        private static int NUM_OF_DECIMAL = 2;
        private static float MYROUND = (float)Math.pow(10,NUM_OF_DECIMAL);

        /*
        Valori degli step per f > 78125
            104166:			OSM=110 FRD=2/3
            156250:  		OSM=110 FRD=1
            208333:			OSM=101 FRD=2/3
            312500:			OSM=101	FRD=1
            416666:			OSM=100	FRD=2/3
            625000:			OSM=100	FRD=1
            833333:			OSM=011 FRD=2/3
            1250000:		OSM=011 FRD=1
            1666666:		OSM=010 FRD=2/3
            2500000:		OSM=010 FRD=1
            3333333:		OSM=001 FRD=2/3
            5000000:		OSM=001 FRD=1
        */
        private static float[] F_FRA = {104166, 208333, 416666, 833333, 1666666, 3333333};
        private static float[] F_INT = {156250, 312500, 625000, 1250000, 2500000, 5000000};



        public static void availableFrequencies(SPConfiguration conf, Float frequencyVal, AvailableFrequencies frequencyCalc) throws SPException {



            if (frequencyVal < 0){
                throw new SPException("Negative frequency are not allowed: " + frequencyVal);
            }

            double SYS_CLOCK_FIELD = conf.getCluster().getActiveSPChipList().get(0).getSpFamily().getSYS_CLOCK();

            if (frequencyVal == 0) {
                frequencyCalc.FRD = 0;
                frequencyCalc.OSM = (float) 0;
                frequencyCalc.DSS_DIVIDER = 2;     // 0x14 DSS_DIVIDER PER RUN 4; TODO: Verificare il valore per RUN5 che dovrebbe essere 0x02
                frequencyCalc.availableFrequency = (float) 0;

                // Required frequency > 0
            } else {
                if(frequencyVal<=limits0){
                    float DSS_DIVIDER_int;
                    float DSS_DIVIDER_fraz;
                    float FREQUENCY_int;
                    float FREQUENCY_fraz;
                    float FRD_fraz = 1;

                    frequencyCalc.OSM = (float) 7.0;

                    DSS_DIVIDER_int = Math.round(SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * frequencyVal*512.0)));
                    DSS_DIVIDER_int = DSS_DIVIDER_int > DSS_MAX ? DSS_MAX: DSS_DIVIDER_int;


                    FREQUENCY_int = (float) (SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) *512.0* DSS_DIVIDER_int)));

                    DSS_DIVIDER_fraz = Math.round((SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * frequencyVal*512.0))) * 2 / 3);
                    DSS_DIVIDER_fraz = DSS_DIVIDER_fraz > DSS_MAX ? DSS_MAX: DSS_DIVIDER_fraz;

                    FREQUENCY_fraz = (float) (SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * 512.0* DSS_DIVIDER_fraz))) * 2 / 3;


                    if (Math.abs(FREQUENCY_int - frequencyVal) < Math.abs(FREQUENCY_fraz - frequencyVal)) {
                        frequencyCalc.DSS_DIVIDER = (int) DSS_DIVIDER_int;
                        frequencyCalc.FRD = 0;
                        FRD_fraz = 1;
                    } else {
                        frequencyCalc.DSS_DIVIDER = (int) DSS_DIVIDER_fraz;
                        frequencyCalc.FRD = 1;
                        FRD_fraz = (float) 2 / (float) 3;
                    }

                    frequencyCalc.availableFrequency = (float) (FRD_fraz * SYS_CLOCK_FIELD / ((float) frequencyCalc.DSS_DIVIDER *512.0* Math.pow(2, frequencyCalc.OSM)));

                }
                else if (frequencyVal < limits1){
                    // 0  < frequencyVal  < 1000
                    // Old manner
                    float DSS_DIVIDER_int;
                    float DSS_DIVIDER_fraz;
                    float FREQUENCY_int;
                    float FREQUENCY_fraz;
                    float FRD_fraz = 1;

                    frequencyCalc.OSM = (float) 7.0;

                    DSS_DIVIDER_int = Math.round(SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * frequencyVal)));
                    DSS_DIVIDER_int = DSS_DIVIDER_int > DSS_MAX ? DSS_MAX: DSS_DIVIDER_int;


                    FREQUENCY_int = (float) (SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * DSS_DIVIDER_int)));

                    DSS_DIVIDER_fraz = Math.round((SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * frequencyVal))) * 2 / 3);
                    DSS_DIVIDER_fraz = DSS_DIVIDER_fraz > DSS_MAX ? DSS_MAX: DSS_DIVIDER_fraz;

                    FREQUENCY_fraz = (float) (SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * DSS_DIVIDER_fraz))) * 2 / 3;


                    if (Math.abs(FREQUENCY_int - frequencyVal) < Math.abs(FREQUENCY_fraz - frequencyVal)) {
                        frequencyCalc.DSS_DIVIDER = (int) DSS_DIVIDER_int;
                        frequencyCalc.FRD = 0;
                        FRD_fraz = 1;
                    } else {
                        frequencyCalc.DSS_DIVIDER = (int) DSS_DIVIDER_fraz;
                        frequencyCalc.FRD = 1;
                        FRD_fraz = (float) 2 / (float) 3;
                    }

                    frequencyCalc.availableFrequency = (float) (FRD_fraz * SYS_CLOCK_FIELD / ((float) frequencyCalc.DSS_DIVIDER * Math.pow(2, frequencyCalc.OSM)));


                } else if (frequencyVal <= limits2){
                    // 1000  <= frequencyVal  <= 78125
                    // LUT
                    float OSM[] = {128,64,32,16,8,4};
                    double Fa, Fa_2_3;
                    boolean freqErrThreshold_INT = false;
                    boolean freqErrThreshold_FRAZ = false;
                    double DSS_Fa, DSS_Fa_2_3, distFa, distFa_2_3;
                    double threshold;

                    int i = 0;
                    while(i < OSM.length && !freqErrThreshold_INT && !freqErrThreshold_FRAZ){

                        threshold = frequencyVal * (freqErrThresholdPerc/100);

                        DSS_Fa = Math.round(SYS_CLOCK_FIELD/(frequencyVal * OSM[i]));
                        DSS_Fa = DSS_Fa > DSS_MAX ? DSS_MAX: DSS_Fa;
                        Fa = SYS_CLOCK_FIELD/(DSS_Fa * OSM[i]);
                        distFa = Math.abs(frequencyVal - Fa);
                        if (distFa < threshold){
                            freqErrThreshold_INT = true;
                            frequencyCalc.OSM = log(OSM[i], 2);
                            frequencyCalc.DSS_DIVIDER = (int)DSS_Fa;
                            frequencyCalc.FRD = 0;
                            frequencyCalc.availableFrequency = (float) Fa;
                        }

                        DSS_Fa_2_3 = Math.round((SYS_CLOCK_FIELD * 2)/(3 * frequencyVal * OSM[i]));
                        DSS_Fa_2_3 = DSS_Fa_2_3 > DSS_MAX ? DSS_MAX: DSS_Fa_2_3;
                        Fa_2_3 = (SYS_CLOCK_FIELD * 2)/(3 * DSS_Fa_2_3 * OSM[i]);
                        distFa_2_3 = Math.abs(frequencyVal - Fa_2_3);
                        if (distFa_2_3 < distFa && distFa_2_3 < threshold){
                            freqErrThreshold_FRAZ = true;
                            frequencyCalc.OSM = log(OSM[i], 2);
                            frequencyCalc.DSS_DIVIDER = (int)DSS_Fa_2_3;
                            frequencyCalc.FRD = 1;
                            frequencyCalc.availableFrequency = (float) Fa_2_3;
                        }
                        i++;
                    }


                } else {

                    double dist, minDist;

                    // Initialize min values
                    frequencyCalc.OSM = (float)7.0;
                    frequencyCalc.FRD = 1;
                    frequencyCalc.DSS_DIVIDER = 1;
                    frequencyCalc.availableFrequency = (float) limits2;
                    minDist = Math.abs(frequencyVal - frequencyCalc.availableFrequency);

                    int i = 0;
                    float OSM = 6;
                    while(i < F_FRA.length){
                        dist = Math.abs(frequencyVal - F_FRA[i]);
                        if (dist < minDist){
                            frequencyCalc.OSM = OSM;
                            frequencyCalc.DSS_DIVIDER = 1;
                            frequencyCalc.FRD = 1;
                            frequencyCalc.availableFrequency = F_FRA[i];
                            minDist = dist;
                        }
                        dist = Math.abs(frequencyVal - F_INT[i]);
                        if (dist < minDist){
                            frequencyCalc.OSM = OSM;
                            frequencyCalc.DSS_DIVIDER = 1;
                            frequencyCalc.FRD = 0;
                            frequencyCalc.availableFrequency = F_INT[i];
                            minDist = dist;
                        }
                        OSM--;
                        i++;
                    }
                }

            }

            frequencyCalc.availableFrequency = (float) Math.round(frequencyCalc.availableFrequency * MYROUND)/MYROUND;

        }


        public static Double evaluateADCConversionRate(double freqIn, double min, double max) {
            //boolean flag = false;
            double adcFreq;
            int freq = (int)freqIn;
            if (freq <= 0){
                return min;
            } else if (freq < min){
                int multiplier = (int)Math.ceil((double)min/freq);
                adcFreq = multiplier * freq;
            } else if (freq < max){
                List<Integer> factors = primeFactors(freq);
                adcFreq = minFactorization(factors, (int)min);

            } else {
                adcFreq = min;
            }
            return adcFreq;
        }

        private static int minFactorization(List<Integer> factors, int min){
            //boolean flag = false;
            int p = 1;
            int n = factors.size();
            int v_i[] = new int[p];
            //int v_i_min[] = new int[p];
            int prodMin = Integer.MAX_VALUE;
            int prod = 1;
            boolean flagToReturn = false;
            for(p = 1; p <= n; p++){
                v_i = new int[p];
                //v_i_min = new int[p];
                for(int i = 0; i < p; i++){
                    v_i[i] = i;
                }
                int k = p - 1;

                while (v_i[k] < n) {
                    // produttoria
                    prod = 1;

                    for (int i = 0; i < v_i.length; i++) {
                        prod *= factors.get(v_i[i]);
                        //System.out.print(factors.get(v_i[i]) + ", ");
                    }
                    //System.out.println(prod);
                    if (prod >= min){
                        flagToReturn = true;
                        if (prod < prodMin){
                            prodMin = prod;
                            //System.arraycopy(v_i, 0, v_i_min, 0, v_i.length);
                        }
                    }
                    if (flagToReturn){
                        return prodMin;
                    }
                    v_i[k]++;
                }

                v_i[k]--;
                k = p - 2;
                while (k >= 0 && v_i[k] >= (v_i[k + 1] - 1)) {
                    k--;
                }
                if (k >= 0) {
                    v_i[k]++;
                    for (int i = k + 1; i < (p - 1); i++) {
                        v_i[i] = v_i[i - 1] + 1;
                    }
                }
            }
            return prodMin;
        }


        private static List<Integer> primeFactors(int numbers) {
            int n = numbers;
            List<Integer> factors = new ArrayList<Integer>();
            for (int i = 2; i <= n / i; i++) {
                while (n % i == 0) {
                    factors.add(i);
                    n /= i;
                }
            }
            if (n > 1) {
                factors.add(n);
            }
            return factors;
        }
    }







}


