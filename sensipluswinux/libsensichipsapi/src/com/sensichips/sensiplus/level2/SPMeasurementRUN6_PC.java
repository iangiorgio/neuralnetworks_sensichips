package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.*;
import com.sensichips.sensiplus.level2.parameters.*;
import com.sensichips.sensiplus.level2.parameters.items.*;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusIQ;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusPOT;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusSENSOR;
import com.sensichips.sensiplus.level2.util.SPMeasurementUtil;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLogger;

import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static com.sensichips.sensiplus.level1.chip.Utility.responseTokenizer;
import static com.sensichips.sensiplus.level1.chip.Utility.responseTokenizerByte;

public class SPMeasurementRUN6_PC extends SPMeasurementRUN5_PC {

    public static boolean resetBeforeSET = false;
    public static String resetBeforeSET_instruction = "WRITE COMMAND 0X42";

    private PrintWriter logForBug;

    private FilterMedianBuffer inPhaseBuffer[] = null;
    private FilterMedianBuffer inQuadratureBuffer[] = null;


    public SPMeasurementRUN6_PC(SPConfiguration conf, String logDirectory, String measure_type) throws SPException {
        super(conf, logDirectory, measure_type);
        try {
            logForBug = new PrintWriter(new File("/home/mario/gdrive/Sensichips/Sensichip-SerialPort/SensiplusWinux/log/logForBug.txt"));
        } catch (Exception e){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "-- logForDebug cannot be created", DEBUG_LEVEL);

        }

    }

    /**
     * @param param
     * @param m
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException if there is errors in low level communication (disconnection, timeout, etc.)
     */

    @Override
    public synchronized void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m) throws SPException {
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setADC", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {

            String instruction, instructionNum;
            ArrayList<String> instructionList = new ArrayList<>();

            // -- test WITH and WITHOUT the following instructions, if no difference then REMOVE
            instruction = "WRITE COMMAND S 0x44";
            instructionList.add(instruction);
            instruction = "WRITE COMMAND S 0x46";
            instructionList.add(instruction);
            // -- end of instructions to be REMOVED


            Integer FNP, OSR_DIVIDER, CICW, SAM_DIVIDER_int, SAM_DIVIDER_fraz, SAM_DIVIDER, Conversion_Rate_int, CCO, FRD = 0;
            //Double Conversion_Rate_fraz;
            Double Conversion_Rate, OSR;
            Integer PRE = 0;

            Conversion_Rate = param.getConversion_Rate();
            /*if(Conversion_Rate==null){
                //Conversion_Rate = 50.0;
                Conversion_Rate = 17.0;
            }*/
            //Conversion_Rate = (12.5);
            //Conversion_Rate = (50.0);
            OSR = (double)512;

            if (Conversion_Rate <= 1000) {
                FNP = 0; //-- enabled ADC precision mode
            } else {
                FNP = 1; //-- enabled ADC fast mode
            }


            if (Conversion_Rate <= clock/511.0){
                SAM_DIVIDER = 32;
                double f_osr = Conversion_Rate * OSR;
                if(f_osr<= clock/511.0){
                    PRE=1;
                    if (FRD == 1){
                        OSR_DIVIDER = (int)Math.round(((clock/512.0)*2/3)/f_osr);
                    } else {
                        OSR_DIVIDER = (int)Math.round((clock/512.0)/f_osr);
                    }
                }else{
                    PRE=0;
                    if (FRD == 1){
                        OSR_DIVIDER = (int)Math.round((clock*2/3)/f_osr);
                    } else {
                        OSR_DIVIDER = (int)Math.round(clock/f_osr);
                    }
                }
                //  OSR_DIVIDER = (int)Math.round(clock / f_osr);
            }  else {
                OSR_DIVIDER = 1;
                SAM_DIVIDER = 32;
                //throw new SPException("Not yet implemented. setADC() Conversion_Rate > clock/511.0 = " + clock/511);
            }


            //if (param.getFIFO_DEEP() == 1)
            if (param.getFIFO_READ() == 1)
                CCO = 0;
            else
                CCO = 1;



            CICW = (int)(15 - 3 * (Math.log(1008.0 / OSR)/ Math.log(2.0)));

            instruction = "WRITE DIG_CONFIG S 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(PRE,1);
            instructionNum +=  SPMeasurementUtil.fromIntegerToBinWithFill(0,7);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE CHA_DIVIDER M 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(SAM_DIVIDER.intValue(), 6);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSR_DIVIDER.intValue(), 9);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(FRD, 1);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE CHA_SELECT+1 S 0X";
            instructionNum =  "000000" + param.getInPortADC();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONDIT+1 S 0X";
            instructionNum =  SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "00" + param.getInGain(); // Rimuovere asap, introdotto come prova il 29 dicembre 2018
            //instructionNum =  SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "0000";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);



            //WRITE CHA_CONFIG S 0xCCO&FNP&ADC_MODE&NData -- FIFO in streaming mode
            String ADC_MODE = "0"; // ADC_Mode = 1 (0: FIFO mode, 1: Stream mode)
            instruction = "WRITE CHA_CONFIG S 0x";
            instructionNum = "" + CCO + "" + FNP + ADC_MODE;
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(param.getFIFO_DEEP(), 5);


            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            sendSequences(instructionList, ref, "setADC");

            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setADC", DEBUG_LEVEL);

    }


    /**old setADC
     *
     * */
    /*@Override
    public synchronized void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m) throws SPException {
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setADC", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {

            String instruction, instructionNum;
            ArrayList<String> instructionList = new ArrayList<>();

            // -- test WITH and WITHOUT the following instructions, if no difference then REMOVE
            //instruction = "WRITE COMMAND S 0x44";
            //instructionList.add(instruction);
            //instruction = "WRITE COMMAND S 0x46";
            //instructionList.add(instruction);
            // -- end of instructions to be REMOVED


            Integer FNP, OSR_DIVIDER, CICW, SAM_DIVIDER_int, SAM_DIVIDER_fraz, SAM_DIVIDER, Conversion_Rate_int, CCO, FRD = 0;
            //Double Conversion_Rate_fraz;
            Double Conversion_Rate, OSR;


            //Integer cutOffMin = 20;
            //Integer cutOffMax = 100;

            Conversion_Rate = (12.5);
            OSR = (double)512;

            if (Conversion_Rate <= 1000) {
                FNP = 0; //-- enabled ADC precision mode
            } else {
                FNP = 1; //-- enabled ADC fast mode
            }


            if (Conversion_Rate <= clock/511.0){
                SAM_DIVIDER = 32;
                double f_osr = Conversion_Rate * OSR;
                if (FRD == 1){
                    OSR_DIVIDER = (int)Math.round((clock*2/3)/f_osr);
                } else {
                    OSR_DIVIDER = (int)Math.round(clock/f_osr);
                }
                //  OSR_DIVIDER = (int)Math.round(clock / f_osr);
            } else {

                throw new SPException("Not yet implemented. setADC() Conversion_Rate > clock/511.0 = " + clock/511);
            }


            //if (param.getFIFO_DEEP() == 1)
            if (param.getFIFO_READ() == 1)
                CCO = 0;
            else
                CCO = 1;

            CICW = (int)(15 - 3 * (Math.log(1008.0 / OSR) / Math.log(2.0)));


            instruction = "WRITE CHA_DIVIDER M 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(SAM_DIVIDER.intValue(), 6);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSR_DIVIDER.intValue(), 9);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(FRD, 1);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE CHA_SELECT+1 S 0X";
            instructionNum =  "000000" + param.getInPortADC();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONDIT+1 S 0X";
            instructionNum =  SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "00" + param.getInGain(); // Rimuovere asap, introdotto come prova il 29 dicembre 2018
            //instructionNum =  SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "0000";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);



            //WRITE CHA_CONFIG S 0xCCO&FNP&ADC_MODE&NData -- FIFO in streaming mode
            String ADC_MODE = "0"; // ADC_Mode = 1 (0: FIFO mode, 1: Stream mode)
            instruction = "WRITE CHA_CONFIG S 0x";
            instructionNum = "" + CCO + "" + FNP + ADC_MODE;
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(param.getFIFO_DEEP(), 5);


            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            sendSequences(instructionList, ref, "setADC");

            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setADC", DEBUG_LEVEL);

    }*/

    /**
     * The setEIS method allows setting all required parameters to perform Electric Impedance Spectroscopy
     * (EIS) measurements.
     *
     * @param param @see SPMeasurementParameterEIS
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    @Override
    public synchronized  void setEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m)
            throws SPException {


        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setEIS", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE, param.toString(), DEBUG_LEVEL);



        int chopCutOff = 10000;

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }


        //System.out.println("Sequential: " + param.isSequentialMode());
        SPMeasurementSequential spMeasurementSequential = new SPMeasurementSequential(param.isSequentialMode());



        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {
            ArrayList<String> instructionList = new ArrayList<>();
            lastParameter = param;
            float OSM;
            int FRD, DSS_DIVIDER;

            AvailableFrequencies frequencyCalculus = new AvailableFrequencies();

            String instruction, instructionNum, instructionForSequential, instructionNumForSequential;


            if(resetBeforeSET){
                instruction = resetBeforeSET_instruction;
                instructionList.add(instruction);
            }


            String F2S = "0";

            if((param.getInPortLabel().equals(SPPort.portLabels[SPPort.PORT_EXT1_1]) ||
                    param.getInPortLabel().equals(SPPort.portLabels[SPPort.PORT_EXT2_1]) ||
                                    param.getInPortLabel().equals(SPPort.portLabels[SPPort.PORT_EXT3_1])) &&
                    (param.getOutPortLabel().equals(SPPort.portLabels[SPPort.PORT_EXT1_1]) ||
                            param.getInPortLabel().equals(SPPort.portLabels[SPPort.PORT_EXT2_1]) ||
                            param.getInPortLabel().equals(SPPort.portLabels[SPPort.PORT_EXT3_1]))

                    ){
                F2S="1";
            }

            instruction = "WRITE CHA_SELECT S 0x";
            instructionNum = param.getModeVI().substring(1, 2) + param.getContacts() + "0" + param.getInPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE DSS_SELECT M 0x";
            if (param.getModeVI().substring(0, 1).equals("1")) {
                if (param.getContactsLabel().equals(SPParamItemContacts.ContactsLabels[0])) {
                    //TWO CONTACTS
                    instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + F2S + param.getModeVI().substring(1, 2) + "1" + param.getOutPort();
                } else if (param.getContactsLabel().equals(SPParamItemContacts.ContactsLabels[1])) {
                    //FOUR CONTACTS
                    instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + "1" + param.getOutPort();
                }

                //instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + "1" + param.getOutPort();
            } else {
                if (param.getContactsLabel().equals(SPParamItemContacts.ContactsLabels[0])) {
                    //two contacts
                    instructionNum = "0" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + F2S + param.getModeVI().substring(1, 2) + param.getContacts() + param.getOutPort();
                } else if (param.getContactsLabel().equals(SPParamItemContacts.ContactsLabels[1])) {
                    //four contacts
                    instructionNum = "0" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + param.getContacts() + param.getOutPort();
                }
            }

            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);
            spMeasurementSequential.addBeforeInstruction(instruction);

            instruction = "WRITE DSS_DIVIDER M 0x";
            AvailableFrequencies.availableFrequencies(spConfiguration, param.getFrequency(), frequencyCalculus);
            OSM = frequencyCalculus.OSM;
            DSS_DIVIDER = frequencyCalculus.DSS_DIVIDER;
            FRD = frequencyCalculus.FRD;

            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(DSS_DIVIDER, 15) + Integer.toString(FRD);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            /*instruction = "WRITE DDS_CONFIG+1 S 0x80";
            instructionList.add(instruction);*/

            int OSM_INT = (int) OSM;
            int numBit = 12;
            int modSignDCBias = bitSignDCBias(param.getDCBiasP(), numBit);


            instruction                 = "WRITE DAS_CONFIG M 0x";
            instructionForSequential    = "WRITE DAS_CONFIG M 0x";

            instructionNum              = "0";
            instructionNumForSequential = "0";

            instructionNum              += SPMeasurementUtil.fromIntegerToBinWithFill(OSM_INT, 3);
            instructionNumForSequential += "000";

            instructionNum              += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBias, numBit);
            instructionNumForSequential += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBias, numBit);

            instruction                 = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionForSequential    = SPMeasurementUtil.fromBitToHexWithConcat(instructionForSequential, instructionNumForSequential);

            //spMeasurementSequential.addBeforeInstruction(instruction);                  // Start the sinosoid
            //spMeasurementSequential.addAfterInstruction(instructionForSequential);      // Stop the sinusoid


            /*
            if (param.isSequentialMode()) {
                // Stop the sinusoid
                sendAfterInstruction(ref, spMeasurementSequential);
                //instructionList.add(spMeasurementSequential.getAfterInstruction());
            } else {
                sendBeforeInstruction(ref, spMeasurementSequential);
                //instructionList.add(spMeasurementSequential.getBeforeInstruction());
            }
            */
            /*if (param.isSequentialMode()) {
                // Stop the sinusoid
                instructionList.add(instructionForSequential);
                //sendAfterInstruction(ref, spMeasurementSequential);
                //instructionList.add(spMeasurementSequential.getAfterInstruction());
            } else {
                //sendBeforeInstruction(ref, spMeasurementSequential);
                instructionList.add(instruction);
                //instructionList.add(spMeasurementSequential.getBeforeInstruction());
            }*/
            instructionList.add(instruction);


            instruction = "WRITE CHA_FILTER M 0x";
            String chopperValue = "";
            if (frequencyCalculus.availableFrequency <= chopCutOff) {
                chopperValue = "8E";
            } else {
                chopperValue = "08"; //only ADc choppers enabled (IA chopper causes error EIS in measurements)
            }


            String HE3 = "0";
            if (param.getHarmonic().equals(SPParamItemHarmonic.harmonicLabels[2])) {
                HE3 = "0";
            }
            instructionNum = "000" + param.getQI() + HE3;

            if (param.getFrequency() == 0){
                instructionNum += "00";
            } else {
                instructionNum +=  param.getHarmonic();
            }
            if (instructionNum.length() != 8)
                throw new SPException("lunghezza dati numerici sbagliata in instruction: " + instruction); //  numeric substring after 'OX' have to have lenght 8 bit

            String hexValueToFillWithZero = SPMeasurementUtil.zeroFill(Integer.toHexString(Integer.parseInt(instructionNum, 2)), 2);
            hexValueToFillWithZero = chopperValue + hexValueToFillWithZero;
            instruction += SPMeasurementUtil.zeroFill(hexValueToFillWithZero, 4);
            instructionList.add(instruction);

            instruction = "WRITE CHA_CONDIT+1 S 0x";

            //String VSCM_BIAS = "011111";


            if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.QUADRANT){
                instructionNum = "000000";
            } else {
                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getPhaseShift(), 5);
                if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.COARSE){
                    instructionNum += "1";
                } else if (param.getPhaseShiftMode() == SPParamItemPhaseShiftMode.FINE){
                    instructionNum += "0";
                }
            }

            instructionNum += param.getInGain();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONDIT S 0x";
            instructionNum = param.getRsense();
            int numBitDCBiasN = 6;
            int modSignDCBiasN = bitSignDCBias(param.getDCBiasN(), numBitDCBiasN);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBiasN, numBitDCBiasN);

            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE ANA_CONFIG S 0x3F";
            instructionList.add(instruction);

            spMeasurementSequential.addAfterInstruction("WRITE DSS_SELECT S 0X14");      // Stop ANALOGICAL BLOCK

            //instruction = "WRITE DAS_CONFIG M 0X0FFF";
            //instructionList.add(instruction);

            sendSequences(instructionList, ref, "setEIS");

            // "" + logDirectory + "/Download/" + measureLogFile

            String nomeFileDump = "" + logDirectory + "/Download/" + "getEIS_dump.txt";
            if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null){
                status = new SPMeasurementStatusIQ(this, param, spProtocol, SPMeasurementParameterEIS.NUM_OUTPUT,
                        frequencyCalculus, FRD, spMeasurementSequential, nomeFileDump);


                int medianFilterDimension = 8;
                int numOfBuffer = 1; //Multicast mode, one write, more read

                if (spMeasurementSequential != null && spMeasurementSequential.getActive()) {
                    numOfBuffer = spProtocol.getSpCluster().getActiveSPChipList().size();
                }

                inPhaseBuffer = new FilterMedianBuffer[numOfBuffer];
                inQuadratureBuffer = new FilterMedianBuffer[numOfBuffer];
                for(int i = 0; i < numOfBuffer; i++){
                    inPhaseBuffer[i] = new FilterMedianBuffer(medianFilterDimension);
                    inQuadratureBuffer[i] = new FilterMedianBuffer(medianFilterDimension);
                }


            }
            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setEIS", DEBUG_LEVEL);

    }

    private final static int ONLY_LAST      = 0;
    private final static int OR_FILTER      = 1;
    private final static int MEDIAN_FILTER = 2;
    private int filter = ONLY_LAST;


    private int COUNTER_FOR_BUG_RUN6 = 0;
    //private static final int COUNTER_FOR_BUG_RUN6_MOD = 16;
    //private static final int INDEX_BUG_RUN6[] = new int[]{2, 1, 3, 1, 1, 2, 1, 1, 1, 1, 1, 1};
    private static final int INDEX_BUG_RUN6[] = new int[]{3};

    //private static final int INDEX_BUG_RUN6[] = new int[]{4, 4, 4, 4};
    //private static final int INDEX_BUG_RUN6[] = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

    private byte[][] filterForRUN6(byte rawData[][], int numOfBytesPerRow){
        byte[][] output = new byte[numOfChips][];

        for(int i = 0; i < output.length; i++) { // Num of chips
            output[i] = new byte[2];
            if (filter ==  OR_FILTER){
                for(int k = 0; k < rawData[i].length/2; k++){
                    output[i][0] = (byte)((int) output[i][0] | (int)rawData[i][k*2]);
                    output[i][1] = (byte)((int) output[i][1] | (int)rawData[i][k * 2 + 1]);
                }
            } else if (filter ==  ONLY_LAST){
                output[i][0] = rawData[i][(rawData[i].length / 2 - 1) * 2];
                output[i][1] = rawData[i][(rawData[i].length / 2 - 1) * 2 + 1];
            } else if (filter == MEDIAN_FILTER){
                output[i] = medianFilter(rawData[i]);
            }

        }
        return output;
    }


    private byte[] medianFilter(byte rawData[]){
        int [] vet = new int[rawData.length/2];
        for(int i = 0; i < rawData.length/2; i++){
            vet[i] = Utility.conversionFromByteToIntSingle(rawData[i * 2 + 1], rawData[i * 2]);
        }
        int medianIndex = selectionSortForIndex(vet);
        byte[] output = new byte[2];

        output[0] = rawData[medianIndex * 2];
        output[1] = rawData[medianIndex * 2 + 1];
        return output;
    }

    public static int selectionSortForIndex(int[] arr){
        int [] arrIndex = new int[arr.length];
        for(int i = 0; i < arrIndex.length; i++){
            arrIndex[i] = i;
        }

        System.out.println("*****************************************");
        for(int i = 0; i < arr.length; i++){
            System.out.println("(" + arr[i] + ", " + arrIndex[i] + ")");
        }


        for (int i = 0; i < arr.length - 1; i++){
            int index = i;
            for (int j = i + 1; j < arr.length; j++){
                if (arr[j] < arr[index]){
                    index = j;//searching for lowest index
                }
            }

            // Swap
            int smallerNumber = arr[index];
            arr[index] = arr[i];
            arr[i] = smallerNumber;

            int smallerNumberIndex = arrIndex[index];
            arrIndex[index] = arrIndex[i];
            arrIndex[i] = smallerNumberIndex;

        }

        for(int i = 0; i < arr.length; i++){
            System.out.println("(" + arr[i] + ", " + arrIndex[i] + ")");
        }
        System.out.println("Selected value: " + arr[arrIndex.length/2]);
        System.out.println("Selected index: " + arrIndex[arrIndex.length/2]);
        System.out.println("*****************************************");

        return arr[arr.length/2];

    }

    protected byte[][] get(SendInstructionInOut ref, SPMeasurementParameterADC param, boolean inPhase, SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput m) throws SPException {


        sendBeforeInstruction(ref, spMeasurementSequential);
        /*
        if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.getBeforeInstruction()  != null){
            // Start sinusoidal signal (for EIS)
            ref.instructionToSend = spMeasurementSequential.getBeforeInstruction();
            sendSequence(ref);
            //SPDelay.delay(spProtocol.getADCDelay() * 10); // Wait for data ready on ADC
        }*/


        byte output[][] = null;

        int NDATA = param.getFIFO_READ() * 2;


        byte rawData[][] = new byte[numOfChips][];
        String allBytes[] = new String[numOfChips];
        for(int i = 0; i < numOfChips; i++){
            //rawData[i] = new byte[NDATA * INDEX_BUG_RUN6[COUNTER_FOR_BUG_RUN6]];
            allBytes[i] = "";
        }




        for(int i = 0; i < INDEX_BUG_RUN6[COUNTER_FOR_BUG_RUN6]; i++){

            if (param.getCommandX20()){
                ref.instructionToSend = "WRITE COMMAND S 0x";    // Trigger per acquisizione

                if (inPhase){
                    if ((i == 0 | i == 1) && INDEX_BUG_RUN6[COUNTER_FOR_BUG_RUN6] > 1){
                        ref.instructionToSend += "20";
                    } else {
                        ref.instructionToSend += "24";
                    }
                } else {
                    if ((i == 0 | i == 1) && INDEX_BUG_RUN6[COUNTER_FOR_BUG_RUN6] > 1){
                        ref.instructionToSend += "20";
                    } else {
                        ref.instructionToSend += "26";
                    }
                }

                sendSequence(ref);
                SPDelay.delay(spProtocol.getADCDelay()); // Wait for data ready on ADC
            }

            //ref.instructionToSend = "READ CHA_FIFOL M " + param.getNData() * 2;
            //ref.instructionToSend = "READ CHA_CONFIG M " + param.getNData() * 2;
            //sendSequence(ref);

            ref.instructionToSend = "READ CHA_FIFOL M " + NDATA;
            sendSequence(ref);



            for(int k = 0; k < ref.recValues.length; k++) {
                String app[] = responseTokenizer(ref.recValues[k]);
                for(int j = 0; j < app.length; j++){
                    allBytes[k] += app[j] + " ";
                }
            }

        }

        ref.instructionToSend = "WRITE COMMAND S 0x46";
        sendSequence(ref);


        for(int i = 0; i < allBytes.length; i++){
            allBytes[i] = "[" + allBytes[i].trim() + "]";
            rawData[i] = responseTokenizerByte(allBytes[i]); //new byte[param.getNData() * 2 * INDEX_BUG_RUN6[COUNTER_FOR_BUG_RUN6]];
            if (logForBug != null)
                logForBug.print(allBytes[i] + " ");
        }
        if (logForBug != null){
            logForBug.println();
            logForBug.flush();
        }

        output = filterForRUN6(rawData, (NDATA * INDEX_BUG_RUN6[COUNTER_FOR_BUG_RUN6]));


        COUNTER_FOR_BUG_RUN6 = (++COUNTER_FOR_BUG_RUN6) % INDEX_BUG_RUN6.length;

        SPLogger.getLogInterface().d(LOG_MESSAGE, "ref.recValues.length" + ref.recValues.length, DEBUG_LEVEL);

        //byte[][] output =  new byte[ref.recValues.length][];
        //for(int k = 0; k < ref.recValues.length; k++) {
        //    output[k] = Utility.responseTokenizerByte(ref.recValues[k]);
        //    SPLogger.getLogInterface().d(LOG_MESSAGE, "output[k]: " + output[k][0] + " " + output[k][1], DEBUG_LEVEL);
        //}

        sendAfterInstruction(ref, spMeasurementSequential);
        /*
        if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.getAfterInstruction()  != null){
            // Stop sinusoidal signal (for EIS)
            ref.instructionToSend = spMeasurementSequential.getAfterInstruction();
            sendSequence(ref);
        }*/

        if (status != null){
            status.updateLog(output);
        }

        ref.log += "END getData.\n-----------------------------\n";

        if (m != null) {
            m.out = ref.log;
        }

        return output;
    }


    /**
     * @param param
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     */
    @Override
    public synchronized  void setPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m)
            throws SPException {

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setPOT", DEBUG_LEVEL);
        ArrayList<String> instructionList = new ArrayList<>();

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {


            Double conversionRate = (double)50;
            String instruction, instructionNum;


            String I_nv = "0";
            String FSC_VSCM = "1";

            String CES = "0";
            if (param.portIsInternal()) {
                CES = "1";    // TODO: perfezionare confronto per individuare se porta interna o esterna
            }

            instruction = "WRITE CHA_SELECT S 0x";
            instructionNum = I_nv+FSC_VSCM + CES + param.getPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONDIT M 0x";
            //param.setInGain("1"); //TODO: why force InGain to 1?
            instructionNum = "000000" + param.getInGain() + param.getRsense() + "011111";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);



            String IVS = "0";       //when set enables the current stimulus
            String FI = "00";        //defines the sine filter strenght (0: filter disable 3:high filtering)
            String MA = "111";      //defines the amplitude of the sine
            String FSC_DAS = param.getContacts();

            instruction = "WRITE DSS_SELECT M 0x";
            //instructionNum = "0000011100" + FSC_DAS + param.getPort(); //OLD
            instructionNum = "0"+IVS+FI+"0"+MA+"00"+FSC_DAS+param.getPort(); //NEW
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            /*
            instruction = "WRITE DSS_DIVIDER M 0x0014"; //???????????
            instructionList.add(instruction);
            */
            String FRD="0";
            instructionNum = "000000000000001"+FRD;
            instruction="WRITE DSS_DIVIDER M 0x";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            String PCE = "1"; 	    //IA chopper enabled
            String PCI = "0"; 	    //Potentiostat Chopper is not inverted
            String PCP = "11";      //IA chopper at maximum frequency
            String ACE = "1";	    // ADC chopper enabled
            String ACP = "00";	    // ADC chopper at maximum frequency ADC_Chop = OSR_Clock/8
            instructionNum=PCE+PCI+PCP+ACE+ACP+"0"+"00000000"; //IA and ADC choppers enabled
            instruction = "WRITE CHA_FILTER M 0x";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            /*
            instruction = "WRITE CHA_FILTER+1 S 0xB2";
            instructionList.add(instruction);*/

            sendSequences(instructionList, ref, "setPOT");

            if (m != null) {
                m.out = ref.log;
            }

            SPMeasurementOutput mADC = new SPMeasurementOutput();
            /*if (true)
                throw new SPException("setADC in autorange should be verified for RUN5 and RUN6 in terms of FIFO_READ and FIFO_DEEP dimension");*/

            setADC(1, 1, param.getInGainLabel(), conversionRate, SPMeasurementParameterADC.INPORT_IA, true, mADC);
            //instructionList.add(instruction);

            if (m != null) {
                m.out += mADC.out;
            }

            String PDE = "0"; // ppr off
            String PME = "0"; // ppr off
            String DAE = "1"; // das on
            String ADE = "1" ; // adc on

            String IAE = "1"; //IA on
            String CME = "1"; // vscm on
            String BGE = "1"; // bandgap on
            String OSE = "1"; // oscillator on
            instructionList = new ArrayList<>();
            //instruction = "WRITE ANA_CONFIG S 0x3F";
            instructionNum = PDE + PME + DAE + ADE + IAE + CME +BGE + OSE;
            instruction = "WRITE ANA_CONFIG S 0x";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            //if (param.getSPMeasurementMetaParameter().getRestartBandGap()){
            //    restartBandGap();
            //}


            if (param.getTypeVoltammetry().equals(SPParamItemPOTType.POTENTIOMETRIC)){
                instruction = "WRITE CHA_SELECT S 0x";
                instructionNum = "11" + CES + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);
            }

            sendSequences(instructionList, ref, "setPOT");

            status = new SPMeasurementStatusPOT(this, spConfiguration, param, SPParamItemPOTMeasures.NUM_OUTPUT,spProtocol, "" + logDirectory + "/Download/getPOT_dump.txt");


            if (m != null) {
                m.out += ref.log;
            }
            lastParameter = param;

        } catch (SPException  e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setPOT", DEBUG_LEVEL);

    }




    @Override
    public synchronized void setSENSOR(SPMeasurementParameterSENSORS param, SPMeasurementOutput m)
            throws SPException {

        SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(param.getSensorName());
        SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        Double conversionRate50 = (double)50;
        String InPortADC = SPMeasurementParameterADC.INPORT_IA;
        Integer FIFO_DEEP = param.getFIFO_DEEP();
        Integer FIFO_READ = param.getFIFO_READ();

        generateCalibrationParameters_new(param.getSensorName());

        if(param.getParamInternalEIS()!=null){

            SPMeasurementOutput m2 = new SPMeasurementOutput();
            //SPMeasurementParameterEIS paramInternal = (SPMeasurementParameterEIS)spSensingElementOnChip.getSensingElementOnFamily().getSPMeasurementParameter(spConfiguration);
            //paramInternal.setSPMeasurementMetaParameter(param.getSPMeasurementMetaParameter());


            SPMeasurementParameterEIS paramInternal = param.getParamInternalEIS();

            // Effettua la ricerca inversa: posseggo il valore, ritrovo la label
            setEIS(paramInternal, m2);
            ref.log += m2.out;
            m2 = new SPMeasurementOutput();
            setADC(FIFO_DEEP, FIFO_READ, paramInternal.getInGainLabel(), conversionRate50, InPortADC, true, m2);
            ref.log += m2.out;

        } else if (spSensingElement.getMeasureTechnique().equals("DIRECT")) {

            /*ref.instructionToSend = "WRITE DAS_CONFIG M 0x07FF";
            sendSequence(ref);*/

            if (spSensingElement.getName().equals("ONCHIP_TEMPERATURE") || spSensingElement.getName().equals("ONCHIP_VOLTAGE")) {
                String instruction = "";
                if(resetBeforeSET){
                    instruction = resetBeforeSET_instruction;
                    ref.instructionToSend = instruction;
                    sendSequence(ref);
                }


                instruction = "WRITE CHA_SELECT S 0X";
                String instructionNum = "110" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);

                ref.instructionToSend = "WRITE DSS_SELECT M 0x0020"; //  -- disconnect DAS from IA
                sendSequence(ref);

                String InGain = "20"; // Rendere coerente con CHA_CONDIT
                if (spSensingElement.getName().equals("ONCHIP_VOLTAGE")){
                    InGain = "1"; // Rendere coerente con CHA_CONDIT
                    ref.instructionToSend = "WRITE CHA_CONDIT M 0x031F";
                } else {
                    InGain = "20";
                    ref.instructionToSend = "WRITE CHA_CONDIT M 0x011F"; // InGain = 40 => 001F; InGain = 20 => 011F, InGain = 12 => 021F; InGain = 1 => 031F
                }
                sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_FILTER M 0x8E00";   //ref.instructionToSend = "WRITE CHA_FILTER+1 S 0xFE"; da verificare con Simone: F6 o FE
                sendSequence(ref);

                //ref.instructionToSend = "WRITE ANA_CONFIG S 0x1B"; // TODO: Da ripristinare 29/12/2018
                ref.instructionToSend = "WRITE ANA_CONFIG S 0x1B";
                sendSequence(ref);


                SPMeasurementOutput m2 = new SPMeasurementOutput();
                setADC(FIFO_DEEP, FIFO_READ, InGain, conversionRate50, InPortADC, true, m2);
                ref.log += m2.out;

                if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null) {
                    status = new SPMeasurementStatusSENSOR(this, param, 1,spProtocol, "");
                }

            } else if (spSensingElement.getName().equals("ONCHIP_LIGHT") || (spSensingElement.getName().equals("ONCHIP_DARK"))){

                ref.instructionToSend = "WRITE DSS_DIVIDER M 0x0002"; //  -- disconnect DAS from IA
                sendSequence(ref);

                String instruction = "WRITE CHA_SELECT S 0X";
                String instructionNum = "010" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);

                instruction = "WRITE DSS_SELECT M 0X"; //  -- disconnect DAS from IA
                instructionNum = "00000000001" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);


                String InGain = "40"; // Rendere coerente con CHA_CONDIT
                ref.instructionToSend = "WRITE CHA_CONDIT M 0x001F"; // 40 Ingain --
                sendSequence(ref);

                //ref.instructionToSend = "WRITE CHA_FILTER+1 S 0xB2";
                //sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_FILTER M 0x8E00";
                sendSequence(ref);

                ref.instructionToSend = "WRITE ANA_CONFIG S 0x3F";
                sendSequence(ref);

                SPMeasurementOutput m2 = new SPMeasurementOutput();
                setADC(FIFO_DEEP, FIFO_READ, InGain, conversionRate50, InPortADC, true, m2);
                ref.log += m2.out;

                if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null) {
                    status = new SPMeasurementStatusSENSOR(this, param, 1,spProtocol, "");
                }
            } else {
                throw new SPException("In setSENSOR. Sensing element unavailable: " + spSensingElement.getMeasureTechnique());
            }
        } else {
            throw new SPException("Measure technique unavailable: " + spSensingElement.getMeasureTechnique());
        }
    }

    /**
     * This version is able to read in multicast or unicast (with sequential mode activated).
     * @param param
     * @param ref
     * @param output
     * @param m
     * @throws SPException
     */

    protected void evaluateImpedanceDataReader(SPMeasurementParameter param,  SendInstructionInOut ref, double output[][], SPMeasurementOutput m) throws SPException {

        // output:
        // in each row is stored a chip
        // first column contains inphase
        // second column contains quadrature



        SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

        ref.log += "START evaluateImpedanceDataReader.\n-----------------------------\n";

        SPMeasurementSequential spMeasurementSequential = iqStatus.getSpMeasurementSequential();
        SPCluster spCluster = spProtocol.getSpCluster();
        byte[] output_temp = null;

        int numOfRepetition = 1; //Multicast mode, one write, more read

        if (spMeasurementSequential != null && spMeasurementSequential.getActive()) {
            numOfRepetition = spCluster.getActiveSPChipList().size();
        }

        String command = null;
        int appo[][];

        for (int i = 0; i < numOfRepetition; i++) {

            // Generate a cluster with one chip
            if (spMeasurementSequential != null && spMeasurementSequential.getActive()) {
                ArrayList<String> list = new ArrayList<>();
                list.add(spCluster.getActiveSPChipList().get(i).getSerialNumber());
                ref.temporaryCluster = cluster.generateTempCluster(list);
            }


            // Turn on sinusoid+
            sendBeforeInstruction(ref, spMeasurementSequential);
            /*
            if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.getBeforeInstruction() != null) {
                // Start sinusoidal signal (for EIS)
                ref.instructionToSend = spMeasurementSequential.getBeforeInstruction();
                sendSequence(ref);
                //SPDelay.delay(spProtocol.getADCDelay() * 10); // Wait for data ready on ADC
            }*/


            boolean readWIthTwoSteps = true;


            if (readWIthTwoSteps){

                // Ignore first two measures
                singleCommandRead("WRITE COMMAND S 0x20", ref, param.getFIFO_READ());
                singleCommandRead("WRITE COMMAND S 0x20", ref, param.getFIFO_READ());


                // Appo conterrà un numero di interi pari alla dimensione di FIFO_READ
                appo = singleCommandRead("WRITE COMMAND S 0x24", ref, param.getFIFO_READ());
                for(int j = 0; j < ref.recValues.length; j++){
                    // only one between j or i will be 0. PAY ATTENTION
                    // Use only the last value
                    output[j + i][SPParamItemMeasures.IN_PHASE] = -appo[j + i][appo[j + i].length - 1];
                }

                ref.instructionToSend = "WRITE COMMAND S 0x46";
                sendSequence(ref);

                // Ignore first two measures
                singleCommandRead("WRITE COMMAND S 0x20", ref, param.getFIFO_READ());
                singleCommandRead("WRITE COMMAND S 0x20", ref, param.getFIFO_READ());

                // Appo conterrà un numero di interi pari alla dimensione di FIFO_READ
                appo = singleCommandRead("WRITE COMMAND S 0x26", ref, param.getFIFO_READ());
                for(int j = 0; j < ref.recValues.length; j++){
                    // only one between j or i will be 0. PAY ATTENTION
                    // Use only the last value
                    output[j + i][SPParamItemMeasures.QUADRATURE] = -appo[j + i][appo[j + i].length - 1];
                }

                ref.instructionToSend = "WRITE COMMAND S 0x46";
                sendSequence(ref);

            } else {

                // Ignore first
                appo = singleCommandRead("WRITE COMMAND S 0x20", ref, param.getFIFO_READ());

                // I/Q reading
                appo = singleCommandRead("WRITE COMMAND S 0x28", ref, param.getFIFO_READ());
                for(int j = 0; j < ref.recValues.length; j++){
                    // only one between j or i will be 0. PAY ATTENTION
                    // Use only the last value
                    output[j + i][SPParamItemMeasures.IN_PHASE] = inPhaseBuffer[j].put(-appo[j + i][appo[j + i].length - 1]);
                }



                appo = singleCommandRead(null, ref, param.getFIFO_READ());
                for(int j = 0; j < ref.recValues.length; j++){
                    // only one between j or i will be 0. PAY ATTENTION
                    // Use only the last value
                    output[j + i][SPParamItemMeasures.QUADRATURE] = inQuadratureBuffer[j].put(-appo[j + i][appo[j + i].length - 1]);
                }

                ref.instructionToSend = "WRITE COMMAND S 0x46";
                sendSequence(ref);

            }


            sendAfterInstruction(ref, spMeasurementSequential);

            /*
            if (spMeasurementSequential != null && spMeasurementSequential.getActive() && spMeasurementSequential.getAfterInstruction() != null) {
                // Stop sinusoidal signal (for EIS)
                ref.instructionToSend = spMeasurementSequential.getAfterInstruction();
                sendSequence(ref);
            }*/
        }

        if (status != null) {
            //status.updateLog(output);
        }

        ref.log += "END evaluateImpedanceDataReader.\n-----------------------------\n";

        if (m != null) {
            m.out = ref.log;
        }

    }


    private int[][] singleCommandRead(String command, SendInstructionInOut ref, int numStageFIFO_ToRead) throws SPException {
        if (command != null){
            ref.instructionToSend = command;
            sendSequence(ref);
            SPDelay.delay(spProtocol.getADCDelay()); // Wait for data ready on ADC
        }

        ref.instructionToSend = "READ CHA_FIFOL M " + numStageFIFO_ToRead * 2;
        sendSequence(ref);

        int[][] output = new int[ref.recValues.length][];

        for(int i = 0; i < ref.recValues.length; i++){
            //rawData[i] = new byte[NDATA * INDEX_BUG_RUN6[COUNTER_FOR_BUG_RUN6]];
            output[i] = new int[numStageFIFO_ToRead];
        }

        // In sequential mode there is only one chip
        for(int i = 0; i < ref.recValues.length; i++){
            // only one between j or i will be 0. PAY ATTENTION
            byte[] output_temp = responseTokenizerByte(ref.recValues[i]);
            // CONSIDERING only last couple of byte
            for(int j = 0; j < numStageFIFO_ToRead; j++){
                output[i][j] = Utility.conversionFromByteToIntSingle(output_temp[j * 2 + 1], output_temp[j * 2]);
            }
        }
        return output;
    }

    /**
     * Sistema Produttore consumatore per l'applicazione del filtro mediano
     *
     */
    public static class FilterMedianBuffer {
        private int v[];
        private int app[];
        private int preleva = 0, inserisci = 0;
        private int queueSize;
        private boolean firstValue;


        public FilterMedianBuffer(int queueSize) {
            this.queueSize = queueSize;
            v = new int[queueSize];
            app = new int[queueSize];
            firstValue = true;
        }


        public int put(int o) throws SPProtocolException {

            if (firstValue){
                firstValue = false;
                for(int i = 0; i < queueSize; i++){
                    v[i] = o;
                }
            } else {
                v[inserisci] = o;
                inserisci = (inserisci + 1) % queueSize;
            }
            System.arraycopy(v, 0, app, 0, app.length);

            return selectionSortForIndex(app);

        }

    }

    public static void main(String args[]) throws SPException {
        FilterMedianBuffer m = new FilterMedianBuffer(8);
        for(int i = 0; i < 20; i++){
            int x = (int)(Math.random()*10);
            System.out.println("Input: " + x + " selected: " + m.put(x));
        }
    }

}
