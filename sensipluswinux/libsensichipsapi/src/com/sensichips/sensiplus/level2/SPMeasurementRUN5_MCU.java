package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.protocols.esp8266.SPProtocolESP8266_SENSIBUS;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacket;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacketExtended;
import com.sensichips.sensiplus.level2.parameters.*;
import com.sensichips.sensiplus.util.SPExtendedInstructionManager;
import org.apache.commons.codec.binary.Hex;

import java.nio.ByteBuffer;

public class SPMeasurementRUN5_MCU extends SPMeasurementRUN5_PC {


    public SPMeasurementRUN5_MCU(SPConfiguration conf, String logDirectory, String MEASURE_TYPE) throws SPException, SPException {
        super(conf, logDirectory, MEASURE_TYPE);
    }

    /**
     * @param ModeTimer
     * @param Timer
     * @param message
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setTimer(String ModeTimer, Integer Timer, SPMeasurementOutput message) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setULTRASOUND(SPMeasurementParameterULTRASOUND param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public double[][] getULTRASOUND(SPMeasurementParameterULTRASOUND param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = SPExtendedInstructionManager.getPacketForSetEIS(param, this.instance_ID);
        byte[] received = spProtocol.sendPacket(packet);
        System.out.println("setEIS MCU effettuata!!!");
        // received.length == 0 expected
    }

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public double[][] getEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m) throws SPException {
        double out[][];
        out = getSingleEIS(param, isAutomaticADCRangeAdjustment(), m);
        return out;
    }



    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = SPExtendedInstructionManager.getPacketForSetADC(param);
        byte[] received = spProtocol.sendPacket(packet);
        System.out.println("SetADC MCU effettuata!");
    }

    @Override
    public double[][] getSingleEIS(SPMeasurementParameterEIS param, boolean ADCAdjustment, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = SPExtendedInstructionManager.getPacketForGetEIS(spProtocol.getSpCluster().getActiveSPChipList().size());
        //TODO: allineare il numero di output EIS tra Java e MCU cosi da poter utilizzare getMCU() anche per la getSingleEIS()
        byte[] received = spProtocol.sendPacket(packet);
        byte[] appo = new byte[8];
        int numOfChip = spProtocol.getSpCluster().getActiveSPChipList().size();
        int dataReceivedPerChip = ((packet.getDataLengthExpected() - 2)/8)/numOfChip;   //-2 perchè tolgo errorCode  e payloadsize, /8 perche 1 double sono 8 byte
        double out[][] = new double[numOfChip][SPMeasurementParameterEIS.NUM_OUTPUT]; //2 sono i chip presenti sul MCU

        // Genero double
        for(int i = 0; i < numOfChip; i++){
            for (int j = 0; j < dataReceivedPerChip; j++) { //-2 perchè tolgo errorCode  e payloadsize, /8 perche 1 double sono 8 byte
                System.arraycopy(received, (j + (dataReceivedPerChip * i)) * 8, appo, 0, 8);

                out[i][j] = ByteBuffer.wrap(appo).getDouble();

            }
        }

        return out;
    }


    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = SPExtendedInstructionManager.getPacketForSetPOT(param, this.instance_ID);
        byte[] received = spProtocol.sendPacket(packet);
        System.out.println("SetPOT MCU effettuata!");
    }

    /**
     * @param InOutPort
     * @param Type
     * @param InitialPotential
     * @param FinalPotential
     * @param step
     * @param PulsePeriod
     * @param PulseAmplitude
     * @param alternativeSignal
     * @param rsense
     * @param InGain
     * @param message
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public double[][] getPOT(String InOutPort, String Type, Integer InitialPotential, Integer FinalPotential, Integer step, Integer PulsePeriod, Integer PulseAmplitude, Boolean alternativeSignal, String rsense, String InGain, SPMeasurementOutput message) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public double[][] getPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = SPExtendedInstructionManager.getPacketForGetPOT(spProtocol.getSpCluster().getActiveSPChipList().size());
        /*
        byte[] received = spProtocol.sendPacket(packet);
        byte[] appo = new byte[8];
        int numOfChip = spProtocol.getSpCluster().getActiveSPChipList().size();
        int dataReceivedPerChip = ((packet.getDataLengthExpected() - 2)/8)/numOfChip;   //-2 perchè tolgo errorCode  e payloadsize, /8 perche 1 double sono 8 byte
        double out[][] = new double[numOfChip][2]; //2 sono i chip presenti sul MCU

        // Genero double
        for(int i = 0; i < numOfChip; i++){
            for (int j = 0; j < dataReceivedPerChip; j++) { //-2 perchè tolgo errorCode  e payloadsize, /8 perche 1 double sono 8 byte
                System.arraycopy(received, (j + (dataReceivedPerChip * i)) * 8, appo, 0, 8);

                out[i][j] = ByteBuffer.wrap(appo).getDouble();

            }
        }
        */
        return getMCU(packet);
    }

    /**
     * @param message   log messages
     * @return
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    @Override
    public int[][] getDATA(SPMeasurementParameter param, Boolean CommandX20, Boolean inPhase,
                           SPMeasurementSequential spMeasurementSequential, SPMeasurementOutput message) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setPPR(SPMeasurementParameterPPR param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");

    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public SPMeasurementOutputPPR[] getPPR(SPMeasurementParameterPPR param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    @Override
    public void setACTUATOR(SPMeasurementParameterActuator parameterActuator) throws SPException {
        throw new SPException("Not yet implemented!");

    }

    @Override
    public void setACTUATOR(SPMeasurementParameterActuator parameterActuator, SPCluster temporaryCluster) throws SPException {
        throw new SPException("Not yet implemented!");

    }

    @Override
    public double[][] getSENSOR(SPMeasurementParameterSENSORS sensorParam, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = SPExtendedInstructionManager.getPacketForGetSENSOR(spProtocol.getSpCluster().getActiveSPChipList().size());
        /*
        byte[] received = spProtocol.sendPacket(packet);
        byte[] appo = new byte[8];
        int numOfChip = spProtocol.getSpCluster().getActiveSPChipList().size();
        int dataReceivedPerChip = ((packet.getDataLengthExpected() - 2)/8)/numOfChip;   //-2 perchè tolgo errorCode  e payloadsize, /8 perche 1 double sono 8 byte
        double out[][] = new double[dataReceivedPerChip][SPMeasurementParameterEIS.NUM_OUTPUT]; //2 sono i chip presenti sul MCU

        // Genero double
        for(int i = 0; i < numOfChip; i++){
            for (int j = 0; j < dataReceivedPerChip; j++) { //-2 perchè tolgo errorCode  e payloadsize, /8 perche 1 double sono 8 byte
                System.arraycopy(received, (j + (dataReceivedPerChip * i)) * 8, appo, 0, 8);

                out[i][j] = ByteBuffer.wrap(appo).getDouble();

            }
        }
        */

        return getMCU(packet);
    }

    @Override
    public void setSENSOR(SPMeasurementParameterSENSORS parameter, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = SPExtendedInstructionManager.getPacketForSetSENSOR(parameter, this.getMeasureIndexToSave(), this.instance_ID);
        byte[] received = spProtocol.sendPacket(packet);
        System.out.println("setSENSOR MCU effettuata!!!");
    }

    /**
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void timerTest(SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");

    }

    private double[][] getMCU(SPProtocolPacketExtended packet) throws SPException {
        byte[] received = spProtocol.sendPacket(packet);
        byte[] appo = new byte[8];
        int numOfChip = spProtocol.getSpCluster().getActiveSPChipList().size();
        int dataReceivedPerChip = ((packet.getDataLengthExpected() - 2)/8)/numOfChip;   //-2 perchè tolgo errorCode  e payloadsize, /8 perche 1 double sono 8 byte
        double out[][] = new double[numOfChip][dataReceivedPerChip]; //2 sono i chip presenti sul MCU

        // Genero double
        for(int i = 0; i < numOfChip; i++){
            for (int j = 0; j < dataReceivedPerChip; j++) { //-2 perchè tolgo errorCode  e payloadsize, /8 perche 1 double sono 8 byte
                System.arraycopy(received, (j + (dataReceivedPerChip * i)) * 8, appo, 0, 8);

                out[i][j] = ByteBuffer.wrap(appo).getDouble();

            }
        }

        return out;
    }

    public static void main(String[] args) {
/*
            SPDriver driver_temp = SPDriverManager.get("WINUX_COMUSB_COM12_");
            SPConfiguration conf_temp = new SPConfiguration();
            SPCluster clust_temp = new SPCluster();
            SPProtocolESP8266_SENSIBUS prot_temp = new SPProtocolESP8266_SENSIBUS(driver_temp, clust_temp, 0);
*/
        int FIRSTBYTE_MASK = 255; // 11111111
        byte[] dataPacket = new byte[2];
        int dcBiasP = -1348;
        int appo = dcBiasP + 2048;
        dataPacket[0] = (byte)(appo >> 8);
        dataPacket[1] = (byte)(appo & FIRSTBYTE_MASK);
        System.out.println(Hex.encodeHexString(dataPacket));

    }

}
