package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterADC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterQI;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemInGain;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemMeasures;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemRSense;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Marco Ferdinandi on 11/04/2017.
 */
public class SPAutoRangeAlgorithm {


    private SPMeasurementParameterEIS savedParamEIS = null;
    private static String[] keys;
    private static float[] Vmax;


    private static Map<String,AutoRangeConfig> autoRangeConfigMap = null;

    public static String[] getKeys() {
        return keys;
    }

    public static float[] getVmax() {
        return Vmax;
    }

    private SPMeasurement measurement;

    public SPAutoRangeAlgorithm(SPMeasurement measurement){
        this.measurement = measurement;
    }


    public static Map<String, AutoRangeConfig> getAutoRangeConfigMap() {
        if(autoRangeConfigMap==null){
            autoRangeConfigMap = createMap();
        }
        return autoRangeConfigMap;
    }



    public static Map<String, AutoRangeConfig> createMap() {
        keys = new String[]{"1_50", "20_50", "40_50", "40_500", "40_5000", "40_50000"};
        //Vmax = new float[]{(float)0.835, (float)2.7, (float)2.7, (float)2.7, (float)2.7, (float)2.7};
        Vmax = new float[]{(float)0.835, (float)2, (float)2, (float)2, (float)2, (float)2};
        HashMap<String, AutoRangeConfig> result = new HashMap<String, AutoRangeConfig>();
        result.put(keys[0], new AutoRangeConfig(SPParamItemRSense.rsenseLabels[3], SPParamItemInGain.ingainLabels[0], Vmax[0], Vmax[0]/10));
        result.put(keys[1], new AutoRangeConfig(SPParamItemRSense.rsenseLabels[3], SPParamItemInGain.ingainLabels[2], Vmax[1], Vmax[1]/2));
        result.put(keys[2], new AutoRangeConfig(SPParamItemRSense.rsenseLabels[3], SPParamItemInGain.ingainLabels[3], Vmax[2], Vmax[2]/10));
        result.put(keys[3], new AutoRangeConfig(SPParamItemRSense.rsenseLabels[2], SPParamItemInGain.ingainLabels[3], Vmax[3], Vmax[3]/10));
        result.put(keys[4], new AutoRangeConfig(SPParamItemRSense.rsenseLabels[1], SPParamItemInGain.ingainLabels[3], Vmax[4], Vmax[4]/10));
        result.put(keys[5], new AutoRangeConfig(SPParamItemRSense.rsenseLabels[0], SPParamItemInGain.ingainLabels[3], Vmax[5], Float.MIN_VALUE));
        return Collections.unmodifiableMap(result);

    }


    public static class AutoRangeConfig{
        String Rsense;
        float Vmax;
        float Vmin;
        String InGain;

        AutoRangeConfig(String Rsense, String InGain, float Vmax, float Vmin){
            this.Rsense = Rsense;
            this.InGain = InGain;
            this.Vmax = Vmax;
            this.Vmin = Vmin;
        }

    }

    public void autoRange(SPMeasurementParameterEIS paramEIS, int VCC)
            throws SPException {

        Map<String, SPAutoRangeAlgorithm.AutoRangeConfig> IDAutoRange = SPAutoRangeAlgorithm.createMap();
        SPMeasurementOutput message = new SPMeasurementOutput();

        int tempIndexOfMeasure = paramEIS.getMeasure();
        int indexOfMeasure = 7; // Capacitance in order to force I and Q measures
        paramEIS.setMeasure(SPParamItemMeasures.measureLabels[indexOfMeasure]);



        //measurement.setINIT(message);
        String dump = "";

        int NDATA = 1;

        double VM;
        int i = 0;
        boolean firstMeasure = true;

        do{
            paramEIS.setInGain(IDAutoRange.get(SPAutoRangeAlgorithm.getKeys()[i]).InGain);
            paramEIS.setRsense(IDAutoRange.get(SPAutoRangeAlgorithm.getKeys()[i]).Rsense);
            measurement.setEIS(paramEIS, message);
            boolean sendCommandX20 = true;

            /*if (true)
                throw new SPException("setADC in autorange should be verified for RUN5 and RUN6 in terms of FIFO_READ and FIFO_DEEP dimension");*/


            measurement.setADC(1, 1, paramEIS.getInGainLabel(), 50.0,
                    SPMeasurementParameterADC.INPORT_IA, sendCommandX20, message);
            //SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;
            //((SPMeasurementStatusIQ) status).setBufferFilled(true);
            double[][] output = measurement.getSingleEIS(paramEIS, false,message);

            output[0][SPMeasurementParameterQI.QUADRATURE] = (output[0][SPMeasurementParameterQI.QUADRATURE] * VCC) / (Math.pow(2,15) * 1000);
            output[0][SPMeasurementParameterQI.IN_PHASE] = (output[0][SPMeasurementParameterQI.IN_PHASE] * VCC) / (Math.pow(2,15) * 1000);
            VM = Math.sqrt(Math.pow(output[0][SPMeasurementParameterQI.QUADRATURE], 2) + Math.pow(output[0][SPMeasurementParameterQI.IN_PHASE], 2));

            if (i == 0 && VM > SPAutoRangeAlgorithm.getVmax()[i]){
                throw new SPException("Overflow condition for auto ranging!");
            }

            if (VM >= IDAutoRange.get(SPAutoRangeAlgorithm.getKeys()[i]).Vmin){
                break;
            }

            i++;
        }while(i < SPAutoRangeAlgorithm.getKeys().length);

        if (i == SPAutoRangeAlgorithm.getKeys().length){
            i--;
        }
        if (VM >= IDAutoRange.get(SPAutoRangeAlgorithm.getKeys()[i]).Vmin){

            //UtilityGUI.Toast(mainActivity.getBaseContext(), "Auto range ok. InGain = " + IDAutoRange.get(keys[i]).InGain + ", RSense = " + IDAutoRange.get(keys[i]).Rsense);
            //SPLogger.getLogInterface().d(LOG,  "Auto range ok. InGain = " + IDAutoRange.get(SPAutoRangeAlgorithm.getKeys()[i]).InGain + ", RSense = " + IDAutoRange.get(keys[i]).Rsense, SPLoggerInterface.DEBUG_VERBOSITY_MED);
            //SPLogger.getLogInterface().d(LOG,  "i: " + i, SPLoggerInterface.DEBUG_VERBOSITY_MED);
            //setParameterToUI(lastParam);
        } else {

            throw new SPException("Auto Range ko!!");
            //UtilityGUI.Toast(mainActivity.getBaseContext(), "Auto range ko!!!");
            //SPLogger.getLogInterface().d(LOG,  "Auto range ko!!!", SPLoggerInterface.DEBUG_VERBOSITY_MED);
        }

        paramEIS.setMeasure(SPParamItemMeasures.measureLabels[tempIndexOfMeasure]);


    }










}
