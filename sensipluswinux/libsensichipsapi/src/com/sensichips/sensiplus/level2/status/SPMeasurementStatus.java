package com.sensichips.sensiplus.level2.status;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameter;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemFilter;

import java.io.File;
import java.io.PrintWriter;

/**
 * Created by mario on 16/09/16.
 */
public abstract class SPMeasurementStatus {
    protected PrintWriter measureLog;
    protected SPProtocol spMnemonic;
    protected SPMeasurementParameter spParameter;
    protected SPMeasurement spMeasurement;

    protected SystemLevelChopper systemLevelChopper = null;
    private int lastPosADC = 0;
    private boolean pipelineFull = false;
    private int ADCPipelineMaxDim = 3;


    /*filter*/
    private int counter = 0;
    private double[][][] buffer;
    private int numOutput = 1;
    int dimBuffer = 0;
    private boolean firstMeasure = true;
    private boolean bufferFilled = false;

    // "" + logDirectory + "/Download/" + measureLogFile
    public SPMeasurementStatus(SPMeasurement spMeasurement, SPMeasurementParameter spParameter, int numOutputForChip,SPProtocol spMnemonic, String measureLogFile)throws SPException {

        try {
            File mPath = new File(measureLogFile);
            measureLog = new PrintWriter(mPath);
        } catch (Exception e) {
            measureLog = null;
        }
        this.spMnemonic = spMnemonic;
        this.spParameter = spParameter;
        this.spMeasurement = spMeasurement;
        this.numOutput = numOutputForChip;
        this.dimBuffer = Integer.parseInt(spParameter.getFilter());
        this.buffer = new double[spMeasurement.getClusterSize()][this.numOutput][this.dimBuffer];
        bufferFilled = !spParameter.isFillBufferBeforeStart();

    }

    public void resetBufferStatus(){
        this.buffer = new double[spMeasurement.getClusterSize()][this.numOutput][this.dimBuffer];
        counter = 0;
    }


    public SystemLevelChopper getSystemLevelChopper() {
        return systemLevelChopper;
    }

    public boolean isBufferFilled() {
        return bufferFilled;
    }

    public void setBufferFilled(boolean bufferFilled) {
        this.bufferFilled = bufferFilled;
    }

    public void updateStatus(double[][] output){
        //TODO:Take care at this threashold-> Temporary testing
        int backgroundThreshold = 50;
        counter++;
        // Media sugli ultimi dim_buffer con buffer a scorrimento
        if (firstMeasure) {
            //Inizializzazione
            firstMeasure = false;
            for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                for (int j = 0; j < numOutput; j++) {
                    for (int k = 0; k < dimBuffer; k++) {
                        buffer[i][j][k] = output[i][j];
                    }
                }
            }
        } else {

            // Ciclo normale
            int inizioCiclo = (dimBuffer - counter) > 0 ? (dimBuffer - counter) : 0;
            // i => indice sui chip
            // j => indice sulle uscite
            for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                for (int j = 0; j < numOutput; j++) {
                    for (int k = 0; k < dimBuffer - 1; k++) {
                        buffer[i][j][k] = buffer[i][j][k + 1];
                    }
                    buffer[i][j][dimBuffer - 1] = output[i][j];
                    output[i][j] = 0;
                    for (int k = inizioCiclo; k < dimBuffer; k++) {
                        output[i][j] += buffer[i][j][k];
                    }
                    output[i][j] = output[i][j] / (dimBuffer - inizioCiclo);
                }
            }
            if (((dimBuffer == Integer.parseInt(SPParamItemFilter.filterValues[SPParamItemFilter.filterValues.length - 1])
                    || dimBuffer == Integer.parseInt(SPParamItemFilter.filterValues[SPParamItemFilter.filterValues.length - 2]))
                    && counter == backgroundThreshold)) {

                int endIndex = (dimBuffer - counter) > 0 ? (dimBuffer - counter) : 0;
                int startIndex = endIndex - backgroundThreshold;
                counter = dimBuffer;
                while (startIndex >= 0) {
                    for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                        for (int j = 0; j < numOutput; j++) {
                            for (int k = startIndex; k < endIndex; k++) {
                                buffer[i][j][k] = buffer[i][j][k + backgroundThreshold];
                            }
                        }
                    }
                    endIndex -= backgroundThreshold;
                    startIndex -= backgroundThreshold;
                }
            }



        }

        // Crea log su file delle misure
        if (measureLog != null) {
            for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                for (int j = 0; j < numOutput; j++) {
                    measureLog.print("" + output[i][j] + "; ");
                }
            }
            measureLog.println();
            measureLog.flush();
        }
    }

    public double[][] getLastInsertedSample(){
        double[][] output = new double[spMeasurement.getClusterSize()][numOutput];
        if(counter>0) {
            for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                for (int j = 0; j < numOutput; j++) {
                    output[i][j] = buffer[i][j][dimBuffer - 1];
                }
            }
        }
        return output;
    }

    public void updateLog(byte[][] output){

        if (measureLog != null) {
            for(int k = 0; k < spMeasurement.getClusterSize(); k++){
                for (int i = 0; i < output[k].length; i++) {
                    measureLog.print("" + output[k][i] + " ");
                }
                measureLog.print("; ");
                measureLog.flush();
            }
        }
    }


    //The input parameter "currentMeasure" contains rxc elements,
    //where:
    //r is the number of chip
    //c is the number measures for each chip.
    //This method is transparent to the measurement type.
    //It changes the values contained in the input vector making the mean with the precedent ADCvalues
    public void updateSystemLevelChopper(double[][] currentMeasure){

        int[][]ADCValues = new int[currentMeasure.length][systemLevelChopper.getNumberOfElementsForChip()];
        for(int i=0; i<currentMeasure.length; i++){
            for(int j = 0; j< systemLevelChopper.getNumberOfElementsForChip(); j++){
                ADCValues[i][j] = (int)currentMeasure[i][j];
            }
        }


        double[][] result = systemLevelChopper.updateAndMean(ADCValues);
        for(int i=0; i<result.length; i++){
            for(int j = 0; j< systemLevelChopper.getNumberOfElementsForChip(); j++){
                currentMeasure[i][j] = result[i][j];
            }
        }

    }




}