package com.sensichips.sensiplus.level2.status;

import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterQI;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementSequential;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemFilter;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemHarmonic;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemInGain;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemRSense;
import com.sensichips.sensiplus.level2.util.SPMeasurementUtil;

import java.io.File;
import java.io.PrintWriter;

// Lettura dati fase/quadratura
public class SPMeasurementStatusIQ extends SPMeasurementStatus {
    /*protected double[][][] buffer;
    protected boolean firstMeasure = true;
    protected int dimBuffer = 0;*/
    private String instructionNum;
    private int counter;
    private double outGain;
    //private int numOutput = 0;

    private double inGain;
    private double Rsense;
    private double k;
    private SPMeasurement.AvailableFrequencies frequencyCalculus;
    private String instructionI_Prepared;
    private String instructionQ_Prepared;
    private int FRD = 0;
    private Double ConversionRate = 10.0;
    private String instruction;



    private SPMeasurementSequential spMeasurementSequential;


    public SPMeasurementStatusIQ(SPMeasurement spMeasurement, SPMeasurementParameterQI spParameter, SPProtocol spProtocol, int numOutput,
                                 SPMeasurement.AvailableFrequencies frequencyCalculus, int FRD, SPMeasurementSequential spMeasurementSequential, String measureLogFile)
            throws SPException {
        super(spMeasurement, spParameter,numOutput, spProtocol, measureLogFile);

        this.instruction = instruction;
        this.spMeasurementSequential = spMeasurementSequential;

        if(spMeasurement.isSystemLevelChopperActive()) {
            this.systemLevelChopper = new SystemLevelChopper(spMeasurement.getClusterSize(), 2);
        }
        //this.numOutput = numOutput;

        counter = 0;
        this.FRD = FRD;

        //bufferFilled = !spParameter.getSPMeasurementMetaParameter().getFillBufferBeforeStart();




        // Preparo file log letture
        File mPath = new File(measureLogFile);
        measureLog = null;

        try {
            measureLog = new PrintWriter(mPath);
        } catch (Exception e) {

        }

        //this.dimBuffer = Integer.parseInt(spParameter.getFilter());
        //buffer = new double[spMeasurement.getClusterSize()][numOutput][dimBuffer];
        this.frequencyCalculus = frequencyCalculus;

        // Calcoli costanti conversione EIS; servono in particolare per calcolare k
        double alfa = 1;
        outGain = Integer.parseInt(spParameter.getOutGain(), 2);

        int i = 0;
        while (i < SPParamItemInGain.ingainValues.length
                && !SPParamItemInGain.ingainValues[i].equalsIgnoreCase(spParameter.getInGain())) {
            i++;
        }
        inGain = Integer.parseInt(SPParamItemInGain.ingainLabels[i]);

        i = 0;
        while (i < SPParamItemRSense.rsenseValues.length
                && !SPParamItemRSense.rsenseValues[i].equalsIgnoreCase(spParameter.getRsense())) {
            i++;
        }
        Rsense = Integer.parseInt(SPParamItemRSense.rsenseLabels[i]);

        k = (alfa * Rsense * inGain) / ((8 - outGain) * Math.PI);


        // Preparo istruzione I
        String I_Q = spParameter.getQI().substring(0,1) + "0";
        //instructionReset();
        instruction = "WRITE CHA_FILTER S 0x";
        String HE3 = "0";
        if (spParameter.getHarmonic().equals(SPParamItemHarmonic.harmonicLabels[2])) {
            HE3 = "1";
        }

        if (frequencyCalculus.availableFrequency > 1000){
            instructionNum = "00000" + HE3 + spParameter.getHarmonic();
        } else {
            instructionNum = "00010" + HE3 + spParameter.getHarmonic();
        }

        //instructionNum = "000" + I_Q + HE3 + param.getHarmonic();
        instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);

        instructionI_Prepared = instruction;

        // Preparo istruzione Q
        I_Q = spParameter.getQI().substring(0,1) + "1";
        //instructionReset();
        instruction = "WRITE CHA_FILTER S 0x";


        if (frequencyCalculus.availableFrequency > 1000){
            instructionNum = "00001" + HE3 + spParameter.getHarmonic();
        } else {
            instructionNum = "00011" + HE3 + spParameter.getHarmonic();
        }



        //instructionNum = "000" + I_Q + HE3 + param.getHarmonic();
        instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);

        instructionQ_Prepared = instruction;
    }




    public String getInstructionToggleCHAFILTERPCI() {
        if(systemLevelChopper.isPCIBitHigh())
            instructionToggleCHAFILTERPCI = "WRITE CHA_FILTER M 0x5201";
        else
            instructionToggleCHAFILTERPCI = "WRITE CHA_FILTER M 0x1201";

        return instructionToggleCHAFILTERPCI;
    }

    private String instructionToggleCHAFILTERPCI = null;



   /* @Override
    public void updateStatus(double[][] output) {
        //TODO:Take care at this threashold-> Temporary testing
        int backgroundThreshold = 50;
        counter++;
        // Media sugli ultimi dim_buffer con buffer a scorrimento
        if (firstMeasure) {
            //Inizializzazione
            firstMeasure = false;
            for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                for (int j = 0; j < numOutput; j++) {
                    for (int k = 0; k < dimBuffer; k++) {
                        buffer[i][j][k] = output[i][j];
                    }
                }
            }
        } else {

            // Ciclo normale
            int inizioCiclo = (dimBuffer - counter) > 0 ? (dimBuffer - counter) : 0;
            // i => indice sui chip
            // j => indice sulle uscite
            for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                for (int j = 0; j < numOutput; j++) {
                    for (int k = 0; k < dimBuffer - 1; k++) {
                        buffer[i][j][k] = buffer[i][j][k + 1];
                    }
                    buffer[i][j][dimBuffer - 1] = output[i][j];
                    output[i][j] = 0;
                    for (int k = inizioCiclo; k < dimBuffer; k++) {
                        output[i][j] += buffer[i][j][k];
                    }
                    output[i][j] = output[i][j] / (dimBuffer - inizioCiclo);
                }
            }
            if (((dimBuffer == Integer.parseInt(SPParamItemFilter.filterValues[SPParamItemFilter.filterValues.length - 1])
                    || dimBuffer == Integer.parseInt(SPParamItemFilter.filterValues[SPParamItemFilter.filterValues.length - 2]))
                    && counter == backgroundThreshold)) {

                int endIndex = (dimBuffer - counter) > 0 ? (dimBuffer - counter) : 0;
                int startIndex = endIndex - backgroundThreshold;
                counter = dimBuffer;
                while (startIndex >= 0) {
                    for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                        for (int j = 0; j < numOutput; j++) {
                            for (int k = startIndex; k < endIndex; k++) {
                                buffer[i][j][k] = buffer[i][j][k + backgroundThreshold];
                            }
                        }
                    }
                    endIndex -= backgroundThreshold;
                    startIndex -= backgroundThreshold;
                }
            }



        }

        // Crea log su file delle misure
        if (measureLog != null) {
            for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                for (int j = 0; j < numOutput; j++) {
                    measureLog.print("" + output[i][j] + "; ");
                }
            }
            measureLog.println();
            measureLog.flush();
        }

    }*/




    public double getConversionRate() {
        return ConversionRate;
    }

    public void setConversionRate(double ConversionRate){
        this.ConversionRate = ConversionRate;
    }

    public int getFRD() {
        return FRD;
    }

    public SPMeasurement.AvailableFrequencies getFrequencyCalculus(){
        return frequencyCalculus;
    }

    public double getK(){
        return k;
    }

    public String getInstructionQ_Prepared(){
        return instructionQ_Prepared;
    }

    public String getInstructionI_Prepared(){
        return instructionI_Prepared;
    }

    public double getInGain() {
        return inGain;
    }

    public double getRsense() {
        return Rsense;
    }




    public SPMeasurementSequential getSpMeasurementSequential(){
        return spMeasurementSequential;
    }
}
