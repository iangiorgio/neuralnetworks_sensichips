package com.sensichips.sensiplus.level2.status;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementMetaParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPOT;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemInGain;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemPOTType;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemRSense;
import com.sensichips.sensiplus.level2.util.SPMeasurementUtil;
import com.sensichips.sensiplus.util.SPDelay;

import java.io.PrintWriter;

/**
 * Created by mario on 16/09/16.
 */
public class SPMeasurementStatusPOT extends SPMeasurementStatus {
    private double FinalPotential;
    private double InitialPotential;
    private double Step;
    private double Stimulus;
    private double PulseAmplitude;
    private double PulsePeriod;
    private double RSense;
    private double InGain;
    private double kADC;
    private int TypeVoltammetry, count, totStepUp, totStepDown;

    public Boolean getUp() {
        return up;
    }

    private Boolean up, alternativeSignal;
    private SPConfiguration spConfiguration;

    public int getRestartStimulusCounter() {
        return restartStimulusCounter;
    }


    public void setRestartStimulusCounter(int restartStimulusCounter) {
        this.restartStimulusCounter = restartStimulusCounter;
    }

    public boolean isCycleFinished() {
        return cycleFinished;
    }

    public void setCycleFinished(boolean cycleFinished) {
        this.cycleFinished = cycleFinished;
    }

    private boolean cycleFinished = false;

    private int restartStimulusCounter = 0;
    private boolean maintainStimulus=false;


    public SPMeasurementStatusPOT(SPMeasurement spMeasurement, SPConfiguration spConiguration, SPMeasurementParameterPOT spParameter,int numOutput, SPProtocol spProtocol, String measureLogFile) throws SPException {
        super(spMeasurement, spParameter,numOutput, spProtocol, measureLogFile);

        this.FinalPotential = spParameter.getFinalPotential();
        this.InitialPotential = spParameter.getInitialPotential();
        this.Step = spParameter.getStep();
        this.TypeVoltammetry = spParameter.getTypeVoltammetry();
        this.Stimulus = spParameter.getInitialPotential();
        this.PulseAmplitude = spParameter.getPulseAmplitude();
        this.PulsePeriod = spParameter.getPulsePeriod();
        this.alternativeSignal = spParameter.getAlternativeSignal();
        this.spConfiguration = spConiguration;

        //this.up = Up;
        this.count = 0;

        this.kADC = (SPMeasurementParameterPOT.Vref_plus - SPMeasurementParameterPOT.Vref_minus) / SPMeasurement.NORMALIZATION_FACTOR;
        this.totStepUp = 0;
        this.totStepDown = 0;

        this.up = (spParameter.getFinalPotential() - spParameter.getInitialPotential()) > 0;

        int i = 0;
        while (i < SPParamItemInGain.ingainValues.length
                && !SPParamItemInGain.ingainValues[i].equalsIgnoreCase(spParameter.getInGain())) {
            i++;
        }
        this.InGain = Integer.parseInt(SPParamItemInGain.ingainLabels[i]);

        i = 0;
        while (i < SPParamItemRSense.rsenseValues.length
                && !SPParamItemRSense.rsenseValues[i].equalsIgnoreCase(spParameter.getRsense())) {
            i++;
        }
        this.RSense = Integer.parseInt(SPParamItemRSense.rsenseLabels[i]) / (double) 1000; // valore in KOhm


    }



    public String getStim2Bit() {
        // convert status.NextStimulus
        int numBit = 12;
        int modSignDCBias = bitSignPOT((int) Stimulus, numBit);
        return SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBias, numBit);
    }

    public double normalizeVoltage(double ADC) {
        return (ADC * kADC) / (RSense * InGain);
    }


    public boolean updateStimulus() {

        boolean restartStimulus = restartStimulus();
        if (restartStimulus) {
            restartStimulusCounter++;
            if(restartStimulusCounter == 2){
                cycleFinished = true;
            }
        }

        if (TypeVoltammetry == SPParamItemPOTType.LINEAR_SWEEP_VOLTAMMETRY) {
            Stimulus += up ? 1 : -1;
        } else if (TypeVoltammetry == SPParamItemPOTType.POTENTIOMETRIC) {

        } else if (TypeVoltammetry == SPParamItemPOTType.CURRENT) {

        } else if (TypeVoltammetry == SPParamItemPOTType.STAIRCASE_VOLTAMMETRY) {
            Stimulus += Step;

        } else if (TypeVoltammetry == SPParamItemPOTType.SQUAREWAVE_VOLTAMMETRY) {
            if (count == 0) {
                Stimulus += PulseAmplitude / 2;
            } else if (count % 2 == 1) {  // DISPARI
                Stimulus -= PulseAmplitude;
            } else {        // PARI
                Stimulus += PulseAmplitude + Step;
            }

        } else if (TypeVoltammetry == SPParamItemPOTType.NORMAL_PULSE_VOLTAMMETRY) {
            if (up && count % 2 == 0 || !up && count % 2 == 1) {     // PARI
                if (up) {
                    totStepUp++;
                    Stimulus = InitialPotential + totStepUp * Step;
                } else {
                    totStepDown++;
                    Stimulus = InitialPotential + Step * (totStepUp - totStepDown);
                }
            } else {        // DISPARI
                Stimulus = InitialPotential;
            }
        } else if (TypeVoltammetry == SPParamItemPOTType.DIFFERENTIAL_PULSE_VOLTAMMETRY) {
            if(!maintainStimulus) {
                if (count % 2 == 0) { // PARI
                    Stimulus += PulseAmplitude;
                } else {        // DISPARI
                    Stimulus -= (PulseAmplitude - Step);
                }
            }

            /*int sign = up?1:-1;
            if (count % 2 == 0) { // PARI

                Stimulus += sign*PulseAmplitude;
            } else {        // DISPARI
                Stimulus += sign*(PulseAmplitude - Step);
            }*/

        }
        //if (up) {
        Stimulus = (Stimulus > SPMeasurementParameterPOT.Vref_plus ? SPMeasurementParameterPOT.Vref_plus : Stimulus);
        //} else {
        Stimulus = (Stimulus < SPMeasurementParameterPOT.Vref_minus ? SPMeasurementParameterPOT.Vref_minus : Stimulus);
        //}



        count++;

        return restartStimulus;
    }

    public void resetStimulus(){
        this.restartStimulusCounter = 0;
        this.Stimulus = this.InitialPotential;
        count = 0;
    }

    public boolean restartStimulus() {
        boolean out = false;
        if (TypeVoltammetry == SPParamItemPOTType.NORMAL_PULSE_VOLTAMMETRY) {
            if (up && Step * totStepUp >= (FinalPotential - InitialPotential)
                    || !up && (Step * (totStepUp - totStepDown)) <= 0) {
                out = true;
                count = 0;
                up = !up;
                if (up) {
                    totStepUp = 0;
                } else {
                    totStepDown = 0;
                }
            }
        } else {
            if (this.TypeVoltammetry != SPParamItemPOTType.NORMAL_PULSE_VOLTAMMETRY &&
                    (up && Stimulus >= FinalPotential || !up && Stimulus <= FinalPotential)) { //TODO:Attention modified Stimulus<=FinalPotential con Stimulus <= InitlialPotential

                if(this.TypeVoltammetry == SPParamItemPOTType.DIFFERENTIAL_PULSE_VOLTAMMETRY && !maintainStimulus && up){
                    maintainStimulus=true;

                }else {

                    out = true;
                    // Azzerare count
                    count = 0;
                    // invertire up
                    if (alternativeSignal) {
                        // Invertire final e initial
                        double temp = FinalPotential;
                        FinalPotential = InitialPotential;
                        InitialPotential = temp;
                        // Invertire up
                        up = !up;

                        if (this.TypeVoltammetry == SPParamItemPOTType.DIFFERENTIAL_PULSE_VOLTAMMETRY) {
                            maintainStimulus = false;

                            PulseAmplitude = -PulseAmplitude;
                        }
                        // Invertire step
                        Step = -Step;

                    } else {
                        Stimulus = InitialPotential;
                    }
                }


            }
        }
        return out;
    }

    public void readingDelay() {
        if (TypeVoltammetry == SPParamItemPOTType.LINEAR_SWEEP_VOLTAMMETRY) {
            // NO DELAY
        } else if (TypeVoltammetry == SPParamItemPOTType.STAIRCASE_VOLTAMMETRY) {
            SPDelay.delay((long) PulsePeriod);
        } else if (TypeVoltammetry == SPParamItemPOTType.SQUAREWAVE_VOLTAMMETRY ||
                TypeVoltammetry == SPParamItemPOTType.NORMAL_PULSE_VOLTAMMETRY ||
                TypeVoltammetry == SPParamItemPOTType.DIFFERENTIAL_PULSE_VOLTAMMETRY) {
            SPDelay.delay((long) PulsePeriod / 2);

        }
    }


    private int bitSignPOT(int x, int numBit) {
        int sign = (int) Math.pow(2, numBit - 1); //RUN4 (numBit - 1)
        if (spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5) ||
                spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN6)){
            sign = (int) Math.pow(2, numBit);
        }
        double m = (double) sign / (SPMeasurementParameterPOT.Vref_plus - SPMeasurementParameterPOT.Vref_minus);
        int out = (int) (m * x);
        int _7FF = 0;
        if (spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5) ||
                spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN6)){
            _7FF  = (int) Math.pow(2, numBit - 1) - 1; // NON sommare per RUN4
        }
        out = out + _7FF;
        if (spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5) ||
                spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN6)){
            out = out >= sign ? (sign - 1) : out;
            out = out < 0 ? 0 : out;
        } else {
            sign = out < 0 ? sign : 0;
            out = Math.abs(out);
            out = out | sign;
        }
        return out;
    }

    public double getStimulus(){
        return Stimulus;
    }

    public double getkADC(){
        return kADC;
    }


    public void updateLog(double[][] output,int[][] read){
        if (measureLog != null) {
            for(int k = 0; k < spMeasurement.getClusterSize(); k++){
                measureLog.println("" + getStimulus() + "; " + getStim2Bit() + "; " +
                        read[k][0] + "; " + output[k][1] + "; " + getkADC() + ";");
                measureLog.flush();
            }
        }
    }
}
