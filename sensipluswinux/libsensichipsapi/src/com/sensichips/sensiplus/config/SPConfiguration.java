package com.sensichips.sensiplus.config;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;

import com.sensichips.sensiplus.level1.protocols.cc2541.SPProtocolCC2541_SPI;
import com.sensichips.sensiplus.level1.protocols.esp8266.SPProtocolESP8266_SENSIBUS;
import com.sensichips.sensiplus.level1.protocols.ft232rl.SPProtocolFT232RL_BUSPIRATE;
import com.sensichips.sensiplus.level1.protocols.ft232rl.SPProtocolFT232RL_I2C;
import com.sensichips.sensiplus.level1.protocols.ft232rl.SPProtocolFT232RL_SENSIBUS;
import com.sensichips.sensiplus.level1.protocols.ft232rl.SPProtocolFT232RL_SPI;

import com.sensichips.sensiplus.level1.chip.*;
import com.sensichips.sensiplus.level1.protocols.stm32l0.SPProtocolSTM32_SENSIBUS;
import com.sensichips.sensiplus.level2.*;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementMetaParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterActuator;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterSENSORS;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.SPExtendedInstructionManager;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class SPConfiguration implements Serializable {

    public static final long serialVersionUID = 42L;

    // Power supply management

    public static final String VCC_MODE_NAME_RATIOMETRIC = "RATIOMETRIC";

    public static final int VCC_MODE_NORMAL = 0;
    public static final int VCC_MODE_RATIOMETRIC = 1;
    public int VCC_MODE = VCC_MODE_NORMAL;

    public static final int VCC_DEFAULT = 3300;
    private int VCC = VCC_DEFAULT; // -- [mV]

    public static final int VCC_Th1 = 1800;
    public static final int VCC_Th2 = 2500;
    public static final int VCC_Th3 = 3300;

    public static final int VREF_1 = 1300;
    public static final int VREF_2 = 2000;
    public static final int VREF_3 = 2800;

    private int VREF = VREF_3;


    private int Vdd = 3300;


    public int getVdd() {
        return Vdd;
    }

    public void setVdd(int vdd) {
        Vdd = vdd;
    }



    public static String API_OWNER_MCU = "MCU";
    public static String API_OWNER_PC = "PC";




    protected static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L2;

    //public static final String COM_USB = "COM_USB";
    //public static final String COM_BLE = "COM_BLE";


    private String protocol="";
    private int addressingType = SPProtocol.FULL_ADDRESS;


    private ArrayList<String> driverParameters = new ArrayList<>();

    private String driverName = "";
    private String hostController = "";
    private String apiOwner = "";
    private String mcu = "";
    private String configurationName = null;
    private String description = "";

    private SPCluster cluster;
    private SPProtocol spProtocol;

    private SPDriver spDriver;
    private Hashtable<String, SPMeasurement> spMeasurementHashtable = new Hashtable<>();

    private List<SPChip> spInstalledChips = new ArrayList<>();

    public static final String LOG_MESSAGE = "SPConfiguration";


    public SPConfiguration () throws SPException {
        this.cluster = new SPCluster();
        setVCC("3300");
    }

    public void addDriverParam(String driverParameter){
        driverParameters.add(driverParameter);
    }




    public SPDriver getSpDriver() {
        return spDriver;
    }



    public int getVREF(){
        return VREF;
    }


    //public static final int VCC_Th1 = 1400;
    //public static final int VCC_Th2 = 2200;
    //public static final int VCC_Th3 = 3000;

    //public static final int VREF_1 = 1200;
    //public static final int VREF_2 = 2000;
    //public static final int VREF_3 = 2800;

    public void setVCC(String VCCString) throws SPException{
        if (VCCString.equals(VCC_MODE_NAME_RATIOMETRIC)){
            VCC_MODE = VCC_MODE_RATIOMETRIC;
            VCC = VCC_DEFAULT;
            VREF = VREF_3;
        } else {

            VCC_MODE = VCC_MODE_NORMAL;
            try{
                VCC = Integer.parseInt(VCCString);
            } catch(NumberFormatException ne){
                throw new SPException("Wrong VCC format: numeric or \"RATIOMETRIC\" values are expected.");
            }

            if (VCC >= VCC_Th1 && VCC < VCC_Th2) {          // 1800 - 2500// 00 => 1300
                VREF = VREF_1;
            } else if (VCC >= VCC_Th2 && VCC < VCC_Th3) {   // 2500 - 3300 => 2000
                VREF = VREF_2;
            } else if (VCC >= VCC_Th3) {                    // 3300 -      => 2800
                VREF = VREF_3;
            }
        }
    }



    public List<SPChip> getSpInstalledChips() {
        return spInstalledChips;
    }

    public void setSpInstalledChips(List<SPChip> spInstalledChips) {
        this.spInstalledChips = spInstalledChips;
    }


    public int getVCC(){
        return VCC;
    }

    public String getConfigurationName() {

        //if (configurationName == null){
            configurationName = driverName + "-" + "CLUSTERID_" + getCluster().getClusterID() + "-" +
                    getMcu() + "_" +
                    protocol + "-" + SPProtocol.ADDRESSING_TYPES[getAddressingType()] + "-" + description.trim();
        //}

        return configurationName;
    }

    public void changeRedLEDStatusChipsWithBandgap(int mode) throws SPException{
        if (this.getCluster().getChipWithBandgapProblem().size() > 0) {
            ArrayList<String> chipsAddress = new ArrayList<>();
            for (int i = 0; i < this.getCluster().getChipWithBandgapProblem().size(); i++) {
                chipsAddress.add(this.getCluster().getChipWithBandgapProblem().get(i).getSerialNumber());
            }

            SPCluster tmp = this.getCluster().generateTempCluster(chipsAddress);

            this.changeStateRedLED(mode, tmp);
        }
    }

    public void setDescription(String description){
        this.description = description;
    }


    public String getDescription(){
        return this.description;
    }


    public SPMeasurement getSPMeasurement(String measurementType) throws SPException {
        SPMeasurement spMeasurement = null;


        verifyMeasurementType(measurementType);

        if (cluster.getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN4)) {
            spMeasurement = new SPMeasurementRUN4(this, null, measurementType);
        } else if (cluster.getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)){
            if (this.getApiOwner().equals(API_OWNER_PC)){
                spMeasurement = new SPMeasurementRUN5_PC(this, null, measurementType);
            } else {
                spMeasurement = new SPMeasurementRUN5_MCU(this, null, measurementType);
            }
        } else if (cluster.getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN6)){
            if (this.getApiOwner().equals(API_OWNER_PC)) {
                spMeasurement = new SPMeasurementRUN6_PC(this, null, measurementType);
            } else {
                throw new SPException("SPMeasurement MCU for: " + cluster.getFamilyOfChips().getHW_VERSION() + " unavailable!!");
            }
        } else {
            throw new SPException("Chip version: " + cluster.getFamilyOfChips().getHW_VERSION() + " unavailable!!");
        }

        return spMeasurement;
    }


    /**
     * Generate one SPMeasurement for each Measure_Type available and any Sensor available
     * @param measurementType
     * @return
     * @throws SPException
     */
    public SPMeasurement getManagedSPMeasurement(String measurementType) throws SPException {
        SPMeasurement spMeasurement = spMeasurementHashtable.get(measurementType);

        if (spMeasurement != null){
            return spMeasurement;
        }

        verifyMeasurementType(measurementType);

        if (cluster.getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN4)) {
            spMeasurement = new SPMeasurementRUN4(this, null, measurementType);
            spMeasurementHashtable.put(measurementType, spMeasurement);
        } else if (cluster.getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)){
            spMeasurement = new SPMeasurementRUN5_PC(this, null, measurementType);
            spMeasurementHashtable.put(measurementType, spMeasurement);
        } else if (cluster.getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN6)){
            spMeasurement = new SPMeasurementRUN5_PC(this, null, measurementType);
            spMeasurementHashtable.put(measurementType, spMeasurement);
        } else {
            throw new SPException("Type of measure or Sensing Element: " + measurementType + " unavailable!!");
        }

        return spMeasurement;
    }

    public void verifyMeasurementType(String measurementType) throws SPException {
        ArrayList<String> list1 = cluster.getFamilyOfChips().getMeasureTypes();
        int i = 0;
        boolean found = false;
        while(i < list1.size() && !(found = list1.get(i).equals(measurementType))){
            i++;
        }
        if (!found){
            List<SPSensingElementOnFamily> list2 = cluster.getFamilyOfChips().getSensingElementOnFamilyList();
            i = 0;
            while(i < list2.size() && !(found = list2.get(i).getID().equals(measurementType))){
                i++;
            }
        }
        if (!found){
            throw new SPException("Type of measure or Sensing Element: " + measurementType + " unavailable!!");
        }
    }


    public SPProtocol getSPProtocol() {
        return spProtocol;
    }


    public void generateAndOpenDriver(SPConfigurationManager.SPDriverGenerator driverGenerator) throws SPDriverException{
        String key = driverGenerator.generateDriver(driverName, driverParameters);
        spDriver = SPDriverManager.get(key);
        spDriver.openConnection();

    }




    public void generateAndOpenProtocol(int addressingMode) throws SPException,SPProtocolException,SPDriverException {

        if (spProtocol == null ) {
            if (mcu.equals(SPProtocolFT232RL_BUSPIRATE.MCU)) {
                if (protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI])) {
                    spProtocol = new SPProtocolFT232RL_SPI(spDriver, getCluster(), addressingMode);
                } else if (protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C])) {
                    spProtocol = new SPProtocolFT232RL_I2C(spDriver, getCluster(), addressingMode);
                } else if (protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS])) {
                    spProtocol = new SPProtocolFT232RL_SENSIBUS(spDriver, getCluster(), addressingMode);
                } else if (protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.BUSPIRATE])) {
                    spProtocol = new SPProtocolFT232RL_BUSPIRATE(spDriver, getCluster(), addressingMode);
                }
            } else if (mcu.equals(SPProtocolCC2541_SPI.MCU)) {
                if (protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI])) {
                    spProtocol = new SPProtocolCC2541_SPI(spDriver, getCluster(), addressingMode);
                }
            } /*else if (mcu.equals(SPProtocolSTM32L0_SPI.MCU)) {
                if (protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI])) {
                    spProtocol = new SPProtocolSTM32L0_SPI(spDriver, getCluster(), addressingMode);
                }*/
            else if(mcu.equals(SPProtocolSTM32_SENSIBUS.MCU)){
                if (protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS])){
                    spProtocol = new SPProtocolSTM32_SENSIBUS(spDriver, getCluster(), addressingMode);
                }
            } else if (mcu.equals(SPProtocolESP8266_SENSIBUS.MCU)){
                if (protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS])){
                    spProtocol = new SPProtocolESP8266_SENSIBUS(spDriver, getCluster(), addressingMode);
                }
            }

            if (spProtocol == null) {
                throw new SPException("Any protocol available for: " + mcu + ", " + protocol + ". Verify config.batch file");
            }
        }
    }

    public ArrayList<String> getDriverParameters() {
        return driverParameters;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getHostController() {
        return hostController;
    }

    public void setHostController(String hostController) {
        this.hostController = hostController;
    }

    public String getApiOwner() {
        return apiOwner;
    }

    public void setApiOwner(String apiOwner) {
        this.apiOwner = apiOwner;
    }

    public String getMcu() {
        return mcu;
    }

    public void setMcu(String mcu) {
        this.mcu = mcu;
    }


    public SPCluster getCluster() {
        return cluster;
    }

    public void setCluster(SPCluster cluster) throws SPException {

        int MAX_CHIPS = 255;

        if(protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI]) && cluster.getActiveSPChipList().size()!=1){
            throw new SPException("error in setCluster - wrong number of chip for SPI protocol");
        }
        else if(protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C]) && cluster.getActiveSPChipList().size()>MAX_CHIPS){
            throw new SPException("error in setCluster - wrong number of chip for I2C protocol");
        }
        else if(protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS])){
            if(addressingType == SPProtocol.NO_ADDRESS && cluster.getActiveSPChipList().size()!=1){
                throw new SPException("error in setCluster - With NO_ADRESS, max chip number is 1");
            }
            else if(addressingType == SPProtocol.SHORT_ADDRESS && this.cluster.getActiveSPChipList().size()>MAX_CHIPS){
                throw new SPException("error in setCluster - With SHORT_ADDRESS, max chip number is 255");
            }
        }
        this.cluster = cluster;
    }




    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) throws SPException{

        if(protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI])||protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C])||protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS])) {
            this.protocol = protocol;
        }else{
            throw new SPException("error in setProtocolName - Wrong protocol name  ");
        }
    }
    public String toString(){
        String out = "\n";
        out += "protocol:       " + protocol + "\n";
        out += "addressingType: " + SPProtocol.ADDRESSING_TYPES[addressingType] + "\n";
        out += "driverName:         " + driverName + "\n";
        out += "hostController: " + hostController + "\n";
        out += "apiOwner:       " + apiOwner + "\n";
        out += "mcu:            " + mcu + "\n";
        //out += "cluster:        " + "\n" + cluster + "\n";
        return out;
    }

    public int getAddressingType() {
        return addressingType;
    }

    public void setAddressingType(String addressingType) throws SPException {
        if(addressingType.equals(SPProtocol.ADDRESSING_TYPES[SPProtocol.NO_ADDRESS]) ||
                addressingType.equals(SPProtocol.ADDRESSING_TYPES[SPProtocol.SHORT_ADDRESS]) ||
                addressingType.equals(SPProtocol.ADDRESSING_TYPES[SPProtocol.FULL_ADDRESS])) {

            if(addressingType.equals(SPProtocol.ADDRESSING_TYPES[SPProtocol.NO_ADDRESS])) {
                if(protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C])){
                    throw new SPException("error in setAddressingType - No-Address can't be used with I2C protocol");
                } else{
                    this.addressingType = SPProtocol.NO_ADDRESS;
                }
            } else if(addressingType.equals(SPProtocol.ADDRESSING_TYPES[SPProtocol.SHORT_ADDRESS])){
                if(protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI])){
                    throw new SPException("error in setAddressingType - Short-Address can't be used with SPI protocol");
                } else{
                    this.addressingType = SPProtocol.SHORT_ADDRESS;
                }
            } else{
                if(protocol.equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS])){
                    this.addressingType = SPProtocol.FULL_ADDRESS;
                } else{
                    throw new SPException("error in setAddressingType - Full-Address can't be used with the protocol you specified");
                }
            }
        }else{
            throw new SPException("error in setProtocolName - Wrong protocol name  ");
        }
    }

    public SPSensingElementOnChip searchSPSensingElementOnChip(String sensorName) throws SPException {
        //SPSensingElementOnChip found = null;
        List<SPSensingElementOnChip> list = cluster.getActiveSPChipList().get(0).getSensingElementOnChipList();
        int i = 0;
        while(i < list.size() && !(list.get(i).getSensingElementOnFamily().getID().equals(sensorName))){
            i++;
        }
        if (i < list.size()){
            return list.get(i);
        }
        String msg = "Sensor name: " + sensorName + " not found in configuration: " + getConfigurationName() + "\n";
        msg += "Sensors available are:\n";
        for(i = 0; i < list.size(); i++){
            msg += list.get(i).getSensingElementOnFamily().getID() + "\n";
        }
        throw new SPException(msg);

    }


    private long blinkingTime = 100;

    public synchronized void stopConfiguration(boolean executeBlinking) throws SPException {


        if (getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)) {



            this.changeRedLEDStatusChipsWithBandgap(SPMeasurementParameterActuator.OFF);

            while (cluster.getChipWithBandgapProblem().size() > 0) {
                cluster.getChipWithBandgapProblem().remove(0);
            }

            cluster.setSPChipAllActive();



            if (executeBlinking) {
                try {
                    totalBlinking();
                } catch (SPException spe) {
                    spe.printStackTrace();
                }
            }

            //resetConfiguration();

            try {
                spProtocol.closeAll();
            } catch (SPProtocolException spe) {
                spe.printStackTrace();
            }
            spProtocol = null;

            try {
                spDriver.closeConnection();
            } catch (SPDriverException spe) {
                spe.printStackTrace();
            }
        } else if (getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN6)) {

            resetConfiguration();

            try {
                spProtocol.closeAll();
            } catch (Exception spe) {
                spe.printStackTrace();
            }
            spProtocol = null;

            try {
                spDriver.closeConnection();
            } catch (Exception spe) {
                spe.printStackTrace();
            }
            spDriver = null;

        }
    }

    public void resetConfiguration() throws SPException{

        if(getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)) {
            spProtocol.sendInstruction("WRITE COMMAND S 0x02");
        }else if(getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN6)){
            spProtocol.sendInstruction("WRITE COMMAND S 0x42");
        }

    }

    public void standby() throws SPException{
        //spProtocol.sendInstruction("WRITE DSS_SELECT S 0X14",null);     //Stop ANALOGICAL BLOCK
        spProtocol.sendInstruction("WRITE DSS_SELECT M 0X0714",null);     //Stop ANALOGICAL BLOCK
    }

    public synchronized void startConfiguration(SPConfigurationManager.SPDriverGenerator spDriverGenerator,
                                                boolean ledBlink,
                                                boolean activateMulticast,
                                                boolean chipDetection, boolean chipDetectionStopOnError,
                                                boolean bandgapRestart, boolean bandgapRestartStopOnError,
                                                boolean disableChipBandgapProblem) throws SPException{


        try {


            SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setINIT", DEBUG_LEVEL);

            getCluster().setSPChipAllActive();//TODO: verificare se è necessario


            generateAndOpenDriver(spDriverGenerator);

            // Workaround: in this way all address modality works. See (**
            //generateAndOpenProtocol(SPProtocol.SHORT_ADDRESS);
            //generateAndOpenProtocol(getAddressingType());


            if (this.getSpInstalledChips().size() > 0) {
                throw new SPException("Please verify this code after translation of generateAndOpenProtocol(...);");
                // Todo: restore the following instruction
                //getCluster().setChipList(getSpInstalledChips());


                /*getCluster().setSPChipAllDisactive();
                ArrayList<String> chipIDs = new ArrayList<>();
                for(int i=0; i<this.getSpInstalledChips().size(); i++){
                    chipIDs.add(getSpInstalledChips().get(i).getSerialNumber());
                }

                getCluster().setSPChipActivationList(chipIDs, true);*/


            }

            if (getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)) {
                // Workaround: in this way all address modality works. See (**

                generateAndOpenProtocol(SPProtocol.SHORT_ADDRESS);
                //generateAndOpenProtocol(getAddressingType());


                // TRIM clock on all chips
                //spProtocol.sendInstruction("WRITE USER_EFUSE S " + getCluster().getFamilyOfChips().getOSC_TRIM());
                // Set the Multicast address to the chip belonging to the group by using unicast address
                spProtocol.sendInstruction("WRITE COMMAND S 0x02"); //Force ID reload

                if (activateMulticast) {
                    spProtocol.activateMulticast();
                }

                trimOscillatorRUN5();

                spProtocol.sendInstruction("WRITE COMMAND S 0x14"); //Force ID reload


                if (chipDetection) {
                    //spProtocol.filterOutUnavailableChips();
                    spProtocol.filterOutUnavailableChipsNew();
                    if (chipDetectionStopOnError && getCluster().getActiveSPChipList().size() <= 0) {
                        throw new SPException("No chip detected on the cable");
                    }
                }

                // Verify bandgap and try to restart
                // Set bandgap
                spProtocol.sendInstruction("WRITE ANA_CONFIG M 0x0000");
                spProtocol.sendInstruction(getSPBandGapFromVCC());
                spProtocol.sendInstruction("WRITE ANA_CONFIG S 0x1B");

                spProtocol.sendChipListToMCU();
                //spProtocol.testSendJSON(); //Aggiunto 17/05/2019 Luca
                //SPExtendedInstructionManager.testSendJSON(spProtocol, apiOwner); //Aggiunto 25/05/2019 Luca
                //SPExtendedInstructionManager.detectChip(this, spProtocol); //Aggiunto 25/05/2019 Luca

                if (bandgapRestart) {
                    try {
                        restartBandGap(disableChipBandgapProblem);
                    } catch (SPException spe) {
                        spe.printStackTrace();
                        if (bandgapRestartStopOnError) {
                            throw spe;
                        }
                    }
                }
                // END Verify CHIP availability

                // Turn on sequentially green led
                if (ledBlink) {
                    if (chipDetection && getCluster().getActiveSPChipList().size() > 0) {
                        sequenceBlinking();
                    }
                    //totalBlinking();
                }


                // Workaround: in this way all address modality works. See (**
                spProtocol.setAddressingType(getAddressingType());


            } else if (getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN6)) {

                generateAndOpenProtocol(SPProtocol.FULL_ADDRESS);

                spProtocol.sendInstruction("WRITE COMMAND S 0x40"); //Force ID reload

                spProtocol.setAddressingType(getAddressingType());


                if (activateMulticast) {
                    spProtocol.activateMulticast();
                }

                trimOscillatorRUN6();

                spProtocol.sendInstruction("WRITE INT_CONFIG S 0x01");


                if (chipDetection) {
                    //spProtocol.filterOutUnavailableChips();
                    spProtocol.filterOutUnavailableChipsNew();
                    if (chipDetectionStopOnError && getCluster().getActiveSPChipList().size() <= 0) {
                        throw new SPException("No chip detected on the cable");
                    }
                }

                // Set bandgap
                spProtocol.sendInstruction("WRITE ANA_CONFIG M 0x0000");
                spProtocol.sendInstruction(getSPBandGapFromVCC());

                // TODO: Verify this instruction
                //spProtocol.sendInstruction("WRITE ANA_CONFIG S 0x1B");


            } else { // RUN4
                //dataToDIGTest = getCluster().getFamilyOfChips().getOSC_TRIM();
                spProtocol.sendInstruction("WRITE DIG_CONFIG M " + getCluster().getFamilyOfChips().getOSC_TRIM() + "02");
                spProtocol.sendInstruction("WRITE INT_CONFIG S 0x01");
            }
            SPLogger.getLogInterface().d(LOG_MESSAGE, "End setINIT", DEBUG_LEVEL);
        } catch (SPException spe){
            try{
                stopConfiguration(false);
            } catch (Exception ex){
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception in stopConfiguration", DEBUG_LEVEL);
            }
            throw spe;
        }
    }





    private void trimOscillatorRUN6() throws SPException {


        //WRITE DIG_CONFIG	M 0xOSC_TRIM&06 -- Tune internal oscillator to 10MHz, INT1 is Push-Pull, Interrupts are latched
        //WRITE USER_EFUSE	S 0xOSC_TRIM  	-- RUN5 | RUN6: Soft trim internal oscillator to 10MHz. Not required when factory trimmed

        List<SPChip> chips = getCluster().getActiveSPChipList();
        for(int i = 0; i < chips.size(); i++){
            List<String> list = new ArrayList<>();
            list.add(chips.get(i).getSerialNumber());
            SPCluster tempCluster = getCluster().generateTempCluster(list);
            spProtocol.sendInstruction("WRITE DIG_CONFIG M " + chips.get(i).getOscTrimOverride() + "06", tempCluster );
            spProtocol.sendInstruction("WRITE USER_EFUSE S " + chips.get(i).getOscTrimOverride(), tempCluster );
        }
    }



    private void trimOscillatorRUN5() throws SPException {
        List<SPChip> chips = getCluster().getActiveSPChipList();
        for(int i = 0; i < chips.size(); i++){
            List<String> list = new ArrayList<>();
            list.add(chips.get(i).getSerialNumber());
            SPCluster tempCluster = getCluster().generateTempCluster(list);
            spProtocol.sendInstruction("WRITE USER_EFUSE S " + chips.get(i).getOscTrimOverride(), tempCluster );
        }
    }

    public void changeStateRedLED(int mode, SPCluster cluster) throws SPProtocolException,SPException {
        //SPCluster greenLed = cluster.generateTempCluster(cluster.getSPActiveSerialNumbers());
        //getSPProtocol().sendInstruction("WRITE INT_CONFIG+1 S 0x02", greenLed, blinkingTime); // Turn on green light

        SPMeasurementParameterActuator actuator = new SPMeasurementParameterActuator(this);
        actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_INT);
        actuator.setTemporaryCluster(cluster);
        actuator.setMode(mode);
        getSPMeasurement("EIS").setACTUATOR(actuator);


    }

    public void sequenceBlinking() throws SPProtocolException {
        SPCluster greenLed = cluster.generateTempCluster(cluster.getSPActiveSerialNumbers());

        getSPProtocol().sendInstruction("WRITE INT_CONFIG+1 S 0x02", greenLed, blinkingTime); // Turn on green light

    }

    public void totalBlinking() throws SPProtocolException {
        // Turn off green led
        for(int i = 0; i < 2; i++){
            SPDelay.delay(blinkingTime);
            getSPProtocol().sendInstruction("WRITE INT_CONFIG+1 S 0x00", null); // Turn on green light all togheter
            SPDelay.delay(blinkingTime);
            getSPProtocol().sendInstruction("WRITE INT_CONFIG+1 S 0x02", null); // Turn on green light all togheter
            SPDelay.delay(blinkingTime);
            getSPProtocol().sendInstruction("WRITE INT_CONFIG+1 S 0x00", null); // Turn on green light all togheter
        }
    }



    public void turnOffGreenLed() throws SPException {
        // TODO: remove comments here
        getSPProtocol().sendInstruction("WRITE INT_CONFIG+1 S 0x00", null); // Turn off green light
    }




    // TODO: verify if is it possible to remove; introduced in order to resolve bandgap problem RUN5
    private void restartBandGap(boolean disableChipBandgapProblems) throws SPException {

        SPMeasurementRUN5_PC measurementTemp = (SPMeasurementRUN5_PC) getSPMeasurement("ONCHIP_VOLTAGE");
        SPMeasurementMetaParameter meta = new SPMeasurementMetaParameter(this.getCluster().getActiveSPChipList().size());
        meta.setRenewBuffer(true);
        //meta.setBurst(1);
        //meta.setFillBufferBeforeStart(false);
        meta.setRestartBandGap(false);

        SPMeasurementParameterSENSORS tempParam = new SPMeasurementParameterSENSORS(this,"ONCHIP_VOLTAGE");
        //tempParam.setSensorName("ONCHIP_VOLTAGE");
        tempParam.setSPMeasurementMetaParameter(meta);
        tempParam.setPort("PORT_VOLTAGE");
        tempParam.setFilter("1");
        tempParam.setBurstMode(false);
        tempParam.setFillBufferBeforeStart(false);

        tempParam.setFIFO_DEEP(1);
        tempParam.setFIFO_READ(1);

        measurementTemp.restartBandGap(tempParam,disableChipBandgapProblems);
    }
    //spProtocol.sendInstruction("WRITE ANA_CONFIG M 0x041B");

    public String getSPBandGapMIN(){
        //return "WRITE ANA_CONFIG+1 S 0x0C";
        return "WRITE ANA_CONFIG+1 S 0x0C"; // In order to resolve bandgap problem
    }

    public String getSPBandGapRATIOMETRIC(){
        //return "WRITE ANA_CONFIG+1 S 0x00";
        return "WRITE ANA_CONFIG+1 S 0x00";
    }


    public String getSPBandGapFromVCC() throws SPException {
        String output = "WRITE ANA_CONFIG+1 S 0x0";
        if (VCC_MODE == VCC_MODE_RATIOMETRIC){
            output = getSPBandGapRATIOMETRIC();
        } else {
            //String instruction = "WRITE ANA_CONFIG M 0x0";
            if (getVREF() == VREF_1) {
                output = getSPBandGapMIN();
            } else if (getVREF() == VREF_2) {
                output += "8";
            } else if (getVREF() == VREF_3) {
                output += "4";
            } else {
                throw new SPException("setINIT: unexpected VREF value in VCC_NORMAL mode: " + VREF);
            }
        }
        return output;
    }

}
