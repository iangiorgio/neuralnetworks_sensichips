//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Cluster complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Cluster">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CLUSTER_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MULTICAST_CLUSTER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CHIP" type="{}Chip" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cluster", propOrder = {
    "clusterid",
    "multicastcluster",
    "chip"
})
public class Cluster {

    @XmlElement(name = "CLUSTER_ID", required = true)
    protected String clusterid;
    @XmlElement(name = "MULTICAST_CLUSTER", required = true)
    protected String multicastcluster;

    //Aggiunto 26/05/2019 Luca
    public void setChip(List<Chip> chip) {
        this.chip = chip;
    }

    @XmlElement(name = "CHIP", required = true)
    protected List<Chip> chip;

    /**
     * Recupera il valore della propriet� clusterid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLUSTERID() {
        return clusterid;
    }

    /**
     * Imposta il valore della propriet� clusterid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLUSTERID(String value) {
        this.clusterid = value;
    }

    /**
     * Recupera il valore della propriet� multicastcluster.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMULTICASTCLUSTER() {
        return multicastcluster;
    }

    /**
     * Imposta il valore della propriet� multicastcluster.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMULTICASTCLUSTER(String value) {
        this.multicastcluster = value;
    }

    /**
     * Gets the value of the chip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCHIP().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Chip }
     * 
     * 
     */
    public List<Chip> getCHIP() {
        if (chip == null) {
            chip = new ArrayList<Chip>();
        }
        return this.chip;
    }

}
