//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per CalibrationParameter complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="CalibrationParameter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="M" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="N" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="threshold" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalibrationParameter", propOrder = {

})
public class CalibrationParameter {

    @XmlElement(name = "M")
    protected double m;
    @XmlElement(name = "N")
    protected double n;
    protected Double threshold;

    /**
     * Recupera il valore della propriet� m.
     * 
     */
    public double getM() {
        return m;
    }

    /**
     * Imposta il valore della propriet� m.
     * 
     */
    public void setM(double value) {
        this.m = value;
    }

    /**
     * Recupera il valore della propriet� n.
     * 
     */
    public double getN() {
        return n;
    }

    /**
     * Imposta il valore della propriet� n.
     * 
     */
    public void setN(double value) {
        this.n = value;
    }

    /**
     * Recupera il valore della propriet� threshold.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getThreshold() {
        return threshold;
    }

    /**
     * Imposta il valore della propriet� threshold.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setThreshold(Double value) {
        this.threshold = value;
    }

}
