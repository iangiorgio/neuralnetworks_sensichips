//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SENSING_ELEMENT" type="{}SensingElement" maxOccurs="unbounded"/>
 *         &lt;element name="FAMILY" type="{}Family" maxOccurs="unbounded"/>
 *         &lt;element name="CLUSTER" type="{}Cluster" maxOccurs="unbounded"/>
 *         &lt;element name="CONFIGURATION" type="{}Configuration" maxOccurs="unbounded"/>
 *         &lt;element name="INSTALLED_CHIPS" type="{}InstalledChips" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sensingelement",
    "family",
    "cluster",
    "configuration",
    "installedchips"
})
@XmlRootElement(name = "LAYOUT_CONFIGURATION")
public class LAYOUTCONFIGURATION {

    @XmlElement(name = "SENSING_ELEMENT", required = true)
    protected List<SensingElement> sensingelement;
    @XmlElement(name = "FAMILY", required = true)
    protected List<Family> family;
    @XmlElement(name = "CLUSTER", required = true)
    protected List<Cluster> cluster;
    @XmlElement(name = "CONFIGURATION", required = true)
    protected List<Configuration> configuration;
    @XmlElement(name = "INSTALLED_CHIPS")
    protected InstalledChips installedchips;

    /**
     * Gets the value of the sensingelement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sensingelement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSENSINGELEMENT().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SensingElement }
     * 
     * 
     */
    public List<SensingElement> getSENSINGELEMENT() {
        if (sensingelement == null) {
            sensingelement = new ArrayList<SensingElement>();
        }
        return this.sensingelement;
    }

    /**
     * Gets the value of the family property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the family property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFAMILY().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Family }
     * 
     * 
     */
    public List<Family> getFAMILY() {
        if (family == null) {
            family = new ArrayList<Family>();
        }
        return this.family;
    }

    /**
     * Gets the value of the cluster property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cluster property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCLUSTER().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cluster }
     * 
     * 
     */
    public List<Cluster> getCLUSTER() {
        if (cluster == null) {
            cluster = new ArrayList<Cluster>();
        }
        return this.cluster;
    }

    /**
     * Gets the value of the configuration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the configuration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCONFIGURATION().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Configuration }
     * 
     * 
     */
    public List<Configuration> getCONFIGURATION() {
        if (configuration == null) {
            configuration = new ArrayList<Configuration>();
        }
        return this.configuration;
    }

    /**
     * Recupera il valore della propriet� installedchips.
     * 
     * @return
     *     possible object is
     *     {@link InstalledChips }
     *     
     */
    public InstalledChips getINSTALLEDCHIPS() {
        return installedchips;
    }

    /**
     * Imposta il valore della propriet� installedchips.
     * 
     * @param value
     *     allowed object is
     *     {@link InstalledChips }
     *     
     */
    public void setINSTALLEDCHIPS(InstalledChips value) {
        this.installedchips = value;
    }

}
