//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SensingElement complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SensingElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InPortADC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NData" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MeasureTechnique" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Filter" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RANGE_MIN" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="RANGE_MAX" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="DEFAULT_ALARM_THRESHOLD" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="MULTIPLIER" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="MEASURE_UNIT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Rsense" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InGain" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OutGain" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Contacts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Frequency" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="Harmonic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DCBiasN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="-32"/>
 *               &lt;maxInclusive value="31"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DCBiasP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="-2048"/>
 *               &lt;maxInclusive value="2047"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ModeVI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MeasureType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhaseShiftMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhaseShift" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SensingElement", propOrder = {

})
public class SensingElement {

    @XmlElement(name = "NAME", required = true)
    protected String name;
    @XmlElement(name = "ConversionRate")
    protected int conversionRate;
    @XmlElement(name = "InPortADC", required = true)
    protected String inPortADC;
    @XmlElement(name = "NData")
    protected int nData;
    @XmlElement(name = "MeasureTechnique", required = true)
    protected String measureTechnique;
    @XmlElement(name = "Filter")
    protected int filter;
    @XmlElement(name = "RANGE_MIN")
    protected double rangemin;
    @XmlElement(name = "RANGE_MAX")
    protected double rangemax;
    @XmlElement(name = "DEFAULT_ALARM_THRESHOLD")
    protected double defaultalarmthreshold;
    @XmlElement(name = "MULTIPLIER")
    protected double multiplier;
    @XmlElement(name = "MEASURE_UNIT", required = true)
    protected String measureunit;
    @XmlElement(name = "Rsense")
    protected Integer rsense;
    @XmlElement(name = "InGain")
    protected Integer inGain;
    @XmlElement(name = "OutGain")
    protected Integer outGain;
    @XmlElement(name = "Contacts")
    protected String contacts;
    @XmlElement(name = "Frequency")
    protected Float frequency;
    @XmlElement(name = "Harmonic")
    protected String harmonic;
    @XmlElement(name = "DCBiasN")
    protected Integer dcBiasN;
    @XmlElement(name = "DCBiasP")
    protected Integer dcBiasP;
    @XmlElement(name = "ModeVI")
    protected String modeVI;
    @XmlElement(name = "MeasureType")
    protected String measureType;
    @XmlElement(name = "PhaseShiftMode")
    protected String phaseShiftMode;
    @XmlElement(name = "PhaseShift")
    protected Integer phaseShift;
    @XmlElement(name = "IQ")
    protected String iq;
    @XmlElement(name = "SequentialMode")
    protected String SequentialMode;


    /**
     * Recupera il valore della propriet� SequentialMode.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSequentialMode() {
        return SequentialMode;
    }

    /**
     * Imposta il valore della propriet� SequentialMode.
     *
     */
    public void setSequentialMode(String value) {
        this.SequentialMode = value;
    }

    /**
     * Recupera il valore della propriet� name.
     *     
     */
    public String getNAME() {
        return name;
    }

    /**
     * Imposta il valore della propriet� name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAME(String value) {
        this.name = value;
    }

    /**
     * Recupera il valore della propriet� conversionRate.
     * 
     */
    public int getConversionRate() {
        return conversionRate;
    }

    /**
     * Imposta il valore della propriet� conversionRate.
     * 
     */
    public void setConversionRate(int value) {
        this.conversionRate = value;
    }

    /**
     * Recupera il valore della propriet� inPortADC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInPortADC() {
        return inPortADC;
    }

    /**
     * Imposta il valore della propriet� inPortADC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInPortADC(String value) {
        this.inPortADC = value;
    }

    /**
     * Recupera il valore della propriet� nData.
     * 
     */
    public int getNData() {
        return nData;
    }

    /**
     * Imposta il valore della propriet� nData.
     * 
     */
    public void setNData(int value) {
        this.nData = value;
    }

    /**
     * Recupera il valore della propriet� measureTechnique.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasureTechnique() {
        return measureTechnique;
    }

    /**
     * Imposta il valore della propriet� measureTechnique.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasureTechnique(String value) {
        this.measureTechnique = value;
    }

    /**
     * Recupera il valore della propriet� filter.
     * 
     */
    public int getFilter() {
        return filter;
    }

    /**
     * Imposta il valore della propriet� filter.
     * 
     */
    public void setFilter(int value) {
        this.filter = value;
    }

    /**
     * Recupera il valore della propriet� rangemin.
     * 
     */
    public double getRANGEMIN() {
        return rangemin;
    }

    /**
     * Imposta il valore della propriet� rangemin.
     * 
     */
    public void setRANGEMIN(double value) {
        this.rangemin = value;
    }

    /**
     * Recupera il valore della propriet� rangemax.
     * 
     */
    public double getRANGEMAX() {
        return rangemax;
    }

    /**
     * Imposta il valore della propriet� rangemax.
     * 
     */
    public void setRANGEMAX(double value) {
        this.rangemax = value;
    }

    /**
     * Recupera il valore della propriet� defaultalarmthreshold.
     * 
     */
    public double getDEFAULTALARMTHRESHOLD() {
        return defaultalarmthreshold;
    }

    /**
     * Imposta il valore della propriet� defaultalarmthreshold.
     * 
     */
    public void setDEFAULTALARMTHRESHOLD(double value) {
        this.defaultalarmthreshold = value;
    }

    /**
     * Recupera il valore della propriet� multiplier.
     * 
     */
    public double getMULTIPLIER() {
        return multiplier;
    }

    /**
     * Imposta il valore della propriet� multiplier.
     * 
     */
    public void setMULTIPLIER(double value) {
        this.multiplier = value;
    }

    /**
     * Recupera il valore della propriet� measureunit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMEASUREUNIT() {
        return measureunit;
    }

    /**
     * Imposta il valore della propriet� measureunit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMEASUREUNIT(String value) {
        this.measureunit = value;
    }

    /**
     * Recupera il valore della propriet� rsense.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRsense() {
        return rsense;
    }

    /**
     * Imposta il valore della propriet� rsense.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRsense(Integer value) {
        this.rsense = value;
    }

    /**
     * Recupera il valore della propriet� inGain.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInGain() {
        return inGain;
    }

    /**
     * Imposta il valore della propriet� inGain.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInGain(Integer value) {
        this.inGain = value;
    }

    /**
     * Recupera il valore della propriet� outGain.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutGain() {
        return outGain;
    }

    /**
     * Imposta il valore della propriet� outGain.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutGain(Integer value) {
        this.outGain = value;
    }

    /**
     * Recupera il valore della propriet� contacts.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContacts() {
        return contacts;
    }

    /**
     * Imposta il valore della propriet� contacts.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContacts(String value) {
        this.contacts = value;
    }

    /**
     * Recupera il valore della propriet� frequency.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getFrequency() {
        return frequency;
    }

    /**
     * Imposta il valore della propriet� frequency.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFrequency(Float value) {
        this.frequency = value;
    }

    /**
     * Recupera il valore della propriet� harmonic.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHarmonic() {
        return harmonic;
    }

    /**
     * Imposta il valore della propriet� harmonic.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHarmonic(String value) {
        this.harmonic = value;
    }

    /**
     * Recupera il valore della propriet� dcBiasN.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDCBiasN() {
        return dcBiasN;
    }

    /**
     * Imposta il valore della propriet� dcBiasN.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDCBiasN(Integer value) {
        this.dcBiasN = value;
    }

    /**
     * Recupera il valore della propriet� dcBiasP.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDCBiasP() {
        return dcBiasP;
    }

    /**
     * Imposta il valore della propriet� dcBiasP.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDCBiasP(Integer value) {
        this.dcBiasP = value;
    }

    /**
     * Recupera il valore della propriet� modeVI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeVI() {
        return modeVI;
    }

    /**
     * Imposta il valore della propriet� modeVI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeVI(String value) {
        this.modeVI = value;
    }

    /**
     * Recupera il valore della propriet� measureType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasureType() {
        return measureType;
    }

    /**
     * Imposta il valore della propriet� measureType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasureType(String value) {
        this.measureType = value;
    }

    /**
     * Recupera il valore della propriet� phaseShiftMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhaseShiftMode() {
        return phaseShiftMode;
    }

    /**
     * Imposta il valore della propriet� phaseShiftMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhaseShiftMode(String value) {
        this.phaseShiftMode = value;
    }

    /**
     * Recupera il valore della propriet� phaseShift.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPhaseShift() {
        return phaseShift;
    }

    /**
     * Imposta il valore della propriet� phaseShift.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPhaseShift(Integer value) {
        this.phaseShift = value;
    }

    /**
     * Recupera il valore della propriet� iq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIQ() {
        return iq;
    }

    /**
     * Imposta il valore della propriet� iq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIQ(String value) {
        this.iq = value;
    }

}
