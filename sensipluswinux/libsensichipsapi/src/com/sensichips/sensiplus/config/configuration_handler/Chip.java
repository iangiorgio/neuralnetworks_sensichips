//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Chip complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Chip">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="spFamily" type="{}Family" minOccurs="0"/>
 *         &lt;element name="FAMILY_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SERIAL_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="I2C_ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OSC_TRIM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SENSING_ELEMENT_ONCHIP" type="{}SensingElementOnChip" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Chip", propOrder = {
    "spFamily",
    "familyid",
    "serialnumber",
    "i2CADDRESS",
    "osctrim",
    "sensingelementonchip"
})
public class Chip {

    protected Family spFamily;
    @XmlElement(name = "FAMILY_ID", required = true)
    protected String familyid;
    @XmlElement(name = "SERIAL_NUMBER", required = true)
    protected String serialnumber;
    @XmlElement(name = "I2C_ADDRESS", required = true)
    protected String i2CADDRESS;
    @XmlElement(name = "OSC_TRIM")
    protected String osctrim;

    //Aggiunto 26/05/2019 Luca
    public void setSensingelementonchip(List<SensingElementOnChip> sensingelementonchip) {
        this.sensingelementonchip = sensingelementonchip;
    }

    @XmlElement(name = "SENSING_ELEMENT_ONCHIP", required = true)
    protected List<SensingElementOnChip> sensingelementonchip;

    /**
     * Recupera il valore della propriet� spFamily.
     * 
     * @return
     *     possible object is
     *     {@link Family }
     *     
     */
    public Family getSpFamily() {
        return spFamily;
    }

    /**
     * Imposta il valore della propriet� spFamily.
     * 
     * @param value
     *     allowed object is
     *     {@link Family }
     *     
     */
    public void setSpFamily(Family value) {
        this.spFamily = value;
    }

    /**
     * Recupera il valore della propriet� familyid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFAMILYID() {
        return familyid;
    }

    /**
     * Imposta il valore della propriet� familyid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFAMILYID(String value) {
        this.familyid = value;
    }

    /**
     * Recupera il valore della propriet� serialnumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERIALNUMBER() {
        return serialnumber;
    }

    /**
     * Imposta il valore della propriet� serialnumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERIALNUMBER(String value) {
        this.serialnumber = value;
    }

    /**
     * Recupera il valore della propriet� i2CADDRESS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getI2CADDRESS() {
        return i2CADDRESS;
    }

    /**
     * Imposta il valore della propriet� i2CADDRESS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setI2CADDRESS(String value) {
        this.i2CADDRESS = value;
    }

    /**
     * Recupera il valore della propriet� osctrim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOSCTRIM() {
        return osctrim;
    }

    /**
     * Imposta il valore della propriet� osctrim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOSCTRIM(String value) {
        this.osctrim = value;
    }

    /**
     * Gets the value of the sensingelementonchip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sensingelementonchip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSENSINGELEMENTONCHIP().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SensingElementOnChip }
     * 
     * 
     */
    public List<SensingElementOnChip> getSENSINGELEMENTONCHIP() {
        if (sensingelementonchip == null) {
            sensingelementonchip = new ArrayList<SensingElementOnChip>();
        }
        return this.sensingelementonchip;
    }

}
