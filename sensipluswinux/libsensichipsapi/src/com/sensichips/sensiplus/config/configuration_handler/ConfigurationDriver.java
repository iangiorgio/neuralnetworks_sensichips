//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ConfigurationDriver complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ConfigurationDriver">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DRIVER_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PARAM1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PARAM2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfigurationDriver", propOrder = {
    "drivername",
    "param1",
    "param2"
})
public class ConfigurationDriver {

    @XmlElement(name = "DRIVER_NAME", required = true)
    protected String drivername;
    @XmlElement(name = "PARAM1", required = true)
    protected String param1;
    @XmlElement(name = "PARAM2", required = true)
    protected String param2;

    /**
     * Recupera il valore della propriet� drivername.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRIVERNAME() {
        return drivername;
    }

    /**
     * Imposta il valore della propriet� drivername.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRIVERNAME(String value) {
        this.drivername = value;
    }

    /**
     * Recupera il valore della propriet� param1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARAM1() {
        return param1;
    }

    /**
     * Imposta il valore della propriet� param1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARAM1(String value) {
        this.param1 = value;
    }

    /**
     * Recupera il valore della propriet� param2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARAM2() {
        return param2;
    }

    /**
     * Imposta il valore della propriet� param2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARAM2(String value) {
        this.param2 = value;
    }

}
