//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Family complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Family">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FAMILY_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FAMILY_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HW_VERSION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SYS_CLOCK" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OSC_TRIM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MEASURE_TYPE" maxOccurs="4">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="EIS"/>
 *               &lt;enumeration value="POT"/>
 *               &lt;enumeration value="ULTRASOUND"/>
 *               &lt;enumeration value="ENERGY_SPECTROSCOPY"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MULTICAST_DEFAULT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BROADCAST" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BROADCAST_SLOW" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SENSING_ELEMENT_ONFAMILY" type="{}SensingElementOnFamily" maxOccurs="unbounded"/>
 *         &lt;element name="ANALYTE" type="{}Analyte" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Family", propOrder = {
    "familyname",
    "familyid",
    "hwversion",
    "sysclock",
    "osctrim",
    "measuretype",
    "multicastdefault",
    "broadcast",
    "broadcastslow",
    "sensingelementonfamily",
    "analyte"
})
public class Family {

    @XmlElement(name = "FAMILY_NAME", required = true)
    protected String familyname;
    @XmlElement(name = "FAMILY_ID", required = true)
    protected String familyid;
    @XmlElement(name = "HW_VERSION", required = true)
    protected String hwversion;
    @XmlElement(name = "SYS_CLOCK")
    protected int sysclock;
    @XmlElement(name = "OSC_TRIM", required = true)
    protected String osctrim;
    @XmlElement(name = "MEASURE_TYPE", required = true)
    protected List<String> measuretype;
    @XmlElement(name = "MULTICAST_DEFAULT", required = true)
    protected String multicastdefault;
    @XmlElement(name = "BROADCAST", required = true)
    protected String broadcast;
    @XmlElement(name = "BROADCAST_SLOW", required = true)
    protected String broadcastslow;
    @XmlElement(name = "SENSING_ELEMENT_ONFAMILY", required = true)
    protected List<SensingElementOnFamily> sensingelementonfamily;
    @XmlElement(name = "ANALYTE", required = true)
    protected List<Analyte> analyte;

    //Aggiunto 26/05/2019 Luca
    public void setMeasuretype(List<String> measuretype) {
        this.measuretype = measuretype;
    }

    //Aggiunto 26/05/2019 Luca
    public void setSensingelementonfamily(List<SensingElementOnFamily> sensingelementonfamily) {
        this.sensingelementonfamily = sensingelementonfamily;
    }


    /**
     * Recupera il valore della propriet� familyname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFAMILYNAME() {
        return familyname;
    }

    /**
     * Imposta il valore della propriet� familyname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFAMILYNAME(String value) {
        this.familyname = value;
    }

    /**
     * Recupera il valore della propriet� familyid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFAMILYID() {
        return familyid;
    }

    /**
     * Imposta il valore della propriet� familyid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFAMILYID(String value) {
        this.familyid = value;
    }

    /**
     * Recupera il valore della propriet� hwversion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHWVERSION() {
        return hwversion;
    }

    /**
     * Imposta il valore della propriet� hwversion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHWVERSION(String value) {
        this.hwversion = value;
    }

    /**
     * Recupera il valore della propriet� sysclock.
     * 
     */
    public int getSYSCLOCK() {
        return sysclock;
    }

    /**
     * Imposta il valore della propriet� sysclock.
     * 
     */
    public void setSYSCLOCK(int value) {
        this.sysclock = value;
    }

    /**
     * Recupera il valore della propriet� osctrim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOSCTRIM() {
        return osctrim;
    }

    /**
     * Imposta il valore della propriet� osctrim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOSCTRIM(String value) {
        this.osctrim = value;
    }

    /**
     * Gets the value of the measuretype property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the measuretype property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMEASURETYPE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMEASURETYPE() {
        if (measuretype == null) {
            measuretype = new ArrayList<String>();
        }
        return this.measuretype;
    }

    /**
     * Recupera il valore della propriet� multicastdefault.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMULTICASTDEFAULT() {
        return multicastdefault;
    }

    /**
     * Imposta il valore della propriet� multicastdefault.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMULTICASTDEFAULT(String value) {
        this.multicastdefault = value;
    }

    /**
     * Recupera il valore della propriet� broadcast.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBROADCAST() {
        return broadcast;
    }

    /**
     * Imposta il valore della propriet� broadcast.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBROADCAST(String value) {
        this.broadcast = value;
    }

    /**
     * Recupera il valore della propriet� broadcastslow.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBROADCASTSLOW() {
        return broadcastslow;
    }

    /**
     * Imposta il valore della propriet� broadcastslow.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBROADCASTSLOW(String value) {
        this.broadcastslow = value;
    }

    /**
     * Gets the value of the sensingelementonfamily property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sensingelementonfamily property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSENSINGELEMENTONFAMILY().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SensingElementOnFamily }
     * 
     * 
     */
    public List<SensingElementOnFamily> getSENSINGELEMENTONFAMILY() {
        if (sensingelementonfamily == null) {
            sensingelementonfamily = new ArrayList<SensingElementOnFamily>();
        }
        return this.sensingelementonfamily;
    }

    /**
     * Gets the value of the analyte property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the analyte property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getANALYTE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Analyte }
     * 
     * 
     */
    public List<Analyte> getANALYTE() {
        if (analyte == null) {
            analyte = new ArrayList<Analyte>();
        }
        return this.analyte;
    }

}
