package com.sensichips.sensiplus.config;


import com.sensichips.sensiplus.config.configuration_handler.InstalledChips;
import com.sensichips.sensiplus.config.configuration_handler.LAYOUTCONFIGURATION;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class TestLayoutConfigurationHandler {

    public static void main(String[] args) {
        //System.out.println("Hello World!");
        try {
            /*SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File("layout_configuration_files/schema_layout_configuration_new.xsd"));
            JAXBContext context = JAXBContext.newInstance(LAYOUTCONFIGURATION.class);
            
            Unmarshaller unmarshaller = context.createUnmarshaller();
            unmarshaller.setSchema(schema);
            //unmarshaller.setEventHandler(new MyValidationEventHandler());
            LAYOUTCONFIGURATION layout = (LAYOUTCONFIGURATION) unmarshaller.unmarshal(new File("layout_configuration_files/layout_configuration_new.xml"));

            System.out.println("DONE");


            File file = new File("layout_configuration_files/prova.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(LAYOUTCONFIGURATION.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            if(layout.getINSTALLEDCHIPS() == null){
                layout.setINSTALLEDCHIPS(new InstalledChips());
            }
            layout.getINSTALLEDCHIPS().getCHIP().add(layout.getCLUSTER().get(0).getCHIP().get(0));
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(layout, file);
            jaxbMarshaller.marshal(layout, System.out);*/


            //File file = new File("layout_configuration_files/schema_layout_configuration_new.xsd");
            SPConfigurationManager.loadConfigurationFromFileWithJAXB(new File("layout_configuration_files/layout_configuration_new.xml"),new File("layout_configuration_files/schema_layout_configuration_new.xsd"));


        }catch (Exception e){
            e.printStackTrace();
}
    }
}
