package com.sensichips.sensiplus.config;

/**
 * Created by Marco Ferdinandi on 30/01/2017.
 */

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.configuration_handler.*;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.*;


import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.InputStream;
import java.util.*;


public class SPConfigurationManager {

    public interface SPDriverGenerator {
        String generateDriver(String driverName, ArrayList<String> driverParameters) throws SPDriverException;
    }


    private static final String LOG_MESSAGE = "SPConfigurationManager";
    private static final int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L2;


    private static final String SENSICHIPS = "Sensichips";
    private static final String SENSING_ELEMENTS = "SENSING_ELEMENTS";
    private static final String SENSING_ELEMENT = "SENSING_ELEMENT";
    private static final String NAME = "NAME";
    private static final String Rsense = "Rsense";
    private static final String InGain = "InGain";
    private static final String OutGain = "OutGain";
    private static final String Contacts = "Contacts";
    private static final String Frequency = "Frequency";
    private static final String Harmonic = "Harmonic";
    private static final String DCBiasP = "DCBiasP";
    private static final String DCBiasN = "DCBiasN";
    private static final String ModeVI = "ModeVI";
    private static final String MeasureType = "MeasureType";
    private static final String Filter = "Filter";
    private static final String PhaseShiftMode = "PhaseShiftMode";
    private static final String PhaseShift = "PhaseShift";
    private static final String IQ = "IQ";
    private static final String SEQUENTIAL_MODE = "SequentialMode";
    private static final String ConversionRate = "ConversionRate";
    private static final String InPortADC = "InPortADC";
    private static final String NData = "NData";
    private static final String SENSING_ELEMENT_MEASURE_UNIT = "MEASURE_UNIT";
    private static final String SENSING_ELEMENT_MULTIPLIER = "MULTIPLIER";
    private static final String MeasureTechnique = "MeasureTechnique";
    private static final String FAMILY = "FAMILY";
    private static final String FAMILY_NAME = "FAMILY_NAME";
    private static final String FAMILY_ID = "FAMILY_ID";
    private static final String SENSING_ELEMENT_ID = "SENSING_ELEMENT_ID";
    private static final String SENSING_ELEMENT_NAME = "SENSING_ELEMENT_NAME";
    private static final String SENSING_ELEMENT_PORT = "SENSING_ELEMENT_PORT";
    private static final String SENSING_ELEMENT_ONFAMILY = "SENSING_ELEMENT_ONFAMILY";
    private static final String SENSING_ELEMENT_ONCHIP = "SENSING_ELEMENT_ONCHIP";
    private static final String OSC_TRIM_OVERRIDE = "OSC_TRIM_OVERRIDE";
    private static final String ANALYTE = "ANALYTE";
    private static final String ANALYTE_NAME = "ANALYTE_NAME";
    private static final String ANALYTE_MEASURE_UNIT = "ANALYTE_MEASURE_UNIT";
    private static final String CLUSTER = "CLUSTER";
    private static final String CLUSTER_ID = "CLUSTER_ID";
    private static final String CHIP = "CHIP";
    private static final String SERIAL_NUMBER = "SERIAL_NUMBER";
    private static final String I2C_ADDRESS = "I2C_ADDRESS";
    private static final String CALIBRATION_PARAMETER = "CALIBRATION_PARAMETER";
    private static final String M = "M";
    private static final String N = "N";
    //private static final String CONFIGURATIONS = "CONFIGURATIONS";
    private static final String CONFIGURATION = "CONFIGURATION";
    private static final String DRIVER = "DRIVER";
    private static final String HOST_CONTROLLER = "HOST_CONTROLLER";
    private static final String API_OWNER = "API_OWNER";
    private static final String MCU = "MCU";
    private static final String PROTOCOL = "PROTOCOL";
    private static final String ADDRESSING_TYPE = "ADDRESSING_TYPE";
    private static final String MULTICAST_CLUSTER = "MULTICAST_CLUSTER";
    private static final String MULTICAST_DEFAULT = "MULTICAST_DEFAULT";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String MEASURE_TYPE = "MEASURE_TYPE";
    private static final String BROADCAST = "BROADCAST";
    private static final String BROADCAST_SLOW = "BROADCAST_SLOW";

    private static final String VCC = "VCC";
    private static final String HW_VERSION = "HW_VERSION";
    private static final String SYS_CLOCK = "SYS_CLOCK";
    private static final String OSC_TRIM = "OSC_TRIM";


    private static final String RANGE_MIN = "RANGE_MIN";
    private static final String RANGE_MAX = "RANGE_MAX";
    private static final String DEFAULT_ALARM_THRESHOLD = "DEFAULT_ALARM_THRESHOLD";


    private static SPConfiguration defaultConfiguration = null;
        private static Hashtable<String, SPConfiguration> configurationsAvailable = new Hashtable<>();


    private static List<SPCluster> clusterList = new ArrayList<SPCluster>();
    public static LAYOUTCONFIGURATION layoutConfiguration; //messo public il 17/05/2019 Luca




    public static int getListSPConfigurationSize() throws SPException {
        if (configurationsAvailable == null) {
            throw new SPException("Any SPConfiguration loaded. Please verifiy your layoutConfiguration.xml file!");
        }
        return configurationsAvailable.size();
    }

    public static void addSPConfiguration(SPConfiguration spConfiguration){
        configurationsAvailable.put(spConfiguration.getConfigurationName(), spConfiguration);
    }


    public static ArrayList<SPConfiguration> getListSPConfiguration() throws SPException {
        if (configurationsAvailable == null) {
            throw new SPException("Any SPConfiguration loaded. Please verifiy your layoutConfiguration.xml file!");
        }
        Collection<SPConfiguration> coll = configurationsAvailable.values();
        int count = 0;
        ArrayList<SPConfiguration> list = new ArrayList<>();

        Iterator<SPConfiguration> iterator = coll.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }

    public static SPConfiguration getSPConfiguration(String name) throws SPException {
        SPConfiguration out = null;
        if(name!=null)
            out = configurationsAvailable.get(name);

        if (out == null) {
            ArrayList<String> listaConfiguration = SPConfigurationManager.getConfigurationNames();
            String configurationNames = "";
            for (String element : listaConfiguration) {
                configurationNames += element + "\n";
            }
            throw new SPException("\nConfiguration: " + name + " does not exist!\nAvailable configurations are:\n" + configurationNames);
        }
        return out;
    }


    public static ArrayList<String> getConfigurationNames() {
        Enumeration keys = configurationsAvailable.keys();
        ArrayList<String> out = new ArrayList<>();
        while (keys.hasMoreElements()) {
            out.add((String) keys.nextElement());
        }
        return out;
    }


    public static void updateLayoutConfigurationAfterReading(LAYOUTCONFIGURATION layoutconfiguration) throws SPException{

        /*FOR EACH CONFIGURATION UPDATE THE POINTER TO THE CLUSTER*/
        for(int i=0; i<layoutconfiguration.getCONFIGURATION().size(); i++){
            Configuration conf = layoutconfiguration.getCONFIGURATION().get(i);
            String currentClusterID = conf.getCLUSTERID();
            int j=0;
            while(j<layoutconfiguration.getCLUSTER().size() && !layoutconfiguration.getCLUSTER().get(j).getCLUSTERID().equals(currentClusterID))
                j++;
            if(j==layoutconfiguration.getCLUSTER().size()){
                throw new SPException("Cluster ID "+currentClusterID+", indicated in "+conf.getDESCRIPTION()+", not present in the layout configuration file");

            }
            conf.setCluster(layoutconfiguration.getCLUSTER().get(j));
        }

        /**/
        for(int i=0; i<layoutconfiguration.getCLUSTER().size(); i++){
            Cluster cluster = layoutconfiguration.getCLUSTER().get(i);
            String familyID = null;
            Family currentFamily = null;
            for(int j=0; j<cluster.getCHIP().size(); j++) {
                Chip chip = cluster.getCHIP().get(j);
                if (familyID == null) {
                    familyID = chip.getFAMILYID();

                    int k = 0;
                    while (k < layoutconfiguration.getFAMILY().size() && !layoutconfiguration.getFAMILY().get(k).getFAMILYID().equals(familyID))
                        k++;
                    if (k == layoutconfiguration.getFAMILY().size())
                        throw new SPException("Invalid Family ID " + familyID + " indicated in chip: " + chip.getSERIALNUMBER());
                    currentFamily = layoutconfiguration.getFAMILY().get(k);
                    chip.setSpFamily(currentFamily);
                }

                else{

                    if(!familyID.equals(chip.getFAMILYID()))
                        throw new SPException("All chip in a cluster must belong to the same family");
                    if(currentFamily==null)
                        throw new SPException("Family not found");

                    chip.setSpFamily(currentFamily);

                }


                for(int h=0; h<chip.getSENSINGELEMENTONCHIP().size(); h++){
                    SensingElementOnChip sensingElementOnChip = chip.getSENSINGELEMENTONCHIP().get(h);
                    String sensingElementID = sensingElementOnChip.getSENSINGELEMENTID();

                    int k=0;
                    while (k < chip.getSpFamily().getSENSINGELEMENTONFAMILY().size() && !chip.getSpFamily().getSENSINGELEMENTONFAMILY().get(k).getSENSINGELEMENTID().equals(sensingElementID))
                        k++;
                    if (k == chip.getSpFamily().getSENSINGELEMENTONFAMILY().size())
                        throw new SPException("Invalid SENSING ELEMENT ID " + sensingElementID + " indicated in chip: " + chip.getSERIALNUMBER());

                    sensingElementOnChip.setSensingElementOnFamily(chip.getSpFamily().getSENSINGELEMENTONFAMILY().get(k));

                }
            }
        }


        for(int i=0; i<layoutconfiguration.getFAMILY().size(); i++){
            Family family = layoutconfiguration.getFAMILY().get(i);
            for(int j=0; j<family.getSENSINGELEMENTONFAMILY().size(); j++) {
                String sensingElementName = family.getSENSINGELEMENTONFAMILY().get(j).getSENSINGELEMENTNAME();

                int k=0;
                while(k<layoutconfiguration.getSENSINGELEMENT().size() && !layoutconfiguration.getSENSINGELEMENT().get(k).getNAME().equals(sensingElementName))
                    k++;
                if(k==layoutconfiguration.getSENSINGELEMENT().size())
                    throw new SPException("Invalid SENSING ELEMENT NAME " + sensingElementName + " indicated in family: " + family.getFAMILYID());
                family.getSENSINGELEMENTONFAMILY().get(j).setSensingElement(layoutconfiguration.getSENSINGELEMENT().get(k));
            }
        }





    }

    public static void getConfigurationAvailable(LAYOUTCONFIGURATION layoutconfiguration){


    }


    public static void updateConfigurationFileWithJAXV(SPConfiguration spConfiguration, File fileXML,File fileXSD) throws SPException{
        SPLayoutConfigurationHandler.generateAndStoreInstalledChips(spConfiguration.getSpInstalledChips(), spConfiguration.getCluster(),layoutConfiguration,fileXML,fileXSD  );
    }


    public static void loadConfigurationFromFileWithJAXB(File fileXML, File fileXSD) throws SPException
    {
        try {
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            //Schema schema = sf.newSchema(new File("layout_configuration_files/schema_layout_configuration_new.xsd"));
            Schema schema = sf.newSchema(fileXSD);

            JAXBContext context = JAXBContext.newInstance(LAYOUTCONFIGURATION.class);

            Unmarshaller unmarshaller = context.createUnmarshaller();
            unmarshaller.setSchema(schema);
            //unmarshaller.setEventHandler(new MyValidationEventHandler());
            //layoutConfiguration = (LAYOUTCONFIGURATION) unmarshaller.unmarshal(new File("layout_configuration_files/layout_configuration_new.xml"));
            layoutConfiguration = (LAYOUTCONFIGURATION) unmarshaller.unmarshal(fileXML);

            updateLayoutConfigurationAfterReading(layoutConfiguration);

            SPDriverManager.resetSPDriverManager();

            configurationsAvailable = SPLayoutConfigurationHandler.generateAvailableConfiguration(layoutConfiguration);

            //System.out.println("DONE");
        }catch (Exception e){
            throw new SPException(e.getMessage());
        }
    }

   /* public static void generateSPConfigurations(LAYOUTCONFIGURATION layoutconfiguration) throws SPException{
        configurationsAvailable = new Hashtable<>();
        for(int i=0; i<layoutconfiguration.getCONFIGURATION().size(); i++){
            Configuration layoutConf = layoutconfiguration.getCONFIGURATION().get(i);
            SPConfiguration currentConfiguration = new SPConfiguration();
            //currentConfiguration.setCluster(layoutConf.getCluster());
        }


    }*/

    public static void loadConfigurationsFromFile(InputStream file) throws SPException {

        SPDriverManager.resetSPDriverManager();


        //defaultConfiguration = 0;
        Hashtable<String, SPSensingElement> sensingElementHashtable = new Hashtable<String, SPSensingElement>();
        Hashtable<String, SPFamily> familyHashtable = new Hashtable<String, SPFamily>();
        String mainCurrentTag = "";
        String currentTag = null;
        int lineCounter = 0;
        configurationsAvailable = new Hashtable<>();

        try {
            //FileReader in = new FileReader(file);
            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser myParser = xmlFactoryObject.newPullParser();
            myParser.setInput(file, null);
            currentTag = null;
            mainCurrentTag = "";
            SPSensingElement currentSPSensingElement = null;
            SPFamily currentFamily = null;
            SPAnalyte currentAnalyte = null;
            SPSensingElementOnFamily currentSPsensingElementOnFamily = null;
            SPSensingElementOnChip currentSPsensingElementOnChip = null;
            SPCluster currentCluster = null;
            SPChip currentChip = null;
            SPConfiguration currentConfiguration = null;

            boolean flag_sensingElementOnFamily = false;
            boolean flag_sensingElements = false;
            boolean flag_sensingElementOnChip = false;
            int event = myParser.getEventType();

            SPCalibrationParameters currentCalibrationParameters = null;

            String name, value;
            while (event != XmlPullParser.END_DOCUMENT) {
                name = myParser.getName();
                value = myParser.getText();

                if (name != null) {
                    currentTag = name;
                    //    System.out.println("Current tag: " + name );
                }

                if (value != null) {
                    //    System.out.println("Current value: " + value);
                }

                //START_TAG
                if (event == XmlPullParser.START_TAG && !name.equals(SENSICHIPS)) {
                    if (currentTag.equals(SENSING_ELEMENTS)) {
                        flag_sensingElements = true;
                    }
                    if (currentTag.equals(SENSING_ELEMENT) && flag_sensingElements) {
                        mainCurrentTag = currentTag;
                        currentSPSensingElement = new SPSensingElement();
                    } else if (currentTag.equals(FAMILY)) {
                        mainCurrentTag = currentTag;
                        currentFamily = new SPFamily();
                    } else if (currentTag.equals(ANALYTE)) {
                        currentAnalyte = new SPAnalyte();
                    } else if (currentTag.equals(CLUSTER)) {
                        mainCurrentTag = name;
                        currentCluster = new SPCluster();
                    } else if (currentTag.equals(CHIP)) {
                        mainCurrentTag = currentTag;
                        currentChip = new SPChip();
                        currentChip.setOscTrimOverride(currentFamily.getOSC_TRIM());
                    } else if (currentTag.equals(CALIBRATION_PARAMETER)) {
                        currentCalibrationParameters = new SPCalibrationParameters();
                    } else if (currentTag.equals(CONFIGURATION)) {
                        mainCurrentTag = currentTag;
                        currentConfiguration = new SPConfiguration();
                    } else if (currentTag.equals(DRIVER)) {

                        String driverParamName = "param";
                        int counter = 1;
                        String driverParamLoaded = myParser.getAttributeValue(null, driverParamName + counter++);
                        while (driverParamLoaded != null) {
                            currentConfiguration.addDriverParam(driverParamLoaded);
                            driverParamLoaded = myParser.getAttributeValue(null, driverParamName + counter++);
                        }


                    }
                    if (mainCurrentTag.equals(CHIP)) {
                        if (currentTag.equals(SENSING_ELEMENT_ONCHIP)) {
                            flag_sensingElementOnChip = true;
                            currentSPsensingElementOnChip = new SPSensingElementOnChip();
                        }
                    }
                    if (mainCurrentTag.equals(FAMILY)) {
                        if (currentTag.equals(SENSING_ELEMENT_ONFAMILY)) {
                            flag_sensingElementOnFamily = true;
                            currentSPsensingElementOnFamily = new SPSensingElementOnFamily();
                        }
                    }
                }

                //VALUE
                else if (event == XmlPullParser.TEXT && value != null && !value.contains("\n")) {
                    if (mainCurrentTag.equals(SENSING_ELEMENT)) {
                        if (currentTag.equals(NAME)) {
                            currentSPSensingElement.setName(value);
                        } else if (currentTag.equals(Rsense)) {
                            currentSPSensingElement.setRsense(value);
                        } else if (currentTag.equals(InGain)) {
                            currentSPSensingElement.setInGain(value);
                        } else if (currentTag.equals(OutGain)) {
                            currentSPSensingElement.setOutGain(value);
                        } else if (currentTag.equals(RANGE_MAX)) {
                            currentSPSensingElement.getSpDataRepresentationManager().setRangeMax(value);
                        } else if (currentTag.equals(RANGE_MIN)) {
                            currentSPSensingElement.getSpDataRepresentationManager().setRangeMin(value);
                        } else if (currentTag.equals(DEFAULT_ALARM_THRESHOLD)) {
                            currentSPSensingElement.getSpDataRepresentationManager().setDefault_alarm_threshold(value);
                        } else if (currentTag.equals(Contacts)) {
                            currentSPSensingElement.setContacts(value);
                        } else if (currentTag.equals(Frequency)) {
                            currentSPSensingElement.setFrequency(Float.parseFloat(value));
                        } else if (currentTag.equals(Harmonic)) {
                            currentSPSensingElement.setHarmonic(value);
                        } else if (currentTag.equals(DCBiasP)) {
                            currentSPSensingElement.setDCBiasP(Integer.parseInt(value));
                        } else if (currentTag.equals(DCBiasN)) {
                            currentSPSensingElement.setDCBiasN(Integer.parseInt(value));
                        } else if (currentTag.equals(ModeVI)) {
                            currentSPSensingElement.setModeVI(value);
                        } else if (currentTag.equals(MeasureTechnique)) {
                            currentSPSensingElement.setMeasureTechnique(value);
                        } else if (currentTag.equals(MeasureType)) {
                            currentSPSensingElement.setMeasureType(value);
                        } else if (currentTag.equals(Filter)) {
                            currentSPSensingElement.setFilter(value);
                        } else if (currentTag.equals(PhaseShiftMode)) {
                            currentSPSensingElement.setPhaseShiftMode(value);
                        } else if (currentTag.equals(PhaseShift)) {
                            currentSPSensingElement.setPhaseShift(Integer.parseInt(value));
                        } else if (currentTag.equals(IQ)) {
                            currentSPSensingElement.setIq(value);
                        } else if (currentTag.equals(SEQUENTIAL_MODE)) {
                            currentSPSensingElement.setSequentialMode(Integer.parseInt(value) == 1);
                        } else if (currentTag.equals(ConversionRate)) {
                            currentSPSensingElement.setConversionRate(Integer.parseInt(value));
                        } else if (currentTag.equals(InPortADC)) {
                            currentSPSensingElement.setInportADC(value);
                        } else if (currentTag.equals(NData)) {
                            currentSPSensingElement.setNData(Integer.parseInt(value));
                        } else if (currentTag.equals(SENSING_ELEMENT_MEASURE_UNIT)) {
                            currentSPSensingElement.getSpDataRepresentationManager().setMeasureUnit(value);
                        } else if (currentTag.equals(SENSING_ELEMENT_MULTIPLIER)) {
                            currentSPSensingElement.getSpDataRepresentationManager().setInitial_multiplier(value);
                        }
                    } else if (mainCurrentTag.equals(FAMILY)) {
                        if (currentTag.equals(FAMILY_NAME)) {
                            currentFamily.setName(value);
                        } else if (currentTag.equals(FAMILY_ID)) {
                            currentFamily.setId(value);
                        } else if (currentTag.equals(BROADCAST)) {
                            currentFamily.setBroadcastAddress(value);
                        } else if (currentTag.equals(BROADCAST_SLOW)) {
                            currentFamily.setBroadcastSlowAddress(value);
                        } else if (currentTag.equals(MULTICAST_DEFAULT)) {
                            currentFamily.setMulticastDefaultAddress(value);
                        } else if (currentTag.equals(SENSING_ELEMENT_ID) && flag_sensingElementOnFamily) {
                            currentSPsensingElementOnFamily.setID(value);
                        } else if (currentTag.equals(SENSING_ELEMENT_NAME) && flag_sensingElementOnFamily) {
                            currentSPsensingElementOnFamily.setSensingElement(sensingElementHashtable.get(value));
                        } else if (currentTag.equals(SENSING_ELEMENT_PORT) && flag_sensingElementOnFamily) {
                            currentSPsensingElementOnFamily.setPort(new SPPort(value));
                        } else if (currentTag.equals(ANALYTE_NAME)) {
                            currentAnalyte.setName(value);
                        } else if (currentTag.equals(ANALYTE_MEASURE_UNIT)) {
                            currentAnalyte.setMeasureUnit(value);
                        } else if (currentTag.equals(OSC_TRIM)) {
                            currentFamily.setOSC_TRIM(value);
                        } else if (currentTag.equals(SYS_CLOCK)) {
                            currentFamily.setSYS_CLOCK(Integer.parseInt(value));
                        } else if (currentTag.equals(HW_VERSION)) {
                            currentFamily.setHW_VERSION(value);
                        } else if (currentTag.equals(MEASURE_TYPE)) {
                            currentFamily.addMeasureType(value);
                        }


                    } else if (mainCurrentTag.equals(CLUSTER)) {
                        if (currentTag.equals(CLUSTER_ID)) {
                            currentCluster.setClusterID(value);
                            //System.out.println(value);
                        } else if (currentTag.equals(MULTICAST_CLUSTER)) {
                            currentCluster.setBroadcastAddressI2C("0x00");
                            currentCluster.setMulticastAddress(value);
                        }

                    } else if (mainCurrentTag.equals(CHIP)) {

                        if (currentTag.equals(OSC_TRIM_OVERRIDE)) {
                            currentChip.setOscTrimOverride(value);
                        } else if (currentTag.equals(FAMILY_ID)) {
                            currentChip.setSpFamily(familyHashtable.get(value));
                        } else if (currentTag.equals(SERIAL_NUMBER)) {
                            currentChip.setSerialNumber(value);
                        } else if (currentTag.equals(I2C_ADDRESS)) {
                            currentChip.setI2CAddress(value);
                        } else if (currentTag.equals(SENSING_ELEMENT_ID)) {
                            List<SPSensingElementOnFamily> list = currentChip.getSpFamily().getSensingElementOnFamilyList();
                            for (SPSensingElementOnFamily element : list) {
                                if (element.getID().equals(value)) {
                                    currentSPsensingElementOnChip.setSensingElementOnFamily(element);
                                    break;
                                }
                            }


                        } else if (currentTag.equals(M)) {
                            currentCalibrationParameters.setM(Double.parseDouble(value));
                        } else if (currentTag.equals(N)) {
                            currentCalibrationParameters.setN(Double.parseDouble(value));
                        }

                    } else if (mainCurrentTag.equals(CONFIGURATION)) {
                        if (currentTag.equals(VCC)) {
                            currentConfiguration.setVCC(value);
                        } else if (currentTag.equals(DRIVER)) {
                            currentConfiguration.setDriverName(value);
                        } else if (currentTag.equals(HOST_CONTROLLER)) {
                            currentConfiguration.setHostController(value);
                        } else if (currentTag.equals(API_OWNER)) {
                            currentConfiguration.setApiOwner(value);
                        } else if (currentTag.equals(MCU)) {
                            currentConfiguration.setMcu(value);
                        } else if (currentTag.equals(PROTOCOL)) {
                            currentConfiguration.setProtocol(value);
                        } else if (currentTag.equals(ADDRESSING_TYPE)) {
                            currentConfiguration.setAddressingType(value);
                        } else if (currentTag.equals(DESCRIPTION)) {
                            currentConfiguration.setDescription(value);
                        } else if (currentTag.equals(CLUSTER_ID)) {
                            for (SPCluster cluster : clusterList) {
                                if (cluster.getClusterID().equalsIgnoreCase(value)) {
                                    currentConfiguration.setCluster(cluster);
                                    break;
                                }
                            }
                        }
                    }
                }

                //END_TAG
                else if (event == XmlPullParser.END_TAG && !name.equals(SENSICHIPS)) {
                    if (currentTag.equals(SENSING_ELEMENT) && flag_sensingElements) {
                        sensingElementHashtable.put(currentSPSensingElement.getName(), currentSPSensingElement);
                        currentSPSensingElement = null;
                        mainCurrentTag = "";
                    } else if (currentTag.equals(SENSING_ELEMENTS)) {
                        flag_sensingElements = false;
                    } else if (currentTag.equals(SENSING_ELEMENT_ONFAMILY) && flag_sensingElementOnFamily) {
                        currentFamily.getSensingElementOnFamilyList().add(currentSPsensingElementOnFamily);
                        //sensingElementOnFamilyHashtable.put(currentSPsensingElementOnFamily.getSensingElement().getName(),currentSPsensingElementOnFamily);
                        currentSPsensingElementOnFamily = null;
                        flag_sensingElementOnFamily = false;
                    } else if (currentTag.equals(SENSING_ELEMENT_ONCHIP) && flag_sensingElementOnChip) {
                        currentChip.getSensingElementOnChipList().add(currentSPsensingElementOnChip);
                        currentSPsensingElementOnChip = null;
                        flag_sensingElementOnChip = false;
                    } else if (currentTag.equals(ANALYTE)) {
                        currentFamily.getAnalyteList().add(currentAnalyte);
                        currentAnalyte = null;
                    } else if (currentTag.equals(FAMILY)) {
                        familyHashtable.put("0X" + currentFamily.getId(), currentFamily);
                    } else if (currentTag.equals(CALIBRATION_PARAMETER)) {
                        currentSPsensingElementOnChip.setCalibrationParameters(currentCalibrationParameters);
                        currentCalibrationParameters = null;
                    } else if (currentTag.equals(CHIP)) {
                        SPFamily chipFamily = currentChip.getSpFamily();
                        SPFamily clusterFamily = currentCluster.getFamilyOfChips();
                        if (chipFamily != null) {
                            if (clusterFamily == null) {
                                currentCluster.setFamilyOfChips(chipFamily);
                            } else {
                                if (!clusterFamily.equals(chipFamily)) {
                                    throw new SPException("\nChip in a cluster must be of the same Family:\n" + clusterFamily + " != " + chipFamily + "\nfor chip " + currentChip.getSerialNumber() + " in cluster " + currentCluster.getClusterID());
                                }
                            }
                        }
                        currentCluster.add(currentChip);
                        SPLogger.getLogInterface().d(LOG_MESSAGE, "currentChip added", DEBUG_LEVEL);

                    } else if (currentTag.equals(CLUSTER)) {
                        SPLogger.getLogInterface().d(LOG_MESSAGE, "currentCluster added", DEBUG_LEVEL);
                        clusterList.add(currentCluster);
                        currentCluster = null;
                    } else if (currentTag.equals(CONFIGURATION)) {
                        //currentConfiguration.init(driverGenerator);
                        configurationsAvailable.put(currentConfiguration.getConfigurationName(), currentConfiguration);
                        currentConfiguration = null;
                        mainCurrentTag = "";
                    }
                }
                lineCounter++;
                event = myParser.next();
            }
            /*
            if (SPConfigurationManager.getListSPConfigurationSize() <= 0){
                throw new SPException("Any configuration defined. Please verify SPBatchParameters.layoutConfiguration");
            }*/

        } catch (Exception e) {
            e.printStackTrace();
            throw new SPException("Error on line: " + lineCounter + ", currentTag: " + currentTag + ", mainTag: " + mainCurrentTag + ".\n" + e.getMessage());
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Configuration loaded...", DEBUG_LEVEL);

    }

    public static  List<SPCluster> getSPClusterList(){
        return clusterList;
    }

    public static SPConfiguration getSPConfigurationDefault() throws SPException {
        if (defaultConfiguration == null) {
            throw new SPException("SPConfigurationManager: any default configuration defined!");
        }
        return defaultConfiguration;
    }

    /*
    public static void setDefaultSPConfiguration(String configurationName) throws SPException {
        SPConfiguration conf = getSPConfiguration(configurationName);

        if (defaultConfiguration != null && defaultConfiguration != conf) {
            // Reset defaultConfiguration
            if (defaultConfiguration.getSPProtocol().getSPDriver().equals(conf.getSPProtocol().getSPDriver())) {
                if (!defaultConfiguration.getSPProtocol().getProtocolName().equals(conf.getSPProtocol().getProtocolName())) {
                    if (!defaultConfiguration.getSPProtocol().isResettable()) {
                        throw new SPException("Change configurations from " + defaultConfiguration.getConfigurationName() + " to " + configurationName + "\n" +
                                " is not possible because protocol " + defaultConfiguration.getSPProtocol().getProtocolName() + " is not resettable!!\n" +
                                "Please verify your configuration file");
                    }
                    defaultConfiguration.getSPProtocol().reset();
                }
            }
        }

        // Va riavviato se default era null oppure se era diverso da ...
        conf.getSPProtocol().isInitialized()) {
            conf.getSPProtocol().init(conf.getAddressingType());
        }

        conf.setINIT();

        defaultConfiguration = conf;
    }*/





    public static void setDefaultSPConfiguration(SPConfiguration spConfiguration) throws SPException {

        if (defaultConfiguration != null) {
            try{

                defaultConfiguration.stopConfiguration(true);

            } catch (SPException spe) {
                spe.printStackTrace();
            }
        }
        defaultConfiguration = spConfiguration;



    }

    public static void updateConfigurationInXMLwithJAXB(String oldConfigurationName, SPConfiguration newSPConfiguration, File fileXML, File fileXSD) throws SPException{


        //List<SPConfiguration> confs = SPLayoutConfigurationHandler.getSPConfigurationListFromLayoutConfiguration(layoutConfiguration);

        configurationsAvailable.remove(oldConfigurationName);
        configurationsAvailable.put(newSPConfiguration.getConfigurationName(),newSPConfiguration);

        int i;
        for(i=0; i<layoutConfiguration.getCONFIGURATION().size(); i++){
            SPConfiguration appo = new SPConfiguration();
            Configuration conf = layoutConfiguration.getCONFIGURATION().get(i);
            SPCluster spCluster = new SPCluster();
            spCluster.setClusterID(conf.getCLUSTERID());
            appo.setCluster(spCluster);
            appo.setDescription(conf.getDESCRIPTION());
            appo.addDriverParam(conf.getDRIVER().getPARAM1());
            appo.addDriverParam(conf.getDRIVER().getPARAM2());
            appo.setDriverName(conf.getDRIVER().getDRIVERNAME());
            appo.setHostController(conf.getHOSTCONTROLLER());
            appo.setApiOwner(conf.getAPIOWNER());
            appo.setMcu(conf.getMCU());
            appo.setProtocol(conf.getPROTOCOL());
            appo.setAddressingType(conf.getADDRESSINGTYPE());
            appo.setVCC(conf.getVCC());

            if(oldConfigurationName.equals(appo.getConfigurationName())){
                break;
            }
        }


        Configuration conf = layoutConfiguration.getCONFIGURATION().get(i);
        conf.setCLUSTERID(newSPConfiguration.getCluster().getClusterID());
        conf.setDESCRIPTION(newSPConfiguration.getDescription());
        conf.getDRIVER().setDRIVERNAME(newSPConfiguration.getDriverName());
        conf.getDRIVER().setPARAM1(newSPConfiguration.getDriverParameters().get(0));
        conf.getDRIVER().setPARAM2(newSPConfiguration.getDriverParameters().get(1));
        conf.setHOSTCONTROLLER(newSPConfiguration.getHostController());
        conf.setAPIOWNER(newSPConfiguration.getApiOwner());
        conf.setMCU(newSPConfiguration.getMcu());
        conf.setPROTOCOL(newSPConfiguration.getProtocol());
        conf.setADDRESSINGTYPE(SPProtocol.ADDRESSING_TYPES[newSPConfiguration.getAddressingType()]);
        conf.setVCC(Integer.toString(newSPConfiguration.getVCC()));


        SPLayoutConfigurationHandler.overwriteXMLWithJAXB(layoutConfiguration,fileXML,fileXSD);


    }

    public static void modifyConfigurationInFile(String oldConfigurationName, SPConfiguration newSPConfiguration){

        try {
            String filePath = "./layout_configuration.xml";
            File fXmlFile = new File(filePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("CONFIGURATION");

            boolean flagConfigurationFound = false;
            Element element = null;
            int i=0;


            while(!flagConfigurationFound && i<nList.getLength()){
                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    element = (Element) nNode;

                    SPConfiguration appo = new SPConfiguration();

                    appo.setDescription(element.getElementsByTagName(DESCRIPTION).item(0).getTextContent());
                    appo.setDriverName(element.getElementsByTagName(DRIVER).item(0).getTextContent());
                    Element driverElement = (Element)element.getElementsByTagName(DRIVER).item(0);
                    appo.addDriverParam(driverElement.getAttribute("param1"));
                    appo.addDriverParam(driverElement.getAttribute("param2"));
                    //driverElement.getAttributes().getLength();

                    appo.setProtocol(element.getElementsByTagName(PROTOCOL).item(0).getTextContent());
                    appo.setMcu(element.getElementsByTagName(MCU).item(0).getTextContent());
                    appo.setHostController(element.getElementsByTagName(HOST_CONTROLLER).item(0).getTextContent());
                    appo.setApiOwner(element.getElementsByTagName(API_OWNER).item(0).getTextContent());
                    appo.setAddressingType(element.getElementsByTagName(ADDRESSING_TYPE).item(0).getTextContent());

                    String clusterID = element.getElementsByTagName(CLUSTER_ID).item(0).getTextContent();
                    for (SPCluster cluster : clusterList) {
                        if (cluster.getClusterID().equalsIgnoreCase(clusterID)) {
                            appo.setCluster(cluster);
                            break;
                        }
                    }
                    appo.setVCC(element.getElementsByTagName(VCC).item(0).getTextContent());

                    if(appo.getConfigurationName().equals(oldConfigurationName))
                        flagConfigurationFound = true;
                    else
                        i++;

                    /*TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    DOMSource source = new DOMSource(doc);
                    StreamResult result = new StreamResult(new File(filePath));
                    transformer.transform(source, result);

                    System.out.println("Done");

                    <DESCRIPTION>SINGLE_CHIP_RUN5</DESCRIPTION>
            <DRIVER param1="/dev/ttyUSB0" param2="460800">WINUX_COMUSB</DRIVER>
            <HOST_CONTROLLER>PC</HOST_CONTROLLER>
            <API_OWNER>PC</API_OWNER>
            <MCU>ESP8266</MCU>
            <PROTOCOL>SENSIBUS</PROTOCOL>
            <ADDRESSING_TYPE>NoAddress</ADDRESSING_TYPE>
            <CLUSTER_ID>0x04</CLUSTER_ID>
            <VCC>3300</VCC>

                    */


                    //System.out.println("Staff id : " + eElement.getAttribute("id"));
                    /*System.out.println(" : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
                    System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
                    System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
                    System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());*/

                }
            }

            //UPDATE the xml configuration

            //DRIVER UPDATE
            Element driverElement = (Element)element.getElementsByTagName(DRIVER).item(0);
            driverElement.setTextContent(newSPConfiguration.getDriverName());
            for(int cont=0; cont<newSPConfiguration.getDriverParameters().size();cont++){
                String attributeName = "param"+(cont+1);
                driverElement.setAttribute(attributeName,newSPConfiguration.getDriverParameters().get(cont));
            }

            element.getElementsByTagName(PROTOCOL).item(0).setTextContent(newSPConfiguration.getProtocol());
            element.getElementsByTagName(MCU).item(0).setTextContent(newSPConfiguration.getMcu());
            element.getElementsByTagName(HOST_CONTROLLER).item(0).setTextContent(newSPConfiguration.getHostController());
            element.getElementsByTagName(API_OWNER).item(0).setTextContent(newSPConfiguration.getApiOwner());
            element.getElementsByTagName(ADDRESSING_TYPE).item(0).setTextContent(SPProtocol.ADDRESSING_TYPES[newSPConfiguration.getAddressingType()]);
            element.getElementsByTagName(CLUSTER_ID).item(0).setTextContent(newSPConfiguration.getCluster().getClusterID());
            element.getElementsByTagName(VCC).item(0).setTextContent(Integer.toString(newSPConfiguration.getVCC()));

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filePath));
            transformer.transform(source, result);

            System.out.println("Done");


        }catch (Exception e){
            e.printStackTrace();
        }
    }
}


