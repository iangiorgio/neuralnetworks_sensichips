package com.sensichips.sensiplus.level0.driver.winux;

import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Marco Ferdinandi on 02/08/2017.
 */

public class SPDriverWINUX_IP extends SPDriver {
    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;
    private static int MAX_TIMEOUT = 10;
    public static final String DRIVERNAME = "WINUX_IP";
    //private static final String DRIVER_KEY = DRIVERNAME;
    private int DIM_BUFFER = 256;

    private String ip;
    private int port;
    private DataInputStream in;
    private DataOutputStream out;
    private String driverKey;
    private Socket socket;


    //private byte[] receivedData = null;
    int numByteReceived = -1;
    //private SPDriverListener driverListener;

    public SPDriverWINUX_IP(ArrayList<String> driverParameters)throws SPDriverException {
        this.ip = driverParameters.get(0);
        this.port = Integer.parseInt(driverParameters.get(1));
        this.driverKey = DRIVERNAME + "_" + ip + "_" + port;

    }


    @Override
    public boolean isConnected() throws SPDriverException {
        return false;
    }

    @Override
    public void openConnection() throws SPDriverException {

        try {
            this.socket = new Socket(ip, port);
            this.in = new DataInputStream(socket.getInputStream());
            this.out = new DataOutputStream(socket.getOutputStream());

        } catch (IOException e) {
            throw new SPDriverException("Socket error: " + ip + ", " + port);
        }
    }

    @Override
    public long getSPDriverTimeOut(){
        return 60000;
    }

    @Override
    public void setSPDriverTimeOut(Integer timeout) {

    }

    @Override
    public int read(byte[] buf) throws SPDriverException {
        int cont = 0;

        //receivedData = new byte[1];


        try {
            //System.out.println("Wait for data ...");
            buf[0] = in.readByte();
            //numByteReceived = receivedData.length;
        } catch (IOException e) {
            e.printStackTrace();
            throw new SPDriverException("Read error in driver: " + DRIVERNAME);
        }


        //System.arraycopy(receivedData, 0, buf, 0, numByteReceived);

        //receivedData = null;
        //numByteReceived = -1;
        return 1;
    }

    @Override
    public int write(byte[] buf) throws SPDriverException {

        try {
            out.write(buf);
        } catch (IOException e) {
            e.printStackTrace();
            throw new SPDriverException("Write error in driver: " + DRIVERNAME);
        }

        return buf.length;

    }

    @Override
    public String getDriverKey() {
        return driverKey;
    }

    @Override
    public void closeConnection() throws SPDriverException {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setParameters(ArrayList<String> driverParameters) throws SPDriverException {

    }

    @Override
    public String getDriverName() {
        return null;
    }




}
