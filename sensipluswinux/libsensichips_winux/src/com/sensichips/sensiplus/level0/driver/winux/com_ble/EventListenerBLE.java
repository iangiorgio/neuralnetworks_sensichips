package com.sensichips.sensiplus.level0.driver.winux.com_ble;



public interface EventListenerBLE {
	
	public void GAPDeviceInformation(String deviceAddress);
	
	public void GAPDiscoveryDone();
	
	public void GAPLinkEstablished();
	
	public void ATTReadByTypeResponse(String dataRead);
	
	public void ATTWriteResponse(String status);
	
	public void ATTHandleValueNotification(byte[] values);

	public void ATTNotificationEnabled();

	public void GAPTerminateLink();

}
