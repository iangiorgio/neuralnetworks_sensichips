package com.sensichips.sensiplus.level0.driver.winux.com_ble;

import java.util.ArrayList;
import java.util.List;

import jssc.SerialPort;
import jssc.SerialPortException;

public class Receiver extends Thread {
	
	private List<Byte> dataRead;
	private SerialPort sp;
	private ArrayList<EventListenerBLE> listenersList;
    private boolean stopThread = false;

    boolean ATTNotificationEnabled = false;
	
	public Receiver(SerialPort sp){
		this.sp = sp;
		dataRead = new ArrayList();
		listenersList = new ArrayList<EventListenerBLE>();
		this.start();
	}
	
	public void addEventListener(EventListenerBLE listener){
		listenersList.add(listener);
	}
	
	public List<Byte> read() throws SerialPortException{
		List<Byte> list = new ArrayList<Byte>();
		
		byte appo1,appo2;
		int length; 
		byte packetType = (byte)0x04;
		byte eventCode = (byte)0xFF;

		appo1 = sp.readBytes(1)[0];
		if(appo1 == packetType &&(appo2=sp.readBytes(1)[0])== eventCode){
			list.add(appo1);
			list.add(appo2);
			list.add(sp.readBytes(1)[0]);
			length = list.get(2).intValue();
			
			for(int i=0; i<length; i++)
			{
				list.add(sp.readBytes(1)[0]);
			}

		}
		
		return list;
	}


    public void setStopThread(boolean stopThread){
        this.stopThread = stopThread;
    }

	public void run(){
		while(!stopThread){
			try {	
				dataRead.addAll(this.read());
				if(dataRead.size()>0){
					//printDataRead(dataRead);
					filterEventOpCode(dataRead);
					dataRead.clear();
				} else {
                    try {
                        sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
				

				
			} catch (SerialPortException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			
		}
	}
	
	public void filterEventOpCode(List<Byte> buffer)
	{
		if(buffer.get(3) == (byte)0x0D  &&  buffer.get(4)==(byte)0x06 && buffer.get(6)==(byte)0x04)
		{
			byte[] address = new byte[6];
			for(int i=0; i<6; i++){
				address[i] = buffer.get(13-i);
			}
			for(int i=0; i<listenersList.size(); i++){
			this.listenersList.get(i).GAPDeviceInformation(bytesToHexStrings(address));
			}
		
		}
		
		else if(buffer.get(3) == (byte)0x05  &&  buffer.get(4)==(byte)0x06){
			for(int i=0; i<listenersList.size(); i++){
				this.listenersList.get(i).GAPLinkEstablished();
			}
		}
		
		else if(buffer.get(3) == (byte)0x01 &&  buffer.get(4)==(byte)0x06){
			for(int i=0; i<listenersList.size(); i++)
				this.listenersList.get(i).GAPDiscoveryDone();
		}
		
		else if(buffer.get(3) == (byte)0x09  &&  buffer.get(4)==(byte)0x05 &&  buffer.get(5)==(byte)0x00){
			int numOfValuesRead = buffer.get(9)-2;
			byte[] values = new byte[numOfValuesRead];
			for(int i=0; i<numOfValuesRead; i++){
				values[i] = buffer.get(12+i);
			}
			for(int i=0; i<listenersList.size(); i++)
				this.listenersList.get(i).ATTReadByTypeResponse(bytesToHexStrings(values));
		}
		else if(buffer.get(3) == (byte)0x13 &&  buffer.get(4)==(byte)0x05){
			if(buffer.get(5) == (byte)0x00){
                if (!ATTNotificationEnabled){
                    for(int i=0; i<listenersList.size(); i++){
                        this.listenersList.get(i).ATTNotificationEnabled();
                    }
                    ATTNotificationEnabled = true;
                } else {
				    for(int i=0; i<listenersList.size(); i++)
					    this.listenersList.get(i).ATTWriteResponse("success");
                }
			}
			else{
				for(int i=0; i<listenersList.size(); i++)
					this.listenersList.get(i).ATTWriteResponse("failed");
			}
			
		}

		//Quando arriva il valore notificato il payload è contenuto negli ultimi 5 byte, di cui:
		//il primo indica quanti sono gli effettivi e gli altri 4 byte contengono le informazioni
		else if(buffer.get(3) == (byte)0x1B  &&  buffer.get(4)==(byte)0x05){
			//int numOfValuesRead = buffer.get(8)-2;
			int start = buffer.size()-5;
			int stop = buffer.size();
			byte[] values = new byte[5];
			for(int i=0; i<5; i++){
				values[i] = buffer.get(start+i);
			}
			
			//printDataRead(buffer);
			
			for(int i=0; i<listenersList.size(); i++)
				this.listenersList.get(i).ATTHandleValueNotification(values);
		}

		else if(buffer.get(3) == (byte)0x06  &&  buffer.get(4)==(byte)0x06){
			for(int i=0; i<listenersList.size(); i++)
				this.listenersList.get(i).GAPTerminateLink();
		}
		
		return ;
		
	}
	
	public void printDataRead(List<Byte> list)
	{
		
		System.out.println();
		for(int i=0; i<list.size(); i++)
		{
			System.out.print(byteToHexString(list.get(i))+" ");
			
		}
		System.out.println();
	}
	
	
	
	
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String byteToHexString(byte b) {
        char[] hexChars = new char[2];

        int v = b & 0xFF;
        hexChars[0] = hexArray[v >>> 4];
        hexChars[1] = hexArray[v & 0x0F];

        return new String(hexChars);
    }
    
    private static String bytesToHexStrings(byte[] buff){
    	String hex="";
    	for(int i=0; i<buff.length; i++){
    		hex=hex+byteToHexString(buff[i])+" ";
    	}
    	
		return hex;
    	
    }

}
