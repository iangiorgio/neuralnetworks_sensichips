package com.sensichips.sensiplus.util.winux;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level0.SPEnvironmentalSensors;
import jssc.SerialPort;
import jssc.SerialPortException;

public class SPTempHumSHT30 implements SPEnvironmentalSensors {

    private float temp;
    private float hum;
    private boolean available = false;
    private String serialPort;
    private int portCounter = 0;
    private String currentSerialPort;
    private boolean stopSHT = false;
    private boolean stoppedSHT = true;
    private int MAX_NUM_PORT = 24;

    public static String WINDOWS = "Windows";

    public SPTempHumSHT30(){
        String osName = System.getProperty("os.name");
        System.out.println(osName);
        if(osName.contains(WINDOWS)){
            serialPort = "COM";
            MAX_NUM_PORT = 30;
        } else {
            serialPort = "/dev/ttyUSB";
            MAX_NUM_PORT = 5;
        }

    }

    @Override
    public boolean isAvailable(){
        return available;
    }

    @Override
    public void startMeasurement(){
        InternalMeasurementThread thread = new InternalMeasurementThread();
        thread.start();
        stopSHT = false;
        stoppedSHT = false;
    }

    public synchronized String getSerialPort(){
        String out = serialPort + portCounter;
        portCounter = ++portCounter % MAX_NUM_PORT;
        return out;
    }

    @Override
    public void stopMeasurement(){
        stopSHT = true;
        while(!stoppedSHT){
            delay(1);
        }
    }


    public synchronized float getTemp() throws SPException {
        if (!available){
            throw new SPException("SHT30 not available!");
        }
        return temp;
    }

    public synchronized float getHum() throws SPException {
        if (!available){
            throw new SPException("SHT30 not available!");
        }
        return hum;
    }

    public synchronized void setTemp(float temp){
        this.temp = temp;
    }

    public synchronized void setHum(float hum){
        this.hum = hum;
    }

    class InternalMeasurementThread extends Thread {



        public void run() {

            float[] values = new float[2];
            SerialPort serialPort = null;

            while (!stopSHT) {

                try {
                    currentSerialPort = getSerialPort();
                    System.out.println("Try with: " + currentSerialPort + " for SHT30");
                    serialPort = new SerialPort(currentSerialPort);
                    serialPort.openPort();
                    //Open serial port
                    serialPort.setParams(115200,
                            SerialPort.DATABITS_8,
                            SerialPort.STOPBITS_1,
                            SerialPort.PARITY_NONE);//Set params. Also you can set params by this string: serialPort.setParams(9600, 8, 1, 0);

                    available = true;
                    System.out.println("Port " + currentSerialPort + " opened.");

                    long lastTimeStamp = System.currentTimeMillis();
                    long lastTimeStampReceived = lastTimeStamp;

                    while (!stopSHT) {
                        byte[] recvd = serialPort.readBytes();
                        if (recvd != null) {

                            String humTemp = new String(recvd);
                            if (humTemp.length() > 0) {
                                try {
                                    tokenizer(values, humTemp);
                                    setTemp(values[0]);
                                    setHum(values[1]);
                                    lastTimeStampReceived = System.currentTimeMillis();
                                } catch (SPException e) {
                                    //System.err.println(e.getMessage());
                                }
                                /*for (int i = 0; i < values.length; i++) {
                                    System.out.print(values[i] + ";");
                                }
                                System.out.println();*/
                            }
                        } else {
                            if (lastTimeStamp - lastTimeStampReceived > 5000) {
                                throw new SerialPortException(currentSerialPort, "run()", "Port probably unavaialable!");
                            }
                        }
                        lastTimeStamp = System.currentTimeMillis();
                        delay(100);
                    }


                } catch (SerialPortException spe) {
                    try {
                        serialPort.closePort();
                    } catch (SerialPortException e) {
                        //System.out.println("Port closed.");
                    }
                    available = false;
                    //System.out.println("SHT30 on port " + currentSerialPort + " is unavailable!");
                } finally {
                    if (serialPort != null) {
                        try {
                            serialPort.closePort();
                        } catch (SerialPortException e) {
                            //e.printStackTrace();
                        }
                    }
                }
                delay(1000);
            }
            System.out.println("Stopped!!!");
            stoppedSHT = true;

        }
    }


    public static void delay(long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String args[]){
        SPTempHumSHT30 spTempHumSHT30 = new SPTempHumSHT30();
        System.out.println("Start measurement...");
        spTempHumSHT30.startMeasurement();
        delay(10000);
        System.out.println("Stop measurement...");
        spTempHumSHT30.stopMeasurement();


        System.out.println("Start measurement 2 ...");
        spTempHumSHT30.startMeasurement();
        delay(10000);
        System.out.println("Stop measurement 2 ...");
        spTempHumSHT30.stopMeasurement();

    }


    public void tokenizer(float values[], String rcvd) throws SPException {
        String lines[] = rcvd.split("\r\n");

        if (lines.length == 0){
            throw new SPException("Are you sure that on " + serialPort + " is connected SHT30?");
        }

        String tokens[] = lines[lines.length - 1].split(";");
        if (tokens.length != 4){
            throw new SPException("Are you sure that on " + serialPort + " is connected SHT30?");
        }

        if (!tokens[0].equals("Temp") || !tokens[1].equals("Hum")){
            throw new SPException("Are you sure that on " + serialPort + " is connected SHT30?");
        }

        values[0] = Float.parseFloat(tokens[2]);
        values[1] = Float.parseFloat(tokens[3]);

    }

}
