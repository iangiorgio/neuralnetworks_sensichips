--
--	        SENSIPLUS RUN2
--
--     	  Test Registers Write & Read
--
--

WRITE COMMAND		S 0x00 	-- chip soft reset because there is an issue with the internal POR

--

READ CHA_CONFIG M 2

WRITE INT_CONFIG 	M 0x3210

READ INT_CONFIG 	M 2

READ PPR_COUNTH M 2

READ SENSIBUS_SET M 2

READ STATUS M 12