package com.sensichips.sensiplus.demoapp.winux;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.config.SPLayoutConfigurationHandler;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;
import com.sun.corba.se.spi.ior.ObjectKey;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class XMLServer extends Thread {

    public static String ANDROID_REQUEST = "Android";
    public static String WINUX_REQUEST = "Winux";

    private static SPConfiguration spConfigurationsList[] = null;
    private SPBatchParameters spBatchParameters = null;


    public XMLServer(SPBatchParameters spBatchParameters) {

        this.spBatchParameters = spBatchParameters;

       /* try{

            System.out.println("Server is loading configuration from file ... ");
            SPConfigurationManager.loadConfigurationFromFileWithJAXB(new File(spBatchParameters.layoutConfigurationFileXML), new File(spBatchParameters.layoutConfigurationFileXSD));

            spConfigurationsList = new SPConfiguration[SPConfigurationManager.getConfigurationNames().size()];
            for(int i = 0; i < spConfigurationsList.length; i++){
                spConfigurationsList[i] = SPConfigurationManager.getSPConfiguration(SPConfigurationManager.getConfigurationNames().get(i));
            }
            System.out.println("... done.\n");

        } catch (SPException exception){
            spConfigurationsList = new SPConfiguration[0];

        }*/

    }


    private void loadConfiguration(String request){

        try{


            SPConfigurationManager.loadConfigurationFromFileWithJAXB(new File(spBatchParameters.layoutConfigurationFileXML), new File(spBatchParameters.layoutConfigurationFileXSD));

            String appo = request.toUpperCase();
            ArrayList<Integer> listOfIndexToWithdraw = new ArrayList<>();

            for(int i=0; i<SPConfigurationManager.getListSPConfiguration().size(); i++){
                if(SPConfigurationManager.getListSPConfiguration().get(i).getConfigurationName().contains(appo)){
                    listOfIndexToWithdraw.add(i);
                }
            }
            spConfigurationsList = new SPConfiguration[listOfIndexToWithdraw.size()];
            for(int i=0; i<listOfIndexToWithdraw.size(); i++){
                spConfigurationsList[i] = SPConfigurationManager.getListSPConfiguration().get(listOfIndexToWithdraw.get(i));
            }


            /*spConfigurationsList = new SPConfiguration[SPConfigurationManager.getConfigurationNames().size()];
            for(int i = 0; i < spConfigurationsList.length; i++){

                spConfigurationsList[i] = SPConfigurationManager.getSPConfiguration(SPConfigurationManager.getConfigurationNames().get(i));
            }*/
            System.out.println("... done.\n");

        } catch (SPException exception){
            spConfigurationsList = new SPConfiguration[0];

        }
    }


    public void run(){

        ServerSocket ss = null;
        while(ss == null) {

            try {
                ss = new ServerSocket(spBatchParameters.port);
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
                try {
                    sleep(10000); // Wait before retry
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.err.println("Retry to open the server socket...");
            }
        }

        try{
            while(true){
                System.out.println("SPConfiguration server listening on port: " + spBatchParameters.port);
                Socket s = ss.accept();

                ObjectInputStream objectInput = new ObjectInputStream(s.getInputStream());
                String request = (String) objectInput.readObject();
                //objectInput.close();
                loadConfiguration(request);

                ObjectOutputStream objectOutput = new ObjectOutputStream(s.getOutputStream());
                objectOutput.writeObject(spConfigurationsList);
                objectOutput.flush();
                System.out.println("SPConfiguration sent to : " + s.getInetAddress() + ":" + s.getPort() + "\n");
                objectOutput.close();
                s.close();
            }
        } catch (IOException ioe){
            System.err.println("ERROR ON SERVER. SERVER SHUTDOWN!!");
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }

    }




}
