package com.sensichips.sensiplus.demoapp.winux;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMBLE;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_IP;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.SPMeasurementRUN5_PC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemMeasures;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import static com.sensichips.sensiplus.config.SPConfigurationManager.setDefaultSPConfiguration;

/**
 * Created by Marco Ferdinandi on 16/04/2018.
 */
public class SensiplusWinuxJSON implements SPConfigurationManager.SPDriverGenerator {
    private static final int PHASE_STARTUP = 0;
    private static final int PHASE_MEASURE = 1;


    private static final String STARTUP_STATE = "start";
    private static final String SETEIS_STATE = "setEIS";
    private static final String GETEIS_STATE = "getEIS";

    private int stateIndex = PHASE_STARTUP;

    public static int socketPort = 9999;
    private JSONParser parser;
    SPConfiguration spConfiguration;
    SPMeasurementParameterEIS spMeasurementParameterEIS ;
    SPMeasurement spMeasurement;
    SPMeasurementOutput spMeasurementOutput;

    public PrintWriter printWriter;
    public BufferedReader bufferedReader;


    public SensiplusWinuxJSON()throws SPException{
        SPConfigurationManager.loadConfigurationFromFileWithJAXB(
                new File("layout_configuration_files/layout_configuration_new.xml"),
                new File("layout_configuration_files/schema_layout_configuration_new.xsd"));



        parser = new JSONParser();
        spMeasurementOutput = new SPMeasurementOutput();

    }





    public static void main(String[] args){

        try {
            SensiplusWinuxJSON sensiplusWinuxJSON = new SensiplusWinuxJSON();


            ServerSocket ss = null;
            while (ss == null) {

                try {
                    ss = new ServerSocket(socketPort);
                } catch (IOException ioe) {
                    System.err.println(ioe.getMessage());
                    try {
                        Thread.sleep(10000); // Wait before retry
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.err.println("Retry to open the server socket...");
                }
            }

            String inputLine,outputLine;
            while (true) {
                System.out.println("SensiplusWinux Listening on port: " + socketPort);
                Socket socket = ss.accept();

                sensiplusWinuxJSON.printWriter = new PrintWriter(socket.getOutputStream(), true);
                sensiplusWinuxJSON.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));


                while ((inputLine = sensiplusWinuxJSON.bufferedReader.readLine()) != null) {
                    outputLine = sensiplusWinuxJSON.processRequest(inputLine);
                    System.out.print("RESPONSE: " + outputLine);
                    if (outputLine.equals("exit"))
                        break;
                    sensiplusWinuxJSON.printWriter.println(outputLine);

                }

                socket.close();

            }




        }catch (SPException|IOException|ParseException e){
            e.printStackTrace();
        }





    }




    private String processRequest(String input)throws ParseException{

        String response = "";

        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(input);



        if (jsonObject.get("state").equals("exit")) {
            response = "exit";
            stateIndex = PHASE_STARTUP;
        } else {

            JSONObject responseJSON = new JSONObject();
            switch (stateIndex) {
                case PHASE_STARTUP:
                    if (jsonObject.get("state").equals(STARTUP_STATE)) {
                        String configurationName = (String) jsonObject.get("configuration");
                        try {
                            spConfiguration = SPConfigurationManager.getSPConfiguration(configurationName);
                            spConfiguration.startConfiguration(this,
                                    true,
                                    true,
                                    true,
                                    true,
                                    true,
                                    true,
                                    true);

                            setDefaultSPConfiguration(spConfiguration);

                            spMeasurement = new SPMeasurementRUN5_PC(spConfiguration, null, "EIS");

                            responseJSON.put("state", "ok");
                            responseJSON.put("message", "Startup executed with success");
                            stateIndex++;

                        } catch (SPException spe) {
                            responseJSON.put("state", "error");
                            responseJSON.put("message", "Problems during the configuration startup. Try again");
                        }
                    } else {
                        responseJSON.put("state", "error");
                        responseJSON.put("message", "Start the configuration as first operation");
                    }

                    break;
                case PHASE_MEASURE:
                    if (jsonObject.get("state").equals(SETEIS_STATE)) {
                        try {
                            boolean flagAutorange = false;
                            spMeasurementParameterEIS = new SPMeasurementParameterEIS(spConfiguration);
                            spMeasurementParameterEIS.setMeasure((String) jsonObject.get("measure"));
                            spMeasurementParameterEIS.setFrequency(Float.parseFloat((String) jsonObject.get("frequency")));
                            String rsense = (String) jsonObject.get("rsense");
                            if (rsense.equals("auto"))
                                flagAutorange = true;
                            else
                                spMeasurementParameterEIS.setRsense(rsense);
                            String ingain = (String) jsonObject.get("ingain");
                            if (ingain.equals("auto"))
                                flagAutorange = true;
                            else
                                spMeasurementParameterEIS.setInGain(ingain);
                            spMeasurementParameterEIS.setOutGain((String) jsonObject.get("outgain"));
                            spMeasurementParameterEIS.setDCBiasP(Integer.parseInt((String) jsonObject.get("dcbiasP")));
                            spMeasurementParameterEIS.setDCBiasN(Integer.parseInt((String) jsonObject.get("dcbiasN")));
                            spMeasurementParameterEIS.setContacts((String) jsonObject.get("contacts"));
                            spMeasurementParameterEIS.setModeVI((String) jsonObject.get("modevi"));
                            spMeasurementParameterEIS.setHarmonic((String) jsonObject.get("harmonic"));
                            spMeasurementParameterEIS.setInPort((String) jsonObject.get("inport"));
                            spMeasurementParameterEIS.setOutPort((String) jsonObject.get("outport"));
                            spMeasurementParameterEIS.setFilter((String) jsonObject.get("filter"));
                            spMeasurementParameterEIS.setPhaseShiftQuadrants((String) jsonObject.get("phaseShiftMode"),
                                    Integer.parseInt((String) jsonObject.get("phaseShift")),
                                    (String) jsonObject.get("IQ"));

                            if(flagAutorange)
                                spMeasurement.autoRange(spMeasurementParameterEIS);

                            spMeasurement.setEIS(spMeasurementParameterEIS, spMeasurementOutput);

                            responseJSON.put("state", "ok");
                            responseJSON.put("message", "");

                        } catch (SPException spe) {
                            spe.printStackTrace();
                            responseJSON.put("state", "error");
                            responseJSON.put("message", "Problems during the parameters' setting");
                        }
                    } else if (jsonObject.get("state").equals(GETEIS_STATE)) {
                        if (spMeasurementParameterEIS == null) {
                            responseJSON.put("state", "error");
                            responseJSON.put("message", "setEIS needed before getEIS!!!");
                        } else {
                            try {
                                double output[][] = spMeasurement.getEIS(spMeasurementParameterEIS, spMeasurementOutput);
                                responseJSON.put("state", "ok");
                                //responseJSON.put("value", output[0][spMeasurementParameterEIS.getMeasure()]);
                                responseJSON.put("value", output);
                                responseJSON.put("timestamp", System.currentTimeMillis());
                                responseJSON.put("unitmeasure", SPParamItemMeasures.unitMeasuresLabels[spMeasurementParameterEIS.getMeasure()]);
                                responseJSON.put("message", "");

                            } catch (SPException spe) {
                                spe.printStackTrace();
                                responseJSON.put("state", "error");
                                responseJSON.put("message", "Problems during getEIS!!!");
                            }
                        }

                    } else {
                        responseJSON.put("state", "error");
                        responseJSON.put("message", "setEIS command is required in this phase");
                    }

                    break;
            }


            //socket.getOutputStream().write(response.toString().getBytes());

            response = responseJSON.toJSONString();

        }

        return response;

    }


    @Override
    public String generateDriver(String driverName, ArrayList<String> driverParameters) throws SPDriverException {
        SPDriver sp;
        if (driverName.equals(SPDriverWINUX_COMUSB.DRIVERNAME)){
            sp = new SPDriverWINUX_COMUSB(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_COMBLE.DRIVERNAME)){
            sp = new SPDriverWINUX_COMBLE(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_IP.DRIVERNAME)){
            sp = new SPDriverWINUX_IP(driverParameters);
        } else {
            throw new SPDriverException("Wrong driverName in generateDriver in SPWinuxTools");
        }

        SPDriverManager.add(sp);
        return sp.getDriverKey();
    }

}
