package com.sensichips.sensiplus.demoapp.winux;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * Created by Marco Ferdinandi on 08/03/2018.
 */
public class UDPServer extends Thread {

    private int BUFFER_LENGTH = 4;
    private String MCAST_ADDRESS = "228.5.6.7";
    private int DEST_PORT = 6789;

    public UDPServer() {

    }

    public void run() {
        try {
            byte[] b = new byte[BUFFER_LENGTH];
            MulticastSocket mSocket = new MulticastSocket(DEST_PORT); // must bind receive side
            InetAddress MCAST_GROUP = InetAddress.getByName(MCAST_ADDRESS);
            mSocket.joinGroup(MCAST_GROUP);

            while (true) {
                System.out.println("Waiting for multicast packets...");
                DatagramPacket dgram = new DatagramPacket(b, b.length);
                mSocket.receive(dgram); // blocks until a datagram is received
                System.out.println("Received " + dgram.getLength() +
                        " bytes from " + dgram.getAddress());
                //dgram.setLength(b.length); // must reset length field!
                System.out.println("" + new String(dgram.getData()));

                DatagramPacket hi = new DatagramPacket(dgram.getAddress().getAddress(), dgram.getAddress().getAddress().length, MCAST_GROUP, DEST_PORT);
                mSocket.send(hi);

                dgram = new DatagramPacket(b, b.length);
                mSocket.receive(dgram); // blocks until a datagram is received


            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
