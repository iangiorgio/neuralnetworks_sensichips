package com.sensichips.sensiplus.demoapp.winux.test;

import java.io.File;
import java.io.PrintWriter;

/**
 * Created by mario on 18/04/16.
 */
public class Main2 {

    public static void main(String args[]) throws Exception {

        Main2 m = new Main2();

        PrintWriter printWriter = new PrintWriter(new File("conversion.txt"));

        byte[] data = new byte[]{(byte)0xFD, (byte)0xF2};

        printWriter.println("Bytes received from FIFO:\tDecimal values:");

        for(int i = 0; i <= 255; i++){
            for(int j = 0; j <= 255; j++){
                data = new byte[]{(byte)i, (byte)j};
                int output = m.toInt(data);


                StringBuilder sb = new StringBuilder();
                for (byte b : data) {
                    sb.append(String.format("%02X ", b));
                }
                printWriter.println(sb.toString() + "                        [" + output + "]");

            }
        }

        printWriter.close();
    }



    public int toInt(byte[] data){
        int maschera = 255;

        int x = ((int) data[1]) << 8;   // Sfhift to left with sign extension
        int y = data[0];
        y = y & maschera;
        return x | y;
    }

}
