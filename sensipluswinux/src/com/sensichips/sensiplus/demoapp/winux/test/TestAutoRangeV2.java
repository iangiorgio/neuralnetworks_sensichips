package com.sensichips.sensiplus.demoapp.winux.test;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMBLE;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_IP;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level2.SPAutoRangeAlgorithmV2;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.SPMeasurementRUN5_PC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterADC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.util.log.SPLogger;

import java.io.File;
import java.util.ArrayList;

import static com.sensichips.sensiplus.config.SPConfigurationManager.getSPConfigurationDefault;
import static com.sensichips.sensiplus.config.SPConfigurationManager.setDefaultSPConfiguration;

/**
 * Created by Marco Ferdinandi on 09/05/2018.
 */
public class TestAutoRangeV2 implements SPConfigurationManager.SPDriverGenerator {


    public TestAutoRangeV2(){

    }

    public static void main(String args[]){
        SPLogger.DEBUG_TH = 0;
        SPConfiguration spConfiguration = null;
        SPMeasurementParameterEIS spMeasurementParameterEIS ;
        SPMeasurement spMeasurement;
        SPMeasurementOutput spMeasurementOutput = new SPMeasurementOutput();

        TestAutoRangeV2 testAutoRangeV2 = new TestAutoRangeV2();

        try {
            SPConfigurationManager.loadConfigurationFromFileWithJAXB(
                    new File("layout_configuration_files/layout_configuration_new.xml"),
                    new File("layout_configuration_files/schema_layout_configuration_new.xsd"));

            SPAutoRangeAlgorithmV2 prova = new SPAutoRangeAlgorithmV2(null, true, 2800);


            for(int i=0; i<SPConfigurationManager.getListSPConfiguration().size(); i++){
                if(SPConfigurationManager.getListSPConfiguration().get(i).getConfigurationName().contains("WINUX") &&
                        SPConfigurationManager.getListSPConfiguration().get(i).getConfigurationName().contains("ESP8266")){
                    spConfiguration = SPConfigurationManager.getListSPConfiguration().get(i);
                }

            }


            //spConfiguration = SPConfigurationManager.getSPConfiguration("");
            spConfiguration.startConfiguration(testAutoRangeV2,
                    true,
                    true,
                    true,
                    true,
                    true,
                    true,
                    true);

            setDefaultSPConfiguration(spConfiguration);

            spMeasurementParameterEIS = new SPMeasurementParameterEIS(getSPConfigurationDefault());
            spMeasurementParameterEIS.setMeasure("CONDUCTANCE");

            spMeasurementParameterEIS.setFrequency(Float.parseFloat("78125.0"));

            spMeasurementParameterEIS.setRsense("50000");
            spMeasurementParameterEIS.setInGain("40");
            spMeasurementParameterEIS.setOutGain("7");

            spMeasurementParameterEIS.setDCBiasP(0);
            spMeasurementParameterEIS.setDCBiasN(0);
            spMeasurementParameterEIS.setContacts("TWO");
            spMeasurementParameterEIS.setModeVI("VOUT_IIN");
            spMeasurementParameterEIS.setHarmonic("FIRST_HARMONIC");
            spMeasurementParameterEIS.setInPort("PORT_HP");
            spMeasurementParameterEIS.setOutPort("PORT_HP");
            spMeasurementParameterEIS.setFilter(Integer.toString(1));
            spMeasurementParameterEIS.setPhaseShiftQuadrants("Quadrants",
                    0,
                    "IN_PHASE");

            spMeasurement = new SPMeasurementRUN5_PC(spConfiguration, null, "EIS");
            //spMeasurement.setMeasureIndexToSave(7);

            SPAutoRangeAlgorithmV2 spAutoRangeAlgorithmV2 = new SPAutoRangeAlgorithmV2(spMeasurement,true,2800);
            spAutoRangeAlgorithmV2.autoRange(spMeasurementParameterEIS);

            spMeasurement.autoRange(spMeasurementParameterEIS);
            spMeasurement.setEIS(spMeasurementParameterEIS,spMeasurementOutput);

            for(int i=0; i<10; i++){
                if (true)
                    throw new SPException("setADC in autorange should be verified for RUN5 and RUN6 in terms of FIFO_READ and FIFO_DEEP dimension");
                spMeasurement.setADC(1, 1, spMeasurementParameterEIS. getInGainLabel(), null, SPMeasurementParameterADC.INPORT_IA, true, spMeasurementOutput);
                double output[][] = spMeasurement.getEIS(spMeasurementParameterEIS,spMeasurementOutput);
                for(int k=0; k<output.length; k++){
                    System.out.println("Measure: "+output[k][6]);
                }
            }

        }catch (SPException e){
            e.printStackTrace();
        }
    }


    @Override
    public String generateDriver(String driverName, ArrayList<String> driverParameters) throws SPDriverException {
        SPDriver sp;
        if (driverName.equals(SPDriverWINUX_COMUSB.DRIVERNAME)){
            sp = new SPDriverWINUX_COMUSB(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_COMBLE.DRIVERNAME)){
            sp = new SPDriverWINUX_COMBLE(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_IP.DRIVERNAME)){
            sp = new SPDriverWINUX_IP(driverParameters);
        } else {
            throw new SPDriverException("Wrong driverName in generateDriver in SPWinuxTools");
        }

        SPDriverManager.add(sp);
        return sp.getDriverKey();
    }
}
