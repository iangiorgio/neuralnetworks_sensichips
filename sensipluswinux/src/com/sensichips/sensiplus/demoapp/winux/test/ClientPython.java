package com.sensichips.sensiplus.demoapp.winux.test;

import java.io.*;
import java.net.Socket;

public class ClientPython {

    public static void main(String args[]) {



        String[] chips = new String[]{"5B05", "5605", "5805", "5905"};

        try {

            for(String chipID : chips){
                //String chipID = "5905";
                String directory = "/home/mario/Dropbox/sensichips/DEMONSTRATORS/Measurements/Misure NOSY gas/10-10-2017/";

                File testFile = new File(directory + "Experiment_10-10-2017_EIS_Port5_ChipID_0X000000" + chipID + ".csv");
                File outFile = new File(directory + "Experiment_10-10-2017_EIS_Port5_ChipID_0X000000" + chipID + "_pred.csv");



                BufferedReader testFileReader = new BufferedReader(new FileReader(testFile));
                PrintWriter outFileWriter = new PrintWriter(outFile);




                String host = "hego.unicas.it";
                int portNumber = 43547;
                System.out.println("Try to connect: " + host + ", port: " + portNumber);
                Socket socketClient = new Socket(host, portNumber);

                OutputStream outputStream = socketClient.getOutputStream();
                PrintWriter printWriter = new PrintWriter(outputStream, true);

                InputStream inputStream = socketClient.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                long startTime = System.currentTimeMillis();

                printWriter.println("0X" + chipID);

                String ack = bufferedReader.readLine();
                System.out.println("Sending data after ack (" + ack + ") ...");

                String predictedClass = null;
                int counter = 0;
                int indexOfADC = 12;
                String valueToSend = null;
                // Ignore the first line
                String line = testFileReader.readLine();

                int rowCounter = 0;
                outFileWriter.println(line + "Sent value;class");
                while((line = testFileReader.readLine()) != null){
                    String[] tokens = line.split(";");
                    valueToSend = tokens[indexOfADC].replace(",", ".");
                    printWriter.println("" + valueToSend);
                    //System.out.println("valueToSend: " + valueToSend);
                    predictedClass = bufferedReader.readLine();
                    outFileWriter.println(line + valueToSend.replace(".", ",") + "; " + predictedClass);
                    outFileWriter.flush();
                    if ((rowCounter % 50) == 0){
                        System.out.println("Row Counter: " + rowCounter);
                    }
                    //System.out.println("" + rowCounter + ";" + valueToSend + "; " + predictedClass);
                    counter++;
                    rowCounter++;
                }
                long endTime = System.currentTimeMillis();
                printWriter.close();
                bufferedReader.close();
                System.out.println("Elapsed [s]: " + (endTime-startTime)/(float)1000);

                socketClient.close();

            }



        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
