--
--	        SENSIPLUS v0.1
--
--     	     Attached Devices
--
--   (to read only once at Applet launch)
--



-- #CONFIGURATION

-- SPI don't care DEV_ID
-- for I2C, DEV_ID is one byte
-- for SENSIBUS, DEV_ID is 6 bytes

-- HW_VERSION 	    = RUN4 -- [RUN3, RUN4]
-- VCC		        = 3300 -- [mV]
-- SYS_CLOCK	    = 10000000 -- [Hz]
-- OSC_TRIM	    = 0x06 -- trim the internal Oscillator


#REGISTERS
--
--  	    Register Address Map
--
--
STATUS		    = 0x00
COMMAND		    = 0x00
INT_CONFIG	    = 0x02
ANA_CONFIG	    = 0x04
DIG_CONFIG	    = 0x06
CHA_DIVIDER	    = 0x08
CHA_CONFIG	    = 0x0A
CHA_LIMIT	    = 0x0C
CHA_CONDIT	    = 0x0E
CHA_FILTER	    = 0x10
CHA_SELECT	    = 0x12
US_BURST	    = 0x14
US_TRANSMIT	    = 0x16
RXCOUNTERL	    = 0x18
US_PROBE	    = 0x18
RXCOUNTERH 	    = 0x1A
US_RECEIVE 	    = 0x1A
DSS_DIVIDER	    = 0x1C
DDS_FIFO	    = 0x1E
DDS_CONFIG	    = 0x20
DAS_CONFIG	    = 0x22
DSS_SELECT	    = 0x24
TIMERL		    = 0x26
TIMER_DIVL	    = 0x26
TIMERH		    = 0x28
TIMER_DIVH	    = 0x28
PPR_COUNTL 	    = 0x2A
PPR_THRESL	    = 0x2A
PPR_COUNTH      = 0x2C
PPR_THRESH	    = 0x2C
PPR_SELECT	    = 0x2E
CHA_FIFOL	    = 0x30
CHA_FIFOH	    = 0x32
EFUSE_PROGR     = 0x34
SENSIBUS_SET    = 0x36

DEVICE_ID0	    = 0x38
DEVICE_ID1	    = 0x3A
DEVICE_ID2	    = 0x3C
USER_EFUSE      = 0x3E
