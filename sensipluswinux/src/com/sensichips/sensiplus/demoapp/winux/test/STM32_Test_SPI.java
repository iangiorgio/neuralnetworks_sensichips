package com.sensichips.sensiplus.demoapp.winux.test;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

/**
 * Created by mario on 03/12/2015.
 */
public class STM32_Test_SPI {

    private static int DEBUG_LEVEL = 2;

    private String driverName = "COM_USB";

    private static String LOG_MESSAGE = "";

    private SerialPort serialPort;

    private String COMPort;
    private boolean connected;

    public STM32_Test_SPI(String COMPort) throws Exception {
        this.COMPort = COMPort;
    }



    public void openConnection() throws Exception {
        try{
            if (!connected){
                serialPort = new SerialPort(COMPort);
                this.serialPort.openPort();
                //Open serial port
                this.serialPort.setParams(460800,
                        SerialPort.DATABITS_8,
                        SerialPort.STOPBITS_1,
                        SerialPort.PARITY_NONE);//Set params. Also you can set params by this string: serialPort.setParams(9600, 8, 1, 0);
                connected = true;
            }
        } catch (SerialPortException spe){
            SerialPortList spl;
            String[] portNames = SerialPortList.getPortNames();
            String mes = "";
            for(int i = 0; i < portNames.length; i++){
                mes += portNames[i] + (i < (portNames.length - 1)?", ":"");
            }

            throw new Exception("SPDriverWINUX_COMUSB in open port: " + spe.getMessage() + "\nAvailable ports: " + mes);
        }
    }

    public String getDriverName() throws Exception {
        return driverName;
    }


    public boolean isConnected(){
        return connected;
    }

    /**
     * Receive bytes on USB at the low possible level
     *
     * @param buf with this buf(byte)0xFE,r the function return the received bytes to the calling function.
     * @return the number received bytes
     */
    public synchronized int read(byte[] buf) throws Exception {
        int numByteReceived = 0;
        try {
            byte[] recvd = serialPort.readBytes();
            if (recvd != null){
                //System.out.println(recvd.length);
                numByteReceived = recvd.length;
                System.arraycopy(recvd, 0, buf, 0, recvd.length);
            } else {
                //System.out.println("Ricevuto null ...");
            }
        } catch (SerialPortException spe){
            throw new Exception(LOG_MESSAGE + " SPDriverWINUX_COMUSB read. " + spe.getMessage());
        }
        return numByteReceived;
    }

    /**
     * Send bytes on USB at the low possible level
     *
     * @param buf all the bytes into te buf array will be send on the USB channel
     * @return the number of bytes ef(byte)0xFE,ctively sent
     */
    public synchronized int write(byte[] buf) throws Exception {
        try{
            boolean ok = serialPort.writeBytes(buf);
            if (ok) {
                return buf.length;
            } else {
                return 0;
            }
        } catch (SerialPortException spe){
            throw new Exception("SPDriverWINUX_COMUSB send. " + spe.getMessage());
        }
    }


    public void closeConnection() throws Exception {
        try {
            connected = false;
            serialPort.closePort();
        } catch (SerialPortException e) {
            throw new Exception("SPDriverWINUX_COMUSB closeConnection(): " + e.getMessage());
        }
    }

    public static void main(String args[]) throws Exception {

        byte[] resp = null;

        STM32_Test_SPI port = new STM32_Test_SPI("/dev/ttyACM0");
        port.openConnection();
        int numByteReceived = 0;
        int numByteReceivedTot = 0;
        int numByteExpected = 0;
        int counter = 0;
        //byte[] send = null;
        byte[] send = new byte[]{(byte)0x00, (byte)0x01, (byte)0x20}; // Scrittura
        byte[] recv = null;
        //(byte)0x03, (byte)0x67, (byte)0x80, (byte)0x45, (byte)0x4D, (byte)0x0F};
        long startTime = System.currentTimeMillis();
        int count = 0;
        int maxTransaction = 10000;
        int numberOfRepetition = 64;
        int totByteSent = 0;
        int totByteRecvd = 0;

        do{

            /****************************************************************/



             port.write(send);
             numByteExpected = send[1];

             port.write(send);

             numByteReceived = 0;
             numByteReceivedTot = 0;
             //Thread.sleep(100);
             counter = 0;


             while(numByteReceivedTot < numByteExpected) {
                 resp = new byte[256];
                 while (numByteReceived == 0) {
                    numByteReceived = port.read(resp);
                    Thread.sleep(1);
                 }

                 numByteReceivedTot += numByteReceived;
                 for (int i = 0; i < numByteReceived; i++) {
                     //if(resp[i] != 0 && resp[i] != 1){
                     counter++;
                     if (count % 100 == 0){
                         System.out.print("" + Integer.toHexString(resp[i]).replace("ffffff", "") + " "); // + "(" + (char)resp[i] + ")");
                     }
                 }
                 numByteReceived = 0;
             }
             if (count % 100 == 0) {
                 System.out.println("\n*******************************************************************");
             }





            numByteReceived = 0;
            numByteReceivedTot = 0;
            numByteExpected = 0;
            counter = 0;
            byte[] bufAppo = new byte[]{(byte)0xC3, (byte)0x02};;

            recv = new byte[2 * numberOfRepetition];

            for(int i = 0; i < recv.length; i = i + 2){
                recv[i] = bufAppo[0];
                recv[i + 1] = bufAppo[1];
            }

            totByteSent += recv.length;

            port.write(recv);

            numByteReceived = 0;
            numByteReceivedTot = 0;
            numByteExpected = recv[1] * numberOfRepetition;
            counter = 0;
            //Thread.sleep(100);
            while(numByteReceivedTot < numByteExpected) {
                resp = new byte[256];
                while (numByteReceived == 0) {
                    numByteReceived = port.read(resp);
                    Thread.sleep(1);
                }

                numByteReceivedTot += numByteReceived;

                for (int i = 0; i < numByteReceived; i++) {
                    //if(resp[i] != 0 ){
                        /*if (counter % 2 == 0){
                            if (resp[i] != send[2]){
                                System.err.println("Error. Expected: " + send[2] + ", received: " + resp[i] + " at count = " + count);
                            }
                        } else {
                            if (resp[i] != send[3]){
                                System.err.println("Error. Expected: " + send[3] + ", received: " + resp[i] + " at count = " + count);
                            }

                        }*/
                        if (count % 100 == 0){
                            System.out.print("" + Integer.toHexString(resp[i]).replace("ffffff", "") + " "); // + "(" + (char)resp[i] + ")");
                            System.out.flush();
                        }
                        counter++;
                    //}
                }
                numByteReceived = 0;

            }
            totByteRecvd += numByteExpected;
            if (count % 100 == 0){
                System.out.println();
                System.out.println("count: " + count);
                System.out.flush();
            }
            //System.out.println("\n*******************************************************************");




            //Thread.sleep(1);
        }while(count++ < maxTransaction);


        count--;
        long endTime = System.currentTimeMillis();
        double seconds = (endTime - startTime)/(double)1000;
        System.out.println("Tot byte sent: " + totByteSent);
        System.out.println("Tot byte recv: " + totByteRecvd);
        System.out.println("Tx [bit/s]:" + (totByteSent * (float)8)/seconds);
        System.out.println("Rx [bit/s]:" + (totByteRecvd * (float)8)/seconds);
        System.out.println("Secondi: " + seconds);
        System.out.println("Operazioni: " + count * numberOfRepetition);
        System.out.println("Scritture/letture per secondo: " + ((double)count * numberOfRepetition)/seconds);
    }



}