package com.sensichips.sensiplus.demoapp.winux.test;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolListener;
import com.sensichips.sensiplus.util.batch.SPBatchExperiment;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import jssc.SerialPortList;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class SPWinuxRegisterDump implements SPProtocolListener, Runnable {


    private int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L2;

    public static String LOG_MESSAGE = "SPWinuxRegisterDump";

    private static String SENSIPLUS_INIT = "SENSIPLUS_Init_RUN4.txt";

    private NumberFormat formatter = new DecimalFormat("#0.00");

    private String outDir = ".\\";
    private String COMPort;
    private String initFileDir = ".";

    // High level and low level
    private SPMeasurement dhl;
    private SPProtocol protocol;
    private SPDecoder decoder;


    private static SPWinuxRegisterDump frontEndLevel;

    public static void main(String[] args) throws Exception {

        System.out.println("COM PORT AVAILABLE:");
        String[] portNames = SerialPortList.getPortNames();
        if (portNames.length > 0){
            for(int i = 0; i < portNames.length; i++){
                System.out.println("\t" + (i + 1) + ". " + portNames[i]);
            }
        } else {
            System.out.println("\n\tANY DEVICE FOUND, THE PROGRAM WILL EXIT!!");
            System.exit(-1);
        }
        System.out.println("------------------------------------");

        frontEndLevel = new SPWinuxRegisterDump();

        if (args.length == 0){
            frontEndLevel.initFileDir = "./";
            frontEndLevel.COMPort = "/dev/ttyUSB0";
            frontEndLevel.outDir = ".";
        } else if (args.length == 2) {
            frontEndLevel.initFileDir = args[0];
            frontEndLevel.COMPort = args[1];
            frontEndLevel.outDir = ".";
        } else {
            System.exit(-1);
        }

        System.out.println("PARAMETERS:");
        System.out.println("initFileDir:\t" + frontEndLevel.initFileDir);
        System.out.println("outDir:\t\t\t" + frontEndLevel.outDir);
        System.out.println("COMPort:\t\t" + frontEndLevel.COMPort);
        System.out.println("-------------------------------------");



        //SPConfigurationManager.loadConfigurationsFromFile(new FileInputStream(new File(SPBatchParameters.conf_dir + SPBatchParameters.layoutConfiguration)), null);

        /*
        SPConfigurationManager.setDefaultSPConfiguration(0);
        SPDriver currentDriver = null;
        if (SPConfigurationManager.getSPConfigurationDefault().getDriverName().equals(SPConfiguration.COM_USB)) {
            int BAUDRATE = SerialPort.BAUDRATE_115200;
            if (SPConfigurationManager.getSPConfigurationDefault().getMcu().contains("STM32")){
                BAUDRATE = 460800;//SerialPort.BAUDRATE_256000;
            }
            currentDriver = new SPDriverWINUX_COMUSB(frontEndLevel.COMPort, BAUDRATE);
        }

        frontEndLevel.protocol = new SPProtocolDelegated(currentDriver, frontEndLevel, SPConfigurationManager.getSPConfigurationDefault());
        new Thread(frontEndLevel).start();

        */
        // Initialize decoder from init file (this file define also the protocol (SPI, I2C, etc.)
        //frontEndLevel.decoder = null; //SPInitialization.decoderFactory(new FileInputStream(new File(frontEndLevel.initFileDir + layoutConfiguration)));

        //String controllerName = null;
        //SPDriver spDriver = null;

        // Create low level channel (on serial port)
        //frontEndLevel.protocol = null; //new SPProtocolDelegated(spDriver, frontEndLevel, SPConfigurationManager.getCurrentConfiguration(), SPProtocol.FULL_ADDRESS);
        //SPBatchParameters.protocol.setSPProtocol(controllerName + SPInitialization.getInterfaceType());

    }

    @Override
    public void connectionUp(SPProtocol protocol) {

        /*
        try{
            SPConfiguration conf = SPConfigurationManager.getDefaultConfiguration();
            SPBatchParameters.decoder        = conf.getSPDecoder();
            SPBatchParameters.spMnemonic     = new SPMnemonic(protocol, SPBatchParameters.decoder);

        } catch (SPException e){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during connectionUp(): " + e, DEBUG_LEVEL);
            System.exit(-1);
        }*/
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Connection up, ready to start measurement...", DEBUG_LEVEL);

    }

    @Override
    public void message(String s) {

    }

    @Override
    public void connectionDown() {

    }

    @Override
    public void interrupt(byte b) {

    }

    @Override
    public void fatalError(String s) {

    }


    private boolean generaNuovoFile = true;
    private SPBatchExperiment SPBatchExperiment;

    @Override
    public void run(){

        ArrayList<String> comando = new ArrayList<String>();
        String rispostaAttesa = null;
        try {
            if (protocol.getProtocolName().equals(SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI])){

                rispostaAttesa = "[0x00 0x00 0x10 0x32 0x54 0x06 0x98 0x3A 0xDC 0xFE 0x54 0x76 0x00 0x00 0x98 0xBA 0xDC 0xFE 0x10 0x32 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0xDC 0xFE 0x00 0x00 0x54 0x00 0x98 0x3A 0x1C 0x76 0x00 0x00 0x00 0x00 0x00]";
                comando.add("[0x00 0x00]");
                comando.add("[0x0A 0x10 0x32]");
                comando.add("[0x12 0x54 0x76]");
                comando.add("[0x1A 0x98 0x3A]");
                comando.add("[0x22 0xDC 0xFE]");
                comando.add("[0x2A 0x10 0x32]");
                comando.add("[0x2A 0x54 0x76]");
                comando.add("[0x3A 0x98 0xBA]");
                comando.add("[0x42 0xDC 0xFE]");
                comando.add("[0x4A 0x10 0x32]");
                comando.add("[0x72 0xDC 0xFE]");
                comando.add("[0x7A 0x10 0x32]");
                comando.add("[0x82 0x54 0x76]");
                comando.add("[0x8A 0x98 0xBA]");
                comando.add("[0x92 0xDC 0xFE]");
                comando.add("[0xA2 0x54 0x76]");
                comando.add("[0xB2 0x98 0xBA]");
                comando.add("[0xB2 0xDC 0xFE]");
                comando.add("[0xBA 0x10 0x32]");
                comando.add("[0xE2 0x5A 0x5A]");
                comando.add("[0x03 r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r]");
            } else if (protocol.getProtocolName().equals(SPProtocol.I2C)){
                rispostaAttesa = "[0x08 0x01 0x10 0x32 0x54 0x06 0x18 0x3A 0xDC 0xFE 0x54 0x36 0x00 0x00 0x98 0x02 0x5C 0xFE 0x10 0x32 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0xDC 0xFE 0x00 0x00 0x54 0x02 0x98 0x3A 0x1C 0x7E 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x1A 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00]";
                comando.add("[0x82 0x00 0x00]");
                comando.add("[0x82 0x0A 0x10 0x32]");
                comando.add("[0x82 0x12 0x54 0x76]");
                comando.add("[0x82 0x1A 0x98 0x3A]");
                comando.add("[0x82 0x22 0xDC 0xFE]");
                comando.add("[0x82 0x2A 0x10 0x32]");
                comando.add("[0x82 0x2A 0x54 0x76]");
                comando.add("[0x82 0x3A 0x98 0xBA]");
                comando.add("[0x82 0x42 0xDC 0xFE]");
                comando.add("[0x82 0x4A 0x10 0x32]");
                comando.add("[0x82 0x72 0xDC 0xFE]");
                comando.add("[0x82 0x7A 0x10 0x32]");
                comando.add("[0x82 0x82 0x54 0x76]");
                comando.add("[0x82 0x8A 0x98 0xBA]");
                comando.add("[0x82 0x92 0xDC 0xFE]");
                comando.add("[0x82 0xA2 0x54 0x76]");
                comando.add("[0x82 0xB2 0x98 0xBA]");
                comando.add("[0x82 0xB2 0xDC 0xFE]");
                comando.add("[0x82 0xBA 0x10 0x32]");
                comando.add("[0x82 0xE2 0x5A 0x5A]");
                comando.add("[0x82 0x03");
                comando.add("[0x83 r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r]");

            } else if (protocol.getProtocolName().equals(SPProtocol.SENSIBUS)){
                rispostaAttesa = "[0x03  0x00  0x00  0x10  0x32  0x54  0x06  0x09  0x04  0xDC  0xFE  0x54  0x76  0x00  0x00  0x98  0xBA  0xDC  0xFE  0x10  0x32  0x00  0x00  0x00  0x00  0x00  0x00  0x00  0x00  0xDC  0xFE  0x00  0x00  0x54  0x00  0x98  0x3A  0x1C  0x76  0x00  0x00  0x00  0x00]";
                //comando.add("[0x00 0x00]");

                comando.add("[0x0A 0x10 0x32]");
                comando.add("[0x12 0x54 0x06]");
                comando.add("[0x1A 0x09 0x04]");
                comando.add("[0x22 0xDC 0xFE]");
                comando.add("[0x2A 0x10 0x32]");
                comando.add("[0x2A 0x54 0x76]");
                comando.add("[0x3A 0x98 0xBA]");
                comando.add("[0x42 0xDC 0xFE]");
                comando.add("[0x4A 0x10 0x32]");
                comando.add("[0x72 0xDC 0xFE]");
                comando.add("[0x7A 0x10 0x32]");
                comando.add("[0x82 0x54 0x76]");
                comando.add("[0x8A 0x98 0xBA]");
                comando.add("[0x92 0xDC 0xFE]");
                comando.add("[0xA2 0x54 0x76]");
                comando.add("[0xB2 0x98 0xBA]");
                comando.add("[0xB2 0xDC 0xFE]");
                comando.add("[0xBA 0x10 0x32]");
                comando.add("[0xE2 0x5A 0x5A]");
                comando.add("[0x03 r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r r]");

            }

            long startTime = System.currentTimeMillis();

            String resp1 = null;
            int count = 0;
            int totNumTest = 100;
            for(int rep = 0; rep < totNumTest; rep++) {
                for (int i = 0; i < comando.size(); i++) {
                    SPDecoderGenericInstruction instruction = new SPDecoderGenericInstruction("");
                    instruction.setDecodedInstruction(comando.get(i));
                    resp1 = frontEndLevel.protocol.send(instruction, new SPChip());
                    //resp1 = frontEndLevel.protocol.read();
                    System.out.println("Resp real: " + resp1);
                }
                System.out.println("Resp expe: " + rispostaAttesa);
                if (resp1.replace(" ", "").equals(rispostaAttesa.replace(" ", ""))) {
                    count++;
                    System.out.println("Test: " + resp1.replace(" ", "").equals(rispostaAttesa.replace(" ", "")));
                } else {
                    System.out.println("Test: " + resp1.replace(" ", "").equals(rispostaAttesa.replace(" ", "")));
                }
                System.out.println("Test " + (rep + 1) + " of " + totNumTest);

            }

            long end = System.currentTimeMillis();


            protocol.closeAll();
            System.out.println("--------------------------------------------");
            System.out.println("Tot num of test: " + totNumTest);
            System.out.println("Testo ok: " + 100*(double)count/totNumTest + "%");

            System.out.println("Elapsed time: " + (end-startTime));

        } catch (SPException e) {
            e.printStackTrace();
        }
    }


}
