package com.sensichips.sensiplus.demoapp.winux.test;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.demoapp.winux.gui.ConfigCreator;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMBLE;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_IP;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.protocols.util.SPProtocolOut;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;

import static com.sensichips.sensiplus.config.SPConfigurationManager.getSPConfigurationDefault;
import static com.sensichips.sensiplus.config.SPConfigurationManager.setDefaultSPConfiguration;

public class Test32x implements SPConfigurationManager.SPDriverGenerator {

    @Override
    public String generateDriver(String driverName, ArrayList<String> driverParameters) throws SPDriverException {
        SPDriver sp;
        if (driverName.equals(SPDriverWINUX_COMUSB.DRIVERNAME)){
            sp = new SPDriverWINUX_COMUSB(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_COMBLE.DRIVERNAME)){
            sp = new SPDriverWINUX_COMBLE(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_IP.DRIVERNAME)){
            sp = new SPDriverWINUX_IP(driverParameters);
        } else {
            throw new SPDriverException("Wrong driverName in generateDriver in SPWinuxTools");
        }

        SPDriverManager.add(sp);
        return sp.getDriverKey();
    }

    public static void main(String args[]){

        Test32x test = new Test32x();
        ConfigCreator configCreator = new ConfigCreator(test);
        Hashtable<String, Hashtable<String, Integer>> errorsPerChip = new Hashtable<>();
        Hashtable<String, Integer> errorsPerPattern;

        boolean stopOnErrorSelected = false;


        SPLogger.DEBUG_TH = 0; //SPLogger.DEBUG_TH | SPLoggerInterface.DEBUG_VERBOSITY_L0
        SPLogger.DEBUG_TH |= false ? SPLoggerInterface.DEBUG_VERBOSITY_L0:0;
        SPLogger.DEBUG_TH |= false ? SPLoggerInterface.DEBUG_VERBOSITY_L1:0;
        SPLogger.DEBUG_TH |= false ? SPLoggerInterface.DEBUG_VERBOSITY_L2:0;
        SPLogger.DEBUG_TH |= false ? SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE:0;


        try {
            String SBspeed = "32x";
            boolean failed = false;

            int numTest = 10000;

            int startTk = 0X6E;
            int endTk = 0xFF; //255;
            int incremento = 5;
            int counter = 0;


            SPConfigurationManager.loadConfigurationsFromFile(new FileInputStream(new File("layout_configuration.xml")));

            //SPConfiguration spConfiguration = SPConfigurationManager.setDefaultSPConfiguration(configCreator.getDefaultSPConfigurationName());
            SPConfiguration spConfiguration = SPConfigurationManager.getSPConfigurationDefault();
            spConfiguration.startConfiguration(test,
                    true,
                    true,
                    true,
                    true,
                    true,
                    true,
                    true);
            setDefaultSPConfiguration(spConfiguration);

            SPCluster cluster = getSPConfigurationDefault().getCluster();



            //cluster.setChipAsInactive(cluster.getActiveSPChipList().get(1));
            int numChips = cluster.getActiveSPChipList().size();

            String patterns[] = new String[]{"AA55", "55AA", "0005", "0006", "0007", "0008", "00FF", "FF00"};
            //String patterns[] = new String[]{"0000"};


            PrintWriter pw[] = new PrintWriter[numChips];
            PrintWriter errorLog = new PrintWriter(new FileOutputStream(new File(SBspeed  + "_errors.csv")), true);


            for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                errorsPerPattern = new Hashtable<>();
                for(int m = 0; m < patterns.length; m++){
                    errorsPerPattern.put(patterns[m], 0);
                }
                String address = cluster.getActiveSPChipList().get(k).getAddress(getSPConfigurationDefault().getSPProtocol().getAddressingMode()
                        , getSPConfigurationDefault().getSPProtocol().getProtocolName());
                errorsPerChip.put(address, errorsPerPattern);
                pw[k] = new PrintWriter(new FileOutputStream(new File(address + "-" + SBspeed  + "_errors.csv")), true);
                pw[k].print("tk;");
                for(int m = 0; m < patterns.length; m++){
                    pw[k].print(patterns[m] + ";");
                }
                pw[k].println();
            }

            for(int k = 0; k < cluster.getActiveSPChipList().size(); k++) {
                errorsPerPattern = new Hashtable<>();
                for (int m = 0; m < patterns.length; m++) {
                    errorsPerPattern.put(patterns[m], 0);
                }
            }

            String commandWrite = "WRITE CHA_LIMIT M 0x";
            String commandRead = "READ CHA_LIMIT M 2";

            long elapsedTime = System.currentTimeMillis();
            SPProtocol spProtocol = getSPConfigurationDefault().getSPProtocol();



            int tk = startTk; // 0x46
            while(tk <= endTk){
                tk = tk + incremento;
                counter++;
            }

            byte[] tks = new byte[counter];

            float bitTime = (float)0.00625;
            float offset = (float)0.460;

            tk = startTk;
            for(int i = 0; i < tks.length; i++){
                tks[i] = (byte) tk;
                System.out.println(String.format("%02x", tks[i]).toUpperCase() + " => " + ((int)tks[i] & 0xFF) + (((int)tks[i] & 0xFF)*bitTime + offset) + "[ns]");
                tk = tk + incremento;
            }

            //System.exit(-1);


            String message = "";
            int errorCounter = 0;
            int i;
            SPProtocolOut out = null;
            long startTime = System.currentTimeMillis();

            // REMEMBER: read only test works only with 1 dimensional pattern length.
            boolean readOnlyTest = false;

            if (readOnlyTest && patterns.length != 1){
                throw new SPException("readOnlyTest is possible only with patterns.length = 1!");
            }

            boolean writeOk = false;
            spProtocol.setCacheActivated(false);

            for(int mm = 0; mm < tks.length; mm++){

                for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                    errorsPerPattern = new Hashtable<>();
                    for(int m = 0; m < patterns.length; m++){
                        errorsPerPattern.put(patterns[m], 0);
                    }
                    String address = cluster.getActiveSPChipList().get(k).getAddress(getSPConfigurationDefault().getSPProtocol().getAddressingMode()
                            , getSPConfigurationDefault().getSPProtocol().getProtocolName());
                    errorsPerChip.put(address, errorsPerPattern);
                }




                System.out.print("--------------------------------\n");
                getSPConfigurationDefault().getSPProtocol().setTk32xToDelete(tks[mm]);
                getSPConfigurationDefault().getSPProtocol().setSpeed(SBspeed , null);
                System.out.println("Current zero time for bit 1: " + "0x" + String.format("%02x", tks[mm]).toUpperCase());

                for(i = 0; i < numTest; i++){
                    //readWriteTextArea.appendText("Step: " + (i + 1) + " of " + numTest + "\n");
                    for(int k = 0; k < patterns.length && (!stopOnErrorSelected || !failed); k++){

                        try{
                            // Write operation
                            if (!readOnlyTest || !writeOk){
                                writeOk = true;
                                //System.out.println("One write");
                                out = spProtocol.sendInstruction(commandWrite + patterns[k], null);
                            }
                            // Read operation
                            out = spProtocol.sendInstruction(commandRead, null);
                            for(int j = 0; j < numChips && (!stopOnErrorSelected || !failed); j++){
                                String val = out.recValues[j].replace("[", "").replace("]","").replace("0x", "").replace("0X", "").trim();
                                String tokens[] = val.split(" ");
                                val = "";
                                for(int m = tokens.length - 1; m >= 0; m--){
                                    val += tokens[m];
                                }

                                out.recValues[j].replace("[", "").replace("]","").replace("0x", "").replace("0X", "").replace(" ", "").trim();
                                if (!val.equals(patterns[k])){
                                    errorCounter++;
                                    failed = true;

                                    String address = cluster.getActiveSPChipList().get(j).getAddress(getSPConfigurationDefault().getSPProtocol().getAddressingMode()
                                            , getSPConfigurationDefault().getSPProtocol().getProtocolName());

                                    errorsPerPattern = errorsPerChip.get(address);
                                    errorsPerPattern.put(patterns[k], errorsPerPattern.get(patterns[k]) + 1);
                                    errorsPerChip.put(address, errorsPerPattern);

                                    message = "Test failed on chip n. " + (j + 1) + ", address: " + address + "\n";
                                    message += "After " + (i + 1) + " read\n";
                                    message += "Last instruction sent:" + commandWrite + patterns[k] + "\n";
                                    message += "Last instruction read:" + val + "\n";

                                    errorLog.print(message);
                                }
                            }
                        } catch (SPException e){
                            failed = true;
                            message += "SPException: " + e.getMessage() + "\n";
                            System.out.print(message);
                        }

                    }

                    if ((i + 1) % 100 == 0){
                        message = "Progress: " + (i + 1) + " of " + numTest + " (Errors: " + errorCounter + ", elapsed time: " + (System.currentTimeMillis() - startTime)/1000 + " sec)\n";
                        //getSPConfigurationDefault().totalBlinking();
                        System.out.print(message);
                    }

                }
                /*System.out.print("Timeout distribution histogram (bin dimension " + SPProtocolESP8266_SENSIBUS.STEP_HIST + " [ms]:");
                for(int k = 0; k < SPProtocolESP8266_SENSIBUS.TIMEOUT_DISTRIBUTION.length; k++){
                    if (SPProtocolESP8266_SENSIBUS.TIMEOUT_DISTRIBUTION[k] > 0)
                        System.out.print("elapsed time " + (k + 1) * SPProtocolESP8266_SENSIBUS.STEP_HIST + " [ms] :\t\t" + SPProtocolESP8266_SENSIBUS.TIMEOUT_DISTRIBUTION[k]);
                }*/

                message = "--------------------------------\n";
                message += "Test " + (failed?" failed!\n" : "ok!\n");
                message += "Number of repetition: " + i + "\n";
                message += "Number of errors: " + errorCounter + "\n";
                message += "Patterns:\n";
                for(int k = 0; k < patterns.length; k++){
                    message += commandWrite + patterns[k] + "\n";
                }
                message += "Chips (" + cluster.getActiveSPChipList().size() + "):\n";
                for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                    String address = cluster.getActiveSPChipList().get(k).getAddress(getSPConfigurationDefault().getSPProtocol().getAddressingMode()
                            , getSPConfigurationDefault().getSPProtocol().getProtocolName());
                    errorsPerPattern = errorsPerChip.get(address);
                    message += "address " + address + ":\n";
                    pw[k].print("0x" + (String.format("%02x", tks[mm]).toUpperCase()) + ";");
                    for(int m = 0; m < patterns.length; m++){
                        message += "\t" + patterns[m] + ": " + errorsPerPattern.get(patterns[m]) + "\n";
                        pw[k].print(errorsPerPattern.get(patterns[m]) + ";");
                    }
                    pw[k].println();
                    message += "\n";
                }
            }

            for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                pw[k].close();
            }
            errorLog.close();

            message += "Elapsed time: " + (System.currentTimeMillis() - elapsedTime)/1000 + " seconds\n";
            message += "Lines sent: " + spProtocol.GLOBAL_SENT_COUNTER + "\n";
            message += "--------------------------------\n";
            spConfiguration.stopConfiguration(true);

            System.out.print(message);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(-1);
        
    }
    
}
