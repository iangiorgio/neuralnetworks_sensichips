package com.sensichips.sensiplus.demoapp.winux.test;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.demoapp.winux.gui.ConfigCreator;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMBLE;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_IP;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.protocols.util.SPProtocolOut;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import javafx.application.Platform;
import javafx.fxml.Initializable;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Marco Ferdinandi on 02/03/2018.
 */
public class InstallationTest implements SPConfigurationManager.SPDriverGenerator{




    public static void main(String args[]){
            try {
                Scanner input = new Scanner(System.in);
                SPLogger.DEBUG_TH = SPLoggerInterface.DEBUG_VERBOSITY_L2;
                File file = null;
                SPConfigurationManager.loadConfigurationFromFileWithJAXB(new File(SPBatchParameters.layoutConfigurationFileXML),
                        new File(SPBatchParameters.layoutConfigurationFileXSD));
                //prefs.put("filePath", file.getPath());

                SPConfiguration currentConfiguration = SPConfigurationManager.getListSPConfiguration().get(0);

                SPConfiguration spConfiguration = SPConfigurationManager.getSPConfiguration(currentConfiguration.getConfigurationName());

                InstallationTest installationTest = new InstallationTest();

                boolean stopLoop = false;

                while(!stopLoop){


                    boolean protocolOpened = false;
                    try {
                        try {
                            spConfiguration.generateAndOpenDriver(installationTest);
                            spConfiguration.generateAndOpenProtocol(spConfiguration.getAddressingType());
                            protocolOpened = true;
                        }catch (SPException e){

                            System.out.println("No chips on the cable");
                            protocolOpened = false;

                            SPDelay.delay(10);

                        }
                        if(protocolOpened) {

                            SPChip foundChip = spConfiguration.getSPProtocol().findChipOnTheCable(spConfiguration.getSpInstalledChips());
                            if(foundChip!=null) {
                                spConfiguration.getSPProtocol().blinkLEDOnChip(foundChip, 3000);

                                spConfiguration.getSpInstalledChips().add(foundChip);
                                System.out.println("CHIP FOUND: "+foundChip.getSerialNumber());
                            }
                            else {
                                System.out.println("No new chip found");
                            }
                            spConfiguration.stopConfiguration(false);
                        }



                    }catch (SPProtocolException e){
                        //alertShow("Installation Problems", e);
                    }
                }
            } catch (SPException e) {

                /*alertShow("Installation Problems", e);
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Installation problems: " + e.getMessage(), DEBUG_LEVEL);
                e.printStackTrace();*/
            }


    }




    @Override
    public String generateDriver(String driverName, ArrayList<String> driverParameters) throws SPDriverException {
        SPDriver sp;
        if (driverName.equals(SPDriverWINUX_COMUSB.DRIVERNAME)){
            sp = new SPDriverWINUX_COMUSB(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_COMBLE.DRIVERNAME)){
            sp = new SPDriverWINUX_COMBLE(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_IP.DRIVERNAME)){
            sp = new SPDriverWINUX_IP(driverParameters);
        } else {
            throw new SPDriverException("Wrong driverName in generateDriver in SPWinuxTools");
        }

        SPDriverManager.add(sp);
        return sp.getDriverKey();
    }
}
