package com.sensichips.sensiplus.demoapp.winux.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RabbitSender {

    private String[] chips;
    private String configuration;
    private String[] analiti;


    public RabbitSender(String chips[], String analiti[], String configuration){
        this.chips = chips;
        this.analiti = analiti;
        this.configuration = configuration;
    }

    public void sendRow(String row) throws JsonProcessingException {

        String[] tokens = row.split(";");

        String timeStamp = tokens[0];
        String chip;
        String analita;

        ObjectMapper mapper = new ObjectMapper();


        int counter = 1;
        for(int i = 0; i < analiti.length; i++){
            for(int j = 0; j < chips.length; j++){
                String appo = configuration + "." + chips[j] + "." + analiti[i] + " => payload: " + timeStamp + ", " + tokens[counter++];
                System.out.println(appo);
            }
        }

    }



}


class MeasureToSend{
    String timeStamp;
    String chip;
    String analita;

    public MeasureToSend(String timeStamp, String chip, String analita) {
        this.timeStamp = timeStamp;
        this.chip = chip;
        this.analita = analita;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public String getAnalita() {
        return analita;
    }

    public void setAnalita(String analita) {
        this.analita = analita;
    }
}
