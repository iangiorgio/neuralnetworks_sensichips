package com.sensichips.sensiplus.demoapp.winux.test;

import java.io.*;

public class RabbitFileReader {



    public static void main(String args[]) throws IOException {
        File f = new File("/home/mario/Scrivania/Sensichips/Sensichip-SerialPort/SensiplusWinux/measures/Experiment_17-10-2017_GLOBAL_PortHP.csv");

        BufferedReader in = new BufferedReader(new FileReader(f));

        String chips[] = new String[]{"41", "42", "49", "4A", "5A"};
        String analiti[] = new String[]{"humidity", "temperature"};

        RabbitSender rabbitSender = new RabbitSender(chips, analiti, "configurationName");


        String row = in.readLine();
        while((row = in.readLine()) != null){
            rabbitSender.sendRow(row);
        }

        in.close();


    }



}
