package com.sensichips.sensiplus.demoapp.winux.test;

import java.util.ArrayList;

/**
 * ONLY FOR TEST
 */
public class Main {

    private ArrayList<String> token = new ArrayList<String>();

    public static void main(String args[]){
        Main m = new Main();
        String command2 = "";
        String command = "[0x00 0x01]";
        command2 += command;
        m.accodaComando(command);

        command = "[0xAA 0x01 0x02]";
        m.accodaComando(command);
        command2 += command;

        command = "[0x00 0x01 0x02 0x03]";
        m.accodaComando(command);
        command2 += command;

        System.out.println(command2 + "\n");
        System.out.println("\n" + m.tokenToWaitInSPIComposed(command2));

        System.out.println(command + "\n");
        System.out.println("\n" + m.tokenToWaitInSPIComposed(command));
    }

    private int tokenToWaitInSPIComposed(String comando){
        int tot = 0;
        String[] elenco = comando.split("]");
        for(int i = 0; i < elenco.length; i++){
            tot += tokenToWaitInSPI(elenco[i]);
        }
        return tot;
    }
    private int tokenToWaitInSPI(String comando){
        // First row: spi>[...]
        // Second row: /CS ENABLED
        // ...
        // Last row: /CS DISABLED
        int rowToIgnore = 3;
        return comando.replace("[", "").replace("]", "").trim().replace("0x", "").split(" ").length + rowToIgnore;
    }

    private void accodaComando(String comando){
        String t[] = comando.replace("[", "").replace("]", "").trim().split(" ");
        for(int i = 0; i < t.length; i++){
            token.add(t[i].trim());
        }
    }

    private String getComando(){
        String out = "[";
        for(int i = 0; i < token.size(); i++){
            out += token.get(i) + " ";
        }
        out = out.trim() + "]";
        token = new ArrayList<String>();
        return out;
    }

}
