package com.sensichips.sensiplus.demoapp.winux.test;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sun.security.krb5.internal.PAData;

import java.io.*;
import java.net.Socket;

/**
 * Created by Marco Ferdinandi on 16/04/2018.
 */
public class TestSensiplusWinuxJSON {


    public static void main(String args[]){
        try {

            Socket socket = new Socket("localhost", 9999);
            socket.setSoTimeout(100000);

            PrintWriter printWriter;
            BufferedReader bufferedReader;

            printWriter = new PrintWriter(socket.getOutputStream(), true);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            JSONParser parser = new JSONParser();
            JSONObject obj = null;
            String response;

            /*--------------------------------------------------------------------------------------------------------*/
            /*---------------------------------EXIT-------------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/
            obj = new JSONObject();
            obj.put("state", "exit");
            printWriter.println(obj);
            System.out.println("Sent: "+obj);

            //MESSAGE RECEIVED
            response = bufferedReader.readLine();
            System.out.println("Received: " + response);

            /*--------------------------------------------------------------------------------------------------------*/
            /*---------------------------------STARTUP----------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/

            //MESSAGE SENT
            obj = new JSONObject();
            obj.put("state", "start");
            obj.put("configuration", "WINUX_COMUSB-CLUSTERID_0X02-FT232RL_SENSIBUS-ShortAddress-ALL_CHIP(RUN5)");
            printWriter.println(obj);
            System.out.println("Sent: "+obj);

            //MESSAGE RECEIVED
            response = bufferedReader.readLine();
            System.out.println("Received: " + response);
            /*--------------------------------------------------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/




            /*--------------------------------------------------------------------------------------------------------*/
            /*---------------------------------SET EIS----------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/

            //MESSAGE SENT
            obj = new JSONObject();
            obj.put("state", "setEIS");
            obj.put("measure", "CAPACITANCE");
            obj.put("frequency", "78125.00");
            obj.put("rsense", "auto");
            obj.put("ingain", "auto");
            obj.put("outgain", "7");
            obj.put("dcbiasP", "0");
            obj.put("dcbiasN", "0");
            obj.put("dcbiasP", "0");
            obj.put("contacts", "TWO");
            obj.put("harmonic", "FIRST_HARMONIC");
            obj.put("modevi", "VOUT_IIN");
            obj.put("inport", "PORT_HP");
            obj.put("outport", "PORT_HP");
            obj.put("filter", "1");
            obj.put("phaseShift", "0");
            obj.put("phaseShiftMode", "Quadrants");
            obj.put("IQ", "IN_PHASE");
            printWriter.println(obj);
            System.out.println("Sent: "+obj);

            //MESSAGE RECEIVED
            response = bufferedReader.readLine();
            System.out.println("Received: " + response);
            /*--------------------------------------------------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/



            /*--------------------------------------------------------------------------------------------------------*/
            /*---------------------------------GET EIS----------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/

            //MESSAGE SENT
            obj = new JSONObject();
            obj.put("state", "getEIS");
            int cont = 0;
            while( cont <10) {
                printWriter.println(obj);
                System.out.println("Sent: " + obj);

                //MESSAGE RECEIVED
                response = bufferedReader.readLine();
                System.out.println("Received: " + response);
                cont++;
            }


            /*--------------------------------------------------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/
            /*--------------------------------------------------------------------------------------------------------*/

            obj = new JSONObject();
            obj.put("state", "exit");
            printWriter.println(obj);


            socket.close();

        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
