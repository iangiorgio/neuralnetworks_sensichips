package com.sensichips.sensiplus.demoapp.winux.gui.utility;

public interface ConfigConnectionListener {

    int OPEN = 0;
    int CLOSE = 1;

    void open(String info);
    void close(String info);

}
