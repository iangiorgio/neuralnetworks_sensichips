package com.sensichips.sensiplus.demoapp.winux.gui;


import com.sensichips.sensiplus.model.Family;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class FamilyEditController {

    @FXML
    private TextField idspfamilyField;
    @FXML
    private TextField idField;
    @FXML
    private TextField hwVersionField;
    @FXML
    private TextField sysClockField;
    @FXML
    private TextField osctrimField;
    @FXML
    private TextField multicastDefaultField;
    @FXML
    private TextField broadcastField;
    @FXML
    private TextField broadcastSlowField;

    private Stage stage;
    private boolean check = false;
    private boolean ok = false;
    private Family family;

    private void setListner(TextField textField, String regex, String replacement) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            textField.setText(newValue.replaceAll(regex, replacement));
        });
    }


    public void setStage(Stage stage) {
        this.stage = stage;
    }


    public void setFamily(Family family) {
        this.family = family;

        idspfamilyField.setText(family.getIdSPFamily());

        idField.setText(family.getId());
        hwVersionField.setText(family.getHwVersion());
        sysClockField.setText(family.getSysclock());
        osctrimField.setText(family.getOsctrim());
        multicastDefaultField.setText(family.getMulticastDefault());
        broadcastField.setText(family.getBroadcast());
        broadcastSlowField.setText(family.getBroadcastSlow());
    }

    @FXML
    public void initialize() {
        //setListner(idspfamilyField, "[^\\d]", "");
        //insriamo un ascoltatore nella TextField idspfamilyField, il quale ascolta cosa inseriamo. Nel caso
        //inseriamo un carattere, esso viene sostituito con uno "". Facciamo ciò per non permettere al cliente di
        //inserire una stringa nel campo.

    }

    @FXML
    public void okButtonAction() {
        if (checkField(check)) {
            family.setIdSPFamily(idspfamilyField.getText());
            family.setId(idField.getText());

            family.setHwVersion(hwVersionField.getText());
            family.setOsctrim(osctrimField.getText());
            family.setSysclock(sysClockField.getText());
            family.setMulticastDefault(multicastDefaultField.getText());
            family.setBroadcast(broadcastField.getText());
            family.setBroadcastSlow(broadcastSlowField.getText());

            ok = true;
            stage.close();
        } else {
            ok = false;
        }
    }

    private boolean checkField(boolean check) {
        String error = "";

        if (check) {

            error += findError(idspfamilyField, "IdSPFamily");
            error += findError(idField, "Id");
            error += findError(hwVersionField, "HwVersion");
            error += findError(sysClockField, "SysClock");
            error += findError(osctrimField, "Osctrim");
            error += findError(multicastDefaultField,"MulticastDefault");
            error += findError(broadcastField, "Broadcast");
            error += findError(broadcastSlowField, "BroadcastSlow");


            if (error.length() == 0) {
                return true;
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(stage);
                alert.setTitle("Invalid fields");
                alert.setHeaderText("Please correct invalid fields");
                alert.setContentText(error);

                alert.showAndWait();

                return false;
            }
        } else {
            return true;
        }
    }


    @FXML
    public void cancelButtonAction() {
        stage.close();
    }

    public boolean okClicked() {
        return ok;
    }


    private String findError(TextField textField, String fieldName) {
        String error = "";

        if (textField.getText().equals("")) {
            error += fieldName + " can not be empty.\n";
        }

        return error;
    }
}

