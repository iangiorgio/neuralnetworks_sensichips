package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.datareader.queue.SPChartQueue;
import com.sensichips.sensiplus.datareader.queue.SPMainQueue;

import com.sensichips.sensiplus.demoapp.winux.gui.utility.Stats;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.TableStats;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.batch.SPBatchExperiment;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;
import com.sun.javafx.charts.Legend;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.awt.*;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Random;

import static com.sensichips.sensiplus.config.SPConfigurationManager.getSPConfigurationDefault;

/**
 * Created by mario on 19/02/17.
 */
public class Chart extends LineChart<Number, Number> {
    private NumberFormat formatter = new DecimalFormat("#0.000");
    private ChartSettings chartSettings;
    private ArrayList<Series<Number,Number>> grandezza;
    private NumberAxis xAxis;

    public NumberAxis getyAxis() {
        return yAxis;
    }

    private NumberAxis yAxis;

    private int typeOfMeasure = 0;

    private double time = 0;

    private double xStart, yStart;

    private double SCALE_DELTA = 1.1;

    private boolean firstValue = true;

    private double lowerTime;
    private double upperTime;


    private SPChartQueue source;

    private int indexToSave;

    //private SPBatchExperiment spBatchExperiment;

    private boolean[] chipMask;
    private TableStats tableStats;

    private TableStats newTableStats = null;

    private SPMainQueue mq;

    public static Color colorList[] = {Color.RED,Color.ORANGE,Color.DARKBLUE,Color.GREEN,Color.BLACK,Color.INDIGO,Color.MAGENTA,Color.MEDIUMSLATEBLUE,Color.SALMON,Color.YELLOWGREEN,Color.VIOLET,
                                Color.SLATEGRAY,Color.SIENNA,Color.SADDLEBROWN,Color.PINK,Color.OLIVE,Color.LIGHTSLATEGRAY,Color.LIGHTCORAL,Color.HOTPINK,Color.GOLDENROD,Color.GOLD,Color.DEEPPINK,
                                Color.DARKSEAGREEN,Color.CYAN,Color.AQUA,Color.AQUAMARINE,Color.BROWN};

    private int indexSeries = -1;

    /**
     * Construct a new LineChart with the given axis.
     *
     * @param xAxis  The x axis to use
     * @param yAxis The y axis to use
     */
    private Chart(SPMainQueue mq, Axis<Number> xAxis, Axis<Number> yAxis, int plotDeep, int typeOfMeasure,
                  ChartSettings chartSettings, boolean chipMask[],int indexToSave,int statsDepth) throws SPException {
        super(xAxis, yAxis);
        this.xAxis = (NumberAxis)xAxis;
        this.yAxis = (NumberAxis)yAxis;
        this.typeOfMeasure = typeOfMeasure;
        this.chartSettings = chartSettings;
        this.indexToSave = indexToSave;
        this.chipMask = chipMask;
        this.mq = mq;

        if(chartSettings.typeOfMeasure == ChartSettings.YX_CHART)
            indexSeries = 0;

        source = new SPChartQueue(plotDeep, true, chipMask,statsDepth);
        mq.subscribe(source);

        init();
    }


    private Chart(SPMainQueue mq, Axis<Number> xAxis, Axis<Number> yAxis, int plotDeep, int typeOfMeasure,
                  ChartSettings chartSettings, boolean chipMask[], int indexToSave, TableStats tab,int statsDepth) throws SPException {
        super(xAxis, yAxis);
        this.xAxis = (NumberAxis)xAxis;
        this.yAxis = (NumberAxis)yAxis;
        this.typeOfMeasure = typeOfMeasure;
        this.chartSettings = chartSettings;
        this.indexToSave = indexToSave;
        this.chipMask = chipMask;
        this.mq = mq;
        tableStats = tab;

        source = new SPChartQueue(plotDeep, true, chipMask,statsDepth);
        mq.subscribe(source);

        init();


        for (Node n : this.getChildrenUnmodifiable()) {
            if (n instanceof Legend) {
                Legend l = (Legend) n;
                for (Legend.LegendItem li : l.getItems()) {
                    for (XYChart.Series<Number, Number> s : this.getData()) {
                        if (s.getName().equals(li.getText())) {
                            li.getSymbol().setCursor(Cursor.HAND); // Hint user that legend symbol is clickable
                            li.getSymbol().setOnMouseClicked(me -> {
                                System.out.println(li.getText());
                                chipsSelectedForStats.add(li.getText());
                                //tableStats.get().getItems().get(0).

                                for(int i=0; i<tableStats.get().getItems().size(); i++){
                                    if(tableStats.get().getItems().get(i).getChipID().equals(li.getText())){
                                        tableStats.get().requestFocus();
                                        tableStats.get().getSelectionModel().select(i);
                                        tableStats.get().getFocusModel().focus(i);
                                    }
                                }



                                /*Platform.runLater(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        tableStats.get().requestFocus();
                                        tableStats.get().getSelectionModel().select(0);
                                        tableStats.get().getFocusModel().focus(0);
                                    }
                                });*/



                                /*if (me.getButton() == MouseButton.PRIMARY) {
                                    s.getNode().setVisible(!s.getNode().isVisible()); // Toggle visibility of line
                                    for (XYChart.Data<Number, Number> d : s.getData()) {
                                        if (d.getNode() != null) {
                                            d.getNode().setVisible(s.getNode().isVisible()); // Toggle visibility of every node in the series
                                        }
                                    }
                                }*/
                            });
                            break;
                        }
                    }
                }
            }
        }
    }

    static void generateColorsList(int numOfElements){


        //Color c = new Color(255,0,0,1);
        Field fields[] = Color.class.getFields();
        String fieldsString[] = new String[fields.length];
        for (int i=0; i<numOfElements; i++){
            fieldsString[i]= fields[i+1].toString();
        }

    }

    public ChartSettings getChartSettings() {
        return chartSettings;
    }

    public void setChartSettings(ChartSettings chartSettings) {
        this.chartSettings = chartSettings;
    }

    public static Chart createNewChart(SPMainQueue mq, ChartSettings chartSettings, boolean chipMask[], int indexToSave,int statsDepth)throws SPException{
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRanging(true);
        return new Chart(mq, xAxis,yAxis, chartSettings.plotDeep, chartSettings.typeOfMeasure, chartSettings, chipMask, indexToSave,statsDepth);
    }

    public static Chart createNewChart(SPMainQueue mq, ChartSettings chartSettings, boolean chipMask[], int indexToSave,
                                       TableStats tableStats,int statsDepth)throws SPException{
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRanging(true);
        return new Chart(mq, xAxis,yAxis, chartSettings.plotDeep, chartSettings.typeOfMeasure, chartSettings, chipMask,
                indexToSave, tableStats,statsDepth);
    }

    private String mean[];
    private String std[];

    private void evaluateStats(SPChartElement element){
        mean = new String[element.getyValues().length];
        std = new String[element.getyValues().length];


        for(int i = 0; i < element.getyValues().length; i++){

            mean[i] = "" + element.getyValues()[i][indexToSave];

        }

    }

    private ArrayList<String> chipsSelectedForStats = new ArrayList<String>();

    private void updateGUI(){

        if (tableStats == null){
            return;
        }

        tableStats.clear();



        try{

            double[][] stats = source.getStatistics(indexToSave);
            /*Stats[] rows = new Stats[chipsSelectedForStats.size()];
            int cont=0;
            for(int i=0; i<getSPConfigurationDefault().getCluster().getActiveSPChipList().size(); i++){
                if(chipsSelectedForStats.contains(getSPConfigurationDefault().getCluster().getActiveSPChipList().get(i).getSerialNumber())){
                    rows[cont] = new Stats(getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().get(i), "" +
                            formatter.format(mq.getMin(i, indexToSave)), "" +
                            formatter.format(mq.getMax(i, indexToSave)),
                            "" +formatter.format(stats[i][SPChartQueue.MAX_SNR_INDEX]),
                            "" + formatter.format(stats[i][SPChartQueue.NOISE_DEV_STD_INDEX]),
                            " " + formatter.format(stats[i][SPChartQueue.SNR_INDEX]));
                }
            }*/


            Stats[] rows = new Stats[stats.length];

            for(int i = 0; i < stats.length; i++){
                rows[i] = new Stats(getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().get(i), "" +
                        formatter.format(mq.getMin(i, indexToSave)), "" +
                        formatter.format(mq.getMax(i, indexToSave)),
                        "" +formatter.format(stats[i][SPChartQueue.MAX_SNR_INDEX]),
                        "" + formatter.format(stats[i][SPChartQueue.NOISE_DEV_STD_INDEX]),
                        " " + formatter.format(stats[i][SPChartQueue.SNR_INDEX]));
            }

            tableStats.addStats(rows);
        } catch(SPException spe){
            spe.printStackTrace();
        }
    }





    protected synchronized void plot(SPChartElement element){

        upperTime = element.getxValue();

        if (firstValue){
            firstValue = false;
            lowerTime = element.getxValue();
        }

        //System.out.println("upperTime - lowerTime: " + (upperTime - lowerTime));
        if ((upperTime - lowerTime) >= (chartSettings.plotDeep - 1)) {
            for(int i = 0; i < grandezza.size(); i++){
                if (grandezza.get(i).getData().size() > 1){
                    grandezza.get(i).getData().remove(0,1);
                    lowerTime = grandezza.get(i).getData().get(0).getXValue().doubleValue();
                }
            }
        }

        int numOfSeries = chartSettings.numberOfChips;
        if(chartSettings.typeOfMeasure == ChartSettings.YX_CHART)
            numOfSeries = chartSettings.numberOfPOTCycle;


        for(int i = 0; i < element.getyValues().length; i++){



            if(element.isResetChart()) {

                indexSeries++;


                for (int k=0; k<indexSeries; k++) {
                    if(k==indexSeries-1 && k!=0){
                        grandezza.get(k).getNode().setStyle("-fx-stroke:#686868");
                    }else if(k==0){
                        grandezza.get(k).getNode().setStyle("-fx-stroke:#000000");
                    } else{
                        grandezza.get(k).getNode().setStyle("-fx-stroke:#" +
                                Color.web(grandezza.get(k).getNode().getStyle().trim().split("#")[1]).
                                        deriveColor(0,1,1.2,1)
                                        .toString().substring(2,8));
                    }
                }

            }

            int ind = chartSettings.typeOfMeasure==ChartSettings.YX_CHART ? indexSeries : i;

            /*Data<Number,Number> d = new Data<>(element.getxValue(),
                    element.getSpDataRepresentation().values[i][indexToSave]);

            Tooltip.install(d.getNode(), new Tooltip("Ciao"));*/

            //Adding class on hover
            //d.getNode().setOnMouseEntered(event -> d.getNode().getStyleClass().add("onHover"));

            //Removing class on exit
            //d.getNode().setOnMouseExited(event -> d.getNode().getStyleClass().remove("onHover"));

            grandezza.get(ind).getData().add(new Data<Number,Number>(element.getxValue(),
                    element.getSpDataRepresentation().values[i][indexToSave]));

        }
    }




    public static void main(String [] args){

        //Chart.generateColorsList(10);

        String style = "-fx-stroke:#" + Integer.toHexString(colorList[0].hashCode());
        Color c = Color.web(style.trim().split("#")[1]);
        c.brighter();
        System.out.println(style);


    }




    public void init()throws SPException{
        setId("lineStockDemo");
        setCreateSymbols(false);
        setAnimated(false);
        setLegendVisible(true);
        setTitle(chartSettings.title);

        if (chartSettings.xLabel.equals("")){
            xAxis.setLabel("Time [s]");
        }else{
            xAxis.setLabel(chartSettings.xLabel + " ["+chartSettings.unitOfMeasureXAxis+"]");
        }


        xAxis.setForceZeroInRange(false);
        yAxis.setForceZeroInRange(false);

        yAxis.setLabel("[" + chartSettings.unitOfMeasureYAxis + "]");
        //yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(yAxis,"[V]",null));
        // add starting data

        grandezza = new ArrayList<>();

        int numOfSeries = chartSettings.numberOfChips;
        if(chartSettings.typeOfMeasure == ChartSettings.YX_CHART){
            setLegendVisible(false);
            numOfSeries = chartSettings.numberOfPOTCycle;
        }



        /*if(chartSettings.typeOfMeasure == ChartSettings.YX_CHART){
            colorList = new Color[numOfSeries];
            for(int i =0; i<numOfSeries; i++){

            }
        }*/

        for(int i = 0; i < numOfSeries; i++){
            Series<Number,Number> series = new Series<>();
            if(chartSettings.typeOfMeasure != ChartSettings.YX_CHART)
                series.setName(getSPConfigurationDefault().getCluster().getActiveSPChipList().get(i).getSerialNumber());
            grandezza.add(series);
            //grandezza.get(i).setName("To specify");

            getData().add(series);

            series.getNode().setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.println("Series clicked: "+series.getName());

                    /*Stage stage = new Stage();

                    Text text = new Text();
                    text.setText("CIAOOO");
                    text.setX(50);
                    text.setY(50);
                    Group root = new Group(text);
                    Scene newScene = new Scene(root, 200, 200);

                    stage.setScene(newScene);
                    stage.show();*/

                }
            });


            series.getNode().setOnMouseEntered(onMouseEnteredSeriesListener);
            series.getNode().setOnMouseExited(onMouseExitedSeriesListener);
            //String style = "-fx-stroke:"+colorList[i];
            //String style = "-fx-stroke:"+Color.GREEN;
            //String style = "-fx-stroke:#2f577a";
            String style = "";

            if(chartSettings.typeOfMeasure == ChartSettings.YX_CHART){
                style = "-fx-stroke:#" + Color.RED.toString().substring(2,8);
                series.getNode().setStyle(style);
            }else {
                if (i >= colorList.length) {
                    Field fields[] = Color.class.getFields();
                    Random rand = new Random();
                    style = "-fx-stroke:#" + fields[rand.nextInt(fields.length - 1)].toString().substring(2,8);
                } else {
                    style = "-fx-stroke:#" + colorList[i].toString().substring(2,8);
                }
                series.getNode().setStyle(style);
            }

        }

        new Thread(new Consumer()).start();

        setOnScroll(onMouseScrolled);
        setOnMousePressed(onMousePressed);
        setOnMouseDragged(onMouseDragged);

    }


    public void unsubscribe(){
        mq.unSubscribe(source);
    }



    class Consumer extends Task<Void> {
        private boolean first = true;
        private SPChartElement element;
        boolean toConsume = true;

        @Override
        public Void call(){

            try {
                String rcvd = "";
                toConsume = true;
                System.out.println("Consumer started ...");
                while (true){


                    element = source.get(indexToSave);
                    toConsume = true;

                    //for(int i = 0; i < element.getyValues().length; i++){
                    //    System.out.println("Global max chip [" + (i + 1) + "]: " + mq.getMax(i, indexToSave));
                    //    System.out.println("Global min chip [" + (i + 1) + "]: " + mq.getMin(i, indexToSave));
                    //}

                    Platform.runLater(() -> {
                        if(first) {
                            first = false;
                            //getyAxis().setLabel("[" + element.getSpDataRepresentation().prefix + getChartSettings().unitOfMeasureYAxis + "]");
                            //if(chartSettings.derivativeChart)
                                //getyAxis().setLabel("[" + element.getSpDataRepresentation().prefix + element.getSpDataRepresentation().measureUnit  + "/"+chartSettings.unitOfMeasureXAxis+"]");
                            //else
                                getyAxis().setLabel("[" + element.getSpDataRepresentation().prefix + element.getSpDataRepresentation().measureUnit  + "]");
                        }
                        plot(element);
                        updateGUI();
                        time++;

                        toConsume = false;
                    });
                    while(toConsume)
                        SPDelay.delay(1);
                }

            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }

            return null;
        }
    };



    EventHandler<MouseEvent> onMouseEnteredSeriesListener =
            (MouseEvent event) -> {
                ((Node)(event.getSource())).setCursor(Cursor.HAND);
            };

    //Lambda expression
    EventHandler<MouseEvent> onMouseExitedSeriesListener =
            (MouseEvent event) -> {
                ((Node)(event.getSource())).setCursor(Cursor.DEFAULT);
            };
    //Lambda expression
    EventHandler<MouseEvent> onMousePressed =
            (MouseEvent event) -> {
                if (event.getClickCount() == 2) {
                    setScaleX(1.0);
                    setScaleY(1.0);
                }

                System.out.println("Pressed ... (" + event.getX() + ", " + event.getY() + ")");
                xStart = event.getX();
                yStart = event.getY();
            };

    //Lambda expression
    EventHandler<ScrollEvent> onMouseScrolled =
            (ScrollEvent event) -> {
        event.consume();
        if (event.getDeltaY() == 0) {
            return;
        }

        double scaleFactor = (event.getDeltaY() > 0) ? SCALE_DELTA : 1 / SCALE_DELTA;

        setScaleX(getScaleX() * scaleFactor);
        setScaleY(getScaleY() * scaleFactor);
    };

    EventHandler<MouseEvent> onMouseDragged =
            (MouseEvent event) -> {
                event.consume();

                System.out.println("Dragged: " + (xStart - event.getX()) + ", " + (yStart - event.getY()));
            };
}
