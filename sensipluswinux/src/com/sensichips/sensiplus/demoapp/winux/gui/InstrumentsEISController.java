package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.SPDataReader;
import com.sensichips.sensiplus.datareader.SPDataReaderConf;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.ConfigConnectionListener;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.TabApiImpSpecStatusElem;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.UtilityGUI;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.chip.SPPort;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementMetaParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterADC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.items.*;
import com.sensichips.sensiplus.util.SPDataRepresentationManager;
import com.sensichips.sensiplus.util.log.SPLogger;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.StringConverter;

import javax.swing.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.function.UnaryOperator;
import java.util.prefs.Preferences;


public class InstrumentsEISController implements TabControllerInterface, ConfigConnectionListener {

    public static int DEBUG_LEVEL = 0;
    private static String LOG_MESSAGE = "InstrumentsEISController";

    ObservableList<String> contactsList;
    ObservableList<String> measureList;
    ObservableList<String> portStimulusList;
    ObservableList<String> gainStimulusList;
    ObservableList<String> portMeasurementList;
    ObservableList<String> gainMeasurementList;
    ObservableList<String> filterList;
    ObservableList<String> rSenseList;
    ObservableList<String> harmonicList;
    ObservableList<String> phaseShiftModeList;

    float stepCurrent = SPMeasurement.AvailablePhaseShift.stepQ;
    int phaseShiftModPrev = SPParamItemPhaseShiftMode.QUADRANT;
    int phaseShiftMod = SPParamItemPhaseShiftMode.QUADRANT;
    UnaryOperator<TextFormatter.Change> filter = t -> {
        //System.out.println(t);
        if (t.isReplaced()) {
            if (t.getText().matches("[^0-9]")) {
                t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
            }
        }

        if (t.isAdded()) {
            if (t.getControlText().contains(".") && t.getText().equals(".")) {
                t.setText("");
            } else if (!t.getText().equals(".") && t.getText().matches("[^0-9]")) {
                t.setText("");
            }
        }
        return t;
    };

    //@FXML
    //private Label imp_spec_meas_PhaseShiftlabelVal;
    private Controller mainController;
    @FXML
    private Label imp_spec_meas_PhaseShiftlabelMin;
    @FXML
    private Label imp_spec_meas_PhaseShiftlabelMax;
    @FXML
    private Slider phaseSlider;
    @FXML
    private TextField phaseField;
    @FXML
    private ChoiceBox phaseShiftMode;
    @FXML
    private AnchorPane tabEIS;
    private float phaseShiftMin = 0;
    private float phaseShiftMax = 270;
    private float phaseShiftVal = 0;
    private int phaseShiftPos = 0;
    private int Quadrant = 0;

    @FXML
    private TextField plotDeep;

    @FXML
    private Label totMeasure;

    @FXML
    private Label measForSec;

    //private float stepCurrentCoarse = SPMeasurement.AvailablePhaseShift.stepQ;
    //private float stepCurrentFine = SPMeasurement.AvailablePhaseShift.stepQ;
    //private int phaseShiftModPrev = SPMeasurementParameterEIS.QUADRANT;
    //private int phaseShiftMod = SPMeasurementParameterEIS.QUADRANT;
    private int imp_spec_meas_labelMax_Phaseshift = 31;
    private int currentMinValue = 0;
    private float FRDfr;
    private NumberFormat formatter = new DecimalFormat("#0.00");
    private double k = 10;
    private float progress;
    private SPMeasurement.AvailableFrequencies fc = new SPMeasurement.AvailableFrequencies();
    private float selectedFrequency;
    private boolean programmaticallyChanged = true;


    //end phaseShift declaration
    @FXML
    private Slider frequencySlider;

    @FXML
    private Slider dcBiasPSlider;

    @FXML
    private Slider dcBiasNSlider;

    @FXML
    private TextField frequencyField;

    @FXML
    private TextField dcBiasPField;

    @FXML
    private TextField dcBiasNField;


    @FXML
    private ChoiceBox Contacts;

    @FXML
    private ChoiceBox Measure;

    @FXML
    private ChoiceBox portStimulus;

    @FXML
    private ChoiceBox gainStimulus;

    @FXML
    private ChoiceBox portMeasurement;

    @FXML
    private ChoiceBox gainMeasurement;

    @FXML
    private ChoiceBox Filter;

    @FXML
    private ChoiceBox rSense;

    @FXML
    private ChoiceBox Harmonic;

    @FXML
    private ToggleButton viStimulus;

    @FXML
    private CheckBox sequentialMode;

    @FXML
    private ToggleButton viMeasurement;


    @FXML
    private Button setDefault;

    @FXML
    private Button startMeasure;

    @FXML
    private Button plotButton;

    @FXML
    private Button autoButton;


    @FXML
    private Button setParametersDefault;


    private InstrumentsEISController setParam;

    private boolean alreadyInitialized = false;
    private SPMeasurementParameterEIS defaultParam;

    public InstrumentsEISController() {
    }

    public void setMainController(Controller mainController) {
        this.mainController = mainController;
        this.mainController.addConfigConnectionListener(this);
    }

    @Override
    public void initTab() {


    }

    private SPDataReader spDataReader;



    private Chart lineChart;
    private int indexToSave;

    @FXML
    private void startMeasureClick(ActionEvent e){

        try {

            if (startMeasure.getText().equals("Start measure")){


                SPMeasurement spMeasurement = SPConfigurationManager.getSPConfigurationDefault().getSPMeasurement("EIS");
                SPMeasurementOutput message = new SPMeasurementOutput();


                SPMeasurementParameterEIS param = getParameterFromUI().param;
                if (SPConfigurationManager.getSPConfigurationDefault().getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)){
                    param.setFIFO_DEEP(1);
                    param.setFIFO_READ(1);
                } else {
                    param.setFIFO_DEEP(3);
                    param.setFIFO_READ(1);
                }

                if(param.getFrequency()<=50.0 && param.getFrequency()!=0){

                    SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().setADCdelay(
                            new Long(Math.round(10+1000*4*1/param.getFrequency())));
                    param.setConversion_Rate(new Double(param.getFrequency()));
                }else{
                    SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().setADCdelay(new Long(Math.round(1000*4*1/50.0)));
                    param.setConversion_Rate(50.0);
                }




                spMeasurement.setEIS(param, message);

                spMeasurement.setADC(param.getFIFO_DEEP(), param.getFIFO_READ(), param.getInGainLabel(), param.getConversion_Rate(), SPMeasurementParameterADC.INPORT_IA, true, message);


                boolean timePlot = true;
                int delay = 1;
                int numOfMeasure = 9;
                float spaceMin = Float.MAX_VALUE;
                float spaceMax = Float.MIN_VALUE;
                float threshold = spaceMin + (spaceMax - spaceMin)/2;
                int dimSpaceQueue = 1;
                int dimTimeQueue = 1000;
                boolean plotAutoRange = true;
                int valueToShow = param.getMeasure();
                int numTempMeasure = SPDataReader.TEMPMEASURE_STOP;
                String measure_unit = "";
                indexToSave = valueToShow;

                SPDataRepresentationManager spDataRepresentationManager = new SPDataRepresentationManager(valueToShow);

                if (valueToShow == SPParamItemMeasures.CAPACITANCE){
                    spDataRepresentationManager.setMeasureUnit("F");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("1.0");
                    spDataRepresentationManager.setInitial_multiplier("-12");
                    spDataRepresentationManager.setDefault_alarm_threshold("0.5");
                } else if (valueToShow == SPParamItemMeasures.RESISTANCE){
                    spDataRepresentationManager.setMeasureUnit("R");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("1000.0");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("1000");
                } else if (valueToShow == SPParamItemMeasures.INDUCTANCE){
                    spDataRepresentationManager.setMeasureUnit("H");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("1.0");
                    spDataRepresentationManager.setInitial_multiplier("-3");
                    spDataRepresentationManager.setDefault_alarm_threshold("1.0");
                } else if (valueToShow == SPMeasurementParameterEIS.IN_PHASE){
                    spDataRepresentationManager.setMeasureUnit("O");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("33000");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("15000");
                } else if (valueToShow == SPMeasurementParameterEIS.QUADRATURE){
                    spDataRepresentationManager.setMeasureUnit("rad");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax(Math.PI/2+"");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold(Math.PI/4+"");
                } else if (valueToShow == SPParamItemMeasures.MODULE){
                    spDataRepresentationManager.setMeasureUnit("O");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("1000.0");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("500");

                } else if (valueToShow == SPParamItemMeasures.PHASE){
                    spDataRepresentationManager.setMeasureUnit("rad");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax(""+(Math.PI/2));
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold(Math.PI/4+"");

                }else if (valueToShow == SPParamItemMeasures.CONDUCTANCE){
                    spDataRepresentationManager.setMeasureUnit("S");
                    spDataRepresentationManager.setRangeMin("1E-8");
                    spDataRepresentationManager.setRangeMax("1E-7");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("0.6E-7");

                } else if (valueToShow == SPParamItemMeasures.SUSCEPTANCE){
                    spDataRepresentationManager.setMeasureUnit("S");
                    spDataRepresentationManager.setRangeMin("10E-9");
                    spDataRepresentationManager.setRangeMax("60E-9");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("50E-9");
                }


                boolean reset = true;
                boolean restartDataReader = reset;


                SPMeasurementMetaParameter meta = new SPMeasurementMetaParameter(SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size());
                param.setFillBufferBeforeStart(false);
                param.setBurstMode(false);
                //meta.setFillBufferBeforeStart(false);
                //meta.setBurst(1);
                meta.setRenewBuffer(true);

                String measureName =  SPFamily.MEASURE_TYPES_AVAILABLE[SPFamily.MEASURE_TYPE_EIS];

                SPDataReaderConf spDataReaderConf = new SPDataReaderConf(param, SPConfigurationManager.getSPConfigurationDefault(), meta,
                        timePlot, delay, dimSpaceQueue, dimTimeQueue, restartDataReader, numOfMeasure, plotAutoRange,
                        "t", "", Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, valueToShow, numTempMeasure, measure_unit,
                        measureName,spDataRepresentationManager);

                spDataReaderConf.setTimePlot(true);
                spDataReader = new SPDataReader(spDataReaderConf, spMeasurement);
                spDataReader.setPauseReader(false);

                GUIStatus.InstrumentsEISRunning = true;
                startMeasure.setText("Stop");
                plotButton.setDisable(false);
                autoButton.setDisable(true);


            } else {
                spDataReader.stopReader();
                startMeasure.setText("Start measure");
                plotButton.setDisable(true);
                autoButton.setDisable(false);
            }


        } catch (SPException e1) {
            Controller.alertShow("ERROR", e1);
        }

    }

    Runnable updateStatusBar = new Runnable() {
        @Override
        public void run() {
            while(true){

                if (spDataReader != null){
                    Platform.runLater(() -> {
                        measForSec.setText("" + spDataReader.getMeasForSec());
                        totMeasure.setText("" + spDataReader.getTotMeasure());
                    });
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    };

    @FXML
    private void plotButtonClick(ActionEvent e){

        int dimQueue = 500;

        try{
            dimQueue = Integer.parseInt(plotDeep.getText());
        } catch (Exception ex){
            Controller.alertShow("Plot deep error: " + plotDeep.getText(), ex);
        }


        int numOfChips = 0;
        try {
            numOfChips = SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size();

            int graphWinWidth = 800;
            int graphWinHeight = 600;

            boolean [] chipMask = new boolean[numOfChips];
            for(int i = 0; i < chipMask.length; i++){
                chipMask[i] = true;
            }

            ChartSettings chartSettings = new ChartSettings();
            chartSettings.plotDeep = dimQueue;
            chartSettings.lowerY = 0;
            chartSettings.upperY = 1;
            chartSettings.typeOfMeasure = ChartSettings.YT_CHART;
            chartSettings.title = SPParamItemMeasures.measureLabels[indexToSave];
            chartSettings.unitOfMeasureYAxis = SPParamItemMeasures.unitMeasuresLabels[indexToSave];

            chartSettings.numberOfChips = numOfChips;

            lineChart = Chart.createNewChart(spDataReader.getSPMainQueue(), chartSettings,
                    chipMask,
                    indexToSave,16);

            HBox hb = new HBox();
            hb.getChildren().add(lineChart);
            hb.setFillHeight(true);

            VBox vb = new VBox();
            vb.setFillWidth(true);
            vb.getChildren().add(hb);

            HBox.setHgrow(lineChart, Priority.ALWAYS);
            VBox.setVgrow(hb, Priority.ALWAYS);

            Scene newScene = new Scene(vb, graphWinWidth, graphWinHeight);

            Stage stage = new Stage();
            stage.setTitle("EIS");
            stage.setScene(newScene);

            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    lineChart.unsubscribe();
                    //stageList[currentIndex + indexOffset] = null;
                    //closeStage(currentIndex);
                    stage.close();
                    System.out.println("lineChart unsubscribed...");
                }
            });
            stage.show();
        } catch (SPException e1) {
            Controller.alertShow("ERROR", e1);
        }

    }




    @FXML
    private void initialize() {

        viStimulus.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (viStimulus.isSelected() == true)
                    viStimulus.setText("I");
                else viStimulus.setText("V");
            }
        });
        viMeasurement.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (viMeasurement.isSelected() == true)
                    viMeasurement.setText("I");
                else viMeasurement.setText("V");
            }
        });

        dcBiasPField.textProperty().bindBidirectional(dcBiasPSlider.valueProperty(), NumberFormat.getIntegerInstance());
        dcBiasNField.textProperty().bindBidirectional(dcBiasNSlider.valueProperty(), NumberFormat.getIntegerInstance());

        phaseSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {


                phaseShiftPos = newValue.intValue();
                if (phaseShiftMod == SPParamItemPhaseShiftMode.QUADRANT) {
                    Quadrant = phaseShiftPos / 8;
                    phaseShiftVal = Quadrant * stepCurrent;
                } else if (phaseShiftMod == SPParamItemPhaseShiftMode.COARSE) {
                    phaseShiftVal = phaseShiftPos * stepCurrent + phaseShiftMin;
                } else if (phaseShiftMod == SPParamItemPhaseShiftMode.FINE) {
                    phaseShiftVal = phaseShiftPos * stepCurrent + phaseShiftMin;
                }
                phaseShiftSectionUpdate();

            }

        });


        phaseShiftMode.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //System.out.println(phaseShiftMode.getItems().get((Integer) newValue));
                phaseShiftMod = (int) newValue;

                phaseShiftSectionValueUpdate();
                phaseShiftSectionUpdate();

                if (!programmaticallyChanged) {
                    phaseSlider.setValue(phaseShiftPos);
                }
                phaseShiftModPrev = phaseShiftMod;
                programmaticallyChanged = false;

            }
        });

        frequencyField.setTextFormatter(new TextFormatter<Object>(filter));
        frequencyField.textProperty().bindBidirectional(frequencySlider.valueProperty(), new MyFrequencyConverter());


        frequencyField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    System.out.println("Textfield out focus");
                    float frequencyRequested = (float) Float.parseFloat(frequencyField.getText());
                    try {
                        SPMeasurement.AvailableFrequencies.availableFrequencies(SPConfigurationManager.getSPConfigurationDefault(), (float) frequencyRequested, fc);
                    } catch (SPException e) {
                        e.printStackTrace();
                    }
                    NumberFormat formatter = new DecimalFormat("#0.00");
                    frequencyField.setText(formatter.format(fc.availableFrequency).replace(",", "."));
                    evaluateStepPhaseShift();

                }
            }
        });


    }

    private void evaluateStepPhaseShift() {
        SPMeasurement.AvailableFrequencies fc = new SPMeasurement.AvailableFrequencies();
        try {
            SPMeasurement.AvailableFrequencies.availableFrequencies(SPConfigurationManager.getSPConfigurationDefault(), (float) selectedFrequency, fc);
        } catch (SPException e) {
            e.printStackTrace();
        }
        SPMeasurement.AvailablePhaseShift.updateSteps(fc);

        phaseShiftSectionValueUpdate();
        phaseSlider.setValue(phaseShiftPos);
        phaseShiftSectionUpdate();

    }

    private void phaseShiftSectionValueUpdate() {
        if (phaseShiftMod == SPParamItemPhaseShiftMode.QUADRANT) {
            stepCurrent = SPMeasurement.AvailablePhaseShift.stepQ;
            phaseShiftMin = 0;
            phaseShiftMax = 270;
            phaseShiftPos = 8 * Quadrant;
            phaseShiftVal = Quadrant * stepCurrent;

        } else if (phaseShiftMod == SPParamItemPhaseShiftMode.COARSE) {
            stepCurrent = SPMeasurement.AvailablePhaseShift.stepC;
            if (phaseShiftModPrev == SPParamItemPhaseShiftMode.QUADRANT) {
                phaseShiftPos = 0;
            }

            phaseShiftMin = Quadrant * SPMeasurement.AvailablePhaseShift.stepQ;
            phaseShiftMax = phaseShiftMin + stepCurrent * imp_spec_meas_labelMax_Phaseshift;
            phaseShiftVal = phaseShiftMin + phaseShiftPos * stepCurrent;

        } else {
            stepCurrent = SPMeasurement.AvailablePhaseShift.stepF;
            if (phaseShiftModPrev == SPParamItemPhaseShiftMode.QUADRANT) {
                phaseShiftPos = 0;
            }

            phaseShiftMin = Quadrant * SPMeasurement.AvailablePhaseShift.stepQ;
            phaseShiftMax = phaseShiftMin + stepCurrent * imp_spec_meas_labelMax_Phaseshift;
            phaseShiftVal = phaseShiftMin + phaseShiftPos * stepCurrent;

        }

    }

    private void phaseShiftSectionUpdate() {
        NumberFormat formatter = new DecimalFormat("#0.0");
        imp_spec_meas_PhaseShiftlabelMin.setText(formatter.format(phaseShiftMin) + "°");
        imp_spec_meas_PhaseShiftlabelMax.setText(formatter.format(phaseShiftMax) + "°");
        phaseField.setText(formatter.format(phaseShiftVal) + "°");

    }


    private SPParamItemContacts spParamItemContacts;
    private SPParamItemMeasures spParamItemMeasures;
    private SPParamItemRSense   spParamItemRSense;
    private SPParamItemHarmonic spParamItemHarmonic;
    private SPParamItemInGain spParamItemInGain;
    private SPParamItemOutGain spParamItemOutGain;
    private SPParamItemPhaseShiftMode spParamItemPhaseShiftMode;
    private SPParamItemFilter spParamItemFilter;

    @Override
    public void open(String info) {
        System.out.println("InstrumentsEISController received opening connection info");
        tabEIS.setDisable(false);
        startMeasure.setDisable(false);


        try {

            SPFamily spFamily = SPConfigurationManager.getSPConfigurationDefault().getCluster().getFamilyOfChips();

            spParamItemContacts         = new SPParamItemContacts(spFamily);
            spParamItemMeasures         = new SPParamItemMeasures(spFamily);
            spParamItemRSense           = new SPParamItemRSense(spFamily);
            spParamItemHarmonic         = new SPParamItemHarmonic(spFamily);
            spParamItemInGain           = new SPParamItemInGain(spFamily);
            spParamItemOutGain          = new SPParamItemOutGain(spFamily);
            spParamItemPhaseShiftMode   = new SPParamItemPhaseShiftMode(spFamily);
            spParamItemFilter           = new SPParamItemFilter(spFamily);

            contactsList = FXCollections.observableArrayList(SPParamItemContacts.ContactsLabels);//"TWO", "FOUR");
            measureList = FXCollections.observableArrayList(SPParamItemMeasures.measureLabels);  //"IN-PHASE", "QUADRATURE", "CONDUCTANCE", "SUSCEPTANCE", "MODULE", "PHASE", "RESISTANCE", "CAPACITANCE", "INDUCTANCE");
            rSenseList = FXCollections.observableArrayList(spParamItemRSense.rsenseLabels);      //"50000", "5000", "500", "50");
            harmonicList = FXCollections.observableArrayList(spParamItemHarmonic.harmonicLabels); //"FIRST_HARMONIC", "SECOND_HARMONIC", "THIRD_HARMONIC");
            gainMeasurementList = FXCollections.observableArrayList(spParamItemInGain.ingainLabels);//"1", "12", "20", "40");
            portStimulusList = FXCollections.observableArrayList("PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3");
            gainStimulusList = FXCollections.observableArrayList(spParamItemOutGain.outgainLabels); //"0", "1", "2", "3", "4", "5", "6", "7");
            portMeasurementList = FXCollections.observableArrayList("PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3");
            filterList = FXCollections.observableArrayList(spParamItemFilter.filterLabels); //"1", "4", "8", "16", "32", "64");
            phaseShiftModeList = FXCollections.observableArrayList(spParamItemPhaseShiftMode.PhaseShiftModeLabels); //"Quadrants", "Coarse", "Fine");

            Measure.setItems(measureList);
            Contacts.setItems(contactsList);
            portStimulus.setItems(portStimulusList);
            gainStimulus.setItems(gainStimulusList);
            portMeasurement.setItems(portMeasurementList);
            gainMeasurement.setItems(gainMeasurementList);
            Filter.setItems(filterList);
            rSense.setItems(rSenseList);
            Harmonic.setItems(harmonicList);
            phaseShiftMode.setItems(phaseShiftModeList);

            setParameterToUI(createDefaultParam());
        } catch (SPException e) {
            SPLogger.getLogInterface().d("InstrumentsEISController", "Default configuration not available. Please open a connection", DEBUG_LEVEL);
        }

        if (!alreadyInitialized) {

            //System.out.println("Try to initialize EIS");
            try {
                setParameterToUI(createDefaultParam());
                alreadyInitialized = true;
            } catch (SPException e) {
                SPLogger.getLogInterface().d("InstrumentsEISController", "Default configuration not available. Please open a connection", DEBUG_LEVEL);
            }
            new Thread(updateStatusBar).start();
        } else {
            System.out.println("EIS already initialized.");

        }

    }

    @Override
    public void close(String info) {
        System.out.println("InstrumentsEISController received closing connection info");
        tabEIS.setDisable(true);

    }

    @FXML
    public void setParametersChoice() {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        JOptionPane.showConfirmDialog(null, "Are you sure to set this parameters?", "WARNING", dialogButton);
        if (dialogButton == JOptionPane.YES_OPTION) {
            String defaultParam = null;
            defaultParam =
                    Contacts.getValue().toString() + "/" +
                            Measure.getValue().toString() + "/" +
                            portStimulus.getValue().toString() + "/" +
                            gainStimulus.getValue().toString() + "/" +
                            frequencyField.getText() + "/" +
                            dcBiasPField.getText() + "/";

            String modeVI = "";
            if (viStimulus.getText().equals("V") && viMeasurement.getText().equals("V")) {
                modeVI = "VOUT_VIN";
            } else if (viStimulus.getText().equals("V") && viMeasurement.getText().equals("I")) {
                modeVI = "VOUT_IIN";
            } else if (viStimulus.getText().equals("I") && viMeasurement.getText().equals("I")) {
                modeVI = "IOUT_IIN";
            } else {
                modeVI = "IOUT_VIN";
            }

            defaultParam += modeVI + "/" +
                    dcBiasNField.getText() + "/" +
                    portMeasurement.getValue().toString() + "/" +
                    gainMeasurement.getValue().toString() + "/" +
                    Filter.getValue().toString() + "/" +
                    rSense.getValue().toString() + "/" +
                    Harmonic.getValue().toString() + "/" +
                    phaseShiftMode.getValue().toString() + "/" +
                    phaseField.getText() + "/" +
                    sequentialMode.isSelected();
            System.out.println(defaultParam);
            Preferences prefs2 = Preferences.userNodeForPackage(InstrumentsEISController.class);
            prefs2.put("default values", defaultParam);

        } else if (dialogButton == JOptionPane.NO_OPTION) {

        }
    }

    private SPMeasurementParameterEIS createDefaultParam() throws SPException {

        try {
            defaultParam = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());

            Preferences prefs = Preferences.userNodeForPackage(InstrumentsEISController.class);
            String defaultValues = prefs.get("default values", "");
            if (defaultValues == "") {

                defaultParam = createBasicDefault();

            } else {

                try{
                    String parameter[] = defaultValues.split("/");

                    defaultParam.setContacts(parameter[0]);
                    defaultParam.setMeasure(parameter[1]);
                    // Stimulus
                    defaultParam.setOutPort(parameter[2]);
                    defaultParam.setOutGain(parameter[3]);

                    defaultParam.setFrequency(Float.parseFloat(parameter[4].replace(",", ".")));

                    defaultParam.setDCBiasP(Integer.parseInt(parameter[5].replace(".", "")));
                    defaultParam.setModeVI(parameter[6]);
                    if (parameter[6].equals("VOUT_VIN")) {
                        viStimulus.setSelected(false);
                        viMeasurement.setSelected(false);
                    } else if (parameter[6].equals("VOUT_IIN")) {
                        viStimulus.setSelected(false);
                        viMeasurement.setSelected(true);
                    } else if (parameter[6].equals("IOUT_IIN")) {
                        viStimulus.setSelected(true);
                        viMeasurement.setSelected(true);
                    } else {
                        viStimulus.setSelected(true);
                        viMeasurement.setSelected(false);
                    }


                    defaultParam.setDCBiasN(Integer.parseInt(parameter[7].replace(".", "")));
                    // Measurement
                    defaultParam.setInPort(parameter[8]);
                    defaultParam.setInGain(parameter[9]);

                    defaultParam.setFilter(parameter[10]);
                    defaultParam.setRsense(parameter[11]);
                    defaultParam.setHarmonic(parameter[12]);

                    defaultParam.setPhaseShiftQuadrants(SPParamItemPhaseShiftMode.PhaseShiftModeLabels[0], 0, SPParamItemQI.I_QLabels[0]);
                    defaultParam.setSequentialMode(parameter[15].equalsIgnoreCase("true"));
                } catch (Exception e){
                    e.printStackTrace();
                    defaultParam = createBasicDefault();
                }
            }

        } catch (SPException e) {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during default loading!\n" + e.getMessage() + "\n", DEBUG_LEVEL);
            throw e;
        }


        return defaultParam;
    }

    private SPMeasurementParameterEIS createBasicDefault() throws SPException{

        defaultParam = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());

        defaultParam.setRsense(SPParamItemRSense.rsenseLabels[1]);
        defaultParam.setContacts(SPParamItemContacts.ContactsLabels[0]);
        defaultParam.setMeasure(SPParamItemMeasures.measureLabels[6]);
        // Stimulus
        defaultParam.setOutPort(SPPort.portLabels[5]);
        defaultParam.setOutGain(SPParamItemOutGain.outgainLabels[7]);
        defaultParam.setModeVI(SPParamItemModeVI.ModeVILabels[0]);
        defaultParam.setFrequency((float) 78125);

        defaultParam.setDCBiasP(0);
        defaultParam.setDCBiasN(0);
        // Measurement
        defaultParam.setInPort(SPPort.portLabels[12]);
        defaultParam.setInGain(SPParamItemInGain.ingainLabels[1]);
        defaultParam.setModeVI(SPParamItemModeVI.ModeVILabels[1]);

        defaultParam.setHarmonic(SPParamItemHarmonic.harmonicLabels[0]);
        defaultParam.setFilter(SPParamItemFilter.filterLabels[0]);
        defaultParam.setPhaseShiftQuadrants(SPParamItemPhaseShiftMode.PhaseShiftModeLabels[0], 0, SPParamItemQI.I_QLabels[0]);
        defaultParam.setSequentialMode(false);

        return defaultParam;
    }


    private void setParameterToUI(SPMeasurementParameterEIS spMeasurementParameterEIS) throws SPException {

        //SPFamily spFamily = SPConfigurationManager.getSPConfigurationDefault().getCluster().getFamilyOfChips();

        Contacts.setValue(spParamItemContacts.setValue(spMeasurementParameterEIS.getContacts()).getLabel());
        //Contacts.setValue(SPParamItemContacts.ContactsLabels[UtilityGUI.searchVector(SPParamItemContacts.ContactsValues, spMeasurementParameterEIS.getContacts())]);

        Measure.setValue(spParamItemMeasures.setValue(spMeasurementParameterEIS.getMeasure()).getLabel());
        //Measure.setValue(SPParamItemMeasures.measureLabels[UtilityGUI.searchVector(SPParamItemMeasures.measureValues, spMeasurementParameterEIS.getMeasure())]);

        rSense.setValue(spParamItemRSense.setValue(spMeasurementParameterEIS.getRsense()).getLabel());
        //rSense.setValue(SPMeasurementParameterEIS.rsenseLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.rsenseValues, spMeasurementParameterEIS.getRsense())]);

        gainMeasurement.setValue(spParamItemInGain.setValue(spMeasurementParameterEIS.getInGain()).getLabel());
        //gainMeasurement.setValue(SPMeasurementParameterEIS.ingainLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.ingainValues, spMeasurementParameterEIS.getInGain())]);

        Harmonic.setValue(spParamItemHarmonic.setValue(spMeasurementParameterEIS.getHarmonic()).getLabel());
        //Harmonic.setValue(SPMeasurementParameterEIS.harmonicLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.harmonicValues, spMeasurementParameterEIS.getHarmonic())]);

        portStimulus.setValue(SPPort.portLabels[UtilityGUI.searchVector(SPPort.portValues, spMeasurementParameterEIS.getOutPort())]);


        gainStimulus.setValue(spParamItemOutGain.setValue(spMeasurementParameterEIS.getOutGain()).getLabel());
        //gainStimulus.setValue(SPMeasurementParameterEIS.outgainLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.outgainValues, spMeasurementParameterEIS.getOutGain())]);


        //Filter.setValue(SPParamItemFilter.filterLabels[UtilityGUI.searchVector(SPParamItemFilter.filterValues, spMeasurementParameterEIS.getFilter())]);
        Filter.setValue(spParamItemFilter.setValue(spMeasurementParameterEIS.getFilter()).getLabel());

        //phaseShiftMode.setValue(SPParamItemPhaseShiftMode.PhaseShiftModeLabels[UtilityGUI.searchVector(SPParamItemPhaseShiftMode.PhaseShiftModeValues, spMeasurementParameterEIS.getPhaseShiftMode())]);
        phaseShiftMode.setValue(spParamItemPhaseShiftMode.setValue(spMeasurementParameterEIS.getPhaseShiftMode()).getLabel());



        frequencySlider.setValue(Math.log10(spMeasurementParameterEIS.getFrequency()) * 10);
        frequencySlider.valueProperty().setValue(Math.log10(spMeasurementParameterEIS.getFrequency()) * 10);
        dcBiasPSlider.setValue(spMeasurementParameterEIS.getDCBiasP());
        dcBiasNSlider.setValue(spMeasurementParameterEIS.getDCBiasN());
        portMeasurement.setValue(SPPort.portLabels[UtilityGUI.searchVector(SPPort.portValues, spMeasurementParameterEIS.getInPort())]);
        sequentialMode.setSelected(spMeasurementParameterEIS.isSequentialMode());

    }

    private TabApiImpSpecStatusElem getParameterFromUI() throws SPException {

        TabApiImpSpecStatusElem paraGUI = new TabApiImpSpecStatusElem();

        paraGUI.param = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());

        //paraGUI = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());
        paraGUI.param.setContacts(Contacts.getValue().toString());
        paraGUI.param.setMeasure(Measure.getValue().toString());
        paraGUI.param.setRsense(rSense.getValue().toString());
        // Stimulus
        paraGUI.param.setOutPort(portStimulus.getValue().toString());
        paraGUI.param.setOutGain(gainStimulus.getValue().toString());

        // Seleziono la frequenza mostrata all'utente (non quella riportata dallo slider)
        paraGUI.param.setFrequency(Float.parseFloat(frequencyField.getText().toString().replace(",", ".")));
        //paraGUI.paramPPR.setFrequency(selectedFrequency);

        paraGUI.param.setFrequency((float) Math.pow(10, progress / k)); // Per salvare lo stato
        paraGUI.param.setDCBiasP(Integer.parseInt(dcBiasPField.getText().toString()));
        paraGUI.param.setDCBiasN(Integer.parseInt(dcBiasNField.getText().toString()));
        paraGUI.param.setDCBiasP((int) dcBiasPSlider.getValue());
        paraGUI.param.setDCBiasN((int) dcBiasNSlider.getValue());


        paraGUI.DCBiasPProgress = Integer.parseInt(dcBiasPField.getText().toString());
        paraGUI.DCBiasNProgress = Integer.parseInt(dcBiasNField.getText().toString());
        paraGUI.phaseShiftMin = phaseShiftMin;
        paraGUI.phaseShiftMax = phaseShiftMax;
        paraGUI.phaseShiftPos = phaseShiftPos;
        paraGUI.Quadrant = Quadrant;


        // Measurement
        paraGUI.param.setFilter(Filter.getValue().toString());
        paraGUI.param.setInPort(portMeasurement.getValue().toString());
        paraGUI.param.setInGain(gainMeasurement.getValue().toString());
        String modeVI_IN = viMeasurement.isSelected()? "IIN" : "VIN";
        String modeVI_OUT = viStimulus.isSelected()? "IOUT" : "VOUT";

        paraGUI.param.setModeVI(modeVI_OUT + "_" + modeVI_IN);


        paraGUI.param.setHarmonic(Harmonic.getValue().toString());

        paraGUI.param.setPhaseShiftQuadrants(phaseShiftMode.getValue().toString(), (int)phaseSlider.getValue(), SPParamItemQI.I_QLabels[Quadrant]);

        paraGUI.param.setSequentialMode(sequentialMode.isSelected());

        return paraGUI;
    }

    @FXML
    private void setParametersDefaultOnAction() throws SPException {

        setParameterToUI(createDefaultParam());

    }

    class MyFrequencyConverter extends StringConverter<Number> {

        /**
         * Converts the object provided into its string form.
         * Format of the returned string is defined by the specific converter.
         *
         * @param object
         * @return a string representation of the object passed in.
         */
        @Override
        public String toString(Number object) {

            String output = "0";
            progress = object.floatValue(); //frequencySlider.getValue();

            if (progress != 0) {
                selectedFrequency = (float) Math.pow(10, progress / k);
            } else {
                selectedFrequency = 0;
            }

            try {
                SPMeasurement.AvailableFrequencies.availableFrequencies(SPConfigurationManager.getSPConfigurationDefault(), (float) selectedFrequency, fc);
                NumberFormat formatter = new DecimalFormat("#0.00");
                output = formatter.format(fc.availableFrequency);
                output = output.replace(",", ".");
                evaluateStepPhaseShift();

            } catch (SPException e) {
                System.out.println("SPConfigurationDefault() not available!");
            }
            //frequencyField.setText(formatter.format(fc.availableFrequency));

            //System.out.println("toString. From: " + object + " to " + output);

            return output;
        }

        /**
         * Converts the string provided into an object defined by the specific converter.
         * Format of the string and type of the resulting object is defined by the specific converter.
         *
         * @param string
         * @return an object representation of the string passed in.
         */
        @Override
        public Number fromString(String string) {
            string = string.replace(",", ".");
            float value = Float.parseFloat(string);

            if (value == 0) {
                progress = 0;
            } else {
                value = (float) Math.log10(value);
                progress = Math.round(value * k);
            }
            //System.out.println("Current position of frequency slider: " + frequencySlider.getValue());
            //System.out.println("To Number from: " + string + " to " + progress);
            evaluateStepPhaseShift();
            return progress;
        }
    }


}


