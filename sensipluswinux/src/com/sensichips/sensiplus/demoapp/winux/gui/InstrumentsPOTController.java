package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.SPDataReader;
import com.sensichips.sensiplus.datareader.SPDataReaderConf;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.ConfigConnectionListener;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.UtilityGUI;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.chip.SPPort;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementMetaParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPOT;
import com.sensichips.sensiplus.level2.parameters.items.SPParamItemPOTType;
import com.sensichips.sensiplus.util.SPDataRepresentationManager;
import com.sensichips.sensiplus.util.log.SPLogger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import javax.swing.*;
import java.text.NumberFormat;
import java.util.function.UnaryOperator;
import java.util.prefs.Preferences;

public class InstrumentsPOTController implements ConfigConnectionListener, TabControllerInterface {

    public static int DEBUG_LEVEL = 0;

    private Controller mainController;
    private boolean alreadyInitialized = false;


    private static String LOG_MESSAGE = "InstrumentsPOTController";
    ObservableList<String> PortList = FXCollections.observableArrayList("PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3","PORT_HP");
    ObservableList<String> TypeList = FXCollections.observableArrayList("LINEAR_SWEEP", "STAIRCASE", "SQUAREWAVE", "NORMAL_PULSE", "DIFFERENTIAL_PULSE", "POTENTIOMETRIC", "CURRENT");
    ObservableList<String> RSenseList = FXCollections.observableArrayList("50", "500", "5000", "50000");
    ObservableList<String> InGainList = FXCollections.observableArrayList("1", "12", "20", "40");
    ObservableList<String> ContactsList = FXCollections.observableArrayList("TWO", "THREE");


    @FXML
    private ChoiceBox Port;

    @FXML
    private ChoiceBox Type;

    @FXML
    private ChoiceBox RSense;

    @FXML
    private ChoiceBox InGain;

    @FXML
    private ChoiceBox Contacts;

    @FXML
    private Slider InitialPotSlider;

    @FXML
    private Label InitialPot_labelMin;

    @FXML
    private Label InitialPot_labelMax;

    @FXML
    private Slider FinalPotSlider;

    @FXML
    private Slider StepSlider;

    @FXML
    private Slider AmplitudeSlider;

    @FXML
    private Slider PeriodSlider;

    @FXML
    private TextField InitialPotField;

    @FXML
    private TextField FinalPotField;

    @FXML
    private TextField StepField;

    @FXML
    private TextField AmplitudeField;

    @FXML
    private TextField PeriodField;

    @FXML
    private Button DefaultSettings;

    @FXML
    private Button SaveSettings;

    @FXML
    private Button LoadSettings;


    //@FXML
    //private Button Plot;

    //@FXML
    //private TextArea TextArea;

    @FXML
    private CheckBox alternativeSignalCheckBox;


    private SPMeasurementParameterPOT defaultParam;

    @FXML
    private AnchorPane tabPOT;

    //@FXML
    //private TextArea logTextArea;

    @FXML
    private Button startMeasure;

    @FXML
    private Button plotButton;

    @FXML
    private Button autoButton;


    UnaryOperator<TextFormatter.Change> filter = new UnaryOperator<TextFormatter.Change>() {
        @Override
        public TextFormatter.Change apply(TextFormatter.Change t) {
            if (t.isReplaced()) {
                if ((t.getText().equals("-"))){
                    t.setText("-");
                }
                else if ((t.getText().equals("."))) {
                    t.setText(".");
                }
                else if (t.getText().matches("[^0-9]")) {
                    t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
                }
            }

            if (t.isAdded()) {
                if (t.getControlText().contains(".") && t.getText().equals(".")) {
                    t.setText("");
                } else if (t.getControlText().contains("-") && t.getText().equals("-")){
                    t.setText("");
                }
                else if ((t.getText().equals("-"))){
                    t.setText("-");
                }
                else if ((!t.getText().equals(".") && t.getText().matches("[^0-9]"))) {
                    t.setText("");
                }


            }
            return t;
        }
    };


    @FXML
    private void initialize() {
        Port.setItems(PortList);

        Type.setItems(TypeList);
        RSense.setItems(RSenseList);
        InGain.setItems(InGainList);
        Contacts.setItems(ContactsList);

        // CONSTRAINS (CIRO)
        Type.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

            InitialPotSlider.setDisable(false);
            InitialPotField.setDisable(false);
            FinalPotSlider.setDisable(false);
            FinalPotField.setDisable(false);
            StepSlider.setDisable(false);
            StepField.setDisable(false);
            AmplitudeSlider.setDisable(false);
            AmplitudeField.setDisable(false);
            PeriodSlider.setDisable(false);
            PeriodField.setDisable(false);

            if(Type.getValue()=="CURRENT" || Type.getValue()=="POTENTIOMETRIC") {
                InitialPotSlider.setDisable(true);
                InitialPotField.setDisable(true);
                FinalPotSlider.setDisable(true);
                FinalPotField.setDisable(true);
                StepSlider.setDisable(true);
                StepField.setDisable(true);
                AmplitudeSlider.setDisable(true);
                AmplitudeField.setDisable(true);
                PeriodSlider.setDisable(true);
                PeriodField.setDisable(true);

            } else if(Type.getValue()=="LINEAR_SWEEP"){
                StepSlider.setDisable(true);
                StepField.setDisable(true);
                AmplitudeSlider.setDisable(true);
                AmplitudeField.setDisable(true);
                PeriodSlider.setDisable(true);
                PeriodField.setDisable(true);

            } else if(Type.getValue()=="STAIRCASE"){
                AmplitudeSlider.setDisable(true);
                AmplitudeField.setDisable(true);

            } else if(Type.getValue()=="SQUAREWAVE"){
                StepSlider.setValue(0);
                StepSlider.setDisable(true);
                StepField.setDisable(true);

            }
        });

        InitialPotSlider.valueProperty().addListener((observable, oldValue, newValue) -> {

            if(InitialPotSlider.getValue()> FinalPotSlider.getValue()){
                FinalPotSlider.setValue(InitialPotSlider.getValue());
            }
        });

        FinalPotSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                                Number oldValue, Number newValue) {
                if(FinalPotSlider.getValue()< InitialPotSlider.getValue()){
                    InitialPotSlider.setValue(FinalPotSlider.getValue());
                }

            }
        });

        InitialPotField.setTextFormatter(new TextFormatter<Object>(filter));
        InitialPotField.textProperty().bindBidirectional(InitialPotSlider.valueProperty(), NumberFormat.getIntegerInstance());


        FinalPotField.setTextFormatter(new TextFormatter<Object>(filter));
        FinalPotField.textProperty().bindBidirectional(FinalPotSlider.valueProperty(), NumberFormat.getIntegerInstance());

        StepField.setTextFormatter(new TextFormatter<Object>(filter));
        StepField.textProperty().bindBidirectional(StepSlider.valueProperty(), NumberFormat.getIntegerInstance());

        AmplitudeField.setTextFormatter(new TextFormatter<Object>(filter));
        AmplitudeField.textProperty().bindBidirectional(AmplitudeSlider.valueProperty(), NumberFormat.getIntegerInstance());

        PeriodField.setTextFormatter(new TextFormatter<Object>(filter));
        PeriodField.textProperty().bindBidirectional(PeriodSlider.valueProperty(), NumberFormat.getIntegerInstance());
    }




    public void setMainController(Controller mainController) {
        this.mainController = mainController;
        this.mainController.addConfigConnectionListener(this);
    }


    private SPDataReader spDataReader;

    private Chart lineChart;
    private int indexToSave;

    @FXML
    private void startMeasureClick(ActionEvent e){
        try {
            if (startMeasure.getText().equals("Start measure")){

                SPMeasurementParameterPOT spMeasurementParameterPOT = new SPMeasurementParameterPOT(SPConfigurationManager.getSPConfigurationDefault());

                spMeasurementParameterPOT.setInGain(InGain.getValue().toString());
                spMeasurementParameterPOT.setInitialPotential((int)InitialPotSlider.getValue());
                spMeasurementParameterPOT.setFinalPotential((int)FinalPotSlider.getValue());
                spMeasurementParameterPOT.setType(Type.getValue().toString());
                spMeasurementParameterPOT.setRsense(RSense.getValue().toString());
                spMeasurementParameterPOT.setPulseAmplitude((int)AmplitudeSlider.getValue());
                spMeasurementParameterPOT.setPulsePeriod((int)PeriodSlider.getValue());
                spMeasurementParameterPOT.setContacts(Contacts.getValue().toString());
                spMeasurementParameterPOT.setPort(Port.getValue().toString());
                spMeasurementParameterPOT.setStep((int)StepSlider.getValue());
                spMeasurementParameterPOT.setAlternativeSignal(alternativeSignalCheckBox.isSelected());

                //logTextArea.appendText(spMeasurementParameterPOT.toString());

                System.out.println(spMeasurementParameterPOT.toString());


                SPMeasurement spMeasurement = SPConfigurationManager.getSPConfigurationDefault().getSPMeasurement("POT");
                SPMeasurementOutput message = new SPMeasurementOutput();

                spMeasurement.setPOT(spMeasurementParameterPOT, message);

                boolean timePlot = true;
                int delay = 1;
                int numOfMeasure = 2;
                float spaceMin = -10;
                float spaceMax = 10;
                float threshold = spaceMin + (spaceMax - spaceMin);
                int dimSpaceQueue = 1;
                int dimTimeQueue = 500;
                boolean plotAutoRange = true;
                int valueToShow = 1;
                int numTempMeasure = SPDataReader.TEMPMEASURE_STOP;

                boolean restartDataReader = true;   //reset || SPConfsShared.getSPDataReaderConf() == null
                                                    //|| !(SPConfsShared.getSPDataReaderConf().getSPMeasurementParameter() instanceof SPMeasurementParameterPOT);
                String measure_unit = "A";

                String measureName = SPFamily.MEASURE_TYPES_AVAILABLE[SPFamily.MEASURE_TYPE_POT];

                SPMeasurementMetaParameter meta = new SPMeasurementMetaParameter(SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size());
                //meta.setFillBufferBeforeStart(false);
                //meta.setBurst(1);
                spMeasurementParameterPOT.setFillBufferBeforeStart(false);
                spMeasurementParameterPOT.setBurstMode(false);

                meta.setRenewBuffer(true);

                SPDataRepresentationManager spDataRepresentationManager = new SPDataRepresentationManager(0);

                SPDataReaderConf spDataReaderConf = new SPDataReaderConf(spMeasurementParameterPOT, SPConfigurationManager.getSPConfigurationDefault(), meta,
                        timePlot, delay, dimSpaceQueue, dimTimeQueue, restartDataReader, numOfMeasure, plotAutoRange,
                        "V", "--", Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, valueToShow, numTempMeasure, measure_unit, measureName,spDataRepresentationManager);

                spDataReaderConf.setTimePlot(true);


                spDataReaderConf.setTimePlot(true);
                spDataReader = new SPDataReader(spDataReaderConf, spMeasurement);
                spDataReader.setPauseReader(false);

                GUIStatus.InstrumentsEISRunning = true;
                startMeasure.setText("Stop");
                plotButton.setDisable(false);
                autoButton.setDisable(true);


            } else {
                spDataReader.stopReader();
                startMeasure.setText("Start measure");
                plotButton.setDisable(true);
                autoButton.setDisable(false);
            }


        } catch(SPException spe){
            spe.printStackTrace();
        }

    }

    @FXML
    private void LoadSettingsOnAction() throws SPException {

        setParameterToUI(createDefaultParam());
    }


    @FXML
    private void setParametersDefaultOnAction() throws SPException {

    }

    @FXML
    public void setParametersChoice() {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        JOptionPane.showConfirmDialog(null, "Are you sure to set this parameters?", "WARNING", dialogButton);
        if (dialogButton == JOptionPane.YES_OPTION) {
            String defaultParam = null;
            defaultParam =
                    Port.getValue().toString() + "/" +
                            Type.getValue().toString() + "/" +
                            RSense.getValue().toString() + "/" +
                            InGain.getValue().toString() + "/" +
                            Contacts.getValue().toString() + "/" +
                            Math.round(InitialPotSlider.getValue()) + "/" +
                            Math.round(FinalPotSlider.getValue()) + "/" +
                            Math.round(StepSlider.getValue()) + "/" +
                            Math.round(AmplitudeSlider.getValue()) + "/" +
                            Math.round(PeriodSlider.getValue()) + "/";

            Preferences prefs2 = Preferences.userNodeForPackage(InstrumentsPOTController.class);
            prefs2.put("default values", defaultParam);

        } else if (dialogButton == JOptionPane.NO_OPTION) {

        }
    }

    @FXML
    private void plotButtonClick(ActionEvent e){

    }

    private SPMeasurementParameterPOT createDefaultParam() throws SPException {

        try {
            defaultParam = new SPMeasurementParameterPOT(SPConfigurationManager.getSPConfigurationDefault());

            Preferences prefs = Preferences.userNodeForPackage(InstrumentsPOTController.class);
            String defaultValues = prefs.get("default values", null);
            if (defaultValues == null) {

                defaultParam = createDefaultParam();

            } else {
                String parameter[] = defaultValues.split("/");

                defaultParam.setPort(parameter[0]);
                defaultParam.setType(parameter[1]);
                defaultParam.setRsense(parameter[2]);
                defaultParam.setInGain(parameter[3]);
                defaultParam.setContacts(parameter[4]);
                defaultParam.setInitialPotential(Integer.parseInt(parameter[5]));
                defaultParam.setFinalPotential(Integer.parseInt(parameter[6]));
                defaultParam.setStep(Integer.parseInt(parameter[7]));
                defaultParam.setPulseAmplitude(Integer.parseInt(parameter[8]));
                defaultParam.setPulsePeriod(Integer.parseInt(parameter[9]));
                defaultParam.setAlternativeSignal(parameter[10].equalsIgnoreCase("true"));

            }


        } catch (SPException e) {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during default loading!\n" + e.getMessage() + "\n", DEBUG_LEVEL);
            throw e;
        }

        return defaultParam;
    }



    @Override
    public void open(String info) {
        System.out.println("InstrumentsPOTController received opening connection info");
        tabPOT.setDisable(false);
        startMeasure.setDisable(false);
    }

    @Override
    public void close(String info) {
        System.out.println("InstrumentsPOTController received closing connection info");
        tabPOT.setDisable(true);
    }


    @Override
    public void initTab() {
        if (!alreadyInitialized) {

            //System.out.println("Try to initialize POT");
            try {
                SPConfigurationManager.getSPConfigurationDefault();
                setParameterToUI(createDefaultParam());
                System.out.println("AlreadyInizialized = true");
                alreadyInitialized = true;
            } catch (SPException e) {
                SPLogger.getLogInterface().d("InstrumentsPOTController", "Default configuration not available. Please open a connection", DEBUG_LEVEL);
            }
        } else {
            System.out.println("POT already initialized.");

        }
    }

    private void setParameterToUI(SPMeasurementParameterPOT pot){


        try {
            defaultParam.setPort(SPPort.portLabels[UtilityGUI.searchVector(SPPort.portLabels, pot.getPort())]);
            defaultParam.setType(SPParamItemPOTType.typeLabels[UtilityGUI.searchVector(SPPort.portLabels, pot.getPort())]);
/*
            defaultParam.setType(parameter[1]);
            defaultParam.setRsense(parameter[2]);
            defaultParam.setInGain(parameter[3]);
            defaultParam.setContacts(parameter[4]);
            defaultParam.setInitialPotential(Integer.parseInt(parameter[5]));
            defaultParam.setFinalPotential(Integer.parseInt(parameter[6]));
            defaultParam.setStep(Integer.parseInt(parameter[7]));
            defaultParam.setPulseAmplitude(Integer.parseInt(parameter[8]));
            defaultParam.setPulsePeriod(Integer.parseInt(parameter[9]));
            defaultParam.setAlternativeSignal(parameter[10].equalsIgnoreCase("true"));*/
        } catch (SPException e) {
            e.printStackTrace();
        }

    }



}
