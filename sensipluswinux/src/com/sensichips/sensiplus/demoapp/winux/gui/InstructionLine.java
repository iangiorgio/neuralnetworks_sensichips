package com.sensichips.sensiplus.demoapp.winux.gui;

/**
 * Created by mario on 07/01/17.
 */
public class InstructionLine {

    private String instruction;
    private String comment;
    private String instructionToSend;
    private String numInstruction;


    public InstructionLine(){
        this(null, null, null, null);
    }

    public InstructionLine(String numInstruction, String instruction, String comment, String instructionToSend) {
        this.instruction = instruction;
        this.comment = comment;
        this.instructionToSend = instructionToSend;
        this.numInstruction = numInstruction;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getInstructionToSend() {
        return instructionToSend;
    }

    public void setInstructionToSend(String instructionToSend) {
        this.instructionToSend = instructionToSend;
    }

    public String getNumInstruction() {
        return numInstruction;
    }

    public void setNumInstruction(String numInstruction) {
        this.numInstruction = numInstruction;
    }
}
