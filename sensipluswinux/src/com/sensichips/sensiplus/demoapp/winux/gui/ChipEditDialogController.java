package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.Chip;
import com.sensichips.sensiplus.model.Family;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.FamilyDAOMYSQLImpl;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.List;

public class ChipEditDialogController {
    @FXML
    private TextField idSPChipField;
    @FXML
    private ComboBox<String> SPFamily_idSPFamilyBox;
    @FXML
    private TextField cAddressField;

    private Chip chip;
    private Stage stage;
    private Stage primaryStage;
    private boolean check = false;
    private boolean ok = false;


    private String[] getItem() {
        String items = "";
        List<Family> list;
        try {
            list = FamilyDAOMYSQLImpl.getInstance().select(new Family());//carico nella lista le famiglie ricevuto dall'interrogazione
            //faccio un ciclo for, e inserisco ogni elemento della lista in una striga e la separo con una virgola
            for ( int i=0; i<list.size(); i++){
                if(i==0){
                    items = list.get(i).getIdSPFamily();}
                else{
                    items = items + ";" + list.get(i).getIdSPFamily();
                }
            }

        } catch (DAOException e) {
            e.printStackTrace();
        }

        String recived[] = items.split(";");
        return recived;
    }

    //Inizialize come sappiamo viene avviata quado viene caricato il file xml associato. Appena sia avvia noi cariachiamo
    //gli elementi nella combox relativi agli id delle famiglie esistenti. In tal modo l'utente puo associare ad un chip
    //soltanto una famiglia esistente
    @FXML
    public void initialize() {
        SPFamily_idSPFamilyBox.getItems().addAll(getItem());
    }


    public void setStage(Stage stage) {
        this.stage = stage;
    }


    public void setChip(Chip chip) {
        this.chip = chip;
        idSPChipField.setText(chip.getIdSPChip());

       cAddressField.setText(chip.getCAddress());

        //sotto assegniamo settiamo alla combobox gli id delle famiglie, ed effettuiamo un controllo.
        //se l'id della famiglia è Integer.MIN_VALUE, vuol dire che quel id era vuoto e nella combox lo mostriamo come " "
        //ovvero un spazio vuoto
        SPFamily_idSPFamilyBox.setValue(chip.getSpFamily_idSPFamily());
    }

    @FXML
    public void okButtonAction() {
        if (checkField(check)) {
            chip.setIdSPChip(idSPChipField.getText());
           chip.setCAddress(cAddressField.getText());
            chip.setSpFamily_idSPFamily(SPFamily_idSPFamilyBox.getValue());
            ok = true;
            stage.close();
        }
    }


    private boolean checkField(boolean check) {
        String error = "";

        if (check) {
            error += findError(idSPChipField, "idSPChip");
            error += findError(cAddressField,"cAddress");

            if (SPFamily_idSPFamilyBox.getValue().equals("")) {
                error += "IdSPFamily can not be empty.\n";
            }

            if (error.length() == 0) {
                return true;
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(stage);
                alert.setTitle("Invalid fields");
                alert.setHeaderText("Please correct invalid fields");
                alert.setContentText(error);

                alert.showAndWait();

                return false;
            }
        } else {
            return true;
        }
    }

    @FXML
    public void cancelButtonAction() {
        stage.close();
    }

    public boolean okClicked() {
        return ok;
    }


    private String findError(TextField textField, String fieldName) {
        String error = "";

        if (textField.getText().equals("")) {
            error += fieldName + " can not be empty.\n";
        }

        return error;
    }

}
