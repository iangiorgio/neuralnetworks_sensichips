package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.Cluster;
import com.sensichips.sensiplus.model.Configuration;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.ClusterDAOMYSQLImpl;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.List;

public class ConfigurationEditController {
    @FXML
    private TextField idSPConfigurationField;
    @FXML
    private TextField driverField;
    @FXML
    private TextField hostControllerField;
    @FXML
    private TextField apiOwnerField;
    @FXML
    private TextField mcuField;
    @FXML
    private TextField protocolField;
    @FXML
    private TextField addressingTypeField;
    @FXML
    private ComboBox<String> idClusterBox;
    @FXML
    private TextField descriptionField;
    @FXML
    private TextField param1Field;
    @FXML
    private TextField param2Field;
    @FXML
    private TextField vccField;

    private Configuration configuration;
    private Stage stage;
    private boolean check = false;
    private boolean ok = false;


    //Inizialize come sappiamo viene avviata quado viene caricato il file xml associato. Appena sia avvia noi cariachiamo
    //gli elementi nella combox relativi agli id dei Cluster esistenti. In tal modo l'utente puo inserire ad una Configurazione
    //soltanto un Cluster esistente
    @FXML
    public void initialize() {
        //carico gli elementi selezionabili all'interno della combox
        idClusterBox.getItems().addAll(getItem());
        idSPConfigurationField.textProperty().addListener((observable, oldValue, newValue) -> {
            idSPConfigurationField.setText(newValue.replaceAll("[^\\d]", ""));
        });
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private String[] getItem() {
        String items = "";
        List<Cluster> list;
        try {
            list = ClusterDAOMYSQLImpl.getInstance().select(new Cluster());//carico nella lista i cluster ricevuti dall'interrogazione
            //faccio un ciclo for, e inserisco ogni elemento della lista in una striga e la separo con una virgola
            for ( int i=0; i<list.size(); i++){
                if(i==0){
                    items = list.get(i).getIdCluster();}
                else{
                    items = items + ";" + list.get(i).getIdCluster();
                }
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        //splitto la strnga items precedentemente caricata, in recived
        String recived[] = items.split(";");
        return recived;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
        //sotto assegniamo alla TextField l'Id della configurazione, ed effettuiamo un controllo prima del insrimento.
        //se l'id del cluster è Integer.MIN_VALUE, vuol dire che quel id era vuoto e nella combox lo mostriamo come " "
        //ovvero un spazio vuoto
        idSPConfigurationField.setText(configuration.getIdSPConfiguration() == Integer.MIN_VALUE ? "" : configuration.getIdSPConfiguration().toString());
        descriptionField.setText(configuration.getDESCRIPTION());
        driverField.setText(configuration.getConfigurationDriver().getDriver());
        param1Field.setText(configuration.getConfigurationDriver().getParam1());
        param2Field.setText(configuration.getConfigurationDriver().getParam2());
        hostControllerField.setText(configuration.getHostController());
        apiOwnerField.setText(configuration.getApiOwner());
        mcuField.setText(configuration.getMcu());
        protocolField.setText(configuration.getProtocol());
        addressingTypeField.setText(configuration.getAddressingType());
        idClusterBox.setValue(configuration.getIdCluster());
        vccField.setText(configuration.getVCC());

    }

    @FXML
    public void okButtonAction() {
        if (checkField(check)) {
            configuration.setIdSPConfiguration(idSPConfigurationField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(idSPConfigurationField.getText()));
            configuration.setDESCRIPTION(descriptionField.getText());
            configuration.getConfigurationDriver().setDriver(driverField.getText());
            configuration.getConfigurationDriver().setParam1(param1Field.getText());
            configuration.getConfigurationDriver().setParam2(param2Field.getText());
            configuration.setHostController(hostControllerField.getText());
            configuration.setApiOwner(apiOwnerField.getText());
            configuration.setMcu(mcuField.getText());
            configuration.setProtocol(protocolField.getText());
            configuration.setAddressingType(addressingTypeField.getText());
            configuration.setIdCluster(idClusterBox.getValue());
            configuration.setVCC(vccField.getText());

            ok = true;
            stage.close();
        } else {
            ok = false;
        }
    }

    private boolean checkField(boolean check) {
        String error = "";

        if (check) {

            error += findError(idSPConfigurationField, "idSPConfigurationField", true);
            error += findError(descriptionField,"descriptionField",false);
            error += findError(driverField, "driverField", false);
            error += findError(param1Field,"param1Field",false);
            error += findError(param2Field,"param2Field",false);
            error += findError(hostControllerField, "hostControllerField", false);
            error += findError(apiOwnerField, "apiOwnerField", false);
            error += findError(mcuField, "mcuField", false);
            error += findError(protocolField, "protocolField", false);
            error += findError(addressingTypeField, "addressingTypeField", false);
            error += findError(vccField,"vccField",false);

            if (idClusterBox.getValue().equals("")) {
                error += "idSPConfigurationField can not be empty.\n";
            }

            //se la stringa di errore non è nulla significa che c'è stato qualche errore di inserimento,
            //e dunque parte un allert che ci spiega cosa è andato storto
            if (error.length() == 0) {
                return true;
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(stage);
                alert.setTitle("Invalid fields");
                alert.setHeaderText("Please correct invalid fields");
                alert.setContentText(error);

                alert.showAndWait();

                return false;
            }
        } else {
            return true;
        }
    }

    private String findError(TextField textField, String fieldName, boolean isNumeric) {
        String error = "";

        if (textField.getText().equals("")) {
            error += fieldName + " can not be empty.\n";
        }
        if (isNumeric) {
            try {
                Integer.parseInt(textField.getText());
            } catch (NumberFormatException e) {
                error += fieldName + " msut be a number.\n";
            }
        }
        return error;
    }

    @FXML
    public void cancelButtonAction() {
        stage.close();
    }

    public boolean okClicked() {
        return ok;
    }


}
