package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.*;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.ClusterDAOMYSQLImpl;
import com.sensichips.sensiplus.model.dao.mysql.FamilyDAOMYSQLImpl;
import com.sensichips.sensiplus.model.dao.mysql.SensingElementOnChipDAOMYSQLImpl;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClusterController {

    @FXML
    private TableView<Cluster> clusterTableView;
    @FXML
    private TableColumn<Cluster, String> idClusterTableColumn;
    @FXML
    private TableColumn<Cluster, String> idFamilyTableColumn;

    @FXML
    private TableView<Chip> chipDispTableView;
    @FXML
    private TableColumn<Chip, String> idSPChipDispTableColumn;
    @FXML
    private TableView<Chip> chipInsTableView;
    @FXML
    private TableColumn<Chip, String> idSPChipInsTableColumn;
    @FXML
    private TableView<SensingElementOnChip> portInsTableView;
    @FXML
    private TableColumn<SensingElementOnChip, String> portPortInsTableColumn;
    @FXML
    private TableColumn<SensingElementOnChip, String> sePortInsTableColumn;
    @FXML
    private TableColumn<SensingElementOnChip, String> mPortInsTableColumn;
    @FXML
    private TableColumn<SensingElementOnChip, String> nPortInsTableColumn;

    @FXML
    private Button modificaButton;
    @FXML
    private Button cancellaButton;

    @FXML
    private Label outPutClusterLabel;
    @FXML
    private Label outPutChipLabel;

    private Stage stage;
    private Stage primaryStage;
    private SensingElementOnChip seOnChipSelected = null;

    private ObservableList<Cluster> clusterData = FXCollections.observableArrayList();
    private ObservableList<Chip> chipDispData = FXCollections.observableArrayList();
    private ObservableList<Chip> chipInsData = FXCollections.observableArrayList();
    private ObservableList<SensingElementOnChip> sensingElementOnChipData = FXCollections.observableArrayList();
    private ObservableList<Port> portInsConSeData = FXCollections.observableArrayList();

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public ObservableList<Cluster> getClusterData() {
        return clusterData;
    }

    public ObservableList<Chip> getChipDispData() {
        return chipDispData;
    }

    public ObservableList<Chip> getChipInsData() {
        return chipInsData;
    }

    public ObservableList<SensingElementOnChip> getSensingElementOnChipData() {
        return sensingElementOnChipData;
    }

    public ObservableList<Port> getPortInsConSeData() {
        return portInsConSeData;
    }

    private FadeTransition fadeIn = new FadeTransition(
            Duration.millis(1000)
    );

    private void outPutMessage(Label outPutLabel, String request, int row) {
        outPutLabel.setVisible(false);
        fadeIn.setNode(outPutLabel);
        if ( row != -1) {
            outPutLabel.setText("SQL: " + request + " Row Affected: " + row + ".");
        } else {
            outPutLabel.setText("SQL: " + request);
        }
        outPutLabel.setVisible(true);
        fadeIn.playFromStart();
    }

    @FXML
    public void initialize() {
        outPutClusterLabel.setVisible(false);
        outPutChipLabel.setVisible(false);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setCycleCount(1);

        Callback<TableColumn<SensingElementOnChip, String>, TableCell<SensingElementOnChip, String>> textFieldFactory
                = (TableColumn<SensingElementOnChip, String> param) -> new TextFieldCell();

        stage = this.getPrimaryStage();
        modificaButton.setDisable(true);
        cancellaButton.setDisable(true);

        clusterTableView.setItems(this.getClusterData());
        chipDispTableView.setItems(this.getChipDispData());
        chipInsTableView.setItems(this.getChipInsData());
        portInsTableView.setItems(this.getSensingElementOnChipData());

        clusterTableView.getItems().addListener((ListChangeListener.Change<? extends Cluster> c) -> {
            if (c.getList().size() == 0) {
                modificaButton.setDisable(true);
                cancellaButton.setDisable(true);
            } else {
                modificaButton.setDisable(false);
                cancellaButton.setDisable(false);
            }
        });

        // Enable multiple selection for portDispTableView elements
        chipDispTableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );
        // Enable multiple selection for portDispTableView elements
        chipInsTableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );

        chipInsTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            showSensingElementOnChipAvaible(newValue);
        });

        idClusterTableColumn.setCellValueFactory(cellData -> cellData.getValue().idClusterProperty());
        //nella riga di sotto settiamo il valore che vogliamo vada a riempire la colonna della tabella
        idFamilyTableColumn.setCellValueFactory(cellData -> cellData.getValue().idSPFamilyProperty());
        idSPChipDispTableColumn.setCellValueFactory(cellData -> cellData.getValue().idSPChipProperty());
        idSPChipInsTableColumn.setCellValueFactory(cellData -> cellData.getValue().idSPChipProperty());
        portPortInsTableColumn.setCellValueFactory(cellData -> cellData.getValue().portNameProperty());
        sePortInsTableColumn.setCellValueFactory(cellData -> cellData.getValue().seAssociatedProperty());

        mPortInsTableColumn.setCellValueFactory(cellData -> cellData.getValue().getCalibrationParameter().mProperty());
        mPortInsTableColumn.setCellFactory(textFieldFactory);
        nPortInsTableColumn.setCellValueFactory(cellData -> cellData.getValue().getCalibrationParameter().nProperty());
        nPortInsTableColumn.setCellFactory(textFieldFactory);

        clusterTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            showChipAvailable(newValue);
        });


        clusterTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2 && clusterTableView.getItems().size() != 0) {
                        modificaButtonAction();
                    } else if (mouseEvent.getClickCount() == 2 && clusterTableView.getItems().size() == 0) {
                        nuovoButtonAction();
                    }
                }
            }
        });


        mPortInsTableColumn.setOnEditStart(event -> {
            seOnChipSelected = portInsTableView.getSelectionModel().getSelectedItem();
        });

        mPortInsTableColumn.setOnEditCommit(event -> {
            if (seOnChipSelected != null) {
                seOnChipSelected.getCalibrationParameter().setM(Double.parseDouble(event.getNewValue().equals("") ? "1.0" : event.getNewValue()));
                insertSensingElementOnChip(seOnChipSelected);
            }
        });

        nPortInsTableColumn.setOnEditStart(event -> {
            seOnChipSelected = portInsTableView.getSelectionModel().getSelectedItem();
        });

        nPortInsTableColumn.setOnEditCommit(event -> {
            if (seOnChipSelected != null) {
                seOnChipSelected.getCalibrationParameter().setN(Double.parseDouble(event.getNewValue().equals("") ? "0.0" : event.getNewValue()));
                insertSensingElementOnChip(seOnChipSelected);
            }
        });
    }

    private void insertSensingElementOnChip(SensingElementOnChip se) {
        try {
            SensingElementOnChipDAOMYSQLImpl.getInstance().update(se);
            outPutMessage(outPutClusterLabel, "Sensing Element On Chip correctly updated.", 1);
        } catch (DAOException e) {
            outPutMessage(outPutClusterLabel, "WARNING! Some Error occured.", -1);
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("Error during DB interaction");
            alert.setHeaderText("Error during update ...");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }
    }

    private void showSensingElementOnChipAvaible(Chip c) {
        List<SensingElementOnChip> listSeOnChip;
        List<Port> listPort;
        SensingElementOnChip seOnChip = new SensingElementOnChip();
        Cluster selectedCluster = clusterTableView.getSelectionModel().getSelectedItem();

        if (c != null) {
            try {
                seOnChip.setIdSPCluster(selectedCluster.getIdCluster());
                seOnChip.setIdFamily(selectedCluster.getIdSPFamily());
                seOnChip.setIdSPChip(c.getIdSPChip());
                listPort = getPortAvailable(c, seOnChip);
                this.getSensingElementOnChipData().clear();
                for (int i = 0; i < listPort.size(); i++) {
                    seOnChip.setPortName(listPort.get(i).getName());
                    seOnChip.setIdSPPort(listPort.get(i).getIdSPPort());
                    seOnChip.setSeAssociated(listPort.get(i).getSeAssociated());
                   // seOnChip.setSeAssociated(listPort.get(i).getSeNameAssociated()); //+
                    listSeOnChip = SensingElementOnChipDAOMYSQLImpl.getInstance().select(seOnChip);
                    if (listSeOnChip != null) {
                        listSeOnChip.get(0).setSeAssociated(seOnChip.getSeAssociated());
                        listSeOnChip.get(0).setPortName(seOnChip.getPortName());
                        listSeOnChip.get(0).setIdSPPort(seOnChip.getIdSPPort());
                        this.getSensingElementOnChipData().add(listSeOnChip.get(0));
                    }
                }
            } catch (DAOException e) {
                e.printStackTrace();
            }

        }
    }

    private List<Port> getPortAvailable(Chip c, SensingElementOnChip seOnChip) {
        List<Port> listPortWithSe = new ArrayList<>();
        List<Port> list;
        if (c != null) {
            Family f = new Family(c.getSpFamily_idSPFamily());
            SensingElement se;
            try {
                //tramite query vedo per una determinata famiglia quali porte sono state inserite, e le salvo nella lista
                list = FamilyDAOMYSQLImpl.getInstance().selectInPort(f);
                this.getPortInsConSeData().clear();
                //this.getSensingElementOnChipData().clear();
                //per porta inserita va a associare un sensing element. Nel caso il se è presente associa il se alla porta
                //altrimenti elimina la porta a cui non è associato il se
                for (int i = 0; i < list.size(); i++) {
                    se = (SensingElement) FamilyDAOMYSQLImpl.getInstance().selectSensingElementIn(f, list.get(i));
                    if (se != null) {
                        list.get(i).setSeAssociated(se.getIdSPSensingElement());
                        list.get(i).setSeNameAssociated(se.getName());
                        listPortWithSe.add(list.get(i));
                    }
                }
                //this.getPortInsData().get(1).setSeAssociated("kasjd");
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
        return listPortWithSe;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private String[] getItem() {
        String items = "";
        List<Family> list;
        try {
            list = FamilyDAOMYSQLImpl.getInstance().select(new Family());//carico nella lista le famiglie ricevuti dall'interrogazione.
            //faccio un ciclo for, e inserisco ogni elemento della lista in una striga e la separo con una virgola
            for (int i = 0; i < list.size(); i++) {
                if (i == 0) {
                    items = list.get(i).getIdSPFamily();
                } else {
                    items = items + ";" + list.get(i).getIdSPFamily();
                }

            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        //splitto la strnga items precedentemente caricata, in recived
        String recived[] = items.split(";");
        return recived;
    }

    //questo metodo è richiamato quando selezioniamo un elemento nella colonna della tabella clusterTableView
    //e va a popolare la colonna della tabella measureTDispTableView e measureTInsTableView
    private void showChipAvailable(Cluster c) {
        if (c != null) {
            List<Chip> list = null;
            try {
                list = ClusterDAOMYSQLImpl.getInstance().selectChipNotIn(c);
                this.getChipDispData().clear();
                this.getChipDispData().addAll(list);
                list = ClusterDAOMYSQLImpl.getInstance().selectChiptIn(c);
                this.getSensingElementOnChipData().clear();
                this.getChipInsData().clear();
                this.getChipInsData().addAll(list);
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void cercaButtonAction() {
        Cluster tempCluster = new Cluster();
        boolean okClicked = this.showClusterEditDialog(stage, tempCluster, false);
        if (okClicked) {
            try {
                List<Cluster> list = ClusterDAOMYSQLImpl.getInstance().select(tempCluster);
                this.getClusterData().clear();
                this.getClusterData().addAll(list);//inserisce nella lista osservabile tutti gli elementi
                // restituiti dalla query di sorpa
            } catch (DAOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during search ...");
                alert.setContentText(e.getMessage());
                alert.showAndWait();
            }
        }
    }

    @FXML
    public void modificaButtonAction() {
        Cluster tempCluster = clusterTableView.getSelectionModel().getSelectedItem();
        if (tempCluster != null) {
            tempCluster.setOldidCluster();
            boolean okClicked = this.showClusterEditDialog(stage, tempCluster, false);
            if (okClicked) {
                try {
                    ClusterDAOMYSQLImpl.getInstance().update(tempCluster);
                    this.getSensingElementOnChipData().clear();
                    showChipAvailable(tempCluster);
                    outPutMessage(outPutClusterLabel, "Cluster correctly updated.", 1);
                } catch (DAOException e) {
                    outPutMessage(outPutClusterLabel, "WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during update ...");
                    alert.setContentText(e.getMessage());

                    alert.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Configuration Selected");
            alert.setContentText("Please select a Configuration in the table.");

            alert.showAndWait();
        }

    }

    @FXML
    public void cancellaButtonAction() {
        Cluster c = clusterTableView.getSelectionModel().getSelectedItem();
        if (c != null) {
            //definisco e implemento un elemento della classe Family a cui ci passo l'emento selezionato nella tabella

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initOwner(stage);
            alert.setTitle("Are you sure?");
            alert.setHeaderText("Delete " + c.getIdCluster() + "?");
            alert.setContentText("Are you sure that you want to delete the selected item?");

            ButtonType buttonTypeOne = new ButtonType("Yes");
            ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                try {
                    ClusterDAOMYSQLImpl.getInstance().delete(c);
                    this.getClusterData().remove(c);
                    outPutMessage(outPutClusterLabel, "Cluster correctly deleted.", 1);
                } catch (DAOException e) {
                    outPutMessage(outPutClusterLabel, "WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alertError = new Alert(Alert.AlertType.ERROR);
                    alertError.initOwner(this.getPrimaryStage());
                    alertError.setTitle("Error during DB interaction");
                    alertError.setHeaderText("Error during delete ...");
                    alertError.setContentText(e.getMessage());

                    alertError.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Configuration Selected");
            alert.setContentText("Please select a Configuration in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    public void nuovoButtonAction() {
        Cluster tempCluster = new Cluster("","","0x0000000011");
        boolean okClicked = this.showClusterEditDialog(stage, tempCluster, true);
        if (okClicked) {
            try {
                ClusterDAOMYSQLImpl.getInstance().insert(tempCluster);
                this.getClusterData().add(tempCluster);
                outPutMessage(outPutClusterLabel, "Cluster correctly inserted.", 1);
            } catch (DAOException e) {
                outPutMessage(outPutClusterLabel, "WARNING! Some Error occured.", -1);
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during insert ...");
                alert.setContentText(e.getMessage());

                alert.showAndWait();
            }
        }
    }

    @FXML
    public void insertChipButtonAction() {
        ObservableList<Chip> chipToIns = FXCollections.observableArrayList();
        chipToIns.addAll(chipDispTableView.getSelectionModel().getSelectedItems());
        Cluster selectedCluster = clusterTableView.getSelectionModel().getSelectedItem();
        SensingElementOnChip seToIns = new SensingElementOnChip();
        List<Port> portAvaible;

        if (selectedCluster != null) {
            if (chipToIns.size() != 0) {
                try {
                    if (!seToIns.equals(new SensingElementOnChip())) {
                        for (Chip chip : chipToIns) {
                            seToIns.setIdSPCluster(selectedCluster.getIdCluster());
                            seToIns.setIdFamily(selectedCluster.getIdSPFamily());
                            seToIns.setIdSPChip(chip.getIdSPChip());
                            portAvaible = getPortAvailable(chip, seToIns);
                            for (Port port : portAvaible) {
                                seToIns.setPortName(port.getName());
                                seToIns.setSeAssociated(port.getSeAssociated());
                              //  seToIns.setSeAssociated(port.getSeNameAssociated());

                                seToIns.setIdSPPort(port.getIdSPPort());
                                SensingElementOnChipDAOMYSQLImpl.getInstance().insert(seToIns);
                            }
                            this.getChipInsData().add(chip);
                            this.getChipDispData().remove(chip);
                        }
                        outPutMessage(outPutChipLabel, "Chip correctly inserted.", chipToIns.size());
                    }
                } catch (DAOException e) {
                    outPutMessage(outPutChipLabel, "WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during insert ...");
                    alert.setContentText(e.getMessage());

                    alert.showAndWait();
                }

            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Selection");
                alert.setHeaderText("No Chip Selected");
                alert.setContentText("Please select a Chip in the Chip Disponibili table.");

                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Cluster Selected");
            alert.setContentText("Please select a Cluster in the table.");
            alert.showAndWait();
        }
    }

    @FXML
    public void insertAllPortButtonAction() {
        ObservableList<Chip> chipToIns = FXCollections.observableArrayList();
        chipToIns.addAll(this.getChipDispData());
        Cluster selectedCluster = clusterTableView.getSelectionModel().getSelectedItem();
        SensingElementOnChip seToIns = new SensingElementOnChip();
        List<Port> portAvaible;

        if (selectedCluster != null) {
            if (chipToIns.size() != 0) {
                try {
                    if (!seToIns.equals(new SensingElementOnChip())) {
                        for (Chip chip : chipToIns) {
                            seToIns.setIdSPCluster(selectedCluster.getIdCluster());
                            seToIns.setIdFamily(selectedCluster.getIdSPFamily());
                            seToIns.setIdSPChip(chip.getIdSPChip());
                            portAvaible = getPortAvailable(chip, seToIns);
                            for (Port port : portAvaible) {
                                seToIns.setPortName(port.getName());
                                seToIns.setSeAssociated(port.getSeAssociated());
                               // seToIns.setSeAssociated(port.getSeNameAssociated());
                                seToIns.setIdSPPort(port.getIdSPPort());
                                SensingElementOnChipDAOMYSQLImpl.getInstance().insert(seToIns);
                            }
                            this.getChipInsData().add(chip);
                            this.getChipDispData().remove(chip);
                        }
                        outPutMessage(outPutChipLabel, "All Chip has been inserted.", chipToIns.size());
                    }
                } catch (DAOException e) {
                    outPutMessage(outPutChipLabel, "WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during insert ...");
                    alert.setContentText(e.getMessage());

                    alert.showAndWait();
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Port");
                alert.setHeaderText("No Chip Available");
                alert.setContentText("There is no Chip to add.");

                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Cluster Selected");
            alert.setContentText("Please select a Cluster in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    public void deleteChipButtonAction() {
        ObservableList<Chip> chipToRemove = FXCollections.observableArrayList();
        chipToRemove.addAll(chipInsTableView.getSelectionModel().getSelectedItems());
        SensingElementOnChip seToRemove = new SensingElementOnChip();
        Cluster selectedCluster = clusterTableView.getSelectionModel().getSelectedItem();
        if (selectedCluster != null) {
            if (chipToRemove.size() != 0) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.initOwner(stage);
                alert.setTitle("Are you sure?");
                alert.setHeaderText("Delete selected Chip(s)?");
                alert.setContentText("Are you sure that you want to delete the selected item(s)?");

                ButtonType buttonTypeOne = new ButtonType("Yes");
                ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
                alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == buttonTypeOne) {
                    seToRemove.setIdSPCluster(selectedCluster.getIdCluster());
                    try {
                        for (Chip chip : chipToRemove) {
                            seToRemove.setIdSPChip(chip.getIdSPChip());
                            SensingElementOnChipDAOMYSQLImpl.getInstance().delete(seToRemove);

                            //per ora non mi serve un inserimento nel database
                            // FamilyDAOMYSQLImpl.getInstance().insertPort(selectedFamily, port);
                            this.getChipDispData().add(chip);
                            this.getChipInsData().remove(chip);
                            this.getSensingElementOnChipData().clear();
                        }
                        outPutMessage(outPutChipLabel, "Chip correctly deleted.", chipToRemove.size());
                    } catch (DAOException e) {
                        outPutMessage(outPutChipLabel, "WARNING! Some Error occured.", -1);
                        e.printStackTrace();
                        Alert alertError = new Alert(Alert.AlertType.ERROR);
                        alertError.initOwner(this.getPrimaryStage());
                        alertError.setTitle("Error during DB interaction");
                        alertError.setHeaderText("Error during delete ...");
                        alertError.setContentText(e.getMessage());

                        alertError.showAndWait();
                    }
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Selection");
                alert.setHeaderText("No Chip Selected");
                alert.setContentText("Please select a Chip in the Chip Inseriti table.");

                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Cluster Selected");
            alert.setContentText("Please select a Cluster in the table.");
            alert.showAndWait();
        }
    }

    @FXML
    public void deleteAllChipButtonAction() {
        ObservableList<Chip> chipToRemove = FXCollections.observableArrayList();
        chipToRemove.addAll(this.getChipInsData());
        SensingElementOnChip seToRemove = new SensingElementOnChip();
        Cluster selectedCluster = clusterTableView.getSelectionModel().getSelectedItem();
        if (selectedCluster != null) {
            seToRemove.setIdSPCluster(selectedCluster.getIdCluster());
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initOwner(stage);
            alert.setTitle("Are you sure?");
            alert.setHeaderText("Delete all Chips?");
            alert.setContentText("Are you sure that you want to delete all associated Chips?");

            ButtonType buttonTypeOne = new ButtonType("Yes");
            ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                seToRemove.setIdSPCluster(selectedCluster.getIdCluster());
                try {
                    SensingElementOnChipDAOMYSQLImpl.getInstance().delete(seToRemove);
                    this.getChipDispData().addAll(this.getChipInsData());
                    outPutMessage(outPutChipLabel, "All Chip has been deleted.", this.getChipInsData().size());
                    this.getChipInsData().clear();
                    this.getSensingElementOnChipData().clear();
                } catch (DAOException e) {
                    outPutMessage(outPutChipLabel, "WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alertError = new Alert(Alert.AlertType.ERROR);
                    alertError.initOwner(this.getPrimaryStage());
                    alertError.setTitle("Error during DB interaction");
                    alertError.setHeaderText("Error during delete ...");
                    alertError.setContentText(e.getMessage());

                    alertError.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Cluster Selected");
            alert.setContentText("Please select a Cluster in the table.");
            alert.showAndWait();
        }
    }

    public boolean showClusterEditDialog(Stage owner, Cluster c, boolean check) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("ClusterEditDialog.fxml"));
            AnchorPane dialog = loader.load();

            Scene scene = new Scene(dialog);
            Stage fDialog = new Stage();
            fDialog.setTitle("Edit Cluster");
            fDialog.initModality(Modality.WINDOW_MODAL);
            fDialog.initOwner(owner);
            fDialog.setScene(scene);
            fDialog.getIcons().add(new Image(getClass().getResourceAsStream("../../../resources/sensichips_logo.png")));

            ClusterEditController controller = loader.getController();
//            controller.setMain(this, check);
            controller.setStage(fDialog);
            controller.setCluster(c);

            fDialog.showAndWait();


            return controller.okClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
