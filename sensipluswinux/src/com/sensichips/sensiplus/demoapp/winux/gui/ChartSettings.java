package com.sensichips.sensiplus.demoapp.winux.gui;

public class ChartSettings {

    public static final int YT_CHART = 0;
    public static final int YX_CHART= 1;

    public int plotDeep = 500;
    public double lowerY = 0;
    public double upperY = 1;
    public int typeOfMeasure = YT_CHART;
    public String title = "Title here...";
    public String xLabel = "";
    public String yLabel = "";
    public String unitOfMeasureYAxis = "F";
    public String unitOfMeasureXAxis = "s";
    public int numberOfChips = 0;
    public int numberOfPOTCycle = 50;

    public boolean derivativeChart = false;

}
