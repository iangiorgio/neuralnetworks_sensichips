package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.Configuration;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.ConfigurationDAOMYSQLImpl;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ConfigurationController {
    @FXML
    private TableView<Configuration> configurationTableView;
    @FXML
    private TableColumn<Configuration, Integer> idSPConfigurationColumn;


    @FXML
    private Label driverLabel;
    @FXML
    private Label hostControllerLabel;
    @FXML
    private Label apiOwnerLabel;
    @FXML
    private Label mcuLabel;
    @FXML
    private Label protocolLabel;
    @FXML
    private Label addressingTypeLabel;
    @FXML
    private Label idClusterLabel;
    @FXML
    private Button modificaButton;
    @FXML
    private Button cancellaButton;
    @FXML
    private Label outPutLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Label param1Label;
    @FXML
    private Label param2Label;
    @FXML
    private Label vccLabel;


    // Reference to the main application.
    private Stage stage;

    private Stage primaryStage;

    public Stage getPrimaryStage() {
        return primaryStage;
    }


    private ObservableList<Configuration> configurationData = FXCollections.observableArrayList();

    public ObservableList<Configuration> getConfigurationData() {
        return configurationData;
    }

    private FadeTransition fadeIn = new FadeTransition(
            Duration.millis(1000)
    );

    private void outPutMessage(String request, int row) {
        outPutLabel.setVisible(false);
        if ( row != -1) {
            outPutLabel.setText("SQL: " + request + " Row Affected: " + row + ".");
        } else {
            outPutLabel.setText("SQL: " + request);
        }
        outPutLabel.setVisible(true);
        fadeIn.playFromStart();
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        outPutLabel.setVisible(false);
        fadeIn.setNode(outPutLabel);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setCycleCount(1);
        fadeIn.setAutoReverse(false);
        stage = this.getPrimaryStage();
        modificaButton.setDisable(true);
        cancellaButton.setDisable(true);

        // Initialize Chip table with the two columns.
        idSPConfigurationColumn.setCellValueFactory(cellData -> cellData.getValue().idSPConfigurationProperty().asObject());

        // Impostazione di tutte le Tableview e degli listener
        configurationTableView.setItems(this.getConfigurationData());

        configurationTableView.getItems().addListener((ListChangeListener.Change<? extends Configuration> c) -> {
            if(c.getList().size() == 0){
                modificaButton.setDisable(true);
                cancellaButton.setDisable(true);
            } else{
                modificaButton.setDisable(false);
                cancellaButton.setDisable(false);
            }
        });



        showConfigurationDetails(null);

        configurationTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showConfigurationDetails(newValue));

        configurationTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2 && configurationTableView.getItems().size() != 0) {
                        modificaButtonAction();
                    } else if (mouseEvent.getClickCount() == 2 && configurationTableView.getItems().size() == 0) {
                        nuovoButtonAction();
                    }
                }
            }
        });

    }

    private void showConfigurationDetails(Configuration c) {
        if (c != null) {
            // riempe le label con le informazioni della configurazione
            descriptionLabel.setText(c.getDESCRIPTION());
            driverLabel.setText(c.getConfigurationDriver().getDriver());
            param1Label.setText(c.getConfigurationDriver().getParam1());
            param2Label.setText(c.getConfigurationDriver().getParam2());
            hostControllerLabel.setText(c.getHostController());
            apiOwnerLabel.setText(c.getApiOwner());
            mcuLabel.setText(c.getMcu());
            protocolLabel.setText(c.getProtocol());
            addressingTypeLabel.setText(c.getAddressingType());
            idClusterLabel.setText(c.getIdCluster());
            vccLabel.setText(c.getVCC());
        } else {

            // se la configurazione ha campi vuoti, non visualizza nulla
            descriptionLabel.setText("");
            driverLabel.setText("");
            param1Label.setText("");
            param2Label.setText("");
            hostControllerLabel.setText("");
            apiOwnerLabel.setText("");
            mcuLabel.setText("");
            protocolLabel.setText("");
            addressingTypeLabel.setText("");
            idClusterLabel.setText("");
            vccLabel.setText("");
        }
    }


    @FXML
    private void nuovoButtonAction() {
        Configuration tempConfiguration = new Configuration(Integer.MIN_VALUE,"","WINUX_COMUSB","/dev/ttyUSB0","","PC","PC","ESP8266","SENSIBUS","ShortAddress","","3300");
        boolean okClicked = this.showConfigurationEditDialog(stage, tempConfiguration, true);
        if (okClicked) {
            try {
                ConfigurationDAOMYSQLImpl.getInstance().insert(tempConfiguration);
                this.getConfigurationData().add(tempConfiguration);
                this.getConfigurationData().sort(Comparator.comparingInt(Configuration::getIdSPConfiguration));
                outPutMessage("Configuration correctly inserted.", 1);
            } catch (DAOException e) {
                outPutMessage("WARNING! Some Error occured.", -1);
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during insert ...");
                alert.setContentText(e.getMessage());

                alert.showAndWait();
            }
        }
    }

    @FXML
    public void modificaButtonAction() {
        //tempConfiguration gli verrà passato l'indirizzo della configurazione su cui volgiamo effettuare una modifica
        Configuration tempConfiguration = configurationTableView.getSelectionModel().getSelectedItem();
        if (tempConfiguration != null) {
            tempConfiguration.setOldidSPConfiguration();
            boolean okClicked = this.showConfigurationEditDialog(stage, tempConfiguration, false);
            if (okClicked) {
                try {
                    ConfigurationDAOMYSQLImpl.getInstance().update(tempConfiguration);
                    showConfigurationDetails(tempConfiguration);
                    outPutMessage("Configuration correctly updated.", 1);
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during update ...");
                    alert.setContentText(e.getMessage());

                    alert.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Configuration Selected");
            alert.setContentText("Please select a Configuration in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    public void cercaButtonAction() {
        Configuration tempConfiguration = new Configuration();
        boolean okClicked = this.showConfigurationEditDialog(stage, tempConfiguration, false);
        if (okClicked) {
            try {
                List<Configuration> list = ConfigurationDAOMYSQLImpl.getInstance().select(tempConfiguration);
                this.getConfigurationData().clear();
                this.getConfigurationData().addAll(list);
            } catch (DAOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during search ...");
                alert.setContentText(e.getMessage());
                alert.showAndWait();
            }
        }
    }

    @FXML
    private void cancellaButtonAction() {
        //the function below returns the index of the selected item in the TableView
        int selectedIndex = configurationTableView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            //definisco e implemento un elemento della classe Family a cui ci passo l'emento selezionato nella tabella
            Configuration c = configurationTableView.getItems().get(selectedIndex);

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initOwner(stage);
            alert.setTitle("Are you sure?");
            alert.setHeaderText("Delete " + c.getIdSPConfiguration() + "?");
            alert.setContentText("Are you sure that you want to delete the selected item?");

            ButtonType buttonTypeOne = new ButtonType("Yes");
            ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                try {
                    ConfigurationDAOMYSQLImpl.getInstance().delete(c);
                    configurationTableView.getItems().remove(c);
                    outPutMessage("Configuration correctly deleted.", 1);
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alertError = new Alert(Alert.AlertType.ERROR);
                    alertError.initOwner(this.getPrimaryStage());
                    alertError.setTitle("Error during DB interaction");
                    alertError.setHeaderText("Error during delete ...");
                    alertError.setContentText(e.getMessage());

                    alertError.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Configuration Selected");
            alert.setContentText("Please select a Configuration in the table.");

            alert.showAndWait();
        }
    }

    public boolean showConfigurationEditDialog(Stage owner, Configuration c, boolean check) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("ConfigurationEditDialog.fxml"));
            AnchorPane dialog = loader.load();

            Scene scene = new Scene(dialog);
            Stage fDialog = new Stage();
            fDialog.setTitle("Edit Configuration");
            fDialog.initModality(Modality.WINDOW_MODAL);
            fDialog.initOwner(owner);
            fDialog.setScene(scene);
            fDialog.getIcons().add(new Image(getClass().getResourceAsStream("../../../resources/sensichips_logo.png")));

            ConfigurationEditController controller = loader.getController();
//            controller.setMain(this, check);
            controller.setStage(fDialog);
            controller.setConfiguration(c);

            fDialog.showAndWait();

            return controller.okClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
