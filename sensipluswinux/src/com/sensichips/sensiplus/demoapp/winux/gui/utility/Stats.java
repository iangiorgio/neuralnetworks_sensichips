package com.sensichips.sensiplus.demoapp.winux.gui.utility;

import javafx.beans.property.SimpleStringProperty;


/**
 * Create a new class that collect all the Chip's measurement parameters
 *
 *
 *
 */

public class Stats {

    public String getChipID() {
        return chipID.get();
    }



    public String getMinValue() {
        return minValue.get();
    }


    public String getMaxValue() {
        return maxValue.get();
    }



    public String getMaxSNR() {
        return maxSNR.get();
    }



    public String getNoiseDevStd() {
        return noiseDevStd.get();
    }



    public String getSNR() {
        return SNR.get();
    }



    private final SimpleStringProperty chipID;
    private final SimpleStringProperty minValue;
    private final SimpleStringProperty maxValue;
    private final SimpleStringProperty maxSNR;
    private final SimpleStringProperty noiseDevStd;
    private final SimpleStringProperty SNR;

    //private double lastMaxSNR = -1;


    /**
     *
     * Constructor
     *
     * @param chipID
     * @param minValue
     * @param maxValue
     * @param maxSNR
     * @param noiseDevStd
     * @param SNR
     */

    public Stats(String chipID, String minValue, String maxValue, String maxSNR, String noiseDevStd,
                 String SNR) {

        this.chipID = new SimpleStringProperty (chipID);
        this.minValue = new SimpleStringProperty (minValue);
        this.maxValue = new SimpleStringProperty (maxValue);
        this.maxSNR = new SimpleStringProperty (maxSNR);
        this.noiseDevStd = new SimpleStringProperty (noiseDevStd);
        this.SNR =new SimpleStringProperty (SNR);



    }




}