package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.Family;
import com.sensichips.sensiplus.model.NeuralNetwork;
import com.sensichips.sensiplus.model.SensingElement;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.FamilyDAOMYSQLImpl;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Window;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class NnController{

    private static final int INPUT_ERROR = 1;
    private static final int OUTPUT_ERROR = 2;

    public FileChooser fileLoader = new FileChooser();
    public TextArea showFile;
    final int initialValue = 6;
    final int minSpinner = 1;
    final int maxSpinner = 20;
    public String srcPath = null;
    public String destPath = "./testPath/";
    public String text;

    @FXML
    public Button importButton;

    @FXML
    public Spinner inputNodes;

    @FXML
    public Spinner outputNodes;

    @FXML
    public VBox inputBox;

    @FXML
    public VBox outputBox;

    @FXML
    public TextField textField;

    @FXML
    private void initialize() {
        initSpinner();
        //showFile.setDisable(true);
    }

    private void initSpinner() {
        inputNodes.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(minSpinner, maxSpinner, initialValue));
        outputNodes.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(minSpinner, maxSpinner, initialValue));
        setNodeBox(initialValue);

        inputNodes.valueProperty().addListener((obs, oldValue, newValue) -> updateNodeBoxInput((Integer) newValue));

        outputNodes.valueProperty().addListener((obs, oldValue, newValue) -> updateNodeBoxOutput((Integer) newValue));
    }


    //TESTING FAMILY (da cambiare con interfaccia dedicata)

    private Stage stage;
    private Stage primaryStage;
    private ObservableList<Family> familytData = FXCollections.observableArrayList();
    public ObservableList<Family> getFamilyData() {
        return familytData;
    }

    public boolean showFamilyEditDialog(Stage owner, Family f, boolean check) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("FamilyEditDialog.fxml"));
            AnchorPane dialog = loader.load();

            Scene scene = new Scene(dialog);
            Stage fDialog = new Stage();
            fDialog.setTitle("Edit Family");
            fDialog.initModality(Modality.WINDOW_MODAL);
            fDialog.initOwner(owner);
            fDialog.setScene(scene);
            fDialog.getIcons().add(new Image(getClass().getResourceAsStream("../../../resources/sensichips_logo.png")));

            FamilyEditController controller = loader.getController();
//            controller.setMain(this, check);
            controller.setStage(fDialog);
            controller.setFamily(f);

            fDialog.showAndWait();

            return controller.okClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void inputButtonBoxAction(int j){
        inputBox.getChildren().get(j).addEventFilter(MouseEvent.MOUSE_CLICKED, event ->{
            Family tempFamily = new Family("","","RUN5","10000000","0X06","0x0000000002","0x0000000001","0x0000000000");
            boolean okClicked = this.showFamilyEditDialog(stage, tempFamily, true);
            if (okClicked) {
                try {
                    FamilyDAOMYSQLImpl.getInstance().insert(tempFamily);
                    this.getFamilyData().add(tempFamily);
                    //outPutMessage("Family correctly inserted.", 1);
                } catch (DAOException e) {
                   // outPutMessage("WARNING! Some Error occured.", -1);
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                   // alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during insert ...");
                    alert.setContentText(e.getMessage());

                    alert.showAndWait();
                }
            }
        });
    }

    // FINE TESTING CON FAMILY

    public void outputButtonBoxAction(int j){
        outputBox.getChildren().get(j).addEventFilter(MouseEvent.MOUSE_CLICKED, event ->
                System.out.println("OUTPUT!"));
    }

    public void updateSpinnerValue(Integer newValue){

        inputNodes.getValueFactory().setValue(newValue);
        outputNodes.getValueFactory().setValue(newValue);
    }

    //TESTING

    public void setNodeBox(int newValue){
        int len  = newValue;
        for(int i = 0 ; i < len; i++ ){
            String sensor = "Set/Get Sensor: "+i;
            Button b = new Button(sensor);
            Button c = new Button(sensor);
            inputBox.getChildren().add(b);
            outputBox.getChildren().add(c);
            inputButtonBoxAction(i);
            outputButtonBoxAction(i);
        }
    }

    public void updateNodeBoxInput(int newValue){
        int currentValue = inputBox.getChildren().size();
        if(newValue > maxSpinner)
            inputNodes.getValueFactory().setValue(maxSpinner);
        else if(newValue < minSpinner){
            inputNodes.getValueFactory().setValue(minSpinner);
        }else if(newValue > currentValue){
            int len  = newValue - currentValue;
            for(int i = 0 ; i < len; i++ ){
                String sensor = "Set/Get Sensor: "+ currentValue++;
                Button b = new Button(sensor);
                inputBox.getChildren().add(b);
                inputButtonBoxAction(currentValue-1);
            }
        }else if(newValue < currentValue) {
            for (int i = currentValue; i > newValue; i--) {
                inputBox.getChildren().remove(i - 1);
            }
        }
    }

    public void updateNodeBoxOutput(int newValue){
        int currentValue = outputBox.getChildren().size();
        if(newValue > maxSpinner)
            outputNodes.getValueFactory().setValue(maxSpinner);
        else if(newValue < minSpinner){
            outputNodes.getValueFactory().setValue(minSpinner);
        }else if(newValue > currentValue){
            int len  = newValue - currentValue;
            for(int i = 0 ; i < len; i++ ){
                String sensor = "Set/Get Sensor: "+ currentValue++;
                Button c = new Button(sensor);
                outputBox.getChildren().add(c);
                outputButtonBoxAction(currentValue-1);
            }
        }else if(newValue < currentValue) {
            for (int i = currentValue; i > newValue; i--) {
                outputBox.getChildren().remove(i - 1);
            }
        }
    }

    public void buttonLoadFile(ActionEvent actionEvent) {

        File myFile = fileLoader.showOpenDialog(importButton.getScene().getWindow());
         //System.out.println(myFile.getPath());
        if(myFile != null)
            srcPath = myFile.getPath();

            try{
            String fileType=Files.probeContentType(Paths.get(myFile.getPath()));//JOptionPane.showMessageDialog(null, fileType);
           // if(fileType==null || fileType=="") throw new IOException();
            if(!fileType.equalsIgnoreCase("text/plain"))
                JOptionPane.showMessageDialog(null, "Type: "+fileType+", not supported");
            else {
                //showFile.setDisable(false);
                showFile.setText(new String(Files.readAllBytes(Paths.get(myFile.getPath()))));
            }

        }catch (IOException e){
            JOptionPane.showMessageDialog(null, e.toString()); // se c'è tempo mostrare un messaggio più carino
        }catch (NullPointerException e){
            JOptionPane.showMessageDialog(null, "File not supported");
        }
    }

    public void buttonGenerateFile(ActionEvent actionEvent){

        if(srcPath == null) {
            textField.setText("Import a file, first!");
            return;
        }

        NeuralNetwork nn = NeuralNetwork.loadNeuralNetworkFromFile(srcPath);

        //Syntax Checking
        int check = checkSyntax(nn);

        if(check == INPUT_ERROR)
            textField.setText("Input does not match");
        else if( check == OUTPUT_ERROR)
            textField.setText("Output does not match");
        else{
            nn.generateWeightsFile(destPath);
            textField.setText("Generated!");
        }
    }

    public int checkSyntax(NeuralNetwork nn){

        int input = inputBox.getChildren().size();
        int output = outputBox.getChildren().size();

        if(input != nn.getInput()){
            return INPUT_ERROR;
        }else if(output != nn.getOutput()){
            return OUTPUT_ERROR;
        }
        return 0;
    }


}
