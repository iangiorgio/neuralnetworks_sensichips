package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.Family;
import com.sensichips.sensiplus.model.MeasureTechnique;
import com.sensichips.sensiplus.model.Port;
import com.sensichips.sensiplus.model.SensingElement;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.FamilyDAOMYSQLImpl;
import com.sensichips.sensiplus.model.dao.mysql.SensingElementDAOMySQLImpl;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;


public class FamilyController {


    @FXML
    private TableView<Family> familyTableView;
    @FXML
    private TableColumn<Family, String> nameSPFamilyColumn;
    @FXML
    private TableView<Port> portDispTableView;
    @FXML
    private TableColumn<Port, String> nameSPPortDispColumn;
    @FXML
    private TableColumn<Port, Boolean> internalSPPortDispColumn;
    @FXML
    private TableView<Port> portInsTableView;
    @FXML
    private TableColumn<Port, String> nameSPPortInsColumn;
    @FXML
    private TableColumn<Port, Boolean> internalSPPortInsColumn;
    @FXML
    private TableColumn<Port, String> seSPPortInsColumn;
    @FXML
    private TableView<MeasureTechnique> measureTDispTableView;
    @FXML
    private TableColumn<MeasureTechnique, String> idSPMeasureTDispColumn;
    @FXML
    private TableView<MeasureTechnique> measureTInsTableView;
    @FXML
    private TableColumn<MeasureTechnique, String> idSPMeasureTInsColumn;

    @FXML
    private Label idspfamilyLabel;
    @FXML
    private Label idLabel;
    @FXML
    private Label hwVersionLabel;
    @FXML
    private Label sysclockLabel;
    @FXML
    private Label osctrimLabel;
    @FXML
    private Label outPutLabel;
    @FXML
    private Label multicastDefaultLabel;
    @FXML
    private Label broadcastLabel;
    @FXML
    private Label broadcastSlowLabel;

    @FXML
    private Button modificaButton;
    @FXML
    private Button cancellaButton;


    // Reference to the main application.
    private Stage stage;
    private Stage primaryStage;

    private ObservableList<Family> familytData = FXCollections.observableArrayList();
    private ObservableList<Port> portDispData = FXCollections.observableArrayList();
    private ObservableList<Port> portInsData = FXCollections.observableArrayList();
    private ObservableList<MeasureTechnique> measureTDispData = FXCollections.observableArrayList();
    private ObservableList<MeasureTechnique> measureTInsData = FXCollections.observableArrayList();

    public ObservableList<Port> getPortDispData() {
        return portDispData;
    }

    public ObservableList<Family> getFamilyData() {
        return familytData;
    }

    public ObservableList<Port> getPortInsData() {
        return portInsData;
    }

    public ObservableList<MeasureTechnique> getMeasureTDispData() {
        return measureTDispData;
    }

    public ObservableList<MeasureTechnique> getMeasureTInsData() {
        return measureTInsData;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    private FadeTransition fadeIn = new FadeTransition(
            Duration.millis(1000)
    );

    private void outPutMessage(String request, int row) {
        outPutLabel.setVisible(false);
        if (row != -1) {
            outPutLabel.setText("SQL: " + request + " Row Affected: " + row + ".");
        } else {
            outPutLabel.setText("SQL: " + request);
        }
        outPutLabel.setVisible(true);
        fadeIn.playFromStart();
    }

    public void clearSelection() {
        portInsTableView.getSelectionModel().clearSelection();
        portDispTableView.getSelectionModel().clearSelection();
        measureTDispTableView.getSelectionModel().clearSelection();
        measureTInsTableView.getSelectionModel().clearSelection();
    }

    private String[] getItem(Family f, Port p) {
        String items = "";
        List<SensingElement> list;
        try {
            list = SensingElementDAOMySQLImpl.getInstance().select(new SensingElement());
            for (int i = 0; i < list.size(); i++) {
                items += list.get(i).getIdSPSensingElement() + ";";
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        String recived[] = items.split(";");
        return recived;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        outPutLabel.setVisible(false);
        fadeIn.setNode(outPutLabel);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setCycleCount(1);
        fadeIn.setAutoReverse(false);

        // Initialize the person table with the two columns.
        stage = this.getPrimaryStage();
        modificaButton.setDisable(true);
        cancellaButton.setDisable(true);
        // Setting up all tableView  and their Listner
        familyTableView.setItems(this.getFamilyData());
        portDispTableView.setItems(this.getPortDispData());
        portInsTableView.setItems(this.getPortInsData());
        measureTDispTableView.setItems(this.getMeasureTDispData());
        measureTInsTableView.setItems(this.getMeasureTInsData());
        //Setting up all Column cell Value
        nameSPFamilyColumn.setCellValueFactory(cellData -> cellData.getValue().idSPFamilyProperty());
        //setto i valori delle colonne all'interno delle tabelle portDispTableView e portInsTableView
        nameSPPortInsColumn.setCellValueFactory((cellData -> cellData.getValue().nameProperty()));
        internalSPPortInsColumn.setCellValueFactory((cellData -> cellData.getValue().internalProperty()));
        nameSPPortDispColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        internalSPPortDispColumn.setCellValueFactory(cellData -> cellData.getValue().internalProperty());
        //setto i valori delle colonne all'interno delle tabelle measureTDispTableView e measureTInsTableView
        idSPMeasureTDispColumn.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
        idSPMeasureTInsColumn.setCellValueFactory(cellData -> cellData.getValue().typeProperty());

        seSPPortInsColumn.setCellValueFactory(
                cellData -> cellData.getValue().seAssociatedProperty()

        );
        //seSPPortInsColumn.setCellFactory(ComboBoxTableCell.forTableColumn(item));

        //se cambio l'elemento presente nella combox viene richiamato il seguente metodo
        seSPPortInsColumn.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Port, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Port, String> t) {
                        List<SensingElement> se = null;
                        List<MeasureTechnique> list = null;
                        String outPut = "";
                        boolean ok = false;
                        try {
                            //effettuiamo un controllo sul associazione del se sulla porta. se il se che vogliiamo associare
                            //ha una measure tecniche che non è presente nella famiglia allora l'associazione non è permessa e viene aperto un
                            //allert
                            se = SensingElementDAOMySQLImpl.getInstance().select(new SensingElement(t.getNewValue()));
                            Family f = familyTableView.getSelectionModel().getSelectedItem();
                            Port p = portInsTableView.getSelectionModel().getSelectedItem();
                            list = FamilyDAOMYSQLImpl.getInstance().selectInMeasureTechnique(f);
                            for (int i = 0; i < list.size(); i++) {
                                //permetto l'associazione :
                                //se il sensing element è di tipo direct, controllo che la misura direct, controllo se la associo
                                //a una porta interna, controllo se la associo a una porta che si chiama come al sensing element
                                if (se.get(0).getMeasureTechnique().equals("DIRECT")) {
                                    if (p.isInternal()) {
                                       /* if (p.getName().equals(se.get(0).getIdSPSensingElement())) {
                                            if (se.get(0).getMeasureTechnique().equals(list.get(i).getType()))*/ {
                                                ok = true;
                                            }


                                        } /*else {
                                            Alert alert = new Alert(Alert.AlertType.WARNING);
                                            alert.initOwner(this.getPrimaryStage());
                                            alert.setTitle("Warning!");
                                            alert.setHeaderText("The selected Sensing Element must have the same name\n of the selected Port");
                                            alert.setContentText("Please select a valid Sensing Element.");

                                            alert.showAndWait();
                                            //deseleziono e seleziono la combo in maniera tale da far scomparire il valore volatile all'interno
                                            //della combo dovuto dall'inserimento andato male
                                            int appo = portInsTableView.getSelectionModel().getSelectedIndex();
                                            portInsTableView.getSelectionModel().clearSelection();
                                            portInsTableView.getSelectionModel().select(appo);
                                            return;
                                        }
                                    } */else {
                                        Alert alert = new Alert(Alert.AlertType.WARNING);
                                        alert.initOwner(stage);
                                        alert.setTitle("Warning!");
                                        alert.setHeaderText("The selected Sensing Element must be on an internal Port");
                                        alert.setContentText("Please select a valid Port.");

                                        alert.showAndWait();
                                        int appo = portInsTableView.getSelectionModel().getSelectedIndex();
                                        portInsTableView.getSelectionModel().clearSelection();
                                        portInsTableView.getSelectionModel().select(appo);
                                        return;
                                    }
                                } else if (se.get(0).getMeasureTechnique().equals(list.get(i).getType())) {
                                    ok = true;
                                }
                            }

                            if (!ok) {
                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                                alert.initOwner(stage);
                                alert.setTitle("Warning!");
                                alert.setHeaderText("The selected Sensing Element need \n\"" + se.get(0).getMeasureTechnique() + "\" as Measure Technique.");
                                alert.setContentText("Do you wish to insert it?");

                                ButtonType buttonTypeOne = new ButtonType("Yes");
                                ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
                                alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

                                Optional<ButtonType> result = alert.showAndWait();
                                if (result.get() == buttonTypeOne) {
                                    List<MeasureTechnique> mtNotIn = FamilyDAOMYSQLImpl.getInstance().selectMeasureTechniqueNotIn(f);
                                    MeasureTechnique measure = null;
                                    for (int j = 0; j < mtNotIn.size(); j++) {
                                        if (se.get(0).getMeasureTechnique().equals(mtNotIn.get(j).getType())) {
                                            measure = mtNotIn.get(j);
                                        }
                                    }
                                    if (measure != null) {
                                        FamilyDAOMYSQLImpl.getInstance().insertMeasureTechnique(f, measure);
                                        outPut += "Measure Technique and ";

                                        ok = true;
                                    }
                                    addMeasure(measure);
                                    removeMeasure(measure);
                                    //this.getMeasureTInsData().add(measure);   //inserisco l'elemento nella tabella in maniera volatile
                                    //this.getMeasureTDispData().remove(measure);   //rimuovo l'elemento nella tabella in maniera volatile
                                    showMeasureTechniqueAvailable(f);
                                } else {
                                    int appo = portInsTableView.getSelectionModel().getSelectedIndex();
                                    portInsTableView.getSelectionModel().clearSelection();
                                    portInsTableView.getSelectionModel().select(appo);
                                }
                            }

                            if (ok) {
                                if (t.getOldValue().length() != 0) {
                                    FamilyDAOMYSQLImpl.getInstance().deleteSensingElementInPort(f, p);
                                }
                                if (t.getNewValue().length() != 0) {
                                    p.setSeAssociated(t.getNewValue());
                                    p.setSeNameAssociated(t.getNewValue()); //AGGIUNTO
                                    FamilyDAOMYSQLImpl.getInstance().insertSensingElementInPort(f, p);
                                    outPut += "SensingElement correctly inserted.";
                                    outPutMessage(outPut, -1);
                                }
                            }
                        } catch (DAOException e) {
                            outPutMessage("WARNING! Some Error occured.", -1);
                            e.printStackTrace();
                            Alert alertError = new Alert(Alert.AlertType.ERROR);
                            alertError.initOwner(stage);
                            alertError.setTitle("Error during DB interaction");
                            alertError.setHeaderText("Error during delete ...");
                            alertError.setContentText(e.getMessage());

                            alertError.showAndWait();
                        }
                    }
                }
        );

        //apro il sensng element selezionato con doppio click
        /*portInsTableView.setOnMouseClicked((mouseEvent) -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2
                    && portInsTableView.getSelectionModel().getSelectedItem().getSeAssociated().length() != 0) {

                this.getSensingElementController().cercaSensingElement(portInsTableView.getSelectionModel().getSelectedItem().getSeAssociated());
                this.getHomePageController().selectTab(this.getHomePageController().getSensingElementTab());
            }
        });*/

        familyTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2 && familyTableView.getItems().size() != 0) {
                        modificaButtonAction();
                    } else if (mouseEvent.getClickCount() == 2 && familyTableView.getItems().size() == 0) {
                        nuovoButtonAction();
                    }
                }
            }
        });

        // Clear Family details.
        showFamilyDetails(null);


        familyTableView.getItems().addListener((ListChangeListener.Change<? extends Family> c) -> {
            if (c.getList().size() == 0) {
                modificaButton.setDisable(true);
                cancellaButton.setDisable(true);
            } else {
                modificaButton.setDisable(false);
                cancellaButton.setDisable(false);
            }
        });

        // Enable multiple selection for portDispTableView elements
        portDispTableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );
        // Enable multiple selection for portInsTableView elements
        portInsTableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );
        // Enable multiple selection for portDispTableView elements
        measureTDispTableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );
        // Enable multiple selection for portInsTableView elements
        measureTInsTableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );


        portInsTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    //quando seleziono una porta inserita nella colonna della tabella
                    //l'ascoltatore ascolta il cambaimento inserisce nella cella della colonna il sensing element
                    //associato alla porta(se presente)e popola gli elementi al'interno della combox
                    seSPPortInsColumn.setCellFactory(ComboBoxTableCell.forTableColumn(
                            getItem(familyTableView.getSelectionModel().getSelectedItem(),
                                    portInsTableView.getSelectionModel().getSelectedItem())));
                    //seSPPortInsColumn.setCellFactory(comboBoxFactory);
                }
        );

        familyTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    showFamilyDetails(newValue);
                    showPortAvailable(newValue);
                    showMeasureTechniqueAvailable(newValue);
                });
    }

    private void addMeasure(MeasureTechnique m){
        this.getMeasureTInsData().add(m);
        return;
    }

    private void removeMeasure(MeasureTechnique m){
        this.getMeasureTInsData().remove(m);
        return;
    }

    //questo metodo è richiamato quando selezioniamo un elemento nella colonna della tabella familyTableView
    //e va a popolare la colonna della tabella portDispTableView e portInsTableView
    private void showPortAvailable(Family f) {
        if (f != null) {
            List<Port> list;
            SensingElement se;

            try {
                list = FamilyDAOMYSQLImpl.getInstance().selectPortNotIn(f);
                this.getPortDispData().clear();
                this.getPortDispData().addAll(list);
                list = FamilyDAOMYSQLImpl.getInstance().selectInPort(f);
                this.getPortInsData().clear();
                this.getPortInsData().addAll(list);
                //per porta inserita va a associare un sensing element (se presente)
                for (int i = 0; i < this.getPortInsData().size(); i++) {
                    se = (SensingElement) FamilyDAOMYSQLImpl.getInstance().selectSensingElementIn(f, this.getPortInsData().get(i));
                    this.getPortInsData().get(i).setSeAssociated(se == null ? "" : se.getIdSPSensingElement());
                    this.getPortInsData().get(i).setSeNameAssociated(se==null ?"": se.getName()); //AGGIUNTO NOMESE
                }

                //this.getPortInsData().get(1).setSeAssociated("kasjd");
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
    }

    //questo metodo è richiamato quando selezioniamo un elemento nella colonna della tabella familyTableView
    //e va a popolare la colonna della tabella measureTDispTableView e measureTInsTableView
    private void showMeasureTechniqueAvailable(Family f) {
        if (f != null) {
            List<MeasureTechnique> list = null;
            try {
                list = FamilyDAOMYSQLImpl.getInstance().selectMeasureTechniqueNotIn(f);
                this.getMeasureTDispData().clear();
                this.getMeasureTDispData().addAll(list);
                list = FamilyDAOMYSQLImpl.getInstance().selectInMeasureTechnique(f);
                this.getMeasureTInsData().clear();
                this.getMeasureTInsData().addAll(list);
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
    }


    private void showFamilyDetails(Family f) {
        if (f != null) {
            // Fill the labels with info from the person object.
            idLabel.setText(f.getId());
            idspfamilyLabel.setText(f.getIdSPFamily());

            hwVersionLabel.setText(f.getHwVersion());
            sysclockLabel.setText(f.getSysclock());
            osctrimLabel.setText(f.getOsctrim());
            multicastDefaultLabel.setText(f.getMulticastDefault());
            broadcastLabel.setText(f.getBroadcast());
            broadcastSlowLabel.setText(f.getBroadcastSlow());

        } else {

            // Person is null, remove all the text.
            idLabel.setText("");
            idspfamilyLabel.setText("");

            hwVersionLabel.setText("");
            sysclockLabel.setText("");
            osctrimLabel.setText("");
            multicastDefaultLabel.setText("");
            broadcastLabel.setText("");
            broadcastSlowLabel.setText("");
        }
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void cancellaButtonAction() {
        //the function below returns the index of the selected item in the TableView
        int selectedIndex = familyTableView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            //definisco e implemento un elemento della classe Family a cui ci passo l'emento selezionato nella tabella
            Family family = familyTableView.getItems().get(selectedIndex);

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initOwner(stage);
            alert.setTitle("Are you sure?");
            alert.setHeaderText("Delete " + family.getIdSPFamily() + "?");
            alert.setContentText("Are you sure that you want to delete the selected item?");

            ButtonType buttonTypeOne = new ButtonType("Yes");
            ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                try {
                    FamilyDAOMYSQLImpl.getInstance().delete(family);
                    familyTableView.getItems().remove(family);
                    outPutMessage("Family correcly deleted.", 1);
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alertError = new Alert(Alert.AlertType.ERROR);
                    alertError.initOwner(this.getPrimaryStage());
                    alertError.setTitle("Error during DB interaction");
                    alertError.setHeaderText("Error during delete ...");
                    alertError.setContentText(e.getMessage());

                    alertError.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    public void modificaButtonAction() {
        Family tmpFamily = familyTableView.getSelectionModel().getSelectedItem();
        if (tmpFamily != null) {
            tmpFamily.setOldidSPFamily();
            boolean okClicked = this.showFamilyEditDialog(stage, tmpFamily, true);
            if (okClicked) {
                try {
                    FamilyDAOMYSQLImpl.getInstance().update(tmpFamily);
                    showFamilyDetails(tmpFamily);
                    outPutMessage("Family correctly updated.", 1);
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during update ...");
                    alert.setContentText(e.getMessage());

                    alert.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    private void nuovoButtonAction() {
        Family tempFamily = new Family("","","RUN5","10000000","0X06","0x0000000002","0x0000000001","0x0000000000");
        boolean okClicked = this.showFamilyEditDialog(stage, tempFamily, true);
        if (okClicked) {
            try {
                FamilyDAOMYSQLImpl.getInstance().insert(tempFamily);
                this.getFamilyData().add(tempFamily);
                outPutMessage("Family correctly inserted.", 1);
            } catch (DAOException e) {
                outPutMessage("WARNING! Some Error occured.", -1);
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during insert ...");
                alert.setContentText(e.getMessage());

                alert.showAndWait();
            }
        }
    }

    @FXML
    public void cercaButtonAction() {
        Family tempFamily = new Family();
        boolean okClicked = this.showFamilyEditDialog(stage, tempFamily, false);
        if (okClicked) {
            try {
                List<Family> list = FamilyDAOMYSQLImpl.getInstance().select(tempFamily);
                this.getFamilyData().clear();
                this.getFamilyData().addAll(list);
            } catch (DAOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during search ...");
                alert.setContentText(e.getMessage());
                alert.showAndWait();
            }
        }
    }

    @FXML
    public void insertPortButtonAction() {
        ObservableList<Port> portToIns = FXCollections.observableArrayList();
        portToIns.addAll(portDispTableView.getSelectionModel().getSelectedItems());
        Family selectedFamily = familyTableView.getSelectionModel().getSelectedItem();
        if (selectedFamily != null) {
            if (portToIns.size() != 0) {
                try {
                    for (Port port : portToIns) {
                        FamilyDAOMYSQLImpl.getInstance().insertPort(selectedFamily, port);
                        this.getPortInsData().add(port);
                        this.getPortInsData().sort(Comparator.comparingInt(Port::getIdSPPort));
                        this.getPortDispData().remove(port);
                    }
                    outPutMessage("Port correcly inserted.", portToIns.size());
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during insert ...");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Selection");
                alert.setHeaderText("No Port Selected");
                alert.setContentText("Please select a Port in the Porte Disponibili table.");

                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    public void insertAllPortButtonAction() {
        ObservableList<Port> portToIns = FXCollections.observableArrayList();
        portToIns.addAll(this.getPortDispData());
        Family selectedFamily = familyTableView.getSelectionModel().getSelectedItem();
        if (selectedFamily != null) {
            if (portToIns.size() != 0) {
                try {
                    for (Port port : portToIns) {
                        FamilyDAOMYSQLImpl.getInstance().insertPort(selectedFamily, port);
                        this.getPortInsData().add(port);
                        this.getPortInsData().sort(Comparator.comparingInt(Port::getIdSPPort));
                        this.getPortDispData().remove(port);
                    }
                    outPutMessage("All Port has been inserted.", portToIns.size());
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during insert ...");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Port");
                alert.setHeaderText("No Port Available");
                alert.setContentText("There is no Port to add.");

                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    private void cancellaPortButtonAction() {
        //the function below returns the index of the selected item in the TableView
        ObservableList<Port> portToRemove = FXCollections.observableArrayList();
        portToRemove.addAll(portInsTableView.getSelectionModel().getSelectedItems());
        //definisco un handler  di classe family e gli facciamo puntare all'elemento selezionato nella colonan della tabella familyTableView
        Family family = familyTableView.getSelectionModel().getSelectedItem();
        if (family != null) {
            if (portToRemove.size() != 0) {

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.initOwner(stage);
                alert.setTitle("Are you sure?");
                alert.setHeaderText("Delete selected Port(s)?");
                alert.setContentText("Are you sure that you want to delete the selected item(s)?");

                ButtonType buttonTypeOne = new ButtonType("Yes");
                ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
                alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == buttonTypeOne) {
                    try {
                        for (Port port : portToRemove) {
                            FamilyDAOMYSQLImpl.getInstance().deletePort(family, port);
                            portInsTableView.getItems().remove(port);   //elimino l'elemento nella tabella in maniera volatile
                            this.getPortDispData().add(port);   //inserisco l'elemento nella tabella in maniera volatile
                            this.getPortDispData().sort(Comparator.comparingInt(Port::getIdSPPort));
                            //pulisco il valore all'interno della combobox
                            showPortAvailable(family);//setto la variabile SeAssociated con una stringa vuota. Faccio questo perchè nel caso in cui andassi a
                            //rinserire la porta mi darebbe ancora il sensing element attaccato. Questa è solo una info volatile, il database è aggiornato,
                            //pero la query viene chiamata solo quando selezioniamo la famiglia.

                            outPutMessage("Port correcly deleted.", 1);
                        }
                    } catch (DAOException e) {
                        outPutMessage("WARNING! Some Error occured.", -1);
                        e.printStackTrace();
                        Alert alerter = new Alert(Alert.AlertType.ERROR);
                        alerter.initOwner(this.getPrimaryStage());
                        alerter.setTitle("Error during DB interaction");
                        alerter.setHeaderText("Error during delete ...");
                        alerter.setContentText(e.getMessage());
                        alerter.showAndWait();
                    }
                }
                //DELETE FROM `sensichipsdb`.`spsensingelement` WHERE `idSPSensingElement`='788';
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Selection");
                alert.setHeaderText("No Port Selected");
                alert.setContentText("Please select a Port in the Porte Inserite table.");
                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");
            alert.showAndWait();
        }
    }

    @FXML
    public void cancellaAllPortButtonAction() {
        Family selectedFamily = familyTableView.getSelectionModel().getSelectedItem();
        ObservableList<Port> portToRemove = FXCollections.observableArrayList();
        portToRemove.addAll(this.getPortInsData());
        if (selectedFamily != null) {
            if (this.getPortInsData().size() != 0) {
                try {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.initOwner(stage);
                    alert.setTitle("Are you sure?");
                    alert.setHeaderText("Delete All Ports?");
                    alert.setContentText("Are you sure that you want to delete all items?");

                    ButtonType buttonTypeOne = new ButtonType("Yes");
                    ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
                    alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == buttonTypeOne) {
                        FamilyDAOMYSQLImpl.getInstance().deleteAllPort(selectedFamily);
                        outPutMessage("All Port has been deleted.", this.getPortInsData().size());
                        this.getPortDispData().addAll(this.getPortInsData());
                        this.getPortDispData().sort(Comparator.comparingInt(Port::getIdSPPort));
                        this.getPortInsData().clear();
                        for (Port port : portToRemove) {
                            port.setSeAssociated("");//setto la variabile SeAssociated con una stringa vuota. Faccio questo perchè nel caso in cui andassi a
                            //rinserire la porta mi darebbe ancora il sensing element attaccato. Questa è solo una info volatile, il database è aggiornato,
                            //pero la query viene chiamata solo quando selezioniamo la famiglia.
                        }
                    }

                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during delete ...");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Port");
                alert.setHeaderText("No Port Available");
                alert.setContentText("There is no Port to remove.");
                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    private void inserireMeasureTechniqueButtonAction() {
        //the function below returns the index of the selected item in the TableView
        ObservableList<MeasureTechnique> measureTechniqueToIns = FXCollections.observableArrayList();
        measureTechniqueToIns.addAll(measureTDispTableView.getSelectionModel().getSelectedItems());
        //definisco un handler  di classe family e gli facciamo puntare all'elemento selezionato nella colonan della tabella familyTableView
        Family family = familyTableView.getSelectionModel().getSelectedItem();
        if (family != null) {
            if (measureTechniqueToIns.size() != 0) {
                try {
                    for (MeasureTechnique measure : measureTechniqueToIns) {
                        FamilyDAOMYSQLImpl.getInstance().insertMeasureTechnique(family, measure);
                        this.getMeasureTInsData().add(measure);   //inserisco l'elemento nella tabella in maniera volatile
                        this.getMeasureTDispData().remove(measure);   //rimuovo l'elemento nella tabella in maniera volatile
                        //Ordino la tabella dove ho inserito il nuovo elemento
                        this.getMeasureTInsData().sort(Comparator.comparingInt(MeasureTechnique::getIdSPMeasureTechnique));
                        outPutMessage("Measure Technique correcly inserted.", 1);
                    }
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during insert ...");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Selection");
                alert.setHeaderText("No MeasureTechnique Selected");
                alert.setContentText("Please select a MeasureTechnique in the Measure Technique Associate table.");
                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");
            alert.showAndWait();
        }
    }

    @FXML
    public void insertAllMeasureTechniqueButtonAction() {
        ObservableList<MeasureTechnique> measureTechniqueToIns = FXCollections.observableArrayList();
        measureTechniqueToIns.addAll(this.getMeasureTDispData());
        Family selectedFamily = familyTableView.getSelectionModel().getSelectedItem();
        if (selectedFamily != null) {
            if (measureTechniqueToIns.size() != 0) {
                try {
                    for (MeasureTechnique measureTechnique : measureTechniqueToIns) {
                        FamilyDAOMYSQLImpl.getInstance().insertMeasureTechnique(selectedFamily, measureTechnique);
                        this.getMeasureTInsData().add(measureTechnique);
                        this.getMeasureTInsData().sort(Comparator.comparingInt(MeasureTechnique::getIdSPMeasureTechnique));
                        this.getMeasureTDispData().remove(measureTechnique);
                    }
                } catch (DAOException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during insert ...");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Measure Technique");
                alert.setHeaderText("No Measure Technique Available");
                alert.setContentText("There is no Port to add.");

                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");

            alert.showAndWait();
        }
    }

    @FXML
    private void cancellaMeasureTechniqueButtonAction() {
        //the function below returns the index of the selected item in the TableView
        boolean ok = true;
        List<SensingElement> se;
        SensingElement s = new SensingElement();
        ObservableList<MeasureTechnique> measureTechniqueToRemove = FXCollections.observableArrayList();
        measureTechniqueToRemove.addAll(measureTInsTableView.getSelectionModel().getSelectedItems());
        //definisco un handler  di classe family e gli facciamo puntare all'elemento selezionato nella colonan della tabella familyTableView
        Family family = familyTableView.getSelectionModel().getSelectedItem();
        List<Port> p = this.getPortInsData();


        if (family != null) {
            if (measureTechniqueToRemove.size() != 0) {
                //in questo ciclo for annidato vado a variare una variabile booleana che parte inizialmente da false.
                //ora se la misura che voglio eliminare, è una misura presente in un sensing element associato a una delle porte esistenti
                //allora la cancellazione non deve essere permessa e mi restituisce un warning


                try {
                    for (MeasureTechnique measure : measureTechniqueToRemove) {
                        for (Port port : p) {
                            s.setidSPSensingElement(port.getSeAssociated());
                            s.setidSPSensingElement(port.getSeNameAssociated()); //AGGIUNTO
                            if (!s.getIdSPSensingElement().equals("")) {
                                se = SensingElementDAOMySQLImpl.getInstance().select(s);
                                if (se.get(0).getMeasureTechnique().equals(measure.getType())) {
                                    ok = false;
                                }
                            }
                        }

                        if (ok) {
                            FamilyDAOMYSQLImpl.getInstance().deleteMeasureTechnique(family, measure);
                            measureTInsTableView.getItems().remove(measure);   //elimino l'elemento nella tabella in maniera volatile
                            this.getMeasureTDispData().add(measure);   //inserisco l'elemento nella tabella in maniera volatile
                            this.getMeasureTDispData().sort(Comparator.comparingInt(MeasureTechnique::getIdSPMeasureTechnique));
                            outPutMessage("Measure Technique correcly deleted.", 1);
                        } else {
                            // Nothing selected.
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.initOwner(this.getPrimaryStage());
                            alert.setTitle("Delete not allowed");
                            alert.setHeaderText("You are trying to delete a Measure Technique used by \n one or more Sensing Element.");
                            alert.setContentText("Please remove the intested SensingElement in order to proceed with delete.");
                            alert.showAndWait();
                        }

                    }

                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during delete ...");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Selection");
                alert.setHeaderText("No MeasureTechnique Selected");
                alert.setContentText("Please select a MeasureTechnique in the Measure Technique Associate table.");
                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");
            alert.showAndWait();
        }
    }

    @FXML
    public void cancellaAllMeasureTechniqueButtonAction() {
        boolean ok = true;
        List<SensingElement> se;
        SensingElement s = new SensingElement();
        Family selectedFamily = familyTableView.getSelectionModel().getSelectedItem();
        ObservableList<MeasureTechnique> measureTechniqueToRemove = FXCollections.observableArrayList();
        measureTechniqueToRemove.addAll(this.getMeasureTInsData());//associo ala variabile tutte le isure da cancellare
        List<Port> p = this.getPortInsData();
        if (selectedFamily != null) {
            if (this.getMeasureTInsData().size() != 0) {
                try {
                    //in questo ciclo for annidato vado a variare una variabile booleana che parte inizialmente da false.
                    //ora se la misura che voglio eliminare, è una misura presente in un sensing element associato a una delle porte esistenti
                    //allora la cancellazione non deve essere permessa e mi restituisce un warning
                    for (MeasureTechnique measure : measureTechniqueToRemove) {
                        for (Port port : p) {
                            s.setidSPSensingElement(port.getSeAssociated());
                            se = SensingElementDAOMySQLImpl.getInstance().select(s);
                            if (se.get(0).getMeasureTechnique().equals(measure.getType()))
                                ok = false;
                        }
                    }

                    if (ok) {
                        FamilyDAOMYSQLImpl.getInstance().deleteAllMeasureTechnique(selectedFamily);
                        outPutMessage("All Measure Technique has been deleted.", this.getMeasureTInsData().size());
                        this.getMeasureTDispData().addAll(this.getMeasureTInsData());
                        this.getMeasureTDispData().sort(Comparator.comparingInt(MeasureTechnique::getIdSPMeasureTechnique));
                        this.getMeasureTInsData().clear();
                    } else {
                        // Nothing selected.
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.initOwner(this.getPrimaryStage());
                        alert.setTitle("Delete not allowed");
                        alert.setHeaderText("You are trying to delete a Measure Technique used by \n one or more Sensing Element.");
                        alert.setContentText("Please remove the intested SensingElement in order to proceed with delete.");
                        alert.showAndWait();

                    }
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during delete ...");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            } else {
                // Nothing selected.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("No Measure Technique");
                alert.setHeaderText("No Measure Technique Available");
                alert.setContentText("There is no Measure Technique to remove.");
                alert.showAndWait();
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Family Selected");
            alert.setContentText("Please select a Family in the table.");

            alert.showAndWait();
        }
    }

    public boolean showFamilyEditDialog(Stage owner, Family f, boolean check) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("FamilyEditDialog.fxml"));
            AnchorPane dialog = loader.load();

            Scene scene = new Scene(dialog);
            Stage fDialog = new Stage();
            fDialog.setTitle("Edit Family");
            fDialog.initModality(Modality.WINDOW_MODAL);
            fDialog.initOwner(owner);
            fDialog.setScene(scene);
            fDialog.getIcons().add(new Image(getClass().getResourceAsStream("../../../resources/sensichips_logo.png")));

            FamilyEditController controller = loader.getController();
//            controller.setMain(this, check);
            controller.setStage(fDialog);
            controller.setFamily(f);

            fDialog.showAndWait();

            return controller.okClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}