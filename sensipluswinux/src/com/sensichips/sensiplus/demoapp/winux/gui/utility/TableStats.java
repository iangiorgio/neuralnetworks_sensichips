package com.sensichips.sensiplus.demoapp.winux.gui.utility;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.demoapp.winux.gui.Chart;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sun.javafx.scene.control.skin.TableHeaderRow;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import static com.sensichips.sensiplus.config.SPConfigurationManager.getSPConfigurationDefault;


/**
 *
 * Class for setting-up the table under the Plot
 *
 *
 */


public class TableStats {

    //Create an observable list of Stats objects contained within the TableView items list

    private TableView<Stats> table;

    //The attribute is final, so that, a copy of data has been made before sending it to methods. This means
    // that inside the method data can not be reassigned

    private final ObservableList<Stats> data = FXCollections.observableArrayList();



    public TableStats(){
        table = new TableView<>();


        //Set the column of the numbers of Chip

        TableColumn chipCol = new TableColumn("Chip ID");
        chipCol.setCellValueFactory(new PropertyValueFactory<Stats, String>("chipID"));
        chipCol.setStyle( "-fx-alignment: CENTER;-fx-font-weight: bold");

        chipCol.setCellFactory(column -> new TableCell<Stats, String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setText(null);

                } else {

                    setText(item);
                    // Style row where balance < 0 with a different color.

                    String chipID = item;
                    try {
                        List<SPChip> chipList = getSPConfigurationDefault().getCluster().getActiveSPChipList();
                        int ind = 0;
                        while (ind < chipList.size() && !chipList.get(ind).getSerialNumber().equals(chipID)) {
                            ind++;
                        }

                        this.setTextFill(Chart.colorList[ind]);
                    }catch (SPException spe){
                        spe.printStackTrace();
                    }
                    /*TableRow currentRow = getTableRow();
                    if (item.equals("1070")) {
                        currentRow.setStyle("-fx-background-color: tomato;");

                    } else currentRow.setStyle("");*/
                }
            }
        });

        /*chipCol.setCellFactory(new Callback<TableColumn<Stats, String>, TableCell<Stats, String>>() {
            @Override
            public TableCell<Stats, String> call(TableColumn<Stats, String> param) {

                //System.out.println(param.getColumns().removeAll());
                //param.getTableView().getItems().removeAll();
                return new TableCell<Stats, String>() {

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        try {
                            if (item != null) {
                                setText(item);

                                String chipID = item;

                                List<SPChip> chipList = getSPConfigurationDefault().getCluster().getActiveSPChipList();
                                int ind = 0;
                                while (ind < chipList.size() && !chipList.get(ind).getSerialNumber().equals(chipID)){
                                    ind++;
                                }

                                this.setTextFill(Chart.colorList[ind]);


                            }

                        }catch (SPException ex){
                            ex.printStackTrace();
                        }
                    }
                };
            }

        });*/


        //Set the columns of the global parameters

        TableColumn minValueColumn = new TableColumn("Min");
        minValueColumn.setCellValueFactory(new PropertyValueFactory<Stats, String>("MinValue"));
        minValueColumn.setStyle("-fx-font-weight: bold");


        TableColumn maxValueColumn = new TableColumn("Max");
        maxValueColumn.setCellValueFactory(new PropertyValueFactory<Stats, String>("MaxValue"));
        maxValueColumn.setStyle("-fx-font-weight: bold");

        TableColumn maxSNRvalueColumn = new TableColumn("Max SNR");
        maxSNRvalueColumn.setCellValueFactory(new PropertyValueFactory<Stats, String>("MaxSNR"));
        maxSNRvalueColumn.setStyle("-fx-font-weight: bold");

        TableColumn noiseValueColumn = new TableColumn("Noise (\u03C3)");
        noiseValueColumn.setCellValueFactory(new PropertyValueFactory<Stats, String>("NoiseDevStd"));
        noiseValueColumn.setStyle("-fx-font-weight: bold");

        TableColumn SNRValueColumn = new TableColumn("SNR");
        SNRValueColumn.setCellValueFactory(new PropertyValueFactory<Stats, String>("SNR"));
        SNRValueColumn.setStyle("-fx-font-weight: bold");


        table.getColumns().addAll(chipCol,minValueColumn,maxValueColumn,maxSNRvalueColumn,noiseValueColumn,SNRValueColumn);

        table.setItems(data);

        //Set the size of columns

        table.setColumnResizePolicy(table.CONSTRAINED_RESIZE_POLICY);
    }

    public TableView<Stats> get(){
        return table;
    }

    //remove all the table elements

    public void clear(){
        data.removeAll();
        for ( int i = 0; i<table.getItems().size(); i++) {
            table.getItems().clear();
        }
    }

    public void addStats(Stats[] list){

        //for statement that iterate over all list items

        for(Stats item:list){
            data.add(item);
        }
    }

    public void addStatistic(Stats stat){
        table.getItems().add(stat);
    }

}
