package com.sensichips.sensiplus.demoapp.winux;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.demoapp.winux.gui.Controller;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMBLE;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_IP;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.util.SPDataRepresentation;
import com.sensichips.sensiplus.util.SPDataRepresentationManager;
import com.sensichips.sensiplus.util.batch.SPBatchExecutor;
import com.sensichips.sensiplus.util.batch.SPBatchExecutorObservator;
import com.sensichips.sensiplus.util.batch.SPBatchExperiment;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import com.sensichips.sensiplus.util.winux.SPBatchCommandLineAnalyzer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import jssc.SerialPortList;
import sun.misc.Signal;
import sun.misc.SignalHandler;


import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class SPWinuxTools extends Application implements SPConfigurationManager.SPDriverGenerator, SPBatchExecutorObservator {

    public static String MAIN_TITLE = "SENSIPLUS WinuxGUI - Unicas for Sensichips, Ver 0.4";
    public static String LOG_MESSAGE = "SPWinuxTools";
    private static int DEBUG_LEVEL_ERROR = SPLoggerInterface.DEBUG_VERBOSITY_ERROR;

    public static String APPLICATION_ICON; // = this.getClass().getResource("/com/sensichips/sensiplus/resources/ic_launcher.png").toExternalForm();
    public static String SPLASH_IMAGE; // = "file:/images/splash.png";

    private PrintWriter globalFile = null;
    private SPBatchParameters spBatchParameters;
    private SPBatchExecutor spBatchExecutor;


    public SPWinuxTools(){}

    public SPWinuxTools(SPBatchParameters spBatchParameters){
        this.spBatchParameters = spBatchParameters;
    }

    public void setSpBatchExecutor(SPBatchExecutor spBatchExecutor){
        this.spBatchExecutor = spBatchExecutor;
    }



    @Override
    public void newMeasure(String row, List<SPDataRepresentation> spDataRepresentationList, String className){
        if (row.length() > 0){
            SPLogger.getLogInterface().d(LOG_MESSAGE, row, SPLoggerInterface.DEBUG_VERBOSITY_ANY);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "class name: " + className, SPLoggerInterface.DEBUG_VERBOSITY_ANY);

            if(globalFile != null) {
                globalFile.println(row);
                globalFile.flush();
            }
        }
    }
    @Override
    public void measureStarted(SPBatchExperiment currentExperiment,String[] unitMeasure){
        try {
            globalFile = new PrintWriter(spBatchParameters.outputDir + "Experiment_" + currentExperiment.getDate() + "_GLOBAL_" + currentExperiment.getId() + ".csv");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void measureFinished(boolean stopped){
        if (globalFile != null){
            globalFile.close();
        }
    }


    @Override
    public void updateInfo(float deltaT, float measureForSecond, float progress){

    }

    @Override
    public void newSetting(String row) {

    }

    public boolean classifySample(){
        return true;
    }

    public void setClass(String className){
        System.out.println("Class belonging to: " + className);
    }


    public static SPBatchCommandLineAnalyzer command;

    public static void main(String[] args) {

        try {

            // Analyze command line parameters
            command = new SPBatchCommandLineAnalyzer(args);

            if (!command.getBatchParameters().valid){
                System.exit(-1);
            }

            if(command.getBatchParameters().gui){
                launch(args);
            } else {
                startWithoutGUI(command);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(-1);
    }

    private static void startWithoutGUI(SPBatchCommandLineAnalyzer command) throws Exception {
        SPWinuxTools winuxTools = new SPWinuxTools(command.getBatchParameters());

        SPConfigurationManager.loadConfigurationsFromFile(new FileInputStream(new File(command.getBatchParameters().layoutConfigurationFileXML)));
        //SPConfigurationManager.setDefaultSPConfiguration("WINUX_COMUSB-CLUSTERID_0X02-FT232RL_SENSIBUS-ShortAddress-ALL_CHIP(RUN5)");


        verifySerialPort();




        //SPConfigurationManager.setDefaultSPConfiguration(command.getBatchParameters().currentConfiguration);

        Thread.sleep(500);
        SPConfigurationManager.getSPConfigurationDefault().turnOffGreenLed();

        // Load configurations
        SPBatchExecutor spBatchExecutor = new SPBatchExecutor(command.getBatchParameters(), winuxTools, SPConfigurationManager.getSPConfigurationDefault());
        if (spBatchExecutor.getExperimentList().size() <= 0){
            throw new SPException("Any experiment defined in " + command.getBatchParameters().batchFile);
        }

        // Generate executor
        winuxTools.setSpBatchExecutor(spBatchExecutor);

        // Execute experiments
        spBatchExecutor.executeExperiments();


        Signal.handle(new Signal("INT"), new SignalHandler() {
            final SPProtocol prot = SPConfigurationManager.getSPConfigurationDefault().getSPProtocol();
            public void handle(Signal signal) {
                try {
                    SPLogger.getLogInterface().d(LOG_MESSAGE,"\nSENSIPLUS powering off...\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                    prot.closeAll();
                } catch (SPException e) {
                    e.printStackTrace();
                }
                SPLogger.getLogInterface().d(LOG_MESSAGE,"Program closed with ctrl+c", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                System.exit(-1);
            }
        });
    }



    public void start(Stage primaryStage) throws Exception {
        long start = System.currentTimeMillis();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gui/maingui.fxml"));
        System.out.println("Elapsed 1: " + (System.currentTimeMillis() - start));
        Parent root = (Parent)loader.load();
        System.out.println("Elapsed 2: " + (System.currentTimeMillis() - start));
        primaryStage.setResizable(false);
        primaryStage.setTitle(MAIN_TITLE);
        primaryStage.setScene(new Scene(root));
        //primaryStage.getIcons().add(new Image(this.getClass().getResource("/com/sensichips/sensiplus/resources/ic_launcher.png").toExternalForm()));
        primaryStage.getIcons().add(new Image(SPWinuxTools.APPLICATION_ICON));
        //primaryStage.getIcons().add(new Image("file:resources/images/ic_launcher.png"));

        Controller controller = loader.getController();
        controller.setStageAndSetupListeners(primaryStage);
        System.out.println("Elapsed 3: " + (System.currentTimeMillis() - start));

        primaryStage.show();
        System.out.println("Elapsed 4: " + (System.currentTimeMillis() - start));
    }

    private boolean toggle = true;



    @Override
    public String generateDriver(String driverName, ArrayList<String> driverParameters) throws SPDriverException {
        SPDriver sp;
        if (driverName.equals(SPDriverWINUX_COMUSB.DRIVERNAME)){
            sp = new SPDriverWINUX_COMUSB(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_COMBLE.DRIVERNAME)){
            sp = new SPDriverWINUX_COMBLE(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_IP.DRIVERNAME)){
            sp = new SPDriverWINUX_IP(driverParameters);
        } else {
            throw new SPDriverException("Wrong driverName in generateDriver in SPWinuxTools");
        }
        SPDriverManager.add(sp);
        return sp.getDriverKey();
    }


    private static void verifySerialPort() throws Exception {
        String[] portNames = SerialPortList.getPortNames();
        if (portNames.length > 0){
            System.out.println("SERIAL PORT AVAILABLE:");
            for(int i = 0; i < portNames.length; i++){
                System.out.println("\t" + (i + 1) + ". " + portNames[i]);
            }
        } else {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "ANY DEVICE FOUND:", DEBUG_LEVEL_ERROR);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "On Linux please verify if /dev/ttyUSBX is available", DEBUG_LEVEL_ERROR);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Try also with these settings: sudo chown 777 /dev/ttyUSBX", DEBUG_LEVEL_ERROR);
            throw new Exception("ANY DEVICE FOUND, THE PROGRAM WILL EXIT!!");

        }
    }

}
