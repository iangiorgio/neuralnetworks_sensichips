package com.sensichips.xmlclientconfiguration;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.Enumeration;

public class XMLServer extends Thread {


    //private static SPConfiguration spConfigurationsList[] = null;
    //private SPBatchParameters spBatchParameters = null;
    PersonArray personArrays[] = null;

    public XMLServer() {




        personArrays = new PersonArray[2];

        for(int i = 0; i < personArrays.length; i++){
            personArrays[i] = new PersonArray();
            for(int j = 0; j < personArrays[i].persons.length; j++){
                personArrays[i].persons[j] = new Person("n_" + i + "_" + j, "c_" + i + "_" + j);
            }
        }


    }


    public void run(){

        ServerSocket ss = null;
        while(ss == null) {

            try {
                ss = new ServerSocket(9999);
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
                try {
                    sleep(10000); // Wait before retry
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.err.println("Retry to open the server socket...");
            }
        }

        try{
            while(true){
                System.out.println("UNICAST: PersonArray server listening on port: " + 9999);
                Socket s = ss.accept();
                ObjectOutputStream objectOutput = new ObjectOutputStream(s.getOutputStream());
                objectOutput.writeObject(personArrays);
                objectOutput.flush();
                System.out.println("UNICAST: PersonArray sent to : " + s.getInetAddress() + ":" + s.getPort() + "\n");
                objectOutput.close();
                s.close();
            }
        } catch (IOException ioe){
            System.err.println("ERROR ON SERVER. SERVER SHUTDOWN!!");
            ioe.printStackTrace();
        }

    }

    public static void main(String args[]) throws Exception {
        XMLServer xmlServer = new XMLServer();
        xmlServer.start();

        UDPServer udpServer = new UDPServer();
        udpServer.start();
    }


}

class UDPServer extends Thread{

    private int BUFFER_LENGTH = 4;
    private String MCAST_ADDRESS = "228.5.6.7";
    private int DEST_PORT = 6789;

    public UDPServer()  {

    }

    public void run(){
        try {
            byte[] b = new byte[BUFFER_LENGTH];
            MulticastSocket mSocket = new MulticastSocket(DEST_PORT); // must bind receive side
            InetAddress MCAST_GROUP = InetAddress.getByName(MCAST_ADDRESS);
            mSocket.joinGroup(MCAST_GROUP);

            while(true) {
                System.out.println("MULTICAST: waiting for requests...");
                DatagramPacket dgram = new DatagramPacket(b, b.length);
                mSocket.receive(dgram); // blocks until a datagram is received
                System.out.println("Received " + dgram.getLength() + " (" + new String(dgram.getData()) + ") " +
                        " bytes from " + dgram.getAddress());

                DatagramPacket hi = new DatagramPacket(dgram.getAddress().getAddress(), dgram.getAddress().getAddress().length, MCAST_GROUP, DEST_PORT);
                mSocket.send(hi);

                dgram = new DatagramPacket(b, b.length);
                mSocket.receive(dgram); // blocks until a datagram is received

                System.out.println("MULTICAST: Received from myself: " + InetAddress.getByAddress(dgram.getData()));


            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}