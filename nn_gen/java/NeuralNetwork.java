package nn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Scanner;

public class NeuralNetwork {
	
	private int input;						// Numero di neuroni in input
	private int hidden;						// Numero di neuroni nell'hidden layer
	private int output;						// Numero di neuroni nell'output

	private double[][] inputWeights;		// Pesi tra input-hidden
	private double[][] outputWeights;		// Pesi tra hidden-output
	
	private double[] inputBias;				// Bias tra input-hidden
	private double[] outputBias;			// Bias tra hidden-output
	
	// Costruttore rete neurale
	public NeuralNetwork(int input, int hidden, int output) {
		
		this.input = input;
		this.hidden = hidden;
		this.output = output;
		
		inputWeights = new double[input][hidden];
		outputWeights = new double[hidden][output];
		
		inputBias = new double[hidden];
		outputBias = new double[output];
	}
	
	// Stampa i pesi tra input-hidden
	public void printInputWeights() {
	
		for (int i=0; i< this.input; i++) {
			for (int j=0; j<this.hidden; j++) {
				System.out.println(this.inputWeights[i][j]);
			}
		}
	}
	
	// Stampa i pesi tra hidden-output
	public void printOutputWeights() {
		
		for (int i=0; i< this.hidden; i++) {
			for (int j=0; j<this.output; j++) {
				System.out.println(this.outputWeights[i][j]);
			}
		}
	}
	
	// Stampa i bias tra input-hidden
	public void printInputBias() {
		for (int i=0; i < this.hidden; i++) {
			System.out.println(this.inputBias[i]);
		}
	}
	
	// Stampa i pesi tra hidden-output
	public void printOutputBias() {
		for (int i =0; i < this.output; i++) {
			System.out.println(this.outputBias[i]);
		}
	}
	
	// Stampa riepilogo pesi rete neurale
	public void summary() {
		
		System.out.println("\nInput:");
		this.printInputWeights();
		System.out.println("\nOutput:");
		this.printOutputWeights();
		System.out.println("\nInputBias");
		this.printInputBias();
		System.out.println("\nOutputBias");
		this.printOutputBias();
	}
	
	// Crea una un oggetto rete neurale, con i pesi definiti nel file "filename"
	public static NeuralNetwork loadNeuralNetworkFromFile(String filename) {
		try {
			String s = NeuralNetwork.readFileToString(filename);
			return NeuralNetwork.loadNeuralNetworkFromString(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	// Crea un oggetto rete neurale, con i pesi definiti nella stringa "s"
	public static NeuralNetwork loadNeuralNetworkFromString(String s) {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(s).useDelimiter(",");
		
		int input = Integer.parseInt(scanner.next());
		int hidden = Integer.parseInt(scanner.next());
		int output = Integer.parseInt(scanner.next());
		
		NeuralNetwork nn = new NeuralNetwork(input, hidden, output);
		
		for (int i=0; i < input; i++) {
			for (int j=0; j < hidden; j++) {
				nn.inputWeights[i][j] = Double.parseDouble(scanner.next());
			}			
		}
		
		for (int i=0; i<hidden; i++) nn.inputBias[i] = Double.parseDouble(scanner.next());
		
		for (int i=0; i < hidden; i++) {
			for (int j=0; j < output; j++) {
				nn.outputWeights[i][j] = Double.parseDouble(scanner.next());
			}			
		}
		
		for (int i=0; i<output; i++) nn.outputBias[i] = Double.parseDouble(scanner.next());
		
		scanner.close();
		return nn;
	}
	
	// Copia il testo del file "filename" all'interno di una stringa
	public static String readFileToString(String filename) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(filename));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
	
	// Genera il file "weights.h" all'interno del percorso path
	public void generateWeightsFile(String path) {
		
		StringBuilder s = new StringBuilder();
		
		s.append("#ifndef _WEIGHTS_H_");
		s.append("\n");
		s.append("#define _WEIGHTS_H_");
		s.append("\n\n");
		
		s.append("#define INPUT_SIZE");
		s.append("\t");
		s.append(String.valueOf(this.input));
		s.append("\n");
		s.append("#define HIDDEN_SIZE");
		s.append("\t");
		s.append(String.valueOf(this.hidden));
		s.append("\n");
		s.append("#define OUTPUT_SIZE");
		s.append("\t");
		s.append(String.valueOf(this.output));
		
		s.append("\n\n");
		s.append("const double weights1[INPUT_SIZE][HIDDEN_SIZE] =  \n{");
		for(int i=0; i < this.input;i++) {
			s.append("{");
			for(int j=0; j < this.hidden; j++) {
				s.append(String.valueOf(this.inputWeights[i][j]));
				if (j != (this.hidden - 1)) s.append(",");
			}
			if (i != (this.input -1)) s.append("},\n");
		}
		s.append("}};");
		
		s.append("\n\n");
		s.append("const double weights2[HIDDEN_SIZE][OUTPUT_SIZE] =  \n{");
		for(int i=0; i < this.hidden;i++) {
			s.append("{");
			for(int j=0; j < this.output; j++) {
				s.append(String.valueOf(this.outputWeights[i][j]));
				if (j != (this.output - 1)) s.append(",");
			}
			if (i != (this.hidden -1)) s.append("},\n");
		}
		s.append("}};");
		
		s.append("\n\n");
		s.append("const double biases1[HIDDEN_SIZE] = \n{");
		for(int i=0; i<this.hidden; i++) {
			s.append(String.valueOf(this.inputBias[i]));
			if (i != (this.hidden - 1)) s.append(",");
		}
		s.append("};");
		
		s.append("\n\n");
		s.append("const double biases2[OUTPUT_SIZE] = \n{");
		for(int i=0; i<this.output; i++) {
			s.append(String.valueOf(this.outputBias[i]));
			if (i != (this.output - 1)) s.append(",");
		}
		s.append("};");
		
		s.append("\n\n");
		s.append("#endif");
		
		String string = s.toString();
		
		Writer writer = null;
		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream(path + "weights.h"), "utf-8"));
		    writer.write(string);
		} catch (IOException ex) {
			System.out.println("Errore creazione file in " + path);
		} finally {
		   try {writer.close();} catch (Exception ex) {/*ignore*/}
		}	
	}
	
}
