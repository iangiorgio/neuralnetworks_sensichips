package nn;

public class MainTest {

	public static void main(String[] args) {
		
		String path = "/home/giorgio/Scrivania/gen/";
		String w_filename = "weights.txt";
		
		NeuralNetwork nn = NeuralNetwork.loadNeuralNetworkFromFile(path + w_filename);
		nn.summary();
		
		nn.generateWeightsFile(path);

	}

}
