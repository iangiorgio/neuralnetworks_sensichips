#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define DATASET_SIZE 6177

/* 
	Estrae i dati per il test sulla rete neurale, e li salva in dataset_test_array.

	I dati sono nel seguente formato: per ogni riga ci sono la classe associata
	(nel nostro caso da 0 a 5) e poi le 8 misure. Tutti i valori separati da uno spazio.
		
	I dati sono forniti da Marco Ferdinandi.
*/

//[classe associata, 8 misurazioni di input]
#define COLUMN_SIZE 9
#define DATASET_PATH "dataset_test.txt"

//la matrice con tutti i dati
double dataset_test_array[DATASET_SIZE][COLUMN_SIZE];

//Apre il file specificato in path (FILE *)
FILE * open_file(char * path){
	
	FILE * fp = fopen(path, "r");
	if (fp == NULL){
		fprintf(stderr, "File %s non trovato\n", path);
		exit(EXIT_FAILURE);
	}
	return fp;
}

void set_input_data(){
	
	char * line = NULL;
	size_t len = 0; 	//lunghezza della linea letta
	ssize_t read;
	int i = 0;
	
	FILE * fp = open_file(DATASET_PATH);
	
	printf("Loading dataset test...\n");

	while ((read = getline(&line, &len, fp)) != -1) {
			
		int j=0;
		char * pch;
		pch = strtok (line,",");
		printf("Riga #%d: ", i);
		while (pch != NULL) {
			dataset_test_array[i][j] = atof(pch);
			printf("%f ", dataset_test_array[i][j]);
			j++;
			pch = strtok (NULL, ",");
  		}
		printf("\n");
		i++;
    	}	
	printf("#Righe analizzate nel dataset test: %d\n", i);
	fclose(fp);
	if (line) free(line);
}
