#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "nn.h"
#include "dataset.h"

/*
	Obiettivo: fornire un esempio di utilizzo della rete neurale implementata,
			nell'ambito di misurazioni con le API Sensichips.

			Le API Sensichips in questo codice sono del tutto fittizie
			e al solo scopo illustrativo per il team di sviluppo.
	
			In particolare individuiamo due API Sensichips fondamentali:
			
				set_measure() -> imposta il tipo di misurazione
				get_measure() -> ottiene la misurazione dal chip
			
			---> Bisogna capire come utilizzare le vere API <---
			
			Compila con:
			"gcc -Wall -Wextra -O2 -o sim sim.c nn.h dataset.h weights.h -lm"		
*/

#define DATASET_SIZE 6177

neural_network my_nn; //allocata sullo stack la rete neurale

typedef double sp_double; //il tipo di dato delle API Sensichips

int confusion_matrix[OUTPUT_SIZE][OUTPUT_SIZE];

//setta la tipologia di misurazione
void set_measure(){
}

//ottieni la misurazione con la tipologia scelta
sp_double get_measure(int test_count, int measure_i){
	return dataset_test_array[test_count][measure_i];
}

void sensor_busy(int ms){
	usleep(ms);
}

int main(){

	neural_network * nn = &(my_nn);	//puntatore alla rete neurale
	init_neural_network(nn);	//inizializzazione rete neurale
	
	set_input_data();	//carico dal database di test tutte le misurazioni e le classi giuste

	int test_count = 0; //incrementato ad ogni riga di test
	int good = 0; //incrementato ogni volta che viene trovata la classe giusta

	printf("\n");
	while(test_count < DATASET_SIZE){
		
		printf("#test: %d\n", test_count); // #test

		int true_class = (int) get_measure(test_count, 0); //prendo la classe vera relativa a quella linea di test
	
		for (int i=0; i < INPUT_SIZE; i++){

			set_measure(); //setta la tipologia di misurazione

			nn->input_nodes[i] = get_measure(test_count, i+1); //ottieni il valore della misura

			prop_input_hidden(nn, i); //propaga la misura nello strato hidden

			//sensor_busy(2000); //ricalcolo sensore
		}
		
		prop_input_bias(nn);	//propaga il nodo bias di input nello strato hidden
		activ_hidden_nodes(nn);	//applica la funzione di attivazione (relu) allo strato hidden

		//propaga lo strato hidden in output
		for(int j=0; j< HIDDEN_SIZE; j++){
			prop_hidden_output(nn, j);
		}

		prop_output_bias(nn);	//propaga il nodo bias di hidden nello strato output

		//applica la funzione di attivazione (relu) allo strato output (NON VA MESSA??)
		//la relu alla fine mi azzera tutta l'informazione!
		//activ_output_nodes(nn);

		softmax(nn->output_nodes, OUTPUT_SIZE); //normalizza, trasforma il vettore in densità di probabilità

		print_output_nodes(nn); //stampa i valori di output (ovvero le 6 classi)

		int found_class = compute_class_found(nn); //ritorno l'indice della classe trovata
		printf("Classe trovata: %d\n", found_class);
		printf("Classe vera: %d\n", true_class);
		if (found_class == true_class) good++; //se ho indovinato, incremento il contatore good
		confusion_matrix[true_class][found_class]++; //incremento la matrice nella posizione corretta
			
		// azzera i dati presenti sulla rete in modo da ripartire con una nuova computazione
		reset_data_neural_network(nn); 
		printf("\n");

		test_count++;
	}
	
	double accuracy = ((double)good/DATASET_SIZE);

	//Alla fine di tutto, stampo la matrice di confusione e la relativa Accuracy
	print_confusion_matrix(confusion_matrix);
	printf("Accuracy: %f\n", accuracy);
}
