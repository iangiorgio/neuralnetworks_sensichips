#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "weights.h"

/*
	Obiettivo: realizzare un modulo di rete neurale indipendente
			e completamente definito in "nn.h".

	Rete neurale con un solo strato hidden.
	
	Codice in inglese, commenti italiano/inglese.
*/


#define INPUT_SIZE 8
#define HIDDEN_SIZE 16
#define OUTPUT_SIZE 6

typedef struct {

	double input_nodes[INPUT_SIZE];		//nodi di input
	double hidden_nodes[HIDDEN_SIZE];	//nodi di hidden
	double output_nodes[OUTPUT_SIZE];	//nodi di output
	
	double input_weights[INPUT_SIZE][HIDDEN_SIZE];		//pesi tra input e hidden
	double output_weights[HIDDEN_SIZE][OUTPUT_SIZE];	//pesi tra hidden e output

	double input_bias_node;				//nodo bias di input
	double output_bias_node;			//nodo bias nello strato hidden

	double input_bias_weights[HIDDEN_SIZE];		//pesi di input nodo bias di input
	double output_bias_weights[OUTPUT_SIZE];	//pesi di output nodo bias di output

} neural_network;

//void load_weights();

//relu
double relu(double v){
	if (v > 0) return v;
	return 0;
}

//sigmoid (NON TESTATA)
double sigmoid(double x)
{
     double exp_value;
     double return_value;

     /*** Exponential calculation ***/
     exp_value = exp(-x);

     /*** Final sigmoid value ***/
     return_value = 1 / (1 + exp_value);

     return return_value;
}


//funzione di attivazione
double activ_func(double v){
	return relu(v);
}

void prop_input_bias(neural_network * nn){

	for(int j=0; j<HIDDEN_SIZE; j++){
		(nn->hidden_nodes)[j]  += (nn->input_bias_node)*((nn->input_bias_weights)[j]);
	}

}

void prop_output_bias(neural_network * nn){

	for(int j=0; j<OUTPUT_SIZE;j++){
		(nn->output_nodes)[j] += (nn->output_bias_node)*((nn->output_bias_weights)[j]);
	
	}

}

//propagazione input-hidden (nodo input i verso strato hidden)
void prop_input_hidden(neural_network * nn, int i){

	for(int j=0; j<HIDDEN_SIZE; j++){
		(nn->hidden_nodes)[j] += ((nn->input_nodes)[i])*((nn->input_weights)[i][j]);
	}
}

//propagazione hidden-output (nodo hidden i verso strato output)
void prop_hidden_output(neural_network * nn, int i){

	for(int j=0; j<OUTPUT_SIZE; j++){
		(nn->output_nodes)[j] += ((nn->hidden_nodes)[i])*((nn->output_weights)[i][j]);
	}
}

//applica la funzione di attivazione allo strato hidden
void activ_hidden_nodes(neural_network * nn){

	for(int j=0; j<HIDDEN_SIZE; j++){
		(nn->hidden_nodes)[j] = activ_func((nn->hidden_nodes)[j]);
	}
}

//applica la funzione di attivazione allo strato output
void activ_output_nodes(neural_network * nn){
	
	for(int j=0; j<OUTPUT_SIZE; j++){
		(nn->output_nodes)[j] = activ_func((nn->output_nodes)[j]);
	}
}

//azzera i valori sugli strati input-hidden-output (mantiene i pesi)
void reset_data_neural_network(neural_network * nn){
		

	for(int i=0; i<INPUT_SIZE; i++){
		(nn->input_nodes)[i] = 0;
	}
	
	for(int i=0; i<HIDDEN_SIZE; i++){
		(nn->hidden_nodes)[i] = 0;
	}

	for(int i=0; i<OUTPUT_SIZE; i++){
		(nn->output_nodes)[i] = 0;
	}
}

//inizializzazione parametri di una rete neurale -già- istanziata
void init_neural_network(neural_network * nn){

	reset_data_neural_network(nn); //pone gli strati input-hidden-output a zero
	
	//importazione dei pesi (pesi forniti dal Prof? per ora li prendo random)
	for(int i=0; i<INPUT_SIZE; i++){
		for(int j=0; j<HIDDEN_SIZE; j++){
			(nn->input_weights)[i][j] = weights1[i][j]; // QUI VANNO MESSI I VERI PESI
		
		}
	}
	
	for(int j=0; j<HIDDEN_SIZE; j++){
		for(int t=0; t<OUTPUT_SIZE; t++){
			(nn->output_weights)[j][t] = weights2[j][t]; // QUI VANNI MESSI I VERI PESI
		}
	}

	for(int j=0; j<HIDDEN_SIZE; j++){
		(nn->input_bias_weights)[j] = biases1[j];
	}
	
	for(int j=0;j<OUTPUT_SIZE;j++){
		(nn->output_bias_weights)[j] = biases2[j];
	}

	nn->input_bias_node = 1;
	nn->output_bias_node = 1;
}



//stampa i valori dello strato di input
void print_input_nodes(neural_network * nn){

	for(int i=0; i<INPUT_SIZE; i++){
		printf("nodo input [%d] = %f \n", i, (nn->input_nodes)[i]);
	}
}

//stampa i valori dello strato di hidden
void print_hidden_nodes(neural_network * nn){
	
	for(int i=0; i<HIDDEN_SIZE; i++){
		printf("nodo hidden [%d] = %f \n", i, (nn->hidden_nodes)[i]);
	}
}

//stampa i valori dello strato di output
void print_output_nodes(neural_network * nn){
	
	for(int i=0; i<OUTPUT_SIZE; i++){
		printf("nodo output [%d] = %f \n", i, (nn->output_nodes)[i]);
	}
}

void print_input_bias_node(neural_network * nn){

	printf("nodo bias input = %f \n", (nn->input_bias_node));

}

void print_output_bias_node(neural_network * nn){

	printf("nodo bias output = %f \n", (nn->output_bias_node));
}

void print_input_bias_weights(neural_network * nn){

	for(int j=0; j<HIDDEN_SIZE; j++){
		printf("peso bias input [%d] = %f \n", j, (nn->input_bias_weights)[j]);
	}
}

void print_output_bias_weights(neural_network * nn){

	for(int j=0; j<OUTPUT_SIZE; j++){
		printf("peso bias output [%d] = %f \n", j,  (nn->output_bias_weights)[j]);
	}
}

//stampa i pesi di input
void print_input_weights(neural_network * nn){

	for(int i=0; i<INPUT_SIZE; i++){
		for(int j=0; j<HIDDEN_SIZE; j++){
			printf("peso input [%d][%d] = %lf \n", i, j, (nn->input_weights)[i][j]);
		}
	}
}

//stampa i pesi di output
void print_output_weights(neural_network * nn){

	for(int j=0; j<HIDDEN_SIZE; j++){
		for(int t=0; t<OUTPUT_SIZE; t++){
			printf("peso output [%d][%d] = %f \n", j, t, (nn->output_weights)[j][t]);
		}
	}	

}

//stampa tutti i pesi (di input e di output)
void print_weights(neural_network * nn){
	
	print_input_weights(nn);
	print_output_weights(nn);
}

//stampa tutti i valori dei nodi (input-hidden-output)
void print_nodes(neural_network * nn){

	print_input_nodes(nn);
	print_hidden_nodes(nn);
	print_output_nodes(nn);
}

void print_stats(neural_network * nn){
	
	printf("NODI DI INPUT: \n");
	print_input_nodes(nn);
	printf("NODI HIDDEN: \n");
	print_hidden_nodes(nn);
	printf("NODI DI OUTPUT: \n");
	print_output_nodes(nn);
	printf("NODI BIAS: \n");
	print_input_bias_node(nn);
	print_output_bias_node(nn);
	
	printf("PESI DI INPUT: \n");
	print_input_weights(nn);
	printf("PESI DI OUTPUT: \n");
	print_output_weights(nn);
	printf("PESI INPUT BIAS: \n");
	print_input_bias_weights(nn);
	printf("PESI OUTPUT BIAS: \n");
	print_output_bias_weights(nn);
}

//estrae il valore massimo in un array di valori
double extract_max(double * input, int input_len){
	
	double m = input[0];
    	for (int i = 1; i < input_len; i++) {
        	if (input[i] > m) {
            		m = input[i];
		}
	}
	return m;
}

int extract_max_index(double * input, int input_len){

	int j = 0;
    	for (int i = 1; i < input_len; i++) {
        	if (input[i] > input[j]) {
            		j = i;
		}
	}
	return j;
}

//normalizza il vettore di double in numeri di probabilità con somma totale pari a 1
void softmax(double * input, int input_len){

	double m = extract_max(input, input_len); //max value of vector
	double sum = 0;

    	for (int i = 0; i < input_len; i++) {
        	sum += exp(input[i]-m);
	}

	for (int i = 0; i < input_len; i++) {
        	input[i] = exp(input[i] - m - log(sum));
	}  

}

int compute_class_found(neural_network * nn){

	int i = extract_max_index(nn->output_nodes, OUTPUT_SIZE);
	return i;	
}

void print_confusion_matrix(int confusion_matrix[OUTPUT_SIZE][OUTPUT_SIZE]){
	printf("Matrice di Confusione: \n");
	for(int i=0;i < OUTPUT_SIZE; i++){
		for(int j=0;j < OUTPUT_SIZE; j++){
			printf("%d ", confusion_matrix[i][j]);
		}
		printf("\n");
	}
}

//semplice test della funzione softmax
void softmax_test(){
	double arr[7] = {1.0, 2.0, 3.0, 4.0, 1.0, 2.0, 3.0};
  	softmax(arr, 7);
   	printf("%.3f %.3f %.3f %.3f %.3f %.3f %.3f", arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6]);
	//0.024 0.064 0.175 0.475 0.024 0.064 0.175
}
