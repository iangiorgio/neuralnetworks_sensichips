
- nn_gen contiene il codice finale della rete neurale divisa in .c e .h. C'è anche una cartella
	di uno script in Java che converte un file di testo contenente la configurazioone dei 
	pesi in un codice .h dei pesi pronto per essere utilizzato

- sensipluswinux contiene una vecchia versione di winux in cui era stato integrato lo script 
	Java descritto sopra

- versione linux è la prima rete implementata in ambiente Linux (ormai datata)

- test_accuracy contiene il codice e i dati per testare l'accuracy (e quindi la correttezza) 
	della rete neurale con i pesi e le misure fornite da Marco Ferdinandi
		
- test_time contiene il codice per eseguire test sul tempo impiegato dalla rete tra input e output 
	per diverse configurazioni, più un report sui test che ho effettuato
		
- test_consumption contiene il codice per eseguire la rete neurale in loop per 10 minuti(circa) per 
	diverse configurazioni, in modo da poter monitorare il consumo energetico