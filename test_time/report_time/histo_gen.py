import numpy as np
import matplotlib.pyplot as plt

mydir = ["double_e_softmax/", "double/", "float/"]

def compute_stat(file_name, folder):
    f = open(folder + file_name + ".txt", "r")
    
    data = []
    for line in f:
        data.append(float(line))
    
    avg =  "{:.1f}".format(np.mean(data))
    var =  "{:.1f}".format(np.var(data))
    
    plt.hist(data,bins='auto')
    plt.xlabel('micros MLP')
    plt.ylabel('#sample')
    plt.grid(True)
    plt.title(file_name +"_histogram" +"   avg:" + avg + "   var:" + str(var))
    plt.savefig(folder + "img/"+ file_name  + "_histogram.png")
    plt.show()
    

for elem in mydir:
    compute_stat("3input_16hidden", elem)
    compute_stat("3input_32hidden", elem)
    compute_stat("3input_64hidden", elem)
    compute_stat("8input_16hidden", elem)
    compute_stat("8input_32hidden", elem)
    compute_stat("8input_64hidden", elem)
    
    
print("finito")
    
    
    
    


