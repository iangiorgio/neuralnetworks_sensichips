#include "Arduino.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "nn.h"

#define TEST_SIZE 10000		// Numero di test da effettuare

void setup()
{
	neural_network my_nn; 				// Allocata la rete neurale sullo stack
	neural_network * nn = &(my_nn);		// Puntatore alla rete neurale
	init_random_neural_network(nn);		// Inizializzazione rete neurale

	long start_time;
	long diff_time;
	int test_count = 0; 				// Contatore per la terminazione del test

	nn_data measure[INPUT_SIZE];		// Qui vengono salvati i dati presi dai sensori

	printf("START...\n");

	while(test_count < TEST_SIZE){

		// Lettura dei dati dai sensori (adesso random)
		for(int i=0; i < INPUT_SIZE; i++) measure[i] = get_random_value(MAX_INPUT_VALUE);

		start_time = micros();		// Start time

		set_input(nn, measure);		// Set input rete con i dati presi dai sensori

		//print_input_nodes(nn); 		// Fare questa stampa per assicurarsi che gli input non siano tutti zeri

		predict(nn); 				// Predice la classe sui valori di input impostati

		//print_output_nodes(nn); 	// Fare questa stampa per assicurarsi che gli output non siano tutti zeri

		// Azzera i dati presenti sulla rete in modo da ripartire con una nuova computazione
		reset_data_neural_network(nn);

		diff_time = micros() - start_time;			// Fine computazione
		printf("%ld\n", diff_time);					// Stampa del tempo trascorso in microsecondi

		test_count++;
	}

	printf("END...\n");
}


void loop(){ }
