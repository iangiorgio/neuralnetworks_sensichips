#include "Arduino.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "nn.h"

#define TEST_SIZE 1885606		// Numero di test da effettuare

/*	Per effettuare un test da circa 10 minuti, modificare TEST SIZE in base al tipo di configurazione scelta:
 *
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 16, SOFTMAX = 1, nn_data = double -----> TEST_SIZE = 1885606
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 32, SOFTMAX = 1, nn_data = double -----> TEST_SIZE = 1415429
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 64, SOFTMAX = 1, nn_data = double -----> TEST_SIZE =	925641
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 16, SOFTMAX = 1, nn_data = double -----> TEST_SIZE = 1444391
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 32, SOFTMAX = 1, nn_data = double -----> TEST_SIZE = 1036987
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 64, SOFTMAX = 1, nn_data = double -----> TEST_SIZE = 653596
 *
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 16, SOFTMAX = 0, nn_data = double -----> TEST_SIZE = 4704038
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 32, SOFTMAX = 0, nn_data = double -----> TEST_SIZE = 2539468
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 64, SOFTMAX = 0, nn_data = double -----> TEST_SIZE = 1321878
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 16, SOFTMAX = 0, nn_data = double -----> TEST_SIZE = 2656396
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 32, SOFTMAX = 0, nn_data = double -----> TEST_SIZE = 1552033
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 64, SOFTMAX = 0, nn_data = double -----> TEST_SIZE = 829166
 *
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 16, SOFTMAX = 0, nn_data = float -----> TEST_SIZE = 38860105
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 32, SOFTMAX = 0, nn_data = float -----> TEST_SIZE = 22650057
 * 	INPUT_SIZE = 3, HIDDEN_SIZE = 64, SOFTMAX = 0, nn_data = float -----> TEST_SIZE = 4261945
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 16, SOFTMAX = 0, nn_data = float -----> TEST_SIZE = 21652834
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 32, SOFTMAX = 0, nn_data = float -----> TEST_SIZE = 14807503
 * 	INPUT_SIZE = 8, HIDDEN_SIZE = 64, SOFTMAX = 0, nn_data = float -----> TEST_SIZE = 8646780
 *
 */

void setup()
{
	neural_network my_nn; 				// Allocata la rete neurale sullo stack
	neural_network * nn = &(my_nn);		// Puntatore alla rete neurale
	init_random_neural_network(nn);		// Inizializzazione rete neurale

	unsigned long start_time;
	unsigned long diff_time;	// Tempo trascorso in millisecondi
	double diff_time_s;			// Tempo trascorso in secondi
	double diff_time_m;			// Tempo trascorso in minuti
	unsigned long test_count = 0; 				// Contatore per la terminazione del test

	nn_data measure[INPUT_SIZE];		// Qui vengono salvati i dati presi dai sensori

	printf("START... \n");

	start_time = millis();		// Start time

	while(test_count < TEST_SIZE){

		// Lettura dei dati dai sensori (adesso random)
		for(int i=0; i < INPUT_SIZE; i++) measure[i] = get_random_value(MAX_INPUT_VALUE);

		set_input(nn, measure);		// Set input rete con i dati presi dai sensori

		//print_input_nodes(nn); 		// Fare questa stampa per assicurarsi che gli input non siano tutti zeri

		predict(nn); 				// Predice la classe sui valori di input impostati

		//print_output_nodes(nn); 	// Fare questa stampa per assicurarsi che gli output non siano tutti zeri

		// Azzera i dati presenti sulla rete in modo da ripartire con una nuova computazione
		reset_data_neural_network(nn);

		test_count++;
	}

	diff_time = millis() - start_time;						// Fine computazione
	diff_time_s = diff_time/1000.0;
	diff_time_m = diff_time_s/60.0;
	printf("Tempo trascorso in millisecondi: %lu \n", diff_time);		// Stampa del tempo trascorso in milli secondi
	printf("Tempo trascorso in secondi: %f \n", diff_time_s);		// Stampa del tempo trascorso in secondi
	printf("Tempo trascorso in minuti: %f \n", diff_time_m);		// Stampa del tempo trascorso in minuti
	printf("END...\n");
}


void loop(){ }
