#include "Arduino.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "nn.h"
#include <SPI.h>
#include <SD.h>

#define DATASET_SIZE 			6177				// Numero di righe del file da leggere
#define COLUMN_SIZE 			9					// Numero di elementi per ogni riga del file
#define DATASET_TEST_PATH 		"/dataset_test.txt"	// Path in cui aprire il file

// Salva in s la prossima riga da analizzare
void get_next_line(File f, char * s){

	char c;
	int i = 0;
	while ((c=f.read())!= '\n'){
		s[i] = c;
		i++;
	}
	s[i] = '\0';
}

// Ottiene la misurazione i-esima
double get_measure(int test_count, int measure_i, File * dataset_file){

	static double measure[COLUMN_SIZE] = {0}; // Vettore in cui salvare la misura corrente
	char s[500];
	if (measure_i==0){ // Nuova riga
		get_next_line(*dataset_file, s);
		int j=0;
		char * pch;
		pch = strtok (s,",");
		while (pch != NULL) {
				measure[j] = atof(pch);
				j++;
				pch = strtok (NULL, ",");
		}
	}
	return measure[measure_i];
}

void setup()
{
	neural_network my_nn; 										// Rete neurale allocata sullo stack
	neural_network * nn = &(my_nn);								// Puntatore alla rete neurale
	int confusion_matrix[OUTPUT_SIZE][OUTPUT_SIZE] = {0};		// Matrice di confusione

	init_neural_network(nn);		// Inizializza la rete neurale
	int test_count = 0; 			// Incrementato a ogni riga di test
	int good_count = 0; 			// Incrementato ogni volta che viene trovata la classe giusta

	File dataset_file; 				// File da cui leggere i dati

	if (!SD.begin(4)) {
				Serial.println("Fallita init memoria SD");
				while (1);	//Se fallisce lo start dell'SD, rimango bloccato qui in loop
			}
	Serial.println("Riuscita init memoria SD");

	dataset_file = SD.open(DATASET_TEST_PATH);		// Apro il file presente nella memoria SD

	while(test_count < DATASET_SIZE){

		printf("#Test: %d\n", test_count); 		// Stampa a che riga di test siamo

		int true_class = (int) get_measure(test_count, 0, &dataset_file); // Prendo la classe vera relativa a quella riga di test

		// Se ho subito il vettore dei dati, posso usare anche set_input()
		for (int i=0; i < INPUT_SIZE; i++){
			nn->input_nodes[i] = get_measure(test_count, i+1, &dataset_file);  // Ottieni il valore della misura
		}

		predict(nn);	// Predice la classe sui valori di input impostati

		int found_class = compute_class_found(nn); 		// Mi dice la classe trovata

		printf("Classe predetta: %d\n", found_class);	// Classe predetta VS Classe vera
		printf("Classe vera: %d\n", true_class);

		if (found_class == true_class) good_count++; 	// Se ho indovinato la classe, aumento contator good
		confusion_matrix[true_class][found_class]++; 	// Incremento di 1 la matrice di confusione nella giusta posizione

		reset_data_neural_network(nn); // Azzera i dati presenti sulla rete in modo da ripartire con una nuova computazione

		test_count++;	// Prossimo test i
		printf("\n");
	}

	//Alla fine di tutto, stampo la matrice di confusione e la relativa Accuracy
	double accuracy = ((double)good_count/DATASET_SIZE);
	print_confusion_matrix(confusion_matrix);
	printf("Accuracy: %.9f\n", accuracy);

}

void loop(){}
